[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.12205430.svg)](https://doi.org/10.5281/zenodo.12205430) [![pipeline status](https://codebase.helmholtz.cloud/hereon-mb/herbie/badges/main/pipeline.svg)](https://codebase.helmholtz.cloud/hereon-mb/herbie/-/commits/main)  [![Latest Release](https://codebase.helmholtz.cloud/hereon-mb/herbie/-/badges/release.svg)](https://codebase.helmholtz.cloud/hereon-mb/herbie/-/releases)

# Herbie - The Semantic Laboratory Notebook & Research Database

Herbie is the electronic laboratory notebook (ELN) and research database
developed at the [Helmholtz-Zentrum Hereon](https://hereon.de) to
facilitate the digitalization efforts within several institutes.  Herbie
builds upon semantic technologies like
[OWL](https://www.w3.org/TR/owl2-primer/) and
[SHACL](https://www.w3.org/TR/shacl/) to offer a user-friendly interface
to the [RDF ecosystem](https://www.w3.org/TR/rdf11-primer/).

Herbie is a platform to collaboratively work on a shared knowledge
graph.  At its core it is a wrapper around an RDF triple store, offering
additional features like versioning, user handling, and management of
access rights.

Users can upload any RDF graph to Herbie in one of the standard formats
like [Turtle](https://www.w3.org/TR/turtle/) or
[JSON-LD](https://www.w3.org/TR/json-ld/). This includes, in particular
vocabularies and ontologies, as well as documents containing SHACL
shapes.

Moreover, Herbie offers an advanced form generation feature which turns
all SHACL documents into user-friendly web forms.  By filling out and
submitting these forms, end users can contribute to a fully semantically
annotated knowledge graph without having to deal with the intricacies of
RDF.

To eventually extract the entered knowledge as tabular data (CSV), JSON,
or in graph form, Herbie offers a default SPARQL endpoint as well as
a simple user interface to interactively create advanced queries which
are run against the (virtual) knowledge graph belonging to the
requesting user.

Herbie provides the means to offer structured, well-adapted protocols to
capture standardized processes and offering an easy and flexible
documentation platform.  It can be flexibly adapted to the needs of any
(scientific) domain (e.g. material science, biology, medicine) making it
a perfect choice for interdisciplinary groups who want to create a rich
and connected data pool.


## Contents

[[_TOC_]]


## Try out

You can try out Herbie by running a local version using docker and
docker compose:

```shell
git submodule update --init
./scripts/build-docker-images.sh
docker compose -f compose.prod.yaml up -d
docker compose -f compose.prod.yaml run django python manage.py migrate
docker compose -f compose.prod.yaml run django python manage.py runscript seed_development_database
```

After doing this, you should be able to access Herbie on
http://localhost:2194/ and log in as `admin` with password `secret`.
There are also three user accounts available, `alice`, `bob`, and
`cindi`, also with password `secret`.


## Getting started

We will give a quick tour of what is possible with Herbie.  This
includes:

- Uploading OWL ontologies and SHACL documents
- Creating data graphs using these documents
- Getting an overview of all entered data
- Performing advanced queries

### Uploading OWL ontologies and SHACL documents

To get an idea of what is possible with Herbie, you can start by
importing the exemplary RO-Crate
[coffee.zip](https://codebase.helmholtz.cloud/hereon-mb/ontologies/coffee/-/raw/main/coffee.zip)
from <https://codebase.helmholtz.cloud/hereon-mb/ontologies/coffee>.
This crate contains a small ontology to model a coffee brewing process
as well as [SHACL](https://www.w3.org/TR/shacl/) documents which Herbie
can use to offer web forms for entering data which uses this ontology.

To import this file you first have to create a new workspace.  Click on
<kbd>Create workspace</kbd> and in the dialog enter a name and click
<kbd>Create workspace</kbd>.  Then click on this newly created workspace
to open it.  Now click on the blue <kbd>+</kbd>-button at the top left
of the page and then select <kbd>Import RO-Crate</kbd> from the menu.
Select the zip file, and if everything was successful you should now see
a number of documents in Herbie.


### Creating data graphs using these documents

Now it is possible to, for example, enter data for a "Roasting"-proces.
To do this, you have to again click on the <kbd>+</kbd>-button at the
top left of the page, but this time select <kbd>Document</kbd> from the
menu.  This will open up a dialog listing all possible kinds of entities
you can create.  To continue with our example, click on the entry
<kbd>Roasting</kbd> in this list.

You will then be presented with a web form which you can fill out to
entery all the parameters of the coffee roasting process.  When you are
finished click on <kbd>Submit</kbd> at the bottom of the page, to
finally persist all the data you have entered in Herbie.  When Herbie
has finished persisting this entry in the database, you should see
a card view of your entry.

You could now continue, to create more entries, for example for the
grinding, brewing or tasting processes.


### Getting an overview of all entered data

After entering your data into Herbie, you probably want to be able to
find the entries you have added.  One way to do this, is to click on
<kbd>Explorer</kbd> on the navigation rail on the left side.  This will
bring you to the explorer, which you can use to find all entries of
a specific class  which were entered into Herbie.

On the left side you should now see a list of all available classes
within our currently active workspace.  Select the <kbd>Roasting</kbd>
class, then next to the list of classes another list appears containing
all instances of the class which you have selected. Select one of these
to see some information about this instance on the right of the screen.
In particular, you will get a link to the document which you have
created earlier to add this instance to Herbie.


### Performing advanced queries

To perform more advanced queries against the knowledge graph you have
created with Herbie, we have integrated the
[Sparklis](https://github.com/sebferre/sparklis) query builder frontend.
Click on <kbd>Sparklis</kbd> on the navigation rail on the left, to open
up this query builder.

When you uploaded your ontologies and created some instances, you should
be able to build queries in Sparklis to retrieve this data.


## Development

To create a local development environment for Herbie you need to install
Docker, Python and NodeJS on your machine and run:

```shell
python -m venv .venv
source .venv/bin/activate
pip install --upgrade pip
pip install --upgrade pipenv
pipenv sync --dev
cp .env.example .env
```

To start a development environment, run each of these three commands in
a separate shell:

```shell
docker compose up
celery -A eln worker -l DEBUG
python manage.py migrate && python manage.py runserver
```

If you are running your development environment for the first time, you
might want initialize the database by running:

```shell
python manage.py runscript seed_development_database
```

This will create a super user `admin` and three normal user accounts
`alice`, `bob`, `cindi`, all with password `secret`.

Finally, to compile all frontend assets, run the following in another
shell:

```shell
cd frontend
npm ci && npm run watch
```

Now, your local Herbie instance should be available at
http://localhost:8000/.


## Releases and source code

You can find all releases of Herbie as well as its latest source code at
[codebase.helmholtz.cloud/hereon-mb/herbie](https://codebase.helmholtz.cloud/hereon-mb/herbie).


## Contact

Do not hesitate to contact us if you have questions, comments, or
feedback. You can reach us at herbie@hereon.de.


## Status

This is actively developed by members of the Institute of Metallic
Biomaterials and the Institute of Membrane Research at Hereon.


## LICENSE

This program is subject to the terms and conditions for non-commercial
use of ELN. You can find the license text in the file LICENSE.en and
under
https://codebase.helmholtz.cloud/hereon-mb/herbie/-/raw/main/LICENSE.en.
If you have any questions or comments, you can contact us at hereon at
herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
Max-Planck-Straße 1, 21502 Geesthacht, Germany.

### Commercial use

Please get in contact with us if you would like to use Herbie
commercially. You can reach us at herbie@hereon.de.


## Authors

Development is led by Catriona Eschke and Fabian Kirchner, within the
group of Regine Willumeit-Römer, with additional contributions by
Anke-Lisa Höhme, Miłosz Meller, Alexander Foremny, Martin Held, and
Sayed Ahmad Sahim.


## Citing

Kirchner, F., Eschke, C., Höhme, A.-L., Meller, M., Foremny, A., Held,
M., Sahim, S. A., & Willumeit-Römer, R. (2024). Herbie - The Semantic
Laboratory Notebook & Research Database (0.1). Zenodo.
https://doi.org/10.5281/zenodo.12205430
