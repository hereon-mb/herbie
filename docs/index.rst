.. Herbie documentation master file, created by
   sphinx-quickstart on Tue Aug 30 09:56:58 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Herbie's documentation!
==================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   development
   docker
   shacl_recipies


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
