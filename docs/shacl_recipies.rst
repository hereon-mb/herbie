SHACL Recipies
==============

Simple fields
-------------

Depending on the properties within a `sh:PropertyShape`, Herbie decides
what kind of form field it renders.

Every field needs at least a type annotation and a `sh:path` value::

  [] a sh:PropertyShape ;
     sh:path :property .


.. toctree::

   simple_fields/text_input
   simple_fields/date_and_time_input
   simple_fields/numerical_input_without_units
   simple_fields/quantity_input
   simple_fields/checkbox
   simple_fields/single_select_combobox
   simple_fields/multi_select_combobox
   simple_fields/file_upload



Container fields
----------------

.. toctree::

   container_fields/collection
   container_fields/grid
   container_fields/variants



Layout optimizations
--------------------

Group
^^^^^

You can group fields by creating a `sh:PropertyGroup` and adding some
`sh:PropertyShape`'s to them::

  :Group a sh:PropertyGroup ;
    rdfs:label "label of the group"@en .

  [] a sh:PropertyShape ;
     sh:path :property
     sh:group :Group .

You can also nest groups inside each other.


Order
^^^^^

To adjust the order in which fields and groups are rendered, you can
provide a `sh:order` value::

  :Group a sh:PropertyGroup ;
    rdfs:label "label of the group"@en
    sh:order 0 .

  [] a sh:PropertyShape ;
     sh:path :property
     sh:group :Group
     sh:order 4 .
