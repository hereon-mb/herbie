Docker setup for Herbie
=======================

Installation
------------

You need to have `docker <https://docs.docker.com/get-docker/>`_ installed and
you need to clone this repository. We also recommend that you install `docker
compose <https://docs.docker.com/compose/install/>`_.

Once you did this, clone the ``herbie`` repository::

  $ git clone https://codebase.helmholtz.cloud/hereon-mb/herbie.git
  $ cd herbie

Now you need to build and deploy the images. We recommend that you do this via
``docker compose`` (see below).



Using docker compose
--------------------

The recommended way to build and deploy the files in this repository is to use
``docker compose``. You can install this manually for your operating system
(see the `docker docs <https://docs.docker.com/compose/install/>`_).


Building the images
^^^^^^^^^^^^^^^^^^^

You first need to build the images locally via::

  $ ./docker/build.sh

This will build three images

* ``herbie-nodejs`` contains all the frontend assets
* ``herbie-django`` runs the django application
* ``herbie-nginx`` runs the nginx webserver


Running the containers
^^^^^^^^^^^^^^^^^^^^^^

To run the containers, simply do a::

  $ docker compose --file docker-compose.prod.yml up

or if you want to detach it, run::

  $ docker compose --file docker-compose.prod.yml up -d

If uploading files should also work, you have to adjust the ownership of the
media volume, so that the django container can write to it::

  $ docker compose --file docker-compose.prod.yml exec -u 0:0 django chown -R default /opt/app-root/src/media


Deploying in production
^^^^^^^^^^^^^^^^^^^^^^^

If you want to deploy the containers in production, make sure to set the secret
variables in an `.env` file (see the `docs
<https://docs.docker.com/compose/env-file/>`_). This file will be ignored by
the version control system. You need to add the following variables in this
`.env`-file:

* `POSTGRES_PASSWORD`: The password for the postgres user

* `ALLOWED_HOSTS`: This should include the host names that you allow from where
  the website is accessed (see `the django docs on allowed hosts
  <https://docs.djangoproject.com/en/3.2/ref/settings/#allowed-hosts>`_).
  Multiple hosts should be delimited by a white space (e.g. `www.example.com
  example.com`)

* `DJANGO_SECRET_KEY`: The secret key for the django application (see `the
  django docs on secret keys
  <https://docs.djangoproject.com/en/3.2/ref/settings/#secret-key>`_). You can
  generate a random one via::

    from django.core.management.utils import get_random_secret_key
    print(get_random_secret_key())
