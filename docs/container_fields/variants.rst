Variants
^^^^^^^^

If a (nested) node shape can come in different variants, you can use
``sh:or`` to make each variant selectable.

.. literalinclude:: ../examples/ontologies/example/input_variants.ttl
   :language: turtle
   :linenos:
   :lines: 15-
   :emphasize-lines: 65-70
