Collection
^^^^^^^^^^

You can nest another node shape inside a node shape to generate
sub-forms.  To do this, you have to provide the nested node shape via
the ``sh:node`` property, its class via the ``sh:class`` property, and
the property ``dash:editor dash:DetailsEditor``.

If you want these sub forms to be re-orderable by the user, you have to
add ``hash:detailsEditorOrderPath :position``, where ``:position`` is
the name of the property where you want to store the position of the
particular instance.  These will have the datatype ``xsd:integer`` and
the first instance will have position ``1``.

.. literalinclude:: ../examples/ontologies/example/collection/1.0.0.ttl
   :language: turtle
   :linenos:
   :lines: 15-
   :emphasize-lines: 28-31,50,52-60
