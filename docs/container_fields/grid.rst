Grid
^^^^

You can render sub-forms in a two-dimensional grid using the following
property shape::

  [] a sh:PropertyShape ;
     sh:order 1 ;
     sh:path :property ;
     sh:node :OtherClassShape ;
     sh:class :OtherClass ;
     dash:editor [
       a hash:GridEditor ;
       hash:gridEditorAvailableRowsPath ( :row ) ;
       hash:gridEditorAvailableColumnsPath ( :column ) ;
       hash:gridEditorRowPath :row ;
       hash:gridEditorColumnPath :column ;
     ] .

  :OtherClassShape a sh:NodeShape .

  :OtherClass a rdfs:Class .
