Development
===========

Dependencies:

* Webserver:

  * python 3.9
  * pip
  * nodejs 16
  * docker & docker-compose or postgresql

* Authentication via LDAP:

  * cyrus_sasl
  * openldap

* Locally testing CI setup:

  * docker
  * docker-compose


Initial setup
-------------

Python environment
^^^^^^^^^^^^^^^^^^

To initially setup a python environment with all dependencies, you have to
run::

   $ python3 -m venv venv         # create a python virtual environment
   $ source venv/bin/activate     # activate virtual environment (Linux or Windows within Git Bash)
   $ ./venv/Scripts/activate.bat  # activate virtual environment (Windows within Command Prompt)
   $ pip install --upgrade pip    # upgrade to latest pip version
   $ pip install pipenv           # install pipenv which we use to manage dependencies
   $ pipenv sync                  # install all python dependencies

After the initial setup you only need to activate the environment when you are
working on the project.


Environment for building frontend assets
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To get a nodejs environment for building the frontend assets, you have to run::

   $ cd frontend
   $ npm install                   # fetch all nodejs dependencies
   $ git submodule update --init   # make all git submodules available
   $ npm run elm-spa-gen           # generate boilerplate


Additional information for Windows
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Git
"""

You download the Windows installer from `here
<https://git-scm.com/download/win>`_. The default answers for the questions
asked when installing are a good choice.  When working with git it is
recommended to always use the Git Bash instead of the standard Windows command
line.


SSH-Key
"""""""

To simplify access to the Gitlab-Repository, you can create an SSH key.  To do
this run ``ssh-keygen -t rsa`` within the Git Bash.  The default location
(``/u/.ssh/id_rsa``) is a good default.  To make it available for
gitlab.hzdr.de you have to go to the `SSH Keys Page
<https://gitlab.hzdr.de/-/profile/keys>`_ and paste the text returned when
running ``cat /u/.ssh/id_rsa.pub`` into the key text area and then adding the
key.

When this was successfull accessing the Gitlab repository with SSH should be
possible.  For example you can now clone it with ``git clone
git@gitlab.hzdr.de:hereon-mb/herbie.git``.

For convenience, if you dont want to type in your SSH key passphrase every
time.  You can run::

   $ eval $(ssh-agent)
   $ ssh-add

at the begining of each Git Bash session.


Python
""""""

You can get the Windows installer from the `Python Homepage
<https://www.python.org/downloads/windows/>`_.  Make sure to pick the one for
the latest Python 3 relase.  In order to be able to install Python without
admin rights, you have to untick the option to install it for all users on the
computer.


Node.js
"""""""

You can get the Windows installer from the `Node.js Homepage
<https://nodejs.org/en/download/>`_.  You will need to have admin privileges to
complete the installation with the installer.

It is also possible to install Node.js on Windows without admin rights.  For
this, you have to download the Windows binary ``.zip``-file and unpack this
somewhere on your computer, e.g. your personal folder on ``C``.  Then you have
to add folder to your ``PATH`` environment variable. To do this press ``Win
+ R`` and run ``rundll32 sysdm.cpl,EditEnvironmentVariables``.  Then select the
``PATH`` entry in the list and press ``Edit`` and ``New`` and enter the full
path of the folder where you unzipped the Node.js binaries.  After restarting
the Git Bash you should have ``npm`` and ``npx`` available.


Development
-----------

Build frontend assets
^^^^^^^^^^^^^^^^^^^^^

For local development you should run webpack in a separate shell to
automatically rebuild all frontend assets::

   $ cd frontend
   $ npx webpack --mode development --watch


Generate frontend code
^^^^^^^^^^^^^^^^^^^^^^

We are using `elm-codegen <https://github.com/mdgriffith/elm-codegen>`_ to
generate frontend boilerplate from the API schema.  To build the dependencies
for elm-codegen you can run::

   $ cd frontend
   $ npm run codegen-install   # to build the dependencies in frontend/codegen/Gen/
   $ npm run codegen-generate  # to generate the actual boilerplate in frontend/generated/


Run local webserver
^^^^^^^^^^^^^^^^^^^

Make sure to activate the virtual environment before running python.

Either have a PostgreSQL instance and an Apache Jena Fuseki instance
running on your machine or start these using Docker::

   $ docker-compose up

When you are starting for the first time, you should run migrations and create
a superuser account::

   $ python manage.py migrate          # create/migrate sqlite database
   $ python manage.py createsuperuser  # create a super user
   $ python manage.py runserver        # run a local development server

If you want to start with some initial data you can run::

   $ python manage.py migrate
   $ python manage.py runscript seed_development_database
   $ python manage.py runscript generate_protocols
   $ python manage.py runserver

This will create a super user account ``admin`` with password ``secret``, as
well as some normal user accounts, some furnaces, moulds, and a few randomly
generate cast and heat treatment protocols.


Run test suite
^^^^^^^^^^^^^^

We are using `pytest <https://docs.pytest.org>`_ for all tests, `black
<https://black.readthedocs.io/en/stable/>`_ for automatic code
formatting/linting and `mypy <http://mypy-lang.org/>`_ for static stype
checking. You can run all python tests and linters with::

   $ source venv/bin/activate
   $ tox

For the frontend we use `elm-test
<https://package.elm-lang.org/packages/elm-explorations/test/latest/>`_ for
tests, `elm-format <https://github.com/avh4/elm-format>`_ for automatic code
formatting and `elm-review
<https://package.elm-lang.org/packages/jfmengels/elm-review/latest/>`_ for
linting.  You can run these with::

   $ cd frontend
   $ npx elm-test --fuzz 1
   $ npx elm-review
   $ npx elm-format src/


Coding Guidelines
-----------------

Fluent (i18n)
^^^^^^^^^^^^^

We use `Fluent <https://projectfluent.org>`_ for internationalization.  Make
sure to read the `guide <https://projectfluent.org/fluent/guide/>`_ to know
what is possible.

When creating Fluent messages, make sure you follow these guidelines:

* Create separate ``.ftl``-files for each Elm module, i.e. for
  ``frontend/src/Form/Note.elm`` there should be
  ``frontend/src/Form/Note.en-US.ftl`` and ``frontend/src/Form/Note.de.ftl``.

* Prefix all message names with a hyphenized version of the full module, i.e.
  for ``Form.Note`` the messages are of the form ``form-note--...``.

* After the module prefix add one of the following scopes:

  * ``..--action--..`` used for buttons
  * ``..--heading--..``
  * ``..--paragraph--..``
  * ``..--input--..``
  * ``..--link--..``
  * ``..--title--..``
  * ``..--validation--..``

* Add a new scope if you run into messages which do not fit.

* Use globally defined `terms
  <https://projectfluent.org/fluent/guide/terms.html>`_, you can find these in
  ``frontend/src/terms.*.ftl``.

* Feel free to create additional terms for e.g. terminology only used in
  specific protocols which can be reused in different places.

* Run ``python scripts/cleanup-fluent.py`` before commmiting your changes. This
  will sort messages alphabetically and adjust the formatting.



