Checkbox
^^^^^^^^

Property shapes with ``sh:datatype xsd:boolean`` are rendered as
checkboxes.

.. literalinclude:: ../examples/ontologies/example/input_checkbox/1.0.0.ttl
   :language: turtle
   :linenos:
   :lines: 15-
   :emphasize-lines: 24-27,33,35-41
