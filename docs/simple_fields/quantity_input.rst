Quantity input (a.k.a numerical input with units)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To obtain field for numerical values with a unit which generates
a ``qudt:Quantity``, you have to provide the following property shape::

   [] a sh:PropertyShape ;
     sh:path :property ;
     sh:class qudt:Quantity ;
     sh:node [
       sh:property [
         sh:path qudt:unit ;
         sh:hasValue unit:PERCENT ;
       ] ;
       sh:property [
         sh:path qudt:value ;
         sh:datatype xsd:decimal ;
       ] ;
     ] ;
   .

Allowed datatypes are ``xsd:integer``, ``xsd:float``, and
``xsd:decimal``. See `QUDT Units
<https://www.qudt.org/doc/DOC_VOCAB-UNITS.html>`_ for a list of all
available units.

.. literalinclude:: ../examples/ontologies/example/quantity_input/1.0.0.ttl
   :language: turtle
   :linenos:
   :lines: 15-
   :emphasize-lines: 25-36,42-44,46-99
