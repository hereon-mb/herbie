Numerical input without units
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To render a single line input field for a numerical value, you have to
add the ``sh:datatype xsd:integer``, ``sh:datatype xsd:float``, or
``sh:datatype xsd:decimal`` property to the property shape.

.. note::

   Here, float is a IEEE single-precision 32-bit floating point value
   and decimal represents any decimal number.  **When in doubt, you
   should use decimal.**

.. literalinclude:: ../examples/ontologies/example/input_numerical_without_units/1.0.0.ttl
   :language: turtle
   :linenos:
   :lines: 15-
   :emphasize-lines: 23-34,40-42,44-67
