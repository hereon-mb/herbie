Multi-select combobox (dropdown)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Similarly to the single-select combobox, you can get a combobox which
allows selecting multiple values.  Already selected values are placed at
the start of the text input and have a small icon button to remove them.

Multi-select comboboxes are generated like single-select ones, with the
only difference of having a ``sh:maxCount`` greater then 1.

.. note::

   Having no ``sh:maxCount`` means infinitely many.

.. literalinclude:: ../examples/ontologies/example/input_dropdown_multiselect/1.0.0.ttl
   :language: turtle
   :linenos:
   :lines: 15-
   :emphasize-lines: 25-28,34,36-42
