Date and datetime input
^^^^^^^^^^^^^^^^^^^^^^^

To render a text field with an included date or datetime picker, you
have to add the ``sh:datatype xsd:date`` or ``sh:datatype xsd:dateTime``
properties.

.. literalinclude:: ../examples/ontologies/example/input_date_time/1.0.0.ttl
   :language: turtle
   :linenos:
   :lines: 14-
   :emphasize-lines: 23-30,36-37,39-54
