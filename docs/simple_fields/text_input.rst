Text input
^^^^^^^^^^

To render a text field for a required value, you have to add the
``sh:datatype xsd:string`` property to the property shape as well as the
properties ``sh:minCount 1`` and ``sh:maxCount 1``.  The following
modifications are possible:

* When ``sh:minCount 0`` is given, the field becomes optional and
  a short info below the field is added.

* For a multiline field (also called text area), you have to add
  ``dash:singleLine false``.

.. literalinclude:: ../examples/ontologies/example/input_text/1.0.0.ttl
   :language: turtle
   :linenos:
   :lines: 15-
   :emphasize-lines: 24-35,41-43,45-69
