Single-select combobox (dropdown)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Herbie can render a single-select combobox which consists of a single
line text input field with an included dropdown.  The dropdown contains
a list of possible values for the text input.  By typing in the text
input, the user can narrow down this list.  Selection of the desired value can
be done using the arrow keys and enter, or by using the mouse.

Comboboxes can be generated for properties whose values come from
a specific ``rdfs:Class``.

.. literalinclude:: ../examples/ontologies/example/input_dropdown/1.0.0.ttl
   :language: turtle
   :linenos:
   :lines: 15-
   :emphasize-lines: 25-28,34,36-43
