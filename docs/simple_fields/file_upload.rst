File upload
^^^^^^^^^^^

To get a field for uploading raw data files, you have to provide the
`sh:nodeKind sh:IRI` property.

.. literalinclude:: ../examples/ontologies/example/file_upload/1.0.0.ttl
   :language: turtle
   :lines: 15-
   :emphasize-lines: 23-26,32,34-40
