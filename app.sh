daphne \
  --verbosity 2 \
  --access-log - \
  --http-timeout ${DAPHNE_HTTP_TIMEOUT:-30} \
  --application-close-timeout ${DAPHNE_APPLICATION_CLOSE_TIMEOUT:-10} \
  --bind 0.0.0.0 \
  --port 8080 \
  eln.asgi:application
