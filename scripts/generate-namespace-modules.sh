#!/usr/bin/env bash

set -x

python \
  .venv/lib/python3.12/site-packages/rdflib/tools/defined_namespace_creator.py \
  ./ro-crates/herbie/hash/1.0.0.ttl \
  "http://purls.helmholtz-metadaten.de/herbie/hash/#" \
  HASH

mv _HASH.py backend/namespace/_HASH.py


python \
  .venv/lib/python3.12/site-packages/rdflib/tools/defined_namespace_creator.py \
  ./ro-crates/herbie/core/1.0.0.ttl \
  "http://purls.helmholtz-metadaten.de/herbie/core/#" \
  HERBIE

mv _HERBIE.py backend/namespace/_HERBIE.py
