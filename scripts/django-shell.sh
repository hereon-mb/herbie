#!/usr/bin/env bash
#
# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

set -e

if [ ! ${PROJECT_NAME} ]; then
  echo -n "Enter a value for PROJECT_NAME: "
  read PROJECT_NAME
  echo
fi

if [ ! ${DJANGO_DATABASE_NAME} ]; then
  echo -n "Enter a value for DJANGO_DATABASE_NAME: "
  read DJANGO_DATABASE_NAME
  echo
fi

if [ ! ${DJANGO_DATABASE_USER} ]; then
  echo -n "Enter a value for DJANGO_DATABASE_USER: "
  read DJANGO_DATABASE_USER
  echo
fi

if [ ! ${DJANGO_DATABASE_PASSWORD} ]; then
  echo -n "Enter a value for DJANGO_DATABASE_PASSWORD: "
  read -s DJANGO_DATABASE_PASSWORD
  echo
fi

if [ ! ${DJANGO_DATABASE_HOST} ]; then
  echo -n "Enter a value for DJANGO_DATABASE_HOST: "
  read DJANGO_DATABASE_HOST
  echo
fi

if [ ! ${DJANGO_DATABASE_PORT} ]; then
  echo -n "Enter a value for DJANGO_DATABASE_PORT: "
  read DJANGO_DATABASE_PORT
  echo
fi

if [ ! ${DJANGO_STATIC_ROOT} ]; then
  echo -n "Enter a value for DJANGO_STATIC_ROOT: "
  read DJANGO_STATIC_ROOT
  echo
fi

if [ ! ${DJANGO_STATIC_URL} ]; then
  echo -n "Enter a value for DJANGO_STATIC_URL: "
  read DJANGO_STATIC_URL
  echo
fi


source ../python-venv/bin/activate

pip install ".[prod]"


env \
  DJANGO_DATABASE_NAME="$DJANGO_DATABASE_NAME" \
  DJANGO_DATABASE_USER="$DJANGO_DATABASE_USER" \
  DJANGO_DATABASE_PASSWORD="$DJANGO_DATABASE_PASSWORD" \
  DJANGO_DATABASE_HOST="$DJANGO_DATABASE_HOST" \
  DJANGO_DATABASE_PORT="$DJANGO_DATABASE_PORT" \
  DJANGO_SECRET_KEY="secret" \
  DJANGO_STATIC_ROOT="$DJANGO_STATIC_ROOT" \
  DJANGO_STATIC_URL="$DJANGO_STATIC_URL" \
  DJANGO_SETTINGS_MODULE="eln.settings.production" \
  python manage.py shell
