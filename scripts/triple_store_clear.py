# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

import logging
from django.db import transaction

from backend.services.triple_store import TripleStore
from rdflib import URIRef
from django.conf import settings


@transaction.atomic
def run():
    logger = logging.getLogger("django.server")
    triple_store = TripleStore()

    result = triple_store.query(
        """
            SELECT DISTINCT ?graph WHERE {
                GRAPH ?graph { ?s ?p ?o }
            }
        """,
    )
    graph_iris = [URIRef(binding["graph"]["value"]) for binding in result["results"]["bindings"] if "graph" in binding]
    graph_iris = [graph_iri for graph_iri in graph_iris if graph_iri.startswith(settings.IRI_PREFIX)]
    logger.info(f"Found {len(graph_iris)} which should be deleted from triple store.")

    for graph_iri in graph_iris:
        triple_store.delete_graph(graph_iri)
        logger.info(f"Deleted {graph_iri} from triple store.")
