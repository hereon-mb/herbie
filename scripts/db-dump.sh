#!/usr/bin/env bash
#
# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

set -e

if [ ! ${DB_DUMPS_DIR} ]; then
  echo -n "Enter a value for DB_DUMPS_DIR: "
  read DB_DUMPS_DIR
fi

if [ ! ${DJANGO_DATABASE_HOST} ]; then
  echo -n "Enter a value for DJANGO_DATABASE_HOST: "
  read DJANGO_DATABASE_HOST
fi

if [ ! ${DJANGO_DATABASE_PORT} ]; then
  echo -n "Enter a value for DJANGO_DATABASE_PORT: "
  read DJANGO_DATABASE_PORT
fi

if [ ! ${DJANGO_DATABASE_NAME} ]; then
  echo -n "Enter a value for DJANGO_DATABASE_NAME: "
  read DJANGO_DATABASE_NAME
fi

if [ ! ${DJANGO_DATABASE_USER} ]; then
  echo -n "Enter a value for DJANGO_DATABASE_USER: "
  read DJANGO_DATABASE_USER
fi

if [ ! ${DJANGO_DATABASE_PASSWORD} ]; then
  echo -n "Enter a value for DJANGO_DATABASE_PASSWORD: "
  read -s DJANGO_DATABASE_PASSWORD
  echo
fi


TIMESTAMP=`date +%Y-%m-%d_%H-%M-%S`
FILENAME="$DB_DUMPS_DIR/db.$DJANGO_DATABASE_NAME.$TIMESTAMP.sql.gz"

mkdir -p $DB_DUMPS_DIR

PGPASSWORD=$DJANGO_DATABASE_PASSWORD \
  pg_dump \
    --host=$DJANGO_DATABASE_HOST \
    --port=$DJANGO_DATABASE_PORT \
    --username=$DJANGO_DATABASE_USER \
    --no-password \
    --dbname=$DJANGO_DATABASE_NAME \
  | gzip \
    --to-stdout \
  > $FILENAME

echo "Successfully created db dump at $FILENAME"
