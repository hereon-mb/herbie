# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from django.contrib.auth import get_user_model
from django.db import transaction

from backend.models.author import Author
from backend.models.preferences_author import PreferencesAuthor


@transaction.atomic
def run():
    author = Author.objects.create(
        user=get_user_model().objects.create_superuser(
            username="admin",
            first_name="Super",
            last_name="User",
            email="admin@example.org",
            password="secret",
        ),
        can_create_rdf_documents=True,
    )
    PreferencesAuthor.objects.create(
        author=author,
        use_rdf_documents=True,
    )
