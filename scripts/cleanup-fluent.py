# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from glob import glob

from fluent.syntax import parse
from fluent.syntax.serializer import FluentSerializer

for filename in glob("frontend/src/**/*.ftl", recursive=True):
    entries = []

    with open(filename, "r", encoding="utf-8") as f:
        try:
            resource = parse(f.read())
        except UnicodeDecodeError as e:
            breakpoint()
            pass
        entries = resource.body
        entries = filter(lambda entry: hasattr(entry, "id"), entries)
        entries = sorted(entries, key=lambda entry: entry.id.name)

    with open(filename, "w", encoding="utf-8") as f:
        for entry in entries:
            f.write(FluentSerializer().serialize_entry(entry))
