#!/usr/bin/env bash
#
# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

set -e

if [ ! ${DB_DUMP} ]; then
  echo -n "Enter a value for DB_DUMP: "
  read DB_DUMP
fi

if [ ! ${DJANGO_DATABASE_HOST} ]; then
  echo -n "Enter a value for DJANGO_DATABASE_HOST: "
  read DJANGO_DATABASE_HOST
fi

if [ ! ${DJANGO_DATABASE_PORT} ]; then
  echo -n "Enter a value for DJANGO_DATABASE_PORT: "
  read DJANGO_DATABASE_PORT
fi

if [ ! ${DJANGO_DATABASE_NAME} ]; then
  echo -n "Enter a value for DJANGO_DATABASE_NAME: "
  read DJANGO_DATABASE_NAME
fi

if [ ! ${DJANGO_DATABASE_USER} ]; then
  echo -n "Enter a value for DJANGO_DATABASE_USER: "
  read DJANGO_DATABASE_USER
fi

if [ ! ${DJANGO_DATABASE_PASSWORD} ]; then
  echo -n "Enter a value for DJANGO_DATABASE_PASSWORD: "
  read -s DJANGO_DATABASE_PASSWORD
fi

if [[ ! -f "dumps/${DB_DUMP}.sql" ]]; then
  gzip -d "dumps/${DB_DUMP}.sql.gz"
fi
sed -i "s/django_eln_user/${DJANGO_DATABASE_USER}/g" "dumps/${DB_DUMP}.sql"

PGPASSWORD=$DJANGO_DATABASE_PASSWORD \
  createdb \
  --host=$DJANGO_DATABASE_HOST \
  --port=$DJANGO_DATABASE_PORT \
  --username=$DJANGO_DATABASE_USER \
  --no-password \
  --template=template0 \
  $DJANGO_DATABASE_NAME

PGPASSWORD=$DJANGO_DATABASE_PASSWORD \
  psql \
  --host=$DJANGO_DATABASE_HOST \
  --port=$DJANGO_DATABASE_PORT \
  --username=$DJANGO_DATABASE_USER \
  --no-password \
  --set ON_ERROR_STOP=on \
  --single-transaction \
  $DJANGO_DATABASE_NAME \
  < "dumps/$DB_DUMP.sql"

echo "ANALYZE" | \
  PGPASSWORD=$DJANGO_DATABASE_PASSWORD \
  psql \
  --host=$DJANGO_DATABASE_HOST \
  --port=$DJANGO_DATABASE_PORT \
  --username=$DJANGO_DATABASE_USER \
  --no-password \
  $DJANGO_DATABASE_NAME

