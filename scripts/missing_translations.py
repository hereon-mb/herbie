# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

import pathlib
import pprint

LANGUAGES = {"en-US", "de"}


def get_grouped_fluent_files(directory):
    subdirectories = set()
    fluent_files = {}

    for path in directory.iterdir():
        if path.is_dir():
            subdirectories.add(path)
        elif path.suffix == ".ftl":
            name, _, _ = str(path).split(".")
            if name not in fluent_files:
                fluent_files[name] = []
            fluent_files[name].append(str(path))

    for name, paths in fluent_files.items():
        yield (name, paths)

    for subdirectory in subdirectories:
        yield from get_grouped_fluent_files(subdirectory)


def get_missing_keys(paths):
    fluent_keys = {path: get_keys_from_path(path) for path in paths}

    if len(fluent_keys) < len(LANGUAGES):
        missing_languages = LANGUAGES - {path.split(".")[-2] for path in paths}
        path_front = paths[0].split(".")[0]
        for language in missing_languages:
            missing_path = f"{path_front}.{language}.ftl"
            fluent_keys[missing_path] = set()

    all_keys = set.union(*fluent_keys.values())
    return {path: list(all_keys - fluent_keys[path]) for path in paths if all_keys - fluent_keys[path]}


def get_keys_from_path(path):
    keys = set()

    with open(path, encoding="utf-8") as f:
        for line in f:
            if "=" in line:
                key = line.split("=")[0]
                keys.add(key.strip())

    return keys


def get_all_missing(start_path):
    all_missing = {}
    for _, path_group in get_grouped_fluent_files(start_path):
        all_missing.update(get_missing_keys(path_group))
    return all_missing


if __name__ == "__main__":
    start_path = pathlib.Path("./frontend/src")
    pp = pprint.PrettyPrinter()

    missing = get_all_missing(start_path)
    pp.pprint(missing)
