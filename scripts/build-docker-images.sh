#!/usr/bin/env bash


set -x

docker build . \
  --file docker/django/Dockerfile \
  --tag registry.hzdr.de/hereon-mb/herbie/django:latest

docker build . \
  --file docker/nginx/Dockerfile \
  --tag registry.hzdr.de/hereon-mb/herbie/nginx:latest
