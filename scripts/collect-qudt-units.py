# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from rdflib import Graph, Namespace

QUDT = Namespace("http://qudt.org/schema/qudt/")
UNIT = Namespace("http://qudt.org/vocab/unit/")


def main():
    data = Graph()
    data.parse("./data/unit.ttl")
    data.bind("qudt", QUDT)
    data.bind("unit", UNIT)

    result = data.query(
        """
            SELECT ?unit ?symbol WHERE {
                ?unit a qudt:Unit .
                ?unit qudt:symbol ?symbol .
            }
        """
    )

    print(result.serialize(format="json").decode("utf-8"))


if __name__ == "__main__":
    main()
