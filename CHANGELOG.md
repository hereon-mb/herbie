# Changelog [![Latest Release](https://codebase.helmholtz.cloud/hereon-mb/herbie/-/badges/release.svg)](https://codebase.helmholtz.cloud/hereon-mb/herbie/-/releases)

All notable changes to
[Herbie](https://codebase.helmholtz.cloud/hereon-mb/herbie) will be
documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/).

## [Unreleased]

### Added

- Support simple default values in SHACL form generation via `sh:defaultValue`.
- Group documents according to their IRI aliases or their document IRI
  to immitate a file browser like experience.

## [0.1] - 2024-06-21

### Added

- initial release


[unreleased]: https://codebase.helmholtz.cloud/hereon-mb/herbie/-/compare/v0.1...HEAD
[0.1]: https://codebase.helmholtz.cloud/hereon-mb/herbie/-/releases/v0.1
