module Herbie exposing (run)

import Ansi.Color
import BackendTask exposing (BackendTask)
import BackendTask.File
import BackendTask.Glob
import BackendTask.Http
import Cli.Option as Option
import Cli.OptionsParser as OptionsParser
import Cli.Program as Program
import FatalError exposing (FatalError)
import Fixture.Context as Context
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Decode
import List.NonEmpty as NonEmpty
import Ontology
import Pages.Script as Script exposing (Script)
import Pretty
import Rdf exposing (Iri)
import Rdf.DictIri as DictIri exposing (DictIri)
import Rdf.Graph as Graph exposing (Graph)
import Rdf.Namespaces.OWL as OWL
import Rdf.SetIri as SetIri exposing (SetIri)
import Result.Extra as Result
import Result.Pipeline as Result
import Shacl
import Shacl.Extra
import Shacl.Form.Canonical as Canonical
import Shacl.Form.Collected as Collected exposing (Spec)
import Shacl.Form.Grouped as Grouped
import Shacl.Form.Merged as Merged
import Shacl.Form.Translated as Translated
import Shacl.Form.Viewed as Form exposing (Form)
import String.Extra as String


run : Script
run =
    Script.withCliOptions program
        (\{ root, config, shaclDocument } ->
            BackendTask.map2 List.append (loadRoCrateMetadatas root) (loadLookups config)
                |> BackendTask.map indexGraphs
                |> BackendTask.andThen (loadGraphClosure shaclDocument)
                |> BackendTask.andThen initForm
        )


loadRoCrateMetadatas : String -> BackendTask FatalError (List File)
loadRoCrateMetadatas root =
    let
        rootCleaned =
            if String.isEmpty rootWithoutTrailingSlash then
                root

            else
                rootWithoutTrailingSlash

        rootWithoutTrailingSlash =
            String.leftOfBack "/" root
    in
    BackendTask.Glob.succeed (String.join "/")
        |> BackendTask.Glob.match (BackendTask.Glob.literal rootCleaned)
        |> BackendTask.Glob.match (BackendTask.Glob.literal "/")
        |> BackendTask.Glob.capture BackendTask.Glob.recursiveWildcard
        |> BackendTask.Glob.match (BackendTask.Glob.literal "/ro-crate-metadata.json")
        |> BackendTask.Glob.toBackendTaskWithOptions
            { includeDotFiles = False
            , include = BackendTask.Glob.OnlyFiles
            , followSymbolicLinks = True
            , caseSensitiveMatch = True
            , gitignore = False
            , maxDepth = Nothing
            }
        |> BackendTask.andThen (List.map (loadRoCrateMetadata rootCleaned) >> BackendTask.combine)
        |> BackendTask.map List.concat


loadRoCrateMetadata : String -> String -> BackendTask FatalError (List File)
loadRoCrateMetadata root path =
    let
        rootActual =
            root ++ "/" ++ path
    in
    BackendTask.File.jsonFile (decoderFiles rootActual) (rootActual ++ "/" ++ "/ro-crate-metadata.json")
        |> BackendTask.allowFatal


loadLookups : String -> BackendTask FatalError (List File)
loadLookups config =
    BackendTask.File.jsonFile decoderLookups config
        |> BackendTask.allowFatal
        |> BackendTask.onError (\_ -> BackendTask.succeed [])


indexGraphs : List File -> DictIri File
indexGraphs =
    List.map (\file -> ( file.iri, file ))
        >> DictIri.fromList


loadGraphClosure : Iri -> DictIri File -> BackendTask FatalError Graph
loadGraphClosure iri files =
    { seed = Graph.initialSeed
    , graph = Graph.emptyGraph
    , irisIncluded = SetIri.empty
    , irisRequired = SetIri.empty
    }
        |> includeGraphs files iri
        |> BackendTask.map .graph


type alias State =
    { seed : Graph.Seed
    , graph : Graph
    , irisIncluded : SetIri
    , irisRequired : SetIri
    }


includeGraphs : DictIri File -> Iri -> State -> BackendTask FatalError State
includeGraphs files iri state =
    case DictIri.get iri files of
        Nothing ->
            BackendTask.fail
                (FatalError.build
                    { title = "Could not load graph"
                    , body =
                        [ "The graph"
                        , ""
                        , "    <" ++ Rdf.toUrl iri ++ ">"
                        , ""
                        , "could not be loaded."
                        ]
                            |> String.join "\n"
                    }
                )

        Just file ->
            file
                |> parseTurtle state.seed
                |> BackendTask.andThen
                    (\( graph, seedNext ) ->
                        let
                            irisRequiredNew =
                                OWL.importsFor iri graph
                                    |> List.filter (not << isIncluded)
                                    |> SetIri.fromList

                            isIncluded iriImported =
                                SetIri.member iriImported state.irisIncluded
                                    || SetIri.member iriImported state.irisRequired

                            stateUpdated =
                                { state
                                    | seed = seedNext
                                    , graph = Graph.union state.graph graph
                                    , irisIncluded = SetIri.insert iri state.irisIncluded
                                    , irisRequired = SetIri.remove iri (SetIri.union irisRequiredNew state.irisRequired)
                                }
                        in
                        case SetIri.toList stateUpdated.irisRequired of
                            [] ->
                                BackendTask.succeed stateUpdated

                            iriRequired :: _ ->
                                includeGraphs files iriRequired stateUpdated
                    )


type Error
    = ErrorCollected Collected.Error
    | ErrorMerged Merged.Error
    | ErrorTranslated Translated.Error
    | ErrorGrouped Grouped.Error


initForm : Graph -> BackendTask FatalError ()
initForm graphShacl =
    let
        ontology =
            Ontology.fromGraph graphShacl
    in
    case Shacl.fromGraph graphShacl of
        Err errorShacl ->
            BackendTask.fail
                (FatalError.build
                    { title = "SHACL error"
                    , body = Shacl.errorToString errorShacl
                    }
                )

        Ok shapes ->
            case Shacl.Extra.nodeShapesWithTargetClasses shapes of
                Err errorShacl ->
                    BackendTask.fail
                        (FatalError.build
                            { title = "SHACL error"
                            , body = Shacl.Extra.errorToString errorShacl
                            }
                        )

                Ok targetClasses ->
                    let
                        spec =
                            { data = graphShacl
                            , shapes = shapes
                            , ontology = ontology
                            , strict = False
                            }
                    in
                    targetClasses
                        |> NonEmpty.toList
                        |> List.map (.nodeShape >> initGrouped spec)
                        |> List.intersperse (Script.log "")
                        |> BackendTask.doEach


initGrouped : Spec -> Shacl.NodeShape -> BackendTask FatalError ()
initGrouped spec nodeShape =
    case
        (Collected.fromShacl spec nodeShape |> Result.mapError ErrorCollected)
            |> Result.andThen (Merged.fromCollected >> Result.mapError ErrorMerged)
            |> Result.andThen Canonical.fromMerged
            |> Result.andThen (Translated.fromCanonical spec >> Result.mapError ErrorTranslated)
            |> Result.andThen (Grouped.fromTranslated spec >> Result.mapError ErrorGrouped)
    of
        Err error ->
            BackendTask.fail
                (FatalError.build
                    (case error of
                        ErrorCollected errorCollected ->
                            { title = "Collecting error"
                            , body = Collected.errorToString errorCollected
                            }

                        ErrorMerged errorMerged ->
                            { title = "Merging error"
                            , body = Merged.errorToString errorMerged
                            }

                        ErrorTranslated errorTranslated ->
                            { title = "Translation error"
                            , body = Translated.errorToString errorTranslated
                            }

                        ErrorGrouped errorGrouped ->
                            { title = "Grouping error"
                            , body = Grouped.errorToString errorGrouped
                            }
                    )
                )

        Ok grouped ->
            [ "●  " ++ Ansi.Color.fontColor Ansi.Color.cyan (Rdf.serializeNode nodeShape.node)
            , Form.logGrouped Context.default.cShacl spec.shapes (Grouped.FieldForm grouped)
                |> Pretty.indent 3
                |> Pretty.pretty 80
            ]
                |> String.join "\n"
                |> Script.log


parseTurtle : Graph.Seed -> File -> BackendTask FatalError ( Graph, Graph.Seed )
parseTurtle seed file =
    let
        parse raw =
            let
                rawWithBase =
                    "@base <" ++ Rdf.toUrl file.iri ++ "> .\n" ++ raw
            in
            case Graph.parseSafe seed rawWithBase of
                Err error ->
                    BackendTask.fail
                        (FatalError.build
                            { title = "Parsing error"
                            , body =
                                [ "I could not parse the file"
                                , ""
                                , "    " ++ file.path
                                , ""
                                , Graph.errorToString rawWithBase error
                                ]
                                    |> String.join "\n"
                            }
                        )

                Ok stuff ->
                    BackendTask.succeed stuff
    in
    if
        String.startsWith "http://" file.path
            || String.startsWith "https://" file.path
    then
        BackendTask.Http.get file.path BackendTask.Http.expectString
            |> BackendTask.allowFatal
            |> BackendTask.andThen parse

    else
        BackendTask.File.rawFile file.path
            |> BackendTask.allowFatal
            |> BackendTask.andThen parse


type alias CliOptions =
    { root : String
    , config : String
    , shaclDocument : Iri
    }


program : Program.Config CliOptions
program =
    Program.config
        |> Program.add
            (OptionsParser.build CliOptions
                |> OptionsParser.with
                    (Option.optionalKeywordArg "root"
                        |> Option.withDefault "."
                    )
                |> OptionsParser.with
                    (Option.optionalKeywordArg "config"
                        |> Option.withDefault "./herbie.json"
                    )
                |> OptionsParser.with
                    (Option.requiredPositionalArg "shacl-document"
                        |> Option.map Rdf.iri
                    )
            )


type alias File =
    { path : String
    , iri : Iri
    }


type alias Entity =
    { id : String
    , type_ : TypeEntity
    , hasPart : List String
    , identifier : Maybe String
    }


type TypeEntity
    = TypeDataset
    | TypeCreativeWork
    | TypeFile


decoderFiles : String -> Decoder (List File)
decoderFiles root =
    Decode.field "@graph" (Decode.list decoderEntity)
        |> Decode.map (List.filterMap (fileFromEntity root))


decoderEntity : Decoder Entity
decoderEntity =
    Decode.succeed Entity
        |> Decode.required "@id" Decode.string
        |> Decode.required "@type" decoderTypeEntity
        |> Decode.optional "hasPart" (Decode.list decoderHasPart) []
        |> Decode.optional "identifier" (Decode.map Just Decode.string) Nothing


decoderTypeEntity : Decoder TypeEntity
decoderTypeEntity =
    let
        fromString string =
            case string of
                "Dataset" ->
                    Decode.succeed TypeDataset

                "CreativeWork" ->
                    Decode.succeed TypeCreativeWork

                "File" ->
                    Decode.succeed TypeFile

                _ ->
                    Decode.fail ("unknown @type: " ++ string)
    in
    Decode.andThen fromString Decode.string


decoderHasPart : Decoder String
decoderHasPart =
    Decode.field "@id" Decode.string


fileFromEntity : String -> Entity -> Maybe File
fileFromEntity root entity =
    case entity.identifier of
        Nothing ->
            Nothing

        Just identifier ->
            if entity.type_ == TypeFile then
                let
                    idCleaned =
                        if entity.id == "./" then
                            ""

                        else
                            entity.id
                in
                Just
                    { path = root ++ "/" ++ idCleaned
                    , iri = Rdf.iri identifier
                    }

            else
                Nothing


decoderLookups : Decoder (List File)
decoderLookups =
    Decode.field "lookups" (Decode.keyValuePairs Decode.string)
        |> Decode.map
            (List.map
                (\( raw, location ) ->
                    { path = location
                    , iri = Rdf.iri raw
                    }
                )
            )
