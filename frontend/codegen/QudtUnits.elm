module QudtUnits exposing (main)

import Elm
import Elm.Annotation
import Elm.Arg
import Elm.Case
import Gen.CodeGen.Generate as Generate
import Json.Decode as Decode exposing (Decoder, Value)
import List.Extra as List


main : Program Value () ()
main =
    Generate.fromJson decoderQudtUnits (List.uniqueBy .url >> generate)


type alias Unit =
    { url : String
    , symbol : String
    , labelEn : Maybe String
    , labelDe : Maybe String
    }


decoderQudtUnits : Decoder (List Unit)
decoderQudtUnits =
    Decode.list decoderQudtUnit


decoderQudtUnit : Decoder Unit
decoderQudtUnit =
    Decode.map4 Unit
        (Decode.at [ "unit", "value" ] Decode.string)
        (Decode.at [ "symbol", "value" ] Decode.string)
        (Decode.oneOf
            [ Decode.string
                |> Decode.at [ "labelEn", "value" ]
                |> Decode.map Just
            , Decode.succeed Nothing
            ]
        )
        (Decode.oneOf
            [ Decode.string
                |> Decode.at [ "labelDe", "value" ]
                |> Decode.map Just
            , Decode.succeed Nothing
            ]
        )


generate : List Unit -> List Elm.File
generate units =
    [ Elm.file [ "Ontology", "Qudt", "Unit" ]
        [ Elm.declaration "urlToSymbol"
            (Elm.fn (Elm.Arg.var "url")
                (\url ->
                    Elm.Case.string url
                        { cases = List.map qudtUnitBranchUrlToSymbol units
                        , otherwise = Elm.nothing
                        }
                        |> Elm.withType (Elm.Annotation.maybe Elm.Annotation.string)
                )
            )
        ]
    ]


qudtUnitBranchUrlToSymbol : Unit -> ( String, Elm.Expression )
qudtUnitBranchUrlToSymbol unit =
    ( unit.url, Elm.just (Elm.string unit.symbol) )
