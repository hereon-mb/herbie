module Gen.Element.Lazy exposing
    ( moduleName_, lazy, lazy2, lazy3, lazy4, lazy5
    , call_, values_
    )

{-|
# Generated bindings for Element.Lazy

@docs moduleName_, lazy, lazy2, lazy3, lazy4, lazy5
@docs call_, values_
-}


import Elm
import Elm.Annotation as Type


{-| The name of this module. -}
moduleName_ : List String
moduleName_ =
    [ "Element", "Lazy" ]


{-| lazy: (a -> Internal.Model.Element msg) -> a -> Internal.Model.Element msg -}
lazy : (Elm.Expression -> Elm.Expression) -> Elm.Expression -> Elm.Expression
lazy lazyArg_ lazyArg_0 =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Lazy" ]
             , name = "lazy"
             , annotation =
                 Just
                     (Type.function
                          [ Type.function
                              [ Type.var "a" ]
                              (Type.namedWith
                                 [ "Internal", "Model" ]
                                 "Element"
                                 [ Type.var "msg" ]
                              )
                          , Type.var "a"
                          ]
                          (Type.namedWith
                               [ "Internal", "Model" ]
                               "Element"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ Elm.functionReduced "lazyUnpack" lazyArg_, lazyArg_0 ]


{-| lazy2: (a -> b -> Internal.Model.Element msg) -> a -> b -> Internal.Model.Element msg -}
lazy2 :
    (Elm.Expression -> Elm.Expression -> Elm.Expression)
    -> Elm.Expression
    -> Elm.Expression
    -> Elm.Expression
lazy2 lazy2Arg_ lazy2Arg_0 lazy2Arg_1 =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Lazy" ]
             , name = "lazy2"
             , annotation =
                 Just
                     (Type.function
                          [ Type.function
                              [ Type.var "a", Type.var "b" ]
                              (Type.namedWith
                                 [ "Internal", "Model" ]
                                 "Element"
                                 [ Type.var "msg" ]
                              )
                          , Type.var "a"
                          , Type.var "b"
                          ]
                          (Type.namedWith
                               [ "Internal", "Model" ]
                               "Element"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ Elm.functionReduced
            "lazy2Unpack"
            (\functionReducedUnpack ->
               Elm.functionReduced "unpack" (lazy2Arg_ functionReducedUnpack)
            )
        , lazy2Arg_0
        , lazy2Arg_1
        ]


{-| lazy3: 
    (a -> b -> c -> Internal.Model.Element msg)
    -> a
    -> b
    -> c
    -> Internal.Model.Element msg
-}
lazy3 :
    (Elm.Expression -> Elm.Expression -> Elm.Expression -> Elm.Expression)
    -> Elm.Expression
    -> Elm.Expression
    -> Elm.Expression
    -> Elm.Expression
lazy3 lazy3Arg_ lazy3Arg_0 lazy3Arg_1 lazy3Arg_2 =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Lazy" ]
             , name = "lazy3"
             , annotation =
                 Just
                     (Type.function
                          [ Type.function
                              [ Type.var "a", Type.var "b", Type.var "c" ]
                              (Type.namedWith
                                 [ "Internal", "Model" ]
                                 "Element"
                                 [ Type.var "msg" ]
                              )
                          , Type.var "a"
                          , Type.var "b"
                          , Type.var "c"
                          ]
                          (Type.namedWith
                               [ "Internal", "Model" ]
                               "Element"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ Elm.functionReduced
            "lazy3Unpack"
            (\functionReducedUnpack ->
               Elm.functionReduced
                   "unpack"
                   (\functionReducedUnpack0 ->
                        Elm.functionReduced
                            "unpack"
                            ((lazy3Arg_ functionReducedUnpack)
                                 functionReducedUnpack0
                            )
                   )
            )
        , lazy3Arg_0
        , lazy3Arg_1
        , lazy3Arg_2
        ]


{-| lazy4: 
    (a -> b -> c -> d -> Internal.Model.Element msg)
    -> a
    -> b
    -> c
    -> d
    -> Internal.Model.Element msg
-}
lazy4 :
    (Elm.Expression
    -> Elm.Expression
    -> Elm.Expression
    -> Elm.Expression
    -> Elm.Expression)
    -> Elm.Expression
    -> Elm.Expression
    -> Elm.Expression
    -> Elm.Expression
    -> Elm.Expression
lazy4 lazy4Arg_ lazy4Arg_0 lazy4Arg_1 lazy4Arg_2 lazy4Arg_3 =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Lazy" ]
             , name = "lazy4"
             , annotation =
                 Just
                     (Type.function
                          [ Type.function
                              [ Type.var "a"
                              , Type.var "b"
                              , Type.var "c"
                              , Type.var "d"
                              ]
                              (Type.namedWith
                                 [ "Internal", "Model" ]
                                 "Element"
                                 [ Type.var "msg" ]
                              )
                          , Type.var "a"
                          , Type.var "b"
                          , Type.var "c"
                          , Type.var "d"
                          ]
                          (Type.namedWith
                               [ "Internal", "Model" ]
                               "Element"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ Elm.functionReduced
            "lazy4Unpack"
            (\functionReducedUnpack ->
               Elm.functionReduced
                   "unpack"
                   (\functionReducedUnpack0 ->
                        Elm.functionReduced
                            "unpack"
                            (\functionReducedUnpack_2_1_2_0_2_0_2_0_0 ->
                                 Elm.functionReduced
                                     "unpack"
                                     (((lazy4Arg_ functionReducedUnpack)
                                           functionReducedUnpack0
                                      )
                                          functionReducedUnpack_2_1_2_0_2_0_2_0_0
                                     )
                            )
                   )
            )
        , lazy4Arg_0
        , lazy4Arg_1
        , lazy4Arg_2
        , lazy4Arg_3
        ]


{-| lazy5: 
    (a -> b -> c -> d -> e -> Internal.Model.Element msg)
    -> a
    -> b
    -> c
    -> d
    -> e
    -> Internal.Model.Element msg
-}
lazy5 :
    (Elm.Expression
    -> Elm.Expression
    -> Elm.Expression
    -> Elm.Expression
    -> Elm.Expression
    -> Elm.Expression)
    -> Elm.Expression
    -> Elm.Expression
    -> Elm.Expression
    -> Elm.Expression
    -> Elm.Expression
    -> Elm.Expression
lazy5 lazy5Arg_ lazy5Arg_0 lazy5Arg_1 lazy5Arg_2 lazy5Arg_3 lazy5Arg_4 =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Lazy" ]
             , name = "lazy5"
             , annotation =
                 Just
                     (Type.function
                          [ Type.function
                              [ Type.var "a"
                              , Type.var "b"
                              , Type.var "c"
                              , Type.var "d"
                              , Type.var "e"
                              ]
                              (Type.namedWith
                                 [ "Internal", "Model" ]
                                 "Element"
                                 [ Type.var "msg" ]
                              )
                          , Type.var "a"
                          , Type.var "b"
                          , Type.var "c"
                          , Type.var "d"
                          , Type.var "e"
                          ]
                          (Type.namedWith
                               [ "Internal", "Model" ]
                               "Element"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ Elm.functionReduced
            "lazy5Unpack"
            (\functionReducedUnpack ->
               Elm.functionReduced
                   "unpack"
                   (\functionReducedUnpack0 ->
                        Elm.functionReduced
                            "unpack"
                            (\functionReducedUnpack_2_1_2_0_2_0_2_0_0 ->
                                 Elm.functionReduced
                                     "unpack"
                                     (\functionReducedUnpack_2_1_2_1_2_0_2_0_2_0_0 ->
                                          Elm.functionReduced
                                              "unpack"
                                              ((((lazy5Arg_
                                                      functionReducedUnpack
                                                 )
                                                     functionReducedUnpack0
                                                )
                                                    functionReducedUnpack_2_1_2_0_2_0_2_0_0
                                               )
                                                   functionReducedUnpack_2_1_2_1_2_0_2_0_2_0_0
                                              )
                                     )
                            )
                   )
            )
        , lazy5Arg_0
        , lazy5Arg_1
        , lazy5Arg_2
        , lazy5Arg_3
        , lazy5Arg_4
        ]


call_ :
    { lazy : Elm.Expression -> Elm.Expression -> Elm.Expression
    , lazy2 :
        Elm.Expression -> Elm.Expression -> Elm.Expression -> Elm.Expression
    , lazy3 :
        Elm.Expression
        -> Elm.Expression
        -> Elm.Expression
        -> Elm.Expression
        -> Elm.Expression
    , lazy4 :
        Elm.Expression
        -> Elm.Expression
        -> Elm.Expression
        -> Elm.Expression
        -> Elm.Expression
        -> Elm.Expression
    , lazy5 :
        Elm.Expression
        -> Elm.Expression
        -> Elm.Expression
        -> Elm.Expression
        -> Elm.Expression
        -> Elm.Expression
        -> Elm.Expression
    }
call_ =
    { lazy =
        \lazyArg_ lazyArg_0 ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Lazy" ]
                     , name = "lazy"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.function
                                      [ Type.var "a" ]
                                      (Type.namedWith
                                         [ "Internal", "Model" ]
                                         "Element"
                                         [ Type.var "msg" ]
                                      )
                                  , Type.var "a"
                                  ]
                                  (Type.namedWith
                                       [ "Internal", "Model" ]
                                       "Element"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ lazyArg_, lazyArg_0 ]
    , lazy2 =
        \lazy2Arg_ lazy2Arg_0 lazy2Arg_1 ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Lazy" ]
                     , name = "lazy2"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.function
                                      [ Type.var "a", Type.var "b" ]
                                      (Type.namedWith
                                         [ "Internal", "Model" ]
                                         "Element"
                                         [ Type.var "msg" ]
                                      )
                                  , Type.var "a"
                                  , Type.var "b"
                                  ]
                                  (Type.namedWith
                                       [ "Internal", "Model" ]
                                       "Element"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ lazy2Arg_, lazy2Arg_0, lazy2Arg_1 ]
    , lazy3 =
        \lazy3Arg_ lazy3Arg_0 lazy3Arg_1 lazy3Arg_2 ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Lazy" ]
                     , name = "lazy3"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.function
                                      [ Type.var "a"
                                      , Type.var "b"
                                      , Type.var "c"
                                      ]
                                      (Type.namedWith
                                         [ "Internal", "Model" ]
                                         "Element"
                                         [ Type.var "msg" ]
                                      )
                                  , Type.var "a"
                                  , Type.var "b"
                                  , Type.var "c"
                                  ]
                                  (Type.namedWith
                                       [ "Internal", "Model" ]
                                       "Element"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ lazy3Arg_, lazy3Arg_0, lazy3Arg_1, lazy3Arg_2 ]
    , lazy4 =
        \lazy4Arg_ lazy4Arg_0 lazy4Arg_1 lazy4Arg_2 lazy4Arg_3 ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Lazy" ]
                     , name = "lazy4"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.function
                                      [ Type.var "a"
                                      , Type.var "b"
                                      , Type.var "c"
                                      , Type.var "d"
                                      ]
                                      (Type.namedWith
                                         [ "Internal", "Model" ]
                                         "Element"
                                         [ Type.var "msg" ]
                                      )
                                  , Type.var "a"
                                  , Type.var "b"
                                  , Type.var "c"
                                  , Type.var "d"
                                  ]
                                  (Type.namedWith
                                       [ "Internal", "Model" ]
                                       "Element"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ lazy4Arg_, lazy4Arg_0, lazy4Arg_1, lazy4Arg_2, lazy4Arg_3 ]
    , lazy5 =
        \lazy5Arg_ lazy5Arg_0 lazy5Arg_1 lazy5Arg_2 lazy5Arg_3 lazy5Arg_4 ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Lazy" ]
                     , name = "lazy5"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.function
                                      [ Type.var "a"
                                      , Type.var "b"
                                      , Type.var "c"
                                      , Type.var "d"
                                      , Type.var "e"
                                      ]
                                      (Type.namedWith
                                         [ "Internal", "Model" ]
                                         "Element"
                                         [ Type.var "msg" ]
                                      )
                                  , Type.var "a"
                                  , Type.var "b"
                                  , Type.var "c"
                                  , Type.var "d"
                                  , Type.var "e"
                                  ]
                                  (Type.namedWith
                                       [ "Internal", "Model" ]
                                       "Element"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ lazy5Arg_
                , lazy5Arg_0
                , lazy5Arg_1
                , lazy5Arg_2
                , lazy5Arg_3
                , lazy5Arg_4
                ]
    }


values_ :
    { lazy : Elm.Expression
    , lazy2 : Elm.Expression
    , lazy3 : Elm.Expression
    , lazy4 : Elm.Expression
    , lazy5 : Elm.Expression
    }
values_ =
    { lazy =
        Elm.value
            { importFrom = [ "Element", "Lazy" ]
            , name = "lazy"
            , annotation =
                Just
                    (Type.function
                         [ Type.function
                             [ Type.var "a" ]
                             (Type.namedWith
                                [ "Internal", "Model" ]
                                "Element"
                                [ Type.var "msg" ]
                             )
                         , Type.var "a"
                         ]
                         (Type.namedWith
                              [ "Internal", "Model" ]
                              "Element"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , lazy2 =
        Elm.value
            { importFrom = [ "Element", "Lazy" ]
            , name = "lazy2"
            , annotation =
                Just
                    (Type.function
                         [ Type.function
                             [ Type.var "a", Type.var "b" ]
                             (Type.namedWith
                                [ "Internal", "Model" ]
                                "Element"
                                [ Type.var "msg" ]
                             )
                         , Type.var "a"
                         , Type.var "b"
                         ]
                         (Type.namedWith
                              [ "Internal", "Model" ]
                              "Element"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , lazy3 =
        Elm.value
            { importFrom = [ "Element", "Lazy" ]
            , name = "lazy3"
            , annotation =
                Just
                    (Type.function
                         [ Type.function
                             [ Type.var "a", Type.var "b", Type.var "c" ]
                             (Type.namedWith
                                [ "Internal", "Model" ]
                                "Element"
                                [ Type.var "msg" ]
                             )
                         , Type.var "a"
                         , Type.var "b"
                         , Type.var "c"
                         ]
                         (Type.namedWith
                              [ "Internal", "Model" ]
                              "Element"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , lazy4 =
        Elm.value
            { importFrom = [ "Element", "Lazy" ]
            , name = "lazy4"
            , annotation =
                Just
                    (Type.function
                         [ Type.function
                             [ Type.var "a"
                             , Type.var "b"
                             , Type.var "c"
                             , Type.var "d"
                             ]
                             (Type.namedWith
                                [ "Internal", "Model" ]
                                "Element"
                                [ Type.var "msg" ]
                             )
                         , Type.var "a"
                         , Type.var "b"
                         , Type.var "c"
                         , Type.var "d"
                         ]
                         (Type.namedWith
                              [ "Internal", "Model" ]
                              "Element"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , lazy5 =
        Elm.value
            { importFrom = [ "Element", "Lazy" ]
            , name = "lazy5"
            , annotation =
                Just
                    (Type.function
                         [ Type.function
                             [ Type.var "a"
                             , Type.var "b"
                             , Type.var "c"
                             , Type.var "d"
                             , Type.var "e"
                             ]
                             (Type.namedWith
                                [ "Internal", "Model" ]
                                "Element"
                                [ Type.var "msg" ]
                             )
                         , Type.var "a"
                         , Type.var "b"
                         , Type.var "c"
                         , Type.var "d"
                         , Type.var "e"
                         ]
                         (Type.namedWith
                              [ "Internal", "Model" ]
                              "Element"
                              [ Type.var "msg" ]
                         )
                    )
            }
    }