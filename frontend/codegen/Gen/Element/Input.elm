module Gen.Element.Input exposing
    ( moduleName_, focusedOnLoad, button, checkbox, defaultCheckbox, text
    , multiline, placeholder, username, newPassword, currentPassword, email, search
    , spellChecked, slider, thumb, defaultThumb, radio, radioRow, option
    , optionWith, labelAbove, labelBelow, labelLeft, labelRight, labelHidden, annotation_
    , make_, caseOf_, call_, values_
    )

{-|
# Generated bindings for Element.Input

@docs moduleName_, focusedOnLoad, button, checkbox, defaultCheckbox, text
@docs multiline, placeholder, username, newPassword, currentPassword, email
@docs search, spellChecked, slider, thumb, defaultThumb, radio
@docs radioRow, option, optionWith, labelAbove, labelBelow, labelLeft
@docs labelRight, labelHidden, annotation_, make_, caseOf_, call_
@docs values_
-}


import Elm
import Elm.Annotation as Type
import Elm.Arg
import Elm.Case


{-| The name of this module. -}
moduleName_ : List String
moduleName_ =
    [ "Element", "Input" ]


{-| Attach this attribute to any `Input` that you would like to be automatically focused when the page loads.

You should only have a maximum of one per page.

focusedOnLoad: Element.Attribute msg
-}
focusedOnLoad : Elm.Expression
focusedOnLoad =
    Elm.value
        { importFrom = [ "Element", "Input" ]
        , name = "focusedOnLoad"
        , annotation =
            Just (Type.namedWith [ "Element" ] "Attribute" [ Type.var "msg" ])
        }


{-| A standard button.

The `onPress` handler will be fired either `onClick` or when the element is focused and the `Enter` key has been pressed.

    import Element exposing (rgb255, text)
    import Element.Background as Background
    import Element.Input as Input

    blue =
        Element.rgb255 238 238 238

    myButton =
        Input.button
            [ Background.color blue
            , Element.focused
                [ Background.color purple ]
            ]
            { onPress = Just ClickMsg
            , label = text "My Button"
            }

**Note** If you have an icon button but want it to be accessible, consider adding a [`Region.description`](Element-Region#description), which will describe the button to screen readers.

button: 
    List (Element.Attribute msg)
    -> { onPress : Maybe msg, label : Element.Element msg }
    -> Element.Element msg
-}
button :
    List Elm.Expression
    -> { onPress : Elm.Expression, label : Elm.Expression }
    -> Elm.Expression
button buttonArg_ buttonArg_0 =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Input" ]
             , name = "button"
             , annotation =
                 Just
                     (Type.function
                          [ Type.list
                              (Type.namedWith
                                 [ "Element" ]
                                 "Attribute"
                                 [ Type.var "msg" ]
                              )
                          , Type.record
                              [ ( "onPress", Type.maybe (Type.var "msg") )
                              , ( "label"
                                , Type.namedWith
                                    [ "Element" ]
                                    "Element"
                                    [ Type.var "msg" ]
                                )
                              ]
                          ]
                          (Type.namedWith
                               [ "Element" ]
                               "Element"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ Elm.list buttonArg_
        , Elm.record
            [ Tuple.pair "onPress" buttonArg_0.onPress
            , Tuple.pair "label" buttonArg_0.label
            ]
        ]


{-| - **onChange** - The `Msg` to send.
  - **icon** - The checkbox icon to show. This can be whatever you'd like, but `Input.defaultCheckbox` is included to get you started.
  - **checked** - The current checked state.
  - **label** - The [`Label`](#Label) for this checkbox

checkbox: 
    List (Element.Attribute msg)
    -> { onChange : Bool -> msg
    , icon : Bool -> Element.Element msg
    , checked : Bool
    , label : Element.Input.Label msg
    }
    -> Element.Element msg
-}
checkbox :
    List Elm.Expression
    -> { onChange : Elm.Expression -> Elm.Expression
    , icon : Elm.Expression -> Elm.Expression
    , checked : Bool
    , label : Elm.Expression
    }
    -> Elm.Expression
checkbox checkboxArg_ checkboxArg_0 =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Input" ]
             , name = "checkbox"
             , annotation =
                 Just
                     (Type.function
                          [ Type.list
                              (Type.namedWith
                                 [ "Element" ]
                                 "Attribute"
                                 [ Type.var "msg" ]
                              )
                          , Type.record
                              [ ( "onChange"
                                , Type.function [ Type.bool ] (Type.var "msg")
                                )
                              , ( "icon"
                                , Type.function
                                    [ Type.bool ]
                                    (Type.namedWith
                                       [ "Element" ]
                                       "Element"
                                       [ Type.var "msg" ]
                                    )
                                )
                              , ( "checked", Type.bool )
                              , ( "label"
                                , Type.namedWith
                                    [ "Element", "Input" ]
                                    "Label"
                                    [ Type.var "msg" ]
                                )
                              ]
                          ]
                          (Type.namedWith
                               [ "Element" ]
                               "Element"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ Elm.list checkboxArg_
        , Elm.record
            [ Tuple.pair
                  "onChange"
                  (Elm.functionReduced "checkboxUnpack" checkboxArg_0.onChange)
            , Tuple.pair
                  "icon"
                  (Elm.functionReduced "checkboxUnpack" checkboxArg_0.icon)
            , Tuple.pair "checked" (Elm.bool checkboxArg_0.checked)
            , Tuple.pair "label" checkboxArg_0.label
            ]
        ]


{-| The blue default checked box icon.

You'll likely want to make your own checkbox at some point that fits your design.

defaultCheckbox: Bool -> Element.Element msg
-}
defaultCheckbox : Bool -> Elm.Expression
defaultCheckbox defaultCheckboxArg_ =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Input" ]
             , name = "defaultCheckbox"
             , annotation =
                 Just
                     (Type.function
                          [ Type.bool ]
                          (Type.namedWith
                               [ "Element" ]
                               "Element"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ Elm.bool defaultCheckboxArg_ ]


{-| text: 
    List (Element.Attribute msg)
    -> { onChange : String -> msg
    , text : String
    , placeholder : Maybe (Element.Input.Placeholder msg)
    , label : Element.Input.Label msg
    }
    -> Element.Element msg
-}
text :
    List Elm.Expression
    -> { onChange : Elm.Expression -> Elm.Expression
    , text : String
    , placeholder : Elm.Expression
    , label : Elm.Expression
    }
    -> Elm.Expression
text textArg_ textArg_0 =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Input" ]
             , name = "text"
             , annotation =
                 Just
                     (Type.function
                          [ Type.list
                              (Type.namedWith
                                 [ "Element" ]
                                 "Attribute"
                                 [ Type.var "msg" ]
                              )
                          , Type.record
                              [ ( "onChange"
                                , Type.function [ Type.string ] (Type.var "msg")
                                )
                              , ( "text", Type.string )
                              , ( "placeholder"
                                , Type.maybe
                                    (Type.namedWith
                                       [ "Element", "Input" ]
                                       "Placeholder"
                                       [ Type.var "msg" ]
                                    )
                                )
                              , ( "label"
                                , Type.namedWith
                                    [ "Element", "Input" ]
                                    "Label"
                                    [ Type.var "msg" ]
                                )
                              ]
                          ]
                          (Type.namedWith
                               [ "Element" ]
                               "Element"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ Elm.list textArg_
        , Elm.record
            [ Tuple.pair
                  "onChange"
                  (Elm.functionReduced "textUnpack" textArg_0.onChange)
            , Tuple.pair "text" (Elm.string textArg_0.text)
            , Tuple.pair "placeholder" textArg_0.placeholder
            , Tuple.pair "label" textArg_0.label
            ]
        ]


{-| A multiline text input.

By default it will have a minimum height of one line and resize based on it's contents.

multiline: 
    List (Element.Attribute msg)
    -> { onChange : String -> msg
    , text : String
    , placeholder : Maybe (Element.Input.Placeholder msg)
    , label : Element.Input.Label msg
    , spellcheck : Bool
    }
    -> Element.Element msg
-}
multiline :
    List Elm.Expression
    -> { onChange : Elm.Expression -> Elm.Expression
    , text : String
    , placeholder : Elm.Expression
    , label : Elm.Expression
    , spellcheck : Bool
    }
    -> Elm.Expression
multiline multilineArg_ multilineArg_0 =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Input" ]
             , name = "multiline"
             , annotation =
                 Just
                     (Type.function
                          [ Type.list
                              (Type.namedWith
                                 [ "Element" ]
                                 "Attribute"
                                 [ Type.var "msg" ]
                              )
                          , Type.record
                              [ ( "onChange"
                                , Type.function [ Type.string ] (Type.var "msg")
                                )
                              , ( "text", Type.string )
                              , ( "placeholder"
                                , Type.maybe
                                    (Type.namedWith
                                       [ "Element", "Input" ]
                                       "Placeholder"
                                       [ Type.var "msg" ]
                                    )
                                )
                              , ( "label"
                                , Type.namedWith
                                    [ "Element", "Input" ]
                                    "Label"
                                    [ Type.var "msg" ]
                                )
                              , ( "spellcheck", Type.bool )
                              ]
                          ]
                          (Type.namedWith
                               [ "Element" ]
                               "Element"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ Elm.list multilineArg_
        , Elm.record
            [ Tuple.pair
                  "onChange"
                  (Elm.functionReduced "multilineUnpack" multilineArg_0.onChange
                  )
            , Tuple.pair "text" (Elm.string multilineArg_0.text)
            , Tuple.pair "placeholder" multilineArg_0.placeholder
            , Tuple.pair "label" multilineArg_0.label
            , Tuple.pair "spellcheck" (Elm.bool multilineArg_0.spellcheck)
            ]
        ]


{-| placeholder: 
    List (Element.Attribute msg)
    -> Element.Element msg
    -> Element.Input.Placeholder msg
-}
placeholder : List Elm.Expression -> Elm.Expression -> Elm.Expression
placeholder placeholderArg_ placeholderArg_0 =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Input" ]
             , name = "placeholder"
             , annotation =
                 Just
                     (Type.function
                          [ Type.list
                              (Type.namedWith
                                 [ "Element" ]
                                 "Attribute"
                                 [ Type.var "msg" ]
                              )
                          , Type.namedWith
                              [ "Element" ]
                              "Element"
                              [ Type.var "msg" ]
                          ]
                          (Type.namedWith
                               [ "Element", "Input" ]
                               "Placeholder"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ Elm.list placeholderArg_, placeholderArg_0 ]


{-| username: 
    List (Element.Attribute msg)
    -> { onChange : String -> msg
    , text : String
    , placeholder : Maybe (Element.Input.Placeholder msg)
    , label : Element.Input.Label msg
    }
    -> Element.Element msg
-}
username :
    List Elm.Expression
    -> { onChange : Elm.Expression -> Elm.Expression
    , text : String
    , placeholder : Elm.Expression
    , label : Elm.Expression
    }
    -> Elm.Expression
username usernameArg_ usernameArg_0 =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Input" ]
             , name = "username"
             , annotation =
                 Just
                     (Type.function
                          [ Type.list
                              (Type.namedWith
                                 [ "Element" ]
                                 "Attribute"
                                 [ Type.var "msg" ]
                              )
                          , Type.record
                              [ ( "onChange"
                                , Type.function [ Type.string ] (Type.var "msg")
                                )
                              , ( "text", Type.string )
                              , ( "placeholder"
                                , Type.maybe
                                    (Type.namedWith
                                       [ "Element", "Input" ]
                                       "Placeholder"
                                       [ Type.var "msg" ]
                                    )
                                )
                              , ( "label"
                                , Type.namedWith
                                    [ "Element", "Input" ]
                                    "Label"
                                    [ Type.var "msg" ]
                                )
                              ]
                          ]
                          (Type.namedWith
                               [ "Element" ]
                               "Element"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ Elm.list usernameArg_
        , Elm.record
            [ Tuple.pair
                  "onChange"
                  (Elm.functionReduced "usernameUnpack" usernameArg_0.onChange)
            , Tuple.pair "text" (Elm.string usernameArg_0.text)
            , Tuple.pair "placeholder" usernameArg_0.placeholder
            , Tuple.pair "label" usernameArg_0.label
            ]
        ]


{-| A password input that allows the browser to autofill.

It's `newPassword` instead of just `password` because it gives the browser a hint on what type of password input it is.

A password takes all the arguments a normal `Input.text` would, and also **show**, which will remove the password mask (e.g. `****` vs `pass1234`)

newPassword: 
    List (Element.Attribute msg)
    -> { onChange : String -> msg
    , text : String
    , placeholder : Maybe (Element.Input.Placeholder msg)
    , label : Element.Input.Label msg
    , show : Bool
    }
    -> Element.Element msg
-}
newPassword :
    List Elm.Expression
    -> { onChange : Elm.Expression -> Elm.Expression
    , text : String
    , placeholder : Elm.Expression
    , label : Elm.Expression
    , show : Bool
    }
    -> Elm.Expression
newPassword newPasswordArg_ newPasswordArg_0 =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Input" ]
             , name = "newPassword"
             , annotation =
                 Just
                     (Type.function
                          [ Type.list
                              (Type.namedWith
                                 [ "Element" ]
                                 "Attribute"
                                 [ Type.var "msg" ]
                              )
                          , Type.record
                              [ ( "onChange"
                                , Type.function [ Type.string ] (Type.var "msg")
                                )
                              , ( "text", Type.string )
                              , ( "placeholder"
                                , Type.maybe
                                    (Type.namedWith
                                       [ "Element", "Input" ]
                                       "Placeholder"
                                       [ Type.var "msg" ]
                                    )
                                )
                              , ( "label"
                                , Type.namedWith
                                    [ "Element", "Input" ]
                                    "Label"
                                    [ Type.var "msg" ]
                                )
                              , ( "show", Type.bool )
                              ]
                          ]
                          (Type.namedWith
                               [ "Element" ]
                               "Element"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ Elm.list newPasswordArg_
        , Elm.record
            [ Tuple.pair
                  "onChange"
                  (Elm.functionReduced
                       "newPasswordUnpack"
                       newPasswordArg_0.onChange
                  )
            , Tuple.pair "text" (Elm.string newPasswordArg_0.text)
            , Tuple.pair "placeholder" newPasswordArg_0.placeholder
            , Tuple.pair "label" newPasswordArg_0.label
            , Tuple.pair "show" (Elm.bool newPasswordArg_0.show)
            ]
        ]


{-| currentPassword: 
    List (Element.Attribute msg)
    -> { onChange : String -> msg
    , text : String
    , placeholder : Maybe (Element.Input.Placeholder msg)
    , label : Element.Input.Label msg
    , show : Bool
    }
    -> Element.Element msg
-}
currentPassword :
    List Elm.Expression
    -> { onChange : Elm.Expression -> Elm.Expression
    , text : String
    , placeholder : Elm.Expression
    , label : Elm.Expression
    , show : Bool
    }
    -> Elm.Expression
currentPassword currentPasswordArg_ currentPasswordArg_0 =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Input" ]
             , name = "currentPassword"
             , annotation =
                 Just
                     (Type.function
                          [ Type.list
                              (Type.namedWith
                                 [ "Element" ]
                                 "Attribute"
                                 [ Type.var "msg" ]
                              )
                          , Type.record
                              [ ( "onChange"
                                , Type.function [ Type.string ] (Type.var "msg")
                                )
                              , ( "text", Type.string )
                              , ( "placeholder"
                                , Type.maybe
                                    (Type.namedWith
                                       [ "Element", "Input" ]
                                       "Placeholder"
                                       [ Type.var "msg" ]
                                    )
                                )
                              , ( "label"
                                , Type.namedWith
                                    [ "Element", "Input" ]
                                    "Label"
                                    [ Type.var "msg" ]
                                )
                              , ( "show", Type.bool )
                              ]
                          ]
                          (Type.namedWith
                               [ "Element" ]
                               "Element"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ Elm.list currentPasswordArg_
        , Elm.record
            [ Tuple.pair
                  "onChange"
                  (Elm.functionReduced
                       "currentPasswordUnpack"
                       currentPasswordArg_0.onChange
                  )
            , Tuple.pair "text" (Elm.string currentPasswordArg_0.text)
            , Tuple.pair "placeholder" currentPasswordArg_0.placeholder
            , Tuple.pair "label" currentPasswordArg_0.label
            , Tuple.pair "show" (Elm.bool currentPasswordArg_0.show)
            ]
        ]


{-| email: 
    List (Element.Attribute msg)
    -> { onChange : String -> msg
    , text : String
    , placeholder : Maybe (Element.Input.Placeholder msg)
    , label : Element.Input.Label msg
    }
    -> Element.Element msg
-}
email :
    List Elm.Expression
    -> { onChange : Elm.Expression -> Elm.Expression
    , text : String
    , placeholder : Elm.Expression
    , label : Elm.Expression
    }
    -> Elm.Expression
email emailArg_ emailArg_0 =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Input" ]
             , name = "email"
             , annotation =
                 Just
                     (Type.function
                          [ Type.list
                              (Type.namedWith
                                 [ "Element" ]
                                 "Attribute"
                                 [ Type.var "msg" ]
                              )
                          , Type.record
                              [ ( "onChange"
                                , Type.function [ Type.string ] (Type.var "msg")
                                )
                              , ( "text", Type.string )
                              , ( "placeholder"
                                , Type.maybe
                                    (Type.namedWith
                                       [ "Element", "Input" ]
                                       "Placeholder"
                                       [ Type.var "msg" ]
                                    )
                                )
                              , ( "label"
                                , Type.namedWith
                                    [ "Element", "Input" ]
                                    "Label"
                                    [ Type.var "msg" ]
                                )
                              ]
                          ]
                          (Type.namedWith
                               [ "Element" ]
                               "Element"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ Elm.list emailArg_
        , Elm.record
            [ Tuple.pair
                  "onChange"
                  (Elm.functionReduced "emailUnpack" emailArg_0.onChange)
            , Tuple.pair "text" (Elm.string emailArg_0.text)
            , Tuple.pair "placeholder" emailArg_0.placeholder
            , Tuple.pair "label" emailArg_0.label
            ]
        ]


{-| search: 
    List (Element.Attribute msg)
    -> { onChange : String -> msg
    , text : String
    , placeholder : Maybe (Element.Input.Placeholder msg)
    , label : Element.Input.Label msg
    }
    -> Element.Element msg
-}
search :
    List Elm.Expression
    -> { onChange : Elm.Expression -> Elm.Expression
    , text : String
    , placeholder : Elm.Expression
    , label : Elm.Expression
    }
    -> Elm.Expression
search searchArg_ searchArg_0 =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Input" ]
             , name = "search"
             , annotation =
                 Just
                     (Type.function
                          [ Type.list
                              (Type.namedWith
                                 [ "Element" ]
                                 "Attribute"
                                 [ Type.var "msg" ]
                              )
                          , Type.record
                              [ ( "onChange"
                                , Type.function [ Type.string ] (Type.var "msg")
                                )
                              , ( "text", Type.string )
                              , ( "placeholder"
                                , Type.maybe
                                    (Type.namedWith
                                       [ "Element", "Input" ]
                                       "Placeholder"
                                       [ Type.var "msg" ]
                                    )
                                )
                              , ( "label"
                                , Type.namedWith
                                    [ "Element", "Input" ]
                                    "Label"
                                    [ Type.var "msg" ]
                                )
                              ]
                          ]
                          (Type.namedWith
                               [ "Element" ]
                               "Element"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ Elm.list searchArg_
        , Elm.record
            [ Tuple.pair
                  "onChange"
                  (Elm.functionReduced "searchUnpack" searchArg_0.onChange)
            , Tuple.pair "text" (Elm.string searchArg_0.text)
            , Tuple.pair "placeholder" searchArg_0.placeholder
            , Tuple.pair "label" searchArg_0.label
            ]
        ]


{-| If spell checking is available, this input will be spellchecked.

spellChecked: 
    List (Element.Attribute msg)
    -> { onChange : String -> msg
    , text : String
    , placeholder : Maybe (Element.Input.Placeholder msg)
    , label : Element.Input.Label msg
    }
    -> Element.Element msg
-}
spellChecked :
    List Elm.Expression
    -> { onChange : Elm.Expression -> Elm.Expression
    , text : String
    , placeholder : Elm.Expression
    , label : Elm.Expression
    }
    -> Elm.Expression
spellChecked spellCheckedArg_ spellCheckedArg_0 =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Input" ]
             , name = "spellChecked"
             , annotation =
                 Just
                     (Type.function
                          [ Type.list
                              (Type.namedWith
                                 [ "Element" ]
                                 "Attribute"
                                 [ Type.var "msg" ]
                              )
                          , Type.record
                              [ ( "onChange"
                                , Type.function [ Type.string ] (Type.var "msg")
                                )
                              , ( "text", Type.string )
                              , ( "placeholder"
                                , Type.maybe
                                    (Type.namedWith
                                       [ "Element", "Input" ]
                                       "Placeholder"
                                       [ Type.var "msg" ]
                                    )
                                )
                              , ( "label"
                                , Type.namedWith
                                    [ "Element", "Input" ]
                                    "Label"
                                    [ Type.var "msg" ]
                                )
                              ]
                          ]
                          (Type.namedWith
                               [ "Element" ]
                               "Element"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ Elm.list spellCheckedArg_
        , Elm.record
            [ Tuple.pair
                  "onChange"
                  (Elm.functionReduced
                       "spellCheckedUnpack"
                       spellCheckedArg_0.onChange
                  )
            , Tuple.pair "text" (Elm.string spellCheckedArg_0.text)
            , Tuple.pair "placeholder" spellCheckedArg_0.placeholder
            , Tuple.pair "label" spellCheckedArg_0.label
            ]
        ]


{-| A slider input, good for capturing float values.

    Input.slider
        [ Element.height (Element.px 30)

        -- Here is where we're creating/styling the "track"
        , Element.behindContent
            (Element.el
                [ Element.width Element.fill
                , Element.height (Element.px 2)
                , Element.centerY
                , Background.color grey
                , Border.rounded 2
                ]
                Element.none
            )
        ]
        { onChange = AdjustValue
        , label =
            Input.labelAbove []
                (text "My Slider Value")
        , min = 0
        , max = 75
        , step = Nothing
        , value = model.sliderValue
        , thumb =
            Input.defaultThumb
        }

`Element.behindContent` is used to render the track of the slider. Without it, no track would be rendered. The `thumb` is the icon that you can move around.

The slider can be vertical or horizontal depending on the width/height of the slider.

  - `height fill` and `width (px someWidth)` will cause the slider to be vertical.
  - `height (px someHeight)` and `width (px someWidth)` where `someHeight` > `someWidth` will also do it.
  - otherwise, the slider will be horizontal.

**Note** If you want a slider for an `Int` value:

  - set `step` to be `Just 1`, or some other whole value
  - `value = toFloat model.myInt`
  - And finally, round the value before making a message `onChange = round >> AdjustValue`

slider: 
    List (Element.Attribute msg)
    -> { onChange : Float -> msg
    , label : Element.Input.Label msg
    , min : Float
    , max : Float
    , value : Float
    , thumb : Element.Input.Thumb
    , step : Maybe Float
    }
    -> Element.Element msg
-}
slider :
    List Elm.Expression
    -> { onChange : Elm.Expression -> Elm.Expression
    , label : Elm.Expression
    , min : Float
    , max : Float
    , value : Float
    , thumb : Elm.Expression
    , step : Elm.Expression
    }
    -> Elm.Expression
slider sliderArg_ sliderArg_0 =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Input" ]
             , name = "slider"
             , annotation =
                 Just
                     (Type.function
                          [ Type.list
                              (Type.namedWith
                                 [ "Element" ]
                                 "Attribute"
                                 [ Type.var "msg" ]
                              )
                          , Type.record
                              [ ( "onChange"
                                , Type.function [ Type.float ] (Type.var "msg")
                                )
                              , ( "label"
                                , Type.namedWith
                                    [ "Element", "Input" ]
                                    "Label"
                                    [ Type.var "msg" ]
                                )
                              , ( "min", Type.float )
                              , ( "max", Type.float )
                              , ( "value", Type.float )
                              , ( "thumb"
                                , Type.namedWith
                                    [ "Element", "Input" ]
                                    "Thumb"
                                    []
                                )
                              , ( "step", Type.maybe Type.float )
                              ]
                          ]
                          (Type.namedWith
                               [ "Element" ]
                               "Element"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ Elm.list sliderArg_
        , Elm.record
            [ Tuple.pair
                  "onChange"
                  (Elm.functionReduced "sliderUnpack" sliderArg_0.onChange)
            , Tuple.pair "label" sliderArg_0.label
            , Tuple.pair "min" (Elm.float sliderArg_0.min)
            , Tuple.pair "max" (Elm.float sliderArg_0.max)
            , Tuple.pair "value" (Elm.float sliderArg_0.value)
            , Tuple.pair "thumb" sliderArg_0.thumb
            , Tuple.pair "step" sliderArg_0.step
            ]
        ]


{-| thumb: List (Element.Attribute Basics.Never) -> Element.Input.Thumb -}
thumb : List Elm.Expression -> Elm.Expression
thumb thumbArg_ =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Input" ]
             , name = "thumb"
             , annotation =
                 Just
                     (Type.function
                          [ Type.list
                              (Type.namedWith
                                 [ "Element" ]
                                 "Attribute"
                                 [ Type.namedWith [ "Basics" ] "Never" [] ]
                              )
                          ]
                          (Type.namedWith [ "Element", "Input" ] "Thumb" [])
                     )
             }
        )
        [ Elm.list thumbArg_ ]


{-| defaultThumb: Element.Input.Thumb -}
defaultThumb : Elm.Expression
defaultThumb =
    Elm.value
        { importFrom = [ "Element", "Input" ]
        , name = "defaultThumb"
        , annotation = Just (Type.namedWith [ "Element", "Input" ] "Thumb" [])
        }


{-| radio: 
    List (Element.Attribute msg)
    -> { onChange : option -> msg
    , options : List (Element.Input.Option option msg)
    , selected : Maybe option
    , label : Element.Input.Label msg
    }
    -> Element.Element msg
-}
radio :
    List Elm.Expression
    -> { onChange : Elm.Expression -> Elm.Expression
    , options : List Elm.Expression
    , selected : Elm.Expression
    , label : Elm.Expression
    }
    -> Elm.Expression
radio radioArg_ radioArg_0 =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Input" ]
             , name = "radio"
             , annotation =
                 Just
                     (Type.function
                          [ Type.list
                              (Type.namedWith
                                 [ "Element" ]
                                 "Attribute"
                                 [ Type.var "msg" ]
                              )
                          , Type.record
                              [ ( "onChange"
                                , Type.function
                                    [ Type.var "option" ]
                                    (Type.var "msg")
                                )
                              , ( "options"
                                , Type.list
                                    (Type.namedWith
                                       [ "Element", "Input" ]
                                       "Option"
                                       [ Type.var "option", Type.var "msg" ]
                                    )
                                )
                              , ( "selected", Type.maybe (Type.var "option") )
                              , ( "label"
                                , Type.namedWith
                                    [ "Element", "Input" ]
                                    "Label"
                                    [ Type.var "msg" ]
                                )
                              ]
                          ]
                          (Type.namedWith
                               [ "Element" ]
                               "Element"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ Elm.list radioArg_
        , Elm.record
            [ Tuple.pair
                  "onChange"
                  (Elm.functionReduced "radioUnpack" radioArg_0.onChange)
            , Tuple.pair "options" (Elm.list radioArg_0.options)
            , Tuple.pair "selected" radioArg_0.selected
            , Tuple.pair "label" radioArg_0.label
            ]
        ]


{-| Same as radio, but displayed as a row

radioRow: 
    List (Element.Attribute msg)
    -> { onChange : option -> msg
    , options : List (Element.Input.Option option msg)
    , selected : Maybe option
    , label : Element.Input.Label msg
    }
    -> Element.Element msg
-}
radioRow :
    List Elm.Expression
    -> { onChange : Elm.Expression -> Elm.Expression
    , options : List Elm.Expression
    , selected : Elm.Expression
    , label : Elm.Expression
    }
    -> Elm.Expression
radioRow radioRowArg_ radioRowArg_0 =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Input" ]
             , name = "radioRow"
             , annotation =
                 Just
                     (Type.function
                          [ Type.list
                              (Type.namedWith
                                 [ "Element" ]
                                 "Attribute"
                                 [ Type.var "msg" ]
                              )
                          , Type.record
                              [ ( "onChange"
                                , Type.function
                                    [ Type.var "option" ]
                                    (Type.var "msg")
                                )
                              , ( "options"
                                , Type.list
                                    (Type.namedWith
                                       [ "Element", "Input" ]
                                       "Option"
                                       [ Type.var "option", Type.var "msg" ]
                                    )
                                )
                              , ( "selected", Type.maybe (Type.var "option") )
                              , ( "label"
                                , Type.namedWith
                                    [ "Element", "Input" ]
                                    "Label"
                                    [ Type.var "msg" ]
                                )
                              ]
                          ]
                          (Type.namedWith
                               [ "Element" ]
                               "Element"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ Elm.list radioRowArg_
        , Elm.record
            [ Tuple.pair
                  "onChange"
                  (Elm.functionReduced "radioRowUnpack" radioRowArg_0.onChange)
            , Tuple.pair "options" (Elm.list radioRowArg_0.options)
            , Tuple.pair "selected" radioRowArg_0.selected
            , Tuple.pair "label" radioRowArg_0.label
            ]
        ]


{-| Add a choice to your radio element. This will be rendered with the default radio icon.

option: value -> Element.Element msg -> Element.Input.Option value msg
-}
option : Elm.Expression -> Elm.Expression -> Elm.Expression
option optionArg_ optionArg_0 =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Input" ]
             , name = "option"
             , annotation =
                 Just
                     (Type.function
                          [ Type.var "value"
                          , Type.namedWith
                              [ "Element" ]
                              "Element"
                              [ Type.var "msg" ]
                          ]
                          (Type.namedWith
                               [ "Element", "Input" ]
                               "Option"
                               [ Type.var "value", Type.var "msg" ]
                          )
                     )
             }
        )
        [ optionArg_, optionArg_0 ]


{-| Customize exactly what your radio option should look like in different states.

optionWith: 
    value
    -> (Element.Input.OptionState -> Element.Element msg)
    -> Element.Input.Option value msg
-}
optionWith :
    Elm.Expression -> (Elm.Expression -> Elm.Expression) -> Elm.Expression
optionWith optionWithArg_ optionWithArg_0 =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Input" ]
             , name = "optionWith"
             , annotation =
                 Just
                     (Type.function
                          [ Type.var "value"
                          , Type.function
                              [ Type.namedWith
                                    [ "Element", "Input" ]
                                    "OptionState"
                                    []
                              ]
                              (Type.namedWith
                                 [ "Element" ]
                                 "Element"
                                 [ Type.var "msg" ]
                              )
                          ]
                          (Type.namedWith
                               [ "Element", "Input" ]
                               "Option"
                               [ Type.var "value", Type.var "msg" ]
                          )
                     )
             }
        )
        [ optionWithArg_
        , Elm.functionReduced "optionWithUnpack" optionWithArg_0
        ]


{-| labelAbove: List (Element.Attribute msg) -> Element.Element msg -> Element.Input.Label msg -}
labelAbove : List Elm.Expression -> Elm.Expression -> Elm.Expression
labelAbove labelAboveArg_ labelAboveArg_0 =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Input" ]
             , name = "labelAbove"
             , annotation =
                 Just
                     (Type.function
                          [ Type.list
                              (Type.namedWith
                                 [ "Element" ]
                                 "Attribute"
                                 [ Type.var "msg" ]
                              )
                          , Type.namedWith
                              [ "Element" ]
                              "Element"
                              [ Type.var "msg" ]
                          ]
                          (Type.namedWith
                               [ "Element", "Input" ]
                               "Label"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ Elm.list labelAboveArg_, labelAboveArg_0 ]


{-| labelBelow: List (Element.Attribute msg) -> Element.Element msg -> Element.Input.Label msg -}
labelBelow : List Elm.Expression -> Elm.Expression -> Elm.Expression
labelBelow labelBelowArg_ labelBelowArg_0 =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Input" ]
             , name = "labelBelow"
             , annotation =
                 Just
                     (Type.function
                          [ Type.list
                              (Type.namedWith
                                 [ "Element" ]
                                 "Attribute"
                                 [ Type.var "msg" ]
                              )
                          , Type.namedWith
                              [ "Element" ]
                              "Element"
                              [ Type.var "msg" ]
                          ]
                          (Type.namedWith
                               [ "Element", "Input" ]
                               "Label"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ Elm.list labelBelowArg_, labelBelowArg_0 ]


{-| labelLeft: List (Element.Attribute msg) -> Element.Element msg -> Element.Input.Label msg -}
labelLeft : List Elm.Expression -> Elm.Expression -> Elm.Expression
labelLeft labelLeftArg_ labelLeftArg_0 =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Input" ]
             , name = "labelLeft"
             , annotation =
                 Just
                     (Type.function
                          [ Type.list
                              (Type.namedWith
                                 [ "Element" ]
                                 "Attribute"
                                 [ Type.var "msg" ]
                              )
                          , Type.namedWith
                              [ "Element" ]
                              "Element"
                              [ Type.var "msg" ]
                          ]
                          (Type.namedWith
                               [ "Element", "Input" ]
                               "Label"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ Elm.list labelLeftArg_, labelLeftArg_0 ]


{-| labelRight: List (Element.Attribute msg) -> Element.Element msg -> Element.Input.Label msg -}
labelRight : List Elm.Expression -> Elm.Expression -> Elm.Expression
labelRight labelRightArg_ labelRightArg_0 =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Input" ]
             , name = "labelRight"
             , annotation =
                 Just
                     (Type.function
                          [ Type.list
                              (Type.namedWith
                                 [ "Element" ]
                                 "Attribute"
                                 [ Type.var "msg" ]
                              )
                          , Type.namedWith
                              [ "Element" ]
                              "Element"
                              [ Type.var "msg" ]
                          ]
                          (Type.namedWith
                               [ "Element", "Input" ]
                               "Label"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ Elm.list labelRightArg_, labelRightArg_0 ]


{-| Sometimes you may need to have a label which is not visible, but is still accessible to screen readers.

Seriously consider a visible label before using this.

The situations where a hidden label makes sense:

  - A searchbar with a `search` button right next to it.
  - A `table` of inputs where the header gives the label.

Basically, a hidden label works when there are other contextual clues that sighted people can pick up on.

labelHidden: String -> Element.Input.Label msg
-}
labelHidden : String -> Elm.Expression
labelHidden labelHiddenArg_ =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Input" ]
             , name = "labelHidden"
             , annotation =
                 Just
                     (Type.function
                          [ Type.string ]
                          (Type.namedWith
                               [ "Element", "Input" ]
                               "Label"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ Elm.string labelHiddenArg_ ]


annotation_ :
    { placeholder : Type.Annotation -> Type.Annotation
    , thumb : Type.Annotation
    , option : Type.Annotation -> Type.Annotation -> Type.Annotation
    , optionState : Type.Annotation
    , label : Type.Annotation -> Type.Annotation
    }
annotation_ =
    { placeholder =
        \placeholderArg0 ->
            Type.namedWith
                [ "Element", "Input" ]
                "Placeholder"
                [ placeholderArg0 ]
    , thumb = Type.namedWith [ "Element", "Input" ] "Thumb" []
    , option =
        \optionArg0 optionArg1 ->
            Type.namedWith
                [ "Element", "Input" ]
                "Option"
                [ optionArg0, optionArg1 ]
    , optionState = Type.namedWith [ "Element", "Input" ] "OptionState" []
    , label =
        \labelArg0 ->
            Type.namedWith [ "Element", "Input" ] "Label" [ labelArg0 ]
    }


make_ :
    { idle : Elm.Expression
    , focused : Elm.Expression
    , selected : Elm.Expression
    }
make_ =
    { idle =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "Idle"
            , annotation = Just (Type.namedWith [] "OptionState" [])
            }
    , focused =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "Focused"
            , annotation = Just (Type.namedWith [] "OptionState" [])
            }
    , selected =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "Selected"
            , annotation = Just (Type.namedWith [] "OptionState" [])
            }
    }


caseOf_ :
    { optionState :
        Elm.Expression
        -> { idle : Elm.Expression
        , focused : Elm.Expression
        , selected : Elm.Expression
        }
        -> Elm.Expression
    }
caseOf_ =
    { optionState =
        \optionStateExpression optionStateTags ->
            Elm.Case.custom
                optionStateExpression
                (Type.namedWith [ "Element", "Input" ] "OptionState" [])
                [ Elm.Case.branch
                    (Elm.Arg.customType "Idle" optionStateTags.idle)
                    Basics.identity
                , Elm.Case.branch
                    (Elm.Arg.customType "Focused" optionStateTags.focused)
                    Basics.identity
                , Elm.Case.branch
                    (Elm.Arg.customType "Selected" optionStateTags.selected)
                    Basics.identity
                ]
    }


call_ :
    { button : Elm.Expression -> Elm.Expression -> Elm.Expression
    , checkbox : Elm.Expression -> Elm.Expression -> Elm.Expression
    , defaultCheckbox : Elm.Expression -> Elm.Expression
    , text : Elm.Expression -> Elm.Expression -> Elm.Expression
    , multiline : Elm.Expression -> Elm.Expression -> Elm.Expression
    , placeholder : Elm.Expression -> Elm.Expression -> Elm.Expression
    , username : Elm.Expression -> Elm.Expression -> Elm.Expression
    , newPassword : Elm.Expression -> Elm.Expression -> Elm.Expression
    , currentPassword : Elm.Expression -> Elm.Expression -> Elm.Expression
    , email : Elm.Expression -> Elm.Expression -> Elm.Expression
    , search : Elm.Expression -> Elm.Expression -> Elm.Expression
    , spellChecked : Elm.Expression -> Elm.Expression -> Elm.Expression
    , slider : Elm.Expression -> Elm.Expression -> Elm.Expression
    , thumb : Elm.Expression -> Elm.Expression
    , radio : Elm.Expression -> Elm.Expression -> Elm.Expression
    , radioRow : Elm.Expression -> Elm.Expression -> Elm.Expression
    , option : Elm.Expression -> Elm.Expression -> Elm.Expression
    , optionWith : Elm.Expression -> Elm.Expression -> Elm.Expression
    , labelAbove : Elm.Expression -> Elm.Expression -> Elm.Expression
    , labelBelow : Elm.Expression -> Elm.Expression -> Elm.Expression
    , labelLeft : Elm.Expression -> Elm.Expression -> Elm.Expression
    , labelRight : Elm.Expression -> Elm.Expression -> Elm.Expression
    , labelHidden : Elm.Expression -> Elm.Expression
    }
call_ =
    { button =
        \buttonArg_ buttonArg_0 ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Input" ]
                     , name = "button"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.list
                                      (Type.namedWith
                                         [ "Element" ]
                                         "Attribute"
                                         [ Type.var "msg" ]
                                      )
                                  , Type.record
                                      [ ( "onPress"
                                        , Type.maybe (Type.var "msg")
                                        )
                                      , ( "label"
                                        , Type.namedWith
                                            [ "Element" ]
                                            "Element"
                                            [ Type.var "msg" ]
                                        )
                                      ]
                                  ]
                                  (Type.namedWith
                                       [ "Element" ]
                                       "Element"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ buttonArg_, buttonArg_0 ]
    , checkbox =
        \checkboxArg_ checkboxArg_0 ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Input" ]
                     , name = "checkbox"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.list
                                      (Type.namedWith
                                         [ "Element" ]
                                         "Attribute"
                                         [ Type.var "msg" ]
                                      )
                                  , Type.record
                                      [ ( "onChange"
                                        , Type.function
                                            [ Type.bool ]
                                            (Type.var "msg")
                                        )
                                      , ( "icon"
                                        , Type.function
                                            [ Type.bool ]
                                            (Type.namedWith
                                               [ "Element" ]
                                               "Element"
                                               [ Type.var "msg" ]
                                            )
                                        )
                                      , ( "checked", Type.bool )
                                      , ( "label"
                                        , Type.namedWith
                                            [ "Element", "Input" ]
                                            "Label"
                                            [ Type.var "msg" ]
                                        )
                                      ]
                                  ]
                                  (Type.namedWith
                                       [ "Element" ]
                                       "Element"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ checkboxArg_, checkboxArg_0 ]
    , defaultCheckbox =
        \defaultCheckboxArg_ ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Input" ]
                     , name = "defaultCheckbox"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.bool ]
                                  (Type.namedWith
                                       [ "Element" ]
                                       "Element"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ defaultCheckboxArg_ ]
    , text =
        \textArg_ textArg_0 ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Input" ]
                     , name = "text"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.list
                                      (Type.namedWith
                                         [ "Element" ]
                                         "Attribute"
                                         [ Type.var "msg" ]
                                      )
                                  , Type.record
                                      [ ( "onChange"
                                        , Type.function
                                            [ Type.string ]
                                            (Type.var "msg")
                                        )
                                      , ( "text", Type.string )
                                      , ( "placeholder"
                                        , Type.maybe
                                            (Type.namedWith
                                               [ "Element", "Input" ]
                                               "Placeholder"
                                               [ Type.var "msg" ]
                                            )
                                        )
                                      , ( "label"
                                        , Type.namedWith
                                            [ "Element", "Input" ]
                                            "Label"
                                            [ Type.var "msg" ]
                                        )
                                      ]
                                  ]
                                  (Type.namedWith
                                       [ "Element" ]
                                       "Element"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ textArg_, textArg_0 ]
    , multiline =
        \multilineArg_ multilineArg_0 ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Input" ]
                     , name = "multiline"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.list
                                      (Type.namedWith
                                         [ "Element" ]
                                         "Attribute"
                                         [ Type.var "msg" ]
                                      )
                                  , Type.record
                                      [ ( "onChange"
                                        , Type.function
                                            [ Type.string ]
                                            (Type.var "msg")
                                        )
                                      , ( "text", Type.string )
                                      , ( "placeholder"
                                        , Type.maybe
                                            (Type.namedWith
                                               [ "Element", "Input" ]
                                               "Placeholder"
                                               [ Type.var "msg" ]
                                            )
                                        )
                                      , ( "label"
                                        , Type.namedWith
                                            [ "Element", "Input" ]
                                            "Label"
                                            [ Type.var "msg" ]
                                        )
                                      , ( "spellcheck", Type.bool )
                                      ]
                                  ]
                                  (Type.namedWith
                                       [ "Element" ]
                                       "Element"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ multilineArg_, multilineArg_0 ]
    , placeholder =
        \placeholderArg_ placeholderArg_0 ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Input" ]
                     , name = "placeholder"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.list
                                      (Type.namedWith
                                         [ "Element" ]
                                         "Attribute"
                                         [ Type.var "msg" ]
                                      )
                                  , Type.namedWith
                                      [ "Element" ]
                                      "Element"
                                      [ Type.var "msg" ]
                                  ]
                                  (Type.namedWith
                                       [ "Element", "Input" ]
                                       "Placeholder"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ placeholderArg_, placeholderArg_0 ]
    , username =
        \usernameArg_ usernameArg_0 ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Input" ]
                     , name = "username"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.list
                                      (Type.namedWith
                                         [ "Element" ]
                                         "Attribute"
                                         [ Type.var "msg" ]
                                      )
                                  , Type.record
                                      [ ( "onChange"
                                        , Type.function
                                            [ Type.string ]
                                            (Type.var "msg")
                                        )
                                      , ( "text", Type.string )
                                      , ( "placeholder"
                                        , Type.maybe
                                            (Type.namedWith
                                               [ "Element", "Input" ]
                                               "Placeholder"
                                               [ Type.var "msg" ]
                                            )
                                        )
                                      , ( "label"
                                        , Type.namedWith
                                            [ "Element", "Input" ]
                                            "Label"
                                            [ Type.var "msg" ]
                                        )
                                      ]
                                  ]
                                  (Type.namedWith
                                       [ "Element" ]
                                       "Element"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ usernameArg_, usernameArg_0 ]
    , newPassword =
        \newPasswordArg_ newPasswordArg_0 ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Input" ]
                     , name = "newPassword"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.list
                                      (Type.namedWith
                                         [ "Element" ]
                                         "Attribute"
                                         [ Type.var "msg" ]
                                      )
                                  , Type.record
                                      [ ( "onChange"
                                        , Type.function
                                            [ Type.string ]
                                            (Type.var "msg")
                                        )
                                      , ( "text", Type.string )
                                      , ( "placeholder"
                                        , Type.maybe
                                            (Type.namedWith
                                               [ "Element", "Input" ]
                                               "Placeholder"
                                               [ Type.var "msg" ]
                                            )
                                        )
                                      , ( "label"
                                        , Type.namedWith
                                            [ "Element", "Input" ]
                                            "Label"
                                            [ Type.var "msg" ]
                                        )
                                      , ( "show", Type.bool )
                                      ]
                                  ]
                                  (Type.namedWith
                                       [ "Element" ]
                                       "Element"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ newPasswordArg_, newPasswordArg_0 ]
    , currentPassword =
        \currentPasswordArg_ currentPasswordArg_0 ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Input" ]
                     , name = "currentPassword"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.list
                                      (Type.namedWith
                                         [ "Element" ]
                                         "Attribute"
                                         [ Type.var "msg" ]
                                      )
                                  , Type.record
                                      [ ( "onChange"
                                        , Type.function
                                            [ Type.string ]
                                            (Type.var "msg")
                                        )
                                      , ( "text", Type.string )
                                      , ( "placeholder"
                                        , Type.maybe
                                            (Type.namedWith
                                               [ "Element", "Input" ]
                                               "Placeholder"
                                               [ Type.var "msg" ]
                                            )
                                        )
                                      , ( "label"
                                        , Type.namedWith
                                            [ "Element", "Input" ]
                                            "Label"
                                            [ Type.var "msg" ]
                                        )
                                      , ( "show", Type.bool )
                                      ]
                                  ]
                                  (Type.namedWith
                                       [ "Element" ]
                                       "Element"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ currentPasswordArg_, currentPasswordArg_0 ]
    , email =
        \emailArg_ emailArg_0 ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Input" ]
                     , name = "email"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.list
                                      (Type.namedWith
                                         [ "Element" ]
                                         "Attribute"
                                         [ Type.var "msg" ]
                                      )
                                  , Type.record
                                      [ ( "onChange"
                                        , Type.function
                                            [ Type.string ]
                                            (Type.var "msg")
                                        )
                                      , ( "text", Type.string )
                                      , ( "placeholder"
                                        , Type.maybe
                                            (Type.namedWith
                                               [ "Element", "Input" ]
                                               "Placeholder"
                                               [ Type.var "msg" ]
                                            )
                                        )
                                      , ( "label"
                                        , Type.namedWith
                                            [ "Element", "Input" ]
                                            "Label"
                                            [ Type.var "msg" ]
                                        )
                                      ]
                                  ]
                                  (Type.namedWith
                                       [ "Element" ]
                                       "Element"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ emailArg_, emailArg_0 ]
    , search =
        \searchArg_ searchArg_0 ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Input" ]
                     , name = "search"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.list
                                      (Type.namedWith
                                         [ "Element" ]
                                         "Attribute"
                                         [ Type.var "msg" ]
                                      )
                                  , Type.record
                                      [ ( "onChange"
                                        , Type.function
                                            [ Type.string ]
                                            (Type.var "msg")
                                        )
                                      , ( "text", Type.string )
                                      , ( "placeholder"
                                        , Type.maybe
                                            (Type.namedWith
                                               [ "Element", "Input" ]
                                               "Placeholder"
                                               [ Type.var "msg" ]
                                            )
                                        )
                                      , ( "label"
                                        , Type.namedWith
                                            [ "Element", "Input" ]
                                            "Label"
                                            [ Type.var "msg" ]
                                        )
                                      ]
                                  ]
                                  (Type.namedWith
                                       [ "Element" ]
                                       "Element"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ searchArg_, searchArg_0 ]
    , spellChecked =
        \spellCheckedArg_ spellCheckedArg_0 ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Input" ]
                     , name = "spellChecked"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.list
                                      (Type.namedWith
                                         [ "Element" ]
                                         "Attribute"
                                         [ Type.var "msg" ]
                                      )
                                  , Type.record
                                      [ ( "onChange"
                                        , Type.function
                                            [ Type.string ]
                                            (Type.var "msg")
                                        )
                                      , ( "text", Type.string )
                                      , ( "placeholder"
                                        , Type.maybe
                                            (Type.namedWith
                                               [ "Element", "Input" ]
                                               "Placeholder"
                                               [ Type.var "msg" ]
                                            )
                                        )
                                      , ( "label"
                                        , Type.namedWith
                                            [ "Element", "Input" ]
                                            "Label"
                                            [ Type.var "msg" ]
                                        )
                                      ]
                                  ]
                                  (Type.namedWith
                                       [ "Element" ]
                                       "Element"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ spellCheckedArg_, spellCheckedArg_0 ]
    , slider =
        \sliderArg_ sliderArg_0 ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Input" ]
                     , name = "slider"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.list
                                      (Type.namedWith
                                         [ "Element" ]
                                         "Attribute"
                                         [ Type.var "msg" ]
                                      )
                                  , Type.record
                                      [ ( "onChange"
                                        , Type.function
                                            [ Type.float ]
                                            (Type.var "msg")
                                        )
                                      , ( "label"
                                        , Type.namedWith
                                            [ "Element", "Input" ]
                                            "Label"
                                            [ Type.var "msg" ]
                                        )
                                      , ( "min", Type.float )
                                      , ( "max", Type.float )
                                      , ( "value", Type.float )
                                      , ( "thumb"
                                        , Type.namedWith
                                            [ "Element", "Input" ]
                                            "Thumb"
                                            []
                                        )
                                      , ( "step", Type.maybe Type.float )
                                      ]
                                  ]
                                  (Type.namedWith
                                       [ "Element" ]
                                       "Element"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ sliderArg_, sliderArg_0 ]
    , thumb =
        \thumbArg_ ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Input" ]
                     , name = "thumb"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.list
                                      (Type.namedWith
                                         [ "Element" ]
                                         "Attribute"
                                         [ Type.namedWith
                                               [ "Basics" ]
                                               "Never"
                                               []
                                         ]
                                      )
                                  ]
                                  (Type.namedWith
                                       [ "Element", "Input" ]
                                       "Thumb"
                                       []
                                  )
                             )
                     }
                )
                [ thumbArg_ ]
    , radio =
        \radioArg_ radioArg_0 ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Input" ]
                     , name = "radio"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.list
                                      (Type.namedWith
                                         [ "Element" ]
                                         "Attribute"
                                         [ Type.var "msg" ]
                                      )
                                  , Type.record
                                      [ ( "onChange"
                                        , Type.function
                                            [ Type.var "option" ]
                                            (Type.var "msg")
                                        )
                                      , ( "options"
                                        , Type.list
                                            (Type.namedWith
                                               [ "Element", "Input" ]
                                               "Option"
                                               [ Type.var "option"
                                               , Type.var "msg"
                                               ]
                                            )
                                        )
                                      , ( "selected"
                                        , Type.maybe (Type.var "option")
                                        )
                                      , ( "label"
                                        , Type.namedWith
                                            [ "Element", "Input" ]
                                            "Label"
                                            [ Type.var "msg" ]
                                        )
                                      ]
                                  ]
                                  (Type.namedWith
                                       [ "Element" ]
                                       "Element"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ radioArg_, radioArg_0 ]
    , radioRow =
        \radioRowArg_ radioRowArg_0 ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Input" ]
                     , name = "radioRow"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.list
                                      (Type.namedWith
                                         [ "Element" ]
                                         "Attribute"
                                         [ Type.var "msg" ]
                                      )
                                  , Type.record
                                      [ ( "onChange"
                                        , Type.function
                                            [ Type.var "option" ]
                                            (Type.var "msg")
                                        )
                                      , ( "options"
                                        , Type.list
                                            (Type.namedWith
                                               [ "Element", "Input" ]
                                               "Option"
                                               [ Type.var "option"
                                               , Type.var "msg"
                                               ]
                                            )
                                        )
                                      , ( "selected"
                                        , Type.maybe (Type.var "option")
                                        )
                                      , ( "label"
                                        , Type.namedWith
                                            [ "Element", "Input" ]
                                            "Label"
                                            [ Type.var "msg" ]
                                        )
                                      ]
                                  ]
                                  (Type.namedWith
                                       [ "Element" ]
                                       "Element"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ radioRowArg_, radioRowArg_0 ]
    , option =
        \optionArg_ optionArg_0 ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Input" ]
                     , name = "option"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.var "value"
                                  , Type.namedWith
                                      [ "Element" ]
                                      "Element"
                                      [ Type.var "msg" ]
                                  ]
                                  (Type.namedWith
                                       [ "Element", "Input" ]
                                       "Option"
                                       [ Type.var "value", Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ optionArg_, optionArg_0 ]
    , optionWith =
        \optionWithArg_ optionWithArg_0 ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Input" ]
                     , name = "optionWith"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.var "value"
                                  , Type.function
                                      [ Type.namedWith
                                            [ "Element", "Input" ]
                                            "OptionState"
                                            []
                                      ]
                                      (Type.namedWith
                                         [ "Element" ]
                                         "Element"
                                         [ Type.var "msg" ]
                                      )
                                  ]
                                  (Type.namedWith
                                       [ "Element", "Input" ]
                                       "Option"
                                       [ Type.var "value", Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ optionWithArg_, optionWithArg_0 ]
    , labelAbove =
        \labelAboveArg_ labelAboveArg_0 ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Input" ]
                     , name = "labelAbove"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.list
                                      (Type.namedWith
                                         [ "Element" ]
                                         "Attribute"
                                         [ Type.var "msg" ]
                                      )
                                  , Type.namedWith
                                      [ "Element" ]
                                      "Element"
                                      [ Type.var "msg" ]
                                  ]
                                  (Type.namedWith
                                       [ "Element", "Input" ]
                                       "Label"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ labelAboveArg_, labelAboveArg_0 ]
    , labelBelow =
        \labelBelowArg_ labelBelowArg_0 ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Input" ]
                     , name = "labelBelow"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.list
                                      (Type.namedWith
                                         [ "Element" ]
                                         "Attribute"
                                         [ Type.var "msg" ]
                                      )
                                  , Type.namedWith
                                      [ "Element" ]
                                      "Element"
                                      [ Type.var "msg" ]
                                  ]
                                  (Type.namedWith
                                       [ "Element", "Input" ]
                                       "Label"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ labelBelowArg_, labelBelowArg_0 ]
    , labelLeft =
        \labelLeftArg_ labelLeftArg_0 ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Input" ]
                     , name = "labelLeft"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.list
                                      (Type.namedWith
                                         [ "Element" ]
                                         "Attribute"
                                         [ Type.var "msg" ]
                                      )
                                  , Type.namedWith
                                      [ "Element" ]
                                      "Element"
                                      [ Type.var "msg" ]
                                  ]
                                  (Type.namedWith
                                       [ "Element", "Input" ]
                                       "Label"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ labelLeftArg_, labelLeftArg_0 ]
    , labelRight =
        \labelRightArg_ labelRightArg_0 ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Input" ]
                     , name = "labelRight"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.list
                                      (Type.namedWith
                                         [ "Element" ]
                                         "Attribute"
                                         [ Type.var "msg" ]
                                      )
                                  , Type.namedWith
                                      [ "Element" ]
                                      "Element"
                                      [ Type.var "msg" ]
                                  ]
                                  (Type.namedWith
                                       [ "Element", "Input" ]
                                       "Label"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ labelRightArg_, labelRightArg_0 ]
    , labelHidden =
        \labelHiddenArg_ ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Input" ]
                     , name = "labelHidden"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.string ]
                                  (Type.namedWith
                                       [ "Element", "Input" ]
                                       "Label"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ labelHiddenArg_ ]
    }


values_ :
    { focusedOnLoad : Elm.Expression
    , button : Elm.Expression
    , checkbox : Elm.Expression
    , defaultCheckbox : Elm.Expression
    , text : Elm.Expression
    , multiline : Elm.Expression
    , placeholder : Elm.Expression
    , username : Elm.Expression
    , newPassword : Elm.Expression
    , currentPassword : Elm.Expression
    , email : Elm.Expression
    , search : Elm.Expression
    , spellChecked : Elm.Expression
    , slider : Elm.Expression
    , thumb : Elm.Expression
    , defaultThumb : Elm.Expression
    , radio : Elm.Expression
    , radioRow : Elm.Expression
    , option : Elm.Expression
    , optionWith : Elm.Expression
    , labelAbove : Elm.Expression
    , labelBelow : Elm.Expression
    , labelLeft : Elm.Expression
    , labelRight : Elm.Expression
    , labelHidden : Elm.Expression
    }
values_ =
    { focusedOnLoad =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "focusedOnLoad"
            , annotation =
                Just
                    (Type.namedWith [ "Element" ] "Attribute" [ Type.var "msg" ]
                    )
            }
    , button =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "button"
            , annotation =
                Just
                    (Type.function
                         [ Type.list
                             (Type.namedWith
                                [ "Element" ]
                                "Attribute"
                                [ Type.var "msg" ]
                             )
                         , Type.record
                             [ ( "onPress", Type.maybe (Type.var "msg") )
                             , ( "label"
                               , Type.namedWith
                                   [ "Element" ]
                                   "Element"
                                   [ Type.var "msg" ]
                               )
                             ]
                         ]
                         (Type.namedWith
                              [ "Element" ]
                              "Element"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , checkbox =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "checkbox"
            , annotation =
                Just
                    (Type.function
                         [ Type.list
                             (Type.namedWith
                                [ "Element" ]
                                "Attribute"
                                [ Type.var "msg" ]
                             )
                         , Type.record
                             [ ( "onChange"
                               , Type.function [ Type.bool ] (Type.var "msg")
                               )
                             , ( "icon"
                               , Type.function
                                   [ Type.bool ]
                                   (Type.namedWith
                                      [ "Element" ]
                                      "Element"
                                      [ Type.var "msg" ]
                                   )
                               )
                             , ( "checked", Type.bool )
                             , ( "label"
                               , Type.namedWith
                                   [ "Element", "Input" ]
                                   "Label"
                                   [ Type.var "msg" ]
                               )
                             ]
                         ]
                         (Type.namedWith
                              [ "Element" ]
                              "Element"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , defaultCheckbox =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "defaultCheckbox"
            , annotation =
                Just
                    (Type.function
                         [ Type.bool ]
                         (Type.namedWith
                              [ "Element" ]
                              "Element"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , text =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "text"
            , annotation =
                Just
                    (Type.function
                         [ Type.list
                             (Type.namedWith
                                [ "Element" ]
                                "Attribute"
                                [ Type.var "msg" ]
                             )
                         , Type.record
                             [ ( "onChange"
                               , Type.function [ Type.string ] (Type.var "msg")
                               )
                             , ( "text", Type.string )
                             , ( "placeholder"
                               , Type.maybe
                                   (Type.namedWith
                                      [ "Element", "Input" ]
                                      "Placeholder"
                                      [ Type.var "msg" ]
                                   )
                               )
                             , ( "label"
                               , Type.namedWith
                                   [ "Element", "Input" ]
                                   "Label"
                                   [ Type.var "msg" ]
                               )
                             ]
                         ]
                         (Type.namedWith
                              [ "Element" ]
                              "Element"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , multiline =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "multiline"
            , annotation =
                Just
                    (Type.function
                         [ Type.list
                             (Type.namedWith
                                [ "Element" ]
                                "Attribute"
                                [ Type.var "msg" ]
                             )
                         , Type.record
                             [ ( "onChange"
                               , Type.function [ Type.string ] (Type.var "msg")
                               )
                             , ( "text", Type.string )
                             , ( "placeholder"
                               , Type.maybe
                                   (Type.namedWith
                                      [ "Element", "Input" ]
                                      "Placeholder"
                                      [ Type.var "msg" ]
                                   )
                               )
                             , ( "label"
                               , Type.namedWith
                                   [ "Element", "Input" ]
                                   "Label"
                                   [ Type.var "msg" ]
                               )
                             , ( "spellcheck", Type.bool )
                             ]
                         ]
                         (Type.namedWith
                              [ "Element" ]
                              "Element"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , placeholder =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "placeholder"
            , annotation =
                Just
                    (Type.function
                         [ Type.list
                             (Type.namedWith
                                [ "Element" ]
                                "Attribute"
                                [ Type.var "msg" ]
                             )
                         , Type.namedWith
                             [ "Element" ]
                             "Element"
                             [ Type.var "msg" ]
                         ]
                         (Type.namedWith
                              [ "Element", "Input" ]
                              "Placeholder"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , username =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "username"
            , annotation =
                Just
                    (Type.function
                         [ Type.list
                             (Type.namedWith
                                [ "Element" ]
                                "Attribute"
                                [ Type.var "msg" ]
                             )
                         , Type.record
                             [ ( "onChange"
                               , Type.function [ Type.string ] (Type.var "msg")
                               )
                             , ( "text", Type.string )
                             , ( "placeholder"
                               , Type.maybe
                                   (Type.namedWith
                                      [ "Element", "Input" ]
                                      "Placeholder"
                                      [ Type.var "msg" ]
                                   )
                               )
                             , ( "label"
                               , Type.namedWith
                                   [ "Element", "Input" ]
                                   "Label"
                                   [ Type.var "msg" ]
                               )
                             ]
                         ]
                         (Type.namedWith
                              [ "Element" ]
                              "Element"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , newPassword =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "newPassword"
            , annotation =
                Just
                    (Type.function
                         [ Type.list
                             (Type.namedWith
                                [ "Element" ]
                                "Attribute"
                                [ Type.var "msg" ]
                             )
                         , Type.record
                             [ ( "onChange"
                               , Type.function [ Type.string ] (Type.var "msg")
                               )
                             , ( "text", Type.string )
                             , ( "placeholder"
                               , Type.maybe
                                   (Type.namedWith
                                      [ "Element", "Input" ]
                                      "Placeholder"
                                      [ Type.var "msg" ]
                                   )
                               )
                             , ( "label"
                               , Type.namedWith
                                   [ "Element", "Input" ]
                                   "Label"
                                   [ Type.var "msg" ]
                               )
                             , ( "show", Type.bool )
                             ]
                         ]
                         (Type.namedWith
                              [ "Element" ]
                              "Element"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , currentPassword =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "currentPassword"
            , annotation =
                Just
                    (Type.function
                         [ Type.list
                             (Type.namedWith
                                [ "Element" ]
                                "Attribute"
                                [ Type.var "msg" ]
                             )
                         , Type.record
                             [ ( "onChange"
                               , Type.function [ Type.string ] (Type.var "msg")
                               )
                             , ( "text", Type.string )
                             , ( "placeholder"
                               , Type.maybe
                                   (Type.namedWith
                                      [ "Element", "Input" ]
                                      "Placeholder"
                                      [ Type.var "msg" ]
                                   )
                               )
                             , ( "label"
                               , Type.namedWith
                                   [ "Element", "Input" ]
                                   "Label"
                                   [ Type.var "msg" ]
                               )
                             , ( "show", Type.bool )
                             ]
                         ]
                         (Type.namedWith
                              [ "Element" ]
                              "Element"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , email =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "email"
            , annotation =
                Just
                    (Type.function
                         [ Type.list
                             (Type.namedWith
                                [ "Element" ]
                                "Attribute"
                                [ Type.var "msg" ]
                             )
                         , Type.record
                             [ ( "onChange"
                               , Type.function [ Type.string ] (Type.var "msg")
                               )
                             , ( "text", Type.string )
                             , ( "placeholder"
                               , Type.maybe
                                   (Type.namedWith
                                      [ "Element", "Input" ]
                                      "Placeholder"
                                      [ Type.var "msg" ]
                                   )
                               )
                             , ( "label"
                               , Type.namedWith
                                   [ "Element", "Input" ]
                                   "Label"
                                   [ Type.var "msg" ]
                               )
                             ]
                         ]
                         (Type.namedWith
                              [ "Element" ]
                              "Element"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , search =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "search"
            , annotation =
                Just
                    (Type.function
                         [ Type.list
                             (Type.namedWith
                                [ "Element" ]
                                "Attribute"
                                [ Type.var "msg" ]
                             )
                         , Type.record
                             [ ( "onChange"
                               , Type.function [ Type.string ] (Type.var "msg")
                               )
                             , ( "text", Type.string )
                             , ( "placeholder"
                               , Type.maybe
                                   (Type.namedWith
                                      [ "Element", "Input" ]
                                      "Placeholder"
                                      [ Type.var "msg" ]
                                   )
                               )
                             , ( "label"
                               , Type.namedWith
                                   [ "Element", "Input" ]
                                   "Label"
                                   [ Type.var "msg" ]
                               )
                             ]
                         ]
                         (Type.namedWith
                              [ "Element" ]
                              "Element"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , spellChecked =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "spellChecked"
            , annotation =
                Just
                    (Type.function
                         [ Type.list
                             (Type.namedWith
                                [ "Element" ]
                                "Attribute"
                                [ Type.var "msg" ]
                             )
                         , Type.record
                             [ ( "onChange"
                               , Type.function [ Type.string ] (Type.var "msg")
                               )
                             , ( "text", Type.string )
                             , ( "placeholder"
                               , Type.maybe
                                   (Type.namedWith
                                      [ "Element", "Input" ]
                                      "Placeholder"
                                      [ Type.var "msg" ]
                                   )
                               )
                             , ( "label"
                               , Type.namedWith
                                   [ "Element", "Input" ]
                                   "Label"
                                   [ Type.var "msg" ]
                               )
                             ]
                         ]
                         (Type.namedWith
                              [ "Element" ]
                              "Element"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , slider =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "slider"
            , annotation =
                Just
                    (Type.function
                         [ Type.list
                             (Type.namedWith
                                [ "Element" ]
                                "Attribute"
                                [ Type.var "msg" ]
                             )
                         , Type.record
                             [ ( "onChange"
                               , Type.function [ Type.float ] (Type.var "msg")
                               )
                             , ( "label"
                               , Type.namedWith
                                   [ "Element", "Input" ]
                                   "Label"
                                   [ Type.var "msg" ]
                               )
                             , ( "min", Type.float )
                             , ( "max", Type.float )
                             , ( "value", Type.float )
                             , ( "thumb"
                               , Type.namedWith
                                   [ "Element", "Input" ]
                                   "Thumb"
                                   []
                               )
                             , ( "step", Type.maybe Type.float )
                             ]
                         ]
                         (Type.namedWith
                              [ "Element" ]
                              "Element"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , thumb =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "thumb"
            , annotation =
                Just
                    (Type.function
                         [ Type.list
                             (Type.namedWith
                                [ "Element" ]
                                "Attribute"
                                [ Type.namedWith [ "Basics" ] "Never" [] ]
                             )
                         ]
                         (Type.namedWith [ "Element", "Input" ] "Thumb" [])
                    )
            }
    , defaultThumb =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "defaultThumb"
            , annotation =
                Just (Type.namedWith [ "Element", "Input" ] "Thumb" [])
            }
    , radio =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "radio"
            , annotation =
                Just
                    (Type.function
                         [ Type.list
                             (Type.namedWith
                                [ "Element" ]
                                "Attribute"
                                [ Type.var "msg" ]
                             )
                         , Type.record
                             [ ( "onChange"
                               , Type.function
                                   [ Type.var "option" ]
                                   (Type.var "msg")
                               )
                             , ( "options"
                               , Type.list
                                   (Type.namedWith
                                      [ "Element", "Input" ]
                                      "Option"
                                      [ Type.var "option", Type.var "msg" ]
                                   )
                               )
                             , ( "selected", Type.maybe (Type.var "option") )
                             , ( "label"
                               , Type.namedWith
                                   [ "Element", "Input" ]
                                   "Label"
                                   [ Type.var "msg" ]
                               )
                             ]
                         ]
                         (Type.namedWith
                              [ "Element" ]
                              "Element"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , radioRow =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "radioRow"
            , annotation =
                Just
                    (Type.function
                         [ Type.list
                             (Type.namedWith
                                [ "Element" ]
                                "Attribute"
                                [ Type.var "msg" ]
                             )
                         , Type.record
                             [ ( "onChange"
                               , Type.function
                                   [ Type.var "option" ]
                                   (Type.var "msg")
                               )
                             , ( "options"
                               , Type.list
                                   (Type.namedWith
                                      [ "Element", "Input" ]
                                      "Option"
                                      [ Type.var "option", Type.var "msg" ]
                                   )
                               )
                             , ( "selected", Type.maybe (Type.var "option") )
                             , ( "label"
                               , Type.namedWith
                                   [ "Element", "Input" ]
                                   "Label"
                                   [ Type.var "msg" ]
                               )
                             ]
                         ]
                         (Type.namedWith
                              [ "Element" ]
                              "Element"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , option =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "option"
            , annotation =
                Just
                    (Type.function
                         [ Type.var "value"
                         , Type.namedWith
                             [ "Element" ]
                             "Element"
                             [ Type.var "msg" ]
                         ]
                         (Type.namedWith
                              [ "Element", "Input" ]
                              "Option"
                              [ Type.var "value", Type.var "msg" ]
                         )
                    )
            }
    , optionWith =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "optionWith"
            , annotation =
                Just
                    (Type.function
                         [ Type.var "value"
                         , Type.function
                             [ Type.namedWith
                                   [ "Element", "Input" ]
                                   "OptionState"
                                   []
                             ]
                             (Type.namedWith
                                [ "Element" ]
                                "Element"
                                [ Type.var "msg" ]
                             )
                         ]
                         (Type.namedWith
                              [ "Element", "Input" ]
                              "Option"
                              [ Type.var "value", Type.var "msg" ]
                         )
                    )
            }
    , labelAbove =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "labelAbove"
            , annotation =
                Just
                    (Type.function
                         [ Type.list
                             (Type.namedWith
                                [ "Element" ]
                                "Attribute"
                                [ Type.var "msg" ]
                             )
                         , Type.namedWith
                             [ "Element" ]
                             "Element"
                             [ Type.var "msg" ]
                         ]
                         (Type.namedWith
                              [ "Element", "Input" ]
                              "Label"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , labelBelow =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "labelBelow"
            , annotation =
                Just
                    (Type.function
                         [ Type.list
                             (Type.namedWith
                                [ "Element" ]
                                "Attribute"
                                [ Type.var "msg" ]
                             )
                         , Type.namedWith
                             [ "Element" ]
                             "Element"
                             [ Type.var "msg" ]
                         ]
                         (Type.namedWith
                              [ "Element", "Input" ]
                              "Label"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , labelLeft =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "labelLeft"
            , annotation =
                Just
                    (Type.function
                         [ Type.list
                             (Type.namedWith
                                [ "Element" ]
                                "Attribute"
                                [ Type.var "msg" ]
                             )
                         , Type.namedWith
                             [ "Element" ]
                             "Element"
                             [ Type.var "msg" ]
                         ]
                         (Type.namedWith
                              [ "Element", "Input" ]
                              "Label"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , labelRight =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "labelRight"
            , annotation =
                Just
                    (Type.function
                         [ Type.list
                             (Type.namedWith
                                [ "Element" ]
                                "Attribute"
                                [ Type.var "msg" ]
                             )
                         , Type.namedWith
                             [ "Element" ]
                             "Element"
                             [ Type.var "msg" ]
                         ]
                         (Type.namedWith
                              [ "Element", "Input" ]
                              "Label"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , labelHidden =
        Elm.value
            { importFrom = [ "Element", "Input" ]
            , name = "labelHidden"
            , annotation =
                Just
                    (Type.function
                         [ Type.string ]
                         (Type.namedWith
                              [ "Element", "Input" ]
                              "Label"
                              [ Type.var "msg" ]
                         )
                    )
            }
    }