module Gen.Element.Events exposing
    ( moduleName_, onClick, onDoubleClick, onMouseDown, onMouseUp, onMouseEnter
    , onMouseLeave, onMouseMove, onFocus, onLoseFocus, call_, values_
    )

{-|
# Generated bindings for Element.Events

@docs moduleName_, onClick, onDoubleClick, onMouseDown, onMouseUp, onMouseEnter
@docs onMouseLeave, onMouseMove, onFocus, onLoseFocus, call_, values_
-}


import Elm
import Elm.Annotation as Type


{-| The name of this module. -}
moduleName_ : List String
moduleName_ =
    [ "Element", "Events" ]


{-| onClick: msg -> Element.Attribute msg -}
onClick : Elm.Expression -> Elm.Expression
onClick onClickArg_ =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Events" ]
             , name = "onClick"
             , annotation =
                 Just
                     (Type.function
                          [ Type.var "msg" ]
                          (Type.namedWith
                               [ "Element" ]
                               "Attribute"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ onClickArg_ ]


{-| onDoubleClick: msg -> Element.Attribute msg -}
onDoubleClick : Elm.Expression -> Elm.Expression
onDoubleClick onDoubleClickArg_ =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Events" ]
             , name = "onDoubleClick"
             , annotation =
                 Just
                     (Type.function
                          [ Type.var "msg" ]
                          (Type.namedWith
                               [ "Element" ]
                               "Attribute"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ onDoubleClickArg_ ]


{-| onMouseDown: msg -> Element.Attribute msg -}
onMouseDown : Elm.Expression -> Elm.Expression
onMouseDown onMouseDownArg_ =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Events" ]
             , name = "onMouseDown"
             , annotation =
                 Just
                     (Type.function
                          [ Type.var "msg" ]
                          (Type.namedWith
                               [ "Element" ]
                               "Attribute"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ onMouseDownArg_ ]


{-| onMouseUp: msg -> Element.Attribute msg -}
onMouseUp : Elm.Expression -> Elm.Expression
onMouseUp onMouseUpArg_ =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Events" ]
             , name = "onMouseUp"
             , annotation =
                 Just
                     (Type.function
                          [ Type.var "msg" ]
                          (Type.namedWith
                               [ "Element" ]
                               "Attribute"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ onMouseUpArg_ ]


{-| onMouseEnter: msg -> Element.Attribute msg -}
onMouseEnter : Elm.Expression -> Elm.Expression
onMouseEnter onMouseEnterArg_ =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Events" ]
             , name = "onMouseEnter"
             , annotation =
                 Just
                     (Type.function
                          [ Type.var "msg" ]
                          (Type.namedWith
                               [ "Element" ]
                               "Attribute"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ onMouseEnterArg_ ]


{-| onMouseLeave: msg -> Element.Attribute msg -}
onMouseLeave : Elm.Expression -> Elm.Expression
onMouseLeave onMouseLeaveArg_ =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Events" ]
             , name = "onMouseLeave"
             , annotation =
                 Just
                     (Type.function
                          [ Type.var "msg" ]
                          (Type.namedWith
                               [ "Element" ]
                               "Attribute"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ onMouseLeaveArg_ ]


{-| onMouseMove: msg -> Element.Attribute msg -}
onMouseMove : Elm.Expression -> Elm.Expression
onMouseMove onMouseMoveArg_ =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Events" ]
             , name = "onMouseMove"
             , annotation =
                 Just
                     (Type.function
                          [ Type.var "msg" ]
                          (Type.namedWith
                               [ "Element" ]
                               "Attribute"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ onMouseMoveArg_ ]


{-| onFocus: msg -> Element.Attribute msg -}
onFocus : Elm.Expression -> Elm.Expression
onFocus onFocusArg_ =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Events" ]
             , name = "onFocus"
             , annotation =
                 Just
                     (Type.function
                          [ Type.var "msg" ]
                          (Type.namedWith
                               [ "Element" ]
                               "Attribute"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ onFocusArg_ ]


{-| onLoseFocus: msg -> Element.Attribute msg -}
onLoseFocus : Elm.Expression -> Elm.Expression
onLoseFocus onLoseFocusArg_ =
    Elm.apply
        (Elm.value
             { importFrom = [ "Element", "Events" ]
             , name = "onLoseFocus"
             , annotation =
                 Just
                     (Type.function
                          [ Type.var "msg" ]
                          (Type.namedWith
                               [ "Element" ]
                               "Attribute"
                               [ Type.var "msg" ]
                          )
                     )
             }
        )
        [ onLoseFocusArg_ ]


call_ :
    { onClick : Elm.Expression -> Elm.Expression
    , onDoubleClick : Elm.Expression -> Elm.Expression
    , onMouseDown : Elm.Expression -> Elm.Expression
    , onMouseUp : Elm.Expression -> Elm.Expression
    , onMouseEnter : Elm.Expression -> Elm.Expression
    , onMouseLeave : Elm.Expression -> Elm.Expression
    , onMouseMove : Elm.Expression -> Elm.Expression
    , onFocus : Elm.Expression -> Elm.Expression
    , onLoseFocus : Elm.Expression -> Elm.Expression
    }
call_ =
    { onClick =
        \onClickArg_ ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Events" ]
                     , name = "onClick"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.var "msg" ]
                                  (Type.namedWith
                                       [ "Element" ]
                                       "Attribute"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ onClickArg_ ]
    , onDoubleClick =
        \onDoubleClickArg_ ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Events" ]
                     , name = "onDoubleClick"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.var "msg" ]
                                  (Type.namedWith
                                       [ "Element" ]
                                       "Attribute"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ onDoubleClickArg_ ]
    , onMouseDown =
        \onMouseDownArg_ ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Events" ]
                     , name = "onMouseDown"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.var "msg" ]
                                  (Type.namedWith
                                       [ "Element" ]
                                       "Attribute"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ onMouseDownArg_ ]
    , onMouseUp =
        \onMouseUpArg_ ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Events" ]
                     , name = "onMouseUp"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.var "msg" ]
                                  (Type.namedWith
                                       [ "Element" ]
                                       "Attribute"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ onMouseUpArg_ ]
    , onMouseEnter =
        \onMouseEnterArg_ ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Events" ]
                     , name = "onMouseEnter"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.var "msg" ]
                                  (Type.namedWith
                                       [ "Element" ]
                                       "Attribute"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ onMouseEnterArg_ ]
    , onMouseLeave =
        \onMouseLeaveArg_ ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Events" ]
                     , name = "onMouseLeave"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.var "msg" ]
                                  (Type.namedWith
                                       [ "Element" ]
                                       "Attribute"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ onMouseLeaveArg_ ]
    , onMouseMove =
        \onMouseMoveArg_ ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Events" ]
                     , name = "onMouseMove"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.var "msg" ]
                                  (Type.namedWith
                                       [ "Element" ]
                                       "Attribute"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ onMouseMoveArg_ ]
    , onFocus =
        \onFocusArg_ ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Events" ]
                     , name = "onFocus"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.var "msg" ]
                                  (Type.namedWith
                                       [ "Element" ]
                                       "Attribute"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ onFocusArg_ ]
    , onLoseFocus =
        \onLoseFocusArg_ ->
            Elm.apply
                (Elm.value
                     { importFrom = [ "Element", "Events" ]
                     , name = "onLoseFocus"
                     , annotation =
                         Just
                             (Type.function
                                  [ Type.var "msg" ]
                                  (Type.namedWith
                                       [ "Element" ]
                                       "Attribute"
                                       [ Type.var "msg" ]
                                  )
                             )
                     }
                )
                [ onLoseFocusArg_ ]
    }


values_ :
    { onClick : Elm.Expression
    , onDoubleClick : Elm.Expression
    , onMouseDown : Elm.Expression
    , onMouseUp : Elm.Expression
    , onMouseEnter : Elm.Expression
    , onMouseLeave : Elm.Expression
    , onMouseMove : Elm.Expression
    , onFocus : Elm.Expression
    , onLoseFocus : Elm.Expression
    }
values_ =
    { onClick =
        Elm.value
            { importFrom = [ "Element", "Events" ]
            , name = "onClick"
            , annotation =
                Just
                    (Type.function
                         [ Type.var "msg" ]
                         (Type.namedWith
                              [ "Element" ]
                              "Attribute"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , onDoubleClick =
        Elm.value
            { importFrom = [ "Element", "Events" ]
            , name = "onDoubleClick"
            , annotation =
                Just
                    (Type.function
                         [ Type.var "msg" ]
                         (Type.namedWith
                              [ "Element" ]
                              "Attribute"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , onMouseDown =
        Elm.value
            { importFrom = [ "Element", "Events" ]
            , name = "onMouseDown"
            , annotation =
                Just
                    (Type.function
                         [ Type.var "msg" ]
                         (Type.namedWith
                              [ "Element" ]
                              "Attribute"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , onMouseUp =
        Elm.value
            { importFrom = [ "Element", "Events" ]
            , name = "onMouseUp"
            , annotation =
                Just
                    (Type.function
                         [ Type.var "msg" ]
                         (Type.namedWith
                              [ "Element" ]
                              "Attribute"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , onMouseEnter =
        Elm.value
            { importFrom = [ "Element", "Events" ]
            , name = "onMouseEnter"
            , annotation =
                Just
                    (Type.function
                         [ Type.var "msg" ]
                         (Type.namedWith
                              [ "Element" ]
                              "Attribute"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , onMouseLeave =
        Elm.value
            { importFrom = [ "Element", "Events" ]
            , name = "onMouseLeave"
            , annotation =
                Just
                    (Type.function
                         [ Type.var "msg" ]
                         (Type.namedWith
                              [ "Element" ]
                              "Attribute"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , onMouseMove =
        Elm.value
            { importFrom = [ "Element", "Events" ]
            , name = "onMouseMove"
            , annotation =
                Just
                    (Type.function
                         [ Type.var "msg" ]
                         (Type.namedWith
                              [ "Element" ]
                              "Attribute"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , onFocus =
        Elm.value
            { importFrom = [ "Element", "Events" ]
            , name = "onFocus"
            , annotation =
                Just
                    (Type.function
                         [ Type.var "msg" ]
                         (Type.namedWith
                              [ "Element" ]
                              "Attribute"
                              [ Type.var "msg" ]
                         )
                    )
            }
    , onLoseFocus =
        Elm.value
            { importFrom = [ "Element", "Events" ]
            , name = "onLoseFocus"
            , annotation =
                Just
                    (Type.function
                         [ Type.var "msg" ]
                         (Type.namedWith
                              [ "Element" ]
                              "Attribute"
                              [ Type.var "msg" ]
                         )
                    )
            }
    }