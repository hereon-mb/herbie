module Theme exposing (main)

import Elm
import Gen.CodeGen.Generate as Generate
import Gen.Element
import Json.Decode as Decode exposing (Decoder, Value)


main : Program Value () ()
main =
    Generate.fromJson decoderScheme generate


type alias Scheme =
    List ( String, Color )


type alias Color =
    { r : Int
    , g : Int
    , b : Int
    }


decoderScheme : Decoder Scheme
decoderScheme =
    Decode.keyValuePairs decoderColor


decoderColor : Decoder Color
decoderColor =
    Decode.map3 Color
        (Decode.field "r" Decode.int)
        (Decode.field "g" Decode.int)
        (Decode.field "b" Decode.int)


generate : Scheme -> List Elm.File
generate scheme =
    let
        toColor color =
            Gen.Element.rgb255 color.r color.g color.b
    in
    [ Elm.file [ "Ui", "Theme", "Light" ]
        [ Elm.declaration "light"
            (scheme
                |> List.map (Tuple.mapSecond toColor)
                |> Elm.record
            )
        ]
    ]
