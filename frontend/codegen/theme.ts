import * as CodeGen from "elm-codegen";

import { argbFromHex, themeFromSourceColor, rgbaFromArgb, argbFromRgba, SchemeTonalSpot, Hct } from "@material/material-color-utilities";

const scheme = new SchemeTonalSpot(Hct.fromInt(argbFromHex('#00467d')), false, 0);

// Get the theme from a hex color
const theme = themeFromSourceColor(argbFromHex('#00467d'), [
  {
    value: argbFromRgba({ r: 229, g: 4, b: 70, a: 255}),
    name: "red",
    blend: true,
  },
  {
    value: argbFromRgba({ r: 3, g: 145, b: 160, a: 255 }),
    name: "cyan",
    blend: true,
  },
  {
    value: argbFromRgba({r: 23, g: 169, b: 226, a: 255}),
    name: "blue",
    blend: true,
  },
  {
    value: argbFromRgba({r: 140, g: 90, b: 180, a: 255}),
    name: "hereon-lila",
    blend: true,
  },
  {
    value: argbFromRgba({r: 0, g: 120, b: 210, a: 255}),
    name: "hereon-mittelblau",
    blend: true,
  },
  {
    value: argbFromRgba({r: 150, g: 182, b: 22, a: 255}),
    name: "hereon-gruen",
    blend: true,
  },
  {
    value: argbFromRgba({r: 175, g: 25, b: 60, a: 255}),
    name: "hereon-dunkelrot",
    blend: true,
  },
  {
    value: argbFromRgba({r: 250, g: 115, b: 80, a: 255}),
    name: "hereon-orange",
    blend: true,
  },
  {
    value: argbFromRgba({r: 250, g: 180, b: 35, a: 255}),
    name: "hereon-gelb",
    blend: true,
  },
]);
const red = theme.customColors[0];
const cyan = theme.customColors[1];
const blue = theme.customColors[2];

const hereonLila = theme.customColors[3].light;
const hereonMittelblau = theme.customColors[4].light;
const hereonGruen = theme.customColors[5].light;
const hereonDunkelrot = theme.customColors[6].light;
const hereonOrange = theme.customColors[7].light;
const hereonGelb = theme.customColors[8].light;
const secondary = red.light;

const schemeLight = {
  primary: rgbaFromArgb(theme.schemes.light.primary),
  onPrimary: rgbaFromArgb(theme.schemes.light.onPrimary),
  primaryContainer: rgbaFromArgb(theme.schemes.light.primaryContainer),
  onPrimaryContainer: rgbaFromArgb(theme.schemes.light.onPrimaryContainer),

  secondary: rgbaFromArgb(secondary.color),
  onSecondary: rgbaFromArgb(secondary.onColor),
  secondaryContainer: rgbaFromArgb(secondary.colorContainer),
  onSecondaryContainer: rgbaFromArgb(secondary.onColorContainer),

  tertiary: rgbaFromArgb(theme.schemes.light.tertiary),
  onTertiary: rgbaFromArgb(theme.schemes.light.onTertiary),
  tertiaryContainer: rgbaFromArgb(theme.schemes.light.tertiaryContainer),
  onTertiaryContainer: rgbaFromArgb(theme.schemes.light.onTertiaryContainer.onColorContainer),

  hereonLila: rgbaFromArgb(hereonLila.color),
  onHereonLila: rgbaFromArgb(hereonLila.onColor),
  hereonLilaContainer: rgbaFromArgb(hereonLila.colorContainer),
  onHereonLilaContainer: rgbaFromArgb(hereonLila.onColorContainer),

  hereonMittelblau: rgbaFromArgb(hereonMittelblau.color),
  onHereonMittelblau: rgbaFromArgb(hereonMittelblau.onColor),
  hereonMittelblauContainer: rgbaFromArgb(hereonMittelblau.colorContainer),
  onHereonMittelblauContainer: rgbaFromArgb(hereonMittelblau.onColorContainer),

  hereonGruen: rgbaFromArgb(hereonGruen.color),
  onHereonGruen: rgbaFromArgb(hereonGruen.onColor),
  hereonGruenContainer: rgbaFromArgb(hereonGruen.colorContainer),
  onHereonGruenContainer: rgbaFromArgb(hereonGruen.onColorContainer),

  hereonDunkelrot: rgbaFromArgb(hereonDunkelrot.color),
  onHereonDunkelrot: rgbaFromArgb(hereonDunkelrot.onColor),
  hereonDunkelrotContainer: rgbaFromArgb(hereonDunkelrot.colorContainer),
  onHereonDunkelrotContainer: rgbaFromArgb(hereonDunkelrot.onColorContainer),

  hereonOrange: rgbaFromArgb(hereonOrange.color),
  onHereonOrange: rgbaFromArgb(hereonOrange.onColor),
  hereonOrangeContainer: rgbaFromArgb(hereonOrange.colorContainer),
  onHereonOrangeContainer: rgbaFromArgb(hereonOrange.onColorContainer),

  hereonGelb: rgbaFromArgb(hereonGelb.color),
  onHereonGelb: rgbaFromArgb(hereonGelb.onColor),
  hereonGelbContainer: rgbaFromArgb(hereonGelb.colorContainer),
  onHereonGelbContainer: rgbaFromArgb(hereonGelb.onColorContainer),

  error: rgbaFromArgb(theme.schemes.light.error),
  onError: rgbaFromArgb(theme.schemes.light.onError),
  errorContainer: rgbaFromArgb(theme.schemes.light.errorContainer),
  onErrorContainer: rgbaFromArgb(theme.schemes.light.onErrorContainer),

  background: rgbaFromArgb(theme.schemes.light.background),
  onBackground: rgbaFromArgb(theme.schemes.light.onBackground),

  surfaceDim: rgbaFromArgb(theme.palettes.neutral.tone(87)),
  surface: rgbaFromArgb(theme.schemes.light.surface),
  surfaceBright: rgbaFromArgb(theme.palettes.neutral.tone(98)),
  surfaceContainerLowest: rgbaFromArgb(theme.palettes.neutral.tone(100)),
  surfaceContainerLow: rgbaFromArgb(theme.palettes.neutral.tone(96)),
  surfaceContainer: rgbaFromArgb(theme.palettes.neutral.tone(94)),
  surfaceContainerHigh: rgbaFromArgb(theme.palettes.neutral.tone(92)),
  surfaceContainerHighest: rgbaFromArgb(theme.palettes.neutral.tone(90)),
  onSurface: rgbaFromArgb(theme.schemes.light.onSurface),
  surfaceVariant: rgbaFromArgb(theme.schemes.light.surfaceVariant),
  onSurfaceVariant: rgbaFromArgb(theme.schemes.light.onSurfaceVariant),
  outline: rgbaFromArgb(theme.schemes.light.outline),
  outlineVariant: rgbaFromArgb(theme.schemes.light.outlineVariant),
  shadow: rgbaFromArgb(theme.schemes.light.shadow),
  scrim: rgbaFromArgb(theme.schemes.light.scrim),
  inverseSurface: rgbaFromArgb(theme.schemes.light.inverseSurface),
  inverseOnSurface: rgbaFromArgb(theme.schemes.light.inverseOnSurface),
  inversePrimary: rgbaFromArgb(theme.schemes.light.inversePrimary),
  primarySurfaceContainerHighest: rgbaFromArgb(theme.palettes.primary.tone(90)),
};

const schemeDark = {
  primary: rgbaFromArgb(theme.schemes.dark.primary),
  onPrimary: rgbaFromArgb(theme.schemes.dark.onPrimary),
  primaryContainer: rgbaFromArgb(theme.schemes.dark.primaryContainer),
  onPrimaryContainer: rgbaFromArgb(theme.schemes.dark.onPrimaryContainer),
  secondary: rgbaFromArgb(theme.schemes.dark.secondary),
  onSecondary: rgbaFromArgb(theme.schemes.dark.onSecondary),
  secondaryContainer: rgbaFromArgb(theme.schemes.dark.secondaryContainer),
  onSecondaryContainer: rgbaFromArgb(theme.schemes.dark.onSecondaryContainer),
  tertiary: rgbaFromArgb(theme.schemes.dark.tertiary),
  onTertiary: rgbaFromArgb(theme.schemes.dark.onTertiary),
  tertiaryContainer: rgbaFromArgb(theme.schemes.dark.tertiaryContainer),
  onTertiaryContainer: rgbaFromArgb(theme.schemes.dark.onTertiaryContainer),
  error: rgbaFromArgb(theme.schemes.dark.error),
  onError: rgbaFromArgb(theme.schemes.dark.onError),
  errorContainer: rgbaFromArgb(theme.schemes.dark.errorContainer),
  onErrorContainer: rgbaFromArgb(theme.schemes.dark.onErrorContainer),
  background: rgbaFromArgb(theme.schemes.dark.background),
  onBackground: rgbaFromArgb(theme.schemes.dark.onBackground),
  surfaceDim: rgbaFromArgb(theme.palettes.neutral.tone(6)),
  surface: rgbaFromArgb(theme.schemes.dark.surface),
  surfaceBright: rgbaFromArgb(theme.palettes.neutral.tone(24)),
  surfaceContainerLowest: rgbaFromArgb(theme.palettes.neutral.tone(4)),
  surfaceContainerLow: rgbaFromArgb(theme.palettes.neutral.tone(10)),
  surfaceContainer: rgbaFromArgb(theme.palettes.neutral.tone(12)),
  surfaceContainerHigh: rgbaFromArgb(theme.palettes.neutral.tone(17)),
  surfaceContainerHighest: rgbaFromArgb(theme.palettes.neutral.tone(24)),
  onSurface: rgbaFromArgb(theme.schemes.dark.onSurface),
  surfaceVariant: rgbaFromArgb(theme.schemes.dark.surfaceVariant),
  onSurfaceVariant: rgbaFromArgb(theme.schemes.dark.onSurfaceVariant),
  outline: rgbaFromArgb(theme.schemes.dark.outline),
  outlineVariant: rgbaFromArgb(theme.schemes.dark.outlineVariant),
  shadow: rgbaFromArgb(theme.schemes.dark.shadow),
  scrim: rgbaFromArgb(theme.schemes.dark.scrim),
  inverseSurface: rgbaFromArgb(theme.schemes.dark.inverseSurface),
  inverseOnSurface: rgbaFromArgb(theme.schemes.dark.inverseOnSurface),
  inversePrimary: rgbaFromArgb(theme.schemes.dark.inversePrimary),
};


CodeGen.run("Theme.elm", {
  debug: true,
  output: "generated",
  flags: schemeLight,
  cwd: "./codegen"
});
