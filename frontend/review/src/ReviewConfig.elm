module ReviewConfig exposing (config)

{-| Do not rename the ReviewConfig module or the config function, because
`elm-review` will look for these.

To add packages that contain rules, add them to this review project using

    `elm install author/packagename`

when inside the directory containing this file.

-}

import CognitiveComplexity
import NoExposingEverything
import NoImportingEverything
import NoMissingSubscriptionsCall
import NoMissingTypeAnnotation
import NoMissingTypeExpose
import NoRecursiveUpdate
import NoUnused.CustomTypeConstructorArgs
import NoUnused.CustomTypeConstructors
import NoUnused.Dependencies
import NoUnused.Exports
import NoUnused.Modules
import NoUnused.Parameters
import NoUnused.Patterns
import NoUnused.Variables
import Review.Rule exposing (Rule)


config : List Rule
config =
    List.map
        (Review.Rule.ignoreErrorsForDirectories
            [ "elm-wai-aria-listbox"
            , "generated"
            , "codegen/Gen"
            , "elm-rdf/src/"
            ]
        )
        [ NoExposingEverything.rule
        , NoImportingEverything.rule []
            |> Review.Rule.ignoreErrorsForFiles
                [ "tests/ShaclTest.elm"
                , "tests/OntologyTest.elm"
                , "tests/RdfTest.elm"
                ]
        , NoMissingTypeAnnotation.rule
            |> Review.Rule.ignoreErrorsForDirectories
                [ ".elm-spa/generated"
                ]
        , NoMissingTypeExpose.rule
        , NoMissingSubscriptionsCall.rule
            |> Review.Rule.ignoreErrorsForDirectories
                [ "tests/Pages" ]
            |> Review.Rule.ignoreErrorsForFiles
                [ "src/Ui/Molecule/DropdownCustomElement.elm"
                , "src/Ui/Atom/Listbox.elm"
                ]
        , NoRecursiveUpdate.rule
        , NoUnused.CustomTypeConstructors.rule []
            |> Review.Rule.ignoreErrorsForFiles
                [ "src/Shacl/Form/InputFileUpload.elm"
                , "src/Shacl/Form/Constraint.elm"
                ]
        , NoUnused.CustomTypeConstructorArgs.rule
            |> Review.Rule.ignoreErrorsForFiles
                [ "src/Shacl.elm"
                , "src/Shacl/Form/Decode/Pipeline.elm" -- FIXME add serialization of Error type
                , "src/Rdf/Store.elm" -- FIXME render the error somewhere
                , "src/Api.elm"
                , "tests/Shacl/Form/MergedTest.elm"
                , "src/Shacl/Form/Merged.elm"
                ]
        , NoUnused.Dependencies.rule
        , NoUnused.Exports.rule
            |> Review.Rule.ignoreErrorsForDirectories
                [ ".elm-spa/generated"
                ]
            |> Review.Rule.ignoreErrorsForFiles
                [ "src/Accessibility/Keyboard.elm"
                , "src/Effect.elm"
                , "src/Rdf/Store.elm"
                , "src/Shacl/Documentation.elm"
                , "src/Shacl/Form.elm"
                , "src/Shacl/Form/Viewed.elm"
                , "src/Spa/Page.elm"
                , "src/Test/Html/Query/Extra.elm"
                , "src/Toast.elm"
                , "src/Ui/Theme/Elevation.elm"
                , "src/Ui/Molecule/Tabs.elm"
                , "src/Util/Route.elm"
                , "src/Rdf/SetIri.elm"
                , "tests/SpaTest.elm"
                ]
        , NoUnused.Variables.rule
            |> Review.Rule.ignoreErrorsForDirectories
                [ ".elm-spa/generated"
                ]
        , NoUnused.Modules.rule
            |> Review.Rule.ignoreErrorsForFiles
                [ "tests/SpaTest.elm"
                , "src/Ui/Molecule/Tabs.elm"
                ]
        , NoUnused.Parameters.rule
            |> Review.Rule.ignoreErrorsForFiles
                [ ".elm-spa/defaults/Auth.elm"
                ]
        , NoUnused.Patterns.rule

        -- FIXME This does not seem to finish
        -- , CognitiveComplexity.rule 81
        ]
