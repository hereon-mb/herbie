time-distance--about-n-hours = ungefähr { time-distance--n-hours }
time-distance--about-n-hours-ago = vor ungefähr { time-distance--n-hours--dative }
time-distance--about-n-months = ungefähr { time-distance--n-months }
time-distance--about-n-months-ago = vor ungefähr { time-distance--n-months--dative }
time-distance--about-n-years = ungefähr { time-distance--n-years }
time-distance--about-n-years-ago = vor ungefähr { time-distance--n-years--dative }
time-distance--almost-n-years = fast { time-distance--n-years }
time-distance--almost-n-years-ago = vor fast { time-distance--n-years--dative }
time-distance--half-a-minute = eine halbe Minute
time-distance--half-a-minute-ago = vor einer halben Minute
time-distance--in-about-n-hours = in ungefähr { time-distance--n-hours--dative }
time-distance--in-about-n-months = in ungefähr { time-distance--n-months--dative }
time-distance--in-about-n-years = in ungefähr { time-distance--n-years--dative }
time-distance--in-almost-n-years = in fast { time-distance--n-years--dative }
time-distance--in-half-a-minute = in einer halben Minute
time-distance--in-less-than-n-minutes = in weniger als { time-distance--n-minutes--dative }
time-distance--in-less-than-n-seconds = in weniger als { time-distance--n-seconds--dative }
time-distance--in-n-days = in { time-distance--n-days--dative }
time-distance--in-n-minutes = in { time-distance--n-minutes--dative }
time-distance--in-n-months = in { time-distance--n-months--dative }
time-distance--in-n-years = in { time-distance--n-years--dative }
time-distance--in-over-n-years = in mehr als { time-distance--n-years--dative }
time-distance--less-than-n-minutes = weniger als { time-distance--n-minutes }
time-distance--less-than-n-minutes-ago = vor weniger als { time-distance--n-minutes--dative }
time-distance--less-than-n-seconds = weniger als { time-distance--n-seconds }
time-distance--less-than-n-seconds-ago = vor weniger als { time-distance--n-seconds--dative }
time-distance--n-days =
    { $days ->
        [one] ein Tag
       *[other] { $days } Tage
    }
time-distance--n-days--dative =
    { $days ->
        [one] einem Tag
       *[other] { $days } Tagen
    }
time-distance--n-days-ago = vor { time-distance--n-days--dative }
time-distance--n-hours =
    { $hours ->
        [one] eine Stunde
       *[other] { $hours } Stunden
    }
time-distance--n-hours--dative =
    { $hours ->
        [one] einer Stunde
       *[other] { $hours } Stunden
    }
time-distance--n-minutes =
    { $minutes ->
        [one] eine Minute
       *[other] { $minutes } Minuten
    }
time-distance--n-minutes--dative =
    { $minutes ->
        [one] einer Minute
       *[other] { $minutes } Minuten
    }
time-distance--n-minutes-ago = vor { time-distance--n-minutes--dative }
time-distance--n-months =
    { $months ->
        [one] ein Monat
       *[other] { $months } Monate
    }
time-distance--n-months--dative =
    { $months ->
        [one] einem Monat
       *[other] { $months } Monaten
    }
time-distance--n-months-ago = vor { time-distance--n-months--dative }
time-distance--n-seconds =
    { $seconds ->
        [one] eine Sekunde
       *[other] { $seconds } Sekunden
    }
time-distance--n-seconds--dative =
    { $seconds ->
        [one] einer Sekunde
       *[other] { $seconds } Sekunden
    }
time-distance--n-years =
    { $years ->
        [one] ein Jahr
       *[other] { $years } Jahre
    }
time-distance--n-years--dative =
    { $years ->
        [one] einem Jahr
       *[other] { $years } Jahren
    }
time-distance--n-years-ago = vor { time-distance--n-years--dative }
time-distance--over-n-years = mehr als { time-distance--n-years }
time-distance--over-n-years-ago = vor mehr als { time-distance--n-years--dative }
