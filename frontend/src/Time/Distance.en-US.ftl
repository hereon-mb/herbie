time-distance--about-n-hours =
    about { $hours ->
        [one] one hour
       *[other] { $hours } hours
    }
time-distance--about-n-hours-ago = { time-distance--about-n-hours } ago
time-distance--about-n-months = about { time-distance--n-months }
time-distance--about-n-months-ago = { time-distance--about-n-months } ago
time-distance--about-n-years = about { time-distance--n-years }
time-distance--about-n-years-ago = { time-distance--about-n-years } ago
time-distance--almost-n-years = almost { time-distance--n-years }
time-distance--almost-n-years-ago = { time-distance--almost-n-years } ago
time-distance--half-a-minute = half a minute
time-distance--half-a-minute-ago = { time-distance--half-a-minute } ago
time-distance--in-about-n-hours = in { time-distance--about-n-hours }
time-distance--in-about-n-months = in { time-distance--about-n-months }
time-distance--in-about-n-years = in { time-distance--about-n-years }
time-distance--in-almost-n-years = in { time-distance--almost-n-years }
time-distance--in-half-a-minute = in { time-distance--half-a-minute }
time-distance--in-less-than-n-minutes = in { time-distance--less-than-n-minutes }
time-distance--in-less-than-n-seconds = in { time-distance--less-than-n-seconds }
time-distance--in-n-days = in { time-distance--n-days }
time-distance--in-n-minutes = in { time-distance--n-minutes }
time-distance--in-n-months = in { time-distance--n-months }
time-distance--in-n-years = in { time-distance--n-years }
time-distance--in-over-n-years = in { time-distance--over-n-years }
time-distance--less-than-n-minutes =
    less than { $minutes ->
        [one] a minute
       *[other] { $minutes } minutes
    }
time-distance--less-than-n-minutes-ago = { time-distance--less-than-n-minutes } ago
time-distance--less-than-n-seconds =
    less than { $seconds ->
        [one] one second
       *[other] { $seconds } seconds
    }
time-distance--less-than-n-seconds-ago = { time-distance--less-than-n-seconds } ago
time-distance--n-days =
    { $days ->
        [one] one day
       *[other] { $days } days
    }
time-distance--n-days-ago = { time-distance--n-days } ago
time-distance--n-minutes =
    { $minutes ->
        [one] one minute
       *[other] { $minutes } minutes
    }
time-distance--n-minutes-ago = { time-distance--n-minutes } ago
time-distance--n-months =
    { $months ->
        [one] one month
       *[other] { $months } months
    }
time-distance--n-months-ago = { time-distance--n-months } ago
time-distance--n-years =
    { $years ->
        [one] one year
       *[other] { $years } years
    }
time-distance--n-years-ago = { time-distance--n-years } ago
time-distance--over-n-years = over { time-distance--n-years }
time-distance--over-n-years-ago = { time-distance--over-n-years } ago
