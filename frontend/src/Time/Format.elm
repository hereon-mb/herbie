{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Time.Format exposing
    ( Format(..)
    , fromLocale
    , posixFromDate
    , posixFromDateTime
    , posixToDate
    , posixToDateTime
    )

import Derberos.Date.Core
import Derberos.Date.Utils
import Locale exposing (Locale)
import Maybe.Pipeline as Maybe
import Time exposing (Posix)


type Format
    = Iso8601
    | EnUs
    | De


fromLocale : Locale -> Format
fromLocale locale =
    case locale of
        Locale.EnUS ->
            EnUs

        Locale.De ->
            De



-- DATE FROM STRING


posixFromDate : Format -> Time.Zone -> String -> Maybe Posix
posixFromDate format zone string =
    case format of
        Iso8601 ->
            posixFromDateIso8601 zone string

        EnUs ->
            posixFromDateEnUS zone string

        De ->
            posixFromDateDe zone string


posixFromDateIso8601 : Time.Zone -> String -> Maybe Posix
posixFromDateIso8601 zone string =
    case String.split "-" string of
        rawYear :: rawMonth :: rawDay :: [] ->
            fromRawDate zone rawYear rawMonth rawDay

        _ ->
            Nothing


posixFromDateEnUS : Time.Zone -> String -> Maybe Posix
posixFromDateEnUS zone string =
    case String.split "/" string of
        rawMonth :: rawDay :: rawYear :: [] ->
            fromRawDate zone rawYear rawMonth rawDay

        _ ->
            Nothing


posixFromDateDe : Time.Zone -> String -> Maybe Posix
posixFromDateDe zone string =
    case String.split "." string of
        rawDay :: rawMonth :: rawYear :: [] ->
            fromRawDate zone rawYear rawMonth rawDay

        _ ->
            Nothing


fromRawDate : Time.Zone -> String -> String -> String -> Maybe Posix
fromRawDate zone rawYear rawMonth rawDay =
    case
        ( String.toInt rawMonth
        , String.toInt rawDay
        , String.toInt rawYear
        )
    of
        ( Just month, Just day, Just year ) ->
            Just
                (Derberos.Date.Core.civilToPosix
                    { year = year
                    , month = month
                    , day = day
                    , hour = 0
                    , minute = 0
                    , second = 0
                    , millis = 0
                    , zone = zone
                    }
                )

        _ ->
            Nothing



-- DATE TO STRING


posixToDate : Format -> Time.Zone -> Posix -> String
posixToDate format zone date =
    let
        day =
            Time.toDay zone date

        month =
            Derberos.Date.Utils.monthToNumber1 (Time.toMonth zone date)

        year =
            Time.toYear zone date
    in
    case format of
        Iso8601 ->
            String.join "-"
                [ String.fromInt year
                , toPaddedInt month
                , toPaddedInt day
                ]

        EnUs ->
            String.join "/"
                (List.map String.fromInt
                    [ month
                    , day
                    , year
                    ]
                )

        De ->
            String.join "."
                (List.map String.fromInt
                    [ day
                    , month
                    , year
                    ]
                )



-- DATE TIME FROM STRING


posixFromDateTime : Format -> Time.Zone -> String -> Maybe Posix
posixFromDateTime format zone string =
    case format of
        Iso8601 ->
            posixFromDateTimeIso8601 zone string

        EnUs ->
            posixFromDateTimeEnUS zone string

        De ->
            posixFromDateTimeDe zone string


posixFromDateTimeIso8601 : Time.Zone -> String -> Maybe Posix
posixFromDateTimeIso8601 zone string =
    case String.split "T" string of
        rawDate :: rawTime :: [] ->
            case
                ( String.split "-" rawDate
                , String.split ":" rawTime
                )
            of
                ( rawYear :: rawMonth :: rawDay :: [], rawHour :: rawMinute :: rawSecond :: [] ) ->
                    fromRawDateTime zone 0 rawYear rawMonth rawDay rawHour rawMinute rawSecond

                _ ->
                    Nothing

        _ ->
            Nothing


posixFromDateTimeEnUS : Time.Zone -> String -> Maybe Posix
posixFromDateTimeEnUS zone string =
    case String.words string of
        rawDate :: rawTime :: rawPeriod :: [] ->
            if
                (String.right 1 rawDate == ",")
                    && (String.toUpper rawPeriod == "AM" || String.toUpper rawPeriod == "PM")
            then
                let
                    offsetHour =
                        case String.toUpper rawPeriod of
                            "AM" ->
                                0

                            "PM" ->
                                12

                            _ ->
                                0
                in
                case
                    ( String.split "/" (String.dropRight 1 rawDate)
                    , String.split ":" rawTime
                    )
                of
                    ( rawMonth :: rawDay :: rawYear :: [], rawHour :: rawMinute :: rawSecond :: [] ) ->
                        fromRawDateTime zone
                            offsetHour
                            rawYear
                            rawMonth
                            rawDay
                            (if rawHour == "12" then
                                "0"

                             else
                                rawHour
                            )
                            rawMinute
                            rawSecond

                    ( rawMonth :: rawDay :: rawYear :: [], rawHour :: rawMinute :: [] ) ->
                        fromRawDateTime zone
                            offsetHour
                            rawYear
                            rawMonth
                            rawDay
                            (if rawHour == "12" then
                                "0"

                             else
                                rawHour
                            )
                            rawMinute
                            "0"

                    _ ->
                        Nothing

            else
                Nothing

        _ ->
            Nothing


posixFromDateTimeDe : Time.Zone -> String -> Maybe Posix
posixFromDateTimeDe zone string =
    case String.words string of
        rawDate :: rawTime :: [] ->
            if String.right 1 rawDate == "," then
                case
                    ( String.split "." (String.dropRight 1 rawDate)
                    , String.split ":" rawTime
                    )
                of
                    ( rawDay :: rawMonth :: rawYear :: [], rawHour :: rawMinute :: rawSecond :: [] ) ->
                        fromRawDateTime zone 0 rawYear rawMonth rawDay rawHour rawMinute rawSecond

                    ( rawDay :: rawMonth :: rawYear :: [], rawHour :: rawMinute :: [] ) ->
                        fromRawDateTime zone 0 rawYear rawMonth rawDay rawHour rawMinute "0"

                    _ ->
                        Nothing

            else
                Nothing

        _ ->
            Nothing


fromRawDateTime :
    Time.Zone
    -> Int
    -> String
    -> String
    -> String
    -> String
    -> String
    -> String
    -> Maybe Posix
fromRawDateTime zone offsetHour rawYear rawMonth rawDay rawHour rawMinute rawSecond =
    Just
        (\year month day hour minute second ->
            Derberos.Date.Core.civilToPosix
                { year = year
                , month = month
                , day = day
                , hour = hour + offsetHour
                , minute = minute
                , second = second
                , millis = 0
                , zone = zone
                }
        )
        |> Maybe.required (String.toInt rawYear)
        |> Maybe.required (String.toInt rawMonth)
        |> Maybe.required (String.toInt rawDay)
        |> Maybe.required (String.toInt rawHour)
        |> Maybe.required (String.toInt rawMinute)
        |> Maybe.required (String.toInt rawSecond)



-- POSIX TO DATE TIME


posixToDateTime : Format -> Time.Zone -> Posix -> String
posixToDateTime format zone posix =
    let
        year =
            Time.toYear zone posix

        month =
            Derberos.Date.Utils.monthToNumber1 (Time.toMonth zone posix)

        day =
            Time.toDay zone posix

        hour =
            Time.toHour zone posix

        minute =
            Time.toMinute zone posix

        second =
            Time.toSecond zone posix
    in
    case format of
        Iso8601 ->
            String.join "T"
                [ String.join "-"
                    [ String.fromInt year
                    , toPaddedInt month
                    , toPaddedInt day
                    ]
                , String.join ":"
                    [ toPaddedInt hour
                    , toPaddedInt minute
                    , toPaddedInt second
                    ]
                ]

        EnUs ->
            String.concat
                [ String.join "/"
                    (List.map String.fromInt
                        [ month
                        , day
                        , year
                        ]
                    )
                , ", "
                , String.join ":"
                    [ String.fromInt
                        (case modBy 12 hour of
                            0 ->
                                12

                            theHour ->
                                theHour
                        )
                    , toPaddedInt minute
                    ]
                , " "
                , if 0 <= hour && hour < 12 then
                    "AM"

                  else
                    "PM"
                ]

        De ->
            String.concat
                [ String.join "."
                    (List.map String.fromInt
                        [ day
                        , month
                        , year
                        ]
                    )
                , ", "
                , String.join ":"
                    [ String.fromInt hour
                    , toPaddedInt minute
                    ]
                ]



-- HELPER


toPaddedInt : Int -> String
toPaddedInt int =
    if int < 10 then
        "0" ++ String.fromInt int

    else
        String.fromInt int
