{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Time.Gap exposing (fromPosix)

import Time


{-| Computes gaps between provided time stamps. Assumes times are ordered chronologically.
-}
fromPosix : List Time.Posix -> List Int
fromPosix times =
    case times of
        [] ->
            []

        _ :: [] ->
            []

        first :: second :: rest ->
            (Time.posixToMillis second - Time.posixToMillis first) :: fromPosix (second :: rest)
