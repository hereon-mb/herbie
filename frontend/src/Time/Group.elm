{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Time.Group exposing (byDay)

{-|

@docs byDay

-}

import List.Extra as List
import Time
import Time.Extra


{-| Group a list values by day.
-}
byDay : (a -> Time.Posix) -> Bool -> Time.Zone -> List a -> List ( Time.Posix, List ( Time.Posix, a ) )
byDay toPosix newestLast zone listA =
    let
        pairWithPosix a =
            ( toPosix a, a )
    in
    listA
        |> List.map pairWithPosix
        |> sortByPosix newestLast
        |> List.groupWhile (onTheSameDay zone)
        |> List.map pairGroupWithPosix
        |> sortByPosix newestLast


pairGroupWithPosix : ( ( a, b ), List ( a, b ) ) -> ( a, List ( a, b ) )
pairGroupWithPosix ( a, listA ) =
    ( Tuple.first a
    , a :: listA
    )


sortByPosix : Bool -> List ( Time.Posix, a ) -> List ( Time.Posix, a )
sortByPosix newestLast =
    if newestLast then
        List.sortBy (Tuple.first >> Time.posixToMillis)

    else
        List.sortBy (Tuple.first >> Time.posixToMillis >> negate)


onTheSameDay : Time.Zone -> ( Time.Posix, a ) -> ( Time.Posix, a ) -> Bool
onTheSameDay zone ( publishedAtA, _ ) ( publishedAtB, _ ) =
    dayFromPosix zone publishedAtA == dayFromPosix zone publishedAtB


dayFromPosix : Time.Zone -> Time.Posix -> Time.Posix
dayFromPosix zone =
    Time.Extra.floor Time.Extra.Day zone
