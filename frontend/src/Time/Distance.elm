{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Time.Distance exposing (inWordsWithAffix)

{-| This code was taken from
[gingko/time-distance](https://github.com/gingko/time-distance) and adapted.
The original code was licensed under

    Copyright 2019-present, Adriano Ferrari

    Permission is hereby granted, free of charge, to any person obtaining
    a copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including without
    limitation the rights to use, copy, modify, merge, publish, distribute,
    sublicense, and/or sell copies of the Software, and to permit persons to
    whom the Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.

-}

import Fluent
    exposing
        ( Fluent
        , int
        )
import Time


minutes_in_day : number
minutes_in_day =
    1440


minutes_in_almost_two_days : number
minutes_in_almost_two_days =
    2520


minutes_in_month : number
minutes_in_month =
    43200


minutes_in_two_months : number
minutes_in_two_months =
    86400


type DistanceId
    = LessThanXSeconds Int
    | HalfAMinute
    | LessThanXMinutes Int
    | XMinutes Int
    | AboutXHours Int
    | XDays Int
    | AboutXMonths Int
    | XMonths Int
    | AboutXYears Int
    | OverXYears Int
    | AlmostXYears Int


type Tense
    = Past
    | Future


inWordsWithAffix : Time.Posix -> Time.Posix -> Fluent
inWordsWithAffix =
    inWordsHelp True


inWordsHelp : Bool -> Time.Posix -> Time.Posix -> Fluent
inWordsHelp withAffix otherTime currentTime =
    let
        currentSeconds =
            currentTime
                |> Time.posixToMillis

        otherSeconds =
            otherTime
                |> Time.posixToMillis

        tense =
            case compare currentSeconds otherSeconds of
                GT ->
                    Past

                _ ->
                    Future

        distance =
            calculateDistance True otherSeconds currentSeconds

        addAffix id =
            if withAffix then
                case tense of
                    Past ->
                        id ++ "-ago"

                    Future ->
                        "in-" ++ id

            else
                id
    in
    case distance of
        LessThanXSeconds i ->
            tr [ int "seconds" i ] (addAffix "less-than-n-seconds")

        HalfAMinute ->
            t (addAffix "half-a-minute")

        LessThanXMinutes i ->
            tr [ int "minutes" i ] (addAffix "less-than-n-minutes")

        XMinutes i ->
            tr [ int "minutes" i ] (addAffix "n-minutes")

        AboutXHours i ->
            tr [ int "hours" i ] (addAffix "about-n-hours")

        XDays i ->
            tr [ int "days" i ] (addAffix "n-days")

        AboutXMonths i ->
            tr [ int "months" i ] (addAffix "about-n-months")

        XMonths i ->
            tr [ int "months" i ] (addAffix "n-months")

        AboutXYears i ->
            tr [ int "years" i ] (addAffix "about-n-years")

        OverXYears i ->
            tr [ int "years" i ] (addAffix "over-n-years")

        AlmostXYears i ->
            tr [ int "years" i ] (addAffix "almost-n-years")


upToOneMinute : Int -> DistanceId
upToOneMinute seconds =
    if seconds < 5 then
        LessThanXSeconds 5

    else if seconds < 10 then
        LessThanXSeconds 10

    else if seconds < 20 then
        LessThanXSeconds 20

    else if seconds < 40 then
        HalfAMinute

    else if seconds < 60 then
        LessThanXMinutes 1

    else
        XMinutes 1


upToOneDay : Int -> DistanceId
upToOneDay minutes =
    let
        hours =
            round <| toFloat minutes / 60
    in
    AboutXHours hours


upToOneMonth : Int -> DistanceId
upToOneMonth minutes =
    let
        days =
            round <| toFloat minutes / minutes_in_day
    in
    XDays days


upToTwoMonths : Int -> DistanceId
upToTwoMonths minutes =
    let
        months =
            round <| toFloat minutes / minutes_in_month
    in
    AboutXMonths months


upToOneYear : Int -> DistanceId
upToOneYear minutes =
    let
        nearestMonth =
            round <| toFloat minutes / minutes_in_month
    in
    XMonths nearestMonth


moreThanTwoMonths : Int -> DistanceId
moreThanTwoMonths minutes =
    let
        months =
            minutes // minutes_in_month
    in
    if months < 12 then
        -- 2 months up to 12 months
        upToOneYear minutes

    else
        -- 1 year up to max Date
        let
            monthsSinceStartOfYear =
                modBy 12 months

            years =
                floor <| toFloat months / 12
        in
        if monthsSinceStartOfYear < 3 then
            -- N years up to 1 years 3 months
            AboutXYears years

        else if monthsSinceStartOfYear < 9 then
            -- N years 3 months up to N years 9 months
            OverXYears years

        else
            -- N years 9 months up to N year 12 months
            AlmostXYears <| years + 1


calculateDistance :
    Bool
    -> Int
    -> Int
    -> DistanceId
calculateDistance includeSeconds s1 s2 =
    let
        seconds =
            abs (s2 - s1) // 1000

        minutes =
            round <| toFloat seconds / 60
    in
    if includeSeconds && minutes < 2 then
        upToOneMinute seconds

    else if minutes == 0 then
        LessThanXMinutes 1

    else if minutes < 2 then
        XMinutes minutes

    else if minutes < 45 then
        -- 2 mins up to 0.75 hrs
        XMinutes minutes

    else if minutes < 90 then
        -- 0.75 hrs up to 1.5 hrs
        AboutXHours 1

    else if minutes < minutes_in_day then
        -- 1.5 hrs up to 24 hrs
        upToOneDay minutes

    else if minutes < minutes_in_almost_two_days then
        -- 1 day up to 1.75 days
        XDays 1

    else if minutes < minutes_in_month then
        -- 1.75 days up to 30 days
        upToOneMonth minutes

    else if minutes < minutes_in_two_months then
        -- 1 month up to 2 months
        upToTwoMonths minutes

    else
        moreThanTwoMonths minutes



---- FLUENT


t : String -> Fluent
t key =
    Fluent.text ("time-distance--" ++ key)


tr : List Fluent.Argument -> String -> Fluent
tr args key =
    Fluent.textWith args ("time-distance--" ++ key)
