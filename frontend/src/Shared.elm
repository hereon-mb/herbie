{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Shared exposing
    ( Model, Callbacks, Dialog(..)
    , toContext
    , sandboxed
    , updateUrl
    , init, Flags, initDebug
    , update, Msg, msgFromAction
    , subscriptions
    , view
    , Data, ErrorTask, MsgTask, Pool, TaskOutput
    )

{-|

@docs Model, Callbacks, Dialog

@docs toContext
@docs draggingWorkspace, sandboxed
@docs updateUrl

@docs init, Flags, initDebug
@docs update, Msg, msgFromAction
@docs subscriptions
@docs view

-}

import Action exposing (Action(..))
import Action.DatasetCreate as DatasetCreate
import Api
import Api.Author as Author exposing (Author)
import Api.Data as ApiData
import Api.Datasets as Datasets
import Api.Graph as Graph
import Api.Instance as Instance
import Api.Me as Me exposing (AuthorDetail)
import Api.Me.Preferences as Preferences exposing (Preferences)
import Api.Triples as Triples
import Api.Workspace as Workspace exposing (Workspace)
import Browser.Dom as Dom
import Browser.Navigation
import ConcurrentTask exposing (ConcurrentTask)
import Context exposing (C)
import Dict exposing (Dict)
import Effect exposing (Effect)
import Element
    exposing
        ( DeviceClass(..)
        , Element
        , Orientation(..)
        , alignRight
        , centerX
        , centerY
        , classifyDevice
        , column
        , el
        , fill
        , fillPortion
        , height
        , map
        , none
        , padding
        , paddingXY
        , px
        , row
        , spacing
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Extra
    exposing
        ( columnWithAncestorWithScrollbarX
        , elWithAncestorWithScrollbarY
        , elWithAncestorWithScrollbars
        , id
        , pointerEvents
        , rowWithAncestorWithScrollbarY
        , stopPropagationOnClick
        , userSelect
        )
import Element.Font as Font
import Element.Keyed as Keyed
import Element.Lazy as Lazy
    exposing
        ( lazy3
        )
import ElmSpa.Request
import Event exposing (Event)
import Fluent
    exposing
        ( Fluent
        , verbatim
        )
import Gen.Route as Route exposing (Route)
import Href
import Http
import Json.Decode as Decode
import Json.Encode as Encode
import List.Extra as List
import List.NonEmpty as NonEmpty
import Locale
import Localstorage
import Maybe.Extra as Maybe
import Ontology.Herbie as Herbie exposing (herbie)
import Ontology.Instance exposing (Instance)
import Ports
import Random
import Rdf exposing (BlankNodeOrIriOrAnyLiteral, Iri)
import Rdf.Decode
import Rdf.DictIri as DictIri exposing (DictIri)
import Rdf.DictIri.ApiData as DictIriApiData exposing (DictIriApiData)
import Rdf.Extra as Rdf
import Rdf.Graph as Rdf exposing (Graph)
import Rdf.Mint as Mint
import Rdf.Namespaces exposing (a, dcterms)
import Rdf.Namespaces.OWL as OWL
import Rdf.Query as Rdf
import Rdf.SetIri as SetIri exposing (SetIri)
import Rdf.Store as Store
import RemoteData
import Shacl
import Shacl.Extra as Shacl
import Shacl.Form.Breadcrumbs exposing (Path)
import Shacl.Form.Decoded as Decoded
import Shacl.Form.Grouped as Grouped
import Shared.Workspace
import String.Extra as String
import Task
import Time
import TimeZone
import Toast
import UUID
import Ui.Atom.ButtonIcon as ButtonIcon exposing (buttonIcon)
import Ui.Atom.Icon as Icon
import Ui.Atom.Portal as Portal
import Ui.Atom.ProgressIndicatorLinear as ProgressIndicatorLinear
import Ui.Atom.Scrolled as Scrolled
import Ui.Atom.Tooltip as Tooltip
import Ui.Molecule.Breadcrumbs as Breadcrumbs exposing (Breadcrumbs)
import Ui.Molecule.DialogHelp as DialogHelp
import Ui.Molecule.Navigation as Navigation
import Ui.Molecule.NavigationRail as NavigationRail
import Ui.Theme.Color as Color
    exposing
        ( scrim
        , surface
        , surfaceContainerHigh
        )
import Ui.Theme.Logo as Logo
import Url exposing (Url)
import Url.Builder
import View exposing (View)
import Viewport exposing (Viewport)



-- MODEL


type Model
    = Model Data


type alias Data =
    { tray : Toast.Tray
    , context : C
    , seeds : UUID.Seeds

    -- NAVIGATION
    , showHelp : Bool
    , helpOffsets : Scrolled.Offsets

    -- WORKSPACE
    , workspace : Shared.Workspace.Model
    , dialog : Dialog

    -- CACHE
    , instances : DictIriApiData Api.Error (List Instance)
    , graphsClosureLoading :
        DictIri
            { graphsRequested : SetIri
            , graphsReceived : SetIri
            , graph : Graph
            }
    , triplesPrev : Maybe String
    , triplesNext : Maybe String
    , fetchingTriplesNext : Bool
    , fetchingTriplesNextNeeded : Bool

    -- GRAPHS
    , graphs : DictIriApiData Api.Error Graph
    , graphsClosure : DictIriApiData Api.Error Graph

    -- SHACL
    , creatingFields : Bool
    , duplicatingFields : Bool
    }


type Callbacks msgMain
    = Callbacks
        { getGraphs : DictIri (List (Api.Response Graph -> msgMain))
        , getGraphsClosure : DictIri (List (Api.Response Graph -> msgMain))
        , getTrigs : DictIri (List (Api.Response (DictIri Graph) -> msgMain))
        , instances : DictIri (List (Api.Response (List Instance) -> msgMain))

        -- SHACL
        , createdFields :
            List
                { preview : Bool
                , path : Path
                , mint : Decoded.Mint
                , field : Grouped.Field
                , onField : Api.Response Decoded.Field -> msgMain
                }
        , duplicatedFields :
            List
                { preview : Bool
                , path : Path
                , mint : Decoded.Mint
                , field : Decoded.Field
                , onField : Api.Response Decoded.Field -> msgMain
                }

        -- CONCURRENT TASKS
        , pool : Pool msgMain
        }


type Dialog
    = NoDialog


toContext : Model -> C
toContext (Model data) =
    data.context


sandboxed : Model -> Bool
sandboxed (Model data) =
    data.context.sandbox



-- INIT


type alias Flags =
    { sandbox : Bool
    , useTripleStore : Bool
    , useHelmholtzAai : Bool
    , hideUserPaswordLogin : Bool
    , useSparklis : Bool
    , allowUploads : Bool
    , imprintUrl : Maybe String
    , dataProtectionUrl : Maybe String
    , accessibilityUrl : Maybe String
    , csrfToken : String
    , fluentBundles : Fluent.Bundles
    , rawLocale : String
    }


init : Browser.Navigation.Key -> Url -> Flags -> AuthorDetail -> ( Model, Callbacks msgMain, Effect (Msg msgMain) )
init key url flags author =
    let
        context =
            initContext key url flags author

        ( workspace, effectWorkspace ) =
            Shared.Workspace.init context

        ( pool, effectCache ) =
            initCache context ConcurrentTask.pool
    in
    ( Model
        { tray = Toast.tray
        , context = context
        , seeds =
            -- FIXME make this random
            { seed1 = Random.initialSeed 1
            , seed2 = Random.initialSeed 2
            , seed3 = Random.initialSeed 3
            , seed4 = Random.initialSeed 4
            }

        -- NAVIGATION
        , showHelp = False
        , helpOffsets = Scrolled.initialOffsets

        -- WORKSPACE
        , workspace = workspace
        , dialog = NoDialog

        -- CACHE
        , instances = DictIriApiData.empty
        , graphsClosureLoading = DictIri.empty
        , triplesPrev = Nothing
        , triplesNext = Nothing
        , fetchingTriplesNext = False
        , fetchingTriplesNextNeeded = False

        -- GRAPHS
        , graphs = DictIriApiData.empty
        , graphsClosure = DictIriApiData.empty

        -- SHACL
        , creatingFields = False
        , duplicatingFields = False
        }
    , Callbacks
        { getGraphs = DictIri.empty
        , getGraphsClosure = DictIri.empty
        , getTrigs = DictIri.empty
        , instances = DictIri.empty
        , createdFields = []
        , duplicatedFields = []

        -- CONCURRENT TASKS
        , pool = pool
        }
    , Effect.batch
        [ Author.list context ApiReturnedAuthors
        , Workspace.list context ApiReturnedWorkspaces
        , getWorkspace context
        , Effect.map MsgWorkspace effectWorkspace
        , Task.attempt BrowserReturnedTimeZone TimeZone.getZone
            |> Effect.fromCmd
        , Task.perform BrowserReturnedCurrentTime Time.now
            |> Effect.fromCmd
        , Viewport.get BrowserReturnedViewport
            |> Effect.fromCmd
        , Effect.map MsgTask effectCache
        ]
    )


getWorkspace : C -> Effect (Msg msgMain)
getWorkspace c =
    case c.workspaceUuid of
        Nothing ->
            Effect.none

        Just uuid ->
            Workspace.get c ApiReturnedWorkspaceCurrent uuid


initContext : Browser.Navigation.Key -> Url -> Flags -> AuthorDetail -> C
initContext key url flags author =
    let
        route =
            Route.fromUrl url

        query =
            (ElmSpa.Request.create (Route.fromUrl url) () url key).query

        workspaceUuid =
            getWorkspaceUuid route query

        locale =
            flags.rawLocale
                |> Locale.fromString
                |> Maybe.withDefault Locale.EnUS

        tooltip =
            Tooltip.init
    in
    { sandbox = flags.sandbox
    , useTripleStore = flags.useTripleStore
    , useSparklis = flags.useSparklis
    , allowUploads = flags.allowUploads
    , imprintUrl = flags.imprintUrl
    , dataProtectionUrl = flags.dataProtectionUrl
    , accessibilityUrl = flags.accessibilityUrl
    , csrfToken = flags.csrfToken
    , author = author
    , url = url
    , route = route
    , query = Dict.remove "details" query
    , workspaceUuid = workspaceUuid
    , workspace = Nothing

    -- TIME
    , zone = Time.utc
    , now = Time.millisToPosix 0
    , locale = locale

    -- DOM
    , viewport = Viewport.init
    , device =
        { class = Element.Desktop
        , orientation = Element.Landscape
        }
    , navigationVisible = False
    , tooltip = tooltip

    -- DATA
    , authors = RemoteData.Loading
    , workspaces = RemoteData.Loading

    -- SHACL
    , cShacl =
        { tooltip = tooltip
        , locale = locale
        , allowUploads = flags.allowUploads
        , permissions = author.permissions
        , preferences = author.preferencesAuthor
        , toUrl =
            toUrl
                { query = Dict.remove "details" query
                , workspaceUuid = workspaceUuid
                }
        }
    }


toUrl : Href.C r -> Iri -> Iri -> String
toUrl c class iri =
    Href.fromRouteWith c
        (Dict.fromList
            (case c.workspaceUuid of
                Nothing ->
                    [ ( "class", Rdf.toUrl class )
                    , ( "instance", Rdf.toUrl iri )
                    ]

                Just uuid ->
                    [ ( "class", Rdf.toUrl class )
                    , ( "instance", Rdf.toUrl iri )
                    , ( "workspace", uuid )
                    ]
            )
        )
        Route.Graph


getWorkspaceUuid : Route -> Dict String String -> Maybe String
getWorkspaceUuid route query =
    Maybe.orList
        [ Dict.get "workspace" query
        , case route of
            Route.Workspaces__Uuid_ { uuid } ->
                Just uuid

            _ ->
                Nothing
        ]


initDebug : Data -> Model
initDebug =
    Model


updateUrl :
    (Msg msgMain -> msgMain)
    -> Browser.Navigation.Key
    -> Url
    -> Model
    ->
        { model : Model
        , effect : Effect msgMain
        , routeChanged : Bool
        , workspaceChanged : Bool
        , queryChanged : Bool
        }
updateUrl toMsgMain key url (Model ({ context } as data)) =
    let
        contextUpdated =
            { context
                | route = route
                , query = query
                , workspaceUuid = workspaceUuid
                , cShacl =
                    { cShacl
                        | toUrl =
                            toUrl
                                { query = query
                                , workspaceUuid = workspaceUuid
                                }
                    }
            }

        { cShacl } =
            context

        route =
            Route.fromUrl url

        query =
            (ElmSpa.Request.create (Route.fromUrl url) () url key).query
                |> Dict.remove "details"
                |> Dict.remove "version"

        workspaceUuid =
            getWorkspaceUuid route query

        routeChanged =
            context.route /= contextUpdated.route

        workspaceChanged =
            context.workspaceUuid /= contextUpdated.workspaceUuid

        queryChanged =
            differentQueries

        differentQueries =
            Dict.merge
                (\_ _ _ -> True)
                (\_ a b result -> result || (a /= b))
                (\_ _ _ -> True)
                contextUpdated.query
                context.query
                False
    in
    { model = Model { data | context = contextUpdated }
    , effect =
        case Maybe.map .uuid context.workspace of
            Nothing ->
                Effect.map toMsgMain (getWorkspace contextUpdated)

            Just uuidPrevious ->
                case workspaceUuid of
                    Nothing ->
                        Effect.none

                    Just uuid ->
                        if uuid /= uuidPrevious then
                            Effect.map toMsgMain (getWorkspace contextUpdated)

                        else
                            Effect.none
    , routeChanged = routeChanged
    , workspaceChanged = workspaceChanged
    , queryChanged = queryChanged
    }



-- UPDATE


type Msg msg
    = Ignore
      -- BROWSER INFO
    | BrowserReturnedTimeZone (Result TimeZone.Error ( String, Time.Zone ))
    | BrowserReturnedCurrentTime Time.Posix
    | BrowserReturnedViewport (Result Dom.Error Viewport)
    | UserResizedBrowser
      -- NAVIGATION
    | UserPressedLogo
    | UserPressedShowHelp
    | UserScrolledHelp Scrolled.Offsets
    | UserPressedCloseHelp
    | UserPressedSendFeedback
    | UserPressedLogout
    | ApiReturnedLogout (Result Http.Error ())
      -- WORKSPACE
    | MsgWorkspace Shared.Workspace.Msg
      -- ACTIONS
    | ReceivedAction (Action msg)
      -- ACTION: USER INTENTS TO CREATE DATASET
    | ApiReturnedGraphOntologyForNewDataset Bool (Result DatasetCreate.Error String -> msg) String Iri (Api.Response Graph)
    | ApiReturnedNewDataset Bool (Result DatasetCreate.Error String -> msg) (Api.Response String)
      -- ACTION: USER INTENTS TO EDIT DATASET
    | ApiStartedEditOfDataset (Api.Response () -> msg) Iri (Api.Response ())
    | CacheReturnedDatasetAfterEditStart (Api.Response () -> msg) (Api.Response Graph)
      -- ACTION: USER INTENTS TO DELETE DRAFT
    | ApiCancelledDraft (Api.Response () -> msg) Iri (Api.Response ())
    | CacheReturnedDatasetAfterCancelDraft (Api.Response () -> msg) (Api.Response Graph)
      -- ACTION: USER INTENTS TO PUBLISH DRAFT
    | ApiPublishedDraft (Api.Response () -> msg) Iri (Api.Response Graph)
    | CacheReturnedDatasetAfterPublishDraft (Api.Response () -> msg) (Api.Response Graph)
      -- ACTION: USER INTENTS TO ADD ALIAS
    | ApiAddedAlias (Api.Response () -> msg) Iri (Api.Response Graph)
    | CacheReturnedDatasetAfterAddAlias (Api.Response () -> msg) (Api.Response Graph)
      -- ACTION: USER INTENTS TO DELETE ALIAS
    | ApiDeletedAlias (Api.Response () -> msg) Iri (Api.Response Graph)
    | CacheReturnedDatasetAfterDeleteAlias (Api.Response () -> msg) (Api.Response Graph)
      -- ACTION: USER INTENTS TO UPDATE PREFERENCES
    | ApiUpdatedPreferences (Result Http.Error Preferences)
      -- ACTION: UI
    | MsgToast Toast.Msg
      -- ACTION: FOCUS
    | Focused
      -- ACTION: DATA FETCHING
    | ApiReturnedAuthorDetails (Api.Response AuthorDetail)
      -- ACTION: GET
    | ApiReturnedGraphTurtle Action.ConfigGetGraph Iri (Api.Response String)
    | CacheReturnedGraphOntology Iri Iri (Api.Response Graph)
      --
    | ApiReturnedGraphInstances Iri (Api.Response (List Instance))
      -- DATA
    | ApiReturnedAuthors (Api.Response (List Author))
    | ApiReturnedWorkspaces (Api.Response (List Workspace))
    | ApiReturnedWorkspaceCurrent (Api.Response Workspace)
      -- SHACL
    | CreatedField (Api.Response Decoded.Field -> msg) (Api.Response ( Decoded.Field, UUID.Seeds ))
    | DuplicatedField (Api.Response Decoded.Field -> msg) (Api.Response ( Decoded.Field, UUID.Seeds ))
      -- CACHE
    | BackendReturnedTriples (Api.Response ( Graph, Maybe String, String ))
    | BackendReturnedTriplesPrev (Api.Response ( Graph, Maybe String ))
    | BackendReturnedTriplesNext (Api.Response ( Graph, String ))
    | BrowserWaitedForTriplesNext
      -- CONCURRENT TASKS
    | MsgTask (MsgTask msg)


type alias Pool msg =
    ConcurrentTask.Pool (MsgTask msg) ErrorTask (TaskOutput msg)


type ErrorTask
    = ErrorOpenTab String
    | ErrorQuadstore Store.Error
    | ErrorQuadstoreParseTurtle Iri Store.Error
    | ErrorLocalstorageWrite Localstorage.WriteError
    | ErrorLocalstorageReadPrev Localstorage.ReadError
    | ErrorLocalstorageReadNext Localstorage.ReadError


type MsgTask msg
    = OnComplete (ConcurrentTask.Response ErrorTask (TaskOutput msg))
    | OnProgress ( Pool msg, Effect (MsgTask msg) )


type TaskOutput msg
    = LocalstorageSetItem ()
    | LocalstorageReturnedPrev (Maybe String)
    | LocalstorageReturnedNext String
    | QuadstoreCleared ()
    | QuadstoreParsedTurtle Action.ConfigGetGraph Iri Graph
    | QuadstoreStoredTurtle
    | QuadstoreParsedTrig Iri (DictIri Graph)
    | QuadstoreStoredTrig Iri (DictIri Graph)
    | QuadstoreReturnedGraphTurtle (String -> msg) String
    | QuadstoreReturnedGraphNTriples (String -> msg) String
    | QuadstoreReturnedGraphJsonLd (String -> msg) String
    | QuadstoreReturnedQueryResult (List (Dict String BlankNodeOrIriOrAnyLiteral) -> msg) (List (Dict String BlankNodeOrIriOrAnyLiteral))
    | QuadstoreDeletedGraphs
    | OpenedTab


msgFromAction : Action msgMain -> Msg msgMain
msgFromAction =
    ReceivedAction


update :
    (Msg msgMain -> msgMain)
    -> Browser.Navigation.Key
    -> Msg msgMain
    -> Event msgMain
    -> Model
    -> Callbacks msgMain
    -> ( Model, Callbacks msgMain, Effect msgMain )
update toMsgMain key msg eventsMain ((Model ({ context } as data)) as model) ((Callbacks subs) as callbacks) =
    case msg of
        Ignore ->
            ( model, callbacks, Effect.none )

        -- BROWSER INFO
        BrowserReturnedTimeZone (Err _) ->
            ( model, callbacks, Effect.none )

        BrowserReturnedTimeZone (Ok ( _, zone )) ->
            -- Only update context when data changes to improve lazy rendering
            if zone == context.zone then
                ( model, callbacks, Effect.none )

            else
                { context | zone = zone }
                    |> setContext data callbacks

        BrowserReturnedCurrentTime now ->
            { context | now = now }
                |> setContext data callbacks

        BrowserReturnedViewport (Err _) ->
            ( model, callbacks, Effect.none )

        BrowserReturnedViewport (Ok viewport) ->
            -- Only update context when data changes to improve lazy rendering
            if viewport == context.viewport then
                ( model, callbacks, Effect.none )

            else
                let
                    newDevice =
                        if data.context.sandbox then
                            viewport
                                |> substractSandboxBox
                                |> classifyDevice

                        else
                            classifyDevice viewport
                in
                { context
                    | viewport = viewport
                    , device = newDevice
                }
                    |> setContext data callbacks

        UserResizedBrowser ->
            ( model
            , callbacks
            , Viewport.get BrowserReturnedViewport
                |> Effect.fromCmd
                |> Effect.map toMsgMain
            )

        -- NAVIGATION
        UserPressedLogo ->
            { context | navigationVisible = not context.navigationVisible }
                |> setContext data callbacks

        UserPressedShowHelp ->
            ( Model { data | showHelp = True }
            , callbacks
            , Effect.none
            )

        UserScrolledHelp offsets ->
            ( Model { data | helpOffsets = offsets }
            , callbacks
            , Effect.none
            )

        UserPressedCloseHelp ->
            ( Model { data | showHelp = False }
            , callbacks
            , Effect.none
            )

        UserPressedSendFeedback ->
            ( model
            , callbacks
            , ("mailto:herbie@hereon.de?subject="
                ++ Url.percentEncode "[Herbie] Feedback"
              )
                |> Browser.Navigation.load
                |> Effect.fromCmd
            )

        UserPressedLogout ->
            ( model
            , callbacks
            , { url = "/logout/"
              , expect = Http.expectWhatever ApiReturnedLogout
              }
                |> Http.get
                |> Effect.fromCmd
                |> Effect.map toMsgMain
            )

        ApiReturnedLogout (Err _) ->
            ( model, callbacks, Effect.none )

        ApiReturnedLogout (Ok ()) ->
            ( model
            , callbacks
            , "/"
                |> Browser.Navigation.load
                |> Effect.fromCmd
            )

        -- WORKSPACE
        MsgWorkspace msgWorkspace ->
            let
                ( workspaceUpdated, effectWorkspace ) =
                    Shared.Workspace.update context key msgWorkspace data.workspace
            in
            ( Model { data | workspace = workspaceUpdated }
            , callbacks
            , Effect.map (toMsgMain << MsgWorkspace) effectWorkspace
            )

        -- ACTION: USER INTENTS TO CREATE DATASET FOR
        ReceivedAction (UserIntentsToCreateDatasetFor workspaceUuid classes) ->
            let
                ( workspaceUpdated, effectWorkspace ) =
                    Shared.Workspace.openDialogCreateDocument workspaceUuid classes data.workspace
            in
            ( Model { data | workspace = workspaceUpdated }
            , callbacks
            , Effect.map (toMsgMain << MsgWorkspace) effectWorkspace
            )

        -- ACTION: USER INTENTS TO CREATE DATASET
        ReceivedAction (UserIntentsToCreateDataset open onResponse workspaceUuid iriOntology) ->
            ( model
            , callbacks
            , iriOntology
                |> Action.GetGraph
                    { store = False
                    , reload = False
                    , closure = True
                    }
                    (ApiReturnedGraphOntologyForNewDataset open onResponse workspaceUuid iriOntology)
                |> Effect.fromAction
                |> Effect.map toMsgMain
            )

        ApiReturnedGraphOntologyForNewDataset _ onResponse _ iriOntology (Err error) ->
            ( model
            , callbacks
            , { iri = iriOntology
              , error = error
              }
                |> DatasetCreate.CouldNotLoadOntology
                |> Err
                |> onResponse
                |> Effect.fromMsg
            )

        ApiReturnedGraphOntologyForNewDataset open onResponse workspaceUuid iriOntology (Ok graphOntology) ->
            case
                graphOntology
                    |> Shacl.fromGraph
                    |> Result.withDefault Shacl.empty
                    |> Shacl.nodeShapesWithTargetClasses
            of
                Err error ->
                    -- FIXME Propagate error
                    ( model
                    , callbacks
                    , DatasetCreate.ShaclError error
                        |> Err
                        |> onResponse
                        |> Effect.fromMsg
                    )

                Ok nodeShapes ->
                    ( model
                    , callbacks
                    , { workspaceUuid = workspaceUuid
                      , targetClass = NonEmpty.head (NonEmpty.head nodeShapes).targetClasses
                      , conformsTo = iriOntology
                      , onResponse = ApiReturnedNewDataset open onResponse
                      }
                        |> Datasets.create context
                        |> Effect.map toMsgMain
                    )

        ApiReturnedNewDataset _ onResponse (Err error) ->
            ( model
            , callbacks
            , { error = error }
                |> DatasetCreate.CouldNotCreateDataset
                |> Err
                |> onResponse
                |> Effect.fromMsg
            )

        ApiReturnedNewDataset open onResponse (Ok urlLocation) ->
            let
                uuid =
                    urlLocation
                        |> String.split "/"
                        |> List.reverse
                        |> List.getAt 1
                        |> Maybe.withDefault ""

                ( poolUpdated, cmdPool ) =
                    if open then
                        { uuid = uuid }
                            |> Route.Datasets__Uuid___Draft
                            |> Href.fromRoute context
                            |> openTab
                            |> ConcurrentTask.map (\_ -> OpenedTab)
                            |> ConcurrentTask.attempt
                                { send = Ports.send
                                , pool = subs.pool
                                , onComplete = OnComplete
                                }

                    else
                        ( subs.pool, Cmd.none )
            in
            ( model
            , Callbacks { subs | pool = poolUpdated }
            , [ sendEventForCacheChange toMsgMain eventsMain data handleTriplesChanged
              , uuid
                    |> Ok
                    |> onResponse
                    |> Effect.fromMsg
              , cmdPool
                    |> Effect.fromCmd
                    |> Effect.map (MsgTask >> toMsgMain)
              ]
                |> Effect.batch
            )

        -- ACTION: USER INTENTS TO EDIT DATASET
        ReceivedAction (UserIntentsToEditDataset onResponse uuid) ->
            ( model
            , callbacks
            , { uuid = uuid
              , onResponse = ApiStartedEditOfDataset onResponse
              }
                |> Datasets.edit context
                |> Effect.map toMsgMain
            )

        ReceivedAction (UserIntentsToEditDatasetWith onResponse uuid file format) ->
            ( model
            , callbacks
            , { uuid = uuid
              , onResponse = ApiStartedEditOfDataset onResponse
              , file = file
              , format = format
              }
                |> Datasets.editWith context
                |> Effect.map toMsgMain
            )

        ApiStartedEditOfDataset onResponse _ ((Err _) as error) ->
            ( model
            , callbacks
            , respondWithError onResponse error
            )

        ApiStartedEditOfDataset onResponse iri (Ok ()) ->
            ( model
            , callbacks
            , iri
                |> Action.GetGraph
                    { store = True
                    , reload = True
                    , closure = False
                    }
                    (CacheReturnedDatasetAfterEditStart onResponse)
                |> Effect.fromAction
                |> Effect.map toMsgMain
            )

        CacheReturnedDatasetAfterEditStart onResponse ((Err _) as error) ->
            ( model
            , callbacks
            , respondWithError onResponse error
            )

        CacheReturnedDatasetAfterEditStart onResponse (Ok _) ->
            ( model
            , callbacks
            , [ sendEventForCacheChange toMsgMain eventsMain data handleTriplesChanged
              , Effect.fromMsg (onResponse (Ok ()))
              ]
                |> Effect.batch
            )

        -- ACTION: USER INTENTS TO DELETE DRAFT
        ReceivedAction (UserIntentsToDeleteDraft onResponse uuid) ->
            ( model
            , callbacks
            , { uuid = uuid
              , onResponse = ApiCancelledDraft onResponse
              }
                |> Datasets.cancel context
                |> Effect.map toMsgMain
            )

        ApiCancelledDraft onResponse _ ((Err _) as error) ->
            ( model
            , callbacks
            , respondWithError onResponse error
            )

        ApiCancelledDraft onResponse iri (Ok ()) ->
            let
                ( poolUpdated, cmdPool ) =
                    maybeDocument
                        |> Maybe.map graphsDeleted
                        |> Maybe.withDefault []
                        |> List.map Store.deleteGraph
                        |> ConcurrentTask.batch
                        |> ConcurrentTask.map (\_ -> QuadstoreDeletedGraphs)
                        |> ConcurrentTask.mapError ErrorQuadstore
                        |> ConcurrentTask.attempt
                            { send = Ports.send
                            , pool = subs.pool
                            , onComplete = OnComplete
                            }

                graphsDeleted document =
                    case document.versions of
                        [] ->
                            document.this :: Maybe.unwrap [] graphsDeletedDraft document.draft

                        _ ->
                            Maybe.unwrap [] graphsDeletedDraft document.draft

                graphsDeletedDraft draft =
                    [ draft.this
                    , Herbie.iriEntered draft.this
                    , Herbie.iriPersisted draft.this
                    , Herbie.iriGenerated draft.this
                    , Herbie.iriSupport draft.this
                    ]

                maybeDocument =
                    data.graphs
                        |> DictIriApiData.get iri
                        |> ApiData.toMaybe
                        |> Maybe.andThen
                            (Rdf.Decode.decode (Rdf.Decode.from iri Herbie.decoderDocument)
                                >> Result.toMaybe
                            )
            in
            ( model
            , Callbacks { subs | pool = poolUpdated }
            , [ if Maybe.unwrap False (.versions >> List.isEmpty) maybeDocument then
                    [ sendEventForCacheChange toMsgMain eventsMain data handleTriplesChanged
                    , Effect.fromMsg (onResponse (Ok ()))
                    ]
                        |> Effect.batch

                else
                    iri
                        |> Action.GetGraph
                            { store = True
                            , reload = True
                            , closure = False
                            }
                            (CacheReturnedDatasetAfterCancelDraft onResponse)
                        |> Effect.fromAction
                        |> Effect.map toMsgMain
              , cmdPool
                    |> Effect.fromCmd
                    |> Effect.map (MsgTask >> toMsgMain)
              ]
                |> Effect.batch
            )

        CacheReturnedDatasetAfterCancelDraft onResponse ((Err _) as error) ->
            ( model
            , callbacks
            , error
                |> Result.map (always ())
                |> onResponse
                |> Effect.fromMsg
            )

        CacheReturnedDatasetAfterCancelDraft onResponse (Ok _) ->
            ( model
            , callbacks
            , [ sendEventForCacheChange toMsgMain eventsMain data handleTriplesChanged
              , Effect.fromMsg (onResponse (Ok ()))
              ]
                |> Effect.batch
            )

        -- ACTION: USER INTENTS TO PUBLISH DRAFT
        ReceivedAction (UserIntentsToPublishDraft onResponse uuid) ->
            ( model
            , callbacks
            , { uuid = uuid
              , onResponse = ApiPublishedDraft onResponse
              }
                |> Datasets.publish context
                |> Effect.map toMsgMain
            )

        ApiPublishedDraft onResponse _ ((Err _) as error) ->
            ( model
            , callbacks
            , respondWithError onResponse error
            )

        ApiPublishedDraft onResponse iri (Ok _) ->
            let
                ( poolUpdated, cmdPool ) =
                    graphsDeleted
                        |> List.map Store.deleteGraph
                        |> ConcurrentTask.batch
                        |> ConcurrentTask.map (\_ -> QuadstoreDeletedGraphs)
                        |> ConcurrentTask.mapError ErrorQuadstore
                        |> ConcurrentTask.attempt
                            { send = Ports.send
                            , pool = subs.pool
                            , onComplete = OnComplete
                            }

                graphsDeleted =
                    data.graphs
                        |> DictIriApiData.get iri
                        |> ApiData.toMaybe
                        |> Maybe.andThen
                            (Rdf.Decode.decode (Rdf.Decode.from iri Herbie.decoderDocument)
                                >> Result.toMaybe
                            )
                        |> Maybe.andThen .draft
                        |> Maybe.map graphsDeletedDraft
                        |> Maybe.withDefault []

                graphsDeletedDraft draft =
                    [ draft.this
                    , Herbie.iriEntered draft.this
                    , Herbie.iriPersisted draft.this
                    , Herbie.iriGenerated draft.this
                    , Herbie.iriSupport draft.this
                    ]
            in
            ( model
            , Callbacks { subs | pool = poolUpdated }
            , [ iri
                    |> Action.GetGraph
                        { store = True
                        , reload = True
                        , closure = False
                        }
                        (CacheReturnedDatasetAfterPublishDraft onResponse)
                    |> Effect.fromAction
                    |> Effect.map toMsgMain
              , cmdPool
                    |> Effect.fromCmd
                    |> Effect.map (MsgTask >> toMsgMain)
              ]
                |> Effect.batch
            )

        CacheReturnedDatasetAfterPublishDraft onResponse ((Err _) as error) ->
            ( model
            , callbacks
            , respondWithError onResponse error
            )

        CacheReturnedDatasetAfterPublishDraft onResponse (Ok _) ->
            ( model
            , callbacks
            , [ sendEventForCacheChange toMsgMain eventsMain data handleTriplesChanged
              , Effect.fromMsg (onResponse (Ok ()))
              ]
                |> Effect.batch
            )

        -- ACTION: USER INTENTS TO ADD ALIAS
        ReceivedAction (UserIntentsToAddAlias onResponse uuid iriAlias) ->
            ( model
            , callbacks
            , { uuid = uuid
              , alias = iriAlias
              , onResponse = ApiAddedAlias onResponse
              }
                |> Datasets.aliasCreate context
                |> Effect.map toMsgMain
            )

        ApiAddedAlias onResponse _ ((Err _) as error) ->
            ( model
            , callbacks
            , respondWithError onResponse error
            )

        ApiAddedAlias onResponse iri (Ok _) ->
            ( model
            , callbacks
            , iri
                |> Action.GetGraph
                    { store = True
                    , reload = True
                    , closure = False
                    }
                    (CacheReturnedDatasetAfterAddAlias onResponse)
                |> Effect.fromAction
                |> Effect.map toMsgMain
            )

        CacheReturnedDatasetAfterAddAlias onResponse ((Err _) as error) ->
            ( model
            , callbacks
            , respondWithError onResponse error
            )

        CacheReturnedDatasetAfterAddAlias onResponse (Ok _) ->
            ( model
            , callbacks
            , Ok ()
                |> onResponse
                |> Effect.fromMsg
            )

        -- ACTION: USER INTENTS TO DELETE ALIAS
        ReceivedAction (UserIntentsToDeleteAlias onResponse uuid iriAlias) ->
            ( model
            , callbacks
            , { uuid = uuid
              , alias = iriAlias
              , onResponse = ApiDeletedAlias onResponse
              }
                |> Datasets.aliasDelete context
                |> Effect.map toMsgMain
            )

        ApiDeletedAlias onResponse _ ((Err _) as error) ->
            ( model
            , callbacks
            , respondWithError onResponse error
            )

        ApiDeletedAlias onResponse iri (Ok _) ->
            ( model
            , callbacks
            , iri
                |> Action.GetGraph
                    { store = True
                    , reload = True
                    , closure = False
                    }
                    (CacheReturnedDatasetAfterDeleteAlias onResponse)
                |> Effect.fromAction
                |> Effect.map toMsgMain
            )

        CacheReturnedDatasetAfterDeleteAlias onResponse ((Err _) as error) ->
            ( model
            , callbacks
            , respondWithError onResponse error
            )

        CacheReturnedDatasetAfterDeleteAlias onResponse (Ok _) ->
            ( model
            , callbacks
            , Ok ()
                |> onResponse
                |> Effect.fromMsg
            )

        -- ACTION: USER INTENTS TO COLLAPSE NAVIGATION
        ReceivedAction UserIntentsToCollapseNavigation ->
            { context | navigationVisible = False }
                |> setContext data callbacks

        -- ACTION: USER INTENTS TO CLEAR STORE
        ReceivedAction Action.UserIntentsToClearStore ->
            let
                ( poolUpdated, effectPool ) =
                    removeItemTriples subs.pool
            in
            ( Model
                { data
                    | triplesPrev = Nothing
                    , triplesNext = Nothing
                }
            , Callbacks { subs | pool = poolUpdated }
            , Effect.map (MsgTask >> toMsgMain) effectPool
            )

        MsgTask (OnComplete (ConcurrentTask.Success (QuadstoreCleared ()))) ->
            ( Model
                { data
                    | triplesPrev = Nothing
                    , triplesNext = Nothing
                }
            , callbacks
            , [ { message = verbatim "Cleared quadstore" }
                    |> Action.AddToast
                    |> Effect.fromAction
              , if useCache context then
                    Triples.get context BackendReturnedTriples

                else
                    Effect.none
              ]
                |> Effect.batch
                |> Effect.map toMsgMain
            )

        -- ACTION: USER INTENTS TO UPDATE PREFERENCES
        ReceivedAction (Action.UserIntentsToUpdatePreferences preferences) ->
            ( model
            , callbacks
            , { preferences = preferences
              , onResponse = ApiUpdatedPreferences
              }
                |> Preferences.update context
                |> Effect.map toMsgMain
            )

        ApiUpdatedPreferences (Err _) ->
            -- FIXME handle this
            ( model, callbacks, Effect.none )

        ApiUpdatedPreferences (Ok preferencesAuthor) ->
            let
                author =
                    context.author

                { cShacl } =
                    context
            in
            { context
                | author = { author | preferencesAuthor = preferencesAuthor }
                , cShacl = { cShacl | preferences = preferencesAuthor }
            }
                |> setContext data callbacks

        -- ACTION: UI
        ReceivedAction (TooltipMsg msgTooltip) ->
            let
                tooltipUpdated =
                    Tooltip.update msgTooltip data.context.tooltip

                { cShacl } =
                    context
            in
            ( Model
                { data
                    | context =
                        { context
                            | tooltip = tooltipUpdated
                            , cShacl = { cShacl | tooltip = tooltipUpdated }
                        }
                }
            , callbacks
            , Effect.none
            )

        ReceivedAction (AddToast toast) ->
            let
                ( trayUpdated, cmdToast ) =
                    toast
                        |> Toast.persistent
                        |> Toast.add data.tray
            in
            ( Model { data | tray = trayUpdated }
            , callbacks
            , Effect.fromCmd (Cmd.map (toMsgMain << MsgToast) cmdToast)
            )

        ReceivedAction (AddToastExpireIn milliseconds toast) ->
            let
                ( trayUpdated, cmdToast ) =
                    toast
                        |> Toast.expireIn milliseconds
                        |> Toast.add data.tray
            in
            ( Model { data | tray = trayUpdated }
            , callbacks
            , Effect.fromCmd (Cmd.map (toMsgMain << MsgToast) cmdToast)
            )

        MsgToast msgToast ->
            let
                ( trayUpdated, cmdToast ) =
                    Toast.update msgToast data.tray
            in
            ( Model { data | tray = trayUpdated }
            , callbacks
            , Effect.fromCmd (Cmd.map (toMsgMain << MsgToast) cmdToast)
            )

        -- ACTION: FOCUS
        ReceivedAction (Focus focusId) ->
            ( model
            , callbacks
            , Effect.fromCmd (Task.attempt (toMsgMain << always Focused) (Dom.focus focusId))
            )

        Focused ->
            ( model, callbacks, Effect.none )

        -- ACTION: DATA FETCHING
        ReceivedAction RequestAuthorDetails ->
            ( model
            , callbacks
            , Me.getAuthor (toMsgMain << ApiReturnedAuthorDetails)
                |> Effect.fromCmd
            )

        ApiReturnedAuthorDetails (Err error) ->
            ( model
            , callbacks
            , error
                |> Action.toastFromError data.context
                |> Effect.fromAction
            )

        ApiReturnedAuthorDetails (Ok author) ->
            let
                { cShacl } =
                    context
            in
            { context
                | author = author
                , cShacl =
                    { cShacl
                        | preferences = author.preferencesAuthor
                        , permissions = author.permissions
                    }
            }
                |> setContext data callbacks

        -- ACTION: GET
        ReceivedAction (GetGraph config onResponse iri) ->
            if config.closure then
                let
                    getGraphsClosureUpdated =
                        appendOnResponse iri onResponse subs.getGraphsClosure
                in
                case DictIriApiData.get iri data.graphsClosure of
                    ApiData.NotAsked ->
                        ( Model
                            { data
                                | graphsClosureLoading =
                                    DictIri.insert iri
                                        { graphsRequested = SetIri.singleton iri
                                        , graphsReceived = SetIri.empty
                                        , graph = Rdf.emptyGraph
                                        }
                                        data.graphsClosureLoading
                                , graphsClosure = DictIriApiData.insert iri ApiData.Loading data.graphsClosure
                            }
                        , Callbacks { subs | getGraphsClosure = getGraphsClosureUpdated }
                        , iri
                            |> GetGraph
                                { store = True
                                , reload = False
                                , closure = False
                                }
                                (toMsgMain << CacheReturnedGraphOntology iri iri)
                            |> Effect.fromAction
                        )

                    ApiData.Loading ->
                        ( model
                        , Callbacks { subs | getGraphsClosure = getGraphsClosureUpdated }
                        , Effect.none
                        )

                    ApiData.Failure error ->
                        ( model
                        , Callbacks { subs | getGraphsClosure = DictIri.remove iri subs.getGraphsClosure }
                        , sendOnResponses iri (Err error) getGraphsClosureUpdated
                        )

                    ApiData.Success dataOntology ->
                        ( model
                        , Callbacks { subs | getGraphsClosure = DictIri.remove iri subs.getGraphsClosure }
                        , sendOnResponses iri (Ok dataOntology) getGraphsClosureUpdated
                        )

                    ApiData.Reloading _ ->
                        ( model
                        , Callbacks { subs | getGraphsClosure = getGraphsClosureUpdated }
                        , Effect.none
                        )

            else
                let
                    getGraphsUpdated =
                        appendOnResponse iri onResponse subs.getGraphs
                in
                case DictIriApiData.get iri data.graphs of
                    ApiData.NotAsked ->
                        ( Model { data | graphs = DictIriApiData.insert iri ApiData.Loading data.graphs }
                        , Callbacks { subs | getGraphs = getGraphsUpdated }
                        , Effect.map toMsgMain (getGraphTurtle context config iri)
                        )

                    ApiData.Loading ->
                        ( model
                        , Callbacks { subs | getGraphs = getGraphsUpdated }
                        , Effect.none
                        )

                    ApiData.Failure error ->
                        if config.reload then
                            ( Model { data | graphs = DictIriApiData.insert iri ApiData.Loading data.graphs }
                            , Callbacks { subs | getGraphs = getGraphsUpdated }
                            , Effect.map toMsgMain (getGraphTurtle context config iri)
                            )

                        else
                            ( model
                            , Callbacks { subs | getGraphs = DictIri.remove iri subs.getGraphs }
                            , sendOnResponses iri (Err error) getGraphsUpdated
                            )

                    ApiData.Success graph ->
                        if config.reload then
                            ( Model { data | graphs = DictIriApiData.insert iri (ApiData.Reloading graph) data.graphs }
                            , Callbacks { subs | getGraphs = getGraphsUpdated }
                            , Effect.map toMsgMain (getGraphTurtle context config iri)
                            )

                        else
                            ( model
                            , Callbacks { subs | getGraphs = DictIri.remove iri subs.getGraphs }
                            , sendOnResponses iri (Ok graph) getGraphsUpdated
                            )

                    ApiData.Reloading _ ->
                        ( model
                        , Callbacks { subs | getGraphs = getGraphsUpdated }
                        , Effect.none
                        )

        CacheReturnedGraphOntology iriRoot _ (Err error) ->
            ( Model
                { data
                    | graphsClosureLoading = DictIri.remove iriRoot data.graphsClosureLoading
                    , graphsClosure = DictIriApiData.insert iriRoot (ApiData.Failure error) data.graphsClosure
                }
            , callbacks
            , sendOnResponses iriRoot (Err error) subs.getGraphsClosure
            )

        CacheReturnedGraphOntology iriRoot iri (Ok graph) ->
            case DictIri.get iriRoot data.graphsClosureLoading of
                Nothing ->
                    ( model, callbacks, Effect.none )

                Just loading ->
                    let
                        loadingUpdated =
                            { loading
                                | graphsRequested = SetIri.remove iri loading.graphsRequested
                                , graphsReceived = SetIri.insert iri loading.graphsReceived
                                , graph = Rdf.union loading.graph graph
                            }

                        graphsRequired =
                            OWL.importsFor iri graph
                                |> List.filter (not << alreadyRequestedOrReceived)
                                |> SetIri.fromList

                        alreadyRequestedOrReceived iriImported =
                            SetIri.member iriImported loadingUpdated.graphsReceived
                                || SetIri.member iriImported loadingUpdated.graphsRequested
                    in
                    if SetIri.isEmpty loadingUpdated.graphsRequested && SetIri.isEmpty graphsRequired then
                        ( Model
                            { data
                                | graphsClosureLoading = DictIri.remove iriRoot data.graphsClosureLoading
                                , graphsClosure = DictIriApiData.insert iriRoot (ApiData.Success loadingUpdated.graph) data.graphsClosure
                            }
                        , Callbacks { subs | getGraphsClosure = DictIri.remove iriRoot subs.getGraphsClosure }
                        , sendOnResponses iriRoot (Ok loadingUpdated.graph) subs.getGraphsClosure
                        )

                    else
                        ( Model
                            { data
                                | graphsClosureLoading =
                                    DictIri.insert iriRoot
                                        { loadingUpdated
                                            | graphsRequested = SetIri.union loadingUpdated.graphsRequested graphsRequired
                                        }
                                        data.graphsClosureLoading
                            }
                        , callbacks
                        , graphsRequired
                            |> SetIri.toList
                            |> List.map
                                (\iriGraphRequired ->
                                    iriGraphRequired
                                        |> GetGraph
                                            { store = True
                                            , reload = False
                                            , closure = False
                                            }
                                            (toMsgMain << CacheReturnedGraphOntology iriRoot iriGraphRequired)
                                        |> Effect.fromAction
                                )
                            |> Effect.batch
                        )

        ApiReturnedGraphTurtle _ iri (Err error) ->
            ( Model { data | graphs = DictIriApiData.insert iri (ApiData.Failure error) data.graphs }
            , callbacks
            , Effect.batch
                [ sendOnResponses iri (Err error) subs.getTrigs
                , sendOnResponses iri (Err error) subs.getGraphs
                ]
            )

        ApiReturnedGraphTurtle config iri (Ok text) ->
            let
                ( poolUpdated, cmdPool ) =
                    Store.parseTurtle iri text
                        |> ConcurrentTask.map (QuadstoreParsedTurtle config iri)
                        |> ConcurrentTask.mapError (ErrorQuadstoreParseTurtle iri)
                        |> ConcurrentTask.attempt
                            { send = Ports.send
                            , pool = subs.pool
                            , onComplete = OnComplete
                            }
            in
            ( model
            , Callbacks { subs | pool = poolUpdated }
            , cmdPool
                |> Effect.fromCmd
                |> Effect.map (MsgTask >> toMsgMain)
            )

        MsgTask (OnComplete (ConcurrentTask.Error (ErrorQuadstoreParseTurtle iri (Store.Error error)))) ->
            ( model
            , callbacks
            , { message =
                    [ "An error occured while parsing '"
                    , Rdf.toUrl iri
                    , "': "
                    , error
                    ]
                        |> String.concat
                        |> verbatim
              }
                |> Action.AddToast
                |> Effect.fromAction
            )

        MsgTask (OnComplete (ConcurrentTask.Success (QuadstoreParsedTurtle config iri graph))) ->
            let
                final =
                    insertCacheDict iri
                        (Ok graph)
                        { data = data.graphs
                        , subs = subs.getGraphs
                        , effects = []
                        }

                modelUpdated =
                    Model { data | graphs = final.data }

                getGraphsUpdated =
                    DictIri.remove iri final.subs

                effect =
                    Effect.batch
                        [ sendEventForCacheChange toMsgMain eventsMain data (handleGraphUpdated iri graph)
                        , Effect.batch final.effects
                        ]
            in
            if config.store then
                let
                    ( poolUpdated, cmdPool ) =
                        (Store.storeGraph iri graph :: storeGraphAliased)
                            |> ConcurrentTask.sequence
                            |> ConcurrentTask.map (\_ -> QuadstoreStoredTurtle)
                            |> ConcurrentTask.mapError ErrorQuadstore
                            |> ConcurrentTask.attempt
                                { send = Ports.send
                                , pool = subs.pool
                                , onComplete = OnComplete
                                }

                    storeGraphAliased =
                        graph
                            |> Rdf.Decode.decode (Rdf.Decode.from iri decoderSources)
                            |> Result.withDefault []
                            |> List.map (\iriSource -> Store.storeGraph iriSource graph)
                in
                ( modelUpdated
                , Callbacks
                    { subs
                        | getGraphs = getGraphsUpdated
                        , pool = poolUpdated
                    }
                , Effect.batch
                    [ effect
                    , cmdPool
                        |> Effect.fromCmd
                        |> Effect.map (MsgTask >> toMsgMain)
                    ]
                )

            else
                ( modelUpdated
                , Callbacks { subs | getGraphs = getGraphsUpdated }
                , effect
                )

        MsgTask (OnComplete (ConcurrentTask.Success QuadstoreStoredTurtle)) ->
            ( model, callbacks, Effect.none )

        ReceivedAction (GetGraphTurtle onResponse iri) ->
            let
                ( poolUpdated, cmdPool ) =
                    Store.getGraphTurtle iri
                        |> ConcurrentTask.map (QuadstoreReturnedGraphTurtle onResponse)
                        |> ConcurrentTask.mapError ErrorQuadstore
                        |> ConcurrentTask.attempt
                            { send = Ports.send
                            , pool = subs.pool
                            , onComplete = OnComplete
                            }
            in
            ( model
            , Callbacks { subs | pool = poolUpdated }
            , cmdPool
                |> Effect.fromCmd
                |> Effect.map (MsgTask >> toMsgMain)
            )

        MsgTask (OnComplete (ConcurrentTask.Success (QuadstoreReturnedGraphTurtle onResponse turtle))) ->
            ( model
            , callbacks
            , turtle
                |> onResponse
                |> Effect.fromMsg
            )

        ReceivedAction (GetGraphNTriples onResponse iri) ->
            let
                ( poolUpdated, cmdPool ) =
                    Store.getGraphNTriples iri
                        |> ConcurrentTask.map (QuadstoreReturnedGraphNTriples onResponse)
                        |> ConcurrentTask.mapError ErrorQuadstore
                        |> ConcurrentTask.attempt
                            { send = Ports.send
                            , pool = subs.pool
                            , onComplete = OnComplete
                            }
            in
            ( model
            , Callbacks { subs | pool = poolUpdated }
            , cmdPool
                |> Effect.fromCmd
                |> Effect.map (MsgTask >> toMsgMain)
            )

        MsgTask (OnComplete (ConcurrentTask.Success (QuadstoreReturnedGraphNTriples onResponse nTriples))) ->
            ( model
            , callbacks
            , nTriples
                |> onResponse
                |> Effect.fromMsg
            )

        ReceivedAction (GetGraphJsonLd onResponse iri) ->
            let
                ( poolUpdated, cmdPool ) =
                    Store.getGraphJsonLd iri
                        |> ConcurrentTask.map (QuadstoreReturnedGraphJsonLd onResponse)
                        |> ConcurrentTask.mapError ErrorQuadstore
                        |> ConcurrentTask.attempt
                            { send = Ports.send
                            , pool = subs.pool
                            , onComplete = OnComplete
                            }
            in
            ( model
            , Callbacks { subs | pool = poolUpdated }
            , cmdPool
                |> Effect.fromCmd
                |> Effect.map (MsgTask >> toMsgMain)
            )

        MsgTask (OnComplete (ConcurrentTask.Success (QuadstoreReturnedGraphJsonLd onResponse jsonLd))) ->
            ( model
            , callbacks
            , jsonLd
                |> onResponse
                |> Effect.fromMsg
            )

        -- ACTION: SELECT WORKSPACE
        ReceivedAction (SelectWorkspace uuid) ->
            let
                contextUpdated =
                    { context
                        | workspaceUuid = Just uuid
                        , cShacl =
                            { cShacl
                                | toUrl =
                                    toUrl
                                        { query = context.query
                                        , workspaceUuid = Just uuid
                                        }
                            }
                    }

                { cShacl } =
                    context
            in
            ( Model { data | context = contextUpdated }
            , callbacks
            , Effect.map toMsgMain (Workspace.get contextUpdated ApiReturnedWorkspaceCurrent uuid)
            )

        -- ACTION: SHACL
        ReceivedAction (CreateField onField preview path mint field) ->
            if not data.creatingFields then
                ( Model { data | creatingFields = True }
                , callbacks
                , createFields data.context data.seeds onField preview path mint field
                    |> Effect.map toMsgMain
                )

            else
                let
                    createdFieldsNew =
                        { preview = preview
                        , path = path
                        , mint = mint
                        , field = field
                        , onField = onField
                        }
                in
                ( model
                , Callbacks { subs | createdFields = createdFieldsNew :: subs.createdFields }
                , Effect.none
                )

        CreatedField onField (Err error) ->
            case subs.createdFields of
                [] ->
                    ( Model { data | creatingFields = False }
                    , callbacks
                    , Effect.fromMsg (onField (Err error))
                    )

                next :: rest ->
                    ( Model { data | creatingFields = True }
                    , Callbacks { subs | createdFields = rest }
                    , createFields
                        data.context
                        data.seeds
                        next.onField
                        next.preview
                        next.path
                        next.mint
                        next.field
                        |> Effect.map toMsgMain
                    )

        CreatedField onField (Ok ( field, seeds )) ->
            case subs.createdFields of
                [] ->
                    ( Model
                        { data
                            | creatingFields = False
                            , seeds = seeds
                        }
                    , callbacks
                    , Effect.fromMsg (onField (Ok field))
                    )

                next :: rest ->
                    ( Model
                        { data
                            | creatingFields = True
                            , seeds = seeds
                        }
                    , Callbacks { subs | createdFields = rest }
                    , Effect.batch
                        [ Effect.fromMsg (onField (Ok field))
                        , createFields
                            data.context
                            seeds
                            next.onField
                            next.preview
                            next.path
                            next.mint
                            next.field
                            |> Effect.map toMsgMain
                        ]
                    )

        ReceivedAction (DuplicateField onField preview path mint field) ->
            if not data.duplicatingFields then
                ( Model { data | duplicatingFields = True }
                , callbacks
                , duplicateFields data.context
                    data.seeds
                    onField
                    preview
                    path
                    mint
                    field
                    |> Effect.map toMsgMain
                )

            else
                let
                    duplicatedFieldsNew =
                        { preview = preview
                        , path = path
                        , mint = mint
                        , field = field
                        , onField = onField
                        }
                in
                ( model
                , Callbacks
                    { subs | duplicatedFields = duplicatedFieldsNew :: subs.duplicatedFields }
                , Effect.none
                )

        DuplicatedField onField (Err error) ->
            case subs.duplicatedFields of
                [] ->
                    ( Model { data | duplicatingFields = False }
                    , callbacks
                    , Effect.fromMsg (onField (Err error))
                    )

                next :: rest ->
                    ( Model { data | duplicatingFields = True }
                    , Callbacks { subs | duplicatedFields = rest }
                    , duplicateFields
                        data.context
                        data.seeds
                        next.onField
                        next.preview
                        next.path
                        next.mint
                        next.field
                        |> Effect.map toMsgMain
                    )

        DuplicatedField onField (Ok ( field, seeds )) ->
            case subs.duplicatedFields of
                [] ->
                    ( Model
                        { data
                            | duplicatingFields = False
                            , seeds = seeds
                        }
                    , callbacks
                    , Effect.fromMsg (onField (Ok field))
                    )

                next :: rest ->
                    ( Model
                        { data
                            | duplicatingFields = True
                            , seeds = seeds
                        }
                    , Callbacks { subs | duplicatedFields = rest }
                    , Effect.batch
                        [ Effect.fromMsg (onField (Ok field))
                        , duplicateFields
                            data.context
                            seeds
                            next.onField
                            next.preview
                            next.path
                            next.mint
                            next.field
                            |> Effect.map toMsgMain
                        ]
                    )

        -- CACHE
        BackendReturnedTriples (Err error) ->
            ( model
            , callbacks
            , error
                |> Action.toastFromError data.context
                |> Effect.fromAction
            )

        BackendReturnedTriples (Ok ( graph, prev, next )) ->
            let
                ( poolUpdated, effectPool ) =
                    setItemTriplesPrev prev subs.pool

                ( poolFinal, effectPoolFinal ) =
                    setItemTriplesNext next poolUpdated
            in
            ( Model
                { data
                    | triplesPrev = prev
                    , triplesNext = Just next
                }
            , Callbacks { subs | pool = poolFinal }
            , [ [ preloadDocumentsFrom graph
                , Maybe.unwrap Effect.none (Triples.getPrev context BackendReturnedTriplesPrev) prev
                , Effect.map MsgTask effectPool
                , Effect.map MsgTask effectPoolFinal
                ]
                    |> Effect.batch
                    |> Effect.map toMsgMain
              , sendEventForCacheChange toMsgMain eventsMain data handleTriplesChanged
              ]
                |> Effect.batch
            )

        BackendReturnedTriplesPrev (Err error) ->
            ( model
            , callbacks
            , error
                |> Action.toastFromError data.context
                |> Effect.fromAction
            )

        BackendReturnedTriplesPrev (Ok ( graph, prev )) ->
            let
                ( poolUpdated, effectPool ) =
                    setItemTriplesPrev prev subs.pool
            in
            ( Model { data | triplesPrev = prev }
            , Callbacks { subs | pool = poolUpdated }
            , [ [ preloadDocumentsFrom graph
                , Maybe.unwrap Effect.none (Triples.getPrev context BackendReturnedTriplesPrev) prev
                , Effect.map MsgTask effectPool
                ]
                    |> Effect.batch
                    |> Effect.map toMsgMain
              , sendEventForCacheChange toMsgMain eventsMain data handleTriplesChanged
              ]
                |> Effect.batch
            )

        BackendReturnedTriplesNext (Err error) ->
            if data.fetchingTriplesNextNeeded then
                case data.triplesNext of
                    Nothing ->
                        ( Model
                            { data
                                | fetchingTriplesNext = False
                                , fetchingTriplesNextNeeded = False
                            }
                        , callbacks
                        , error
                            |> Action.toastFromError data.context
                            |> Effect.fromAction
                        )

                    Just next ->
                        ( Model
                            { data
                                | fetchingTriplesNext = True
                                , fetchingTriplesNextNeeded = False
                            }
                        , callbacks
                        , [ error
                                |> Action.toastFromError data.context
                                |> Effect.fromAction
                          , Triples.getNext context BackendReturnedTriplesNext next
                                |> Effect.map toMsgMain
                          ]
                            |> Effect.batch
                        )

            else
                ( Model { data | fetchingTriplesNext = False }
                , callbacks
                , error
                    |> Action.toastFromError data.context
                    |> Effect.fromAction
                )

        BackendReturnedTriplesNext (Ok ( graph, next )) ->
            let
                ( poolUpdated, effectPool ) =
                    setItemTriplesNext next subs.pool
            in
            if data.fetchingTriplesNextNeeded then
                ( Model
                    { data
                        | fetchingTriplesNext = True
                        , fetchingTriplesNextNeeded = False
                        , triplesNext = Just next
                    }
                , Callbacks { subs | pool = poolUpdated }
                , [ [ preloadDocumentsFrom graph
                    , Effect.map MsgTask effectPool
                    , Triples.getNext context BackendReturnedTriplesNext next
                    ]
                        |> Effect.batch
                        |> Effect.map toMsgMain
                  , sendEventForCacheChange toMsgMain eventsMain data handleTriplesChanged
                  ]
                    |> Effect.batch
                )

            else
                ( Model
                    { data
                        | fetchingTriplesNext = False
                        , triplesNext = Just next
                    }
                , Callbacks { subs | pool = poolUpdated }
                , [ [ preloadDocumentsFrom graph
                    , Effect.map MsgTask effectPool
                    ]
                        |> Effect.batch
                        |> Effect.map toMsgMain
                  , sendEventForCacheChange toMsgMain eventsMain data handleTriplesChanged
                  ]
                    |> Effect.batch
                )

        BrowserWaitedForTriplesNext ->
            if data.fetchingTriplesNext then
                ( model, callbacks, Effect.none )

            else
                case data.triplesNext of
                    Nothing ->
                        ( model, callbacks, Effect.none )

                    Just next ->
                        ( Model { data | fetchingTriplesNext = True }
                        , callbacks
                        , Triples.getNext context BackendReturnedTriplesNext next
                            |> Effect.map toMsgMain
                        )

        ReceivedAction UpdateTriples ->
            if useCache context then
                if data.fetchingTriplesNext then
                    ( model, callbacks, Effect.none )

                else
                    case data.triplesNext of
                        Nothing ->
                            ( model, callbacks, Effect.none )

                        Just next ->
                            ( Model { data | fetchingTriplesNext = True }
                            , callbacks
                            , Triples.getNext context BackendReturnedTriplesNext next
                                |> Effect.map toMsgMain
                            )

            else
                ( model, callbacks, Effect.none )

        MsgTask (OnComplete (ConcurrentTask.Error (ErrorLocalstorageReadPrev readError))) ->
            ( model
            , callbacks
            , case readError of
                Localstorage.NoValue ->
                    Triples.get context BackendReturnedTriples
                        |> Effect.map toMsgMain

                Localstorage.ReadBlocked ->
                    { message = verbatim "Could not read from local storage" }
                        |> Action.AddToast
                        |> Effect.fromAction

                Localstorage.DecodeError decodeError ->
                    [ { message =
                            [ "Could not decode local storage: "
                            , Decode.errorToString decodeError
                            ]
                                |> String.concat
                                |> verbatim
                      }
                        |> Action.AddToast
                        |> Effect.fromAction
                    , Triples.get context BackendReturnedTriples
                        |> Effect.map toMsgMain
                    ]
                        |> Effect.batch
            )

        MsgTask (OnComplete (ConcurrentTask.Success (LocalstorageReturnedPrev prev))) ->
            ( Model { data | triplesPrev = prev }
            , callbacks
            , case prev of
                Nothing ->
                    Effect.none

                Just urlPrev ->
                    urlPrev
                        |> Triples.getPrev context BackendReturnedTriplesPrev
                        |> Effect.map toMsgMain
            )

        MsgTask (OnComplete (ConcurrentTask.Error (ErrorLocalstorageReadNext readError))) ->
            ( model
            , callbacks
            , case readError of
                Localstorage.NoValue ->
                    Effect.none

                Localstorage.ReadBlocked ->
                    { message = verbatim "Could not read from local storage" }
                        |> Action.AddToast
                        |> Effect.fromAction

                Localstorage.DecodeError decodeError ->
                    { message =
                        [ "Could not decode local storage: "
                        , Decode.errorToString decodeError
                        ]
                            |> String.concat
                            |> verbatim
                    }
                        |> Action.AddToast
                        |> Effect.fromAction
            )

        MsgTask (OnComplete (ConcurrentTask.Success (LocalstorageReturnedNext next))) ->
            ( Model { data | triplesNext = Just next }
            , callbacks
            , Effect.none
            )

        MsgTask (OnComplete (ConcurrentTask.Error (ErrorLocalstorageWrite writeError))) ->
            ( model
            , callbacks
            , { message =
                    case writeError of
                        Localstorage.QuotaExceeded ->
                            verbatim "No space left in local storage"

                        Localstorage.WriteBlocked ->
                            verbatim "Could not write to local storage"
              }
                |> Action.AddToast
                |> Effect.fromAction
            )

        MsgTask (OnComplete (ConcurrentTask.Success (LocalstorageSetItem ()))) ->
            ( model, callbacks, Effect.none )

        --
        ReceivedAction (GetInstances onResponse config) ->
            let
                final =
                    getCacheDict
                        { onResponse = onResponse
                        , get =
                            Instance.list context
                                (toMsgMain << ApiReturnedGraphInstances config.class)
                                { workspaceUuid = config.workspaceUuid
                                , class = config.class
                                , includes = config.includes
                                }
                        , reload = False
                        }
                        config.class
                        { data = data.instances
                        , subs = subs.instances
                        , effects = []
                        }
            in
            ( Model { data | instances = final.data }
            , Callbacks { subs | instances = final.subs }
            , Effect.batch final.effects
            )

        ApiReturnedGraphInstances iri result ->
            let
                final =
                    insertCacheDict
                        iri
                        result
                        { data = data.instances
                        , subs = subs.instances
                        , effects = []
                        }
            in
            ( Model { data | instances = final.data }
            , Callbacks { subs | instances = final.subs }
            , [ Effect.batch final.effects
              , DictIri.get iri subs.instances
                    |> Maybe.map (List.map (\onResponse -> Effect.fromMsg (onResponse result)) >> Effect.batch)
                    |> Maybe.withDefault Effect.none
              ]
                |> Effect.batch
            )

        ReceivedAction (StoreTrig onResponse iri text) ->
            let
                ( poolUpdated, cmdPool ) =
                    Store.parseTrig iri text
                        |> ConcurrentTask.map (QuadstoreParsedTrig iri)
                        |> ConcurrentTask.mapError ErrorQuadstore
                        |> ConcurrentTask.attempt
                            { send = Ports.send
                            , pool = subs.pool
                            , onComplete = OnComplete
                            }
            in
            ( model
            , Callbacks
                { subs
                    | getTrigs = appendOnResponse iri onResponse subs.getTrigs
                    , pool = poolUpdated
                }
            , cmdPool
                |> Effect.fromCmd
                |> Effect.map (MsgTask >> toMsgMain)
            )

        MsgTask (OnComplete (ConcurrentTask.Success (QuadstoreParsedTrig iriRequested graphs))) ->
            let
                ( poolUpdated, cmdPool ) =
                    (storeGraphs ++ storeGraphsAliased)
                        |> ConcurrentTask.sequence
                        |> ConcurrentTask.map (\_ -> QuadstoreStoredTrig iriRequested graphs)
                        |> ConcurrentTask.mapError ErrorQuadstore
                        |> ConcurrentTask.attempt
                            { send = Ports.send
                            , pool = subs.pool
                            , onComplete = OnComplete
                            }

                storeGraphs =
                    graphs
                        |> DictIri.toList
                        |> List.map storeGraph

                storeGraph ( iriGraph, graph ) =
                    Store.storeGraph iriGraph graph

                storeGraphsAliased =
                    graphs
                        |> DictIri.toList
                        |> List.concatMap storeGraphAliased

                storeGraphAliased ( iriGraph, graph ) =
                    graph
                        |> Rdf.Decode.decode (Rdf.Decode.from iriGraph decoderSources)
                        |> Result.withDefault []
                        |> List.map (\iriSource -> Store.storeGraph iriSource graph)
            in
            ( model
            , Callbacks { subs | pool = poolUpdated }
            , cmdPool
                |> Effect.fromCmd
                |> Effect.map (MsgTask >> toMsgMain)
            )

        MsgTask (OnComplete (ConcurrentTask.Success (QuadstoreStoredTrig iriRequested graphs))) ->
            if DictIri.isEmpty graphs then
                let
                    graphsUpdated =
                        DictIriApiData.insert iriRequested (ApiData.Success Rdf.emptyGraph) data.graphs

                    subsUpdated =
                        { subs
                            | getGraphs = DictIri.remove iriRequested subs.getGraphs
                            , getTrigs = DictIri.remove iriRequested subs.getTrigs
                        }
                in
                ( Model { data | graphs = graphsUpdated }
                , Callbacks subsUpdated
                , [ sendEventForCacheChange toMsgMain eventsMain data (handleGraphUpdated iriRequested Rdf.emptyGraph)
                  , sendOnResponses iriRequested (Ok Rdf.emptyGraph) subs.getGraphs
                  , sendOnResponses iriRequested (Ok graphs) subs.getTrigs
                  ]
                    |> Effect.batch
                )

            else
                let
                    final =
                        graphs
                            |> DictIri.toList
                            |> List.foldl (\( iri, graph ) -> insertCacheDict iri (Ok graph))
                                { data = data.graphs
                                , subs = subs.getGraphs
                                , effects = []
                                }

                    subsUpdated =
                        { subs
                            | getGraphs = DictIri.remove iriRequested final.subs
                            , getTrigs = DictIri.remove iriRequested subs.getTrigs
                        }
                in
                ( Model { data | graphs = final.data }
                , Callbacks subsUpdated
                , [ DictIri.toList graphs
                        |> List.map (\( iri, graph ) -> sendEventForCacheChange toMsgMain eventsMain data (handleGraphUpdated iri graph))
                        |> Effect.batch
                  , Effect.batch final.effects
                  , sendOnResponses iriRequested (Ok graphs) subs.getTrigs
                  ]
                    |> Effect.batch
                )

        ReceivedAction (Query onRows query) ->
            let
                ( poolUpdated, cmdPool ) =
                    query
                        |> Store.query
                        |> ConcurrentTask.map (QuadstoreReturnedQueryResult onRows)
                        |> ConcurrentTask.mapError ErrorQuadstore
                        |> ConcurrentTask.attempt
                            { send = Ports.send
                            , pool = subs.pool
                            , onComplete = OnComplete
                            }
            in
            ( model
            , Callbacks { subs | pool = poolUpdated }
            , cmdPool
                |> Effect.fromCmd
                |> Effect.map (MsgTask >> toMsgMain)
            )

        MsgTask (OnComplete (ConcurrentTask.Success (QuadstoreReturnedQueryResult onRows rows))) ->
            ( model
            , callbacks
            , Effect.fromMsg (onRows rows)
            )

        -- DATA
        ApiReturnedAuthors response ->
            ( Model { data | context = { context | authors = RemoteData.fromResult response } }
            , callbacks
            , Effect.none
            )

        ApiReturnedWorkspaces response ->
            ( Model { data | context = { context | workspaces = RemoteData.fromResult response } }
            , callbacks
            , Effect.none
            )

        ApiReturnedWorkspaceCurrent (Err _) ->
            ( model
            , callbacks
            , { message = verbatim "Could not load current workspace." }
                |> Action.AddToast
                |> Effect.fromAction
            )

        ApiReturnedWorkspaceCurrent (Ok workspace) ->
            ( Model { data | context = { context | workspace = Just workspace } }
            , callbacks
            , Effect.none
            )

        -- CLIPBOARD
        ReceivedAction (CopyToClipboard value) ->
            ( Model data
            , callbacks
            , [ Ports.copyToClipboard value
                    |> Effect.fromCmd
              , { message =
                    "Copied to clipboard"
                        |> verbatim
                }
                    |> Action.AddToastExpireIn 3000
                    |> Effect.fromAction
              ]
                |> Effect.batch
            )

        -- CONCURRENT TASKS
        MsgTask (OnComplete (ConcurrentTask.Success QuadstoreDeletedGraphs)) ->
            ( model
            , callbacks
            , Effect.none
            )

        MsgTask (OnComplete (ConcurrentTask.Error (ErrorQuadstore (Store.Error error)))) ->
            ( model
            , callbacks
            , { message = verbatim ("Quadstore error: " ++ error) }
                |> Action.AddToast
                |> Effect.fromAction
            )

        MsgTask (OnComplete (ConcurrentTask.UnexpectedError error)) ->
            ( model
            , callbacks
            , { message = verbatim ("Something unexpected happened: " ++ unexpectedErrorToString error) }
                |> Action.AddToast
                |> Effect.fromAction
            )

        MsgTask (OnProgress ( poolUpdated, cmdPool )) ->
            ( model
            , Callbacks { subs | pool = poolUpdated }
            , cmdPool
                |> Effect.map (MsgTask >> toMsgMain)
            )

        MsgTask (OnComplete (ConcurrentTask.Error (ErrorOpenTab error))) ->
            ( model
            , callbacks
            , { message = verbatim ("Open tab: " ++ error) }
                |> Action.AddToast
                |> Effect.fromAction
            )

        MsgTask (OnComplete (ConcurrentTask.Success OpenedTab)) ->
            ( model
            , callbacks
            , Effect.none
            )


createFields :
    C
    -> UUID.Seeds
    -> (Api.Response Decoded.Field -> msg)
    -> Bool
    -> Path
    -> Decoded.Mint
    -> Grouped.Field
    -> Effect (Msg msg)
createFields c seeds onField preview path mint field =
    if preview then
        seeds
            |> Mint.run (Decoded.create path mint field)
            |> Task.attempt (CreatedField onField)
            |> Effect.fromCmd

    else
        seeds
            |> Mint.runWith (configDefault c) (Decoded.create path mint field)
            |> Task.attempt (CreatedField onField)
            |> Effect.fromCmd


duplicateFields :
    C
    -> UUID.Seeds
    -> (Api.Response Decoded.Field -> msg)
    -> Bool
    -> Path
    -> Decoded.Mint
    -> Decoded.Field
    -> Effect (Msg msg)
duplicateFields c seeds onField preview path mint field =
    if preview then
        seeds
            |> Mint.run (Decoded.duplicate path mint field)
            |> Task.attempt (DuplicatedField onField)
            |> Effect.fromCmd

    else
        seeds
            |> Mint.runWith (configDefault c) (Decoded.duplicate path mint field)
            |> Task.attempt (DuplicatedField onField)
            |> Effect.fromCmd


configDefault : C -> Mint.Config Api.Error
configDefault c =
    { mintIri =
        \seeds nodeFocus class ->
            let
                pathSegments =
                    nodeFocus
                        |> Rdf.toIri
                        |> Maybe.map
                            (Rdf.toUrl
                                >> String.split "/"
                                >> List.dropWhile ((/=) "graph")
                                >> List.takeWhile ((/=) "")
                            )
                        |> Maybe.withDefault [ "graph" ]

                nameClass =
                    class
                        |> Rdf.defaultLabel
                        |> Maybe.map String.dasherize
                        |> Maybe.withDefault "resource"
            in
            Graph.mintTask c pathSegments nameClass
                |> Task.map (\iri -> ( iri, seeds ))
    }


respondWithError : (Api.Response () -> msg) -> Api.Response a -> Effect msg
respondWithError onResponse error =
    error
        |> Result.map (always ())
        |> onResponse
        |> Effect.fromMsg


getGraphTurtle : C -> Action.ConfigGetGraph -> Iri -> Effect (Msg msg)
getGraphTurtle c config iri =
    Graph.get c
        { iri = iri
        , onResponse = ApiReturnedGraphTurtle config iri
        }


unexpectedErrorToString : ConcurrentTask.UnexpectedError -> String
unexpectedErrorToString error =
    case error of
        ConcurrentTask.UnhandledJsException { function, message } ->
            function ++ ": " ++ message

        ConcurrentTask.ResponseDecoderFailure { function } ->
            function ++ ": response decoder failure"

        ConcurrentTask.ErrorsDecoderFailure { function } ->
            function ++ ": errors decoder failure"

        ConcurrentTask.MissingFunction name ->
            "missing function: " ++ name

        ConcurrentTask.InternalError message ->
            "internal error: " ++ message


appendOnResponse : Iri -> (response -> msg) -> DictIri (List (response -> msg)) -> DictIri (List (response -> msg))
appendOnResponse iri onResponse subs =
    DictIri.update iri
        (Maybe.map ((::) onResponse)
            >> Maybe.withDefault [ onResponse ]
            >> Just
        )
        subs


sendOnResponses : Iri -> response -> DictIri (List (response -> msg)) -> Effect msg
sendOnResponses iri response subs =
    DictIri.get iri subs
        |> Maybe.withDefault []
        |> List.map (\onResponse -> Effect.fromMsg (onResponse response))
        |> Effect.batch


type alias CacheStepDictConfig error data msgMain =
    { onResponse : Result error data -> msgMain
    , get : Effect msgMain
    , reload : Bool
    }


type alias CacheStepDict error data msgMain =
    { data : DictIriApiData error data
    , subs : DictIri (List (Result error data -> msgMain))
    , effects : List (Effect msgMain)
    }


getCacheDict : CacheStepDictConfig error data msgMain -> Iri -> CacheStepDict error data msgMain -> CacheStepDict error data msgMain
getCacheDict { onResponse, get, reload } iri step =
    let
        subsWithOnResponse =
            DictIri.update iri
                (Maybe.map ((::) onResponse)
                    >> Maybe.withDefault [ onResponse ]
                    >> Just
                )
                step.subs

        sendResult result =
            { step
                | subs = DictIri.remove iri step.subs
                , effects =
                    (DictIri.get iri step.subs
                        |> Maybe.withDefault []
                        |> (::) onResponse
                        |> List.map (\onResponseOther -> Effect.fromMsg (onResponseOther result))
                    )
                        ++ step.effects
            }
    in
    case DictIriApiData.get iri step.data of
        ApiData.NotAsked ->
            { step
                | data = DictIriApiData.insert iri ApiData.Loading step.data
                , subs = subsWithOnResponse
                , effects = get :: step.effects
            }

        ApiData.Loading ->
            { step | subs = subsWithOnResponse }

        ApiData.Failure error ->
            sendResult (Err error)

        ApiData.Success nTriples ->
            if reload then
                { step
                    | data = DictIriApiData.insert iri (ApiData.Reloading nTriples) step.data
                    , subs = subsWithOnResponse
                    , effects = get :: step.effects
                }

            else
                sendResult (Ok nTriples)

        ApiData.Reloading _ ->
            { step | subs = subsWithOnResponse }


insertCacheDict : Iri -> Result error data -> CacheStepDict error data msgMain -> CacheStepDict error data msgMain
insertCacheDict iri result step =
    { step
        | data = DictIriApiData.insert iri (ApiData.fromResult result) step.data
        , subs = DictIri.remove iri step.subs
        , effects = sendOnResponses iri result step.subs :: step.effects
    }


sendEventForCacheChange : (Msg msgMain -> msgMain) -> Event msgMain -> Data -> (Event.CacheChange msgMain -> Effect msgMain) -> Effect msgMain
sendEventForCacheChange toMsgMain eventsMain data cacheChange =
    [ Event.map (toMsgMain << MsgWorkspace) (Shared.Workspace.events data.workspace)
    , eventsMain
    , Event.map toMsgMain (events data)
    ]
        |> Event.batch
        |> Event.cacheChangesToEffect cacheChange


handleTriplesChanged : Event.CacheChange msgMain -> Effect msgMain
handleTriplesChanged cacheChange =
    case cacheChange of
        Event.TriplesChanged msg ->
            Effect.fromMsg msg

        _ ->
            Effect.none


handleGraphUpdated : Iri -> Graph -> Event.CacheChange msgMain -> Effect msgMain
handleGraphUpdated iri graph cacheChange =
    case cacheChange of
        Event.GraphUpdated toMsg iriEvent ->
            if iriEvent == iri then
                graph
                    |> toMsg
                    |> Effect.fromMsg

            else
                Effect.none

        _ ->
            Effect.none


setContext : Data -> Callbacks msgMain -> C -> ( Model, Callbacks msgMain, Effect msgMain )
setContext data callbacks contextUpdated =
    ( Model { data | context = contextUpdated }
    , callbacks
    , Effect.none
    )


substractSandboxBox : Viewport -> Viewport
substractSandboxBox viewport =
    { width = viewport.width - 32
    , height = viewport.height - 32
    }


openTab : String -> ConcurrentTask ErrorTask ()
openTab url =
    ConcurrentTask.define
        { function = "openTab"
        , expect = ConcurrentTask.expectWhatever
        , errors = ConcurrentTask.expectThrows ErrorOpenTab
        , args = Encode.object [ ( "url", Encode.string url ) ]
        }


decoderSources : Rdf.Decode.Decoder (List Iri)
decoderSources =
    Rdf.Decode.predicate (dcterms "source") (Rdf.Decode.many Rdf.Decode.iri)



-- SUBSCRIPTIONS


subscriptions : Model -> Callbacks msgMain -> Sub (Msg msgMain)
subscriptions (Model data) (Callbacks callbacks) =
    Sub.batch
        [ Sub.map MsgWorkspace (Shared.Workspace.subscriptions data.context data.workspace)
        , Time.every (30 * 1000) BrowserReturnedCurrentTime
        , Viewport.onResize UserResizedBrowser
        , subscriptionsCache data.context data.triplesNext

        -- CONCURRENT TASKS
        , callbacks.pool
            |> ConcurrentTask.onProgress
                { send = Ports.send
                , receive = Ports.receive
                , onProgress = Tuple.mapSecond Effect.fromCmd >> OnProgress
                }
            |> Sub.map MsgTask
        ]


events : Data -> Event (Msg msgMain)
events _ =
    Event.none



-- VIEW


view : Url -> { page : View msg, toMsg : Msg msgMain -> msg } -> Model -> View msg
view url { page, toMsg } (Model data) =
    case ( data.context.device.class, data.context.device.orientation ) of
        ( Phone, _ ) ->
            viewPhone url page toMsg data

        ( Tablet, _ ) ->
            viewTablet url page toMsg data

        ( Desktop, Portrait ) ->
            if data.context.viewport.width < 1300 + 32 then
                viewTablet url page toMsg data

            else
                viewDesktop url page toMsg data

        ( Desktop, Landscape ) ->
            if data.context.viewport.width < 1300 + 32 then
                viewTablet url page toMsg data

            else
                viewDesktop url page toMsg data

        ( BigDesktop, _ ) ->
            viewDesktop url page toMsg data


viewPhone : Url -> View msg -> (Msg msgMain -> msg) -> Data -> View msg
viewPhone url page toMsg data =
    { title = page.title
    , element = viewBodyPhone toMsg page data
    , overlays =
        [ [ map toMsg (viewOverlayWorkspace page.fabVisible data)
          , map toMsg (viewOverlayNavigationPhone data.context url)
          , map toMsg (viewOverlayHelp data)
          , map toMsg (viewOverlayDialog data)
          ]
        , page.overlays
        , [ map toMsg (viewTray data.tray)
          ]
        ]
            |> List.concat
    , breadcrumbs = page.breadcrumbs
    , fabVisible = page.fabVisible
    , navigationRailVisible = page.navigationRailVisible
    , loading = page.loading
    }


viewTablet : Url -> View msg -> (Msg msgMain -> msg) -> Data -> View msg
viewTablet url page toMsg data =
    { title = page.title
    , element = viewBodyTablet url toMsg page data
    , overlays =
        [ [ map toMsg (viewOverlayWorkspace page.fabVisible data)
          , map toMsg (viewOverlayNavigation data.context url)
          , map toMsg (viewOverlayHelp data)
          , map toMsg (viewOverlayDialog data)
          ]
        , page.overlays
        , [ map toMsg (viewTray data.tray)
          ]
        ]
            |> List.concat
    , breadcrumbs = page.breadcrumbs
    , fabVisible = page.fabVisible
    , navigationRailVisible = page.navigationRailVisible
    , loading = page.loading
    }


viewDesktop : Url -> View msg -> (Msg msgMain -> msg) -> Data -> View msg
viewDesktop url page toMsg data =
    { title = page.title
    , element = viewBodyDesktop url toMsg page data
    , overlays =
        [ [ map toMsg (viewOverlayWorkspace page.fabVisible data)
          , map toMsg (viewOverlayNavigation data.context url)
          , map toMsg (viewOverlayHelp data)
          , map toMsg (viewOverlayDialog data)
          ]
        , page.overlays
        , [ map toMsg (viewTray data.tray)
          ]
        ]
            |> List.concat
    , breadcrumbs = page.breadcrumbs
    , fabVisible = page.fabVisible
    , navigationRailVisible = page.navigationRailVisible
    , loading = page.loading
    }


viewBodyPhone : (Msg msgMain -> msg) -> View msg -> Data -> Element msg
viewBodyPhone toMsg page data =
    Keyed.column
        [ width fill
        , height fill
        ]
        [ ( "viewTopAppBar"
          , viewTopAppBar data.context toMsg True page.breadcrumbs
          )
        , ( "main"
          , viewWorkspace toMsg data page.element
                |> elWithAncestorWithScrollbarY
                    [ width (fillPortion 8)
                    , height fill
                    , Background.color surface
                    ]
          )
        , ( "Portal.target"
          , Lazy.lazy (\_ -> Portal.target) ()
          )
        ]


viewBodyTablet : Url -> (Msg msgMain -> msg) -> View msg -> Data -> Element msg
viewBodyTablet url toMsg page data =
    Keyed.column
        [ width fill
        , height fill
        ]
        [ ( "viewTopAppBar"
          , viewTopAppBar data.context toMsg False page.breadcrumbs
          )
        , ( "progressIndicator"
          , if page.loading then
                ProgressIndicatorLinear.view ProgressIndicatorLinear.Indeterminate

            else
                el
                    [ width fill
                    , height (px 4)
                    ]
                    none
          )
        , ( "main"
          , [ if page.navigationRailVisible then
                viewNavigationRail data.context url page

              else
                none
            , viewWorkspace toMsg data page.element
                |> elWithAncestorWithScrollbarY
                    [ width fill
                    , height fill
                    ]
            ]
                |> rowWithAncestorWithScrollbarY
                    [ width fill
                    , height fill
                    , Background.color surface
                    ]
          )
        , ( "Portal.target"
          , Lazy.lazy (\_ -> Portal.target) ()
          )
        ]


viewBodyDesktop : Url -> (Msg msgMain -> msg) -> View msg -> Data -> Element msg
viewBodyDesktop url toMsg page data =
    Keyed.column
        [ width fill
        , height fill
        ]
        [ ( "viewTopAppBar"
          , viewTopAppBar data.context toMsg False page.breadcrumbs
          )
        , ( "progressIndicator"
          , if page.loading then
                ProgressIndicatorLinear.view ProgressIndicatorLinear.Indeterminate

            else
                el
                    [ width fill
                    , height (px 4)
                    , Background.color surface
                    ]
                    none
          )
        , ( "main"
          , [ [ if page.navigationRailVisible then
                    viewNavigationRail data.context url page

                else
                    none
              , viewWorkspace toMsg data page.element
              ]
                |> rowWithAncestorWithScrollbarY
                    [ width fill
                    , height fill
                    ]
            ]
                |> rowWithAncestorWithScrollbarY
                    [ width fill
                    , height fill
                    , Background.color surface
                    ]
          )
        , ( "Portal.target"
          , Lazy.lazy (\_ -> Portal.target) ()
          )
        ]


viewTopAppBar : C -> (Msg msgMain -> msg) -> Bool -> Breadcrumbs -> Element msg
viewTopAppBar c toMsg compact breadcrumbs =
    [ [ Icon.Menu
            |> buttonIcon (toMsg UserPressedLogo)
            |> ButtonIcon.toElement
            |> el
                [ centerX
                , centerY
                ]
            |> el
                [ width (px 64)
                , centerY
                ]
      , el
            [ height fill
            , width (px 96)
            , padding 8
            ]
            Logo.herbie
      ]
        |> row
            [ spacing 8
            , centerY
            ]
    , if compact then
        none

      else
        Breadcrumbs.toElement breadcrumbs
    , (String.left 1 c.author.user.firstName ++ String.left 1 c.author.user.lastName)
        |> Element.text
        |> el
            [ centerX
            , centerY
            , Font.size 16
            , Font.medium
            ]
        |> el
            [ width (px 40)
            , height (px 40)
            , Border.rounded 20
            , Background.color surfaceContainerHigh
            , alignRight
            ]
    ]
        |> row
            [ width fill
            , height (px 48)
            , spacing 32
            , paddingXY 8 0
            , Background.color Color.surface
            ]


viewNavigationRail : C -> Url -> View msg -> Element msg
viewNavigationRail c url page =
    let
        activeExplorer =
            case Route.fromUrl url of
                Route.Graph ->
                    NavigationRail.active

                _ ->
                    identity

        activeDocuments =
            case Route.fromUrl url of
                Route.Datasets ->
                    NavigationRail.active

                Route.Datasets__Uuid_ _ ->
                    NavigationRail.active

                Route.Datasets__Uuid___Draft _ ->
                    NavigationRail.active

                Route.Datasets__Uuid___Versions__Index_ _ ->
                    NavigationRail.active

                _ ->
                    identity
    in
    if page.navigationRailVisible then
        [ Icon.TravelExplore
            |> NavigationRail.item (Href.fromRoute c Route.Graph) (verbatim "Explorer")
            |> activeExplorer
            |> Just
        , if c.useSparklis && c.author.permissions.canSendSparqlQueries then
            Icon.SavedSearch
                |> NavigationRail.item
                    (Url.Builder.absolute
                        [ "static", "sparklis", "osparklis.html" ]
                        [ Url.Builder.string "endpoint"
                            ("/query/" ++ Maybe.withDefault "" c.workspaceUuid ++ "/")
                        , Url.Builder.string "method_get" "true"
                        , Url.Builder.string "withCredentials" "true"
                        ]
                    )
                    (verbatim "Sparklis")
                |> NavigationRail.newTab
                |> Just

          else
            Nothing
        , Icon.Article
            |> NavigationRail.item (Href.fromRoute c Route.Datasets) (verbatim "Documents")
            |> activeDocuments
            |> Just
        ]
            |> List.filterMap identity
            |> NavigationRail.fromItems
            |> NavigationRail.view

    else
        none


viewWorkspace : (Msg msgMain -> msg) -> Data -> Element msg -> Element msg
viewWorkspace toMsg data element =
    [ element
        |> elWithAncestorWithScrollbars
            [ width fill
            , height fill
            ]
    , data.workspace
        |> Shared.Workspace.view data.context
        |> map MsgWorkspace
        |> map toMsg
    ]
        |> columnWithAncestorWithScrollbarX
            [ width fill
            , height fill
            , id "body"
            ]


viewOverlayWorkspace : Bool -> Data -> Element (Msg msgMain)
viewOverlayWorkspace fabVisible data =
    if fabVisible then
        data.workspace
            |> Shared.Workspace.viewOverlayFabMenu data.context
            |> map MsgWorkspace

    else
        none


viewOverlayHelp : Data -> Element (Msg msgMain)
viewOverlayHelp data =
    if data.showHelp then
        DialogHelp.view
            { onScroll = UserScrolledHelp
            , onClose = UserPressedCloseHelp
            }
            data.helpOffsets

    else
        none


viewOverlayDialog : Data -> Element (Msg msgMain)
viewOverlayDialog _ =
    none


viewTray : Toast.Tray -> Element (Msg msgMain)
viewTray tray =
    Toast.render tray
        |> map MsgToast


viewOverlayNavigationPhone : C -> Url -> Element (Msg msgMain)
viewOverlayNavigationPhone c url =
    if c.navigationVisible then
        viewNavigation c url True
            |> el
                [ width fill
                , height fill
                , pointerEvents True
                , userSelect True
                ]

    else
        none


viewOverlayNavigation : C -> Url -> Element (Msg msgMain)
viewOverlayNavigation c url =
    if c.navigationVisible then
        [ [ viewNavigation c url False
                |> el
                    [ height fill
                    , width (px 360)
                    , pointerEvents True
                    , userSelect True
                    ]
          , el
                [ width fill
                , height fill
                , Background.color scrim
                , pointerEvents True
                , userSelect True
                , stopPropagationOnClick UserPressedLogo
                ]
                none
          ]
            |> row
                [ width fill
                , height fill
                ]
        ]
            |> column
                [ width fill
                , height fill
                , pointerEvents False
                , userSelect False
                ]

    else
        none


viewNavigation : C -> Url -> Bool -> Element (Msg msgMain)
viewNavigation =
    lazy3
        (\c url fill ->
            Navigation.view c
                { items = navigationItems c url
                , fill = fill
                , expanded = c.navigationVisible
                , onToggleExpanded = UserPressedLogo
                , userPressedShowHelp = UserPressedShowHelp
                , userPressedSendFeedback = UserPressedSendFeedback
                }
        )


navigationItems : C -> Url -> List (Navigation.Item (Msg msg))
navigationItems c url =
    let
        route =
            Route.fromUrl url

        label id =
            t ("navigation-item--" ++ id)

        workspaces =
            RemoteData.withDefault [] c.workspaces
    in
    [ case c.workspace of
        Nothing ->
            Nothing

        Just workspace ->
            [ "graph"
                |> label
                |> Navigation.itemLink (Href.fromRoute c Route.Graph) Icon.TravelExplore
                |> Navigation.isSelected (route == Route.Graph)
            , "sparklis"
                |> label
                |> Navigation.itemLink
                    (Url.Builder.absolute
                        [ "static", "sparklis", "osparklis.html" ]
                        [ Url.Builder.string "endpoint" "/query/"
                        , Url.Builder.string "method_get" "true"
                        , Url.Builder.string "withCredentials" "true"
                        ]
                    )
                    Icon.SavedSearch
                |> Navigation.newTab
            , "query"
                |> label
                |> Navigation.itemLink (Href.fromRoute c Route.Query) Icon.Search
                |> Navigation.isSelected (route == Route.Query)
            , "documents"
                |> label
                |> Navigation.itemLink (Href.fromRoute c Route.Datasets) Icon.Article
                |> Navigation.isSelected
                    (case route of
                        Route.Datasets ->
                            True

                        Route.Datasets__Uuid_ _ ->
                            True

                        Route.Datasets__Uuid___Draft _ ->
                            True

                        Route.Datasets__Uuid___Versions__Index_ _ ->
                            True

                        _ ->
                            False
                    )
            , "Settings"
                |> verbatim
                |> Navigation.itemLink (Href.fromRoute c (Route.Workspaces__Uuid_ { uuid = workspace.uuid })) Icon.Settings
                |> Navigation.isSelected
                    (case route of
                        Route.Workspaces__Uuid_ { uuid } ->
                            uuid == workspace.uuid

                        _ ->
                            False
                    )
            ]
                |> Navigation.itemGroup
                |> Navigation.withLabel (verbatim workspace.name)
                |> Just
    , if List.isEmpty workspaces then
        Nothing

      else
        workspaces
            |> List.map (navigationItemWorkspace c route)
            |> Navigation.itemGroup
            |> Navigation.withLabel (verbatim "Switch workspace")
            |> Just
    , [ "Workspaces"
            |> verbatim
            |> Navigation.itemLink (Href.fromRoute c Route.Workspaces) Icon.Workspaces
            |> Navigation.isSelected (route == Route.Workspaces)
      , "Documentation"
            |> verbatim
            |> Navigation.itemLink (Href.fromRoute c Route.Documentation) Icon.HelpCenter
            |> Navigation.isSelected (route == Route.Documentation)
      , "settings"
            |> label
            |> Navigation.itemLink (Href.fromRoute c Route.Settings) Icon.Settings
            |> Navigation.isSelected (route == Route.Settings)
      , "logout"
            |> label
            |> Navigation.itemButton UserPressedLogout Icon.Logout
      ]
        |> Navigation.itemGroup
        |> Just
    ]
        |> List.filterMap identity


navigationItemWorkspace : C -> Route -> Workspace -> Navigation.Item (Msg msg)
navigationItemWorkspace c route workspace =
    let
        icon =
            if c.workspaceUuid == Just workspace.uuid then
                Icon.RadioButtonChecked

            else
                Icon.RadioButtonUnchecked

        href =
            case route of
                Route.Datasets__Uuid_ _ ->
                    if c.workspaceUuid == Just workspace.uuid then
                        Href.fromRoute c route

                    else
                        Href.fromRoute c (Route.Workspaces__Uuid_ { uuid = workspace.uuid })

                Route.Datasets__Uuid___Draft _ ->
                    if c.workspaceUuid == Just workspace.uuid then
                        Href.fromRoute c route

                    else
                        Href.fromRoute c (Route.Workspaces__Uuid_ { uuid = workspace.uuid })

                Route.Datasets__Uuid___Versions__Index_ _ ->
                    if c.workspaceUuid == Just workspace.uuid then
                        Href.fromRoute c route

                    else
                        Href.fromRoute c (Route.Workspaces__Uuid_ { uuid = workspace.uuid })

                Route.Workspaces__Uuid_ _ ->
                    Href.fromRoute c (Route.Workspaces__Uuid_ { uuid = workspace.uuid })

                _ ->
                    Href.fromRouteWith c (Dict.singleton "workspace" workspace.uuid) route
    in
    workspace.name
        |> verbatim
        |> Navigation.itemLink href icon



-- FLUENT


t : String -> Fluent
t key =
    Fluent.text ("shared--" ++ key)



-- CACHE


initCache : C -> Pool msg -> ( Pool msg, Effect (MsgTask msg) )
initCache c pool =
    if useCache c then
        let
            ( poolUpdated, effectPool ) =
                getItemTriplesPrev pool

            ( poolFinal, effectPoolFinal ) =
                getItemTriplesNext poolUpdated
        in
        ( poolFinal
        , Effect.batch
            [ effectPool
            , effectPoolFinal
            ]
        )

    else
        ( ConcurrentTask.pool, Effect.none )


subscriptionsCache : C -> Maybe String -> Sub (Msg msg)
subscriptionsCache c triplesNext =
    if useCache c then
        case triplesNext of
            Nothing ->
                Sub.none

            Just _ ->
                Time.every (60 * 1000) (\_ -> BrowserWaitedForTriplesNext)

    else
        Sub.none


useCache : C -> Bool
useCache c =
    Dict.member "cache" c.query


getItemTriplesPrev : Pool msg -> ( Pool msg, Effect (MsgTask msg) )
getItemTriplesPrev pool =
    Decode.oneOf
        [ Decode.null Nothing
        , Decode.map Just Decode.string
        ]
        |> Localstorage.getItem "triples:prev"
        |> ConcurrentTask.map LocalstorageReturnedPrev
        |> ConcurrentTask.mapError ErrorLocalstorageReadPrev
        |> ConcurrentTask.attempt
            { send = Ports.send
            , pool = pool
            , onComplete = OnComplete
            }
        |> Tuple.mapSecond Effect.fromCmd


getItemTriplesNext : Pool msg -> ( Pool msg, Effect (MsgTask msg) )
getItemTriplesNext pool =
    Decode.string
        |> Localstorage.getItem "triples:next"
        |> ConcurrentTask.map LocalstorageReturnedNext
        |> ConcurrentTask.mapError ErrorLocalstorageReadNext
        |> ConcurrentTask.attempt
            { send = Ports.send
            , pool = pool
            , onComplete = OnComplete
            }
        |> Tuple.mapSecond Effect.fromCmd


setItemTriplesPrev : Maybe String -> Pool msg -> ( Pool msg, Effect (MsgTask msg) )
setItemTriplesPrev prev pool =
    prev
        |> Maybe.unwrap Encode.null Encode.string
        |> Localstorage.setItem "triples:prev"
        |> ConcurrentTask.map LocalstorageSetItem
        |> ConcurrentTask.mapError ErrorLocalstorageWrite
        |> ConcurrentTask.attempt
            { send = Ports.send
            , pool = pool
            , onComplete = OnComplete
            }
        |> Tuple.mapSecond Effect.fromCmd


setItemTriplesNext : String -> Pool msg -> ( Pool msg, Effect (MsgTask msg) )
setItemTriplesNext next pool =
    next
        |> Encode.string
        |> Localstorage.setItem "triples:next"
        |> ConcurrentTask.map LocalstorageSetItem
        |> ConcurrentTask.mapError ErrorLocalstorageWrite
        |> ConcurrentTask.attempt
            { send = Ports.send
            , pool = pool
            , onComplete = OnComplete
            }
        |> Tuple.mapSecond Effect.fromCmd


removeItemTriples : Pool msg -> ( Pool msg, Effect (MsgTask msg) )
removeItemTriples pool =
    Localstorage.removeItem "triples:prev"
        |> ConcurrentTask.andThenDo (Localstorage.removeItem "triples:next")
        |> ConcurrentTask.andThenDo
            (Store.clear
                |> ConcurrentTask.map QuadstoreCleared
                |> ConcurrentTask.mapError ErrorQuadstore
            )
        |> ConcurrentTask.attempt
            { send = Ports.send
            , pool = pool
            , onComplete = OnComplete
            }
        |> Tuple.mapSecond Effect.fromCmd


preloadDocumentsFrom : Graph -> Effect (Msg msg)
preloadDocumentsFrom graph =
    Rdf.emptyQuery
        |> Rdf.withPredicate a
        |> Rdf.withObject (herbie "Document")
        |> Rdf.getIriSubjects graph
        |> List.map preloadDocument
        |> Effect.batch


preloadDocument : Iri -> Effect (Msg msg)
preloadDocument iri =
    iri
        |> Action.GetGraph
            { store = True
            , reload = True
            , closure = False
            }
            (\_ -> Ignore)
        |> Effect.fromAction
