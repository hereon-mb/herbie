module String.Extra.Extra exposing
    ( slugify
    , forTextInput
    )

{-|

@docs slugify
@docs forTextInput

-}


{-| Clean up a string to be a proper value for a single line text input field by

  - replacing '\\t' with ' '
  - replacing '\\n' with ' '
  - droping '\\r'

-}
forTextInput : String -> String
forTextInput =
    String.replace "\t" " "
        >> String.replace "\n" " "
        >> String.replace "\u{000D}" ""


{-| Replace all spaces with underscores and all none-alphanumeric symbols with
dashes.
-}
slugify : String -> String
slugify =
    String.toLower
        >> String.map slugifyChar


slugifyChar : Char -> Char
slugifyChar char =
    if Char.isAlphaNum char then
        char

    else if char == ' ' then
        '_'

    else
        '-'
