// Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
// a client-server based web application for research documentation
//
// Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
//
// Authors: Fabian Kirchner, Catriona Eschke
//
// This program is subject to the terms and conditions for non-commercial use of
// ELN. You can find the license text in the file LICENSE.en and under
// http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
// you have any questions or comments, you can contact us at hereon at
// herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
// Max-Planck-Straße 1, 21502 Geesthacht, Germany.

import { Elm } from "./Ui/Molecule/DatePicker.elm";
import { registerCustomElementUiAtomChoiceChips } from "./Ui/Atom/ChoiceChips.js";
import { registerCustomElementUiAtomSegmentedButtonsSingleSelect } from "./Ui/Atom/SegmentedButtonsSingleSelect.js";
import { registerCustomElementUiAtomTextInputAffixSelect } from "./Ui/Atom/TextInputAffixSelect.js";
import { registerCustomElementUiMoleculeDropdown } from "./Ui/Molecule/Dropdown.js";
import { registerCustomElementUiMoleculeDatePicker } from "./Ui/Molecule/DatePicker.js";
import { registerCustomElementUiMoleculeComboboxMultiselect } from "./Ui/Molecule/ComboboxMultiselect.js";
import { registerCustomElementUiMoleculeComboboxSingleselect } from "./Ui/Molecule/ComboboxSingleselect.js";
import { registerCustomElementCodeMirror } from "./Ui/Molecule/CodeMirror.js";


export function registerCustomElements() {
  registerCustomElementUiAtomChoiceChips(Elm);
  registerCustomElementUiAtomSegmentedButtonsSingleSelect(Elm);
  registerCustomElementUiAtomTextInputAffixSelect(Elm);
  registerCustomElementUiMoleculeDropdown(Elm);
  registerCustomElementUiMoleculeDatePicker(Elm);
  registerCustomElementUiMoleculeComboboxMultiselect(Elm);
  registerCustomElementUiMoleculeComboboxSingleselect(Elm);
  registerCustomElementCodeMirror();
}
