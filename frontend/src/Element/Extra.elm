{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Element.Extra exposing
    ( ariaActivedescendant
    , ariaChecked
    , ariaCheckedRaw
    , ariaControls
    , ariaLabel
    , ariaLabelledby
    , ariaMultiselectable
    , ariaSelected
    , class
    , colorToRgba
    , columnWithAncestorWithScrollbarX
    , disabled
    , elWithAncestorWithScrollbarX
    , elWithAncestorWithScrollbarY
    , elWithAncestorWithScrollbars
    , id
    , loadingAnimation
    , loadingAnimationContainerLow
    , maxlength
    , onBlur
    , onFocus
    , onFocusIn
    , onFocusOut
    , onKeyDown
    , onMouseDown
    , onMouseUp
    , onScroll
    , pointerEvents
    , preventDefaultAndStopPropagationOnClick
    , preventDefaultOnClick
    , preventDefaultOnKeyDown
    , role
    , rowWithAncestorWithScrollbarY
    , stopPropagationAndPreventDefaultOnClick
    , stopPropagationOnClick
    , style
    , tabindex
    , userSelect
    )

import Accessibility.Key
import Element
    exposing
        ( Attribute
        , Color
        , Element
        , clip
        , column
        , el
        , htmlAttribute
        , row
        , toRgb
        )
import Html.Attributes
import Html.Events
import Json.Decode as Decode exposing (Decoder)


elWithAncestorWithScrollbarY : List (Attribute msg) -> Element msg -> Element msg
elWithAncestorWithScrollbarY attrs =
    -- TODO workaround for https://github.com/mdgriffith/elm-ui/issues/12
    el (clip :: style "flex-shrink" "1" :: attrs)


rowWithAncestorWithScrollbarY : List (Attribute msg) -> List (Element msg) -> Element msg
rowWithAncestorWithScrollbarY attrs =
    -- TODO workaround for https://github.com/mdgriffith/elm-ui/issues/12
    row (clip :: style "flex-shrink" "1" :: attrs)


elWithAncestorWithScrollbarX : List (Attribute msg) -> Element msg -> Element msg
elWithAncestorWithScrollbarX attrs =
    -- TODO fix to ensure code blocks render with a horizontal scrollbar
    el (style "min-width" "0" :: attrs)


columnWithAncestorWithScrollbarX : List (Attribute msg) -> List (Element msg) -> Element msg
columnWithAncestorWithScrollbarX attrs =
    -- TODO fix to ensure code blocks render with a horizontal scrollbar
    column (style "min-width" "0" :: attrs)


elWithAncestorWithScrollbars : List (Attribute msg) -> Element msg -> Element msg
elWithAncestorWithScrollbars attrs =
    -- TODO workaround for https://github.com/mdgriffith/elm-ui/issues/12
    -- TODO fix to ensure code blocks render with a horizontal scrollbar
    el (clip :: style "flex-shrink" "1" :: style "min-width" "0" :: attrs)


id : String -> Attribute msg
id theId =
    htmlAttribute (Html.Attributes.id theId)


class : String -> Attribute msg
class theClass =
    htmlAttribute (Html.Attributes.class theClass)


style : String -> String -> Attribute msg
style attribute value =
    htmlAttribute (Html.Attributes.style attribute value)


loadingAnimation : Attribute msg
loadingAnimation =
    htmlAttribute (Html.Attributes.class "loading-animation")


loadingAnimationContainerLow : Attribute msg
loadingAnimationContainerLow =
    htmlAttribute (Html.Attributes.class "loading-animation--container-low")


role : String -> Attribute msg
role theRole =
    htmlAttribute (Html.Attributes.attribute "role" theRole)


ariaLabel : String -> Attribute msg
ariaLabel label =
    htmlAttribute (Html.Attributes.attribute "aria-label" label)


ariaLabelledby : String -> Attribute msg
ariaLabelledby theId =
    htmlAttribute (Html.Attributes.attribute "aria-labelledby" theId)


ariaSelected : Bool -> Attribute msg
ariaSelected selected =
    htmlAttribute
        (Html.Attributes.attribute "aria-selected"
            (if selected then
                "true"

             else
                "false"
            )
        )


ariaMultiselectable : String -> Attribute msg
ariaMultiselectable label =
    htmlAttribute (Html.Attributes.attribute "aria-multiselectable" label)


ariaActivedescendant : String -> Attribute msg
ariaActivedescendant label =
    htmlAttribute (Html.Attributes.attribute "aria-activedescendant" label)


ariaCheckedRaw : String -> Attribute msg
ariaCheckedRaw checked =
    htmlAttribute (Html.Attributes.attribute "aria-checked" checked)


ariaChecked : Bool -> Attribute msg
ariaChecked checked =
    ariaCheckedRaw
        (if checked then
            "true"

         else
            "false"
        )


ariaControls : String -> Attribute msg
ariaControls theId =
    htmlAttribute (Html.Attributes.attribute "aria-controls" theId)


tabindex : Int -> Attribute msg
tabindex index =
    htmlAttribute (Html.Attributes.tabindex index)


maxlength : Int -> Attribute msg
maxlength length =
    htmlAttribute (Html.Attributes.attribute "maxlength" (String.fromInt length))


disabled : Bool -> Attribute msg
disabled =
    htmlAttribute << Html.Attributes.disabled


onScroll : msg -> Attribute msg
onScroll msg =
    htmlAttribute (Html.Events.on "scroll" (Decode.succeed msg))


onBlur : msg -> Attribute msg
onBlur msg =
    htmlAttribute (Html.Events.on "blur" (Decode.succeed msg))


onFocus : msg -> Attribute msg
onFocus msg =
    htmlAttribute (Html.Events.on "focus" (Decode.succeed msg))


onFocusIn : msg -> Attribute msg
onFocusIn msg =
    htmlAttribute (Html.Events.on "focusin" (Decode.succeed msg))


onFocusOut : msg -> Attribute msg
onFocusOut msg =
    htmlAttribute (Html.Events.on "focusout" (Decode.succeed msg))


onMouseDown : msg -> Attribute msg
onMouseDown msg =
    htmlAttribute (Html.Events.on "mousedown" (Decode.succeed msg))


onMouseUp : msg -> Attribute msg
onMouseUp msg =
    htmlAttribute (Html.Events.on "mouseup" (Decode.succeed msg))


onKeyDown : List (Decoder msg) -> Attribute msg
onKeyDown =
    Element.htmlAttribute << Accessibility.Key.onKeyDown


preventDefaultOnKeyDown : List (Decoder ( msg, Bool )) -> Attribute msg
preventDefaultOnKeyDown decoders =
    Element.htmlAttribute
        (Html.Events.preventDefaultOn "keydown"
            (Decode.oneOf decoders)
        )


preventDefaultOnClick : msg -> Attribute msg
preventDefaultOnClick msg =
    Element.htmlAttribute
        (Html.Events.preventDefaultOn "click"
            (Decode.succeed ( msg, True ))
        )


stopPropagationOnClick : msg -> Attribute msg
stopPropagationOnClick msg =
    Element.htmlAttribute
        (Html.Events.stopPropagationOn "click"
            (Decode.succeed ( msg, True ))
        )


preventDefaultAndStopPropagationOnClick : msg -> Attribute msg
preventDefaultAndStopPropagationOnClick msg =
    Element.htmlAttribute
        (Html.Events.custom "click"
            (Decode.succeed
                { message = msg
                , preventDefault = True
                , stopPropagation = True
                }
            )
        )


stopPropagationAndPreventDefaultOnClick : msg -> Attribute msg
stopPropagationAndPreventDefaultOnClick msg =
    Element.htmlAttribute
        (Html.Events.custom "click"
            (Decode.succeed
                { message = msg
                , stopPropagation = True
                , preventDefault = True
                }
            )
        )


userSelect : Bool -> Attribute msg
userSelect auto =
    style "user-select"
        (if auto then
            "auto"

         else
            "none"
        )


pointerEvents : Bool -> Attribute msg
pointerEvents auto =
    style "pointer-events"
        (if auto then
            "auto"

         else
            "none"
        )


colorToRgba : Color -> String
colorToRgba color =
    let
        { red, green, blue, alpha } =
            toRgb color
    in
    "rgba("
        ++ String.join ","
            [ String.fromInt (floor (255 * red))
            , String.fromInt (floor (255 * green))
            , String.fromInt (floor (255 * blue))
            , String.fromFloat alpha
            ]
        ++ ")"
