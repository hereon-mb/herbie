{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Event exposing
    ( CacheChange(..)
    , Event
    , batch
    , cacheChangesToEffect
    , map
    , none
    , onCacheChange
    )

import Effect exposing (Effect)
import Rdf exposing (Iri)
import Rdf.Graph exposing (Graph)


type Event msg
    = None
    | Batch (List (Event msg))
    | OnCacheChange (CacheChange msg)


type CacheChange msg
    = GraphUpdated (Graph -> msg) Iri
    | TriplesChanged msg


none : Event msg
none =
    None


batch : List (Event msg) -> Event msg
batch =
    Batch


onCacheChange : CacheChange msg -> Event msg
onCacheChange =
    OnCacheChange


map : (msgA -> msgB) -> Event msgA -> Event msgB
map func event =
    case event of
        None ->
            None

        Batch events ->
            Batch (List.map (map func) events)

        OnCacheChange cacheChange ->
            OnCacheChange (mapCacheChange func cacheChange)


mapCacheChange : (msgA -> msgB) -> CacheChange msgA -> CacheChange msgB
mapCacheChange func cacheChange =
    case cacheChange of
        GraphUpdated toMsg iri ->
            GraphUpdated (toMsg >> func) iri

        TriplesChanged msg ->
            TriplesChanged (func msg)


cacheChangesToEffect : (CacheChange msg -> Effect msg) -> Event msg -> Effect msg
cacheChangesToEffect toEffect event =
    case event of
        None ->
            Effect.none

        Batch events ->
            events
                |> List.map (cacheChangesToEffect toEffect)
                |> Effect.batch

        OnCacheChange cacheChange ->
            toEffect cacheChange
