{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Main exposing (Model, Msg, main)

import Action
import Api
import Api.Login
import Api.Me exposing (AuthorDetail)
import Browser
import Browser.Navigation as Nav
import Effect
import Effect.Internal
import Element
    exposing
        ( Element
        , centerX
        , column
        , el
        , fill
        , height
        , none
        , padding
        , paddingEach
        , paddingXY
        , paragraph
        , scrollbarY
        , spacing
        , width
        , wrappedRow
        )
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import ElmSpa.Request
import Event
import Fluent exposing (Fluent, verbatim)
import Gen.Model
import Gen.Msg
import Gen.Pages as Pages
import Gen.Route as Route
import Pages.Datasets as Datasets
import Pages.Datasets.Uuid_ as DatasetsUuid_
import Pages.Datasets.Uuid_.Draft as DatasetsUuid_Draft
import Pages.Datasets.Uuid_.Versions.Index_ as DatasetsUuid_VersionsIndex_
import Pages.Documentation as Documentation
import Pages.Graph as Graph
import Pages.Workspaces as Workspaces
import Shared exposing (Flags)
import Ui.Atom.Button as Button exposing (button, buttonLink)
import Ui.Atom.Divider as Divider
import Ui.Atom.TextInput as TextInput
    exposing
        ( currentPasswordInput
        , labelAbove
        , usernameInput
        )
import Ui.Theme.Color
    exposing
        ( error
        , outline
        , surface
        , surfaceContainerLow
        )
import Ui.Theme.Layout
import Ui.Theme.Logo as Logo
import Ui.Theme.Typography
    exposing
        ( body1
        , body2
        , newTabLink2
        )
import Url exposing (Url)
import View


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        , onUrlRequest = LinkClicked
        , onUrlChange = UrlChanged
        }



-- INIT


type Model
    = Loading
        { flags : Flags
        , url : Url
        , key : Nav.Key
        }
    | LoggedOut LoggedOutData
    | Loaded DataLoaded


type alias LoggedOutData =
    { flags : Flags
    , url : Url
    , key : Nav.Key
    , username : String
    , password : String
    , loginFailed : Bool
    }


type alias DataLoaded =
    { flags : Flags
    , url : Url
    , key : Nav.Key
    , fluentBundles : Fluent.Bundles
    , shared : Shared.Model
    , sharedCallbacks : Shared.Callbacks Msg
    , page : Pages.Model
    }


init : Flags -> Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url key =
    ( Loading
        { flags = flags
        , url = url
        , key = key
        }
    , Api.Me.getAuthor ApiReturnedAuthor
    )



-- UPDATE


type Msg
    = ApiReturnedAuthor (Api.Response AuthorDetail)
      -- LOGGED OUT
    | UserChangedUsername String
    | UserChangedPassword String
    | UserPressedLogin
    | ApiReturnedLogin (Api.Response ())
      -- LOADED
    | LinkClicked Browser.UrlRequest
    | UrlChanged Url
    | Shared (Shared.Msg Msg)
    | Pages Pages.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case model of
        Loading data ->
            case msg of
                ApiReturnedAuthor (Err _) ->
                    ( LoggedOut
                        { flags = data.flags
                        , url = data.url
                        , key = data.key
                        , username = ""
                        , password = ""
                        , loginFailed = False
                        }
                    , -- FIXME Report something to the user
                      Cmd.none
                    )

                ApiReturnedAuthor (Ok author) ->
                    let
                        ( shared, callbacks, effectShared ) =
                            Shared.init data.key data.url data.flags author

                        ( page, effectPages ) =
                            Pages.init (Shared.toContext shared).route shared data.url data.key
                    in
                    ( Loaded
                        { flags = data.flags
                        , url = data.url
                        , key = data.key
                        , fluentBundles = data.flags.fluentBundles
                        , shared = shared
                        , sharedCallbacks = callbacks
                        , page = page
                        }
                    , Cmd.batch
                        [ effectShared
                            |> Effect.map Shared
                            |> Effect.Internal.toCmd (Shared << Shared.msgFromAction)
                        , effectPages
                            |> Effect.map Pages
                            |> Effect.Internal.toCmd (Shared << Shared.msgFromAction)
                        ]
                    )

                _ ->
                    ( model, Cmd.none )

        LoggedOut data ->
            case msg of
                UserChangedUsername newUsername ->
                    ( LoggedOut { data | username = newUsername }
                    , Cmd.none
                    )

                UserChangedPassword newPassword ->
                    ( LoggedOut { data | password = newPassword }
                    , Cmd.none
                    )

                UserPressedLogin ->
                    ( model
                    , Api.Login.create
                        { onResponse = ApiReturnedLogin
                        , csrfToken = data.flags.csrfToken
                        , username = data.username
                        , password = data.password
                        }
                    )

                ApiReturnedLogin result ->
                    case result of
                        Err error ->
                            case error of
                                Api.BadStatus metadata _ ->
                                    if metadata.statusCode == 401 then
                                        ( LoggedOut { data | loginFailed = True }
                                        , Cmd.none
                                        )

                                    else
                                        ( model
                                        , Cmd.none
                                        )

                                _ ->
                                    ( model
                                    , Cmd.none
                                    )

                        Ok () ->
                            ( model
                            , Nav.load (Url.toString data.url)
                            )

                LinkClicked (Browser.Internal url) ->
                    ( model
                    , if url.path == "/helmholtz-aai/login/" then
                        Nav.load (Url.toString url)

                      else
                        Nav.pushUrl data.key (Url.toString url)
                    )

                LinkClicked (Browser.External href) ->
                    ( model
                    , Nav.load href
                    )

                _ ->
                    ( model, Cmd.none )

        Loaded data ->
            case msg of
                ApiReturnedAuthor _ ->
                    ( model, Cmd.none )

                LinkClicked (Browser.Internal url) ->
                    ( model
                    , Nav.pushUrl data.key (Url.toString url)
                    )

                LinkClicked (Browser.External href) ->
                    ( model
                    , Nav.load href
                    )

                UrlChanged url ->
                    let
                        stuff =
                            Shared.updateUrl Shared data.key url data.shared

                        reinitPage =
                            let
                                ( page, effectPages ) =
                                    Pages.init (Route.fromUrl url) stuff.model url data.key
                            in
                            ( Loaded
                                { data
                                    | shared = stuff.model
                                    , page = page
                                    , url = url
                                }
                            , [ [ Effect.fromAction Action.UserIntentsToCollapseNavigation
                                , effectPages
                                ]
                                    |> Effect.batch
                                    |> Effect.map Pages
                                    |> Effect.Internal.toCmd (Shared << Shared.msgFromAction)
                              , stuff.effect
                                    |> Effect.Internal.toCmd (Shared << Shared.msgFromAction)
                              ]
                                |> Cmd.batch
                            )
                    in
                    if stuff.routeChanged then
                        reinitPage

                    else if stuff.workspaceChanged then
                        reinitPage

                    else if stuff.queryChanged then
                        case Route.fromUrl url of
                            Route.Datasets ->
                                case data.page of
                                    Gen.Model.Datasets params modelDatasets ->
                                        let
                                            ( modelDatasetsUpdated, effectDatasets ) =
                                                Datasets.initWith context request modelDatasets

                                            context =
                                                Shared.toContext stuff.model

                                            request =
                                                ElmSpa.Request.create Route.Datasets params url data.key
                                        in
                                        ( Loaded
                                            { data
                                                | shared = stuff.model
                                                , page = Gen.Model.Datasets params modelDatasetsUpdated
                                                , url = url
                                            }
                                        , effectDatasets
                                            |> Effect.map (Pages << Gen.Msg.Datasets)
                                            |> Effect.Internal.toCmd (Shared << Shared.msgFromAction)
                                        )

                                    _ ->
                                        reinitPage

                            Route.Datasets__Uuid___Versions__Index_ params ->
                                case data.page of
                                    Gen.Model.Datasets__Uuid___Versions__Index_ paramsCurrent modelCurrent ->
                                        if params == paramsCurrent then
                                            let
                                                ( modelUpdated, effect ) =
                                                    DatasetsUuid_VersionsIndex_.initWith context request modelCurrent

                                                context =
                                                    Shared.toContext stuff.model

                                                request =
                                                    ElmSpa.Request.create (Route.Datasets__Uuid___Versions__Index_ params) params url data.key
                                            in
                                            ( Loaded
                                                { data
                                                    | shared = stuff.model
                                                    , page = Gen.Model.Datasets__Uuid___Versions__Index_ params modelUpdated
                                                    , url = url
                                                }
                                            , effect
                                                |> Effect.map (Pages << Gen.Msg.Datasets__Uuid___Versions__Index_)
                                                |> Effect.Internal.toCmd (Shared << Shared.msgFromAction)
                                            )

                                        else
                                            reinitPage

                                    _ ->
                                        reinitPage

                            Route.Graph ->
                                case data.page of
                                    Gen.Model.Graph params modelGraph ->
                                        let
                                            ( modelGraphUpdated, effectGraph ) =
                                                Graph.initWith context request modelGraph

                                            context =
                                                Shared.toContext stuff.model

                                            request =
                                                ElmSpa.Request.create Route.Graph params url data.key
                                        in
                                        ( Loaded
                                            { data
                                                | shared = stuff.model
                                                , page = Gen.Model.Graph params modelGraphUpdated
                                                , url = url
                                            }
                                        , effectGraph
                                            |> Effect.map (Pages << Gen.Msg.Graph)
                                            |> Effect.Internal.toCmd (Shared << Shared.msgFromAction)
                                        )

                                    _ ->
                                        reinitPage

                            Route.Documentation ->
                                case data.page of
                                    Gen.Model.Documentation params modelDocumentation ->
                                        let
                                            ( modelDocumentationUpdated, effectDocumentation ) =
                                                Documentation.initWith context request modelDocumentation

                                            context =
                                                Shared.toContext stuff.model

                                            request =
                                                ElmSpa.Request.create Route.Documentation params url data.key
                                        in
                                        ( Loaded
                                            { data
                                                | shared = stuff.model
                                                , page = Gen.Model.Documentation params modelDocumentationUpdated
                                                , url = url
                                            }
                                        , effectDocumentation
                                            |> Effect.map (Pages << Gen.Msg.Documentation)
                                            |> Effect.Internal.toCmd (Shared << Shared.msgFromAction)
                                        )

                                    _ ->
                                        reinitPage

                            _ ->
                                reinitPage

                    else
                        ( Loaded
                            { data
                                | shared = stuff.model
                                , url = url
                            }
                        , Cmd.none
                        )

                Shared sharedMsg ->
                    let
                        ( shared, sharedCallbacks, effectShared ) =
                            Shared.update Shared data.key sharedMsg cacheSubs data.shared data.sharedCallbacks

                        cacheSubs =
                            case data.page of
                                Gen.Model.Datasets _ modelDatasets ->
                                    Event.map (Pages << Gen.Msg.Datasets) (Datasets.events (Shared.toContext data.shared) modelDatasets)

                                Gen.Model.Datasets__Uuid_ _ modelPage ->
                                    Event.map (Pages << Gen.Msg.Datasets__Uuid_) (DatasetsUuid_.events (Shared.toContext data.shared) modelPage)

                                Gen.Model.Datasets__Uuid___Draft _ modelPage ->
                                    Event.map (Pages << Gen.Msg.Datasets__Uuid___Draft) (DatasetsUuid_Draft.events (Shared.toContext data.shared) modelPage)

                                Gen.Model.Datasets__Uuid___Versions__Index_ _ modelPage ->
                                    Event.map (Pages << Gen.Msg.Datasets__Uuid___Versions__Index_) (DatasetsUuid_VersionsIndex_.events (Shared.toContext data.shared) modelPage)

                                Gen.Model.Graph _ modelPage ->
                                    Event.map (Pages << Gen.Msg.Graph) (Graph.events (Shared.toContext data.shared) modelPage)

                                Gen.Model.Workspaces _ modelWorkspaces ->
                                    Event.map (Pages << Gen.Msg.Workspaces) (Workspaces.events (Shared.toContext data.shared) modelWorkspaces)

                                _ ->
                                    Event.none
                    in
                    ( Loaded
                        { data
                            | shared = shared
                            , sharedCallbacks = sharedCallbacks
                        }
                    , effectShared
                        |> Effect.Internal.toCmd (Shared << Shared.msgFromAction)
                    )

                Pages pageMsg ->
                    let
                        ( page, effectPages ) =
                            Pages.update pageMsg data.page data.shared data.url data.key
                    in
                    ( Loaded { data | page = page }
                    , effectPages
                        |> Effect.map Pages
                        |> Effect.Internal.toCmd (Shared << Shared.msgFromAction)
                    )

                _ ->
                    ( model, Cmd.none )


view : Model -> Browser.Document Msg
view model =
    case model of
        Loading _ ->
            { title = "Loading"
            , body = []
            }

        LoggedOut data ->
            { title = "Login"
            , body =
                [ Fluent.provider data.flags.fluentBundles
                    [ Ui.Theme.Layout.default (viewLoginPage data) ]
                ]
            }

        Loaded data ->
            let
                page =
                    Pages.view data.page data.shared data.url data.key
            in
            data.shared
                |> Shared.view data.url
                    { page =
                        { title = page.title
                        , element = Element.map Pages page.element
                        , overlays = List.map (Element.map Pages) page.overlays
                        , breadcrumbs = page.breadcrumbs
                        , fabVisible = page.fabVisible
                        , navigationRailVisible = page.navigationRailVisible
                        , loading = page.loading
                        }
                    , toMsg = Shared
                    }
                |> View.toBrowserDocument
                    { fluentBundles = data.fluentBundles
                    , sandbox = Shared.sandboxed data.shared
                    , dragging = False
                    }


viewLoginPage : LoggedOutData -> Element Msg
viewLoginPage data =
    el
        [ width fill
        , height fill
        , scrollbarY
        , Background.color surfaceContainerLow
        ]
        (column
            [ centerX
            , width (Element.maximum 400 fill)
            , height fill
            , paddingEach
                { top = 16
                , bottom = 32
                , left = 16
                , right = 16
                }
            ]
            [ el
                [ width fill
                , height (Element.maximum 64 fill)
                ]
                none
            , column
                [ spacing 64
                ]
                [ el
                    [ width fill ]
                    Logo.full
                , el
                    [ width fill ]
                    (el
                        [ width fill
                        , padding 16
                        , Border.color outline
                        , Border.rounded 4
                        , Background.color surface
                        ]
                        (viewSignIn data)
                    )
                , wrappedRow
                    [ centerX
                    , spacing 16
                    ]
                    (List.intersperse
                        (body2 (Fluent.verbatim "•"))
                        (List.filterMap (\( label, url ) -> Maybe.map (viewLink label) url)
                            [ ( "Imprint", data.flags.imprintUrl )
                            , ( "Data Protection", data.flags.dataProtectionUrl )
                            , ( "Accessibility", data.flags.accessibilityUrl )
                            ]
                        )
                    )
                ]
            ]
        )


viewLink : String -> String -> Element Msg
viewLink label url =
    newTabLink2
        { url = url
        , label = Fluent.verbatim label
        }


viewSignIn : LoggedOutData -> Element Msg
viewSignIn data =
    if data.flags.useHelmholtzAai then
        [ viewSignInHelmholtzAai
        , if data.flags.hideUserPaswordLogin then
            none

          else
            [ Divider.horizontal
            , [ "Alternatively, you can sign in with your username and password"
                    |> verbatim
                    |> body2
              ]
                |> paragraph
                    [ width fill
                    , paddingXY 16 0
                    ]
            , viewSignInUsernamePassword data
            ]
                |> column
                    [ width fill
                    , spacing 16
                    ]
        ]
            |> column
                [ width fill
                , spacing 16
                ]

    else
        [ [ "Sign in with your username and password"
                |> verbatim
                |> body2
          ]
            |> paragraph
                [ width fill
                , paddingXY 16 0
                ]
        , viewSignInUsernamePassword data
        ]
            |> column
                [ width fill
                , spacing 16
                ]


viewSignInHelmholtzAai : Element msg
viewSignInHelmholtzAai =
    [ "Sign in with Helmholtz ID"
        |> verbatim
        |> buttonLink "/helmholtz-aai/login/"
        |> Button.withFormat Button.Contained
        |> Button.withWidth fill
        |> Button.view
    , [ "Please sign in with "
            |> verbatim
            |> body2
      , { url = "https://aai.helmholtz.de/howto"
        , label = verbatim "Helmholtz ID"
        }
            |> newTabLink2
      , " (also known as Helmholtz AAI). Select your home institution or a social provider like ORCID, GitHub, or Google."
            |> verbatim
            |> body2
      ]
        |> paragraph
            [ width fill
            , paddingXY 16 0
            ]
    ]
        |> column
            [ width fill
            , spacing 16
            ]


viewSignInUsernamePassword : LoggedOutData -> Element Msg
viewSignInUsernamePassword data =
    [ data.username
        |> usernameInput UserChangedUsername (labelAbove (t "username"))
        |> TextInput.onEnter UserPressedLogin
        |> TextInput.view
    , data.password
        |> currentPasswordInput UserChangedPassword (labelAbove (t "password"))
        |> TextInput.onEnter UserPressedLogin
        |> TextInput.view
    , if data.loginFailed then
        t "could-not-log-in"
            |> body1
            |> List.singleton
            |> paragraph [ Font.color error ]

      else
        none
    , "Sign in"
        |> verbatim
        |> button UserPressedLogin
        |> Button.withFormat
            (if data.flags.useHelmholtzAai then
                Button.Outlined

             else
                Button.Contained
            )
        |> Button.withWidth fill
        |> Button.withId "login"
        |> Button.view
    ]
        |> column
            [ width fill
            , spacing 16
            ]


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        Loading _ ->
            Sub.none

        LoggedOut _ ->
            Sub.none

        Loaded data ->
            Sub.batch
                [ Shared.subscriptions data.shared data.sharedCallbacks
                    |> Sub.map Shared
                , Pages.subscriptions data.page data.shared data.url data.key
                    |> Sub.map Pages
                ]



---- FLUENT


t : String -> Fluent
t key =
    Fluent.text ("main--" ++ key)
