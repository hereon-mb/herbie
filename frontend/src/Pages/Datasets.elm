module Pages.Datasets exposing (Model, Msg, events, initWith, page)

{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}

import Action
import Api
import Api.Workspace as Workspace exposing (Workspace)
import Browser.Navigation
import Context exposing (C)
import Dict exposing (Dict)
import DocumentTree exposing (DocumentTree)
import Effect exposing (Effect)
import Element
    exposing
        ( DeviceClass(..)
        , Element
        , alignTop
        , centerX
        , centerY
        , column
        , el
        , fill
        , height
        , maximum
        , none
        , padding
        , paddingEach
        , paddingXY
        , paragraph
        , row
        , scrollbarY
        , spacing
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Extra exposing (class)
import Element.Font as Font
import Element.Lazy exposing (lazy2)
import Event exposing (Event)
import File.Download
import Fluent
    exposing
        ( verbatim
        )
import Gen.Route as Route
import Href
import Http
import List.Extra as List
import Maybe.Extra as Maybe
import Maybe.Pipeline as Maybe
import Ontology.Herbie as Herbie exposing (herbie)
import Page
import Query
import Rdf exposing (Iri)
import Rdf.Decode as Decode
import Rdf.DictIri as DictIri exposing (DictIri)
import Rdf.Graph exposing (Graph)
import Rdf.Namespaces exposing (a)
import Rdf.Query as Rdf
import Request exposing (Request)
import Shacl.Form.Localize as Localize
import Shared
import Shared.Loading as Loading exposing (Loading)
import Time exposing (Posix)
import Ui.Atom.Divider as Divider
import Ui.Atom.Icon as Icon
import Ui.Molecule.Breadcrumbs as Breadcrumbs exposing (Breadcrumbs)
import Ui.Molecule.ErrorBox as ErrorBox exposing (errorBox)
import Ui.Molecule.Search as Search
import Ui.Organism.CardDataset.Draft as CardDatasetDraft
import Ui.Organism.CardDataset.Folder as CardDatasetFolder exposing (cardDatasetFolder)
import Ui.Organism.CardDataset.Published as CardDatasetPublished
import Ui.Theme.Card as Card exposing (card)
import Ui.Theme.Color as Color exposing (error)
import Ui.Theme.Dimensions exposing (paneWidthMax)
import Ui.Theme.Typography
    exposing
        ( body2
        , caption
        )
import Url.Builder as Url
import View exposing (View)


type Model
    = Redirecting
    | Loading DataLoading
    | Failed DataFailed
    | Loaded DataLoaded


type alias DataLoading =
    { params : Params
    , workspace : Maybe Workspace
    , documentsPublished : Maybe (DictIri Herbie.Document)
    , documentsPublishedTree : Maybe (List DocumentTree)
    , documentsBeingEdited : Maybe (List Herbie.Document)
    , graphsLoading : Loading
    , ontologiesLoading : Loading
    }


type alias DataFailed =
    { params : Params
    , error : Error
    }


type Error
    = LoadingWorkspaceFailed Api.Error
    | LoadingDocumentsPublishedFailed Api.Error
    | LoadingDocumentsBeingEditedFailed Api.Error


type alias DataLoaded =
    { params : Params
    , workspace : Workspace
    , documentsPublished : DictIri Herbie.Document
    , documentsPublishedTree : List DocumentTree
    , documentsBeingEdited : List Herbie.Document
    , graphsLoading : Loading
    , ontologiesLoading : Loading
    , query : String
    }


type alias Params =
    { workspaceUuid : String
    , iriRoot : Maybe Iri
    }


paramsFromQuery : Dict String String -> Maybe Params
paramsFromQuery query =
    Just Params
        |> Maybe.required (Dict.get "workspace" query)
        |> Maybe.hardcoded (Maybe.map Rdf.iri (Dict.get "root" query))


documentsFromGraph : Graph -> Maybe (List Herbie.Document)
documentsFromGraph graph =
    Rdf.emptyQuery
        |> Rdf.withPredicate a
        |> Rdf.withObject (herbie "Document")
        |> Rdf.getIriSubjects graph
        |> List.map
            (\iriDocument ->
                graph
                    |> Decode.decode (Decode.from iriDocument Herbie.decoderDocument)
                    |> Result.toMaybe
            )
        |> Maybe.combine


iriDictFromDocumentsList : List Herbie.Document -> DictIri Herbie.Document
iriDictFromDocumentsList =
    List.concatMap
        (\document ->
            [ document.versions
                |> List.head
                |> Maybe.map (.terms >> .source)
                |> Maybe.withDefault []
            , [ document.this ]
            ]
                |> List.concat
                |> List.map (\iri -> ( iri, document ))
        )
        >> DictIri.fromList


documentsTreeFromDocuments : List Herbie.Document -> List DocumentTree
documentsTreeFromDocuments documents =
    documents
        |> List.filterMap
            (\document ->
                case List.head document.versions of
                    Nothing ->
                        Nothing

                    Just version ->
                        Just (document.this :: version.terms.source)
            )
        |> List.concat
        |> List.map Rdf.toUrl
        |> DocumentTree.fromList


page : Shared.Model -> Request -> Page.With Model Msg
page shared request =
    let
        c =
            Shared.toContext shared
    in
    Page.advanced
        { init = init c request
        , update = update c request.key
        , subscriptions = subscriptions
        , view = view c
        }


init : C -> Request -> ( Model, Effect Msg )
init c request =
    case paramsFromQuery request.query of
        Nothing ->
            ( Redirecting
            , Route.Workspaces
                |> Href.fromRoute c
                |> Browser.Navigation.replaceUrl request.key
                |> Effect.fromCmd
            )

        Just params ->
            ( Loading
                { params = params
                , workspace = Nothing
                , documentsPublished = Nothing
                , documentsPublishedTree = Nothing
                , documentsBeingEdited = Nothing
                , graphsLoading = Loading.empty
                , ontologiesLoading = Loading.empty
                }
            , [ Workspace.get c ApiReturnedWorkspace params.workspaceUuid
              , getDocumentsPublished c params
              , getDocumentsBeingEdited c params
              ]
                |> Effect.batch
            )


initWith : C -> Request -> Model -> ( Model, Effect Msg )
initWith c request model =
    case model of
        Loaded data ->
            case paramsFromQuery request.query of
                Nothing ->
                    init c request

                Just params ->
                    ( Loaded { data | params = params }
                    , Effect.none
                    )

        _ ->
            init c request


type Msg
    = NoOp
      -- LOADING
    | ApiReturnedWorkspace (Api.Response Workspace)
    | ApiReturnedDocumentsPublished (Api.Response Graph)
    | ApiReturnedDocumentsBeingEdited (Api.Response Graph)
      -- LOADED
    | CacheLoadedGraph Iri (Api.Response Graph)
    | CacheLoadedGraphOntology Iri (Api.Response Graph)
    | CacheChangedTriples
    | UserChangedQuery String
    | UserClearedQuery
    | UserPressedDownload Iri
    | ApiReturnedDatasetForDownload Iri (Api.Response String)
    | UserPressedEdit Iri
    | ApiStartedEditOfDocument String (Api.Response ())
    | UserPressedDelete Iri
    | ApiDeletedDraft (Api.Response ())


update : C -> Browser.Navigation.Key -> Msg -> Model -> ( Model, Effect Msg )
update c key msg model =
    case model of
        Redirecting ->
            ( model, Effect.none )

        Loading data ->
            updateLoading c msg data

        Failed _ ->
            ( model, Effect.none )

        Loaded data ->
            updateLoaded c key msg data
                |> Tuple.mapFirst Loaded


updateLoading : C -> Msg -> DataLoading -> ( Model, Effect Msg )
updateLoading c msg data =
    case msg of
        NoOp ->
            ( Loading data, Effect.none )

        -- LOADING
        ApiReturnedWorkspace (Err error) ->
            ( Failed
                { params = data.params
                , error = LoadingWorkspaceFailed error
                }
            , Effect.none
            )

        ApiReturnedWorkspace (Ok workspace) ->
            { data | workspace = Just workspace }
                |> initLoaded c

        ApiReturnedDocumentsPublished (Err error) ->
            ( Failed
                { params = data.params
                , error = LoadingDocumentsPublishedFailed error
                }
            , Effect.none
            )

        ApiReturnedDocumentsPublished (Ok graph) ->
            let
                documents =
                    documentsFromGraph graph
            in
            { data
                | documentsPublished = Maybe.map iriDictFromDocumentsList documents
                , documentsPublishedTree = Maybe.map documentsTreeFromDocuments documents
            }
                |> initLoaded c

        ApiReturnedDocumentsBeingEdited (Err error) ->
            ( Failed
                { params = data.params
                , error = LoadingDocumentsBeingEditedFailed error
                }
            , Effect.none
            )

        ApiReturnedDocumentsBeingEdited (Ok graph) ->
            { data | documentsBeingEdited = documentsFromGraph graph }
                |> initLoaded c

        -- LOADED
        CacheLoadedGraph _ _ ->
            ( Loading data, Effect.none )

        CacheLoadedGraphOntology _ _ ->
            ( Loading data, Effect.none )

        CacheChangedTriples ->
            ( Loading data, Effect.none )

        UserChangedQuery _ ->
            ( Loading data, Effect.none )

        UserClearedQuery ->
            ( Loading data, Effect.none )

        UserPressedDownload _ ->
            ( Loading data, Effect.none )

        ApiReturnedDatasetForDownload _ _ ->
            ( Loading data, Effect.none )

        UserPressedEdit _ ->
            ( Loading data, Effect.none )

        ApiStartedEditOfDocument _ _ ->
            ( Loading data, Effect.none )

        UserPressedDelete _ ->
            ( Loading data, Effect.none )

        ApiDeletedDraft _ ->
            ( Loading data, Effect.none )


initLoaded : C -> DataLoading -> ( Model, Effect Msg )
initLoaded _ data =
    Just
        (\workspace documentsPublished documentsPublishedTree documentsBeingEdited ->
            let
                graphsLoadingUpdated =
                    data.graphsLoading
                        |> Loading.addMany
                            ([--  dependenciesPublished.iriGraphs
                              -- dependenciesDraft.iriGraphs
                             ]
                                |> List.concat
                            )

                ontologiesLoadingUpdated =
                    data.ontologiesLoading
                        |> Loading.addMany
                            ([-- dependenciesPublished.iriOntologies
                              -- dependenciesDraft.iriOntologies
                             ]
                                |> List.concat
                            )
            in
            ( Loaded
                { params = data.params
                , workspace = workspace
                , documentsPublished = documentsPublished
                , documentsPublishedTree = documentsPublishedTree
                , documentsBeingEdited = documentsBeingEdited
                , graphsLoading = Loading.asked graphsLoadingUpdated
                , ontologiesLoading = Loading.asked ontologiesLoadingUpdated
                , query = ""
                }
            , [ graphsLoadingUpdated
                    |> Loading.notAsked
                    |> List.map
                        (\iri ->
                            Action.GetGraph
                                { store = True
                                , reload = False
                                , closure = False
                                }
                                (CacheLoadedGraph iri)
                                iri
                                |> Effect.fromAction
                        )
                    |> Effect.batch
              , ontologiesLoadingUpdated
                    |> Loading.notAsked
                    |> List.map
                        (\iri ->
                            Action.GetGraph
                                { store = False
                                , reload = False
                                , closure = True
                                }
                                (CacheLoadedGraphOntology iri)
                                iri
                                |> Effect.fromAction
                        )
                    |> Effect.batch
              ]
                |> Effect.batch
            )
        )
        |> Maybe.required data.workspace
        |> Maybe.required data.documentsPublished
        |> Maybe.required data.documentsPublishedTree
        |> Maybe.required data.documentsBeingEdited
        |> Maybe.withDefault
            ( Loading data
            , Effect.none
            )


updateLoaded : C -> Browser.Navigation.Key -> Msg -> DataLoaded -> ( DataLoaded, Effect Msg )
updateLoaded c key msg data =
    case msg of
        NoOp ->
            ( data, Effect.none )

        -- LOADING
        ApiReturnedWorkspace _ ->
            ( data, Effect.none )

        ApiReturnedDocumentsPublished (Err error) ->
            ( data
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiReturnedDocumentsPublished (Ok graph) ->
            case documentsFromGraph graph of
                Nothing ->
                    ( data, Effect.none )

                Just documents ->
                    ( { data
                        | documentsPublished = iriDictFromDocumentsList documents
                        , documentsPublishedTree = documentsTreeFromDocuments documents
                      }
                    , Effect.none
                    )

        ApiReturnedDocumentsBeingEdited (Err error) ->
            ( data
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiReturnedDocumentsBeingEdited (Ok graph) ->
            ( { data | documentsBeingEdited = Maybe.withDefault data.documentsBeingEdited (documentsFromGraph graph) }
            , Effect.none
            )

        -- LOADED
        CacheLoadedGraph iri (Err error) ->
            { data | graphsLoading = Loading.failed iri error data.graphsLoading }
                |> loadMore c

        CacheLoadedGraph iri (Ok _) ->
            { data | graphsLoading = Loading.loaded iri data.graphsLoading }
                |> loadMore c

        CacheLoadedGraphOntology iri (Err error) ->
            { data | ontologiesLoading = Loading.failed iri error data.ontologiesLoading }
                |> loadMore c

        CacheLoadedGraphOntology iri (Ok _) ->
            { data | ontologiesLoading = Loading.loaded iri data.ontologiesLoading }
                |> loadMore c

        CacheChangedTriples ->
            ( data
            , [ getDocumentsPublished c data.params
              , getDocumentsBeingEdited c data.params
              ]
                |> Effect.batch
            )

        UserChangedQuery query ->
            ( { data | query = query }
            , Effect.none
            )

        UserClearedQuery ->
            ( { data | query = "" }
            , Effect.none
            )

        UserPressedDownload iriDataset ->
            ( data
            , Api.requestIri c
                { method = "GET"
                , headers = [ Http.header "Accept" "application/trig" ]
                , iri = iriDataset
                , queryParameters = []
                , body = Http.emptyBody
                , expect = Api.expectString (ApiReturnedDatasetForDownload iriDataset)
                , tracker = Nothing
                }
            )

        ApiReturnedDatasetForDownload _ (Err error) ->
            ( data
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiReturnedDatasetForDownload iri (Ok content) ->
            let
                uuid =
                    iri
                        |> uuidFromIri
                        |> Maybe.withDefault "document"
            in
            ( data
            , content
                |> File.Download.string (uuid ++ ".trig") "application/trig"
                |> Effect.fromCmd
            )

        UserPressedEdit iri ->
            case uuidFromIri iri of
                Nothing ->
                    ( data, Effect.none )

                Just uuid ->
                    ( data
                    , uuid
                        |> Action.UserIntentsToEditDataset (ApiStartedEditOfDocument uuid)
                        |> Effect.fromAction
                    )

        ApiStartedEditOfDocument _ (Err error) ->
            ( data
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiStartedEditOfDocument uuid (Ok ()) ->
            ( data
            , { uuid = uuid }
                |> Route.Datasets__Uuid___Draft
                |> Href.fromRoute c
                |> Browser.Navigation.pushUrl key
                |> Effect.fromCmd
            )

        UserPressedDelete iri ->
            case uuidFromIri iri of
                Nothing ->
                    ( data, Effect.none )

                Just uuid ->
                    ( data
                    , uuid
                        |> Action.UserIntentsToDeleteDraft ApiDeletedDraft
                        |> Effect.fromAction
                    )

        ApiDeletedDraft (Err error) ->
            ( data
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiDeletedDraft (Ok ()) ->
            ( data
            , { message = verbatim "Deleted graph."
              }
                |> Action.AddToastExpireIn 300
                |> Effect.fromAction
            )


uuidFromIri : Iri -> Maybe String
uuidFromIri iri =
    iri
        |> Rdf.toUrl
        |> String.split "/"
        |> List.reverse
        |> List.getAt 1


loadMore : C -> DataLoaded -> ( DataLoaded, Effect Msg )
loadMore _ data =
    let
        graphsLoadingUpdated =
            data.graphsLoading
                |> Loading.addMany
                    ([-- dependenciesPublished.iriGraphs
                      -- dependenciesDraft.iriGraphs
                     ]
                        |> List.concat
                    )

        ontologiesLoadingUpdated =
            data.ontologiesLoading
                |> Loading.addMany
                    ([-- dependenciesPublished.iriOntologies
                      -- dependenciesDraft.iriOntologies
                     ]
                        |> List.concat
                    )
    in
    ( { data
        | graphsLoading = Loading.asked graphsLoadingUpdated
        , ontologiesLoading = Loading.asked ontologiesLoadingUpdated
      }
    , [ graphsLoadingUpdated
            |> Loading.notAsked
            |> List.map
                (\iri ->
                    Action.GetGraph
                        { store = True
                        , reload = False
                        , closure = False
                        }
                        (CacheLoadedGraph iri)
                        iri
                        |> Effect.fromAction
                )
            |> Effect.batch
      , ontologiesLoadingUpdated
            |> Loading.notAsked
            |> List.map
                (\iri ->
                    Action.GetGraph
                        { store = False
                        , reload = False
                        , closure = True
                        }
                        (CacheLoadedGraphOntology iri)
                        iri
                        |> Effect.fromAction
                )
            |> Effect.batch
      ]
        |> Effect.batch
    )


getDocumentsPublished : C -> Params -> Effect Msg
getDocumentsPublished c params =
    if c.useTripleStore then
        let
            iri =
                Api.iriHttpWith c
                    [ "construct", "documents-published" ]
                    [ Url.string "workspace" params.workspaceUuid ]
        in
        iri
            |> Action.GetGraph
                { store = False
                , reload = True
                , closure = False
                }
                ApiReturnedDocumentsPublished
            |> Effect.fromAction

    else
        -- FIXME
        -- queryDocumentsPublished
        --     |> Action.Query CacheReturnedDocumentsPublished
        --     |> Effect.fromAction
        Effect.none


getDocumentsBeingEdited : C -> Params -> Effect Msg
getDocumentsBeingEdited c params =
    if c.useTripleStore then
        let
            iri =
                Api.iriHttpWith c
                    [ "construct", "documents-being-edited" ]
                    [ Url.string "workspace" params.workspaceUuid ]
        in
        iri
            |> Action.GetGraph
                { store = False
                , reload = True
                , closure = False
                }
                ApiReturnedDocumentsBeingEdited
            |> Effect.fromAction

    else
        -- FIXME
        -- queryDocumentsBeingEdited
        --     |> Action.Query CacheReturnedDocumentsBeingEdited
        --     |> Effect.fromAction
        Effect.none


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


events : C -> Model -> Event Msg
events _ model =
    case model of
        Redirecting ->
            Event.none

        Loading _ ->
            Event.none

        Failed _ ->
            Event.none

        Loaded _ ->
            Event.onCacheChange (Event.TriplesChanged CacheChangedTriples)



-- VIEW


view : C -> Model -> View Msg
view c model =
    { title = "Datasets"
    , element =
        case model of
            Redirecting ->
                none

            Loading data ->
                viewLoading c data

            Failed data ->
                viewFailed c data

            Loaded data ->
                viewLoaded c data
    , overlays = []
    , breadcrumbs = breadcrumbs c model
    , fabVisible = True
    , navigationRailVisible = True
    , loading =
        case model of
            Redirecting ->
                True

            Loading _ ->
                True

            Failed _ ->
                False

            Loaded _ ->
                False
    }


breadcrumbs : C -> Model -> Breadcrumbs
breadcrumbs c model =
    case model of
        Redirecting ->
            Breadcrumbs.empty

        Loading _ ->
            Breadcrumbs.from [] (verbatim "Documents")

        Failed _ ->
            Breadcrumbs.from [] (verbatim "Documents")

        Loaded data ->
            case data.params.iriRoot of
                Nothing ->
                    Breadcrumbs.from
                        [ Breadcrumbs.link
                            { url = Href.fromRoute c (Route.Workspaces__Uuid_ { uuid = data.workspace.uuid })
                            , label = verbatim data.workspace.name
                            }
                        ]
                        (verbatim "Documents")

                Just iri ->
                    Breadcrumbs.from
                        [ Breadcrumbs.link
                            { url = Href.fromRoute c (Route.Workspaces__Uuid_ { uuid = data.workspace.uuid })
                            , label = verbatim data.workspace.name
                            }
                        , Breadcrumbs.link
                            { url = Href.fromRoute c Route.Datasets
                            , label = verbatim "Documents"
                            }
                        ]
                        (verbatim (Rdf.toUrl iri ++ "..."))


viewLoading : C -> DataLoading -> Element Msg
viewLoading c data =
    [ { label = verbatim "Loading graphs"
      , loading = data.graphsLoading
      }
    , { label = verbatim "Loading ontologies"
      , loading = data.ontologiesLoading
      }
    ]
        |> Loading.viewMany c
        |> el
            [ width fill
            , height fill
            , padding 16
            ]


viewFailed : C -> DataFailed -> Element Msg
viewFailed c data =
    data.error
        |> errorToHeading
        |> errorBox
        |> ErrorBox.withInfo (errorToString c data.error)
        |> ErrorBox.view
        |> el
            [ centerX
            , centerY
            ]


errorToHeading : Error -> String
errorToHeading error =
    case error of
        LoadingWorkspaceFailed _ ->
            "Could not load workspace"

        LoadingDocumentsPublishedFailed _ ->
            "Could not load published documents"

        LoadingDocumentsBeingEditedFailed _ ->
            "Could not load drafts"


errorToString : C -> Error -> String
errorToString c error =
    case error of
        LoadingWorkspaceFailed errorApi ->
            errorApi
                |> Api.errorToMessages
                |> Localize.verbatims c

        LoadingDocumentsPublishedFailed errorApi ->
            errorApi
                |> Api.errorToMessages
                |> Localize.verbatims c

        LoadingDocumentsBeingEditedFailed errorApi ->
            errorApi
                |> Api.errorToMessages
                |> Localize.verbatims c


viewLoaded : C -> DataLoaded -> Element Msg
viewLoaded =
    lazy2
        (\c data ->
            el
                [ width fill
                , height fill
                , scrollbarY
                ]
                (viewLoadedBody c data)
        )


viewLoadedBody : C -> DataLoaded -> Element Msg
viewLoadedBody c data =
    column
        [ width (maximum paneWidthMax fill)
        , centerX
        , case c.device.class of
            Phone ->
                paddingEach
                    { top = 0
                    , bottom = 16 + 56 + 16
                    , left = 16
                    , right = 16
                    }

            _ ->
                paddingEach
                    { top = 0
                    , bottom = 16
                    , left = 16
                    , right = 16
                    }
        , spacing 32
        ]
        [ viewSearch data
        , column
            [ width fill
            , spacing 16
            ]
            [ viewDatasetsPublished c data
            , viewDatasetsDraft c data
            ]
        ]


viewSearch : DataLoaded -> Element Msg
viewSearch data =
    el
        [ width fill
        , paddingEach
            { top = 12
            , bottom = 0
            , left = 0
            , right = 0
            }
        , Background.color Color.surface
        , Border.roundEach
            { topLeft = 0
            , topRight = 0
            , bottomRight = 28
            , bottomLeft = 28
            }
        , class "pages-datasets--search-container"
        ]
        (Search.view searchConfig data.query)


searchConfig : Search.Config Msg
searchConfig =
    { userChangedQuery = UserChangedQuery
    , userClearedQuery = UserClearedQuery
    }


viewDatasetsPublished : C -> DataLoaded -> Element Msg
viewDatasetsPublished c data =
    if String.trim data.query == "" then
        case viewPublished c data.params.iriRoot data.documentsPublished data.documentsPublishedTree of
            Nothing ->
                none

            Just cards ->
                column
                    [ width fill
                    , spacing 24
                    ]
                    cards

    else
        viewDatasetsPublishedForQuery c data


viewDatasetsDraft : C -> DataLoaded -> Element Msg
viewDatasetsDraft c data =
    case viewDrafts c data.documentsBeingEdited of
        Nothing ->
            none

        Just cards ->
            column
                [ width fill
                , spacing 8
                ]
                [ column
                    [ width fill
                    , spacing 8
                    , paddingXY 16 0
                    ]
                    [ Divider.horizontal
                    , "Drafts"
                        |> verbatim
                        |> caption
                    ]
                , column
                    [ width fill
                    , spacing 24
                    ]
                    cards
                ]


viewPublished : C -> Maybe Iri -> DictIri Herbie.Document -> List DocumentTree -> Maybe (List (Element Msg))
viewPublished c iriRoot documentsPublished tree =
    let
        ( treeSelected, path ) =
            case iriRoot of
                Nothing ->
                    ( tree, [] )

                Just iri ->
                    tree
                        |> treeFor [] iri
                        |> Maybe.withDefault ( tree, [] )
    in
    case path of
        [] ->
            treeSelected
                |> List.filterMap (viewDocumentTree c documentsPublished Nothing)
                |> emptyListToNothing

        _ :: rest ->
            let
                parent =
                    List.head rest
            in
            [ parent
                |> Maybe.map (\urlParent -> urlParent ++ "...")
                |> Maybe.withDefault "http://..."
                |> verbatim
                |> cardDatasetFolder
                    (parent
                        |> Maybe.map (\urlParent -> Href.fromRouteWith c (Dict.singleton "root" urlParent) Route.Datasets)
                        |> Maybe.withDefault (Href.fromRoute c Route.Datasets)
                    )
                |> CardDatasetFolder.leadsToParent
                |> CardDatasetFolder.toElement
                |> List.singleton
            , treeSelected
                |> List.filterMap (viewDocumentTree c documentsPublished parent)
            ]
                |> List.concat
                |> emptyListToNothing


treeFor : List String -> Iri -> List DocumentTree -> Maybe ( List DocumentTree, List String )
treeFor path iriRoot tree =
    case tree of
        [] ->
            Nothing

        first :: rest ->
            treeForHelp path iriRoot first rest


treeForHelp : List String -> Iri -> DocumentTree -> List DocumentTree -> Maybe ( List DocumentTree, List String )
treeForHelp path iri first rest =
    case first of
        DocumentTree.Folder url children ->
            if url == Rdf.toUrl iri then
                Just ( children, url :: path )

            else if String.startsWith url (Rdf.toUrl iri) then
                treeFor (url :: path) iri children

            else
                treeFor path iri rest

        DocumentTree.Document _ ->
            treeFor path iri rest


viewDocumentTree : C -> DictIri Herbie.Document -> Maybe String -> DocumentTree -> Maybe (Element Msg)
viewDocumentTree c documentsPublished parent tree =
    case tree of
        DocumentTree.Folder url _ ->
            (url ++ "...")
                |> verbatim
                |> cardDatasetFolder (Href.fromRouteWith c (Dict.singleton "root" url) Route.Datasets)
                |> CardDatasetFolder.toElement
                |> Just

        DocumentTree.Document url ->
            DictIri.get (Rdf.iri url) documentsPublished
                |> Maybe.map (viewVersionPublished c parent url)


viewDatasetsPublishedForQuery : C -> DataLoaded -> Element Msg
viewDatasetsPublishedForQuery c data =
    column
        [ width fill
        , spacing 24
        ]
        (data.documentsPublished
            |> DictIri.toList
            |> Query.sortByScore (documentVersionFields c) data.query
            |> List.map (\( iri, document ) -> viewVersionPublished c Nothing (Rdf.toUrl iri) document)
        )


documentVersionFields : C -> List (( iri, Herbie.Document ) -> Maybe String)
documentVersionFields c =
    [ Tuple.second >> .versions >> List.head >> Maybe.andThen (.rdfs >> .label) >> Maybe.map (Localize.verbatim c)
    , Tuple.second >> .versions >> List.head >> Maybe.andThen (.rdfs >> .comment) >> Maybe.map (Localize.verbatim c)
    ]


viewVersionPublished : C -> Maybe String -> String -> Herbie.Document -> Element Msg
viewVersionPublished c parent url document =
    document
        |> CardDatasetPublished.view c cardDatasetPublishedConfig parent url
        |> Maybe.withDefault viewCouldNotLoad


cardDatasetPublishedConfig : CardDatasetPublished.Config Msg
cardDatasetPublishedConfig =
    { userPressedDownload = UserPressedDownload
    , userPressedEdit = UserPressedEdit
    , noOp = NoOp
    }


viewDrafts : C -> List Herbie.Document -> Maybe (List (Element Msg))
viewDrafts c documentsBeingEdited =
    documentsBeingEdited
        |> List.map (viewVersionDraft c)
        |> List.sortBy (Tuple.first >> Time.posixToMillis)
        |> List.map Tuple.second
        |> emptyListToNothing


viewVersionDraft : C -> Herbie.Document -> ( Posix, Element Msg )
viewVersionDraft c document =
    let
        viewHelp draft =
            CardDatasetDraft.view c
                { userPressedDownload = UserPressedDownload document.this
                , userPressedDelete = UserPressedDelete document.this
                , noOp = NoOp
                }
                { document = document
                , draft = draft
                }
                |> Tuple.pair document.terms.modified
    in
    Just viewHelp
        |> Maybe.required document.draft
        |> Maybe.withDefault ( document.terms.modified, viewCouldNotLoad )


emptyListToNothing : List a -> Maybe (List a)
emptyListToNothing items =
    case items of
        [] ->
            Nothing

        _ ->
            Just items



-- SHARED


viewCouldNotLoad : Element msg
viewCouldNotLoad =
    [ Icon.Error
        |> Icon.viewSmall
        |> el
            [ Font.color error
            , alignTop
            , paddingXY 0 3
            ]
    , "Could not load document."
        |> verbatim
        |> body2
        |> List.singleton
        |> paragraph
            [ Font.color error
            , alignTop
            ]
    ]
        |> row
            [ width fill
            , spacing 8
            ]
        |> card
        |> Card.withType Card.Outlined
        |> Card.toElement
