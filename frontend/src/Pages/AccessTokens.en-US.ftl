pages-access-tokens--action--generate-access-token = Generate access token
pages-access-tokens--action--generate-new-access-token = Generate another access token
pages-access-tokens--caption--access-token = Access token
pages-access-tokens--caption--expiry = Expires at
pages-access-tokens--paragraph--no-access-token--info = If you want to access the Herbie API outside your browser, you can generate an acess token here.
pages-access-tokens--paragraph--usage-info = You can use this token to authenticate as your user against the Herbie API by providing the following HTTP header:
