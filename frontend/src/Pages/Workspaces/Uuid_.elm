module Pages.Workspaces.Uuid_ exposing (Model, Msg, Params, page)

{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}

import Action
import Api
import Api.Author exposing (Author)
import Api.Decimal as Decimal
import Api.RoCrateImport as RoCrateImport exposing (RoCrateImport)
import Api.Workspace as Workspace exposing (Membership, MembershipType, Workspace)
import Browser.Navigation
import Context exposing (C)
import Effect exposing (Effect)
import Element
    exposing
        ( DeviceClass(..)
        , Element
        , alignRight
        , centerX
        , centerY
        , column
        , el
        , fill
        , height
        , maximum
        , moveDown
        , none
        , paddingEach
        , paddingXY
        , paragraph
        , px
        , row
        , scrollbarY
        , shrink
        , spacing
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Lazy exposing (lazy2)
import Fluent
    exposing
        ( Fluent
        , verbatim
        )
import List.Extra as List
import Page
import Pointer
import Query
import Rdf
import RemoteData exposing (RemoteData)
import Request
import Shacl.Form.Localize as Localize
import Shared
import String.Extra.Extra as String
import Time
import Ui.Atom.Button as Button exposing (button)
import Ui.Atom.ButtonIcon as ButtonIcon exposing (buttonIcon)
import Ui.Atom.Icon as Icon
import Ui.Atom.OptionCombobox exposing (OptionCombobox)
import Ui.Atom.Portal as Portal
import Ui.Atom.Radio as Radio exposing (radio)
import Ui.Atom.TextArea as TextArea exposing (textArea)
import Ui.Atom.TextInput as TextInput exposing (textInput)
import Ui.Molecule.Breadcrumbs as Breadcrumbs exposing (Breadcrumbs)
import Ui.Molecule.ComboboxMultiselect as ComboboxMultiselect exposing (comboboxMultiselect)
import Ui.Molecule.Dialog as Dialog
import Ui.Molecule.ErrorBox as ErrorBox exposing (errorBox)
import Ui.Molecule.Table as Table exposing (table)
import Ui.Molecule.TableCell as TableCell
import Ui.Theme.Card as Card exposing (card)
import Ui.Theme.Color exposing (surfaceContainerHigh)
import Ui.Theme.Dimensions exposing (paneWidthMax)
import Ui.Theme.Typography exposing (body1, body1Strong, body2, h4, h5)
import View exposing (View)


type Model
    = Loading DataLoading
    | Failed DataFailed
    | Loaded DataLoaded


type alias DataLoading =
    {}


type alias DataFailed =
    { error : Error
    }


type Error
    = CouldNotLoadWorkspace Api.Error


type alias DataLoaded =
    { workspace : Workspace
    , roCrateImports : RemoteData Api.Error (List RoCrateImport)
    , dialog : Dialog
    }


type Dialog
    = NoDialog
    | DialogEditWorkspace DataDialogEditWorkspace
    | DialogAddMembers DataDialogAddMembers


type alias DataDialogEditWorkspace =
    { name : String
    , slug : String
    , slugModified : Bool
    , description : String
    }


type alias DataDialogAddMembers =
    { queryAuthor : String
    , authors : List Author
    , membershipType : MembershipType
    }


type alias Params =
    { uuid : String
    }


page : Shared.Model -> Request.With Params -> Page.With Model Msg
page shared request =
    let
        c =
            Shared.toContext shared
    in
    Page.advanced
        { init = init c request.params
        , update = update c request.key
        , subscriptions = subscriptions
        , view = view c
        }


init : C -> Params -> ( Model, Effect Msg )
init c params =
    ( Loading {}
    , Workspace.get c ApiReturnedWorkspace params.uuid
    )


type Msg
    = NoOp
      -- LOADING
    | ApiReturnedWorkspace (Api.Response Workspace)
      -- LOADED
    | ApiReturnedRoCrateImports (Api.Response (List RoCrateImport))
    | BrowserTickedForRoCrateImports
    | UserPressedEditWorkspace
    | MsgDialogEditWorkspace MsgDialogEditWorkspace
    | ApiReturnedUpdatedWorkspaceAfterEdit (Api.Response Workspace)
    | UserPressedRemoveMember Membership
    | ApiReturnedUpdatedWorkspaceAfterRemoveMember (Api.Response Workspace)
    | UserPressedAddMembers
    | UserCancelledDialog
    | MsgDialogAddMembers MsgDialogAddMembers
    | ApiReturnedUpdatedWorkspaceAfterAddMembers (Api.Response Workspace)


type MsgDialogEditWorkspace
    = UserChangedName String
    | UserChangedSlug String
    | UserPressedResetSlug
    | UserChangedDescription String
    | UserPressedStoreChanges


type MsgDialogAddMembers
    = UserChangedQueryAuthor String
    | UserSelectedAuthor Author
    | UserUnselectedAuthor Author
    | UserChangedMembershipType MembershipType
    | UserPressedAddMembersInDialog


update : C -> Browser.Navigation.Key -> Msg -> Model -> ( Model, Effect Msg )
update c key msg model =
    case model of
        Loading data ->
            updateLoading c msg data

        Failed _ ->
            ( model, Effect.none )

        Loaded data ->
            updateLoaded c key msg data
                |> Tuple.mapFirst Loaded


updateLoading : C -> Msg -> DataLoading -> ( Model, Effect Msg )
updateLoading c msg data =
    case msg of
        NoOp ->
            ( Loading data, Effect.none )

        -- LOADING
        ApiReturnedWorkspace (Err error) ->
            ( Failed { error = CouldNotLoadWorkspace error }
            , Effect.none
            )

        ApiReturnedWorkspace (Ok workspace) ->
            ( Loaded
                { workspace = workspace
                , dialog = NoDialog
                , roCrateImports = RemoteData.Loading
                }
            , RoCrateImport.list c ApiReturnedRoCrateImports workspace.uuid
            )

        -- LOADED
        ApiReturnedRoCrateImports _ ->
            ( Loading data, Effect.none )

        BrowserTickedForRoCrateImports ->
            ( Loading data, Effect.none )

        UserPressedEditWorkspace ->
            ( Loading data, Effect.none )

        MsgDialogEditWorkspace _ ->
            ( Loading data, Effect.none )

        ApiReturnedUpdatedWorkspaceAfterEdit _ ->
            ( Loading data, Effect.none )

        UserPressedRemoveMember _ ->
            ( Loading data, Effect.none )

        ApiReturnedUpdatedWorkspaceAfterRemoveMember _ ->
            ( Loading data, Effect.none )

        UserPressedAddMembers ->
            ( Loading data, Effect.none )

        UserCancelledDialog ->
            ( Loading data, Effect.none )

        MsgDialogAddMembers _ ->
            ( Loading data, Effect.none )

        ApiReturnedUpdatedWorkspaceAfterAddMembers _ ->
            ( Loading data, Effect.none )


updateLoaded : C -> Browser.Navigation.Key -> Msg -> DataLoaded -> ( DataLoaded, Effect Msg )
updateLoaded c _ msg data =
    case msg of
        NoOp ->
            ( data, Effect.none )

        -- LOADING
        ApiReturnedWorkspace _ ->
            ( data, Effect.none )

        -- LOADED
        ApiReturnedRoCrateImports (Err error) ->
            ( { data | roCrateImports = RemoteData.Failure error }
            , Effect.none
            )

        ApiReturnedRoCrateImports (Ok roCrateImports) ->
            ( { data | roCrateImports = RemoteData.Success roCrateImports }
            , Effect.none
            )

        BrowserTickedForRoCrateImports ->
            ( data
            , RoCrateImport.list c ApiReturnedRoCrateImports data.workspace.uuid
            )

        UserPressedEditWorkspace ->
            ( { data
                | dialog =
                    DialogEditWorkspace
                        { name = data.workspace.name
                        , slug = data.workspace.slug
                        , slugModified = True
                        , description = data.workspace.description
                        }
              }
            , Effect.none
            )

        MsgDialogEditWorkspace msgDialog ->
            case data.dialog of
                DialogEditWorkspace dataDialog ->
                    let
                        ( dataDialogUpdated, effect ) =
                            updateDialogEditWorkspace c msgDialog data.workspace dataDialog
                    in
                    ( { data | dialog = DialogEditWorkspace dataDialogUpdated }
                    , effect
                    )

                _ ->
                    ( data, Effect.none )

        ApiReturnedUpdatedWorkspaceAfterEdit (Err _) ->
            ( data
            , Effect.fromAction
                (Action.AddToast
                    { message = verbatim "Could not update workspace." }
                )
            )

        ApiReturnedUpdatedWorkspaceAfterEdit (Ok workspace) ->
            ( { data
                | dialog = NoDialog
                , workspace = workspace
              }
            , Effect.fromAction
                (Action.AddToastExpireIn 3000
                    { message = verbatim "Updated workspace." }
                )
            )

        UserPressedRemoveMember membership ->
            ( data
            , Workspace.removeMembership c ApiReturnedUpdatedWorkspaceAfterRemoveMember membership
            )

        ApiReturnedUpdatedWorkspaceAfterRemoveMember (Err _) ->
            ( data
            , Effect.fromAction
                (Action.AddToast
                    { message = verbatim "Could not remove member from workspace." }
                )
            )

        ApiReturnedUpdatedWorkspaceAfterRemoveMember (Ok workspace) ->
            ( { data
                | dialog = NoDialog
                , workspace = workspace
              }
            , Effect.fromAction
                (Action.AddToastExpireIn 3000
                    { message = verbatim "Removed member from workspace." }
                )
            )

        UserPressedAddMembers ->
            ( { data
                | dialog =
                    DialogAddMembers
                        { queryAuthor = ""
                        , authors = []
                        , membershipType = Workspace.Guest
                        }
              }
            , Effect.none
            )

        UserCancelledDialog ->
            ( { data | dialog = NoDialog }
            , Effect.none
            )

        MsgDialogAddMembers msgDialog ->
            case data.dialog of
                DialogAddMembers dataDialog ->
                    let
                        ( dataDialogUpdated, effect ) =
                            updateDialogAddMembers c msgDialog data.workspace dataDialog
                    in
                    ( { data | dialog = DialogAddMembers dataDialogUpdated }
                    , effect
                    )

                _ ->
                    ( data, Effect.none )

        ApiReturnedUpdatedWorkspaceAfterAddMembers (Err _) ->
            ( data
            , Effect.fromAction
                (Action.AddToast
                    { message = verbatim "Could not add members to workspace." }
                )
            )

        ApiReturnedUpdatedWorkspaceAfterAddMembers (Ok workspace) ->
            ( { data
                | dialog = NoDialog
                , workspace = workspace
              }
            , Effect.fromAction
                (Action.AddToastExpireIn 3000
                    { message = verbatim "Added members to workspace." }
                )
            )


updateDialogEditWorkspace : C -> MsgDialogEditWorkspace -> Workspace -> DataDialogEditWorkspace -> ( DataDialogEditWorkspace, Effect Msg )
updateDialogEditWorkspace c msg workspace data =
    case msg of
        UserChangedName name ->
            ( { data
                | name = name
                , slug =
                    if data.slugModified then
                        data.slug

                    else
                        String.slugify name
              }
            , Effect.none
            )

        UserChangedSlug slug ->
            ( { data
                | slug = slug
                , slugModified = True
              }
            , Effect.none
            )

        UserPressedResetSlug ->
            ( { data
                | slug = String.slugify data.name
                , slugModified = False
              }
            , Effect.none
            )

        UserChangedDescription description ->
            ( { data | description = description }
            , Effect.none
            )

        UserPressedStoreChanges ->
            ( data
            , Workspace.update c
                ApiReturnedUpdatedWorkspaceAfterEdit
                workspace
                { name = String.trim data.name
                , slug = String.trim data.slug
                , description = data.description
                }
            )


updateDialogAddMembers : C -> MsgDialogAddMembers -> Workspace -> DataDialogAddMembers -> ( DataDialogAddMembers, Effect Msg )
updateDialogAddMembers c msg workspace data =
    case msg of
        UserChangedQueryAuthor queryAuthor ->
            ( { data | queryAuthor = queryAuthor }
            , Effect.none
            )

        UserSelectedAuthor author ->
            ( { data | authors = data.authors ++ [ author ] }
            , Effect.none
            )

        UserUnselectedAuthor author ->
            ( { data | authors = List.filter (\authorSelected -> authorSelected.id /= author.id) data.authors }
            , Effect.none
            )

        UserChangedMembershipType membershipType ->
            ( { data | membershipType = membershipType }
            , Effect.none
            )

        UserPressedAddMembersInDialog ->
            let
                paramFromAuthor author =
                    { author = author
                    , membershipType = data.membershipType
                    }
            in
            ( data
            , Workspace.addMemberships c
                ApiReturnedUpdatedWorkspaceAfterAddMembers
                workspace.uuid
                (List.map paramFromAuthor data.authors)
            )


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        Loading _ ->
            Sub.none

        Failed _ ->
            Sub.none

        Loaded data ->
            let
                anyPendingOrStarted =
                    data.roCrateImports
                        |> RemoteData.withDefault []
                        |> List.any
                            (\roCrateImport ->
                                (roCrateImport.status == RoCrateImport.Pending)
                                    || (roCrateImport.status == RoCrateImport.Started)
                            )
            in
            if anyPendingOrStarted then
                Time.every 3000 (\_ -> BrowserTickedForRoCrateImports)

            else
                Sub.none



-- VIEW


view : C -> Model -> View Msg
view c model =
    { title = "Workspace"
    , element =
        case model of
            Loading data ->
                viewLoading c data

            Failed data ->
                viewFailed c data

            Loaded data ->
                viewLoaded c data
    , overlays = viewOverlays c model
    , breadcrumbs = breadcrumbs model
    , fabVisible = True
    , navigationRailVisible = True
    , loading =
        case model of
            Loading _ ->
                True

            Failed _ ->
                False

            Loaded _ ->
                False
    }


breadcrumbs : Model -> Breadcrumbs
breadcrumbs model =
    case model of
        Loading _ ->
            Breadcrumbs.from [] (verbatim "Workspace")

        Failed _ ->
            Breadcrumbs.from [] (verbatim "Workspace")

        Loaded data ->
            Breadcrumbs.from [] (verbatim data.workspace.name)


viewLoading : C -> DataLoading -> Element Msg
viewLoading _ _ =
    none


viewFailed : C -> DataFailed -> Element Msg
viewFailed c data =
    data.error
        |> errorToHeading
        |> errorBox
        |> ErrorBox.withInfo (errorToString c data.error)
        |> ErrorBox.view
        |> el
            [ centerX
            , centerY
            ]


errorToHeading : Error -> String
errorToHeading error =
    case error of
        CouldNotLoadWorkspace _ ->
            "Could not load workspace"


errorToString : C -> Error -> String
errorToString c error =
    case error of
        CouldNotLoadWorkspace errorApi ->
            errorApi
                |> Api.errorToMessages
                |> Localize.verbatims c


viewLoaded : C -> DataLoaded -> Element Msg
viewLoaded =
    lazy2
        (\c data ->
            el
                [ width fill
                , height fill
                , scrollbarY
                ]
                (viewLoadedBody c data)
        )


viewLoadedBody : C -> DataLoaded -> Element Msg
viewLoadedBody c data =
    column
        [ width (maximum paneWidthMax fill)
        , centerX
        , case c.device.class of
            Phone ->
                paddingEach
                    { top = 0
                    , bottom = 16 + 56 + 16
                    , left = 16
                    , right = 16
                    }

            _ ->
                paddingEach
                    { top = 0
                    , bottom = 16
                    , left = 16
                    , right = 16
                    }
        , spacing 32
        ]
        [ column
            [ width fill
            , spacing 32
            ]
            [ viewMeta c data
            , viewMembers c data
            , viewRoCrateImports c data
            ]
        ]


viewMeta : C -> DataLoaded -> Element Msg
viewMeta c data =
    column
        [ width fill
        , spacing 16
        ]
        [ row
            [ width fill
            ]
            [ h4 (verbatim data.workspace.name)
            , if Workspace.canEdit c.author data.workspace then
                el [ alignRight ]
                    (Icon.Edit
                        |> buttonIcon UserPressedEditWorkspace
                        |> ButtonIcon.toElement
                    )

              else
                none
            ]
        , if data.workspace.description == "" then
            none

          else
            paragraph [ width fill ]
                [ body1 (verbatim data.workspace.description)
                ]
        ]


viewMembers : C -> DataLoaded -> Element Msg
viewMembers c data =
    let
        memberships =
            List.filter (\{ deletedAt } -> deletedAt == Nothing) data.workspace.memberships
    in
    column
        [ width fill
        , height fill
        , spacing 16
        ]
        [ h5 (verbatim "Members")
        , column
            [ width fill
            , spacing 16
            ]
            (List.map (viewMembership c data.workspace) memberships)
        , if Workspace.canAddMembers c.author data.workspace then
            "Add members"
                |> verbatim
                |> button UserPressedAddMembers
                |> Button.withFormat Button.Outlined
                |> Button.view

          else
            none
        ]


viewMembership : C -> Workspace -> Membership -> Element Msg
viewMembership c workspace membership =
    row
        [ width fill
        , spacing 16
        ]
        [ viewAuthorLogo membership.author
        , column
            [ spacing 8
            , centerY
            ]
            [ body1Strong (labelAuthor membership.author)
            , row
                [ spacing 8 ]
                [ body2 (labelMembershipType membership.membershipType)
                , body2 (verbatim "·")
                , body2 (verbatim membership.author.user.email)
                ]
            ]
        , if Workspace.canRemoveMember c.author workspace membership then
            el [ alignRight ]
                ("Remove"
                    |> verbatim
                    |> button (UserPressedRemoveMember membership)
                    |> Button.withFormat Button.Text
                    |> Button.view
                )

          else
            none
        ]
        |> card
        |> Card.withType Card.Outlined
        |> Card.toElement


viewAuthorLogo : Author -> Element msg
viewAuthorLogo author =
    (String.left 1 author.user.firstName ++ String.left 1 author.user.lastName)
        |> Element.text
        |> el
            [ centerX
            , centerY
            , Font.size 16
            , Font.medium
            ]
        |> el
            [ width (px 48)
            , height (px 48)
            , Border.rounded 24
            , Background.color surfaceContainerHigh
            ]


labelAuthor : Author -> Fluent
labelAuthor author =
    verbatim (nameAuthor author)


labelMembershipType : MembershipType -> Fluent
labelMembershipType membershipType =
    verbatim
        (case membershipType of
            Workspace.Owner ->
                "Owner"

            Workspace.Admin ->
                "Admin"

            Workspace.Manager ->
                "Manager"

            Workspace.Editor ->
                "Editor"

            Workspace.Guest ->
                "Guest"
        )


viewRoCrateImports : C -> DataLoaded -> Element Msg
viewRoCrateImports c data =
    let
        roCrateImports =
            data.roCrateImports
                |> RemoteData.withDefault []
                |> List.sortBy (.createdAt >> Time.posixToMillis)
                |> List.reverse
    in
    column
        [ width fill
        , height fill
        , spacing 16
        ]
        [ h5 (verbatim "RO-Crate Imports")
        , { data = roCrateImports
          , columns = columnsRoCrateImport
          }
            |> table
            |> Table.view c
        ]


columnsRoCrateImport : List (Table.Column RoCrateImport Msg)
columnsRoCrateImport =
    [ { header = TableCell.textBold (verbatim "File")
      , width = fill
      , view = \roCrateImport -> TableCell.text (verbatim roCrateImport.filename)
      }
    , { header = TableCell.textBold (verbatim "Uploaded by")
      , width = shrink
      , view = \roCrateImport -> TableCell.text (verbatim roCrateImport.createdBy.user.username)
      }
    , { header = TableCell.textBold (verbatim "Uploaded at")
      , width = shrink
      , view = \roCrateImport -> TableCell.dateTime roCrateImport.createdAt
      }
    , { header = TableCell.textBold (verbatim "Finished at")
      , width = shrink
      , view =
            \roCrateImport ->
                roCrateImport.stoppedAt
                    |> Maybe.map TableCell.dateTime
                    |> Maybe.withDefault TableCell.empty
      }
    , { header =
            TableCell.textBold (verbatim "Took")
                |> TableCell.withAffix
                    { label = "s"
                    , value = Rdf.iri "#"
                    }
      , width = shrink
      , view =
            \roCrateImport ->
                case roCrateImport.stoppedAt of
                    Nothing ->
                        TableCell.empty

                    Just stoppedAt ->
                        let
                            tookInSeconds =
                                (Time.posixToMillis stoppedAt - Time.posixToMillis roCrateImport.createdAt)
                                    // 1000
                        in
                        TableCell.decimalStatic
                            { value =
                                tookInSeconds
                                    |> Decimal.fromInt
                                    |> Just
                            , valueDecimalPlaces = 0
                            , prefix = Nothing
                            , minimalPreDecimalPositions = 0
                            }
      }
    , { header = TableCell.textBold (verbatim "Status")
      , width = shrink
      , view = \roCrateImport -> TableCell.text (verbatim (roCrateImportStatusToString roCrateImport.status))
      }
    ]


roCrateImportStatusToString : RoCrateImport.Status -> String
roCrateImportStatusToString status =
    case status of
        RoCrateImport.Pending ->
            "Waiting"

        RoCrateImport.Started ->
            "Processing..."

        RoCrateImport.Success ->
            "Successful"

        RoCrateImport.Failure ->
            "Failed"


viewOverlays : C -> Model -> List (Element Msg)
viewOverlays c model =
    case model of
        Loading _ ->
            []

        Failed _ ->
            []

        Loaded data ->
            case data.dialog of
                NoDialog ->
                    []

                DialogEditWorkspace dataDialog ->
                    [ viewDialogEditWorkspace dataDialog ]

                DialogAddMembers dataDialog ->
                    [ viewDialogAddMembers c data.workspace dataDialog ]


viewDialogEditWorkspace : DataDialogEditWorkspace -> Element Msg
viewDialogEditWorkspace data =
    column
        [ width fill ]
        [ data.name
            |> textInput (MsgDialogEditWorkspace << UserChangedName) (TextInput.labelAbove (verbatim "Name"))
            |> TextInput.view
        , row
            [ width fill
            , spacing 16
            ]
            [ data.slug
                |> textInput (MsgDialogEditWorkspace << UserChangedSlug) (TextInput.labelAbove (verbatim "Slug"))
                |> TextInput.view
            , el
                [ moveDown 4
                ]
                ("Reset"
                    |> verbatim
                    |> button (MsgDialogEditWorkspace UserPressedResetSlug)
                    |> Button.withFormat Button.Outlined
                    |> Button.withEnabled data.slugModified
                    |> Button.view
                )
            ]
        , el
            [ width fill
            , height (px 128)
            ]
            (data.description
                |> textArea (MsgDialogEditWorkspace << UserChangedDescription)
                |> TextArea.withLabel (verbatim "Description")
                |> TextArea.withSupportingText (verbatim "Optional")
                |> TextArea.toElement
            )
        ]
        |> Dialog.desktop (verbatim "Edit workspace")
        |> Dialog.withScrimEvents
            { userPressedScrim = UserCancelledDialog
            , noOp = NoOp
            }
        |> Dialog.withActions
            [ el
                [ paddingXY 8 0 ]
                ("Store changes"
                    |> verbatim
                    |> button (MsgDialogEditWorkspace UserPressedStoreChanges)
                    |> Button.view
                )
            , el [ alignRight ]
                ("Cancel"
                    |> verbatim
                    |> button UserCancelledDialog
                    |> Button.withFormat Button.Text
                    |> Button.view
                )
            ]
        |> Dialog.toElement


viewDialogAddMembers : C -> Workspace -> DataDialogAddMembers -> Element Msg
viewDialogAddMembers c workspace data =
    column
        [ width fill
        , spacing 16
        ]
        [ viewAuthors c workspace data
        , viewMembershipType data
        , Portal.target
        ]
        |> Dialog.desktop (verbatim "Add members")
        |> Dialog.withScrimEvents
            { userPressedScrim = UserCancelledDialog
            , noOp = NoOp
            }
        |> Dialog.withActions
            [ el
                [ paddingXY 8 0 ]
                ("Add members"
                    |> verbatim
                    |> button (MsgDialogAddMembers UserPressedAddMembersInDialog)
                    |> Button.view
                )
            , el [ alignRight ]
                ("Cancel"
                    |> verbatim
                    |> button UserCancelledDialog
                    |> Button.withFormat Button.Text
                    |> Button.view
                )
            ]
        |> Dialog.toElement


viewAuthors : C -> Workspace -> DataDialogAddMembers -> Element Msg
viewAuthors c workspace data =
    let
        options =
            c.authors
                |> RemoteData.withDefault []
                |> List.filter isNotSelected
                |> List.filter isNotMember
                |> Query.sortByScore
                    [ nameAuthor >> Just
                    , .user >> .email >> Just
                    ]
                    data.queryAuthor

        isNotSelected author =
            not (List.any (\authorSelected -> author.id == authorSelected.id) data.authors)

        isNotMember author =
            not (List.any (\membership -> author.id == membership.author.id) memberships)

        memberships =
            List.filter (\{ deletedAt } -> deletedAt == Nothing) workspace.memberships
    in
    comboboxMultiselect
        { id = Pointer.verbatim "dialog-add-members--authors"
        , label = ComboboxMultiselect.labelAbove (verbatim "Users")
        , initialQuery = ""
        , supportingText = Nothing
        , help = []
        , options = options
        , selected = data.authors
        , toOption = authorToOption
        , fromOption = authorFromOption c
        , onChange = MsgDialogAddMembers << UserChangedQueryAuthor
        , onSelect = MsgDialogAddMembers << UserSelectedAuthor
        , onUnselect = MsgDialogAddMembers << UserUnselectedAuthor
        , onCopy = Nothing
        , onPaste = Nothing
        , noOp = NoOp
        , renderSubLabels = True
        }


authorToOption : Author -> OptionCombobox
authorToOption author =
    { id = author.user.email
    , value = nameAuthor author
    , label = verbatim (nameAuthor author)
    , subLabel = verbatim author.user.email
    }


authorFromOption : C -> OptionCombobox -> Maybe Author
authorFromOption c option =
    List.find (\author -> author.user.email == option.id) (RemoteData.withDefault [] c.authors)


nameAuthor : Author -> String
nameAuthor author =
    author.user.firstName ++ " " ++ author.user.lastName


viewMembershipType : DataDialogAddMembers -> Element Msg
viewMembershipType data =
    [ Radio.option Workspace.Guest (verbatim "Guest")
        |> Radio.withSupportingText (verbatim "Can see all documents within the workspace.")
    , Radio.option Workspace.Editor (verbatim "Editor")
        |> Radio.withSupportingText (verbatim "Can create documents within the workspace, which must conform to a SHACL document. Can edit the documents they created.")
    , Radio.option Workspace.Manager (verbatim "Manager")
        |> Radio.withSupportingText (verbatim "Can create any documents, in particular SHACL Shape documents. Can edit and soft-delete any document.")
    , Radio.option Workspace.Admin (verbatim "Admin")
        |> Radio.withSupportingText (verbatim "Can manage workspace members and change the workspace settings.")
    , Radio.option Workspace.Owner (verbatim "Owner")
        |> Radio.withSupportingText (verbatim "Can soft-delete the workspace")
    ]
        |> radio (MsgDialogAddMembers << UserChangedMembershipType) (verbatim "Role") (Just data.membershipType)
        |> Radio.view
