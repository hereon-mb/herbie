pages-settings--action--generate-access-token = Generiere Access Token
pages-settings--action--generate-new-access-token = Generiere ein neues Access Token
pages-settings--caption--access-token = Access Token
pages-settings--caption--expiry = Gültig bis
pages-settings--paragraph--no-access-token--info = Falls du auf die Herbie API außerhalb deines Browsers zugreifen möchtest, kannst du hier ein Access Token erzeugen.
pages-settings--paragraph--usage-info = Du kannst dieses Token nutzen, um dich gegenüber der Herbie API zu authentifizieren, indem du den folgenden HTTP Header angibst:
