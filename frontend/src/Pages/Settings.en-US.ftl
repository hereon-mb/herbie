pages-settings--action--generate-access-token = Generate access token
pages-settings--action--generate-new-access-token = Generate another access token
pages-settings--caption--access-token = Access token
pages-settings--caption--expiry = Expires at
pages-settings--paragraph--no-access-token--info = If you want to access the Herbie API outside your browser, you can generate an acess token here.
pages-settings--paragraph--usage-info = You can use this token to authenticate as your user against the Herbie API by providing the following HTTP header:
