module Pages.Datasets.Uuid_ exposing
    ( Model
    , Msg
    , Params
    , events
    , page
    )

import Action
import Api
import Browser.Navigation
import Context exposing (C)
import Dict exposing (Dict)
import Effect exposing (Effect)
import Element
    exposing
        ( none
        )
import Event exposing (Event)
import Fluent
    exposing
        ( verbatim
        )
import Gen.Route as Route
import Href
import Ontology.Herbie as Herbie
import Page
import Rdf.Decode as Decode
import Rdf.Graph exposing (Graph)
import Request
import Shared
import Ui.Molecule.Breadcrumbs as Breadcrumbs
import View exposing (View)


page : Shared.Model -> Request.With Params -> Page.With Model Msg
page shared request =
    let
        c =
            Shared.toContext shared
    in
    Page.advanced
        { init = init c request.params
        , update = update c request.key request.query
        , subscriptions = subscriptions
        , view = view c
        }


type alias Params =
    { uuid : String
    }


type alias Model =
    { uuid : String }


init : C -> Params -> ( Model, Effect Msg )
init c params =
    ( { uuid = params.uuid }
    , Api.iriHttp c [ "datasets", params.uuid ]
        |> Action.GetGraph
            { store = True
            , reload = False
            , closure = False
            }
            CacheReturnedDocument
        |> Effect.fromAction
    )


type Msg
    = CacheReturnedDocument (Api.Response Graph)


update : C -> Browser.Navigation.Key -> Dict String String -> Msg -> Model -> ( Model, Effect Msg )
update c key query msg model =
    case msg of
        CacheReturnedDocument (Err error) ->
            ( model
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        CacheReturnedDocument (Ok graph) ->
            let
                iri =
                    Api.iriHttp c [ "datasets", model.uuid ]
            in
            case Decode.decode (Decode.from iri Herbie.decoderDocument) graph of
                Err _ ->
                    ( model
                    , Effect.none
                    )

                Ok document ->
                    ( model
                    , case document.draft of
                        Nothing ->
                            { uuid = model.uuid
                            , index = String.fromInt (List.length document.versions)
                            }
                                |> Route.Datasets__Uuid___Versions__Index_
                                |> Href.fromRouteWith c query
                                |> Browser.Navigation.replaceUrl key
                                |> Effect.fromCmd

                        Just _ ->
                            { uuid = model.uuid }
                                |> Route.Datasets__Uuid___Draft
                                |> Href.fromRouteWith c query
                                |> Browser.Navigation.replaceUrl key
                                |> Effect.fromCmd
                    )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


events : C -> Model -> Event Msg
events _ _ =
    Event.none


view : C -> Model -> View Msg
view _ model =
    { title = "Dataset"
    , element = none
    , overlays = []
    , breadcrumbs =
        Breadcrumbs.from
            [ Breadcrumbs.static (verbatim "Documents")
            ]
            (verbatim model.uuid)
    , fabVisible = True
    , navigationRailVisible = True
    , loading = True
    }
