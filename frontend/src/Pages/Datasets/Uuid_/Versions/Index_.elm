module Pages.Datasets.Uuid_.Versions.Index_ exposing (Model, Msg, Params, events, initWith, page)

import Accessibility.Blob as Blob
import Action
import Api
import Api.Datasets as Datasets
import Browser.Navigation
import Context exposing (C)
import Dict exposing (Dict)
import Effect exposing (Effect)
import Element
    exposing
        ( Element
        , alignRight
        , alignTop
        , centerY
        , column
        , el
        , fill
        , height
        , none
        , padding
        , paddingEach
        , paddingXY
        , paragraph
        , px
        , row
        , scrollbarY
        , spacing
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Extra
    exposing
        ( style
        )
import Element.Font as Font
import Element.Lazy exposing (lazy2, lazy4)
import Event exposing (Event)
import File exposing (File)
import File.Download
import File.Select
import Fluent
    exposing
        ( Fluent
        , verbatim
        )
import Gen.Route as Route
import Href
import Http
import Maybe.Extra as Maybe
import Maybe.Pipeline as Maybe
import Ontology
import Ontology.Herbie as Herbie
import Page
import Ports
import Rdf exposing (Iri)
import Rdf.Decode as Decode
import Rdf.DictIri as DictIri exposing (DictIri)
import Rdf.Format as Format exposing (Format)
import Rdf.Graph as Rdf exposing (Graph)
import Request
import Result.Extra as Result
import Shacl
import Shacl.Extra as Shacl
import Shacl.Form.Loader as Loader exposing (Loader)
import Shacl.Form.Localize as Localize
import Shacl.Form.Viewed as Form exposing (Form)
import Shared
import Shared.Loading as Loading exposing (Loading)
import Ui.Atom.Banner as Banner exposing (banner)
import Ui.Atom.Button as Button exposing (button, buttonLink)
import Ui.Atom.ButtonIcon as ButtonIcon exposing (buttonIcon)
import Ui.Atom.Checkbox as Checkbox exposing (checkbox)
import Ui.Atom.ChipSuggestion as ChipSuggestion exposing (chipSuggestion)
import Ui.Atom.Icon as Icon
import Ui.Atom.Labelled as Labelled
import Ui.Atom.Pane as Pane exposing (pane)
import Ui.Atom.Radio as Radio
import Ui.Molecule.Breadcrumbs as Breadcrumbs exposing (Breadcrumbs)
import Ui.Molecule.Dialog as Dialog
import Ui.Organism.DocumentVersions as DocumentVersions
import Ui.Organism.FormPreview as FormPreview
import Ui.Organism.GraphSerializations as GraphSerializations
import Ui.Organism.Instances as Instances
import Ui.Organism.InstancesList as InstancesList
import Ui.Theme.Color
    exposing
        ( outlineVariant
        , surfaceContainer
        )
import Ui.Theme.Input as Input
import Ui.Theme.Typography
    exposing
        ( body1
        , body1Mono
        , body1Strong
        , body2
        )
import View exposing (View)


page : Shared.Model -> Request.With Params -> Page.With Model Msg
page shared request =
    let
        c =
            Shared.toContext shared
    in
    Page.advanced
        { init = init c request
        , update = update c request.key
        , subscriptions = subscriptions
        , view = view c
        }


view : C -> Model -> View Msg
view c model =
    { title = "Version"
    , element = element c model
    , overlays = overlays c model
    , breadcrumbs = breadcrumbs c model
    , fabVisible = True
    , navigationRailVisible = True
    , loading =
        case model of
            Loading _ ->
                True

            Failed _ ->
                False

            Loaded data ->
                loadingForm data || data.creatingDraft
    }


type Model
    = Loading DataLoading
    | Failed DataFailed
    | Loaded DataLoaded


type alias DataLoading =
    { uuid : String
    , index : Int
    , iri : Iri
    , metaVisible : Bool
    , tabVisible : Tab
    , formatVisible : Format
    , instancesExpanded : Bool

    -- DATA
    , loader : Loader
    }


type alias DataFailed =
    { uuid : String
    , index : Int
    , metaVisible : Bool
    , tabVisible : Tab
    , formatVisible : Format
    , instancesExpanded : Bool
    }


type alias DataLoaded =
    { tabVisible : Tab
    , formatVisible : Format
    , metaVisible : Bool
    , instancesListConfig : InstancesList.Config
    , instancesExpanded : Bool

    -- DOCUMENT
    , uuid : String
    , index : Int
    , document : Herbie.Document
    , latest : Loader.Latest
    , version : Herbie.DocumentVersion
    , graph : Graph
    , graphCombined : Graph
    , access : Herbie.Access
    , turtle : String
    , nTriples : String
    , jsonLd : String
    , conformsTo : Maybe Iri
    , form : Maybe Form
    , formPreview : Form

    -- DIALOGS
    , dialogImportGraph : Maybe DialogImportGraph
    , creatingDraft : Bool
    }


type Tab
    = TabCard
    | TabGraph


tabAndFormatToString : Tab -> Format -> String
tabAndFormatToString tab format =
    case tab of
        TabCard ->
            "card"

        TabGraph ->
            Format.toString format


tabAndFormatFromString : String -> Maybe ( Tab, Format )
tabAndFormatFromString raw =
    case raw of
        "card" ->
            Just ( TabCard, Format.Turtle )

        _ ->
            raw
                |> Format.fromString
                |> Maybe.map (Tuple.pair TabGraph)


uuid : Model -> String
uuid model =
    case model of
        Loading data ->
            data.uuid

        Failed data ->
            data.uuid

        Loaded data ->
            data.uuid


index : Model -> Int
index model =
    case model of
        Loading data ->
            data.index

        Failed data ->
            data.index

        Loaded data ->
            data.index


type alias DialogImportGraph =
    { file : File
    , format : Maybe Format
    }


type alias Params =
    { uuid : String
    , index : String
    }


init : C -> Request.With Params -> ( Model, Effect Msg )
init c { query, params } =
    let
        iri =
            Api.iriHttp c [ "datasets", params.uuid ]

        metaVisible =
            query
                |> Dict.get "meta"
                |> Maybe.map (\meta -> meta == "")
                |> Maybe.withDefault False

        instancesExpanded =
            query
                |> Dict.get "instances"
                |> Maybe.map (\instances -> instances == "")
                |> Maybe.withDefault False

        ( tabVisible, formatVisible ) =
            query
                |> Dict.get "tab"
                |> Maybe.andThen tabAndFormatFromString
                |> Maybe.withDefault ( TabCard, Format.Turtle )
    in
    case String.toInt params.index of
        Nothing ->
            ( Failed
                { uuid = params.uuid
                , index = -1 -- FIXME Handle this better.
                , metaVisible = metaVisible
                , tabVisible = tabVisible
                , formatVisible = formatVisible
                , instancesExpanded = instancesExpanded
                }
            , Effect.none
            )

        Just theIndex ->
            let
                ( loader, effectLoader ) =
                    Loader.loadVersion iri theIndex
            in
            ( Loading
                { uuid = params.uuid
                , index = theIndex
                , iri = iri
                , metaVisible = metaVisible
                , tabVisible = tabVisible
                , formatVisible = formatVisible
                , instancesExpanded = instancesExpanded
                , loader = loader
                }
            , Effect.map MsgLoader effectLoader
            )


initWith : C -> Request.With Params -> Model -> ( Model, Effect Msg )
initWith c ({ query } as request) model =
    let
        metaVisible =
            query
                |> Dict.get "meta"
                |> Maybe.map (\meta -> meta == "")
                |> Maybe.withDefault False

        instancesExpanded =
            query
                |> Dict.get "instances"
                |> Maybe.map (\instances -> instances == "")
                |> Maybe.withDefault False

        ( tabVisible, formatVisible ) =
            query
                |> Dict.get "tab"
                |> Maybe.andThen tabAndFormatFromString
                |> Maybe.withDefault ( TabCard, Format.Turtle )
    in
    case model of
        Loaded data ->
            ( Loaded
                { data
                    | metaVisible =
                        if metaVisible == data.metaVisible then
                            data.metaVisible

                        else
                            metaVisible
                    , instancesExpanded = instancesExpanded
                    , tabVisible =
                        if tabVisible == data.tabVisible then
                            data.tabVisible

                        else
                            tabVisible
                    , formatVisible =
                        if formatVisible == data.formatVisible then
                            data.formatVisible

                        else
                            formatVisible
                }
            , Effect.none
            )

        _ ->
            init c request


type Msg
    = NoOp
      -- LOADING
    | MsgLoader Loader.Msg
      -- LOADED
    | CacheUpdatedDocument Graph
    | ApiReturnedDocumentLatestTurtle String
    | ApiReturnedDocumentLatestNTriples String
    | ApiReturnedDocumentLatestJsonLd String
    | UserSelectedTab Tab
    | UserPressedDownload
    | ApiReturnedDatasetForDownload (Api.Response String)
    | UserPressedDelete
    | ApiDeletedDataset (Api.Response String)
    | UserPressedToggleMeta
    | UserSelectedFormatVisible Format
    | UserPressedInstancesFilterAll
    | UserPressedInstancesFilterOntology
    | UserPressedInstancesFilterShacl
    | UserPressedInstancesFilterOther
    | UserToggledExpansionOnInstances
      -- FORM
    | MsgForm Form.Msg
    | MsgFormPreview Form.Msg
      -- STATEMENTS
    | UserPressedCopyIri Iri
      -- ACTIONS
    | UserPressedEditDocument
    | UserPressedUploadNewVersion
    | BrowserLoadedGraphFile File
    | UserSelectedFormat Format
    | UserPressedCancelInDialogImportGraph
    | UserPressedImportInDialogImportGraph
    | ApiStartedEditOfDocument (Api.Response ())
    | UserCheckedVisibleToEveryone Bool
    | ApiUpdatedDatasetAccess (Api.Response String)
    | CacheStoredDatasetAccess (Api.Response (DictIri Graph))


update : C -> Browser.Navigation.Key -> Msg -> Model -> ( Model, Effect Msg )
update c key msg model =
    case model of
        Loading data ->
            updateLoading c msg data

        Failed data ->
            updateFailed c msg data
                |> Tuple.mapFirst Failed

        Loaded data ->
            updateLoaded c key msg data
                |> Tuple.mapFirst Loaded


updateLoading : C -> Msg -> DataLoading -> ( Model, Effect Msg )
updateLoading c msg data =
    case msg of
        NoOp ->
            ( Loading data, Effect.none )

        -- LOADING
        MsgLoader msgLoader ->
            case Loader.update c msgLoader data.loader of
                Loader.Running loaderUpdated effectLoader ->
                    ( Loading { data | loader = loaderUpdated }
                    , Effect.map MsgLoader effectLoader
                    )

                Loader.SuccessDraft _ ->
                    ( Loading data, Effect.none )

                Loader.SuccessDraftWithForm _ ->
                    ( Loading data, Effect.none )

                Loader.SuccessVersion stuff ->
                    Just initLoadedWithoutOntology
                        |> Maybe.hardcoded c
                        |> Maybe.hardcoded data
                        |> Maybe.hardcoded stuff.document
                        |> Maybe.hardcoded stuff.version
                        |> Maybe.hardcoded stuff.latest
                        |> Maybe.hardcoded stuff.graphVersion
                        |> Maybe.hardcoded stuff.graphAccess
                        |> Maybe.hardcoded stuff.graphEntered
                        |> Maybe.hardcoded stuff.graphGenerated
                        |> Maybe.withDefault ( initFailed data, Effect.none )

                Loader.SuccessVersionWithForm stuff ->
                    Just initLoadedWithOntology
                        |> Maybe.hardcoded c
                        |> Maybe.hardcoded data
                        |> Maybe.hardcoded stuff.document
                        |> Maybe.hardcoded stuff.version
                        |> Maybe.hardcoded stuff.latest
                        |> Maybe.hardcoded stuff.graphVersion
                        |> Maybe.hardcoded stuff.graphAccess
                        |> Maybe.hardcoded stuff.graphEntered
                        |> Maybe.hardcoded stuff.graphGenerated
                        |> Maybe.hardcoded stuff.form
                        |> Maybe.hardcoded stuff.conformsTo
                        |> Maybe.withDefault ( initFailed data, Effect.none )

                Loader.Failed _ ->
                    ( Failed
                        { uuid = data.uuid
                        , index = data.index
                        , metaVisible = data.metaVisible
                        , tabVisible = data.tabVisible
                        , formatVisible = data.formatVisible
                        , instancesExpanded = data.instancesExpanded
                        }
                    , Effect.none
                    )

        -- LOADED
        CacheUpdatedDocument _ ->
            ( Loading data, Effect.none )

        ApiReturnedDocumentLatestTurtle _ ->
            ( Loading data, Effect.none )

        ApiReturnedDocumentLatestNTriples _ ->
            ( Loading data, Effect.none )

        ApiReturnedDocumentLatestJsonLd _ ->
            ( Loading data, Effect.none )

        UserSelectedTab _ ->
            ( Loading data, Effect.none )

        UserPressedDownload ->
            ( Loading data, Effect.none )

        ApiReturnedDatasetForDownload _ ->
            ( Loading data, Effect.none )

        UserPressedDelete ->
            ( Loading data, Effect.none )

        ApiDeletedDataset _ ->
            ( Loading data, Effect.none )

        UserPressedToggleMeta ->
            ( Loading data, Effect.none )

        UserSelectedFormatVisible _ ->
            ( Loading data, Effect.none )

        UserPressedInstancesFilterAll ->
            ( Loading data, Effect.none )

        UserPressedInstancesFilterOntology ->
            ( Loading data, Effect.none )

        UserPressedInstancesFilterShacl ->
            ( Loading data, Effect.none )

        UserPressedInstancesFilterOther ->
            ( Loading data, Effect.none )

        UserToggledExpansionOnInstances ->
            ( Loading data, Effect.none )

        MsgForm _ ->
            ( Loading data, Effect.none )

        MsgFormPreview _ ->
            ( Loading data, Effect.none )

        UserPressedCopyIri _ ->
            ( Loading data, Effect.none )

        UserPressedEditDocument ->
            ( Loading data, Effect.none )

        UserPressedUploadNewVersion ->
            ( Loading data, Effect.none )

        BrowserLoadedGraphFile _ ->
            ( Loading data, Effect.none )

        UserSelectedFormat _ ->
            ( Loading data, Effect.none )

        UserPressedCancelInDialogImportGraph ->
            ( Loading data, Effect.none )

        UserPressedImportInDialogImportGraph ->
            ( Loading data, Effect.none )

        ApiStartedEditOfDocument _ ->
            ( Loading data, Effect.none )

        UserCheckedVisibleToEveryone _ ->
            ( Loading data, Effect.none )

        ApiUpdatedDatasetAccess _ ->
            ( Loading data, Effect.none )

        CacheStoredDatasetAccess _ ->
            ( Loading data, Effect.none )


initLoadedWithoutOntology :
    C
    -> DataLoading
    -> Herbie.Document
    -> Herbie.DocumentVersion
    -> Loader.Latest
    -> Rdf.Graph
    -> Rdf.Graph
    -> Rdf.Graph
    -> Rdf.Graph
    -> ( Model, Effect Msg )
initLoadedWithoutOntology c data document version latest graphVersion graphAccess graphEntered graphGenerated =
    let
        ( formPreview, effectFormPreview, _ ) =
            Form.initPreview document.herbie.workspaceUuid version.this graphVersion
    in
    ( Loaded
        { tabVisible = data.tabVisible
        , formatVisible = data.formatVisible
        , metaVisible = data.metaVisible
        , instancesListConfig = InstancesList.default
        , instancesExpanded = data.instancesExpanded

        -- DOCUMENT
        , uuid = data.uuid
        , index = data.index
        , document = document
        , latest = latest
        , version = version
        , graph = graphVersion
        , graphCombined = Rdf.union graphEntered graphGenerated
        , access = Herbie.accessFromGraph document graphAccess
        , turtle = ""
        , nTriples = ""
        , jsonLd = ""
        , conformsTo = Nothing
        , form = Nothing
        , formPreview = formPreview

        -- DIALOGS
        , dialogImportGraph = Nothing

        -- DATA
        , creatingDraft = False
        }
    , [ getDocumentLatest version.this
      , Effect.map MsgFormPreview (Loader.effectToEffect c data.uuid effectFormPreview)
      , Effect.fromAction (Action.SelectWorkspace document.herbie.workspaceUuid)
      ]
        |> Effect.batch
    )


initLoadedWithOntology :
    C
    -> DataLoading
    -> Herbie.Document
    -> Herbie.DocumentVersion
    -> Loader.Latest
    -> Rdf.Graph
    -> Rdf.Graph
    -> Rdf.Graph
    -> Rdf.Graph
    -> Form
    -> Rdf.Iri
    -> ( Model, Effect Msg )
initLoadedWithOntology c data document version latest graphVersion graphAccess graphEntered graphGenerated form conformsTo =
    let
        ( formPreview, effectFormPreview, _ ) =
            Form.initPreview document.herbie.workspaceUuid version.this graphVersion
    in
    ( Loaded
        { tabVisible = data.tabVisible
        , formatVisible = data.formatVisible
        , metaVisible = data.metaVisible
        , instancesListConfig = InstancesList.default
        , instancesExpanded = data.instancesExpanded

        -- DOCUMENT
        , uuid = data.uuid
        , index = data.index
        , document = document
        , latest = latest
        , version = version
        , graph = graphVersion
        , graphCombined = Rdf.union graphEntered graphGenerated
        , access = Herbie.accessFromGraph document graphAccess
        , turtle = ""
        , nTriples = ""
        , jsonLd = ""
        , conformsTo = Just conformsTo
        , form = Just form
        , formPreview = formPreview

        -- DIALOGS
        , dialogImportGraph = Nothing

        -- DATA
        , creatingDraft = False
        }
    , Effect.batch
        [ getDocumentLatest version.this
        , Effect.map MsgFormPreview (Loader.effectToEffect c data.uuid effectFormPreview)
        , Effect.fromAction (Action.SelectWorkspace document.herbie.workspaceUuid)
        ]
    )


initFailed : DataLoading -> Model
initFailed data =
    Failed
        { uuid = data.uuid
        , index = data.index
        , metaVisible = data.metaVisible
        , tabVisible = data.tabVisible
        , formatVisible = data.formatVisible
        , instancesExpanded = data.instancesExpanded
        }


updateFailed : C -> Msg -> DataFailed -> ( DataFailed, Effect Msg )
updateFailed _ msg data =
    case msg of
        _ ->
            ( data, Effect.none )


updateLoaded : C -> Browser.Navigation.Key -> Msg -> DataLoaded -> ( DataLoaded, Effect Msg )
updateLoaded c key msg ({ access, instancesListConfig } as data) =
    let
        populateDefaultValues =
            Herbie.populateDefaultValues data.document
    in
    case msg of
        NoOp ->
            ( data, Effect.none )

        -- LOADING
        MsgLoader _ ->
            ( data, Effect.none )

        -- LOADED
        CacheUpdatedDocument graph ->
            case Decode.decode (Decode.from data.document.this Herbie.decoderDocument) graph of
                Err _ ->
                    ( data, Effect.none )

                Ok _ ->
                    -- FIXME
                    ( data, Effect.none )

        ApiReturnedDocumentLatestTurtle turtle ->
            ( { data | turtle = turtle }
            , Effect.none
            )

        ApiReturnedDocumentLatestNTriples nTriples ->
            ( { data | nTriples = nTriples }
            , Effect.none
            )

        ApiReturnedDocumentLatestJsonLd jsonLd ->
            ( { data | jsonLd = jsonLd }
            , Effect.none
            )

        UserSelectedTab tab ->
            ( data
            , { uuid = data.uuid
              , index = String.fromInt data.index
              }
                |> Route.Datasets__Uuid___Versions__Index_
                |> Href.fromRouteWith c
                    (data
                        |> queryCurrent
                        |> (case tab of
                                TabCard ->
                                    Dict.remove "tab"

                                _ ->
                                    Dict.insert "tab" (tabAndFormatToString tab data.formatVisible)
                           )
                    )
                |> Browser.Navigation.replaceUrl key
                |> Effect.fromCmd
            )

        UserPressedDownload ->
            ( data
            , Api.requestIri c
                { method = "GET"
                , headers = [ Http.header "Accept" "application/trig" ]
                , iri = data.document.this
                , queryParameters = []
                , body = Http.emptyBody
                , expect = Api.expectString ApiReturnedDatasetForDownload
                , tracker = Nothing
                }
            )

        ApiReturnedDatasetForDownload (Err error) ->
            ( data
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiReturnedDatasetForDownload (Ok content) ->
            ( data
            , content
                |> File.Download.string (data.uuid ++ ".trig") "application/trig"
                |> Effect.fromCmd
            )

        UserPressedDelete ->
            ( data
            , Api.requestIri c
                { method = "DELETE"
                , headers = [ Http.header "Accept" "application/trig" ]
                , iri = data.document.this
                , queryParameters = []
                , body = Http.emptyBody
                , expect = Api.expectLocation ApiDeletedDataset
                , tracker = Nothing
                }
            )

        ApiDeletedDataset (Err error) ->
            ( data
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiDeletedDataset (Ok _) ->
            ( data
            , Route.Datasets
                |> Href.fromRoute c
                |> Browser.Navigation.pushUrl key
                |> Effect.fromCmd
            )

        UserPressedToggleMeta ->
            ( data
            , { uuid = data.uuid
              , index = String.fromInt data.index
              }
                |> Route.Datasets__Uuid___Versions__Index_
                |> Href.fromRouteWith c
                    (data
                        |> queryCurrent
                        |> (if data.metaVisible then
                                Dict.remove "meta"

                            else
                                Dict.insert "meta" ""
                           )
                    )
                |> Browser.Navigation.replaceUrl key
                |> Effect.fromCmd
            )

        UserSelectedFormatVisible format ->
            ( data
            , { uuid = data.uuid
              , index = String.fromInt data.index
              }
                |> Route.Datasets__Uuid___Versions__Index_
                |> Href.fromRouteWith c
                    (data
                        |> queryCurrent
                        |> Dict.insert "tab" (Format.toString format)
                    )
                |> Browser.Navigation.replaceUrl key
                |> Effect.fromCmd
            )

        UserPressedInstancesFilterAll ->
            ( { data | instancesListConfig = InstancesList.default }
            , Effect.none
            )

        UserPressedInstancesFilterOntology ->
            ( { data | instancesListConfig = InstancesList.toggleOntology instancesListConfig }
            , Effect.none
            )

        UserPressedInstancesFilterShacl ->
            ( { data | instancesListConfig = InstancesList.toggleShacl instancesListConfig }
            , Effect.none
            )

        UserPressedInstancesFilterOther ->
            ( { data | instancesListConfig = InstancesList.toggleOther instancesListConfig }
            , Effect.none
            )

        UserToggledExpansionOnInstances ->
            ( data
            , { uuid = data.uuid
              , index = String.fromInt data.index
              }
                |> Route.Datasets__Uuid___Versions__Index_
                |> Href.fromRouteWith c
                    (data
                        |> queryCurrent
                        |> (if data.instancesExpanded then
                                Dict.remove "instances"

                            else
                                Dict.insert "instances" ""
                           )
                    )
                |> Browser.Navigation.replaceUrl key
                |> Effect.fromCmd
            )

        -- FORM
        MsgForm msgForm ->
            case data.form of
                Nothing ->
                    ( data, Effect.none )

                Just form ->
                    let
                        ( formUpdated, effectForm, _ ) =
                            Form.update populateDefaultValues c.cShacl msgForm form
                    in
                    ( { data | form = Just formUpdated }
                    , Effect.map MsgForm (Loader.effectToEffect c data.uuid effectForm)
                    )

        MsgFormPreview msgForm ->
            let
                ( formUpdated, effectForm, _ ) =
                    Form.update populateDefaultValues c.cShacl msgForm data.formPreview
            in
            ( { data | formPreview = formUpdated }
            , Effect.map MsgFormPreview (Loader.effectToEffect c data.uuid effectForm)
            )

        -- STATEMENTS
        UserPressedCopyIri iri ->
            ( data
            , iri
                |> Blob.textUriList
                |> Ports.copyToClipboard
                |> Effect.fromCmd
            )

        -- ACTIONS
        UserPressedEditDocument ->
            ( { data | creatingDraft = True }
            , data.uuid
                |> Action.UserIntentsToEditDataset ApiStartedEditOfDocument
                |> Effect.fromAction
            )

        UserPressedUploadNewVersion ->
            ( data
            , File.Select.file
                [ "application/n-triples"
                , "text/turtle"
                , "application/ld+json"
                ]
                BrowserLoadedGraphFile
                |> Effect.fromCmd
            )

        BrowserLoadedGraphFile file ->
            ( { data
                | dialogImportGraph =
                    Just
                        { file = file
                        , format =
                            file
                                |> File.mime
                                |> Format.fromMime
                        }
              }
            , Effect.none
            )

        UserSelectedFormat format ->
            case data.dialogImportGraph of
                Nothing ->
                    ( data, Effect.none )

                Just dialogImportGraph ->
                    ( { data | dialogImportGraph = Just { dialogImportGraph | format = Just format } }
                    , Effect.none
                    )

        UserPressedCancelInDialogImportGraph ->
            ( { data | dialogImportGraph = Nothing }
            , Effect.none
            )

        UserPressedImportInDialogImportGraph ->
            case data.dialogImportGraph of
                Nothing ->
                    ( data, Effect.none )

                Just dialogImportGraph ->
                    case dialogImportGraph.format of
                        Nothing ->
                            -- FIXME show validation error
                            ( data, Effect.none )

                        Just format ->
                            ( { data
                                | dialogImportGraph = Nothing
                                , creatingDraft = True
                              }
                            , Action.UserIntentsToEditDatasetWith ApiStartedEditOfDocument data.uuid dialogImportGraph.file format
                                |> Effect.fromAction
                            )

        ApiStartedEditOfDocument (Err error) ->
            ( { data | creatingDraft = False }
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiStartedEditOfDocument (Ok ()) ->
            ( data
            , { uuid = data.uuid }
                |> Route.Datasets__Uuid___Draft
                |> Href.fromRouteWith c (queryCurrent data)
                |> Browser.Navigation.pushUrl key
                |> Effect.fromCmd
            )

        UserCheckedVisibleToEveryone checked ->
            let
                accessUpdated =
                    { access | visibleToEveryone = checked }
            in
            ( { data | access = accessUpdated }
            , accessUpdated
                |> Herbie.accessToGraph data.document
                |> putGraphAccess c data
            )

        ApiUpdatedDatasetAccess (Err error) ->
            ( data
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiUpdatedDatasetAccess (Ok raw) ->
            ( data
            , Action.StoreTrig CacheStoredDatasetAccess (iriAccess data.document.this) raw
                |> Effect.fromAction
            )

        CacheStoredDatasetAccess (Err error) ->
            ( data
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        CacheStoredDatasetAccess (Ok graphs) ->
            case DictIri.get (iriAccess data.document.this) graphs of
                Nothing ->
                    ( data, Effect.none )

                Just graphAccess ->
                    ( { data | access = Herbie.accessFromGraph data.document graphAccess }
                    , Effect.none
                    )


putGraphAccess : C -> DataLoaded -> Graph -> Effect Msg
putGraphAccess c data graph =
    Api.request c
        { method = "PUT"
        , headers = [ Http.header "Accept" "application/trig" ]
        , pathSegments = [ "datasets", data.uuid, "access" ]
        , queryParameters = []
        , body =
            graph
                |> Rdf.serialize
                |> Http.stringBody "application/n-triples"
        , expect = Api.expectString ApiUpdatedDatasetAccess
        , tracker = Nothing
        }


iriAccess : Iri -> Iri
iriAccess iri =
    Rdf.iri (Rdf.toUrl iri ++ "access/")


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        Loading _ ->
            Sub.none

        Failed _ ->
            Sub.none

        Loaded data ->
            data.form
                |> Maybe.map (Form.subscriptions >> Sub.map MsgForm)
                |> Maybe.withDefault Sub.none


events : C -> Model -> Event Msg
events _ model =
    case model of
        Loading _ ->
            Event.none

        Failed _ ->
            Event.none

        Loaded data ->
            data.document.this
                |> Event.GraphUpdated CacheUpdatedDocument
                |> Event.onCacheChange


getDocumentLatest : Iri -> Effect Msg
getDocumentLatest iri =
    [ Action.GetGraphTurtle ApiReturnedDocumentLatestTurtle iri
        |> Effect.fromAction
    , Action.GetGraphNTriples ApiReturnedDocumentLatestNTriples iri
        |> Effect.fromAction
    , Action.GetGraphJsonLd ApiReturnedDocumentLatestJsonLd iri
        |> Effect.fromAction
    ]
        |> Effect.batch


element : C -> Model -> Element Msg
element c model =
    case model of
        Loading data ->
            data.loader
                |> Loader.toLoading
                |> viewLoading c
                |> pane
                |> Pane.toElement c

        Failed _ ->
            viewFailed
                |> pane
                |> Pane.toElement c

        Loaded data ->
            let
                maximize =
                    case data.tabVisible of
                        TabCard ->
                            identity

                        TabGraph ->
                            Pane.maximize

                withSecondary =
                    if data.metaVisible then
                        data
                            |> viewMeta c
                            |> Pane.withSecondary

                    else
                        identity
            in
            viewLoaded c data
                |> pane
                |> maximize
                |> withSecondary
                |> Pane.toElement c


loadingForm : DataLoaded -> Bool
loadingForm data =
    case data.latest of
        Loader.LatestVersion latest ->
            Maybe.unwrap False Form.loading latest.form || Maybe.unwrap False Form.loading data.form

        Loader.LatestDraft latest ->
            Maybe.unwrap False Form.loading latest.form || Maybe.unwrap False Form.loading data.form


viewLoading : C -> Loading -> Element Msg
viewLoading c graphsLoading =
    [ { label = verbatim "Loading graphs"
      , loading = graphsLoading
      }
    ]
        |> Loading.viewMany c
        |> el
            [ width fill
            , height fill
            , padding 16
            , Border.rounded 12
            ]


viewFailed : Element Msg
viewFailed =
    none


viewLoaded : C -> DataLoaded -> Element Msg
viewLoaded c data =
    column
        [ width fill
        , height fill
        , spacing 16
        ]
        [ viewDraftInfo c data
        , viewCurrentVersionInfo c data
        , viewHeading c data
        , viewBody c data
        ]


viewHeading : C -> DataLoaded -> Element Msg
viewHeading c data =
    [ [ [ case data.version.rdfs.label of
            Nothing ->
                "Untitled graph"
                    |> Element.text
                    |> el
                        [ Font.size 24
                        , Font.medium
                        , Font.italic
                        ]
                    |> List.singleton
                    |> paragraph
                        [ width fill
                        , paddingXY 16 0
                        ]

            Just label ->
                label
                    |> Localize.forLabel c
                    |> Element.text
                    |> el
                        [ Font.size 24
                        , Font.medium
                        ]
                    |> List.singleton
                    |> paragraph
                        [ width fill
                        , paddingXY 16 0
                        ]
        , [ data.version.this
                |> Rdf.toUrl
                |> verbatim
                |> body2
          ]
            |> paragraph
                [ width fill
                , paddingXY 16 0
                ]
        ]
            |> column
                [ width fill
                , spacing 4
                , paddingXY 0 8
                ]
      , [ case data.tabVisible of
            TabCard ->
                "Raw"
                    |> verbatim
                    |> chipSuggestion (UserSelectedTab TabGraph)
                    |> ChipSuggestion.toElement

            TabGraph ->
                "Card"
                    |> verbatim
                    |> chipSuggestion (UserSelectedTab TabCard)
                    |> ChipSuggestion.toElement
        , Icon.FileDownload
            |> buttonIcon UserPressedDownload
            |> ButtonIcon.toElement
        , if Maybe.unwrap False (Datasets.canDelete c.author) c.workspace then
            Icon.Delete
                |> buttonIcon UserPressedDelete
                |> ButtonIcon.toElement

          else
            none
        , Icon.Settings
            |> buttonIcon UserPressedToggleMeta
            |> ButtonIcon.toElement
        ]
            |> row
                [ spacing 4
                , alignRight
                , alignTop
                ]
      ]
        |> row
            [ width fill ]
    , case data.version.rdfs.comment of
        Nothing ->
            none

        Just comment ->
            comment
                |> Localize.verbatim c
                |> verbatim
                |> body1
                |> List.singleton
                |> paragraph
                    [ width fill
                    , paddingXY 16 0
                    ]
    ]
        |> column
            [ width fill
            , spacing 8
            ]


viewBody : C -> DataLoaded -> Element Msg
viewBody c data =
    if data.tabVisible == TabGraph then
        viewGraph data

    else
        [ if data.version.terms.conformsTo == Nothing then
            none

          else
            viewVersion c data
        , viewPreview c data.tabVisible data.graph data.formPreview
        , viewInstances c data.instancesListConfig data.instancesExpanded data.graphCombined
        , viewActionsOntology c data
        ]
            |> column
                [ width fill
                , height fill
                , spacing 16
                ]


viewPreview : C -> Tab -> Graph -> Form -> Element Msg
viewPreview =
    lazy4
        (\c tabVisible graph formPreview ->
            let
                containsShaclShapes =
                    graph
                        |> Shacl.fromGraph
                        |> Result.withDefault Shacl.empty
                        |> Shacl.nodeShapesWithTargetClasses
                        |> Result.isOk
            in
            if containsShaclShapes then
                case tabVisible of
                    TabCard ->
                        FormPreview.view c formPreview
                            |> Element.map MsgFormPreview

                    TabGraph ->
                        none

            else
                none
        )


viewInstances : C -> InstancesList.Config -> Bool -> Rdf.Graph -> Element Msg
viewInstances =
    lazy4
        (\c instancesListConfig instancesExpanded graphCombined ->
            Instances.view c
                (instancesConfig instancesExpanded)
                instancesListConfig
                graphCombined
        )


instancesConfig : Bool -> Instances.Config Msg
instancesConfig expanded =
    { userPressedInstancesFilterAll = UserPressedInstancesFilterAll
    , userPressedInstancesFilterOntology = UserPressedInstancesFilterOntology
    , userPressedInstancesFilterShacl = UserPressedInstancesFilterShacl
    , userPressedInstancesFilterOther = UserPressedInstancesFilterOther
    , userPressedCopyIri = UserPressedCopyIri
    , userToggledExpansion = UserToggledExpansionOnInstances
    , expanded = expanded
    }


viewCurrentVersionInfo : C -> DataLoaded -> Element Msg
viewCurrentVersionInfo c data =
    if isCurrent data then
        none

    else
        let
            url =
                { uuid = data.uuid
                , index = String.fromInt (List.length data.document.versions)
                }
                    |> Route.Datasets__Uuid___Versions__Index_
                    |> Href.fromRouteWith c (queryCurrent data)
        in
        verbatim "This is a previous version of the document."
            |> banner
            |> Banner.withFormat Banner.Info
            |> Banner.withButton
                (t "action--open-current-version"
                    |> buttonLink url
                    |> Button.withLeadingIcon Icon.Edit
                )
            |> Banner.toElement


viewDraftInfo : C -> DataLoaded -> Element Msg
viewDraftInfo c data =
    let
        draftExists =
            case data.document.draft of
                Nothing ->
                    False

                Just _ ->
                    True
    in
    if draftExists then
        let
            url =
                { uuid = data.uuid }
                    |> Route.Datasets__Uuid___Draft
                    |> Href.fromRouteWith c (queryCurrent data)
        in
        verbatim "This document is currently being edited."
            |> banner
            |> Banner.withFormat Banner.Primary
            |> Banner.withButton
                (t "action--open-draft"
                    |> buttonLink url
                    |> Button.withLeadingIcon Icon.Edit
                )
            |> Banner.toElement

    else
        none


viewVersion : C -> DataLoaded -> Element Msg
viewVersion c data =
    [ [ Icon.viewNormal Icon.Assignment
      , viewTitle c data
            |> el
                [ width fill
                , centerY
                ]
      ]
        |> row
            [ width fill
            , paddingXY 16 0
            , height (px 64)
            , spacing 16
            , Background.color surfaceContainer
            , Border.roundEach
                { topLeft = 12
                , topRight = 12
                , bottomLeft = 0
                , bottomRight = 0
                }
            ]
    , data.form
        |> Maybe.map
            (viewCard c
                >> el
                    [ width fill
                    , height fill
                    ]
            )
        |> Maybe.withDefault none
    ]
        |> column
            [ width fill
            , Border.width 1
            , Border.color outlineVariant
            , Border.rounded 12
            ]


viewTitle : C -> DataLoaded -> Element msg
viewTitle c data =
    let
        title =
            case data.latest of
                Loader.LatestVersion { version, conformsTo, form } ->
                    Maybe.andThen (titleHelp version.this version.terms.source conformsTo) form
                        |> Maybe.withDefault "Generic graph"

                Loader.LatestDraft { draft, conformsTo, form } ->
                    Maybe.andThen (titleHelp draft.this draft.terms.source conformsTo) form
                        |> Maybe.withDefault "Generic graph"

        titleHelp iri dctermsSource conformsTo form =
            if conformsTo == Nothing then
                data.graph
                    |> ontologyMeta iri dctermsSource
                    |> Maybe.andThen (.rdfs >> .label)
                    |> Maybe.map (Localize.forLabel c)

            else
                Just (Form.heading c.cShacl form)

        ontologyMeta iri dctermsSource graph =
            Ontology.metaFromGraph iri graph
                :: List.map (\iriSource -> Ontology.metaFromGraph iriSource graph) dctermsSource
                |> Maybe.orList
    in
    title
        |> verbatim
        |> body1
        |> List.singleton
        |> paragraph [ width fill ]


viewMeta : C -> DataLoaded -> Element Msg
viewMeta =
    lazy2
        (\c data ->
            [ [ "Meta"
                    |> verbatim
                    |> body1Strong
              , Icon.Close
                    |> buttonIcon UserPressedToggleMeta
                    |> ButtonIcon.toElement
                    |> el [ alignRight ]
              ]
                |> row
                    [ width fill
                    , spacing 16
                    , paddingEach
                        { top = 8
                        , bottom = 8
                        , left = 16
                        , right = 8
                        }
                    ]
            , [ case data.conformsTo of
                    Nothing ->
                        none

                    Just conformsTo ->
                        { url = Rdf.toUrl conformsTo
                        , label =
                            conformsTo
                                |> Rdf.toUrl
                                |> verbatim
                        }
                            |> Labelled.link (t "meta--conforms-to--label")
                            |> el [ paddingXY 16 0 ]
              , viewAliases data
                    |> el [ paddingXY 16 0 ]
              , data.version.createdAt
                    |> Labelled.posixWithDistance c (t "meta--created-at--label")
                    |> el [ paddingXY 16 0 ]
              , data.version.publishedAt
                    |> Labelled.posixWithDistance c (t "meta--published-at--label")
                    |> el [ paddingXY 16 0 ]
              , data.version.createdBy.rdfs.label
                    |> Localize.verbatim c
                    |> Labelled.string (t "meta--author--label")
                    |> el [ paddingXY 16 0 ]
              , data.version.this
                    |> Rdf.toUrl
                    |> Labelled.string (t "meta--iri--label")
                    |> el [ paddingXY 16 0 ]
              , if List.isEmpty data.version.schema.hasPart then
                    none

                else
                    data.version.schema.hasPart
                        |> List.map
                            (\hasPart ->
                                { url = Rdf.toUrl hasPart.this
                                , label =
                                    case hasPart.rdfs.label of
                                        Nothing ->
                                            hasPart.this
                                                |> Rdf.toUrl
                                                |> verbatim

                                        Just label ->
                                            label
                                                |> Localize.forLabel c
                                                |> verbatim
                                }
                            )
                        |> Labelled.linkList (verbatim "Has part")
                        |> el [ paddingXY 16 0 ]
              , if Maybe.unwrap False (Datasets.canUpdate c c.author data.document) c.workspace then
                    data.access.visibleToEveryone
                        |> checkbox UserCheckedVisibleToEveryone (verbatim "Visible to everyone")
                        |> Checkbox.toElement

                else
                    none
              , DocumentVersions.view c
                    { uuid = data.uuid
                    , document = data.document
                    , active =
                        if isCurrent data then
                            DocumentVersions.VersionCurrent

                        else
                            DocumentVersions.VersionPrevious data.index
                    , query = queryCurrent data
                    }
              ]
                |> column
                    [ width fill
                    , spacing 8
                    ]
                |> el
                    [ width fill
                    , scrollbarY
                    , style "flex-basis" "unset"
                    ]
                |> el
                    [ width fill
                    , paddingEach
                        { top = 0
                        , bottom = 16
                        , left = 0
                        , right = 0
                        }
                    , style "min-height" "0"
                    , style "flex-basis" "unset"
                    , style "flex-shrink" "unset"
                    ]
            ]
                |> column
                    [ width fill
                    , style "min-height" "0"
                    , style "flex-shrink" "unset"
                    ]
        )


viewAliases : DataLoaded -> Element Msg
viewAliases data =
    if List.isEmpty data.version.terms.source then
        none

    else
        data.version.terms.source
            |> List.map
                (\dctermsSource ->
                    { url = Rdf.toUrl dctermsSource
                    , label = verbatim (Rdf.toUrl dctermsSource)
                    }
                )
            |> Labelled.linkList (t "meta--aliases--label")


viewCard : C -> Form -> Element Msg
viewCard c form =
    Form.card c.cShacl form
        |> Element.map MsgForm
        |> el
            [ width fill
            , height fill
            , padding 16
            ]


viewGraph : DataLoaded -> Element Msg
viewGraph data =
    GraphSerializations.view
        { userSelectedFormatVisible = UserSelectedFormatVisible }
        { turtle = data.turtle
        , nTriples = data.nTriples
        , jsonLd = data.jsonLd
        , formatVisible = data.formatVisible
        }


viewActionsOntology : C -> DataLoaded -> Element Msg
viewActionsOntology c data =
    if Maybe.unwrap False (Datasets.canEdit c c.author data.document) c.workspace then
        [ t "action--edit-dataset"
            |> button UserPressedEditDocument
            |> Button.withLeadingIcon Icon.Edit
            |> Button.withFormat Button.Contained
            |> Button.view
        , t "action--upload-new-version"
            |> button UserPressedUploadNewVersion
            |> Button.withLeadingIcon Icon.FileUpload
            |> Button.withFormat Button.Outlined
            |> Button.view
        ]
            |> row
                [ paddingXY 16 0
                , spacing 16
                ]

    else
        none


overlays : C -> Model -> List (Element Msg)
overlays _ model =
    case model of
        Loading _ ->
            []

        Failed _ ->
            []

        Loaded data ->
            [ Maybe.map viewDialogImportGraph data.dialogImportGraph
            ]
                |> List.filterMap identity


viewDialogImportGraph : DialogImportGraph -> Element Msg
viewDialogImportGraph data =
    [ column
        [ width fill ]
        [ Input.label
            { id = Nothing
            , text = t "input--file--label"
            , help = []
            }
        , row
            [ width fill
            , spacing 8
            , paddingEach
                { top = 8
                , bottom = 8
                , left = 16
                , right = 8
                }
            ]
            [ data.file
                |> File.name
                |> verbatim
                |> body1Mono
                |> el [ width fill ]
            ]
        ]
    , Radio.view
        (Radio.radio
            UserSelectedFormat
            (t "input--file-format--label")
            data.format
            [ Radio.option Format.Turtle (t "input--file-format--option--turtle")
            , Radio.option Format.NTriples (t "input--file-format--option--n-triples")
            , Radio.option Format.JsonLd (t "input--file-format--option--json-ld")
            ]
        )
    ]
        |> column
            [ width fill
            , spacing 16
            ]
        |> Dialog.desktop (t "heading--import-graph")
        |> Dialog.withActions
            [ t "action--import"
                |> button UserPressedImportInDialogImportGraph
                |> Button.view
                |> el [ paddingXY 8 0 ]
            , t "action--cancel"
                |> button UserPressedCancelInDialogImportGraph
                |> Button.withFormat Button.Text
                |> Button.view
                |> el [ alignRight ]
            ]
        |> Dialog.withScrimEvents
            { userPressedScrim = UserPressedCancelInDialogImportGraph
            , noOp = NoOp
            }
        |> Dialog.toElement


breadcrumbs : C -> Model -> Breadcrumbs
breadcrumbs c model =
    Breadcrumbs.from
        (List.filterMap identity
            [ case c.workspace of
                Nothing ->
                    Nothing

                Just workspace ->
                    { url = Href.fromRoute c (Route.Workspaces__Uuid_ { uuid = workspace.uuid })
                    , label = verbatim workspace.name
                    }
                        |> Breadcrumbs.link
                        |> Just
            , { url = Href.fromRoute c Route.Datasets
              , label = verbatim "Documents"
              }
                |> Breadcrumbs.link
                |> Just
            , { url =
                    { uuid = uuid model }
                        |> Route.Datasets__Uuid_
                        |> Href.fromRouteWith c (queryCurrentFromModel model)
              , label = verbatim (uuid model)
              }
                |> Breadcrumbs.link
                |> Just
            ]
        )
        (case model of
            Loading _ ->
                verbatim ("#" ++ String.fromInt (index model))

            Failed _ ->
                verbatim ("#" ++ String.fromInt (index model))

            Loaded data ->
                if isCurrent data then
                    verbatim ("Current version (#" ++ String.fromInt (index model) ++ ")")

                else
                    verbatim ("Previous version (#" ++ String.fromInt (index model) ++ ")")
        )


isCurrent : DataLoaded -> Bool
isCurrent data =
    Just data.version.this
        == (data.document.versions
                |> List.head
                |> Maybe.map .this
           )


queryCurrentFromModel : Model -> Dict String String
queryCurrentFromModel model =
    case model of
        Loading data ->
            queryCurrent data

        Failed data ->
            queryCurrent data

        Loaded data ->
            queryCurrent data


queryCurrent :
    { rest
        | tabVisible : Tab
        , formatVisible : Format
        , metaVisible : Bool
        , instancesExpanded : Bool
    }
    -> Dict String String
queryCurrent data =
    [ case data.tabVisible of
        TabCard ->
            Nothing

        _ ->
            Just ( "tab", tabAndFormatToString data.tabVisible data.formatVisible )
    , if data.metaVisible then
        Just ( "meta", "" )

      else
        Nothing
    , if data.instancesExpanded then
        Just ( "instances", "" )

      else
        Nothing
    ]
        |> List.filterMap identity
        |> Dict.fromList


t : String -> Fluent
t key =
    Fluent.text ("pages-datasets-uuid-versions-index--" ++ key)
