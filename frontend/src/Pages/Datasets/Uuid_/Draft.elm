module Pages.Datasets.Uuid_.Draft exposing (Model, Msg, Params, events, page)

import Accessibility.Blob as Blob
import Action
import Api
import Api.Datasets as Datasets
import Browser.Dom
import Browser.Navigation
import Context exposing (C)
import Dict exposing (Dict)
import Effect exposing (Effect)
import Element
    exposing
        ( Element
        , alignRight
        , alignTop
        , centerX
        , centerY
        , column
        , el
        , fill
        , height
        , html
        , maximum
        , none
        , padding
        , paddingEach
        , paddingXY
        , paragraph
        , px
        , row
        , scrollbarY
        , spacing
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Extra as Element exposing (style)
import Element.Font as Font
import Event exposing (Event)
import File.Download
import Fluent exposing (Fluent, verbatim)
import Gen.Route as Route
import Href
import Html
import Html.Attributes
import Http
import Maybe.Extra as Maybe
import Maybe.Pipeline as Maybe
import Ontology.Herbie as Herbie
import Page
import Ports
import Rdf exposing (Iri)
import Rdf.Decode as Decode
import Rdf.DictIri as DictIri exposing (DictIri)
import Rdf.Format as Format exposing (Format)
import Rdf.Graph as Rdf exposing (Graph)
import Request
import Result.Extra as Result
import Shacl
import Shacl.Extra as Shacl
import Shacl.Form.Loader as Loader exposing (Loader)
import Shacl.Form.Localize as Localize
import Shacl.Form.Viewed as Form
import Shacl.Report as Report exposing (Report)
import Shared
import Shared.Loading as Loading exposing (Loading)
import Synchronization exposing (Synchronization)
import Task
import Ui.Atom.Banner as Banner exposing (banner)
import Ui.Atom.Button as Button exposing (button, buttonLink)
import Ui.Atom.ButtonIcon as ButtonIcon exposing (buttonIcon)
import Ui.Atom.Checkbox as Checkbox exposing (checkbox)
import Ui.Atom.Chip as Chip
import Ui.Atom.ChipAssist as ChipAssist exposing (chipAssist)
import Ui.Atom.ChipSuggestion as ChipSuggestion exposing (chipSuggestion)
import Ui.Atom.Icon as Icon
import Ui.Atom.Labelled as Labelled
import Ui.Atom.Pane as Pane exposing (pane)
import Ui.Atom.TextInput as TextInput exposing (textInput)
import Ui.Molecule.Breadcrumbs as Breadcrumbs exposing (Breadcrumbs)
import Ui.Molecule.Dialog as Dialog
import Ui.Molecule.Listbox as Listbox
import Ui.Organism.DocumentVersions as DocumentVersions
import Ui.Organism.FormPreview as FormPreview
import Ui.Organism.GraphSerializations as GraphSerializations
import Ui.Organism.Instances as Instances
import Ui.Organism.InstancesList as InstancesList
import Ui.Theme.Color
    exposing
        ( error
        , onSurfaceVariant
        , outlineVariant
        , surface
        , surfaceContainer
        , surfaceContainerLow
        )
import Ui.Theme.Dimensions exposing (paneWidthMax)
import Ui.Theme.Typography
    exposing
        ( body1
        , body1Strong
        , body2
        , caption
        )
import View exposing (View)


page : Shared.Model -> Request.With Params -> Page.With Model Msg
page shared request =
    let
        c =
            Shared.toContext shared
    in
    Page.advanced
        { init = init c request
        , update = update c request.key
        , subscriptions = subscriptions
        , view = view c
        }


view : C -> Model -> View Msg
view c model =
    { title = "Draft"
    , element = element c model
    , overlays = overlays c model
    , breadcrumbs = breadcrumbs c model
    , fabVisible = True
    , navigationRailVisible = True
    , loading =
        case model of
            Loading _ ->
                True

            Failed _ ->
                False

            Loaded data ->
                data.deletingDraft
                    || data.generating
                    || data.publishRequested
    }


type Model
    = Loading DataLoading
    | Failed DataFailed
    | Loaded DataLoaded


type alias DataLoading =
    { uuid : String
    , iri : Iri
    , metaVisible : Bool
    , tabVisible : Tab
    , formatVisible : Format
    , instancesExpanded : Bool

    -- DATA
    , loader : Loader
    }


type alias DataFailed =
    { uuid : String
    , error : Error
    , metaVisible : Bool
    , tabVisible : Tab
    , formatVisible : Format
    , instancesExpanded : Bool
    }


type Error
    = ErrorUnspecified
    | CouldNotLoadForm Loader.Error


errorToString : C -> Error -> String
errorToString c error =
    case error of
        ErrorUnspecified ->
            "I ran into an unspecified error."

        CouldNotLoadForm errorLoader ->
            "Could not load form: " ++ Loader.errorToString c errorLoader ++ "."


apiErrorToString : C -> Api.Error -> String
apiErrorToString c error =
    error
        |> Api.errorToMessages
        |> Localize.verbatims c


type alias DataLoaded =
    { tabVisible : Tab
    , formatVisible : Format
    , deletingDraft : Bool
    , generating : Bool
    , metaVisible : Bool
    , instancesListConfig : InstancesList.Config
    , instancesExpanded : Bool

    -- DOCUMENT
    , uuid : String
    , document : Herbie.Document

    -- DRAFT
    , draft : Herbie.DocumentDraft
    , graph : Graph
    , graphEntered : Graph
    , graphGenerated : Graph
    , access : Herbie.Access
    , turtle : String
    , nTriples : String
    , jsonLd : String
    , form : Form
    , synchronization : Synchronization
    , formPreview : Form.Form
    , publishRequested : Bool

    -- DIALOGS
    , dialogAddAlias : Maybe DialogAddAlias
    }


type Form
    = FormNotRequired
    | FormLoaded
        { conformsTo : Iri
        , form : Form.Form
        , report : Report
        }


type Tab
    = TabForm
    | TabCard
    | TabGraph


tabAndFormatToString : Tab -> Format -> String
tabAndFormatToString tab format =
    case tab of
        TabForm ->
            "form"

        TabCard ->
            "card"

        TabGraph ->
            Format.toString format


tabAndFormatFromString : String -> Maybe ( Tab, Format )
tabAndFormatFromString raw =
    case raw of
        "form" ->
            Just ( TabForm, Format.Turtle )

        "card" ->
            Just ( TabCard, Format.Turtle )

        _ ->
            raw
                |> Format.fromString
                |> Maybe.map (Tuple.pair TabGraph)


uuid : Model -> String
uuid model =
    case model of
        Loading data ->
            data.uuid

        Failed data ->
            data.uuid

        Loaded data ->
            data.uuid


type alias DialogAddAlias =
    { iri : String
    }


type alias Params =
    { uuid : String
    }


init : C -> Request.With Params -> ( Model, Effect Msg )
init c { query, params } =
    let
        iri =
            Api.iriHttp c [ "datasets", params.uuid ]

        ( loader, effectLoader ) =
            Loader.loadDraft iri

        metaVisible =
            query
                |> Dict.get "meta"
                |> Maybe.map (\meta -> meta == "")
                |> Maybe.withDefault False

        instancesExpanded =
            query
                |> Dict.get "instances"
                |> Maybe.map (\instances -> instances == "")
                |> Maybe.withDefault False

        ( tabVisible, formatVisible ) =
            query
                |> Dict.get "tab"
                |> Maybe.andThen tabAndFormatFromString
                |> Maybe.withDefault ( TabForm, Format.Turtle )
    in
    ( Loading
        { uuid = params.uuid
        , iri = iri
        , metaVisible = metaVisible
        , tabVisible = tabVisible
        , formatVisible = formatVisible
        , instancesExpanded = instancesExpanded
        , loader = loader
        }
    , Effect.map MsgLoader effectLoader
    )


type Msg
    = NoOp
      -- LOADING
    | MsgLoader Loader.Msg
      -- LOADED
    | CacheUpdatedDocument Graph
    | ApiReturnedDocumentLatestTurtle String
    | ApiReturnedDocumentLatestNTriples String
    | ApiReturnedDocumentLatestJsonLd String
    | UserSelectedTab Tab
    | UserPressedDownload
    | ApiReturnedDatasetForDownload (Api.Response String)
    | UserPressedDelete
    | ApiDeletedDataset (Api.Response String)
    | UserPressedToggleMeta
    | UserPressedDeleteAlias Iri
    | ApiDeletedAlias (Api.Response ())
    | UserPressedAddAlias
    | UserChangedIri String
    | UserPressedCancelInDialogAddAlias
    | UserPressedAddInDialogAddAlias
    | ApiAddedAlias (Api.Response ())
    | UserPressedSubmit
    | ApiPublishedDraft (Api.Response ())
    | UserPressedDeleteDraft
    | ApiCancelledDraft (Api.Response ())
    | UserPressedGenerate
    | ApiUpdatedDatasetAfterGenerate (Api.Response String)
    | CacheStoredDatasetAfterGenerate (Api.Response (DictIri Graph))
    | UserSelectedFormatVisible Format
    | UserPressedInstancesFilterAll
    | UserPressedInstancesFilterOntology
    | UserPressedInstancesFilterShacl
    | UserPressedInstancesFilterOther
    | UserToggledExpansionOnInstances
      -- FORM
    | MsgForm Form.Msg
    | MsgSynchronization Synchronization.Msg
    | ApiUpdatedDataset (Api.Response String)
    | CacheStoredDataset (Api.Response (DictIri Graph))
    | MsgFormPreview Form.Msg
      -- STATEMENTS
    | UserPressedCopyIri Iri
    | UserCheckedVisibleToEveryone Bool
    | ApiUpdatedDatasetAccess (Api.Response String)
    | CacheStoredDatasetAccess (Api.Response (DictIri Graph))


update : C -> Browser.Navigation.Key -> Msg -> Model -> ( Model, Effect Msg )
update c key msg model =
    case model of
        Loading data ->
            updateLoading c msg data

        Failed data ->
            updateFailed c msg data
                |> Tuple.mapFirst Failed

        Loaded data ->
            updateLoaded c key msg data
                |> Tuple.mapFirst Loaded


updateLoading : C -> Msg -> DataLoading -> ( Model, Effect Msg )
updateLoading c msg data =
    case msg of
        NoOp ->
            ( Loading data, Effect.none )

        -- LOADING
        MsgLoader msgLoader ->
            case Loader.update c msgLoader data.loader of
                Loader.Running loaderUpdated effectLoader ->
                    ( Loading { data | loader = loaderUpdated }
                    , Effect.map MsgLoader effectLoader
                    )

                Loader.SuccessDraft stuff ->
                    Just initLoadedWithoutOntology
                        |> Maybe.hardcoded c
                        |> Maybe.hardcoded data
                        |> Maybe.hardcoded stuff.document
                        |> Maybe.required stuff.document.draft
                        |> Maybe.hardcoded stuff.graphDraft
                        |> Maybe.hardcoded stuff.graphAccess
                        |> Maybe.hardcoded stuff.graphEntered
                        |> Maybe.hardcoded stuff.graphGenerated
                        |> Maybe.withDefault ( initFailed data ErrorUnspecified, Effect.none )

                Loader.SuccessDraftWithForm stuff ->
                    Just initLoadedWithOntology
                        |> Maybe.hardcoded c
                        |> Maybe.hardcoded data
                        |> Maybe.hardcoded stuff.document
                        |> Maybe.required stuff.document.draft
                        |> Maybe.hardcoded stuff.graphDraft
                        |> Maybe.hardcoded stuff.graphAccess
                        |> Maybe.hardcoded stuff.graphEntered
                        |> Maybe.hardcoded stuff.graphGenerated
                        |> Maybe.hardcoded stuff.form
                        |> Maybe.hardcoded stuff.effect
                        |> Maybe.hardcoded stuff.outMsg
                        |> Maybe.hardcoded stuff.conformsTo
                        |> Maybe.withDefault
                            ( initFailed data ErrorUnspecified
                            , Effect.map MsgForm stuff.effect
                            )

                Loader.SuccessVersion _ ->
                    ( Loading data, Effect.none )

                Loader.SuccessVersionWithForm _ ->
                    ( Loading data, Effect.none )

                Loader.Failed error ->
                    ( Failed
                        { uuid = data.uuid
                        , error = CouldNotLoadForm error
                        , metaVisible = data.metaVisible
                        , tabVisible = data.tabVisible
                        , formatVisible = data.formatVisible
                        , instancesExpanded = data.instancesExpanded
                        }
                    , Effect.none
                    )

        -- LOADED
        CacheUpdatedDocument _ ->
            ( Loading data, Effect.none )

        ApiReturnedDocumentLatestTurtle _ ->
            ( Loading data, Effect.none )

        ApiReturnedDocumentLatestNTriples _ ->
            ( Loading data, Effect.none )

        ApiReturnedDocumentLatestJsonLd _ ->
            ( Loading data, Effect.none )

        UserSelectedTab _ ->
            ( Loading data, Effect.none )

        UserPressedDownload ->
            ( Loading data, Effect.none )

        ApiReturnedDatasetForDownload _ ->
            ( Loading data, Effect.none )

        UserPressedDelete ->
            ( Loading data, Effect.none )

        ApiDeletedDataset _ ->
            ( Loading data, Effect.none )

        UserPressedToggleMeta ->
            ( Loading data, Effect.none )

        UserPressedDeleteAlias _ ->
            ( Loading data, Effect.none )

        ApiDeletedAlias _ ->
            ( Loading data, Effect.none )

        UserPressedAddAlias ->
            ( Loading data, Effect.none )

        UserChangedIri _ ->
            ( Loading data, Effect.none )

        UserPressedCancelInDialogAddAlias ->
            ( Loading data, Effect.none )

        UserPressedAddInDialogAddAlias ->
            ( Loading data, Effect.none )

        ApiAddedAlias _ ->
            ( Loading data, Effect.none )

        UserPressedSubmit ->
            ( Loading data, Effect.none )

        ApiPublishedDraft _ ->
            ( Loading data, Effect.none )

        UserPressedDeleteDraft ->
            ( Loading data, Effect.none )

        ApiCancelledDraft _ ->
            ( Loading data, Effect.none )

        UserPressedGenerate ->
            ( Loading data, Effect.none )

        ApiUpdatedDatasetAfterGenerate _ ->
            ( Loading data, Effect.none )

        CacheStoredDatasetAfterGenerate _ ->
            ( Loading data, Effect.none )

        UserSelectedFormatVisible _ ->
            ( Loading data, Effect.none )

        UserPressedInstancesFilterAll ->
            ( Loading data, Effect.none )

        UserPressedInstancesFilterOntology ->
            ( Loading data, Effect.none )

        UserPressedInstancesFilterShacl ->
            ( Loading data, Effect.none )

        UserPressedInstancesFilterOther ->
            ( Loading data, Effect.none )

        UserToggledExpansionOnInstances ->
            ( Loading data, Effect.none )

        MsgForm _ ->
            ( Loading data, Effect.none )

        MsgSynchronization _ ->
            ( Loading data, Effect.none )

        ApiUpdatedDataset _ ->
            ( Loading data, Effect.none )

        CacheStoredDataset _ ->
            ( Loading data, Effect.none )

        MsgFormPreview _ ->
            ( Loading data, Effect.none )

        UserPressedCopyIri _ ->
            ( Loading data, Effect.none )

        UserCheckedVisibleToEveryone _ ->
            ( Loading data, Effect.none )

        ApiUpdatedDatasetAccess _ ->
            ( Loading data, Effect.none )

        CacheStoredDatasetAccess _ ->
            ( Loading data, Effect.none )


initLoadedWithoutOntology :
    C
    -> DataLoading
    -> Herbie.Document
    -> Herbie.DocumentDraft
    -> Graph
    -> Graph
    -> Graph
    -> Graph
    -> ( Model, Effect Msg )
initLoadedWithoutOntology c data document draft graph graphAccess graphEntered graphGenerated =
    let
        ( formPreview, effectFormPreview, _ ) =
            Form.initPreview document.herbie.workspaceUuid draft.this graph
    in
    ( Loaded
        { tabVisible = data.tabVisible
        , formatVisible = data.formatVisible
        , deletingDraft = False
        , generating = False
        , metaVisible = data.metaVisible
        , instancesListConfig = InstancesList.default
        , instancesExpanded = data.instancesExpanded

        -- DOCUMENT
        , uuid = data.uuid
        , document = document

        -- SELECTED
        , draft = draft
        , graph = graph
        , graphEntered = graphEntered
        , graphGenerated = graphGenerated
        , access = Herbie.accessFromGraph document graphAccess
        , turtle = ""
        , nTriples = ""
        , jsonLd = ""
        , form = FormNotRequired -- FIXME We should add a separat constructor
        , synchronization = Synchronization.synced
        , formPreview = formPreview
        , publishRequested = False

        -- DIALOGS
        , dialogAddAlias = Nothing
        }
    , [ getGraphRaw draft
      , Effect.map MsgFormPreview
            (Loader.effectToEffect c document.herbie.workspaceUuid effectFormPreview)
      , Effect.fromAction (Action.SelectWorkspace document.herbie.workspaceUuid)
      ]
        |> Effect.batch
    )


initLoadedWithOntology :
    C
    -> DataLoading
    -> Herbie.Document
    -> Herbie.DocumentDraft
    -> Graph
    -> Graph
    -> Graph
    -> Graph
    -> Form.Form
    -> Effect Form.Msg
    -> Maybe Form.OutMsg
    -> Rdf.Iri
    -> ( Model, Effect Msg )
initLoadedWithOntology c data document draft graph graphAccess graphEntered graphGenerated form effect maybeOutMsg conformsTo =
    let
        ( formUpdated, effectForm ) =
            Form.loadResources form

        ( formPreview, effectFormPreview, _ ) =
            Form.initPreview document.herbie.workspaceUuid draft.this graph
    in
    ( Loaded
        { tabVisible = data.tabVisible
        , formatVisible = data.formatVisible
        , deletingDraft = False
        , generating = False
        , metaVisible = data.metaVisible
        , instancesListConfig = InstancesList.default
        , instancesExpanded = data.instancesExpanded

        -- DOCUMENT
        , uuid = data.uuid
        , document = document

        -- SELECTED
        , draft = draft
        , graph = graph
        , graphEntered = graphEntered
        , graphGenerated = graphGenerated
        , access = Herbie.accessFromGraph document graphAccess
        , turtle = ""
        , nTriples = ""
        , jsonLd = ""
        , form =
            FormLoaded
                { conformsTo = conformsTo
                , form = formUpdated
                , report = Report.empty
                }
        , synchronization =
            case maybeOutMsg of
                Nothing ->
                    Synchronization.synced

                Just Form.GraphChanged ->
                    Synchronization.changed Synchronization.synced
        , formPreview = formPreview
        , publishRequested = False

        -- DIALOGS
        , dialogAddAlias = Nothing
        }
    , [ getGraphRaw draft
      , Effect.map MsgForm
            (Loader.effectToEffect c document.herbie.workspaceUuid effectForm)
      , Effect.map MsgForm effect
      , Effect.map MsgFormPreview
            (Loader.effectToEffect c document.herbie.workspaceUuid effectFormPreview)
      , Effect.fromAction (Action.SelectWorkspace document.herbie.workspaceUuid)
      ]
        |> Effect.batch
    )


initFailed : DataLoading -> Error -> Model
initFailed data error =
    Failed
        { uuid = data.uuid
        , error = error
        , metaVisible = data.metaVisible
        , tabVisible = data.tabVisible
        , formatVisible = data.formatVisible
        , instancesExpanded = data.instancesExpanded
        }


updateFailed : C -> Msg -> DataFailed -> ( DataFailed, Effect Msg )
updateFailed _ msg data =
    case msg of
        _ ->
            ( data, Effect.none )


updateLoaded : C -> Browser.Navigation.Key -> Msg -> DataLoaded -> ( DataLoaded, Effect Msg )
updateLoaded c key msg ({ access, instancesListConfig } as data) =
    let
        populateDefaultValues =
            Herbie.populateDefaultValues data.document
    in
    case msg of
        NoOp ->
            ( data, Effect.none )

        -- LOADING
        MsgLoader _ ->
            ( data, Effect.none )

        -- LOADED
        CacheUpdatedDocument graph ->
            case Decode.decode (Decode.from data.document.this Herbie.decoderDocument) graph of
                Err _ ->
                    ( data, Effect.none )

                Ok document ->
                    case document.draft of
                        Nothing ->
                            ( data, Effect.none )

                        Just draft ->
                            ( { data
                                | document = document
                                , draft = draft
                              }
                            , getGraphRaw draft
                            )

        ApiReturnedDocumentLatestTurtle turtle ->
            ( { data | turtle = turtle }
            , Effect.none
            )

        ApiReturnedDocumentLatestNTriples nTriples ->
            ( { data | nTriples = nTriples }
            , Effect.none
            )

        ApiReturnedDocumentLatestJsonLd jsonLd ->
            ( { data | jsonLd = jsonLd }
            , Effect.none
            )

        UserSelectedTab tab ->
            ( data
            , { uuid = data.uuid }
                |> Route.Datasets__Uuid___Draft
                |> Href.fromRouteWith c
                    (data
                        |> queryCurrent
                        |> (case tab of
                                TabForm ->
                                    Dict.remove "tab"

                                _ ->
                                    Dict.insert "tab" (tabAndFormatToString tab data.formatVisible)
                           )
                    )
                |> Browser.Navigation.replaceUrl key
                |> Effect.fromCmd
            )

        UserPressedDownload ->
            ( data
            , Api.requestIri c
                { method = "GET"
                , headers = [ Http.header "Accept" "application/trig" ]
                , iri = data.document.this
                , queryParameters = []
                , body = Http.emptyBody
                , expect = Api.expectString ApiReturnedDatasetForDownload
                , tracker = Nothing
                }
            )

        ApiReturnedDatasetForDownload (Err error) ->
            ( data
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiReturnedDatasetForDownload (Ok content) ->
            ( data
            , content
                |> File.Download.string (data.uuid ++ ".trig") "application/trig"
                |> Effect.fromCmd
            )

        UserPressedDelete ->
            ( data
            , Api.requestIri c
                { method = "DELETE"
                , headers = [ Http.header "Accept" "application/trig" ]
                , iri = data.document.this
                , queryParameters = []
                , body = Http.emptyBody
                , expect = Api.expectLocation ApiDeletedDataset
                , tracker = Nothing
                }
            )

        ApiDeletedDataset (Err error) ->
            ( data
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiDeletedDataset (Ok _) ->
            ( data
            , Route.Datasets
                |> Href.fromRoute c
                |> Browser.Navigation.pushUrl key
                |> Effect.fromCmd
            )

        UserPressedToggleMeta ->
            ( data
            , { uuid = data.uuid }
                |> Route.Datasets__Uuid___Draft
                |> Href.fromRouteWith c
                    (data
                        |> queryCurrent
                        |> (if data.metaVisible then
                                Dict.remove "meta"

                            else
                                Dict.insert "meta" ""
                           )
                    )
                |> Browser.Navigation.replaceUrl key
                |> Effect.fromCmd
            )

        UserPressedDeleteAlias iriSource ->
            ( data
            , Action.UserIntentsToDeleteAlias ApiDeletedAlias data.uuid iriSource
                |> Effect.fromAction
            )

        ApiDeletedAlias (Err error) ->
            ( data
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiDeletedAlias (Ok _) ->
            ( data
            , Effect.none
            )

        UserPressedAddAlias ->
            ( { data | dialogAddAlias = Just { iri = "" } }
            , Effect.none
            )

        UserChangedIri iri ->
            case data.dialogAddAlias of
                Nothing ->
                    ( data, Effect.none )

                Just dataDialog ->
                    ( { data | dialogAddAlias = Just { dataDialog | iri = iri } }
                    , Effect.none
                    )

        UserPressedCancelInDialogAddAlias ->
            ( { data | dialogAddAlias = Nothing }
            , Effect.none
            )

        UserPressedAddInDialogAddAlias ->
            case data.dialogAddAlias of
                Nothing ->
                    ( data, Effect.none )

                Just { iri } ->
                    ( data
                    , Action.UserIntentsToAddAlias ApiAddedAlias data.uuid (Rdf.iri iri)
                        |> Effect.fromAction
                    )

        ApiAddedAlias (Err error) ->
            ( { data | dialogAddAlias = Nothing }
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiAddedAlias (Ok ()) ->
            ( { data | dialogAddAlias = Nothing }
            , Effect.none
            )

        UserPressedSubmit ->
            if data.publishRequested then
                ( data, Effect.none )

            else
                case Synchronization.state data.synchronization of
                    Synchronization.Synced ->
                        ( { data | publishRequested = True }
                        , Action.UserIntentsToPublishDraft ApiPublishedDraft data.uuid
                            |> Effect.fromAction
                        )

                    Synchronization.SyncNeeded ->
                        ( { data | publishRequested = True }, Effect.none )

                    Synchronization.Syncing ->
                        ( { data | publishRequested = True }, Effect.none )

                    Synchronization.SyncFailed _ ->
                        ( data, Effect.none )

        ApiPublishedDraft (Err (Api.BadRequestValidationReport report)) ->
            ( { data
                | publishRequested = False
                , form =
                    case data.form of
                        FormNotRequired ->
                            data.form

                        FormLoaded dataForm ->
                            FormLoaded { dataForm | report = report }
              }
            , [ { message = verbatim "Could not publish draft." }
                    |> Action.AddToastExpireIn 3000
                    |> Effect.fromAction
              , Browser.Dom.focus "report"
                    |> Task.attempt (\_ -> NoOp)
                    |> Effect.fromCmd
              ]
                |> Effect.batch
            )

        ApiPublishedDraft (Err error) ->
            ( { data | publishRequested = False }
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiPublishedDraft (Ok ()) ->
            let
                indexVersion =
                    List.length data.document.versions + 1
            in
            ( data
            , { uuid = data.uuid
              , index = String.fromInt indexVersion
              }
                |> Route.Datasets__Uuid___Versions__Index_
                |> Href.fromRouteWith c (queryCurrent data)
                |> Browser.Navigation.pushUrl key
                |> Effect.fromCmd
            )

        UserPressedDeleteDraft ->
            ( { data | deletingDraft = True }
            , Action.UserIntentsToDeleteDraft ApiCancelledDraft data.uuid
                |> Effect.fromAction
            )

        ApiCancelledDraft (Err error) ->
            ( data
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiCancelledDraft (Ok ()) ->
            ( data
            , case data.document.versions of
                [] ->
                    Route.Datasets
                        |> Href.fromRoute c
                        |> Browser.Navigation.pushUrl key
                        |> Effect.fromCmd

                _ ->
                    let
                        indexVersion =
                            List.length data.document.versions
                    in
                    { uuid = data.uuid
                    , index = String.fromInt indexVersion
                    }
                        |> Route.Datasets__Uuid___Versions__Index_
                        |> Href.fromRouteWith c (queryCurrent data)
                        |> Browser.Navigation.pushUrl key
                        |> Effect.fromCmd
            )

        UserPressedGenerate ->
            ( { data | generating = True }
            , Api.request c
                { method = "POST"
                , headers = [ Http.header "Accept" "application/trig" ]
                , pathSegments = [ "datasets", data.uuid, "draft", "generate" ]
                , queryParameters = []
                , body = Http.emptyBody
                , expect = Api.expectString ApiUpdatedDatasetAfterGenerate
                , tracker = Nothing
                }
            )

        ApiUpdatedDatasetAfterGenerate (Err error) ->
            ( { data | generating = False }
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiUpdatedDatasetAfterGenerate (Ok raw) ->
            ( { data | generating = False }
            , Action.StoreTrig CacheStoredDatasetAfterGenerate data.draft.this raw
                |> Effect.fromAction
            )

        CacheStoredDatasetAfterGenerate (Err error) ->
            ( data
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        CacheStoredDatasetAfterGenerate (Ok graphs) ->
            case data.form of
                FormNotRequired ->
                    ( data, Effect.none )

                FormLoaded dataForm ->
                    let
                        ( formUpdated, effectForm, maybeOutMsg ) =
                            Form.graphsChanged c.cShacl
                                populateDefaultValues
                                True
                                graphs
                                dataForm.form
                    in
                    ( { data
                        | form = FormLoaded { dataForm | form = formUpdated }
                        , synchronization =
                            case maybeOutMsg of
                                Nothing ->
                                    data.synchronization

                                Just Form.GraphChanged ->
                                    Synchronization.changed data.synchronization
                      }
                    , Effect.map MsgForm
                        (Loader.effectToEffect c data.document.herbie.workspaceUuid effectForm)
                    )

        UserSelectedFormatVisible format ->
            ( data
            , { uuid = data.uuid }
                |> Route.Datasets__Uuid___Draft
                |> Href.fromRouteWith c
                    (data
                        |> queryCurrent
                        |> Dict.insert "tab" (Format.toString format)
                    )
                |> Browser.Navigation.replaceUrl key
                |> Effect.fromCmd
            )

        UserPressedInstancesFilterAll ->
            ( { data | instancesListConfig = InstancesList.default }
            , Effect.none
            )

        UserPressedInstancesFilterOntology ->
            ( { data | instancesListConfig = InstancesList.toggleOntology instancesListConfig }
            , Effect.none
            )

        UserPressedInstancesFilterShacl ->
            ( { data | instancesListConfig = InstancesList.toggleShacl instancesListConfig }
            , Effect.none
            )

        UserPressedInstancesFilterOther ->
            ( { data | instancesListConfig = InstancesList.toggleOther instancesListConfig }
            , Effect.none
            )

        UserToggledExpansionOnInstances ->
            ( data
            , { uuid = data.uuid
              }
                |> Route.Datasets__Uuid___Draft
                |> Href.fromRouteWith c
                    (data
                        |> queryCurrent
                        |> (if data.instancesExpanded then
                                Dict.remove "instances"

                            else
                                Dict.insert "instances" ""
                           )
                    )
                |> Browser.Navigation.replaceUrl key
                |> Effect.fromCmd
            )

        -- FORM
        MsgForm msgForm ->
            case data.form of
                FormNotRequired ->
                    ( data, Effect.none )

                FormLoaded dataForm ->
                    let
                        ( formUpdated, effectForm, maybeOutMsg ) =
                            Form.update populateDefaultValues
                                c.cShacl
                                msgForm
                                dataForm.form
                    in
                    ( { data
                        | form = FormLoaded { dataForm | form = formUpdated }
                        , synchronization =
                            case maybeOutMsg of
                                Nothing ->
                                    data.synchronization

                                Just Form.GraphChanged ->
                                    Synchronization.changed data.synchronization
                      }
                    , Effect.map MsgForm
                        (Loader.effectToEffect c data.document.herbie.workspaceUuid effectForm)
                    )

        MsgSynchronization msgSynchronization ->
            let
                ( synchronizationUpdated, request ) =
                    Synchronization.update msgSynchronization data.synchronization
            in
            ( { data | synchronization = synchronizationUpdated }
            , case request of
                Synchronization.NoRequest ->
                    Effect.none

                Synchronization.Sync ->
                    putGraphFrontend c data
            )

        ApiUpdatedDataset (Err error) ->
            ( { data | synchronization = Synchronization.failed error data.synchronization }
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiUpdatedDataset (Ok raw) ->
            ( data
            , Action.StoreTrig CacheStoredDataset data.draft.this raw
                |> Effect.fromAction
            )

        CacheStoredDataset (Err error) ->
            ( data
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        CacheStoredDataset (Ok _) ->
            -- XXX: We do not update the form state via Form.graphsChanged as
            -- this function is not handling collection, schedule and grid
            -- properly.  We could therefore potentially end up with a stale
            -- UI.  The graphs returned here should in theory match what would
            -- be generated by Form.graph though.
            let
                ( synchronizationUpdated, request ) =
                    Synchronization.finished data.synchronization
            in
            case request of
                Synchronization.NoRequest ->
                    if data.publishRequested then
                        ( { data | synchronization = synchronizationUpdated }
                        , Action.UserIntentsToPublishDraft ApiPublishedDraft data.uuid
                            |> Effect.fromAction
                        )

                    else
                        ( { data | synchronization = synchronizationUpdated }
                        , Effect.none
                        )

                Synchronization.Sync ->
                    ( { data | synchronization = synchronizationUpdated }
                    , putGraphFrontend c data
                    )

        MsgFormPreview msgForm ->
            let
                ( formUpdated, effectForm, _ ) =
                    Form.update populateDefaultValues
                        c.cShacl
                        msgForm
                        data.formPreview
            in
            ( { data
                | formPreview = formUpdated
              }
            , Effect.map MsgFormPreview
                (Loader.effectToEffect c data.document.herbie.workspaceUuid effectForm)
            )

        -- STATEMENTS
        UserPressedCopyIri iri ->
            ( data
            , iri
                |> Rdf.toUrl
                |> Blob.textPlain
                |> Ports.copyToClipboard
                |> Effect.fromCmd
            )

        UserCheckedVisibleToEveryone checked ->
            let
                accessUpdated =
                    { access | visibleToEveryone = checked }
            in
            ( { data | access = accessUpdated }
            , accessUpdated
                |> Herbie.accessToGraph data.document
                |> putGraphAccess c data
            )

        ApiUpdatedDatasetAccess (Err error) ->
            ( data
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiUpdatedDatasetAccess (Ok raw) ->
            ( data
            , Action.StoreTrig CacheStoredDatasetAccess (iriAccess data.document.this) raw
                |> Effect.fromAction
            )

        CacheStoredDatasetAccess (Err error) ->
            ( data
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        CacheStoredDatasetAccess (Ok graphs) ->
            case DictIri.get (iriAccess data.document.this) graphs of
                Nothing ->
                    ( data, Effect.none )

                Just graphAccess ->
                    ( { data | access = Herbie.accessFromGraph data.document graphAccess }
                    , Effect.none
                    )


putGraphAccess : C -> DataLoaded -> Graph -> Effect Msg
putGraphAccess c data graph =
    Api.request c
        { method = "PUT"
        , headers = [ Http.header "Accept" "application/trig" ]
        , pathSegments = [ "datasets", data.uuid, "access" ]
        , queryParameters = []
        , body =
            graph
                |> Rdf.serialize
                |> Http.stringBody "application/n-triples"
        , expect = Api.expectString ApiUpdatedDatasetAccess
        , tracker = Nothing
        }


putGraphFrontend : C -> DataLoaded -> Effect Msg
putGraphFrontend c data =
    case data.form of
        FormNotRequired ->
            Effect.none

        FormLoaded { form } ->
            case Form.graphFrontend form of
                Nothing ->
                    Effect.none

                Just graphFrontend ->
                    Api.request c
                        { method = "PUT"
                        , headers = [ Http.header "Accept" "application/trig" ]
                        , pathSegments = [ "datasets", data.uuid, "draft" ]
                        , queryParameters = []
                        , body =
                            graphFrontend
                                |> Rdf.serialize
                                |> Http.stringBody "application/n-triples"
                        , expect = Api.expectString ApiUpdatedDataset
                        , tracker = Nothing
                        }


getGraphRaw : Herbie.DocumentDraft -> Effect Msg
getGraphRaw draft =
    [ Action.GetGraphTurtle ApiReturnedDocumentLatestTurtle draft.this
    , Action.GetGraphNTriples ApiReturnedDocumentLatestNTriples draft.this
    , Action.GetGraphJsonLd ApiReturnedDocumentLatestJsonLd draft.this
    ]
        |> List.map Effect.fromAction
        |> Effect.batch


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        Loading _ ->
            Sub.none

        Failed _ ->
            Sub.none

        Loaded data ->
            [ case data.form of
                FormNotRequired ->
                    Sub.none

                FormLoaded { form } ->
                    form
                        |> Form.subscriptions
                        |> Sub.map MsgForm
            , Sub.map MsgSynchronization (Synchronization.subscriptions data.synchronization)
            ]
                |> Sub.batch


events : C -> Model -> Event Msg
events _ model =
    case model of
        Loading _ ->
            Event.none

        Failed _ ->
            Event.none

        Loaded data ->
            data.document.this
                |> Event.GraphUpdated CacheUpdatedDocument
                |> Event.onCacheChange


element : C -> Model -> Element Msg
element c model =
    case model of
        Loading data ->
            data.loader
                |> Loader.toLoading
                |> viewLoading c
                |> pane
                |> Pane.toElement c

        Failed data ->
            data
                |> viewFailed c
                |> pane
                |> Pane.toElement c

        Loaded data ->
            let
                maximize =
                    case data.tabVisible of
                        TabForm ->
                            identity

                        TabCard ->
                            identity

                        TabGraph ->
                            Pane.maximize

                withSecondary =
                    if data.metaVisible then
                        data
                            |> viewMetaDraft c
                            |> Pane.withSecondary

                    else
                        identity

                withBottom =
                    if Datasets.canUpdateDraft c c.author data.document then
                        data
                            |> viewBottom c
                            |> Pane.withBottom

                    else
                        identity
            in
            data
                |> viewLoaded c
                |> pane
                |> maximize
                |> withSecondary
                |> withBottom
                |> Pane.toElement c


viewBottom : C -> DataLoaded -> Element Msg
viewBottom c data =
    [ (if data.publishRequested then
        t "action--submitting"

       else
        t "action--submit"
      )
        |> button UserPressedSubmit
        |> Button.view
    , (if data.generating then
        t "action--generating"

       else
        t "action--generate"
      )
        |> button UserPressedGenerate
        |> Button.withFormat Button.Outlined
        |> Button.view
    , t "action--delete-draft"
        |> button UserPressedDeleteDraft
        |> Button.withFormat Button.Text
        |> Button.view
    , if data.publishRequested then
        none

      else
        data
            |> viewFormState c
            |> el [ alignRight ]
    ]
        |> row
            [ width fill
            , spacing 16
            ]


viewLoading : C -> Loading -> Element Msg
viewLoading c graphsLoading =
    [ { label = verbatim "Loading graphs"
      , loading = graphsLoading
      }
    ]
        |> Loading.viewMany c
        |> el
            [ width fill
            , height fill
            , padding 16
            , Border.rounded 12
            ]


viewFailed : C -> DataFailed -> Element Msg
viewFailed c data =
    el
        [ width (maximum paneWidthMax fill)
        , height (maximum 640 fill)
        , padding 16
        , Border.rounded 12
        , Border.color error
        , Border.width 6
        , Font.family
            [ Font.typeface "Source Code Pro" ]
        , Font.size 12
        ]
        (el
            [ padding 16 ]
            (html
                (Html.div
                    [ Html.Attributes.style "white-space" "pre"
                    , Html.Attributes.style "line-height" "1.3"
                    ]
                    [ Html.text
                        (data.error
                            |> errorToString c
                        )
                    ]
                )
            )
        )
        |> el [ centerY ]
        |> el
            [ height fill
            , centerX
            ]


viewLoaded : C -> DataLoaded -> Element Msg
viewLoaded c data =
    if data.deletingDraft then
        none

    else
        let
            containsShaclShapes =
                data.graph
                    |> Shacl.fromGraph
                    |> Result.withDefault Shacl.empty
                    |> Shacl.nodeShapesWithTargetClasses
                    |> Result.isOk
        in
        [ viewCurrentVersionInfo c data
        , viewHeading c data
        , if data.tabVisible == TabGraph then
            viewGraph data

          else
            [ if data.draft.terms.conformsTo == Nothing then
                none

              else
                viewDraft c data
            , if containsShaclShapes then
                case data.tabVisible of
                    TabForm ->
                        FormPreview.view c data.formPreview
                            |> Element.map MsgFormPreview

                    TabCard ->
                        FormPreview.view c data.formPreview
                            |> Element.map MsgFormPreview

                    TabGraph ->
                        none

              else
                none
            , Rdf.union data.graphEntered data.graphGenerated
                |> Instances.view c
                    (instancesConfig data.instancesExpanded)
                    data.instancesListConfig
            ]
                |> column
                    [ width fill
                    , height fill
                    , spacing 16
                    ]
        ]
            |> column
                [ width fill
                , height fill
                , spacing 16
                ]


viewHeading : C -> DataLoaded -> Element Msg
viewHeading c data =
    [ [ [ case data.draft.rdfs.label of
            Nothing ->
                "Untitled graph"
                    |> Element.text
                    |> el
                        [ Font.size 24
                        , Font.medium
                        , Font.italic
                        ]
                    |> List.singleton
                    |> paragraph
                        [ width fill
                        , paddingXY 16 0
                        ]

            Just label ->
                label
                    |> Localize.forLabel c
                    |> Element.text
                    |> el
                        [ Font.size 24
                        , Font.medium
                        ]
                    |> List.singleton
                    |> paragraph
                        [ width fill
                        , paddingXY 16 0
                        ]
        , [ data.draft.this
                |> Rdf.toUrl
                |> verbatim
                |> body2
          , " (current)"
                |> verbatim
                |> body2
          ]
            |> paragraph
                [ width fill
                , paddingXY 16 0
                ]
        , [ [ Rdf.toUrl data.document.this
            , "versions/"
            , String.fromInt (List.length data.document.versions + 1)
            , "/"
            ]
                |> String.concat
                |> verbatim
                |> body2
          , " (after publish)"
                |> verbatim
                |> body2
          ]
            |> paragraph
                [ width fill
                , paddingXY 16 0
                ]
        ]
            |> column
                [ width fill
                , spacing 4
                , paddingXY 0 8
                ]
      , [ case data.tabVisible of
            TabForm ->
                "Raw"
                    |> verbatim
                    |> chipSuggestion (UserSelectedTab TabGraph)
                    |> ChipSuggestion.toElement

            TabCard ->
                "Raw"
                    |> verbatim
                    |> chipSuggestion (UserSelectedTab TabGraph)
                    |> ChipSuggestion.toElement

            TabGraph ->
                "Form"
                    |> verbatim
                    |> chipSuggestion (UserSelectedTab TabForm)
                    |> ChipSuggestion.toElement
        , Icon.FileDownload
            |> buttonIcon UserPressedDownload
            |> ButtonIcon.toElement
        , if Maybe.unwrap False (Datasets.canDelete c.author) c.workspace then
            Icon.Delete
                |> buttonIcon UserPressedDelete
                |> ButtonIcon.toElement

          else
            none
        , Icon.Settings
            |> buttonIcon UserPressedToggleMeta
            |> ButtonIcon.toElement
        ]
            |> row
                [ spacing 4
                , alignRight
                , alignTop
                ]
      ]
        |> row
            [ width fill ]
    , case data.draft.rdfs.comment of
        Nothing ->
            none

        Just comment ->
            comment
                |> Localize.verbatim c
                |> verbatim
                |> body1
                |> List.singleton
                |> paragraph
                    [ width fill
                    , paddingXY 16 0
                    ]
    ]
        |> column
            [ width fill
            , spacing 8
            ]


instancesConfig : Bool -> Instances.Config Msg
instancesConfig expanded =
    { userPressedInstancesFilterAll = UserPressedInstancesFilterAll
    , userPressedInstancesFilterOntology = UserPressedInstancesFilterOntology
    , userPressedInstancesFilterShacl = UserPressedInstancesFilterShacl
    , userPressedInstancesFilterOther = UserPressedInstancesFilterOther
    , userPressedCopyIri = UserPressedCopyIri
    , userToggledExpansion = UserToggledExpansionOnInstances
    , expanded = expanded
    }


viewCurrentVersionInfo : C -> DataLoaded -> Element Msg
viewCurrentVersionInfo c data =
    case List.head data.document.versions of
        Nothing ->
            none

        Just _ ->
            let
                indexVersion =
                    List.length data.document.versions
            in
            verbatim "You are editing an already published document."
                |> banner
                |> Banner.withFormat Banner.Info
                |> Banner.withButton
                    (verbatim "Open published version"
                        |> buttonLink
                            ({ uuid = data.uuid
                             , index = String.fromInt indexVersion
                             }
                                |> Route.Datasets__Uuid___Versions__Index_
                                |> Href.fromRouteWith c (queryCurrent data)
                            )
                        |> Button.withLeadingIcon Icon.Edit
                    )
                |> Banner.toElement


viewDraft : C -> DataLoaded -> Element Msg
viewDraft c data =
    let
        viewHelp content =
            [ [ Icon.viewNormal Icon.Assignment
              , viewTitle c data
                    |> el
                        [ width fill
                        , centerY
                        ]
              , if Datasets.canUpdateDraft c c.author data.document then
                    [ Chip.view
                        { iconLeft =
                            if data.tabVisible == TabForm then
                                Just Icon.Done

                            else
                                Nothing
                        , label = verbatim "Form"
                        , iconRight = Nothing
                        , narrow = False
                        , elevated = False
                        , elevatedOnHover = False
                        , primary = data.tabVisible == TabForm
                        , onPress = Just (UserSelectedTab TabForm)
                        , onPressRight = Nothing
                        , connectedOnLeft = False
                        , connectedOnRight = False
                        }
                    , Chip.view
                        { iconLeft =
                            if data.tabVisible == TabCard then
                                Just Icon.Done

                            else
                                Nothing
                        , label = verbatim "Card"
                        , iconRight = Nothing
                        , narrow = False
                        , elevated = False
                        , elevatedOnHover = False
                        , primary = data.tabVisible == TabCard
                        , onPress = Just (UserSelectedTab TabCard)
                        , onPressRight = Nothing
                        , connectedOnLeft = False
                        , connectedOnRight = False
                        }
                    ]
                        |> row
                            [ spacing 8
                            , alignRight
                            , centerY
                            ]

                else
                    none
              ]
                |> row
                    [ width fill
                    , paddingXY 16 0
                    , height (px 64)
                    , spacing 16
                    , Background.color surfaceContainer
                    , Border.roundEach
                        { topLeft = 12
                        , topRight = 12
                        , bottomLeft = 0
                        , bottomRight = 0
                        }
                    ]
            , content
            ]
                |> column
                    [ width fill
                    , Border.width 1
                    , Border.color outlineVariant
                    , Border.rounded 12
                    ]
    in
    case data.tabVisible of
        TabForm ->
            if Datasets.canUpdateDraft c c.author data.document then
                viewForm c data
                    |> el
                        [ width fill
                        , height fill
                        ]
                    |> viewHelp

            else
                viewCard c data
                    |> el
                        [ width fill
                        , height fill
                        ]
                    |> viewHelp

        TabCard ->
            viewCard c data
                |> el
                    [ width fill
                    , height fill
                    ]
                |> viewHelp

        TabGraph ->
            none


viewTitle : C -> DataLoaded -> Element msg
viewTitle c data =
    let
        viewUntitled =
            "Untitled"
                |> verbatim
                |> body1Strong
                |> List.singleton
                |> paragraph
                    [ width fill
                    , Font.italic
                    ]
    in
    case data.form of
        FormNotRequired ->
            viewUntitled

        FormLoaded { form } ->
            Form.heading c.cShacl form
                |> verbatim
                |> body1Strong
                |> List.singleton
                |> paragraph [ width fill ]


viewMetaDraft : C -> DataLoaded -> Element Msg
viewMetaDraft c data =
    [ [ "Meta"
            |> verbatim
            |> body1Strong
      , Icon.Close
            |> buttonIcon UserPressedToggleMeta
            |> ButtonIcon.toElement
            |> el [ alignRight ]
      ]
        |> row
            [ width fill
            , spacing 16
            , paddingEach
                { top = 8
                , bottom = 8
                , left = 16
                , right = 8
                }
            ]
    , [ case data.draft.terms.conformsTo of
            Nothing ->
                none

            Just conformsTo ->
                { url = Rdf.toUrl conformsTo.this
                , label =
                    conformsTo.this
                        |> Rdf.toUrl
                        |> verbatim
                }
                    |> Labelled.link (t "meta--conforms-to--label")
                    |> el [ paddingXY 16 0 ]
      , viewAliasesDraft c data
      , data.draft.prov.generatedAtTime
            |> Labelled.posixWithDistance c (t "meta--created-at--label")
            |> el [ paddingXY 16 0 ]
      , data.draft.terms.modified
            |> Labelled.posixWithDistance c (t "meta--modified-at--label")
            |> el [ paddingXY 16 0 ]
      , data.draft.prov.wasAttributedTo.rdfs.label
            |> Localize.verbatim c
            |> Labelled.string (t "meta--author--label")
            |> el [ paddingXY 16 0 ]
      , data.draft.this
            |> Rdf.toUrl
            |> Labelled.string (t "meta--iri-current--label")
            |> el [ paddingXY 16 0 ]
      , [ Rdf.toUrl data.document.this
        , "versions/"
        , String.fromInt (List.length data.document.versions + 1)
        , "/"
        ]
            |> String.concat
            |> Labelled.string (t "meta--iri-after-publish--label")
            |> el [ paddingXY 16 0 ]
      , if Maybe.unwrap False (Datasets.canUpdate c c.author data.document) c.workspace then
            data.access.visibleToEveryone
                |> checkbox UserCheckedVisibleToEveryone (verbatim "Visible to everyone")
                |> Checkbox.toElement

        else
            none
      , DocumentVersions.view c
            { uuid = data.uuid
            , document = data.document
            , active = DocumentVersions.Draft
            , query = queryCurrent data
            }
      ]
        |> column
            [ width fill
            , spacing 8
            ]
        |> el
            [ width fill
            , scrollbarY
            , style "flex-basis" "unset"
            ]
        |> el
            [ width fill
            , paddingEach
                { top = 0
                , bottom = 16
                , left = 0
                , right = 0
                }
            , style "min-height" "0"
            , style "flex-basis" "unset"
            , style "flex-shrink" "unset"
            ]
    ]
        |> column
            [ width fill
            , style "min-height" "0"
            , style "flex-shrink" "unset"
            ]


viewAliasesDraft : C -> DataLoaded -> Element Msg
viewAliasesDraft c data =
    [ t "meta--aliases--label"
        |> caption
        |> el
            [ paddingXY 16 0
            , Font.color onSurfaceVariant
            ]
    , [ data.draft.terms.source
            |> List.map
                (\iriSource ->
                    iriSource
                        |> Rdf.toUrl
                        |> verbatim
                        |> Listbox.item NoOp
                        |> Listbox.withActionIconAfterText Icon.Delete (UserPressedDeleteAlias iriSource)
                )
            |> Listbox.fromItems
            |> Listbox.withBackgroundColor surfaceContainerLow
            |> Listbox.withPaddingY 0
            |> Listbox.toElement
      , if Datasets.canUpdateDraft c c.author data.document then
            t "action--add-alias"
                |> chipAssist UserPressedAddAlias
                |> ChipAssist.withIcon Icon.Badge
                |> ChipAssist.toElement
                |> el [ paddingXY 16 0 ]

        else
            none
      ]
        |> column
            [ width fill
            , spacing 8
            ]
    ]
        |> column [ width fill ]


viewForm : C -> DataLoaded -> Element Msg
viewForm c data =
    case data.form of
        FormNotRequired ->
            none

        FormLoaded { form, report } ->
            form
                |> Form.view c.cShacl report
                |> Element.map MsgForm
                |> el
                    [ width fill
                    , height fill
                    , padding 16
                    ]


viewCard : C -> DataLoaded -> Element Msg
viewCard c data =
    case data.form of
        FormNotRequired ->
            none

        FormLoaded { form } ->
            form
                |> Form.card c.cShacl
                |> Element.map MsgForm
                |> el
                    [ width fill
                    , height fill
                    , padding 16
                    ]


viewFormState : C -> DataLoaded -> Element msg
viewFormState c data =
    if data.publishRequested then
        none

    else
        case Synchronization.state data.synchronization of
            Synchronization.Synced ->
                [ t "paragraph--everything-is-saved"
                    |> body2
                    |> el [ centerY ]
                , Icon.CheckCircleOutline
                    |> Icon.viewNormal
                ]
                    |> row [ spacing 8 ]

            Synchronization.SyncNeeded ->
                [ t "paragraph--unsaved-changes"
                    |> body2
                    |> el [ centerY ]
                , Icon.RadioButtonUnchecked
                    |> Icon.viewNormal
                ]
                    |> row [ spacing 8 ]

            Synchronization.Syncing ->
                [ t "paragraph--saving"
                    |> body2
                    |> el [ centerY ]
                , Icon.ArrowCircleUp
                    |> Icon.viewNormal
                ]
                    |> row [ spacing 8 ]

            Synchronization.SyncFailed err ->
                let
                    reason =
                        apiErrorToString c err
                in
                [ ("Saving failed (" ++ reason ++ ")")
                    |> verbatim
                    |> body2
                , Icon.Error
                    |> Icon.viewNormal
                ]
                    |> row
                        [ spacing 8
                        , Font.color error
                        ]


viewGraph : DataLoaded -> Element Msg
viewGraph data =
    { turtle = data.turtle
    , nTriples = data.nTriples
    , jsonLd = data.jsonLd
    , formatVisible = data.formatVisible
    }
        |> GraphSerializations.view { userSelectedFormatVisible = UserSelectedFormatVisible }
        |> el
            [ width fill
            , paddingEach
                { top = 16
                , bottom = 0
                , left = 0
                , right = 0
                }
            , Border.width 1
            , Border.color outlineVariant
            , Border.rounded 12
            ]


overlays : C -> Model -> List (Element Msg)
overlays c model =
    case model of
        Loading _ ->
            []

        Failed _ ->
            []

        Loaded data ->
            [ Maybe.map viewDialogAddAlias data.dialogAddAlias
            , case data.form of
                FormNotRequired ->
                    Nothing

                FormLoaded { form, report } ->
                    Form.viewFullscreen c.cShacl report form
                        |> Maybe.map
                            (Element.map MsgForm
                                >> el
                                    [ -- FIXME an overlay parent seems to specify `pointerEvents False`
                                      Element.pointerEvents True
                                    , width fill
                                    , height fill
                                    , scrollbarY
                                    , Background.color surface
                                    ]
                            )
            ]
                |> List.filterMap identity


viewDialogAddAlias : DialogAddAlias -> Element Msg
viewDialogAddAlias data =
    [ data.iri
        |> textInput UserChangedIri (TextInput.labelAbove (t "input--iri--label"))
        |> TextInput.onEnter UserPressedAddInDialogAddAlias
        |> TextInput.view
    ]
        |> column
            [ width fill
            , spacing 16
            ]
        |> Dialog.desktop (t "heading--add-alias")
        |> Dialog.withActions
            [ t "action--add-alias"
                |> button UserPressedAddInDialogAddAlias
                |> Button.view
                |> el [ paddingXY 8 0 ]
            , t "action--cancel"
                |> button UserPressedCancelInDialogAddAlias
                |> Button.withFormat Button.Text
                |> Button.view
                |> el [ alignRight ]
            ]
        |> Dialog.withScrimEvents
            { userPressedScrim = UserPressedCancelInDialogAddAlias
            , noOp = NoOp
            }
        |> Dialog.toElement


breadcrumbs : C -> Model -> Breadcrumbs
breadcrumbs c model =
    Breadcrumbs.from
        (List.filterMap identity
            [ case c.workspace of
                Nothing ->
                    Nothing

                Just workspace ->
                    { url = Href.fromRoute c (Route.Workspaces__Uuid_ { uuid = workspace.uuid })
                    , label = verbatim workspace.name
                    }
                        |> Breadcrumbs.link
                        |> Just
            , { url = Href.fromRoute c Route.Datasets
              , label = verbatim "Documents"
              }
                |> Breadcrumbs.link
                |> Just
            , { url =
                    { uuid = uuid model }
                        |> Route.Datasets__Uuid_
                        |> Href.fromRouteWith c (queryCurrentFromModel model)
              , label = verbatim (uuid model)
              }
                |> Breadcrumbs.link
                |> Just
            ]
        )
        (case model of
            Loading _ ->
                verbatim "Draft"

            Failed _ ->
                verbatim "Draft"

            Loaded data ->
                if List.head data.document.versions == Nothing then
                    t "heading--draft-initial"

                else
                    t "heading--draft-editing"
        )


queryCurrentFromModel : Model -> Dict String String
queryCurrentFromModel model =
    case model of
        Loading data ->
            queryCurrent data

        Failed data ->
            queryCurrent data

        Loaded data ->
            queryCurrent data


queryCurrent :
    { rest
        | tabVisible : Tab
        , formatVisible : Format
        , metaVisible : Bool
        , instancesExpanded : Bool
    }
    -> Dict String String
queryCurrent data =
    [ case data.tabVisible of
        TabForm ->
            Nothing

        _ ->
            Just ( "tab", tabAndFormatToString data.tabVisible data.formatVisible )
    , if data.metaVisible then
        Just ( "meta", "" )

      else
        Nothing
    , if data.instancesExpanded then
        Just ( "instances", "" )

      else
        Nothing
    ]
        |> List.filterMap identity
        |> Dict.fromList


iriAccess : Rdf.Iri -> Rdf.Iri
iriAccess iri =
    Rdf.iri (Rdf.toUrl iri ++ "access/")


t : String -> Fluent
t key =
    Fluent.text ("pages-datasets-uuid-draft--" ++ key)
