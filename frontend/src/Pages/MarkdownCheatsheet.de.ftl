pages-markdown-cheatsheet--autolinks--heading = Automatisch verlinkte Referenzen
pages-markdown-cheatsheet--blockquotes--heading = Blockzitate
pages-markdown-cheatsheet--code--heading = Code
pages-markdown-cheatsheet--emphasis--heading = Hervorhebungen
pages-markdown-cheatsheet--heading = Markdown Anleitung
pages-markdown-cheatsheet--headings--heading = Überschriften
pages-markdown-cheatsheet--introduction =
    Du kannst die folgenden Markdownelemente verwenden, um Texteinträge und
    Kommentare zu formatieren.
pages-markdown-cheatsheet--links--heading = Links
pages-markdown-cheatsheet--lists--heading = Listen
pages-markdown-cheatsheet--tables--heading = Tabellen
