module Pages.Graph exposing (Model, Msg, events, initWith, page)

{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}

import Accessibility.Blob as Blob
import Action
import Action.DatasetCreate as DatasetCreate
import Api
import Api.Class as Class
import Api.Datasets as Datasets
import Api.Instance as Instance
import Api.Workspace as Workspace exposing (Workspace)
import Browser.Navigation
import Context exposing (C)
import Dict exposing (Dict)
import Effect exposing (Effect)
import Element
    exposing
        ( Element
        , alignRight
        , alignTop
        , centerX
        , centerY
        , column
        , el
        , fill
        , height
        , maximum
        , none
        , padding
        , paddingEach
        , paddingXY
        , paragraph
        , px
        , row
        , scrollbarY
        , spacing
        , width
        )
import Element.Border as Border
import Element.Extra exposing (style)
import Element.Font as Font
import Event exposing (Event)
import File.Download
import Fluent
    exposing
        ( verbatim
        )
import Gen.Route as Route
import Href
import Http
import Inflect
import List.Extra as List
import List.NonEmpty as NonEmpty
import Locale
import Maybe.Extra as Maybe
import Maybe.Pipeline as Maybe
import Ontology.Class exposing (Class, NodeShape)
import Ontology.Instance as Instance exposing (Instance)
import Page
import Ports
import Rdf exposing (Iri, StringOrLangString)
import Rdf.Extra as Rdf
import Rdf.Namespaces.RDFS as RDFS
import RemoteData exposing (RemoteData)
import Request exposing (Request)
import Shacl.Form.Loader as Loader exposing (Loader)
import Shacl.Form.Localize as Localize
import Shacl.Form.Viewed as Form exposing (Form)
import Shared
import Time.Distance
import Tree exposing (Tree)
import Ui.Atom.Button as Button exposing (button, buttonLink)
import Ui.Atom.ButtonIcon as ButtonIcon exposing (buttonIcon)
import Ui.Atom.Checkbox as Checkbox exposing (checkbox)
import Ui.Atom.Divider exposing (horizontal)
import Ui.Atom.Icon as Icon
import Ui.Atom.Pane as Pane exposing (pane)
import Ui.Atom.TextInput as TextInput exposing (textInput)
import Ui.Atom.Tooltip as Tooltip
import Ui.Molecule.Breadcrumbs as Breadcrumbs exposing (Breadcrumbs)
import Ui.Molecule.ErrorBox as ErrorBox exposing (errorBox)
import Ui.Molecule.Listbox as Listbox
import Ui.Theme.Card as Card exposing (card)
import Ui.Theme.Color exposing (error)
import Ui.Theme.Dimensions exposing (paneWidthMax)
import Ui.Theme.Typography as Typography
    exposing
        ( body1
        , h6
        , link2
        , link2StoppingPropagation
        )
import Url.Extra as Url
import View exposing (View)


type Model
    = Redirecting
    | Loading DataLoading
    | Failed DataFailed
    | Loaded DataLoaded


apiErrorToString : C -> Api.Error -> String
apiErrorToString c error =
    error
        |> Api.errorToMessages
        |> Localize.verbatims c


type alias DataLoading =
    { params : Params
    , workspace : Maybe Workspace
    }


type alias DataFailed =
    { error : Error
    }


type Error
    = CouldNotLoadWorkspace Api.Error


type alias DataLoaded =
    { params : Params
    , workspace : Workspace
    , treeClasses : RemoteData Api.Error (Tree Class)
    , queryClasses : String
    , showClassesWithoutNodeShape : Bool
    , collapsed : Dict (List String) Bool
    , classSelected : Maybe Class
    , instances : RemoteData Api.Error (List Instance)
    , instanceDetails : RemoteData Api.Error Instance
    , loader : Maybe Loader
    , form : Maybe Form
    }


type alias Params =
    { workspaceUuid : String
    , iriClass : Maybe Iri
    , iriInstance : Maybe Iri
    }


paramsFromQuery : Dict String String -> Maybe Params
paramsFromQuery query =
    Just Params
        |> Maybe.required (Dict.get "workspace" query)
        |> Maybe.hardcoded (Maybe.map Rdf.iri (Dict.get "class" query))
        |> Maybe.hardcoded (Maybe.map Rdf.iri (Dict.get "instance" query))


page : Shared.Model -> Request -> Page.With Model Msg
page shared request =
    let
        c =
            Shared.toContext shared
    in
    Page.advanced
        { init = init c request
        , update = update c request.key
        , subscriptions = subscriptions
        , view = view c
        }


init : C -> Request -> ( Model, Effect Msg )
init c request =
    case paramsFromQuery request.query of
        Nothing ->
            ( Redirecting
            , Route.Workspaces
                |> Href.fromRoute c
                |> Browser.Navigation.replaceUrl request.key
                |> Effect.fromCmd
            )

        Just params ->
            ( Loading
                { params = params
                , workspace = Nothing
                }
            , Workspace.get c ApiReturnedWorkspace params.workspaceUuid
            )


initWith : C -> Request -> Model -> ( Model, Effect Msg )
initWith c request model =
    case paramsFromQuery request.query of
        Nothing ->
            init c request

        Just params ->
            case model of
                Loaded data ->
                    if params.iriClass /= data.params.iriClass then
                        { data
                            | params = params
                            , instanceDetails = RemoteData.NotAsked
                            , loader = Nothing
                            , form = Nothing
                        }
                            |> ensureInstances c
                            |> Tuple.mapFirst Loaded

                    else if params.iriInstance /= data.params.iriInstance then
                        { data
                            | params = params
                            , loader = Nothing
                            , form = Nothing
                        }
                            |> ensureInstance c
                            |> Tuple.mapFirst Loaded

                    else
                        ( Loaded data
                        , Effect.none
                        )

                _ ->
                    init c request


type Msg
    = NoOp
    | MsgTooltip Tooltip.Msg
      -- LOADING
    | ApiReturnedWorkspace (Api.Response Workspace)
      -- LOADED
    | ApiReturnedClasses (Api.Response (List Class))
    | ApiReturnedInstances (Api.Response (List Instance))
    | ApiReturnedInstance (Api.Response Instance)
      -- CLASSES
    | UserChangedQueryClasses String
    | UserPressedToggledClassesWithoutNodeShape Bool
    | UserPressedChevron (List String)
      -- INSTANCE
    | MsgLoader Loader.Msg
    | MsgForm Form.Msg
    | UserPressedAddInstance NodeShape
    | ApiCreatedDataset (Result DatasetCreate.Error String)
    | UserPressedLink Iri
    | UserPressedDownload Iri
    | ApiReturnedDatasetForDownload Iri (Api.Response String)


update : C -> Browser.Navigation.Key -> Msg -> Model -> ( Model, Effect Msg )
update c key msg model =
    case model of
        Redirecting ->
            ( model, Effect.none )

        Loading data ->
            updateLoading c msg data

        Failed _ ->
            ( model, Effect.none )

        Loaded data ->
            updateLoaded c key msg data
                |> Tuple.mapFirst Loaded


updateLoading : C -> Msg -> DataLoading -> ( Model, Effect Msg )
updateLoading c msg data =
    case msg of
        NoOp ->
            ( Loading data, Effect.none )

        MsgTooltip msgTooltip ->
            ( Loading data
            , Effect.fromAction (Action.TooltipMsg msgTooltip)
            )

        -- LOADING
        ApiReturnedWorkspace (Err error) ->
            ( Failed { error = CouldNotLoadWorkspace error }
            , Effect.none
            )

        ApiReturnedWorkspace (Ok workspace) ->
            ( Loaded
                { params = data.params
                , workspace = workspace
                , treeClasses = RemoteData.Loading
                , queryClasses = ""
                , showClassesWithoutNodeShape = False
                , collapsed = Dict.empty
                , classSelected = Nothing
                , instances = RemoteData.NotAsked
                , instanceDetails = RemoteData.NotAsked
                , loader = Nothing
                , form = Nothing
                }
            , Class.list c ApiReturnedClasses workspace.uuid
            )

        -- LOADED
        ApiReturnedClasses _ ->
            ( Loading data, Effect.none )

        ApiReturnedInstances _ ->
            ( Loading data, Effect.none )

        ApiReturnedInstance _ ->
            ( Loading data, Effect.none )

        -- CLASSES
        UserChangedQueryClasses _ ->
            ( Loading data, Effect.none )

        UserPressedToggledClassesWithoutNodeShape _ ->
            ( Loading data, Effect.none )

        UserPressedChevron _ ->
            ( Loading data, Effect.none )

        -- INSTANCE
        MsgLoader _ ->
            ( Loading data, Effect.none )

        MsgForm _ ->
            ( Loading data, Effect.none )

        UserPressedAddInstance _ ->
            ( Loading data, Effect.none )

        ApiCreatedDataset _ ->
            ( Loading data, Effect.none )

        UserPressedDownload _ ->
            ( Loading data, Effect.none )

        UserPressedLink _ ->
            ( Loading data, Effect.none )

        ApiReturnedDatasetForDownload _ _ ->
            ( Loading data, Effect.none )


updateLoaded : C -> Browser.Navigation.Key -> Msg -> DataLoaded -> ( DataLoaded, Effect Msg )
updateLoaded c key msg data =
    case msg of
        NoOp ->
            ( data, Effect.none )

        MsgTooltip msgTooltip ->
            ( data
            , Effect.fromAction (Action.TooltipMsg msgTooltip)
            )

        -- LOADING
        ApiReturnedWorkspace _ ->
            ( data, Effect.none )

        -- LOADED
        ApiReturnedClasses (Err error) ->
            ( { data | treeClasses = RemoteData.Failure error }
            , Effect.none
            )

        ApiReturnedClasses (Ok classes) ->
            { data
                | treeClasses =
                    classes
                        |> List.map addRdfsResourceParentClass
                        |> addRdfsResource
                        |> treeFromClasses
                        |> Result.withDefault (Tree.singleton rdfsResource)
                        |> RemoteData.Success
            }
                |> ensureInstances c

        ApiReturnedInstances (Err error) ->
            ( { data | instances = RemoteData.Failure error }
            , Effect.none
            )

        ApiReturnedInstances (Ok instances) ->
            { data | instances = RemoteData.Success instances }
                |> ensureInstance c

        ApiReturnedInstance (Err error) ->
            ( { data | instanceDetails = RemoteData.Failure error }
            , Effect.none
            )

        ApiReturnedInstance (Ok details) ->
            case details.rdfs.isDefinedBy of
                Nothing ->
                    ( { data | instanceDetails = RemoteData.Success details }
                    , Effect.none
                    )

                Just isDefinedBy ->
                    let
                        ( loader, effectLoader ) =
                            Loader.loadVersionIri isDefinedBy.this
                    in
                    ( { data
                        | instanceDetails = RemoteData.Success details
                        , loader = Just loader
                      }
                    , Effect.map MsgLoader effectLoader
                    )

        -- CLASSES
        UserChangedQueryClasses queryClasses ->
            ( { data | queryClasses = queryClasses }
            , Effect.none
            )

        UserPressedToggledClassesWithoutNodeShape showClassesWithoutNodeShape ->
            ( { data | showClassesWithoutNodeShape = showClassesWithoutNodeShape }
            , Effect.none
            )

        UserPressedChevron slugs ->
            ( { data | collapsed = Dict.update slugs (Maybe.map not >> Maybe.withDefault False >> Just) data.collapsed }
            , Effect.none
            )

        -- INSTANCE
        MsgLoader msgLoader ->
            case data.loader of
                Nothing ->
                    ( data, Effect.none )

                Just loader ->
                    case Loader.update c msgLoader loader of
                        Loader.Running loaderUpdated effectLoader ->
                            ( { data | loader = Just loaderUpdated }
                            , Effect.map MsgLoader effectLoader
                            )

                        Loader.SuccessDraft _ ->
                            ( { data | loader = Nothing }
                            , Effect.none
                            )

                        Loader.SuccessDraftWithForm _ ->
                            ( { data | loader = Nothing }
                            , Effect.none
                            )

                        Loader.SuccessVersion _ ->
                            -- FIXME What should we do here?
                            ( { data | loader = Nothing }
                            , Effect.none
                            )

                        Loader.SuccessVersionWithForm stuff ->
                            ( { data
                                | loader = Nothing
                                , form = Just stuff.form
                              }
                            , Effect.none
                            )

                        Loader.Failed _ ->
                            -- FIXME Report error
                            ( { data | loader = Nothing }
                            , Effect.none
                            )

        MsgForm msgForm ->
            case data.form of
                Nothing ->
                    ( data, Effect.none )

                Just form ->
                    let
                        ( formUpdated, effectForm, _ ) =
                            Form.update { populateDefaultValues = False } c.cShacl msgForm form
                    in
                    ( { data | form = Just formUpdated }
                    , Effect.map MsgForm (Loader.effectToEffect c data.workspace.uuid effectForm)
                    )

        UserPressedAddInstance nodeShape ->
            ( data
            , nodeShape.rdfs.isDefinedBy
                |> Action.UserIntentsToCreateDataset False ApiCreatedDataset data.params.workspaceUuid
                |> Effect.fromAction
            )

        ApiCreatedDataset (Err error) ->
            ( data
            , { message = verbatim (DatasetCreate.toString c error) }
                |> Action.AddToast
                |> Effect.fromAction
            )

        ApiCreatedDataset (Ok uuid) ->
            ( data
            , { uuid = uuid }
                |> Route.Datasets__Uuid___Draft
                |> Href.fromRoute c
                |> Browser.Navigation.pushUrl key
                |> Effect.fromCmd
            )

        UserPressedDownload iri ->
            ( data
            , Api.requestIri c
                { method = "GET"
                , headers = [ Http.header "Accept" "text/turtle" ]
                , iri = iri
                , queryParameters = []
                , body = Http.emptyBody
                , expect = Api.expectString (ApiReturnedDatasetForDownload iri)
                , tracker = Nothing
                }
            )

        UserPressedLink iri ->
            ( data
            , iri
                |> Rdf.toUrl
                |> Blob.textPlain
                |> Ports.copyToClipboard
                |> Effect.fromCmd
            )

        ApiReturnedDatasetForDownload _ (Err error) ->
            ( data
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiReturnedDatasetForDownload iri (Ok content) ->
            let
                uuid =
                    iri
                        |> Rdf.toUrl
                        |> String.split "/"
                        |> List.reverse
                        |> List.getAt 3
                        |> Maybe.withDefault "document"
            in
            ( data
            , content
                |> File.Download.string (uuid ++ ".ttl") "text/turtle"
                |> Effect.fromCmd
            )


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        Redirecting ->
            Sub.none

        Loading _ ->
            Sub.none

        Failed _ ->
            Sub.none

        Loaded data ->
            case data.form of
                Nothing ->
                    Sub.none

                Just form ->
                    Sub.map MsgForm (Form.subscriptions form)


events : C -> Model -> Event Msg
events _ _ =
    Event.none


addRdfsResource : List Class -> List Class
addRdfsResource classes =
    if List.any (\{ this } -> this == RDFS.resource) classes then
        classes

    else
        rdfsResource :: classes


rdfsResource : Class
rdfsResource =
    { this = RDFS.resource
    , nodeShapes = []
    , rdfs =
        { label = Nothing
        , comment = Nothing
        , subClassOf = []
        }
    }


addRdfsResourceParentClass : Class -> Class
addRdfsResourceParentClass ({ rdfs } as class) =
    if List.isEmpty class.rdfs.subClassOf && (class.this /= RDFS.resource) then
        { class | rdfs = { rdfs | subClassOf = [ RDFS.resource ] } }

    else
        class


ensureInstances : C -> DataLoaded -> ( DataLoaded, Effect Msg )
ensureInstances c data =
    let
        classSelected =
            data.params.iriClass
                |> Maybe.andThen
                    (\iriClass_ ->
                        data.treeClasses
                            |> RemoteData.toMaybe
                            |> Maybe.andThen
                                (Tree.toList
                                    >> List.find (\{ this } -> this == iriClass_)
                                )
                    )
    in
    ( { data
        | classSelected = classSelected
        , instances =
            if classSelected == Nothing then
                RemoteData.NotAsked

            else
                RemoteData.Loading
      }
    , classSelected
        |> Maybe.map (.this >> getInstances c data.params)
        |> Maybe.withDefault Effect.none
    )


getInstances : C -> Params -> Iri -> Effect Msg
getInstances c params iriClass =
    Instance.list c
        ApiReturnedInstances
        { workspaceUuid = params.workspaceUuid
        , class = iriClass
        , includes = []
        }


ensureInstance : C -> DataLoaded -> ( DataLoaded, Effect Msg )
ensureInstance c data =
    ( { data
        | instanceDetails =
            case data.params.iriInstance of
                Nothing ->
                    RemoteData.NotAsked

                Just _ ->
                    RemoteData.Loading
      }
    , data.params.iriInstance
        |> Maybe.map (getInstance c)
        |> Maybe.withDefault Effect.none
    )


getInstance : C -> Iri -> Effect Msg
getInstance c iri =
    Instance.get c ApiReturnedInstance iri


addParamClass : Maybe Class -> Dict String String -> Dict String String
addParamClass maybeClass =
    case maybeClass of
        Nothing ->
            identity

        Just class ->
            Dict.insert "class" (Rdf.toUrl class.this)


view : C -> Model -> View Msg
view c model =
    { title = "Explorer"
    , element = element c model
    , overlays = viewOverlays c model
    , breadcrumbs = breadcrumbs c model
    , fabVisible = False
    , navigationRailVisible = Maybe.unwrap False (Datasets.isAtLeastManager c.author) c.workspace
    , loading =
        case model of
            Redirecting ->
                True

            Loading _ ->
                True

            Failed _ ->
                False

            Loaded data ->
                (data.treeClasses == RemoteData.Loading)
                    || (data.instances == RemoteData.Loading)
                    || (data.instanceDetails == RemoteData.Loading)
    }


breadcrumbs : C -> Model -> Breadcrumbs
breadcrumbs c model =
    case model of
        Redirecting ->
            Breadcrumbs.empty

        Loading _ ->
            Breadcrumbs.from [] (verbatim "Explorer")

        Failed _ ->
            Breadcrumbs.from [] (verbatim "Explorer")

        Loaded data ->
            case data.classSelected of
                Nothing ->
                    Breadcrumbs.from [] (verbatim data.workspace.name)

                Just class ->
                    case RemoteData.toMaybe data.instanceDetails of
                        Nothing ->
                            Breadcrumbs.from
                                [ Breadcrumbs.link
                                    { url = Href.fromRoute c Route.Graph
                                    , label = verbatim data.workspace.name
                                    }
                                ]
                                (verbatim (classLabelPluralSafe c class))

                        Just details ->
                            Breadcrumbs.from
                                [ Breadcrumbs.link
                                    { url = Href.fromRoute c Route.Graph
                                    , label = verbatim data.workspace.name
                                    }
                                , Breadcrumbs.link
                                    { url =
                                        Href.fromRouteWith c
                                            (Dict.empty
                                                |> addParamClass (Just class)
                                            )
                                            Route.Graph
                                    , label = verbatim (classLabelPluralSafe c class)
                                    }
                                ]
                                (verbatim (instanceLabelSafe c details))


element : C -> Model -> Element Msg
element c model =
    case model of
        Redirecting ->
            none

        Loading _ ->
            none

        Failed data ->
            viewFailed c data

        Loaded data ->
            viewLoaded c data


viewFailed : C -> DataFailed -> Element Msg
viewFailed c data =
    data.error
        |> errorToHeading
        |> errorBox
        |> ErrorBox.withInfo (errorToString c data.error)
        |> ErrorBox.view
        |> el
            [ centerX
            , centerY
            ]


errorToHeading : Error -> String
errorToHeading error =
    case error of
        CouldNotLoadWorkspace _ ->
            "Could not load workspace"


errorToString : C -> Error -> String
errorToString c error =
    case error of
        CouldNotLoadWorkspace errorApi ->
            errorApi
                |> Api.errorToMessages
                |> Localize.verbatims c


viewLoaded : C -> DataLoaded -> Element Msg
viewLoaded c data =
    pane (viewInstances c data)
        |> Pane.withSecondaryOnLeft (viewClasses c data)
        |> Pane.loadingSecondary (data.treeClasses == RemoteData.Loading)
        |> Pane.toElement c


viewClasses : C -> DataLoaded -> Element Msg
viewClasses c data =
    case data.treeClasses of
        RemoteData.NotAsked ->
            none

        RemoteData.Loading ->
            none

        RemoteData.Failure _ ->
            -- FIXME Render failure info.
            paragraph
                [ width (maximum paneWidthMax fill)
                , height (maximum 640 fill)
                , Border.rounded 12
                ]
                [ Typography.body1 (verbatim "Could not load classes.")
                ]

        RemoteData.Success tree ->
            column
                [ width fill
                , Border.rounded 12
                , style "max-height" "inherit"
                , paddingEach
                    { top = 0
                    , bottom = 12
                    , left = 0
                    , right = 0
                    }
                ]
                [ viewClassesFilters data
                , viewClassesList c data tree
                ]


viewClassesFilters : DataLoaded -> Element Msg
viewClassesFilters data =
    column
        [ width fill
        , paddingXY 0 8
        , spacing 8
        , Border.roundEach
            { topLeft = 12
            , topRight = 12
            , bottomRight = 0
            , bottomLeft = 0
            }
        ]
        [ el
            [ width fill
            , paddingXY 32 8
            ]
            (h6 (verbatim "Classes"))
        , data.queryClasses
            |> textInput UserChangedQueryClasses (TextInput.labelAbove (verbatim "Filter classes"))
            |> TextInput.view
            |> el
                [ width fill
                , paddingXY 16 0
                ]
        , el
            [ width fill
            , paddingXY 16 0
            ]
            (data.showClassesWithoutNodeShape
                |> checkbox UserPressedToggledClassesWithoutNodeShape (verbatim "Show classes without shapes")
                |> Checkbox.toElement
            )
        ]


viewClassesList : C -> DataLoaded -> Tree Class -> Element Msg
viewClassesList c data tree =
    let
        toComparable class =
            class.rdfs.label
                |> Maybe.map (Localize.verbatim c)
                |> Maybe.orElse (Rdf.defaultLabel class.this)
                |> Maybe.withDefault (Rdf.toUrl class.this)
    in
    tree
        |> Tree.children
        |> List.sortBy (Tree.label >> toComparable)
        |> List.map
            (Tree.depthFirstTraversal
                (\state _ label children -> ( state, label, children ))
                (\s ancestors class children ->
                    let
                        slugs =
                            (class :: ancestors)
                                |> List.map toComparable
                                |> List.reverse

                        item =
                            class
                                |> itemClass c data.classSelected

                        isCollapsed =
                            Dict.get slugs data.collapsed
                                |> Maybe.withDefault True

                        filteredChildren =
                            List.filterMap identity children
                                |> List.sortBy Tuple.second
                                |> List.map Tuple.first

                        classHasNodeShape =
                            if data.showClassesWithoutNodeShape then
                                True

                            else
                                not (List.isEmpty class.nodeShapes)

                        slug =
                            class.rdfs.label
                                |> Maybe.map (Localize.verbatim c)
                                |> Maybe.orElse (Rdf.defaultLabel class.this)
                                |> Maybe.withDefault (Rdf.toUrl class.this)
                    in
                    ( s
                    , if List.isEmpty filteredChildren then
                        if classContainsQuery c data.queryClasses class && classHasNodeShape then
                            Just
                                ( item
                                    |> Listbox.withSpaceBeforeText
                                , slug
                                )

                        else
                            Nothing

                      else if isCollapsed then
                        Just
                            ( item
                                |> Listbox.withActionIconBeforeText Icon.ChevronRight (UserPressedChevron slugs)
                            , slug
                            )

                      else
                        Just
                            ( item
                                |> Listbox.withActionIconBeforeText Icon.ExpandMore (UserPressedChevron slugs)
                                |> Listbox.withSubItems filteredChildren
                            , slug
                            )
                    )
                )
                ()
                >> Tuple.second
            )
        |> List.filterMap identity
        |> List.map Tuple.first
        |> Listbox.fromItems
        |> Listbox.toElement
        |> el
            [ width fill
            , height fill
            ]
        |> List.singleton
        |> column
            [ width fill
            , scrollbarY
            , style "flex-basis" "unset"
            , style "min-height" "unset"
            ]


classContainsQuery : C -> String -> Class -> Bool
classContainsQuery c query class =
    let
        matchesWord word =
            Maybe.unwrap False (String.contains word) label
                || Maybe.unwrap False (String.contains word) comment

        queryWords =
            query
                |> String.toLower
                |> String.words

        label =
            Maybe.map (String.toLower << Localize.verbatim c) class.rdfs.label

        comment =
            Maybe.map (String.toLower << Localize.verbatim c) class.rdfs.comment
    in
    List.all matchesWord queryWords


treeFromClasses : List Class -> Result (Tree.StratifyError Class) (Tree Class)
treeFromClasses classes =
    Tree.stratify
        { id = .this >> Rdf.toUrl
        , parentId = .rdfs >> .subClassOf >> List.head >> Maybe.map Rdf.toUrl
        , transform = identity
        }
        classes


itemClass : C -> Maybe Class -> Class -> Listbox.Item Msg
itemClass c classSelected class =
    let
        url =
            Route.Graph
                |> Href.fromRouteWith c
                    (Dict.empty
                        |> addParamClass (Just class)
                    )

        withSelected =
            if Just class.this == Maybe.map .this classSelected then
                Listbox.active

            else
                identity

        withNodeShapes =
            if List.isEmpty class.nodeShapes then
                identity

            else
                [ String.fromInt (List.length class.nodeShapes)
                , if List.length class.nodeShapes == 1 then
                    " SHACL Shape available"

                  else
                    " SHACL Shapes available"
                ]
                    |> String.concat
                    |> verbatim
                    |> Listbox.withSecondaryParagraph
    in
    class.rdfs.label
        |> Maybe.map (Localize.forLabel c)
        |> Maybe.orElse (Rdf.defaultLabel class.this)
        |> Maybe.withDefault (Rdf.toUrl class.this)
        |> verbatim
        |> Listbox.itemLink url
        |> withNodeShapes
        |> withSelected


viewInstances : C -> DataLoaded -> Element Msg
viewInstances c data =
    Maybe.unwrap viewNoClassSelectedInfo (viewInstancesHelp c data) data.classSelected


viewInstancesHelp : C -> DataLoaded -> Class -> Element Msg
viewInstancesHelp c data class =
    column
        [ width (maximum paneWidthMax fill)
        , height fill
        ]
        [ column
            [ width fill
            , spacing 24
            ]
            [ viewClassInfo c class
            , case data.instanceDetails of
                RemoteData.NotAsked ->
                    viewInstancesList c data class

                RemoteData.Loading ->
                    -- FIXME
                    viewInstancesList c data class

                RemoteData.Success _ ->
                    el
                        [ width fill
                        , paddingEach
                            { top = 0
                            , bottom = 16
                            , left = 0
                            , right = 0
                            }
                        ]
                        (verbatim ("Show all " ++ classWithinSentencePluralSafe c class)
                            |> buttonLink
                                (Route.Graph
                                    |> Href.fromRouteWith c
                                        (Dict.empty
                                            |> addParamClass (Just class)
                                        )
                                )
                            |> Button.withLeadingIcon Icon.ArrowBack
                            |> Button.withFormat Button.Text
                            |> Button.view
                        )

                RemoteData.Failure _ ->
                    none
            ]
        , case data.instanceDetails of
            RemoteData.NotAsked ->
                none

            RemoteData.Loading ->
                none

            RemoteData.Success details ->
                viewInstanceDetails c data details

            RemoteData.Failure errorApi ->
                [ errorApi
                    |> apiErrorToString c
                    |> verbatim
                    |> body1
                ]
                    |> paragraph
                        [ width (maximum paneWidthMax fill)
                        , height (maximum 640 fill)
                        , padding 16
                        , Border.rounded 12
                        , Border.color error
                        , Border.width 6
                        ]
                    |> el [ centerY ]
                    |> el
                        [ height fill
                        , centerX
                        ]
        ]


viewClassInfo : C -> Class -> Element Msg
viewClassInfo c class =
    column
        [ width fill
        , paddingXY 16 0
        , spacing 8
        ]
        [ column
            [ width fill
            , spacing 4
            ]
            [ row
                [ width fill
                ]
                [ viewClassName c class
                , viewAddInstance c class
                ]
            , viewClassIri class
            ]
        , viewClassComment c class
        ]


viewClassName : C -> Class -> Element Msg
viewClassName c class =
    paragraph
        [ width fill
        , alignTop
        ]
        [ Typography.h3 (verbatim (classLabelPluralSafe c class))
        ]


viewAddInstance : C -> Class -> Element Msg
viewAddInstance c class =
    case class.nodeShapes of
        [] ->
            none

        nodeShape :: _ ->
            el
                [ alignRight
                , alignTop
                , paddingEach
                    { top = 8
                    , bottom = 0
                    , left = 0
                    , right = 0
                    }
                ]
                (verbatim ("Create " ++ classWithinSentenceSafe c class)
                    |> button (UserPressedAddInstance nodeShape)
                    |> Button.withLeadingIcon Icon.Add
                    |> Button.view
                )


viewClassIri : Class -> Element Msg
viewClassIri class =
    paragraph [ width fill ]
        [ link2
            { url = "#" -- FIXME Where should this be linked to?
            , label = verbatim (Rdf.toUrl class.this)
            }
        ]


viewClassComment : C -> Class -> Element Msg
viewClassComment c class =
    case class.rdfs.comment of
        Nothing ->
            none

        Just comment ->
            paragraph [ width fill ]
                [ Typography.body1 (verbatim (Localize.verbatim c comment)) ]


classLabelSafe : C -> Class -> String
classLabelSafe c class =
    class.rdfs.label
        |> Maybe.map (Localize.forLabel c)
        |> Maybe.orElse (defaultLabel c class.this)
        |> Maybe.withDefault (Rdf.toUrl class.this)


classLabelPluralSafe : C -> Class -> String
classLabelPluralSafe c class =
    class.rdfs.label
        |> Maybe.map (Localize.forLabelPlural c)
        |> Maybe.orElse (defaultLabelPlural c class.this)
        |> Maybe.withDefault (Rdf.toUrl class.this)


classWithinSentenceSafe : C -> Class -> String
classWithinSentenceSafe c class =
    class.rdfs.label
        |> Maybe.map (Localize.withinSentence c)
        |> Maybe.orElse (defaultLabel c class.this)
        |> Maybe.withDefault (Rdf.toUrl class.this)


classWithinSentencePluralSafe : C -> Class -> String
classWithinSentencePluralSafe c class =
    class.rdfs.label
        |> Maybe.map (Localize.withinSentencePlural c)
        |> Maybe.orElse (defaultLabelPlural c class.this)
        |> Maybe.withDefault (Rdf.toUrl class.this)


instanceLabelSafe : C -> Instance -> String
instanceLabelSafe c instance =
    instance.rdfs.label
        |> Maybe.map (Localize.forLabel c)
        |> Maybe.withDefault (classLabelSafe c (NonEmpty.head instance.a))


viewNoClassSelectedInfo : Element Msg
viewNoClassSelectedInfo =
    el
        [ height fill
        , width (maximum paneWidthMax fill)
        ]
        (column
            [ width (maximum paneWidthMax fill)
            , height (maximum (paneWidthMax // 2) fill)
            , centerX
            , centerY
            , padding 16
            , spacing 16
            ]
            [ paragraph
                [ centerX
                , Font.center
                ]
                [ Typography.h4 (verbatim "No class selected")
                ]
            , paragraph
                [ width fill
                , height fill
                , Font.center
                ]
                [ Typography.body1 (verbatim "Select a class to get more info about it as well as a list of all its instances.")
                ]
            ]
        )


viewInstancesList : C -> DataLoaded -> Class -> Element Msg
viewInstancesList c data class =
    case data.instances of
        RemoteData.NotAsked ->
            none

        RemoteData.Loading ->
            none

        RemoteData.Success instances ->
            viewInstancesListHelp c class instances

        RemoteData.Failure errorApi ->
            [ errorApi
                |> apiErrorToString c
                |> verbatim
                |> body1
            ]
                |> paragraph
                    [ width (maximum paneWidthMax fill)
                    , height (maximum 640 fill)
                    , padding 16
                    , Border.rounded 12
                    , Border.color error
                    , Border.width 6
                    ]
                |> el [ centerY ]
                |> el
                    [ height fill
                    , centerX
                    ]


viewInstancesListHelp : C -> Class -> List Instance -> Element Msg
viewInstancesListHelp c class instances =
    if List.isEmpty instances then
        viewInstancesListEmpty c class

    else
        viewInstancesCards c instances


viewInstancesListEmpty : C -> Class -> Element Msg
viewInstancesListEmpty c class =
    el
        [ height fill
        , centerX
        ]
        (el [ centerY ]
            (paragraph
                [ width (maximum paneWidthMax fill)
                , height (maximum 640 fill)
                , padding 16
                ]
                [ Typography.body1
                    (verbatim
                        ("There are  no "
                            ++ classWithinSentencePluralSafe c class
                            ++ " in the system."
                        )
                    )
                ]
            )
        )


viewInstancesCards : C -> List Instance -> Element Msg
viewInstancesCards c instances =
    column
        [ width fill
        , height fill
        , spacing 16
        ]
        (List.map (viewInstance c) instances)


viewInstance : C -> Instance -> Element Msg
viewInstance c instance =
    let
        url =
            Route.Graph
                |> Href.fromRouteWith c
                    (Dict.empty
                        |> addParamClass (Just (NonEmpty.head instance.a))
                        |> Dict.insert "instance" (Rdf.toUrl instance.this)
                    )
    in
    Element.link
        [ width fill ]
        { url = url
        , label = viewInstanceBody c instance
        }
        |> card
        |> Card.withType Card.Outlined
        |> Card.withoutPadding
        |> Card.toElement


viewInstanceDetails : C -> DataLoaded -> Instance -> Element Msg
viewInstanceDetails c data instance =
    viewInstanceDetailsBody c data instance
        |> card
        |> Card.withType Card.Outlined
        |> Card.withoutPadding
        |> Card.toElement


viewInstanceBody : C -> Instance -> Element Msg
viewInstanceBody c instance =
    column
        [ width fill
        , height fill
        ]
        [ column
            [ width fill
            , paddingEach
                { top = 16
                , bottom = 8
                , left = 16
                , right = 16
                }
            , spacing 8
            ]
            [ viewInstanceHeading c instance
            , viewInstanceDetailsComment c instance
            ]
        , column
            [ width fill
            , height fill
            , spacing 4
            , paddingXY 16 0
            ]
            (List.filterMap identity
                [ Maybe.map (viewInstanceDetailsIsDefinedBy c) instance.rdfs.isDefinedBy
                , Maybe.map (viewInstanceDetailsConformsTo c) (Maybe.andThen (.terms >> .conformsTo) instance.rdfs.isDefinedBy)
                ]
            )
        , Maybe.unwrap
            (el
                [ width fill
                , height (px 16)
                ]
                none
            )
            (viewInstanceDetailsBottom c)
            instance.rdfs.isDefinedBy
        ]


viewInstanceDetailsBody : C -> DataLoaded -> Instance -> Element Msg
viewInstanceDetailsBody c data instance =
    column
        [ width fill
        , height fill
        ]
        [ column
            [ width fill
            , paddingEach
                { top = 16
                , bottom = 8
                , left = 16
                , right = 16
                }
            , spacing 8
            ]
            [ viewInstanceDetailsHeading c instance
            , viewInstanceDetailsComment c instance
            ]
        , Maybe.unwrap none (viewInstanceDetailsForm c) data.form
        , column
            [ width fill
            , height fill
            , spacing 4
            , paddingXY 16 0
            ]
            (List.filterMap identity
                [ Maybe.map (viewInstanceDetailsIsDefinedBy c) instance.rdfs.isDefinedBy
                , Maybe.map (viewInstanceDetailsConformsTo c) (Maybe.andThen (.terms >> .conformsTo) instance.rdfs.isDefinedBy)
                , Just (viewInstanceDetailsSeeAlsos c instance.rdfs.seeAlso)
                ]
            )
        , Maybe.unwrap
            (el
                [ width fill
                , height (px 16)
                ]
                none
            )
            (viewInstanceDetailsBottom c)
            instance.rdfs.isDefinedBy
        ]


viewInstanceHeading : C -> Instance -> Element Msg
viewInstanceHeading c instance =
    row
        [ width fill
        , spacing 16
        ]
        [ paragraph
            [ width fill ]
            [ Typography.h5 (verbatim (instanceLabelSafe c instance))
            ]
        , Icon.Link
            |> buttonIcon (UserPressedLink instance.this)
            |> ButtonIcon.toElement
            |> Tooltip.add
                { label = verbatim "Copy IRI"
                , placement = Tooltip.Above
                , tooltip = c.tooltip
                , tooltipMsg = MsgTooltip
                , id = Rdf.serializeNode instance.this ++ "--copy-link"
                }
        ]


viewInstanceDetailsHeading : C -> Instance -> Element Msg
viewInstanceDetailsHeading c instance =
    row
        [ width fill
        , spacing 16
        ]
        [ paragraph
            [ width fill ]
            [ Typography.h4 (verbatim (instanceLabelSafe c instance))
            ]
        , Icon.Link
            |> buttonIcon (UserPressedLink instance.this)
            |> ButtonIcon.toElement
            |> Tooltip.add
                { label = verbatim "Copy IRI"
                , placement = Tooltip.Above
                , tooltip = c.tooltip
                , tooltipMsg = MsgTooltip
                , id = Rdf.serializeNode instance.this ++ "--copy-link"
                }
        ]


viewInstanceDetailsComment : C -> Instance -> Element Msg
viewInstanceDetailsComment c instance =
    case instance.rdfs.comment of
        Nothing ->
            none

        Just comment ->
            paragraph [ width fill ]
                [ Typography.body1 (verbatim (Localize.verbatim c comment)) ]


viewInstanceDetailsForm : C -> Form -> Element Msg
viewInstanceDetailsForm c form =
    column
        [ width fill
        , spacing 16
        , paddingEach
            { top = 0
            , bottom = 8
            , left = 0
            , right = 0
            }
        ]
        [ el
            [ width fill
            , paddingXY 16 8
            ]
            (Element.map MsgForm (Form.card c.cShacl form))
        , horizontal
        ]


viewInstanceDetailsIsDefinedBy : C -> Instance.IsDefinedBy -> Element Msg
viewInstanceDetailsIsDefinedBy c isDefinedBy =
    paragraph
        [ width fill ]
        [ "Defined by "
            |> verbatim
            |> Typography.body2Strong
        , viewLink c isDefinedBy.this isDefinedBy.rdfs.label
        ]


viewInstanceDetailsSeeAlsos : C -> List Instance.SeeAlso -> Element Msg
viewInstanceDetailsSeeAlsos c seeAlsos =
    column
        [ width fill
        , spacing 4
        ]
        (List.map (viewInstanceDetailsSeeAlso c) seeAlsos)


viewInstanceDetailsSeeAlso : C -> Instance.SeeAlso -> Element Msg
viewInstanceDetailsSeeAlso c seeAlso =
    paragraph
        [ width fill ]
        [ Icon.Link
            |> Icon.viewTiny
            |> el [ style "bottom" "-2px" ]
        , " Mentioned in "
            |> verbatim
            |> Typography.body2Strong
        , viewLink c seeAlso.this seeAlso.rdfs.label
        ]


viewInstanceDetailsConformsTo : C -> Instance.ConformsTo -> Element Msg
viewInstanceDetailsConformsTo c conformsTo =
    paragraph
        [ width fill ]
        [ "which conforms to "
            |> verbatim
            |> Typography.body2Strong
        , viewLink c conformsTo.this conformsTo.rdfs.label
        ]


viewInstanceDetailsBottom : C -> Instance.IsDefinedBy -> Element Msg
viewInstanceDetailsBottom c isDefinedBy =
    row [ width fill ]
        [ paragraph
            [ width fill
            , centerY
            , paddingXY 16 0
            ]
            [ Typography.body2Strong
                (verbatim (Localize.verbatim c isDefinedBy.prov.specializationOf.prov.wasAttributedTo.rdfs.label))
            , Typography.body2 (verbatim " ")
            , Typography.body2 (Time.Distance.inWordsWithAffix isDefinedBy.prov.specializationOf.terms.created c.now)

            -- FIXME
            -- , if countVersions > 1 then
            --     Typography.body2 (verbatim (" · edited " ++ times (countVersions - 1)))
            --   else
            --     none
            -- FIXME
            -- , if hasDraft then
            --     Typography.body2 (verbatim " · being edited")
            --   else
            --     none
            ]
        , viewInstanceDetailsBottomActions c isDefinedBy
        ]


viewInstanceDetailsBottomActions : C -> Instance.IsDefinedBy -> Element Msg
viewInstanceDetailsBottomActions c isDefinedBy =
    row
        [ spacing 2
        , paddingEach
            { top = 4
            , bottom = 4
            , left = 0
            , right = 8
            }
        ]
        [ Icon.Link
            |> buttonIcon (UserPressedLink isDefinedBy.this)
            |> ButtonIcon.toElement
            |> Tooltip.add
                { label = verbatim "Copy link"
                , placement = Tooltip.Above
                , tooltip = c.tooltip
                , tooltipMsg = MsgTooltip
                , id = Rdf.serializeNode isDefinedBy.this ++ "--copy-link"
                }
        , Icon.FileDownload
            |> buttonIcon (UserPressedDownload isDefinedBy.this)
            |> ButtonIcon.small
            |> ButtonIcon.toElement

        -- FIXME
        -- , if Maybe.unwrap False (Datasets.canEdit c c.author data.document) c.workspace then
        --     buttonIcon UserPressedEdit Icon.Edit
        --         |> ButtonIcon.small
        --         |> ButtonIcon.withEnabled (not hasDraft)
        --         |> ButtonIcon.toElement
        --   else
        --     none
        ]


viewLink : C -> Iri -> Maybe StringOrLangString -> Element Msg
viewLink c iri maybeLabel =
    link2StoppingPropagation NoOp
        { url = Url.setProtocol c (Rdf.toUrl iri)
        , label =
            verbatim
                (case maybeLabel of
                    Nothing ->
                        Rdf.toUrl iri

                    Just label ->
                        Localize.forLabel c label
                )
        }


viewOverlays : C -> Model -> List (Element Msg)
viewOverlays _ _ =
    []


defaultLabel : C -> Iri -> Maybe String
defaultLabel c iri =
    case c.locale of
        Locale.EnUS ->
            iri
                |> Rdf.defaultLabel

        Locale.De ->
            Rdf.defaultLabel iri


defaultLabelPlural : C -> Iri -> Maybe String
defaultLabelPlural c iri =
    case c.locale of
        Locale.EnUS ->
            iri
                |> Rdf.defaultLabel
                |> Maybe.map Inflect.toPlural

        Locale.De ->
            -- FIXME add german pluralization
            Rdf.defaultLabel iri
