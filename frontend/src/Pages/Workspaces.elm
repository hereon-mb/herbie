module Pages.Workspaces exposing (Model, Msg, events, page)

{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}

import Action
import Api
import Api.Workspace as Workspace exposing (Workspace)
import Browser.Navigation
import Context exposing (C)
import Dict
import Effect exposing (Effect)
import Element
    exposing
        ( DeviceClass(..)
        , Element
        , alignRight
        , centerX
        , centerY
        , column
        , el
        , fill
        , height
        , link
        , maximum
        , moveDown
        , none
        , padding
        , paddingEach
        , paddingXY
        , paragraph
        , px
        , row
        , scrollbarY
        , spacing
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Lazy exposing (lazy2)
import Event exposing (Event)
import Fluent
    exposing
        ( verbatim
        )
import Gen.Route as Route
import Href
import Page
import Request exposing (Request)
import Shacl.Form.Localize as Localize
import Shared
import String.Extra.Extra as String
import Ui.Atom.Button as Button exposing (button)
import Ui.Atom.Icon as Icon
import Ui.Atom.TextArea as TextArea exposing (textArea)
import Ui.Atom.TextInput as TextInput exposing (textInput)
import Ui.Molecule.Breadcrumbs as Breadcrumbs exposing (Breadcrumbs)
import Ui.Molecule.Dialog as Dialog
import Ui.Molecule.ErrorBox as ErrorBox exposing (errorBox)
import Ui.Theme.Card as Card exposing (card)
import Ui.Theme.Color exposing (outline, surfaceContainerHigh)
import Ui.Theme.Dimensions exposing (paneWidthMax)
import Ui.Theme.Typography exposing (body1Strong, body2, h4)
import View exposing (View)


type Model
    = Loading DataLoading
    | Failed DataFailed
    | Loaded DataLoaded


type alias DataLoading =
    {}


type alias DataFailed =
    { error : Error
    }


type Error
    = CouldNotLoadWorkspaces Api.Error


type alias DataLoaded =
    { workspaces : List Workspace
    , dialog : Dialog
    }


type Dialog
    = NoDialog
    | DialogCreateWorkspace DataDialogCreateWorkspace


type alias DataDialogCreateWorkspace =
    { name : String
    , slug : String
    , slugModified : Bool
    , description : String
    }


page : Shared.Model -> Request -> Page.With Model Msg
page shared request =
    let
        c =
            Shared.toContext shared
    in
    Page.advanced
        { init = init c request
        , update = update c request.key
        , subscriptions = subscriptions
        , view = view c
        }


init : C -> Request -> ( Model, Effect Msg )
init c _ =
    ( Loading {}
    , Workspace.list c ApiReturnedWorkspaces
    )


type Msg
    = NoOp
      -- LOADING
    | ApiReturnedWorkspaces (Api.Response (List Workspace))
      -- LOADED
    | UserPressedCreateWorkspace
    | UserCancelledDialog
    | MsgDialogCreateWorkspace MsgDialogCreateWorkspace
    | ApiReturnedNewWorkspace (Api.Response Workspace)


type MsgDialogCreateWorkspace
    = UserChangedName String
    | UserChangedSlug String
    | UserPressedResetSlug
    | UserChangedDescription String
    | UserPressedCreateWorkspaceInDialog


update : C -> Browser.Navigation.Key -> Msg -> Model -> ( Model, Effect Msg )
update c key msg model =
    case model of
        Loading data ->
            updateLoading c msg data

        Failed _ ->
            ( model, Effect.none )

        Loaded data ->
            updateLoaded c key msg data
                |> Tuple.mapFirst Loaded


updateLoading : C -> Msg -> DataLoading -> ( Model, Effect Msg )
updateLoading _ msg data =
    case msg of
        NoOp ->
            ( Loading data, Effect.none )

        -- LOADING
        ApiReturnedWorkspaces (Err error) ->
            ( Failed { error = CouldNotLoadWorkspaces error }
            , Effect.none
            )

        ApiReturnedWorkspaces (Ok workspaces) ->
            ( Loaded
                { workspaces = workspaces
                , dialog = NoDialog
                }
            , Effect.none
            )

        -- LOADED
        UserPressedCreateWorkspace ->
            ( Loading data, Effect.none )

        UserCancelledDialog ->
            ( Loading data, Effect.none )

        MsgDialogCreateWorkspace _ ->
            ( Loading data, Effect.none )

        ApiReturnedNewWorkspace _ ->
            ( Loading data, Effect.none )


updateLoaded : C -> Browser.Navigation.Key -> Msg -> DataLoaded -> ( DataLoaded, Effect Msg )
updateLoaded c _ msg data =
    case msg of
        NoOp ->
            ( data, Effect.none )

        -- LOADING
        ApiReturnedWorkspaces _ ->
            ( data, Effect.none )

        -- LOADED
        UserPressedCreateWorkspace ->
            ( { data
                | dialog =
                    DialogCreateWorkspace
                        { name = ""
                        , slug = ""
                        , slugModified = False
                        , description = ""
                        }
              }
            , Effect.none
            )

        UserCancelledDialog ->
            ( { data | dialog = NoDialog }
            , Effect.none
            )

        MsgDialogCreateWorkspace msgDialog ->
            case data.dialog of
                DialogCreateWorkspace dataDialog ->
                    let
                        ( dataDialogUpdated, effect ) =
                            updateDialogCreateWorkspace c msgDialog dataDialog
                    in
                    ( { data | dialog = DialogCreateWorkspace dataDialogUpdated }
                    , effect
                    )

                _ ->
                    ( data, Effect.none )

        ApiReturnedNewWorkspace (Err _) ->
            ( data
            , Effect.fromAction
                (Action.AddToast
                    { message = verbatim "Could not create workspace." }
                )
            )

        ApiReturnedNewWorkspace (Ok workspace) ->
            ( { data
                | dialog = NoDialog
                , workspaces = workspace :: data.workspaces
              }
            , Effect.fromAction
                (Action.AddToastExpireIn 3000
                    { message = verbatim "Created workspace." }
                )
            )


updateDialogCreateWorkspace : C -> MsgDialogCreateWorkspace -> DataDialogCreateWorkspace -> ( DataDialogCreateWorkspace, Effect Msg )
updateDialogCreateWorkspace c msg data =
    case msg of
        UserChangedName name ->
            ( { data
                | name = name
                , slug =
                    if data.slugModified then
                        data.slug

                    else
                        String.slugify name
              }
            , Effect.none
            )

        UserChangedSlug slug ->
            ( { data
                | slug = slug
                , slugModified = True
              }
            , Effect.none
            )

        UserPressedResetSlug ->
            ( { data
                | slug = String.slugify data.name
                , slugModified = False
              }
            , Effect.none
            )

        UserChangedDescription description ->
            ( { data | description = description }
            , Effect.none
            )

        UserPressedCreateWorkspaceInDialog ->
            ( data
            , Workspace.create c
                ApiReturnedNewWorkspace
                { name = String.trim data.name
                , slug = String.trim data.slug
                , description = data.description
                }
            )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


events : C -> Model -> Event Msg
events _ _ =
    Event.none



-- VIEW


view : C -> Model -> View Msg
view c model =
    { title = "Workspaces"
    , element =
        case model of
            Loading data ->
                viewLoading c data

            Failed data ->
                viewFailed c data

            Loaded data ->
                viewLoaded c data
    , overlays = viewOverlays c model
    , breadcrumbs = breadcrumbs model
    , fabVisible = False
    , navigationRailVisible = False
    , loading =
        case model of
            Loading _ ->
                True

            Failed _ ->
                False

            Loaded _ ->
                False
    }


breadcrumbs : Model -> Breadcrumbs
breadcrumbs _ =
    Breadcrumbs.from [] (verbatim "Workspaces")


viewLoading : C -> DataLoading -> Element Msg
viewLoading _ _ =
    none


viewFailed : C -> DataFailed -> Element Msg
viewFailed c data =
    data.error
        |> errorToHeading
        |> errorBox
        |> ErrorBox.withInfo (errorToString c data.error)
        |> ErrorBox.view
        |> el
            [ centerX
            , centerY
            ]


errorToHeading : Error -> String
errorToHeading error =
    case error of
        CouldNotLoadWorkspaces _ ->
            "Could not load workspaces"


errorToString : C -> Error -> String
errorToString c error =
    case error of
        CouldNotLoadWorkspaces errorApi ->
            errorApi
                |> Api.errorToMessages
                |> Localize.verbatims c


viewLoaded : C -> DataLoaded -> Element Msg
viewLoaded =
    lazy2
        (\c data ->
            el
                [ width fill
                , height fill
                , scrollbarY
                ]
                (viewLoadedBody c data)
        )


viewLoadedBody : C -> DataLoaded -> Element Msg
viewLoadedBody c data =
    column
        [ width (maximum paneWidthMax fill)
        , centerX
        , case c.device.class of
            Phone ->
                paddingEach
                    { top = 0
                    , bottom = 16 + 56 + 16
                    , left = 16
                    , right = 16
                    }

            _ ->
                paddingEach
                    { top = 0
                    , bottom = 16
                    , left = 16
                    , right = 16
                    }
        , spacing 32
        ]
        [ column
            [ width fill
            , spacing 16
            ]
            [ viewHeading
            , viewWorkspaces c data
            ]
        ]


viewHeading : Element Msg
viewHeading =
    row
        [ width fill ]
        [ el [ paddingXY 16 0 ]
            (h4 (verbatim "Workspaces"))
        ]


viewWorkspaces : C -> DataLoaded -> Element Msg
viewWorkspaces c data =
    if List.isEmpty data.workspaces then
        viewNoWorkspaces

    else
        column
            [ width fill
            , height fill
            , spacing 16
            ]
            [ column
                [ width fill
                , height fill
                , spacing 16
                ]
                (List.map (viewWorkspace c) data.workspaces)
            , "Create workspace"
                |> verbatim
                |> button UserPressedCreateWorkspace
                |> Button.withFormat Button.Outlined
                |> Button.withLeadingIcon Icon.Add
                |> Button.view
            ]


viewNoWorkspaces : Element Msg
viewNoWorkspaces =
    column
        [ width fill
        , spacing 32
        , padding 32
        , Border.width 1
        , Border.rounded 32
        , Border.color outline
        , Background.color surfaceContainerHigh
        ]
        [ column
            [ width fill
            , spacing 16
            ]
            [ paragraph
                [ centerX
                , Font.center
                ]
                [ body1Strong (verbatim "There are no workspaces, yet.") ]
            , paragraph
                [ width fill
                , Font.center
                ]
                [ body2
                    (verbatim "You don't have access to any workspace.  You can upload your graphs to workspaces and create documents in them.  Create your first workspace to get started or ask a colleague to add you to their workspace.")
                ]
            ]
        , el
            [ centerX
            ]
            ("Create workspace"
                |> verbatim
                |> button UserPressedCreateWorkspace
                |> Button.withLeadingIcon Icon.Add
                |> Button.view
            )
        ]


viewWorkspace : C -> Workspace -> Element Msg
viewWorkspace c workspace =
    link
        [ width fill ]
        { url =
            Href.fromRouteWith c
                (Dict.singleton "workspace" workspace.uuid)
                Route.Graph
        , label =
            column
                [ width fill
                , spacing 16
                ]
                [ body1Strong (verbatim workspace.name)
                , if workspace.description == "" then
                    none

                  else
                    paragraph
                        [ width fill ]
                        [ body2 (verbatim workspace.description) ]
                ]
                |> card
                |> Card.withType Card.Outlined
                |> Card.toElement
        }


viewOverlays : C -> Model -> List (Element Msg)
viewOverlays _ model =
    case model of
        Loading _ ->
            []

        Failed _ ->
            []

        Loaded data ->
            case data.dialog of
                NoDialog ->
                    []

                DialogCreateWorkspace dataDialog ->
                    [ viewDialogCreateWorkspace dataDialog ]


viewDialogCreateWorkspace : DataDialogCreateWorkspace -> Element Msg
viewDialogCreateWorkspace data =
    column
        [ width fill ]
        [ data.name
            |> textInput (MsgDialogCreateWorkspace << UserChangedName) (TextInput.labelAbove (verbatim "Name"))
            |> TextInput.view
        , row
            [ width fill
            , spacing 16
            ]
            [ data.slug
                |> textInput (MsgDialogCreateWorkspace << UserChangedSlug) (TextInput.labelAbove (verbatim "Slug"))
                |> TextInput.view
            , el
                [ moveDown 4
                ]
                ("Reset"
                    |> verbatim
                    |> button (MsgDialogCreateWorkspace UserPressedResetSlug)
                    |> Button.withFormat Button.Outlined
                    |> Button.withEnabled data.slugModified
                    |> Button.view
                )
            ]
        , el
            [ width fill
            , height (px 128)
            ]
            (data.description
                |> textArea (MsgDialogCreateWorkspace << UserChangedDescription)
                |> TextArea.withLabel (verbatim "Description")
                |> TextArea.withSupportingText (verbatim "Optional")
                |> TextArea.toElement
            )
        ]
        |> Dialog.desktop (verbatim "New workspace")
        |> Dialog.withScrimEvents
            { userPressedScrim = UserCancelledDialog
            , noOp = NoOp
            }
        |> Dialog.withActions
            [ el
                [ paddingXY 8 0 ]
                ("Create workspace"
                    |> verbatim
                    |> button (MsgDialogCreateWorkspace UserPressedCreateWorkspaceInDialog)
                    |> Button.view
                )
            , el [ alignRight ]
                ("Cancel"
                    |> verbatim
                    |> button UserCancelledDialog
                    |> Button.withFormat Button.Text
                    |> Button.view
                )
            ]
        |> Dialog.toElement
