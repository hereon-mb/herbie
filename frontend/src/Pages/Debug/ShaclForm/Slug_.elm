module Pages.Debug.ShaclForm.Slug_ exposing
    ( Params, Model, Msg
    , page
    )

{-|

@docs Params, Model, Msg
@docs page

-}

import Action
import Api
import Context exposing (C)
import Effect exposing (Effect)
import Element
    exposing
        ( Element
        , centerX
        , centerY
        , column
        , el
        , fill
        , height
        , maximum
        , none
        , padding
        , paragraph
        , row
        , scrollbars
        , spacing
        , text
        , width
        )
import Element.Font as Font
import Fluent exposing (verbatim)
import Page
import Rdf exposing (Iri)
import Rdf.Graph as RDF exposing (Graph)
import Request
import Shacl.Form.Loader as Loader
import Shacl.Form.Viewed as Form exposing (Form)
import Shacl.Report as Report
import Shared
import Ui.Molecule.Breadcrumbs as Breadcrumbs
import Ui.Theme.Dimensions as Dimensions
import Ui.Theme.Typography exposing (body2Mono)
import View exposing (View)


type Model
    = Loading
    | Loaded DataLoaded


type alias DataLoaded =
    { form : Form
    }


type alias Params =
    { slug : String }


type Msg
    = -- LOADING
      CacheReturnedGraph Iri (Api.Response Graph)
      -- LOADED
    | MsgForm Form.Msg


page : Shared.Model -> Request.With Params -> Page.With Model Msg
page shared request =
    let
        c =
            Shared.toContext shared
    in
    Page.advanced
        { init = init c request.params
        , update = update c
        , subscriptions = subscriptions c
        , view = view c request.params
        }


init : C -> Params -> ( Model, Effect Msg )
init c params =
    let
        iri =
            Api.iriHttp c [ "static", "examples", params.slug ++ ".trig" ]
    in
    ( Loading
    , iri
        |> Action.GetGraph
            { store = False
            , reload = True
            , closure = False
            }
            (CacheReturnedGraph iri)
        |> Effect.fromAction
    )


update : C -> Msg -> Model -> ( Model, Effect Msg )
update c msg model =
    case model of
        Loading ->
            updateLoading c msg

        Loaded data ->
            updateLoaded c msg data
                |> Tuple.mapFirst Loaded


updateLoading : C -> Msg -> ( Model, Effect Msg )
updateLoading c msg =
    case msg of
        CacheReturnedGraph _ (Err error) ->
            ( Loading
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        CacheReturnedGraph iri (Ok graph) ->
            let
                ( form, effectForm, _ ) =
                    Form.initPreview "workspace-uuid" iri graph
            in
            ( Loaded { form = form }
            , Effect.map MsgForm (Loader.effectToEffect c "workspace-uuid" effectForm)
            )

        MsgForm _ ->
            ( Loading, Effect.none )


updateLoaded : C -> Msg -> DataLoaded -> ( DataLoaded, Effect Msg )
updateLoaded c msg data =
    case msg of
        CacheReturnedGraph _ _ ->
            ( data, Effect.none )

        MsgForm msgForm ->
            let
                ( formUpdated, effectForm, _ ) =
                    Form.update { populateDefaultValues = True } c.cShacl msgForm data.form
            in
            ( { data | form = formUpdated }
            , Effect.map MsgForm (Loader.effectToEffect c "workspace-uuid" effectForm)
            )


subscriptions : C -> Model -> Sub Msg
subscriptions _ model =
    case model of
        Loading ->
            Sub.none

        Loaded data ->
            Sub.map MsgForm (Form.subscriptions data.form)


view : C -> Params -> Model -> View Msg
view c params model =
    { title = "Debug SHACL Forms"
    , element = element c model
    , overlays = overlays c model
    , breadcrumbs =
        Breadcrumbs.from
            [ Breadcrumbs.static (verbatim "Debug")
            , Breadcrumbs.static (verbatim "SHACL Form")
            ]
            (verbatim params.slug)
    , fabVisible = False
    , navigationRailVisible = True
    , loading =
        case model of
            Loading ->
                True

            Loaded _ ->
                False
    }


element : C -> Model -> Element Msg
element c model =
    case model of
        Loading ->
            none

        Loaded data ->
            [ [ data.form
                    |> Form.view c.cShacl Report.empty
                    |> Element.map MsgForm
              , data.form
                    |> Form.card c.cShacl
                    |> Element.map MsgForm
              ]
                |> row
                    [ height fill
                    , width fill
                    , spacing 16
                    ]
            , data.form
                |> Form.graphFrontend
                |> Maybe.map viewGraph
                |> Maybe.withDefault viewNoGraph
            ]
                |> column
                    [ width (maximum (2 * Dimensions.paneWidthMax + 16) fill)
                    , height fill
                    , centerX
                    , spacing 16
                    ]
                |> el
                    [ width fill
                    , height fill
                    , padding 32
                    ]


viewGraph : Graph -> Element msg
viewGraph graph =
    graph
        |> RDF.serialize
        |> verbatim
        |> body2Mono
        |> el
            [ width fill
            , height fill
            , padding 16
            ]
        |> el
            [ width fill
            , height fill
            , scrollbars
            ]


viewNoGraph : Element msg
viewNoGraph =
    [ text "No graph generated." ]
        |> paragraph
            [ width fill
            , centerY
            , Font.center
            ]


overlays : C -> Model -> List (Element Msg)
overlays c model =
    case model of
        Loading ->
            []

        Loaded data ->
            [ data.form
                |> Form.viewFullscreen c.cShacl Report.empty
                |> Maybe.map (Element.map MsgForm)
            ]
                |> List.filterMap identity
