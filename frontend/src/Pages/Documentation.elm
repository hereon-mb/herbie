module Pages.Documentation exposing (Doc, Model, Msg, initWith, page)

{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}

import Context exposing (C)
import Dict exposing (Dict)
import Documentation
import Effect exposing (Effect)
import Element as Ui exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Element.Extra as Ui
import Element.Lazy as Ui
import Fluent
import Gen.Route as Route
import Href
import List.Extra as List
import List.NonEmpty as NonEmpty
import Maybe.Extra as Maybe
import Page
import Rdf.Graph as Graph exposing (Graph)
import Request exposing (Request)
import Shacl.Documentation as Documentation exposing (Documentation)
import Shacl.Form.Loader as Loader
import Shacl.Form.Viewed as Form exposing (Form)
import Shacl.Report as Report
import Shared
import Tree
import Tree.Zipper as Zipper exposing (Zipper)
import Ui.Atom.Divider as Divider
import Ui.Atom.Icon as Icon
import Ui.Molecule.Breadcrumbs as Breadcrumbs
import Ui.Molecule.Listbox as Listbox
import Ui.Theme.Color as Color
import Ui.Theme.Typography as Typography
import View exposing (View)


page : Shared.Model -> Request -> Page.With Model Msg
page shared request =
    let
        c =
            Shared.toContext shared
    in
    Page.advanced
        { init = init c request
        , update = update c
        , subscriptions = subscriptions
        , view = view c
        }


type alias Model =
    { docs : Zipper Documentation
    , collapsed : Dict (List String) Bool
    , selected : Maybe Doc
    }


type alias Doc =
    { slugs : List String
    , info : Documentation.Info
    , infoForm : Maybe (Result Documentation.Error Documentation.InfoForm)
    }


init : C -> Request -> ( Model, Effect Msg )
init c request =
    initWith c
        request
        { docs = docsInitial
        , collapsed = Dict.empty
        , selected = Nothing
        }


initWith : C -> Request -> Model -> ( Model, Effect Msg )
initWith c request model =
    case Dict.get "example" request.query of
        Nothing ->
            ( model, Effect.none )

        Just example ->
            let
                slugs =
                    String.split "/" example

                findNext slug =
                    Maybe.andThen
                        (\zipper ->
                            if (Documentation.toInfo (Zipper.label zipper)).headingShort == slug then
                                Just zipper

                            else
                                Zipper.findNext (\d -> (Documentation.toInfo d).headingShort == slug) zipper
                        )
            in
            case
                List.foldl findNext (Just docsInitial) slugs
                    |> Maybe.map Zipper.label
            of
                Nothing ->
                    ( model, Effect.none )

                Just documentation ->
                    case Documentation.toForm c documentation of
                        Nothing ->
                            ( { model
                                | collapsed =
                                    slugs
                                        |> List.inits
                                        |> List.foldl (\slugsPart -> Dict.insert slugsPart False) model.collapsed
                                , selected =
                                    Just
                                        { slugs = slugs
                                        , info = Documentation.toInfo documentation
                                        , infoForm = Nothing
                                        }
                              }
                            , Effect.none
                            )

                        Just (Err error) ->
                            ( { model
                                | collapsed =
                                    slugs
                                        |> List.inits
                                        |> List.foldl (\slugsPart -> Dict.insert slugsPart False) model.collapsed
                                , selected =
                                    Just
                                        { slugs = slugs
                                        , info = Documentation.toInfo documentation
                                        , infoForm = Just (Err error)
                                        }
                              }
                            , Effect.none
                            )

                        Just (Ok ( infoForm, effect )) ->
                            ( { model
                                | collapsed =
                                    slugs
                                        |> List.inits
                                        |> List.foldl (\slugsPart -> Dict.insert slugsPart False) model.collapsed
                                , selected =
                                    Just
                                        { slugs = slugs
                                        , info = Documentation.toInfo documentation
                                        , infoForm = Just (Ok infoForm)
                                        }
                              }
                            , Effect.map MsgForm effect
                            )


docsInitial : Zipper Documentation
docsInitial =
    Zipper.fromForest (Tuple.first Documentation.all) (Tuple.second Documentation.all)


type Msg
    = UserPressedChevron (List String)
    | MsgForm Form.Msg


update : C -> Msg -> Model -> ( Model, Effect Msg )
update c msg model =
    case msg of
        UserPressedChevron slugs ->
            ( { model | collapsed = Dict.update slugs (Maybe.map not >> Maybe.withDefault False >> Just) model.collapsed }
            , Effect.none
            )

        MsgForm msgForm ->
            case model.selected of
                Nothing ->
                    ( model, Effect.none )

                Just selected ->
                    case selected.infoForm of
                        Nothing ->
                            ( model, Effect.none )

                        Just (Err _) ->
                            ( model, Effect.none )

                        Just (Ok infoForm) ->
                            let
                                ( formUpdated, effectForm, _ ) =
                                    Form.update { populateDefaultValues = False }
                                        c.cShacl
                                        msgForm
                                        infoForm.form
                            in
                            ( { model
                                | selected =
                                    Just { selected | infoForm = Just (Ok { infoForm | form = formUpdated }) }
                              }
                            , Effect.map MsgForm (Loader.effectToEffect c "workspace" effectForm)
                            )


subscriptions : Model -> Sub Msg
subscriptions model =
    case model.selected of
        Nothing ->
            Sub.none

        Just selected ->
            case selected.infoForm of
                Nothing ->
                    Sub.none

                Just (Err _) ->
                    Sub.none

                Just (Ok infoForm) ->
                    Sub.map MsgForm (Form.subscriptions infoForm.form)


view : C -> Model -> View Msg
view c model =
    { title = "Documentation"
    , element = viewPage c model
    , overlays = []
    , breadcrumbs = Breadcrumbs.from [] (Fluent.verbatim "Documentation")
    , fabVisible = False
    , navigationRailVisible = False
    , loading = False
    }


viewPage : C -> Model -> Element Msg
viewPage c model =
    Ui.row
        [ Ui.width Ui.fill
        , Ui.height Ui.fill
        ]
        [ Ui.column
            [ Ui.width (Ui.maximum 480 Ui.fill)
            , Ui.height Ui.fill
            ]
            [ Ui.column
                [ Ui.width Ui.fill
                , Ui.height Ui.fill
                , Ui.scrollbarY
                , Ui.alignTop
                ]
                [ viewListbox c model model.docs
                ]
            ]
        , Ui.columnWithAncestorWithScrollbarX
            [ Ui.width Ui.fill
            , Ui.height Ui.fill
            ]
            [ Ui.columnWithAncestorWithScrollbarX
                [ Ui.width Ui.fill
                , Ui.height Ui.fill
                , Ui.scrollbarY
                ]
                [ Ui.row
                    [ Ui.width Ui.fill
                    , Ui.height Ui.fill
                    , Ui.padding 16
                    ]
                    [ Maybe.unwrap Ui.none (viewSelected c) model.selected
                    ]
                ]
            ]
        ]


viewListbox : C -> Model -> Zipper Documentation -> Element Msg
viewListbox c model docs =
    docs
        |> Zipper.toForest
        |> NonEmpty.toList
        |> List.map
            (Tree.depthFirstTraversal
                (\state _ label children -> ( state, label, children ))
                (\s ancestors documentation children ->
                    let
                        slugs =
                            (documentation :: ancestors)
                                |> List.map (.headingShort << Documentation.toInfo)
                                |> List.reverse

                        item =
                            Documentation.toInfo documentation
                                |> .headingShort
                                |> Fluent.verbatim
                                |> Listbox.itemLink url
                                |> active

                        url =
                            Href.fromRouteWith c
                                (Dict.singleton "example" (String.join "/" slugs))
                                Route.Documentation

                        active =
                            if Just slugs == Maybe.map .slugs model.selected then
                                Listbox.active

                            else
                                identity

                        collapsed =
                            Dict.get slugs model.collapsed
                                |> Maybe.withDefault True
                    in
                    ( s
                    , if List.isEmpty children then
                        item
                            |> Listbox.withSpaceBeforeText

                      else if collapsed then
                        item
                            |> Listbox.withActionIconBeforeText Icon.ChevronRight (UserPressedChevron slugs)

                      else
                        item
                            |> Listbox.withActionIconBeforeText Icon.ExpandMore (UserPressedChevron slugs)
                            |> Listbox.withSubItems children
                    )
                )
                ()
                >> Tuple.second
            )
        |> Listbox.fromItems
        |> Listbox.toElement


viewSelected : C -> Doc -> Element Msg
viewSelected =
    Ui.lazy2
        (\c doc ->
            Ui.columnWithAncestorWithScrollbarX
                [ Ui.width Ui.fill
                , Ui.height Ui.fill
                , Ui.spacing 16
                ]
                [ viewInfo doc
                , case doc.infoForm of
                    Nothing ->
                        Ui.none

                    Just (Err error) ->
                        viewError error

                    Just (Ok infoForm) ->
                        Ui.column
                            [ Ui.width Ui.fill
                            , Ui.spacing 16
                            ]
                            [ viewDemo c infoForm.form
                            , viewCode infoForm
                            ]
                ]
        )


viewInfo : Doc -> Element Msg
viewInfo doc =
    Ui.column
        [ Ui.width Ui.fill
        , Ui.spacing 8
        , Ui.paddingXY 16 0
        ]
        [ Typography.h5 (Fluent.verbatim doc.info.headingFull)
        , Ui.paragraph
            [ Ui.width Ui.fill
            ]
            [ Typography.body1 (Fluent.verbatim doc.info.description)
            ]
        ]


viewError : Documentation.Error -> Element Msg
viewError error =
    let
        errorString =
            case error of
                Documentation.ErrorGraphShape documentShacl errorGraph ->
                    "Could not parse SHACL graph: " ++ Graph.errorToString documentShacl errorGraph

                Documentation.ErrorGraphData documentData errorGraph ->
                    "Could not parse data graph: " ++ Graph.errorToString documentData errorGraph

                Documentation.ErrorForm errorForm ->
                    "Could not initialize form: " ++ Form.errorToString errorForm
    in
    Ui.column
        [ Ui.width Ui.fill
        , Border.rounded 12
        , Background.color Color.surfaceContainerHighest
        ]
        [ Ui.el
            [ Ui.width Ui.fill
            , Ui.paddingEach
                { top = 16
                , bottom = 0
                , left = 16
                , right = 16
                }
            ]
            (Typography.h6 (Fluent.verbatim "Demo could not be loaded"))
        , Ui.el
            [ Ui.width Ui.fill
            ]
            (Ui.el
                [ Ui.width Ui.fill
                , Ui.height Ui.fill
                , Ui.scrollbarX
                ]
                (Ui.row
                    [ Ui.width Ui.fill
                    , Ui.height Ui.fill
                    , Ui.padding 16
                    ]
                    [ Ui.el
                        [ Ui.width Ui.fill
                        , Ui.height Ui.fill
                        ]
                        (Typography.body1Mono (Fluent.verbatim errorString))
                    ]
                )
            )
        ]


viewDemo : C -> Form -> Element Msg
viewDemo c form =
    Ui.columnWithAncestorWithScrollbarX
        [ Ui.width Ui.fill
        , Border.rounded 12
        , Border.width 6
        , Border.color Color.hereonGelbContainer
        ]
        [ Ui.el
            [ Ui.width Ui.fill
            , Ui.paddingEach
                { top = 12
                , bottom = 4
                , left = 12
                , right = 12
                }
            ]
            (Typography.h6 (Fluent.verbatim "Demo"))
        , Ui.row
            [ Ui.width Ui.fill
            , Ui.spacing 32
            , Ui.paddingXY 12 0
            ]
            [ Ui.columnWithAncestorWithScrollbarX
                [ Ui.width Ui.fill
                , Ui.alignTop
                ]
                [ Ui.map MsgForm (Form.view c.cShacl Report.empty form) ]
            , Ui.columnWithAncestorWithScrollbarX
                [ Ui.width Ui.fill
                , Ui.alignTop
                ]
                [ Ui.map MsgForm (Form.card c.cShacl form) ]
            ]
        , Divider.horizontal
        , Ui.el
            [ Ui.width Ui.fill ]
            (case Form.graphFrontend form of
                Nothing ->
                    Typography.body1 (Fluent.verbatim "No graph generated.")

                Just graph ->
                    viewGraph graph
            )
        ]


viewGraph : Graph -> Element Msg
viewGraph graph =
    let
        raw =
            graph
                |> Graph.setBase "http://example.org/document/"
                |> Graph.addPrefix "showcase" "http://purls.helmholtz-metadaten.de/herbie/showcase/ont/#"
                |> Graph.addPrefix "rdf" "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                |> Graph.addPrefix "rdfs" "http://www.w3.org/2000/01/rdf-schema#"
                |> Graph.serializeTurtle
    in
    Ui.el
        [ Ui.width Ui.fill
        , Ui.height Ui.fill
        , Ui.scrollbarX
        ]
        (Ui.row
            [ Ui.width Ui.fill
            , Ui.height Ui.fill
            ]
            [ Ui.el
                [ Ui.width Ui.fill
                , Ui.height Ui.fill
                , Ui.padding 12
                ]
                (Typography.body2Mono (Fluent.verbatim raw))
            ]
        )


viewCode : Documentation.InfoForm -> Element Msg
viewCode infoForm =
    let
        code =
            infoForm.documentShacl
    in
    Ui.column
        [ Ui.width Ui.fill
        , Border.rounded 12
        , Border.width 1
        , Border.color Color.outline
        ]
        [ Ui.el
            [ Ui.width Ui.fill
            , Ui.paddingEach
                { top = 16
                , bottom = 0
                , left = 16
                , right = 16
                }
            ]
            (Typography.h6 (Fluent.verbatim "SHACL Code"))
        , Ui.row
            [ Ui.width Ui.fill
            ]
            [ Ui.el
                [ Ui.width Ui.fill
                , Ui.scrollbarX
                ]
                (Ui.row
                    [ Ui.width Ui.fill
                    , Ui.height Ui.fill
                    , Ui.padding 16
                    ]
                    [ Ui.el
                        [ Ui.width Ui.fill
                        , Ui.height Ui.fill
                        ]
                        (Typography.body1Mono (Fluent.verbatim code))
                    ]
                )
            ]
        ]
