pages-markdown-cheatsheet--autolinks--heading = Autolinked references
pages-markdown-cheatsheet--blockquotes--heading = Blockquotes
pages-markdown-cheatsheet--code--heading = Code
pages-markdown-cheatsheet--emphasis--heading = Emphasis
pages-markdown-cheatsheet--heading = Markdown Guide
pages-markdown-cheatsheet--headings--heading = Headings
pages-markdown-cheatsheet--introduction =
    You can use the following Markdown elements to add formatting to all
    text entries and comment fields in protocols.
pages-markdown-cheatsheet--links--heading = Links
pages-markdown-cheatsheet--lists--heading = Lists
pages-markdown-cheatsheet--tables--heading = Tables
