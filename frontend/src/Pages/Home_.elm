module Pages.Home_ exposing (Model, Msg, page)

{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}

import Gen.Route as Route
import Page
import Request exposing (Request)
import Shared
import View


type alias Model =
    ()


type alias Msg =
    Never


page : Shared.Model -> Request -> Page.With Model Msg
page _ req =
    Page.element
        { init = ( (), Request.replaceRoute Route.Graph req )
        , update = \_ model -> ( model, Cmd.none )
        , view = \_ -> View.none
        , subscriptions = \_ -> Sub.none
        }
