pages-datasets--action--cancel = Cancel
pages-datasets--date-tooltip = { DATETIME($date, year: "numeric", month: "numeric", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric") }
pages-datasets--heading--drafts = Drafts
