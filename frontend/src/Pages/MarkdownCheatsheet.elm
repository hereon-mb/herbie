module Pages.MarkdownCheatsheet exposing (Model, Msg, page)

{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}

import Browser.Dom
import Element
    exposing
        ( Element
        , alignTop
        , column
        , el
        , fill
        , fillPortion
        , height
        , html
        , htmlAttribute
        , link
        , minimum
        , none
        , padding
        , paddingEach
        , paddingXY
        , paragraph
        , px
        , row
        , scrollbarY
        , spacing
        , text
        , textColumn
        , width
        , wrappedRow
        )
import Element.Background as Background
import Element.Border as Border
import Element.Extra
    exposing
        ( id
        , tabindex
        )
import Element.Font as Font
import Fluent exposing (Fluent, verbatim)
import Html
import Html.Attributes
import Page
import Request exposing (Request)
import Shared
import Task
import Ui.Atom.Divider
import Ui.Molecule.Breadcrumbs as Breadcrumbs
import Ui.Theme.Color exposing (surfaceContainerLow)
import Ui.Theme.Markdown
import Ui.Theme.Typography
import Url exposing (Url)


type alias Model =
    Url


type Msg
    = NoOp


page : Shared.Model -> Request -> Page.With Model Msg
page _ req =
    Page.element
        { init =
            ( req.url
            , req.url.fragment
                |> Maybe.map focusFragment
                |> Maybe.withDefault Cmd.none
            )
        , update = \_ model -> ( model, Cmd.none )
        , subscriptions = always Sub.none
        , view =
            always
                { title = "Markdown Cheatsheet"
                , element =
                    el
                        [ width fill
                        , height fill
                        , scrollbarY
                        ]
                        viewBody
                , overlays = []
                , breadcrumbs = Breadcrumbs.from [] (verbatim "Markdown Cheatsheet")
                , fabVisible = False
                , navigationRailVisible = False
                , loading = False
                }
        }


focusFragment : String -> Cmd Msg
focusFragment fragment =
    Task.attempt (always NoOp) (Browser.Dom.focus fragment)


viewBody : Element msg
viewBody =
    row
        [ width fill
        , height fill
        , paddingEach
            { top = 64
            , bottom = 128
            , left = 0
            , right = 0
            }
        ]
        [ paddingSide
        , textColumn
            [ width (fillPortion 8)
            , height fill
            , spacing 32
            ]
            [ Ui.Theme.Typography.h2 (t "heading")
            , paragraph []
                [ Ui.Theme.Typography.body1 (t "introduction") ]
            , viewToc
            , viewSection "headings" headings
            , viewSection "emphasis" emphasis
            , viewSection "lists" lists
            , viewSection "links" links
            , viewSection "autolinks" autolinks
            , viewSection "blockquotes" blockquotes
            , viewSection "tables" tables
            , viewSection "code" code
            ]
        , paddingSide
        ]


paddingSide : Element msg
paddingSide =
    el
        [ width (fillPortion 2)
        , height fill
        ]
        none


viewToc : Element msg
viewToc =
    column
        [ paddingXY 16 0
        , spacing 8
        ]
        (List.map toLink sections)


toLink : String -> Element msg
toLink name =
    row []
        [ el
            [ alignTop
            , width (px 24)
            ]
            (text "•")
        , Ui.Theme.Typography.link1
            { url = "#" ++ name
            , label = t (name ++ "--heading")
            }
        ]


viewSection : String -> String -> Element msg
viewSection name content =
    let
        toParagraph line =
            paragraph
                [ htmlAttribute
                    (Html.Attributes.style "line-height" "1.1")
                ]
                [ html
                    (Html.div
                        [ Html.Attributes.style "white-space" "pre-wrap" ]
                        [ Html.text line ]
                    )
                ]
    in
    column
        [ paddingEach
            { top = 16
            , bottom = 0
            , left = 0
            , right = 0
            }
        , spacing 16
        ]
        [ link []
            { url = "#" ++ name
            , label = Ui.Theme.Typography.h3 (t (name ++ "--heading"))
            }
        , Ui.Atom.Divider.horizontal
        , wrappedRow
            [ width fill
            , spacing 32
            , id name
            , tabindex -1
            ]
            [ column
                [ width (minimum 448 Element.fill)
                , alignTop
                , padding 16
                , spacing 12
                , Background.color surfaceContainerLow
                , Border.rounded 4
                , Font.family
                    [ Font.typeface "Source Code Pro" ]
                ]
                (List.map toParagraph (String.split "\n" content))
            , el
                [ width (minimum 448 Element.fill)
                , alignTop
                , paddingXY 0 16
                ]
                (Ui.Theme.Markdown.view content)
            ]
        ]



---- SECTIONS


sections : List String
sections =
    [ "headings"
    , "emphasis"
    , "lists"
    , "links"
    , "autolinks"
    , "blockquotes"
    , "tables"
    , "code"
    ]


headings : String
headings =
    """# Heading Level 1
## Heading Level 2
### Heading Level 3
#### Heading Level 4
##### Heading Level 5
###### Heading Level 6"""


emphasis : String
emphasis =
    "You can emphasize text with **bold**, *italic*.  Alternatively, you can use underscores (__bold__, _italic_) or nest **_italic_ within bold**.  It is also possible to ~~strikethrough~~ text."


lists : String
lists =
    """You can create ordered and unordered lists

1. First item
2. Another item
1. Consecutive numbers do not matter

- Unordered item
- Another unordered item"""


links : String
links =
    """There are several ways to create links:

[Inline link](https://www.hzg.de)

[Reference-style link][Some case-insensitive reference text]

[Reference-style link using numbers][1]


[some case-insensitive reference text]: https://www.hzg.de
[1]: https://www.hzg.de
"""


autolinks : String
autolinks =
    """References to materials, protocols, equipment and other entities in the journal are automatically shortened and converted to links:

Equipment: #nabertherm

Material: #210001"""


blockquotes : String
blockquotes =
    """You write quoted text by beginning the line with >:

> Some quoted text.
> This belongs to the same quote.

Quote break.

> A quotation can be a very long line. It will be automatically line-wrapped. There is no need to have additional >'s."""


tables : String
tables =
    """You can create (simple) tables like this:

| First Header | Second Header |
| ------------ | ------------- |
| Content Cell | Content Cell  |
| Content Cell | Content Cell  |

The pipes on the end are optional. You need at least 3 -'s to separate the header row from the content. Apart from this there is no need to align the cells perfectly:

Name | Symbol
--- | ---
Magnesium | Mg
Silver | Ag
Calcium | Ca

You can adjust the alignment within a column using :'s:

Left-aligned | Centered | Right-aligned
--- |:---:| ---:
Some | interesting | content
"""


code : String
code =
    """You can create inline code using `back-ticks`.  Blocks of (unformatted) code can be created by wrapping text in three back-ticks ```:

```
var s = "foo";
alert(s);
```

Alternatively, you can create code blocks by indenting the text by 4 spaces:

    s = "bar"
    print(s)"""



---- FLUENT


t : String -> Fluent
t key =
    Fluent.text ("pages-markdown-cheatsheet--" ++ key)
