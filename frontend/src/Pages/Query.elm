module Pages.Query exposing (Model, Msg, page)

{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}

import Action
import Api
import Browser.Navigation
import Context exposing (C)
import Dict exposing (Dict)
import Effect exposing (Effect)
import Element
    exposing
        ( Element
        , alignRight
        , centerY
        , clip
        , column
        , el
        , fill
        , fillPortion
        , height
        , none
        , padding
        , paddingEach
        , paddingXY
        , pointer
        , row
        , scrollbarY
        , scrollbars
        , shrink
        , spacing
        , width
        )
import Element.Events as Events
import Fluent exposing (Fluent, verbatim)
import Http
import Json.Decode as Decode
import Maybe.Extra as Maybe
import Ontology.Herbie exposing (herbie)
import Page
import Paginate exposing (PaginatedList)
import Rdf exposing (BlankNodeOrIriOrAnyLiteral, Iri)
import Rdf.Namespaces exposing (dcterms, owl, prov, rdfs, sh)
import Rdf.Namespaces.OWL as OWL
import Rdf.Namespaces.RDFS as RDFS
import Rdf.Sparql as Sparql
import Request exposing (Request)
import SPARQL
import SPARQL.Node exposing (Node, iri, path, var)
import Shared
import Ui.Atom.Button as Button exposing (button)
import Ui.Atom.ButtonIcon as ButtonIcon exposing (buttonIcon)
import Ui.Atom.Icon as Icon
import Ui.Atom.TextArea as TextArea exposing (textArea)
import Ui.Molecule.Breadcrumbs as Breadcrumbs
import Ui.Molecule.Table as Table exposing (table)
import Ui.Molecule.TableCell as TableCell exposing (TableCell)
import Ui.Theme.Card as Card exposing (card)
import Ui.Theme.Typography
    exposing
        ( body1
        , body2
        , body2Mono
        , h6
        )
import Url.Builder
import View exposing (View)


type Model
    = Loading DataLoading
    | Loaded DataLoaded


type alias DataLoading =
    {}


type alias DataLoaded =
    { query : String
    , running : Bool
    , rows : Maybe (PaginatedList (Dict String BlankNodeOrIriOrAnyLiteral))
    , raw : Maybe String
    }


page : Shared.Model -> Request -> Page.With Model Msg
page shared ({ key } as req) =
    let
        c =
            Shared.toContext shared
    in
    Page.advanced
        { init = init c req
        , update = update c key
        , subscriptions = subscriptions
        , view = view c
        }


init : C -> Request -> ( Model, Effect Msg )
init _ _ =
    ( Loaded
        { query = queryAllTriples.queryString
        , running = False
        , rows = Nothing
        , raw = Nothing
        }
    , Effect.none
    )


type Msg
    = UserPressedQuery String
    | UserChangedQuery String
    | UserPressedSearch
    | ApiReturnedResultsForSelect (Api.Response Sparql.Results)
    | ApiReturnedResultsForConstruct (Api.Response String)
    | UserPressedFirstPage
    | UserPressedPreviousPage
    | UserPressedNextPage
    | UserPressedLastPage


update : C -> Browser.Navigation.Key -> Msg -> Model -> ( Model, Effect Msg )
update c key msg model =
    case model of
        Loading data ->
            updateLoading c msg data

        Loaded data ->
            updateLoaded c key msg data
                |> Tuple.mapFirst Loaded


updateLoading : C -> Msg -> DataLoading -> ( Model, Effect Msg )
updateLoading _ _ data =
    ( Loading data, Effect.none )


updateLoaded : C -> Browser.Navigation.Key -> Msg -> DataLoaded -> ( DataLoaded, Effect Msg )
updateLoaded c _ msg data =
    case msg of
        UserPressedQuery query ->
            ( { data | query = query }
            , Effect.none
            )

        UserChangedQuery query ->
            ( { data | query = query }
            , Effect.none
            )

        UserPressedSearch ->
            ( { data
                | running = True
                , rows = Nothing
              }
            , if String.contains "SELECT" data.query then
                Api.request c
                    { method = "GET"
                    , headers = []
                    , pathSegments = [ "query" ]
                    , queryParameters =
                        [ Url.Builder.string "query" data.query
                        , Url.Builder.string "workspace" (Maybe.withDefault "" c.workspaceUuid)
                        ]
                    , body = Http.emptyBody
                    , expect =
                        Api.expectJson ApiReturnedResultsForSelect
                            (Decode.at [ "results", "bindings" ]
                                Sparql.resultsDecoder
                            )
                    , tracker = Nothing
                    }

              else if String.contains "CONSTRUCT" data.query then
                Api.request c
                    { method = "GET"
                    , headers = [ Http.header "Accept" "text/turtle" ]
                    , pathSegments = [ "construct" ]
                    , queryParameters =
                        [ Url.Builder.string "query" data.query
                        , Url.Builder.string "workspace" (Maybe.withDefault "" c.workspaceUuid)
                        ]
                    , body = Http.emptyBody
                    , expect = Api.expectString ApiReturnedResultsForConstruct
                    , tracker = Nothing
                    }

              else
                Effect.none
            )

        ApiReturnedResultsForSelect (Err error) ->
            ( { data | running = False }
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiReturnedResultsForSelect (Ok rows) ->
            ( { data
                | running = False
                , rows = Just (Paginate.fromList pageSize rows)
              }
            , Effect.none
            )

        ApiReturnedResultsForConstruct (Err error) ->
            ( { data | running = False }
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiReturnedResultsForConstruct (Ok raw) ->
            ( { data
                | running = False
                , raw = Just raw
              }
            , Effect.none
            )

        UserPressedFirstPage ->
            ( { data | rows = Maybe.map Paginate.first data.rows }
            , Effect.none
            )

        UserPressedPreviousPage ->
            ( { data | rows = Maybe.map Paginate.prev data.rows }
            , Effect.none
            )

        UserPressedNextPage ->
            ( { data | rows = Maybe.map Paginate.next data.rows }
            , Effect.none
            )

        UserPressedLastPage ->
            ( { data | rows = Maybe.map Paginate.last data.rows }
            , Effect.none
            )


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        Loading _ ->
            Sub.none

        Loaded _ ->
            Sub.none



-- VIEW


view : C -> Model -> View Msg
view c model =
    { title = "Query"
    , element =
        case model of
            Loading _ ->
                none

            Loaded data ->
                viewLoaded c data
    , overlays = []
    , breadcrumbs = Breadcrumbs.from [] (verbatim "Query")
    , fabVisible = False
    , navigationRailVisible = True
    , loading =
        case model of
            Loading _ ->
                True

            Loaded data ->
                data.running
    }


viewLoaded : C -> DataLoaded -> Element Msg
viewLoaded c data =
    [ [ [ data.query
            |> textArea UserChangedQuery
            |> TextArea.withLabel (verbatim "SPARQL query")
            |> TextArea.toElement
        , viewActions data
        ]
            |> column
                [ width (fillPortion 2)
                , height fill
                , padding 16
                , spacing 16
                ]
      , viewQueries
      ]
        |> row
            [ width fill
            , height fill
            , clip
            ]
    , (case ( data.rows, data.raw ) of
        ( Just rows, _ ) ->
            [ viewTable c rows
            , viewPagination rows
            ]
                |> column
                    [ width fill
                    , height fill
                    ]

        ( _, Just raw ) ->
            raw
                |> verbatim
                |> body2Mono
                |> el
                    [ width fill
                    , height fill
                    , padding 16
                    ]
                |> List.singleton
                -- FIXME wrap in a row in order for the padding on the right to appear
                |> row
                    [ width fill
                    , height fill
                    ]
                |> el
                    [ width fill
                    , height fill
                    , scrollbars
                    ]
                |> List.singleton
                |> column
                    [ width fill
                    , height fill
                    ]

        ( Nothing, Nothing ) ->
            none
      )
        |> el
            [ width fill
            , height (fillPortion 2)
            ]
    ]
        |> column
            [ width fill
            , height fill
            ]


viewActions : DataLoaded -> Element Msg
viewActions data =
    [ "Search"
        |> verbatim
        |> button UserPressedSearch
        |> Button.view
    , if data.running then
        "Query running ..."
            |> verbatim
            |> body1

      else if data.rows /= Nothing || data.raw /= Nothing then
        "Query running ... finished!"
            |> verbatim
            |> body1

      else
        none
    ]
        |> row
            [ width fill
            , spacing 16
            ]


viewQueries : Element Msg
viewQueries =
    queries
        |> List.map viewQuery
        |> column
            [ width fill
            , height fill
            , spacing 16
            , paddingEach
                { top = 24
                , bottom = 16
                , left = 16
                , right = 16
                }
            , scrollbarY
            ]


viewQuery : Query -> Element Msg
viewQuery query =
    [ query.title
        |> verbatim
        |> h6
    , query.queryString
        |> verbatim
        |> body2Mono
    ]
        |> column
            [ width fill
            , spacing 8
            , clip
            ]
        |> card
        |> Card.toElement
        |> el
            [ width fill
            , pointer
            , Events.onClick (UserPressedQuery query.queryString)
            ]


viewTable : C -> PaginatedList (Dict String BlankNodeOrIriOrAnyLiteral) -> Element Msg
viewTable c rows =
    let
        keys =
            case Paginate.page rows of
                [] ->
                    []

                row :: _ ->
                    row
                        |> Dict.keys
    in
    { data = Paginate.page rows
    , columns =
        keys
            |> List.map
                (\key ->
                    { header =
                        key
                            |> verbatim
                            |> TableCell.textBold
                    , width = shrink
                    , view =
                        \value ->
                            value
                                |> Dict.get key
                                |> Maybe.map termToTableCell
                                |> Maybe.withDefault TableCell.empty
                    }
                )
    }
        |> table
        |> Table.withoutScrolling
        |> Table.view c
        |> el
            [ width fill
            , height fill
            , padding 16
            ]
        |> List.singleton
        -- FIXME wrap in a row in order for the padding on the right to appear
        |> row
            [ width fill
            , height fill
            ]
        |> el
            [ width fill
            , height fill
            , scrollbars
            ]


termToTableCell : BlankNodeOrIriOrAnyLiteral -> TableCell msg
termToTableCell term =
    [ Rdf.toIri term
        |> Maybe.map Rdf.toUrl
        |> Maybe.map
            (\url ->
                TableCell.link
                    { url = url
                    , label = verbatim url
                    }
            )
    , Rdf.toString term
        |> Maybe.map (verbatim >> TableCell.text)
    , Rdf.toLangString term
        |> Maybe.map
            (\( languageTag, value ) ->
                value
                    |> verbatim
                    |> TableCell.text
                    |> TableCell.withInfo (verbatim languageTag)
            )
    ]
        |> Maybe.orList
        |> Maybe.withDefault
            (term
                |> Rdf.serializeNode
                |> verbatim
                |> TableCell.text
            )


viewPagination : PaginatedList a -> Element Msg
viewPagination materials =
    row
        [ paddingXY 16 8
        , width fill
        , spacing 64
        ]
        [ row
            [ alignRight
            , centerY
            , spacing 32
            ]
            [ body2
                (tr
                    [ Fluent.int "count" (Paginate.length materials)
                    , Fluent.int "firstInPage"
                        (1
                            + (Paginate.currentPage materials - 1)
                            * Paginate.itemsPerPage materials
                        )
                    , Fluent.int "lastInPage"
                        (min
                            (Paginate.currentPage materials
                                * Paginate.itemsPerPage materials
                            )
                            (Paginate.length materials)
                        )
                    ]
                    "count-info"
                )
            , row []
                [ buttonIcon UserPressedFirstPage Icon.FirstPage
                    |> ButtonIcon.withEnabled (not (Paginate.isFirst materials))
                    |> ButtonIcon.toElement
                , buttonIcon UserPressedPreviousPage Icon.ChevronLeft
                    |> ButtonIcon.withEnabled (not (Paginate.isFirst materials))
                    |> ButtonIcon.toElement
                , buttonIcon UserPressedNextPage Icon.ChevronRight
                    |> ButtonIcon.withEnabled (not (Paginate.isLast materials))
                    |> ButtonIcon.toElement
                , buttonIcon UserPressedLastPage Icon.LastPage
                    |> ButtonIcon.withEnabled (not (Paginate.isLast materials))
                    |> ButtonIcon.toElement
                ]
            ]
        ]


tr : List Fluent.Argument -> String -> Fluent
tr args key =
    Fluent.textWith args ("pages-query--" ++ key)


pageSize : Int
pageSize =
    25



-- QUERIES


type alias Query =
    { title : String
    , queryString : String
    }


queries : List Query
queries =
    [ queryAllTriples
    , queryAllNodeShapes
    , queryAllSubClassesOfMaterial
    , queryAllAlloyingElements
    , queryAllDocuments
    , queryAllPublishedVersions
    , queryAllPublishedMaterials
    , queryAllPublishedCastMaterials
    ]


queryAllTriples : Query
queryAllTriples =
    { title = "All triples"
    , queryString =
        [ SPARQL.bgp [ ( var "subject", var "predicate", var "object" ) ] ]
            |> SPARQL.select [ "subject", "predicate", "object" ]
            |> SPARQL.toString
    }


queryAllNodeShapes : Query
queryAllNodeShapes =
    { title = "All sh:NodeShape's"
    , queryString =
        [ SPARQL.bgp
            [ ( var "subject", a, iri (sh "NodeShape") )
            ]
        ]
            |> SPARQL.select [ "subject" ]
            |> SPARQL.withPrefix "owl" (owl "")
            |> SPARQL.withPrefix "sh" (sh "")
            |> SPARQL.toString
    }


queryAllSubClassesOfMaterial : Query
queryAllSubClassesOfMaterial =
    { title = "All sub classes of herbie:Material"
    , queryString =
        [ SPARQL.bgp
            [ ( var "subClass", iri RDFS.subClassOf, iri (herbie "Material") )
            ]
        ]
            |> SPARQL.select [ "subClass" ]
            |> SPARQL.withPrefix "herbie" (herbie "")
            |> SPARQL.withPrefix "owl" (owl "")
            |> SPARQL.withPrefix "sh" (sh "")
            |> SPARQL.toString
    }


queryAllDocuments : Query
queryAllDocuments =
    { title = "All herbie:Document's"
    , queryString =
        [ SPARQL.bgp
            [ ( var "document", a, iri (herbie "Document") )
            , ( var "document", path [ prov "wasAttributedTo", RDFS.label ], var "author" )
            ]
        ]
            |> SPARQL.select [ "document", "author" ]
            |> SPARQL.withPrefix "herbie" (herbie "")
            |> SPARQL.withPrefix "prov" (prov "")
            |> SPARQL.withPrefix "rdfs" (rdfs "")
            |> SPARQL.withPrefix "dcterms" (dcterms "")
            |> SPARQL.toString
    }


queryAllPublishedVersions : Query
queryAllPublishedVersions =
    { title = "All published herbie:DocumentVersion's"
    , queryString =
        [ SPARQL.bgp
            [ ( var "published", a, iri (herbie "Document") )
            , ( var "version", a, iri (herbie "DocumentVersion") )
            , ( var "version", iri (dcterms "conformsTo"), var "conformsTo" )
            ]
        , SPARQL.optional
            [ SPARQL.graph (var "version")
                [ ( var "version", iri (prov "invalidatedAt"), var "date" ) ]
            ]
        , SPARQL.filter "!bound(?date)"
        ]
            |> SPARQL.select [ "published", "version", "conformsTo" ]
            |> SPARQL.withPrefix "herbie" (herbie "")
            |> SPARQL.withPrefix "prov" (prov "")
            |> SPARQL.withPrefix "dcterms" (dcterms "")
            |> SPARQL.toString
    }


queryAllPublishedMaterials : Query
queryAllPublishedMaterials =
    { title = "All published herbie:Material's"
    , queryString =
        [ SPARQL.graph (var "published")
            [ ( var "published", a, iri (herbie "Document") )
            , ( var "version", a, iri (herbie "DocumentVersion") )
            , ( var "version", iri (dcterms "conformsTo"), var "conformsTo" )
            ]
        , SPARQL.graph (var "version")
            [ ( var "material", a, var "classMaterial" )
            , ( var "material", iri (herbie "publicId"), var "publicId" )
            , ( var "material", iri RDFS.label, var "label" )
            ]
        , SPARQL.optional
            [ SPARQL.graph (var "version")
                [ ( var "version", iri (prov "invalidatedAt"), var "date" )
                ]
            ]
        , SPARQL.filter "!bound(?date)"
        , SPARQL.subSelectDistinct [ "classMaterial" ]
            [ SPARQL.graph (var "graph")
                [ ( var "classMaterial", iri RDFS.subClassOf, iri (herbie "Material") )
                , ( var "graph", a, iri OWL.ontology )
                ]
            ]
        ]
            |> SPARQL.select [ "material", "published", "publicId", "label" ]
            |> SPARQL.withPrefix "herbie" (herbie "")
            |> SPARQL.withPrefix "owl" (owl "")
            |> SPARQL.withPrefix "prov" (prov "")
            |> SPARQL.withPrefix "rdfs" (rdfs "")
            |> SPARQL.withPrefix "dcterms" (dcterms "")
            |> SPARQL.toString
    }


queryAllPublishedCastMaterials : Query
queryAllPublishedCastMaterials =
    { title = "All published cast:Material's"
    , queryString =
        [ SPARQL.graph (var "published")
            [ ( var "published", a, iri (herbie "Document") )
            , ( var "version", a, iri (herbie "DocumentVersion") )
            , ( var "version", iri (dcterms "conformsTo"), var "conformsTo" )
            ]
        , SPARQL.graph (var "version")
            [ ( var "material", a, iri (cast "Material") )
            , ( var "material", iri (herbie "publicId"), var "publicId" )
            , ( var "material", iri RDFS.label, var "label" )
            ]
        , SPARQL.optional
            [ SPARQL.graph (var "version")
                [ ( var "version", iri (prov "invalidatedAt"), var "date" ) ]
            ]
        , SPARQL.filter "!bound(?date)"
        ]
            |> SPARQL.select [ "material", "published", "publicId", "label" ]
            |> SPARQL.withPrefix "herbie" (herbie "")
            |> SPARQL.withPrefix "cast" (cast "")
            |> SPARQL.withPrefix "owl" (owl "")
            |> SPARQL.withPrefix "prov" (prov "")
            |> SPARQL.withPrefix "rdfs" (rdfs "")
            |> SPARQL.withPrefix "dcterms" (dcterms "")
            |> SPARQL.toString
    }


queryAllAlloyingElements : Query
queryAllAlloyingElements =
    { title = "All mb:AlloyingElement's"
    , queryString =
        [ SPARQL.bgp
            [ ( var "alloyingElement", a, iri (mb "AlloyingElement") )
            , ( var "alloyingElement", iri RDFS.label, var "label" )
            ]
        ]
            |> SPARQL.select [ "alloyingElement", "label" ]
            |> SPARQL.withPrefix "herbie" (herbie "")
            |> SPARQL.withPrefix "mb" (mb "")
            |> SPARQL.withPrefix "owl" (owl "")
            |> SPARQL.withPrefix "prov" (prov "")
            |> SPARQL.withPrefix "rdfs" (rdfs "")
            |> SPARQL.withPrefix "dcterms" (dcterms "")
            |> SPARQL.toString
    }


a : Node
a =
    iri Rdf.Namespaces.a


cast : String -> Iri
cast name =
    Rdf.iri ("http://purls.helmholtz-metadaten.de/herbie/mb/mbf/cast/#" ++ name)


mb : String -> Iri
mb name =
    Rdf.iri ("http://purls.helmholtz-metadaten.de/herbie/mb/#" ++ name)
