pages-access-tokens--action--generate-access-token = Generiere Access Token
pages-access-tokens--action--generate-new-access-token = Generiere ein neues Access Token
pages-access-tokens--caption--access-token = Access Token
pages-access-tokens--caption--expiry = Gültig bis
pages-access-tokens--paragraph--no-access-token--info = Falls du auf die Herbie API außerhalb deines Browsers zugreifen möchtest, kannst du hier ein Access Token erzeugen.
pages-access-tokens--paragraph--usage-info = Du kannst dieses Token nutzen, um dich gegenüber der Herbie API zu authentifizieren, indem du den folgenden HTTP Header angibst:
