module Pages.NotFound exposing (page)

{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}

import Element
    exposing
        ( centerX
        , centerY
        , el
        )
import Fluent exposing (Fluent)
import Page exposing (Page)
import Request exposing (Request)
import Shared
import Ui.Molecule.Breadcrumbs as Breadcrumbs
import Ui.Theme.Typography
import View exposing (View)


page : Shared.Model -> Request -> Page
page _ _ =
    Page.static { view = view }


view : View msg
view =
    { title = "404 - Page not found"
    , element =
        el
            [ centerX
            , centerY
            ]
            (Ui.Theme.Typography.h3 (t "heading"))
    , overlays = []
    , breadcrumbs = Breadcrumbs.empty
    , fabVisible = False
    , navigationRailVisible = False
    , loading = False
    }


t : String -> Fluent
t key =
    Fluent.text ("pages-not-found--" ++ key)
