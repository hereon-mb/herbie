module Pages.Settings exposing (Model, Msg, page)

{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}

import Action
import Api
import Api.Auth as Auth exposing (AccessToken)
import Context exposing (C)
import Effect exposing (Effect)
import Element
    exposing
        ( Element
        , centerX
        , column
        , el
        , fill
        , height
        , html
        , maximum
        , none
        , padding
        , paddingEach
        , paddingXY
        , paragraph
        , row
        , spacing
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Fluent
    exposing
        ( Fluent
        , verbatim
        )
import Html
import Html.Attributes
import Http
import Page
import Request exposing (Request)
import Shared
import Ui.Atom.Button as Button exposing (button)
import Ui.Atom.Checkbox as Checkbox exposing (checkbox)
import Ui.Atom.Icon as Icon
import Ui.Atom.Labelled as Labelled
import Ui.Molecule.Breadcrumbs as Breadcrumbs
import Ui.Theme.Color
    exposing
        ( onSurface
        , surfaceContainerLow
        )
import Ui.Theme.Dimensions exposing (paneWidthMax)
import Ui.Theme.Typography
    exposing
        ( body1
        , h4
        )
import View exposing (View)


page : Shared.Model -> Request -> Page.With Model Msg
page shared _ =
    Page.advanced
        { init = init
        , update = update (Shared.toContext shared)
        , subscriptions = subscriptions
        , view = view (Shared.toContext shared)
        }


type alias Model =
    { token : Maybe AccessToken
    }


init : ( Model, Effect Msg )
init =
    ( { token = Nothing }
    , Effect.none
    )


type Msg
    = UserPressedRenderIrisInDropdownMenus Bool
    | UserPressedGenerateAccessToken
    | ApiReturnedAccessToken (Api.Response AccessToken)
    | UserPressedClearCache
    | UserPressedClearRdfStore
    | ApiClearedRdfStore (Api.Response ())
    | UserPressedSyncRdfStore
    | UserPressedSyncRdfStoreAuthors
    | UserPressedSyncRdfStoreDepartments
    | ApiSyncedRdfStore (Api.Response ())


update : C -> Msg -> Model -> ( Model, Effect Msg )
update c msg model =
    case msg of
        UserPressedRenderIrisInDropdownMenus renderIrisInDropdownMenus ->
            let
                preferences =
                    c.author.preferencesAuthor
            in
            ( model
            , Effect.fromAction
                (Action.UserIntentsToUpdatePreferences
                    { preferences | renderIrisInDropdownMenus = renderIrisInDropdownMenus }
                )
            )

        UserPressedGenerateAccessToken ->
            ( model
            , Auth.login c ApiReturnedAccessToken
            )

        ApiReturnedAccessToken (Err error) ->
            ( model
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiReturnedAccessToken (Ok token) ->
            ( { model | token = Just token }
            , Effect.fromAction Action.RequestAuthorDetails
            )

        UserPressedClearCache ->
            ( model
            , Action.UserIntentsToClearStore
                |> Effect.fromAction
            )

        UserPressedClearRdfStore ->
            ( model
            , Api.request c
                { method = "POST"
                , headers = []
                , pathSegments = [ "triplestore", "clear" ]
                , queryParameters = []
                , body = Http.emptyBody
                , expect = Api.expectWhatever ApiClearedRdfStore
                , tracker = Nothing
                }
            )

        ApiClearedRdfStore (Err error) ->
            ( model
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiClearedRdfStore (Ok ()) ->
            ( model
            , { message = verbatim "Cleared RDF store" }
                |> Action.AddToastExpireIn 3000
                |> Effect.fromAction
            )

        UserPressedSyncRdfStore ->
            ( model
            , syncRdfStore c []
            )

        UserPressedSyncRdfStoreAuthors ->
            ( model
            , syncRdfStore c [ "authors" ]
            )

        UserPressedSyncRdfStoreDepartments ->
            ( model
            , syncRdfStore c [ "departments" ]
            )

        ApiSyncedRdfStore (Err error) ->
            ( model
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiSyncedRdfStore (Ok ()) ->
            ( model
            , { message = verbatim "Started synchronization of RDF store" }
                |> Action.AddToastExpireIn 3000
                |> Effect.fromAction
            )


syncRdfStore : C -> List String -> Effect Msg
syncRdfStore c pathSegments =
    Api.request c
        { method = "POST"
        , headers = []
        , pathSegments = "triplestore" :: "sync" :: pathSegments
        , queryParameters = []
        , body = Http.emptyBody
        , expect = Api.expectWhatever ApiSyncedRdfStore
        , tracker = Nothing
        }


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


view : C -> Model -> View Msg
view c model =
    { title = "Settings"
    , element =
        [ viewPreferences c
        , viewAccessTokens model
        , viewStore
        , if c.author.permissions.superuser then
            viewAdministration

          else
            none
        ]
            |> column
                [ width (maximum paneWidthMax fill)
                , height fill
                , spacing 32
                , padding 32
                , centerX
                ]
    , overlays = []
    , breadcrumbs = Breadcrumbs.from [] (verbatim "Settings")
    , fabVisible = False
    , navigationRailVisible = False
    , loading = False
    }


viewPreferences : C -> Element Msg
viewPreferences c =
    column
        [ width fill
        , spacing 16
        ]
        [ row
            [ width fill
            , spacing 16
            ]
            [ Icon.viewLarge Icon.Settings
            , h4 (verbatim "Preferences")
            ]
        , column
            [ width fill
            , paddingXY 32 0
            , spacing 16
            ]
            [ c.author.preferencesAuthor.renderIrisInDropdownMenus
                |> checkbox UserPressedRenderIrisInDropdownMenus (verbatim "Render IRI's in dropdown menus.")
                |> Checkbox.toElement
            ]
        ]


viewAccessTokens : Model -> Element Msg
viewAccessTokens model =
    [ [ Icon.GeneratingTokens
            |> Icon.viewLarge
      , "Access tokens"
            |> verbatim
            |> h4
      ]
        |> row
            [ width fill
            , spacing 16
            ]
    , [ case model.token of
            Nothing ->
                t "paragraph--no-access-token--info"
                    |> body1
                    |> List.singleton
                    |> paragraph
                        [ width (maximum 480 fill)
                        , Font.color onSurface
                        ]

            Just token ->
                let
                    header =
                        "Authorization: Token " ++ token.token
                in
                [ [ Labelled.string (t "caption--access-token") token.token
                  , Labelled.posix (t "caption--expiry") token.expiry
                  ]
                    |> column
                        [ width fill
                        , spacing 16
                        ]
                , [ t "paragraph--usage-info"
                        |> body1
                        |> List.singleton
                        |> paragraph
                            [ width fill
                            , Font.color onSurface
                            ]
                  , [ Html.text header ]
                        |> Html.div
                            [ Html.Attributes.style "white-space" "pre"
                            , Html.Attributes.style "line-height" "1.3"
                            ]
                        |> html
                        |> el [ padding 16 ]
                        |> el
                            [ width fill
                            , Background.color surfaceContainerLow
                            , Border.rounded 4
                            , Font.size 18
                            , Font.family
                                [ Font.typeface "Source Code Pro" ]
                            ]
                  ]
                    |> column
                        [ width fill
                        , spacing 16
                        ]
                ]
                    |> column [ spacing 16 ]
      , if model.token == Nothing then
            t "action--generate-access-token"
                |> button UserPressedGenerateAccessToken
                |> Button.withFormat Button.Outlined
                |> Button.view

        else
            t "action--generate-new-access-token"
                |> button UserPressedGenerateAccessToken
                |> Button.withFormat Button.Outlined
                |> Button.view
      ]
        |> column
            [ width fill
            , paddingEach
                { top = 0
                , bottom = 0
                , left = 48
                , right = 0
                }
            , spacing 16
            ]
    ]
        |> column
            [ width fill
            , spacing 16
            ]


viewStore : Element Msg
viewStore =
    [ [ Icon.Storage
            |> Icon.viewLarge
      , "Quadstore"
            |> verbatim
            |> h4
      ]
        |> row
            [ width fill
            , spacing 16
            ]
    , [ "Clear cache"
            |> verbatim
            |> button UserPressedClearCache
            |> Button.withLeadingIcon Icon.Autorenew
            |> Button.withFormat Button.Outlined
            |> Button.view
      ]
        |> column
            [ width fill
            , paddingEach
                { top = 0
                , bottom = 0
                , left = 48
                , right = 0
                }
            , spacing 16
            ]
    ]
        |> column
            [ width fill
            , spacing 16
            ]


viewAdministration : Element Msg
viewAdministration =
    [ [ Icon.AdminPanelSettings
            |> Icon.viewLarge
      , "Administration"
            |> verbatim
            |> h4
      ]
        |> row
            [ width fill
            , spacing 16
            ]
    , [ "Clear RDF Store"
            |> verbatim
            |> button UserPressedClearRdfStore
            |> Button.withLeadingIcon Icon.Delete
            |> Button.withFormat Button.Outlined
            |> Button.view
      , "Sync RDF Store"
            |> verbatim
            |> button UserPressedSyncRdfStore
            |> Button.withLeadingIcon Icon.Autorenew
            |> Button.withFormat Button.Outlined
            |> Button.view
      , "Sync RDF Store (only authors)"
            |> verbatim
            |> button UserPressedSyncRdfStoreAuthors
            |> Button.withLeadingIcon Icon.Autorenew
            |> Button.withFormat Button.Outlined
            |> Button.view
      , "Sync RDF Store (only departments)"
            |> verbatim
            |> button UserPressedSyncRdfStoreDepartments
            |> Button.withLeadingIcon Icon.Autorenew
            |> Button.withFormat Button.Outlined
            |> Button.view
      ]
        |> column
            [ width fill
            , paddingEach
                { top = 0
                , bottom = 0
                , left = 48
                , right = 0
                }
            , spacing 16
            ]
    ]
        |> column
            [ width fill
            , spacing 16
            ]


t : String -> Fluent
t key =
    Fluent.text ("pages-settings--" ++ key)
