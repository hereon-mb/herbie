module Pages.Construct.Slug_ exposing
    ( Model
    , Msg
    , Params
    , page
    )

import Action
import Api
import Context exposing (C)
import Effect exposing (Effect)
import Element
    exposing
        ( Element
        , column
        , el
        , fill
        , height
        , none
        , padding
        , scrollbarY
        , width
        )
import Fluent exposing (verbatim)
import Maybe.Pipeline as Maybe
import Page
import Rdf exposing (Iri)
import Rdf.Format as Format exposing (Format)
import Rdf.Graph exposing (Graph)
import Request
import Shared
import Ui.Molecule.Breadcrumbs as Breadcrumbs
import Ui.Organism.GraphSerializations as GraphSerializations
import Ui.Theme.Typography exposing (h6)
import View exposing (View)


type alias Params =
    { slug : String }


type Model
    = Loading DataLoading
    | Failed DataFailed
    | Loaded DataLoaded


type alias DataLoading =
    { iri : Iri
    , graph : Maybe Graph
    , turtle : Maybe String
    , nTriples : Maybe String
    , jsonLd : Maybe String
    }


type alias DataFailed =
    { iri : Iri
    }


type alias DataLoaded =
    { iri : Iri
    , graph : Graph
    , turtle : String
    , nTriples : String
    , jsonLd : String
    , formatVisible : Format
    }


page : Shared.Model -> Request.With Params -> Page.With Model Msg
page shared request =
    let
        c =
            Shared.toContext shared
    in
    Page.advanced
        { init = init c request.params
        , update = update c
        , subscriptions = subscriptions
        , view = view c request.params
        }


init : C -> Params -> ( Model, Effect Msg )
init c params =
    let
        iri =
            Api.iriHttp c [ "construct", params.slug ]
    in
    ( Loading
        { iri = iri
        , graph = Nothing
        , turtle = Nothing
        , nTriples = Nothing
        , jsonLd = Nothing
        }
    , iri
        |> Action.GetGraph
            { store = False
            , reload = True
            , closure = False
            }
            ApiReturnedGraph
        |> Effect.fromAction
    )


type Msg
    = ApiReturnedGraph (Api.Response Graph)
    | ApiReturnedGraphTurtle String
    | ApiReturnedGraphNTriples String
    | ApiReturnedGraphJsonLd String
    | UserSelectedFormatVisible Format


update : C -> Msg -> Model -> ( Model, Effect Msg )
update c msg model =
    case model of
        Loading data ->
            updateLoading c msg data

        Failed data ->
            updateFailed msg data

        Loaded data ->
            updateLoaded msg data


updateLoading : C -> Msg -> DataLoading -> ( Model, Effect Msg )
updateLoading c msg data =
    case msg of
        ApiReturnedGraph (Err error) ->
            ( Failed { iri = data.iri }
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiReturnedGraph (Ok graph) ->
            ( Loading
                { data
                    | iri = data.iri
                    , graph = Just graph
                }
            , [ data.iri
                    |> Action.GetGraphTurtle ApiReturnedGraphTurtle
                    |> Effect.fromAction
              , data.iri
                    |> Action.GetGraphNTriples ApiReturnedGraphNTriples
                    |> Effect.fromAction
              , data.iri
                    |> Action.GetGraphJsonLd ApiReturnedGraphJsonLd
                    |> Effect.fromAction
              ]
                |> Effect.batch
            )

        ApiReturnedGraphTurtle turtle ->
            { data | turtle = Just turtle }
                |> initLoaded

        ApiReturnedGraphNTriples nTriples ->
            { data | nTriples = Just nTriples }
                |> initLoaded

        ApiReturnedGraphJsonLd jsonLd ->
            { data | jsonLd = Just jsonLd }
                |> initLoaded

        UserSelectedFormatVisible _ ->
            ( Loading data, Effect.none )


initLoaded : DataLoading -> ( Model, Effect Msg )
initLoaded data =
    Just DataLoaded
        |> Maybe.hardcoded data.iri
        |> Maybe.required data.graph
        |> Maybe.required data.turtle
        |> Maybe.required data.nTriples
        |> Maybe.required data.jsonLd
        |> Maybe.hardcoded Format.Turtle
        |> Maybe.map (\loaded -> ( Loaded loaded, Effect.none ))
        |> Maybe.withDefault ( Loading data, Effect.none )


updateFailed : Msg -> DataFailed -> ( Model, Effect Msg )
updateFailed _ data =
    ( Failed data, Effect.none )


updateLoaded : Msg -> DataLoaded -> ( Model, Effect Msg )
updateLoaded msg data =
    case msg of
        ApiReturnedGraph _ ->
            ( Loaded data, Effect.none )

        ApiReturnedGraphTurtle _ ->
            ( Loaded data, Effect.none )

        ApiReturnedGraphJsonLd _ ->
            ( Loaded data, Effect.none )

        ApiReturnedGraphNTriples _ ->
            ( Loaded data, Effect.none )

        UserSelectedFormatVisible format ->
            ( Loaded { data | formatVisible = format }
            , Effect.none
            )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


view : C -> Params -> Model -> View Msg
view _ params model =
    let
        iri =
            case model of
                Loading data ->
                    data.iri

                Failed data ->
                    data.iri

                Loaded data ->
                    data.iri
    in
    { title = Rdf.toUrl iri
    , element = element model
    , overlays = []
    , breadcrumbs =
        Breadcrumbs.from
            [ Breadcrumbs.static (verbatim "Constructed graphs") ]
            (verbatim params.slug)
    , fabVisible = False
    , navigationRailVisible = True
    , loading =
        case model of
            Loading _ ->
                True

            Failed _ ->
                False

            Loaded _ ->
                False
    }


element : Model -> Element Msg
element model =
    case model of
        Loading _ ->
            none

        Failed _ ->
            none

        Loaded data ->
            [ data.iri
                |> Rdf.toUrl
                |> verbatim
                |> h6
                |> el
                    [ padding 16
                    ]
            , viewGraph data
                |> el
                    [ width fill
                    , height fill
                    , scrollbarY
                    ]
                |> List.singleton
                |> column
                    [ width fill
                    , height fill
                    ]
            ]
                |> column
                    [ width fill
                    , height fill
                    ]


viewGraph : DataLoaded -> Element Msg
viewGraph data =
    GraphSerializations.view
        { userSelectedFormatVisible = UserSelectedFormatVisible }
        { turtle = data.turtle
        , nTriples = data.nTriples
        , jsonLd = data.jsonLd
        , formatVisible = data.formatVisible
        }
