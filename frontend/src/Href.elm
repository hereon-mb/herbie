{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Href exposing
    ( fromRoute, fromRouteWith
    , C
    )

{-|

@docs fromRoute, fromRouteWith
@docs C

-}

import Dict exposing (Dict)
import Gen.Route as Route exposing (Route)
import Url


type alias C rest =
    { rest
        | query : Dict String String
        , workspaceUuid : Maybe String
    }


fromRoute : C r -> Route -> String
fromRoute c r =
    fromRouteWith c Dict.empty r


fromRouteWith : C r -> Dict String String -> Route -> String
fromRouteWith c query route =
    let
        queryInitial =
            case Dict.get "workspace" c.query of
                Nothing ->
                    case c.workspaceUuid of
                        Nothing ->
                            Dict.empty

                        Just uuid ->
                            case route of
                                Route.Datasets__Uuid_ _ ->
                                    Dict.empty

                                Route.Datasets__Uuid___Draft _ ->
                                    Dict.empty

                                Route.Datasets__Uuid___Versions__Index_ _ ->
                                    Dict.empty

                                Route.Workspaces__Uuid_ _ ->
                                    Dict.empty

                                _ ->
                                    Dict.singleton "workspace" uuid

                Just uuid ->
                    case route of
                        Route.Workspaces__Uuid_ _ ->
                            Dict.empty

                        _ ->
                            Dict.singleton "workspace" uuid
    in
    href route (Dict.union query queryInitial)


href : Route -> Dict String String -> String
href route query =
    if Dict.isEmpty query then
        Route.toHref route

    else
        Route.toHref route ++ "?" ++ queryStringFromDict query


queryStringFromDict : Dict String String -> String
queryStringFromDict query =
    let
        toParam ( field, value ) =
            if value == "" then
                field

            else
                field ++ "=" ++ Url.percentEncode value
    in
    query
        |> Dict.toList
        |> List.map toParam
        |> String.join "&"
