module Bool.Apply exposing (apply)


apply : (a -> a) -> Bool -> a -> a
apply f p =
    if p then
        f

    else
        identity
