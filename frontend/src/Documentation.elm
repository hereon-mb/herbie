module Documentation exposing (all, tests)

import Documentation.Combinations
import Documentation.InputCheckbox
import Documentation.InputCollection
import Documentation.InputConstant
import Documentation.InputDateTime
import Documentation.InputFileUpload
import Documentation.InputGrid
import Documentation.InputGroup
import Documentation.InputNumber
import Documentation.InputSchedule
import Documentation.InputSelectMany
import Documentation.InputSelectOne
import Documentation.InputSingleNested
import Documentation.InputText
import Documentation.InputVariants
import List.NonEmpty as NonEmpty
import Shacl.Documentation as Documentation exposing (Documentation)
import Test exposing (Test)
import Tree exposing (Tree, tree)


all : ( Tree Documentation, List (Tree Documentation) )
all =
    ( tree
        (Documentation.fromInfo
            { headingFull = "Simple Fields"
            , headingShort = "Simple"
            , description = ""
            }
        )
        [ Documentation.InputCheckbox.all
        , Documentation.InputText.all
        , Documentation.InputNumber.all
        , Documentation.InputDateTime.all
        , Documentation.InputSelectOne.all
        , Documentation.InputSelectMany.all
        , Documentation.InputFileUpload.all
        , Documentation.InputConstant.all
        ]
    , [ tree
            (Documentation.fromInfo
                { headingFull = "Nested Fields"
                , headingShort = "Nested"
                , description = ""
                }
            )
            [ Documentation.InputCollection.all
            , Documentation.InputSchedule.all
            , Documentation.InputGrid.all
            , Documentation.InputSingleNested.all
            , Documentation.InputVariants.all
            ]
      , tree
            (Documentation.fromInfo
                { headingFull = "Visual"
                , headingShort = "Visual"
                , description = ""
                }
            )
            [ Documentation.InputGroup.all
            ]
      , Documentation.Combinations.all
      ]
    )


tests : Test
tests =
    Test.describe "Documentation"
        (List.filterMap testFromTree (NonEmpty.toList all))


testFromTree : Tree Documentation -> Maybe Test
testFromTree =
    Tree.restructure identity
        (\documentation children ->
            let
                testsNested =
                    Documentation.tests documentation ++ List.filterMap identity children
            in
            if List.isEmpty testsNested then
                Nothing

            else
                Just
                    (Test.describe
                        (Documentation.toInfo documentation).headingShort
                        testsNested
                    )
        )
