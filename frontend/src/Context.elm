{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Context exposing (C)

import Api
import Api.Author exposing (Author)
import Api.Me exposing (AuthorDetail)
import Api.Workspace exposing (Workspace)
import Dict exposing (Dict)
import Element exposing (Device)
import Gen.Route exposing (Route)
import Locale exposing (Locale)
import RemoteData exposing (RemoteData)
import Shacl.Form.Context as Shacl
import Time
import Ui.Atom.Tooltip exposing (Tooltip)
import Url exposing (Url)
import Viewport exposing (Viewport)


type alias C =
    { sandbox : Bool
    , useTripleStore : Bool
    , useSparklis : Bool
    , allowUploads : Bool
    , imprintUrl : Maybe String
    , dataProtectionUrl : Maybe String
    , accessibilityUrl : Maybe String
    , csrfToken : String
    , author : AuthorDetail
    , url : Url
    , route : Route
    , query : Dict String String
    , workspaceUuid : Maybe String
    , workspace : Maybe Workspace

    -- TIME
    , zone : Time.Zone
    , now : Time.Posix
    , locale : Locale

    -- DOM
    , viewport : Viewport
    , device : Device
    , navigationVisible : Bool
    , tooltip : Tooltip

    -- DATA
    , authors : RemoteData Api.Error (List Author)
    , workspaces : RemoteData Api.Error (List Workspace)

    -- SHACL
    , cShacl : Shacl.C
    }
