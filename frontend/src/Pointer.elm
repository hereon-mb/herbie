{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Pointer exposing
    ( Pointer
    , empty, field, id, verbatim
    , append
    , toString
    )

{-|

@docs Pointer
@docs empty, field, id, verbatim
@docs append
@docs toString

-}

import Api.Id


type Pointer
    = Pointer (List String)
    | Verbatim String


empty : Pointer
empty =
    Pointer []


field : String -> Pointer
field name =
    Pointer [ name ]


id : Api.Id.Id tag -> Pointer
id apiId =
    Pointer [ Api.Id.toString apiId ]


verbatim : String -> Pointer
verbatim =
    Verbatim


append : Pointer -> Pointer -> Pointer
append pointerRight pointerLeft =
    case ( pointerRight, pointerLeft ) of
        ( Pointer fragmentsRight, Pointer fragmentsLeft ) ->
            Pointer (fragmentsLeft ++ fragmentsRight)

        ( Verbatim right, Verbatim left ) ->
            Verbatim (left ++ right)

        ( Verbatim right, Pointer fragmentsLeft ) ->
            Pointer (fragmentsLeft ++ [ right ])

        ( Pointer fragmentsRight, Verbatim left ) ->
            Pointer (left :: fragmentsRight)


toString : Pointer -> String
toString pointer =
    case pointer of
        Pointer fragments ->
            let
                escape =
                    String.replace "~" "~0"
                        >> String.replace "/" "~1"
            in
            "/" ++ String.join "/" (List.map escape fragments)

        Verbatim text ->
            text
