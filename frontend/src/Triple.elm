module Triple exposing
    ( first
    , apply
    , mapSecond
    )

{-|

@docs first

@docs apply

-}


first : ( a, b, c ) -> a
first ( a, _, _ ) =
    a


apply : (a -> b -> c -> d) -> ( a, b, c ) -> d
apply f ( a, b, c ) =
    f a b c


mapSecond : (b1 -> b2) -> ( a, b1, c ) -> ( a, b2, c )
mapSecond f ( a, b1, c ) =
    ( a, f b1, c )
