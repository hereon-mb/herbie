{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module HashTag exposing (HashTag, find, findAt)

import Regex exposing (Regex)


type alias HashTag =
    { position : Int
    , tag : String
    }


find : String -> List HashTag
find content =
    let
        toHashTag { index, match } =
            { position = index
            , tag = String.dropLeft 1 match
            }
    in
    content
        |> Regex.find regex
        |> List.map toHashTag


findAt : Int -> String -> Maybe HashTag
findAt position content =
    let
        toHashTagAtPosition { index, match } =
            let
                start =
                    index

                end =
                    index + String.length match
            in
            if (start < position) && (position <= end) then
                Just
                    { position = start
                    , tag = String.dropLeft 1 match
                    }

            else
                Nothing
    in
    content
        |> Regex.find regex
        |> List.filterMap toHashTagAtPosition
        |> List.head


regex : Regex
regex =
    Regex.fromString "\\#([0-9\\w\\-.:]*[0-9\\w\\-]+)"
        |> Maybe.withDefault Regex.never
