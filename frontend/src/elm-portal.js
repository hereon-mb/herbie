// This code was taken from https://github.com/mdgriffith/elm-prefab and
// adapted.  The original code is licensed as follows:
//
// BSD 3-Clause License
//
// Copyright (c) 2023, Matthew Griffith
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


function offsetTopUntilPortalTarget(el, id) {
  for (const sibling of getAllSiblings(el)) {
    if (sibling.id === id) {
      return el.offsetTop + el.clientTop ;
    }
  }

  if (el.parentElement) {
    return el.offsetTop + el.clientTop + offsetTopUntilPortalTarget(el.parentElement, id);
  }

  return null;
}


function offsetLeftUntilPortalTarget(el, id) {
  for (const sibling of getAllSiblings(el)) {
    if (sibling.id === id) {
      return el.offsetLeft + el.clientLeft;
    }
  }

  if (el.parentElement) {
    return el.offsetLeft + el.clientLeft + offsetLeftUntilPortalTarget(el.parentElement, id);
  }

  return null;
}


function nearestNodeWithId(el, id) {
  for (const sibling of getAllSiblings(el)) {
    if (sibling.id === id) {
      return sibling
    }
  }

  if (el.parentElement) {
    return nearestNodeWithId(el.parentElement, id)
  }

  return null
}


function getAllSiblings(el) {
  const siblings = [];
  let sibling = el?.parentNode?.firstChild;
  if (sibling) {
    do {
      // text node
      if (sibling.nodeType !== 3) {
        siblings.push(sibling);
      }
    } while ((sibling = sibling.nextSibling))
  }
  return siblings;
}


export default function include(options) {
  Object.defineProperty(Element.prototype, "__getPortalViewport", {
    get() {
      const offsetTop = offsetTopUntilPortalTarget(this, options.mountId);
      const offsetLeft = offsetLeftUntilPortalTarget(this, options.mountId);

      return {
        parent: {
          boundingClientRect: this.getBoundingClientRect(),
          offsetTop: offsetTop,
          offsetLeft: offsetLeft,
        },
        window: {
          width: window.innerWidth,
          height: window.innerHeight ,
        },
      };
    },
  });

  customElements.define(
    "elm-portal",
    class extends HTMLElement {
      // Base custom element stuff
      connectedCallback() {
        this._targetNode = document.createElement("div");
        if (this.target) {
          this.target.appendChild(this._targetNode);
        } else {
          // If there is no place to mount the elements, we want to throw immediately.
          // This node should always be present, even at app startup.
          throw new Error(
            `There is no place to mount elements that are using elm-portal.  I was looking for #${options.mountId}, but didn't find anything.`
          );
        }
      }

      get target() {
        if (this._targetCached) {
          return this._targetCached;
        } else {
          this._targetCached = nearestNodeWithId(this, options.mountId);
          return this._targetCached;
        }
      }

      disconnectedCallback() {
        if (this.target) {
          this.target.removeChild(this._targetNode);
        }
      }

      // Re-implementations of HTMLElement functions
      get childNodes() {
        return this._targetNode.childNodes;
      }

      replaceData(...args) {
        return this._targetNode.replaceData(...args);
      }

      removeChild(...args) {
        return this._targetNode.removeChild(...args);
      }

      insertBefore(...args) {
        return this._targetNode.insertBefore(...args);
      }
      appendChild(node) {
        // To cooperate with the Elm runtime
        requestAnimationFrame(() => {
          return this._targetNode.appendChild(node);
        });
        return node;
      }
    }
  );
}
