// Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
// a client-server based web application for research documentation
//
// Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
//
// Authors: Fabian Kirchner, Catriona Eschke
//
// This program is subject to the terms and conditions for non-commercial use of
// ELN. You can find the license text in the file LICENSE.en and under
// http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
// you have any questions or comments, you can contact us at hereon at
// herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
// Max-Planck-Straße 1, 21502 Geesthacht, Germany.

// Taken and adapted from
//
//   https://github.com/zurb/tribute/blob/e4edbac023ca22295317faa5236bf15b9c78dc3a/src/TributeRange.js#L447
//
// Was original licensed as follows:
//
// The MIT License (MIT)
//
// Copyright (c) 2017-2020 ZURB, Inc.
// Copyright (c) 2014 Jeff Collins
// Copyright (c) 2012 Matt York
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

export function getCharacterCoordinatesOf(element, position) {
    let properties = ['direction', 'boxSizing', 'width', 'height', 'overflowX',
        'overflowY', 'borderTopWidth', 'borderRightWidth',
        'borderBottomWidth', 'borderLeftWidth', 'paddingTop',
        'paddingRight', 'paddingBottom', 'paddingLeft',
        'fontStyle', 'fontVariant', 'fontWeight', 'fontStretch',
        'fontSize', 'fontSizeAdjust', 'lineHeight', 'fontFamily',
        'textAlign', 'textTransform', 'textIndent',
        'textDecoration', 'letterSpacing', 'wordSpacing'
    ]

    let isFirefox = (window.mozInnerScreenX !== null)

    let div = document.createElement('div')
    div.id = 'input-textarea-caret-position-mirror-div'
    document.body.appendChild(div)

    let style = div.style
    let computed = window.getComputedStyle ? getComputedStyle(element) : element.currentStyle

    style.whiteSpace = 'pre-wrap'
    if (element.nodeName !== 'INPUT') {
        style.wordWrap = 'break-word'
    }

    // position off-screen
    style.position = 'absolute'
    style.visibility = 'hidden'

    // transfer the element's properties to the div
    properties.forEach(prop => {
        style[prop] = computed[prop]
    })

    if (isFirefox) {
        style.width = `${(parseInt(computed.width) - 2)}px`
        if (element.scrollHeight > parseInt(computed.height))
            style.overflowY = 'scroll'
    } else {
        style.overflow = 'hidden'
    }

    div.textContent = element.value.substring(0, position)

    if (element.nodeName === 'INPUT') {
        div.textContent = div.textContent.replace(/\s/g, ' ')
    }

    let span = document.createElement('span')
    span.textContent = element.value.substring(position) || '.'
    div.appendChild(span)

    let doc = document.documentElement
    let windowLeft = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0)
    let windowTop = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0)

    let top = 0;
    let left = 0;

    let coordinates = {
        x: left + windowLeft + span.offsetLeft + parseInt(computed.borderLeftWidth),
        y: top + windowTop + span.offsetTop + parseInt(computed.borderTopWidth) + parseInt(computed.fontSize) - element.scrollTop,
    };

    document.body.removeChild(div)
    return coordinates
}
