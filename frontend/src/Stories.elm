{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


port module Stories exposing (main)

import Bulletproof
import Stories.Atom.Button
import Stories.Atom.ButtonIcon
import Stories.Atom.ButtonToggle
import Stories.Atom.Checkbox
import Stories.Atom.ChipAssist
import Stories.Atom.ChipFilter
import Stories.Atom.ChipInput
import Stories.Atom.ChipSuggestion
import Stories.Atom.ChoiceChips
import Stories.Atom.Icon
import Stories.Atom.ProgressIndicatorLinear
import Stories.Molecule.ComboboxMultiselect
import Stories.Molecule.ComboboxSingleselect
import Stories.Molecule.DatePicker
import Stories.Molecule.Dialog
import Stories.Molecule.FabMenu
import Stories.Molecule.Listbox
import Stories.Molecule.Navigation
import Stories.Molecule.Note
import Stories.Sandbox.Animations
import Stories.Sandbox.ElmUiBugs
import Stories.Theme.Card
import Stories.Theme.Color
import Stories.Theme.Typography


port saveSettings : String -> Cmd msg


main : Bulletproof.Program
main =
    Bulletproof.program saveSettings
        [ Bulletproof.folder "Theme"
            [ Stories.Theme.Card.story
            , Stories.Theme.Color.story
            , Stories.Theme.Typography.story
            ]
        , Bulletproof.folder "Atom"
            [ Stories.Atom.Button.story
            , Stories.Atom.ButtonIcon.story
            , Stories.Atom.ButtonToggle.story
            , Stories.Atom.Checkbox.story
            , Bulletproof.folder "Chips"
                [ Stories.Atom.ChipAssist.story
                , Stories.Atom.ChipFilter.story
                , Stories.Atom.ChipInput.story
                , Stories.Atom.ChipSuggestion.story
                ]
            , Stories.Atom.Icon.story
            , Stories.Atom.ChoiceChips.story
            , Stories.Atom.ProgressIndicatorLinear.story
            ]
        , Bulletproof.folder "Molecule"
            [ Stories.Molecule.DatePicker.story
            , Stories.Molecule.Dialog.story
            , Stories.Molecule.Note.story
            , Stories.Molecule.Navigation.story
            , Stories.Molecule.Listbox.story
            , Stories.Molecule.FabMenu.story
            , Stories.Molecule.ComboboxMultiselect.story
            , Stories.Molecule.ComboboxSingleselect.story
            ]
        , Bulletproof.folder "Organism"
            []
        , Bulletproof.folder "Sandbox"
            [ Stories.Sandbox.Animations.story
            , Stories.Sandbox.ElmUiBugs.story
            ]
        ]
