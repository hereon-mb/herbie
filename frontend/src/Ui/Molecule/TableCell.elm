{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Molecule.TableCell exposing
    ( TableCell, toElement
    , empty, text, textBold
    , decimalStatic
    , date, dateTime
    , link, linkList
    , download
    , withInfo
    , Affix, withAffix
    , withHovering, withHoveredRow, Hovering
    , withPosition, Position(..)
    , C
    )

{-|

@docs TableCell, toElement

@docs empty, text, textBold
@docs decimalStatic
@docs date, dateTime
@docs link, linkList
@docs download

@docs withInfo
@docs Affix, withAffix

@docs withHovering, withHoveredRow, Hovering
@docs withPosition, Position

@docs C

-}

import Api.Decimal as Decimal exposing (Decimal)
import Element
    exposing
        ( Attribute
        , Element
        , alignBottom
        , centerY
        , column
        , el
        , fill
        , height
        , none
        , padding
        , paddingEach
        , paddingXY
        , px
        , row
        , spacing
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Events
    exposing
        ( onMouseEnter
        , onMouseLeave
        )
import Fluent
    exposing
        ( Fluent
        , verbatim
        )
import Locale exposing (Locale)
import Rdf exposing (Iri)
import Time exposing (Posix)
import Ui.Atom.ButtonIcon as ButtonIcon exposing (buttonIcon)
import Ui.Atom.Icon exposing (Icon)
import Ui.Theme.Color as Color
    exposing
        ( onSurface
        , outline
        , primary
        , surface
        , transparent
        , withOpacity
        )
import Ui.Theme.Typography
    exposing
        ( LinkConfig
        , body1
        , body1Mono
        , body1MonoStrong
        , body1Strong
        , body2
        , download1
        , newTabLink1
        )


type alias C rest =
    { rest
        | locale : Locale
    }


type TableCell msg
    = TableCell (TableCellData msg)
    | TableCellEmpty (TableCellEmptyData msg)


type alias TableCellData msg =
    { content : Content msg
    , secondary : Secondary
    , overline : Overline
    , action : Maybe ( msg, Icon )
    , hovering : Maybe (Hovering msg)
    , hoveredRow : Bool
    , hoveredColumn : Bool
    , position : Position
    , highlightBold : Bool
    , active : Bool
    }


type alias TableCellEmptyData msg =
    { hovering : Maybe (Hovering msg)
    , hoveredRow : Bool
    , hoveredColumn : Bool
    , position : Position
    }


type Content msg
    = Text Fluent
    | TextBold Fluent
    | Date Posix
    | DateTime Posix
    | DecimalStatic
        { value : Maybe Decimal
        , valueDecimalPlaces : Int
        , prefix : Maybe ()
        , minimalPreDecimalPositions : Int
        }
    | Link
        { url : String
        , label : Fluent
        }
    | LinkList
        (List
            { url : String
            , label : Fluent
            }
        )
    | Download
        { url : String
        , label : Fluent
        }


type Secondary
    = NoSecondary
    | SecondaryAffix Affix


type alias Affix =
    { label : String
    , value : Iri
    }


type Overline
    = NoOverline
    | OverlineInfo Fluent


type alias Hovering msg =
    { onMouseEnter : msg
    , onMouseLeave : msg
    , hovered : Bool
    }


type Position
    = TopLeft
    | TopRight
    | Left
    | Center
    | BottomLeft
    | BottomRight


empty : TableCell msg
empty =
    TableCellEmpty
        { hovering = Nothing
        , hoveredRow = False
        , hoveredColumn = False
        , position = Center
        }


text : Fluent -> TableCell msg
text value =
    init (Text value)


textBold : Fluent -> TableCell msg
textBold value =
    init (TextBold value)


date : Posix -> TableCell msg
date value =
    init (Date value)


dateTime : Posix -> TableCell msg
dateTime value =
    init (DateTime value)


decimalStatic :
    { value : Maybe Decimal
    , valueDecimalPlaces : Int
    , prefix : Maybe ()
    , minimalPreDecimalPositions : Int
    }
    -> TableCell msg
decimalStatic config =
    init (DecimalStatic config)


link : LinkConfig -> TableCell msg
link config =
    init (Link config)


linkList : List LinkConfig -> TableCell msg
linkList configs =
    init (LinkList configs)


download : LinkConfig -> TableCell msg
download config =
    init (Download config)


init : Content msg -> TableCell msg
init content =
    TableCell
        { content = content
        , secondary = NoSecondary
        , overline = NoOverline
        , action = Nothing
        , hovering = Nothing
        , hoveredRow = False
        , hoveredColumn = False
        , position = Center
        , highlightBold = False
        , active = False
        }


withInfo : Fluent -> TableCell msg -> TableCell msg
withInfo info model =
    case model of
        TableCell data ->
            TableCell { data | overline = OverlineInfo info }

        TableCellEmpty _ ->
            model


withAffix : Affix -> TableCell msg -> TableCell msg
withAffix affix model =
    case model of
        TableCell data ->
            TableCell { data | secondary = SecondaryAffix affix }

        TableCellEmpty _ ->
            model


withHovering : Hovering msg -> TableCell msg -> TableCell msg
withHovering hovering model =
    case model of
        TableCell data ->
            TableCell { data | hovering = Just hovering }

        TableCellEmpty data ->
            TableCellEmpty { data | hovering = Just hovering }


withHoveredRow : Bool -> TableCell msg -> TableCell msg
withHoveredRow hovered model =
    case model of
        TableCell data ->
            TableCell { data | hoveredRow = hovered }

        TableCellEmpty data ->
            TableCellEmpty { data | hoveredRow = hovered }


withPosition : Position -> TableCell msg -> TableCell msg
withPosition position model =
    case model of
        TableCell data ->
            TableCell { data | position = position }

        TableCellEmpty data ->
            TableCellEmpty { data | position = position }


toElement : C r -> TableCell msg -> Element msg
toElement c model =
    case model of
        TableCell data ->
            viewTableCell c data

        TableCellEmpty data ->
            viewTableCellEmpty data


viewTableCell : C r -> TableCellData msg -> Element msg
viewTableCell c data =
    let
        addHovering attrs =
            case data.hovering of
                Nothing ->
                    attrs

                Just hovering ->
                    onMouseEnter hovering.onMouseEnter
                        :: onMouseLeave hovering.onMouseLeave
                        :: attrs

        hovered =
            data.hovering
                |> Maybe.map .hovered
                |> Maybe.withDefault False

        viewContent =
            el
                [ paddingEach
                    { top = 0
                    , bottom = 0
                    , left = 8
                    , right = 8 + 12 + 18
                    }
                , centerY
                ]
                >> el
                    [ height (px 36)
                    , width fill
                    ]
    in
    el
        ([ width fill
         , height fill
         , outerPadding data.position
         , outerBorderRounded data.position
         , Border.color outline
         , outerBorderWidth data.position
         , if data.active then
            Background.color (Color.overlayFocused primary)

           else if hovered || data.hoveredRow || data.hoveredColumn then
            onSurface
                |> withOpacity 0.08
                |> Background.color

           else
            Background.color surface
         ]
            |> addHovering
        )
        (column
            [ width fill
            , height fill
            , spacing 4
            ]
            [ case data.overline of
                NoOverline ->
                    none

                OverlineInfo info ->
                    info
                        |> body2
                        |> el [ paddingXY 10 0 ]
            , row
                [ width fill
                , alignBottom
                , spacing 4
                ]
                [ row
                    [ width fill
                    , height fill
                    , spacing 4
                    , Background.color transparent
                    , Border.width 2
                    , Border.color transparent
                    , Border.rounded 4
                    ]
                    [ case data.content of
                        Text value ->
                            if data.highlightBold then
                                value
                                    |> body1Strong
                                    |> viewContent

                            else
                                value
                                    |> body1
                                    |> viewContent

                        TextBold value ->
                            value
                                |> body1Strong
                                |> viewContent

                        Date value ->
                            tr [ Fluent.posix "date" value ] "date"
                                |> body1
                                |> viewContent

                        DateTime value ->
                            tr [ Fluent.posix "posix" value ] "posix"
                                |> body1
                                |> viewContent

                        DecimalStatic { value, valueDecimalPlaces, prefix } ->
                            if data.highlightBold then
                                printDecimal c prefix value valueDecimalPlaces
                                    |> body1MonoStrong
                                    |> viewContent

                            else
                                printDecimal c prefix value valueDecimalPlaces
                                    |> body1Mono
                                    |> viewContent

                        Link stuff ->
                            stuff
                                |> newTabLink1
                                |> el [ paddingXY 8 0 ]

                        LinkList stuffs ->
                            stuffs
                                |> List.map newTabLink1
                                |> row
                                    [ paddingXY 8 0
                                    , spacing 8
                                    ]

                        Download stuff ->
                            stuff
                                |> download1
                                |> el [ paddingXY 8 0 ]
                    , case data.secondary of
                        NoSecondary ->
                            none

                        SecondaryAffix affix ->
                            affix.label
                                |> verbatim
                                |> body1
                                |> el
                                    [ centerY
                                    , paddingXY 4 0
                                    ]
                    ]
                , case data.action of
                    Nothing ->
                        el
                            [ width (px 36)
                            , height (px 36)
                            ]
                            none

                    Just ( msg, icon ) ->
                        icon
                            |> buttonIcon msg
                            |> ButtonIcon.small
                            |> ButtonIcon.toElement
                            |> el [ Element.transparent (not hovered) ]
                ]
            ]
        )


printDecimal : C r -> Maybe () -> Maybe Decimal -> Int -> Fluent
printDecimal c prefix value valueDecimalPlaces =
    String.join " "
        (List.filterMap identity
            [ Maybe.map stringFromPrefix prefix
            , value
                |> Maybe.map (Decimal.toStringWith c.locale valueDecimalPlaces)
            ]
        )
        |> verbatim


stringFromPrefix : () -> String
stringFromPrefix _ =
    "FIXME"


viewTableCellEmpty : TableCellEmptyData msg -> Element msg
viewTableCellEmpty data =
    let
        addHovering attrs =
            case data.hovering of
                Nothing ->
                    attrs

                Just hovering ->
                    onMouseEnter hovering.onMouseEnter
                        :: onMouseLeave hovering.onMouseLeave
                        :: attrs
    in
    el
        ([ width fill
         , height fill
         , outerPadding data.position
         , outerBorderRounded data.position
         , Border.color outline
         , outerBorderWidth data.position
         ]
            |> addHovering
        )
        none


outerPadding : Position -> Attribute msg
outerPadding position =
    case position of
        TopLeft ->
            paddingEach
                { top = 4
                , bottom = 4
                , left = 12
                , right = 4
                }

        TopRight ->
            padding 4

        Left ->
            paddingEach
                { top = 4
                , bottom = 4
                , left = 12
                , right = 4
                }

        Center ->
            padding 4

        BottomLeft ->
            paddingEach
                { top = 4
                , bottom = 4
                , left = 12
                , right = 4
                }

        BottomRight ->
            padding 4


outerBorderRounded : Position -> Attribute msg
outerBorderRounded position =
    case position of
        TopLeft ->
            Border.roundEach
                { topLeft = 4
                , topRight = 0
                , bottomRight = 0
                , bottomLeft = 0
                }

        TopRight ->
            Border.roundEach
                { topLeft = 0
                , topRight = 4
                , bottomRight = 0
                , bottomLeft = 0
                }

        Left ->
            Border.rounded 0

        Center ->
            Border.rounded 0

        BottomLeft ->
            Border.roundEach
                { topLeft = 0
                , topRight = 0
                , bottomRight = 0
                , bottomLeft = 4
                }

        BottomRight ->
            Border.roundEach
                { topLeft = 0
                , topRight = 0
                , bottomRight = 4
                , bottomLeft = 0
                }


outerBorderWidth : Position -> Attribute msg
outerBorderWidth position =
    case position of
        TopLeft ->
            Border.widthEach
                { top = 1
                , bottom = 0
                , left = 1
                , right = 0
                }

        TopRight ->
            Border.widthEach
                { top = 1
                , bottom = 0
                , left = 0
                , right = 0
                }

        Left ->
            Border.widthEach
                { top = 1
                , bottom = 0
                , left = 1
                , right = 0
                }

        Center ->
            Border.widthEach
                { top = 1
                , bottom = 0
                , left = 0
                , right = 0
                }

        BottomLeft ->
            Border.widthEach
                { top = 1
                , bottom = 0
                , left = 1
                , right = 0
                }

        BottomRight ->
            Border.widthEach
                { top = 1
                , bottom = 0
                , left = 0
                , right = 0
                }


tr : List Fluent.Argument -> String -> Fluent
tr args key =
    Fluent.textWith args ("ui-molecule-table-cell--" ++ key)
