{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Molecule.Table exposing (Column, Config, Table, table, view, withoutScrolling)

import Element
    exposing
        ( Element
        , Length
        , column
        , el
        , fill
        , indexedTable
        , none
        , row
        , scrollbarX
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Extra
    exposing
        ( style
        )
import Ui.Molecule.TableCell as TableCell exposing (TableCell)
import Ui.Theme.Color
    exposing
        ( outline
        , surface
        )


type alias Config record msg =
    { data : List record
    , columns :
        List (Column record msg)
    }


type alias Column record msg =
    { header : TableCell msg
    , width : Length
    , view : record -> TableCell msg
    }


type Table record msg
    = Table
        { config : Config record msg
        , hovered : Maybe ( Int, Int )
        , hoveringMsgs :
            Maybe
                { userEnteredCell : Int -> Int -> msg
                , userLeftCell : msg
                }
        , scroll : Bool
        }


table : Config record msg -> Table record msg
table config =
    Table
        { config = config
        , hovered = Nothing
        , hoveringMsgs = Nothing
        , scroll = True
        }


withoutScrolling : Table record msg -> Table record msg
withoutScrolling (Table stuff) =
    Table { stuff | scroll = False }


view : TableCell.C r -> Table record msg -> Element msg
view c (Table { config, hovered, hoveringMsgs, scroll }) =
    let
        toColumn indexColumn column =
            { header =
                column.header
                    |> withPosition indexColumn 0
                    |> addHovering indexColumn 0
                    |> addHoveredRow indexColumn 0
                    |> TableCell.toElement c
            , width = column.width
            , view =
                \indexRow ->
                    column.view
                        >> withPosition indexColumn (indexRow + 1)
                        >> addHovering indexColumn (indexRow + 1)
                        >> addHoveredRow indexColumn (indexRow + 1)
                        >> TableCell.toElement c
            }

        withPosition indexColumn indexRow =
            if indexColumn == 0 then
                if indexRow == 0 then
                    TableCell.withPosition TableCell.TopLeft

                else if indexRow == countRows then
                    TableCell.withPosition TableCell.BottomLeft

                else
                    TableCell.withPosition TableCell.Left

            else if indexColumn == countColumns - 1 then
                if indexRow == 0 then
                    TableCell.withPosition TableCell.TopRight

                else if indexRow == countRows then
                    TableCell.withPosition TableCell.BottomRight

                else
                    identity

            else
                identity

        addHovering indexColumn indexRow =
            case hoveringMsgs of
                Nothing ->
                    identity

                Just { userEnteredCell, userLeftCell } ->
                    TableCell.withHovering
                        { onMouseEnter = userEnteredCell indexColumn indexRow
                        , onMouseLeave = userLeftCell
                        , hovered =
                            case hovered of
                                Nothing ->
                                    False

                                Just ( hoveredColumn, hoveredRow ) ->
                                    hoveredColumn == indexColumn && hoveredRow == indexRow
                        }

        addHoveredRow indexColumn indexRow =
            case hovered of
                Nothing ->
                    identity

                Just ( hoveredColumn, hoveredRow ) ->
                    TableCell.withHoveredRow (hoveredColumn == indexColumn || hoveredRow == indexRow)

        countRows =
            List.length config.data

        countColumns =
            List.length config.columns
    in
    [ [ indexedTable
            [ Border.widthEach
                { top = 0
                , bottom = 1
                , left = 0
                , right = 1
                }
            , Border.rounded 4
            , Border.color outline
            , Background.color surface
            ]
            { data = config.data
            , columns = List.indexedMap toColumn config.columns
            }
      , if scroll then
            -- FIXME hack so bottom border is visible when scrollbars appear
            el [] none

        else
            none
      ]
        |> column []
    , if scroll then
        -- FIXME hack so right border is visible when scrollbars appear
        el [] none

      else
        none
    ]
        |> row
            (if scroll then
                [ width fill
                , scrollbarX
                , style "flex-basis" "auto"
                ]

             else
                [ width fill
                ]
            )
