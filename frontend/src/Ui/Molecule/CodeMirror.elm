module Ui.Molecule.CodeMirror exposing (Config, codeMirror)

import Element as Ui exposing (Element)
import Element.Border as Border
import Html exposing (Html)
import Html.Attributes
import Html.Events
import Json.Decode as Decode
import Json.Encode as Encode exposing (Value)
import Rdf.Format as Format exposing (Format)
import Ui.Theme.Color as Color


nameNode : String
nameNode =
    "code-mirror"


type alias Config msg =
    { content : String
    , format : Format
    , onChange : String -> msg
    }


codeMirror : Config msg -> Element msg
codeMirror config =
    Ui.el
        [ Ui.width Ui.fill
        , Ui.height Ui.fill
        , Ui.alignTop
        , Ui.paddingXY 0 4
        , Border.rounded 4
        , Border.width 1
        , Border.color Color.outline
        ]
        (Ui.html (codeMirrorHelp config))


codeMirrorHelp : Config msg -> Html msg
codeMirrorHelp config =
    Html.node nameNode
        [ { content = config.content
          , format = config.format
          }
            |> encode
            |> Html.Attributes.property "elmData"
        , Html.Events.stopPropagationOn "text-changed"
            (Decode.field "detail" Decode.string
                |> Decode.map
                    (\text ->
                        ( config.onChange text
                        , True
                        )
                    )
            )
        , Html.Attributes.style "display" "contents"
        ]
        []


type alias External =
    { content : String
    , format : Format
    }


encode : External -> Value
encode external =
    Encode.object
        [ ( "content", Encode.string external.content )
        , ( "format", Encode.string (Format.toMime external.format) )
        ]
