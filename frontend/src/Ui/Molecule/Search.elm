module Ui.Molecule.Search exposing (Config, view)

import Element
    exposing
        ( Element
        , centerY
        , el
        , fill
        , height
        , mouseOver
        , none
        , paddingEach
        , paddingXY
        , px
        , row
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Fluent exposing (verbatim)
import Ui.Atom.Icon as Icon
import Ui.Theme.Color as Color
import Ui.Theme.Elevation as Elevation
import Ui.Theme.Typography as Typography


type alias Config msg =
    { userChangedQuery : String -> msg
    , userClearedQuery : msg
    }


view : Config msg -> String -> Element msg
view config query =
    row
        [ width fill
        , height (px 56)
        , Background.color Color.surfaceContainerHigh
        , Border.rounded 28
        , Elevation.z4
        ]
        [ viewLeadingIcon
        , viewInput config query
        , viewTrailingIconButton config query
        ]


viewLeadingIcon : Element msg
viewLeadingIcon =
    el
        [ centerY
        , paddingXY 16 0
        ]
        (Icon.viewNormal Icon.Search)


viewInput : Config msg -> String -> Element msg
viewInput config query =
    let
        emptyQuery =
            String.trim query == ""
    in
    Input.text
        [ width fill
        , height fill
        , paddingEach
            { top = 19
            , bottom = 19
            , left = 0
            , right =
                if emptyQuery then
                    32

                else
                    0
            }
        , Font.size 18
        , Background.color Color.transparent
        , Border.width 0
        ]
        { onChange = config.userChangedQuery
        , text = query
        , placeholder = Just viewPlaceholder
        , label = Input.labelHidden "search"
        }


viewPlaceholder : Input.Placeholder msg
viewPlaceholder =
    Input.placeholder
        [ Font.color Color.onSurfaceVariant ]
        (Typography.body1 (verbatim "Search"))


viewTrailingIconButton : Config msg -> String -> Element msg
viewTrailingIconButton config query =
    let
        emptyQuery =
            String.trim query == ""
    in
    if emptyQuery then
        none

    else
        Input.button
            [ centerY
            , paddingXY 16 0
            , Font.color Color.onSurfaceVariant
            , mouseOver
                [ Font.color Color.primary ]
            ]
            { onPress = Just config.userClearedQuery
            , label = Icon.viewNormal Icon.Clear
            }
