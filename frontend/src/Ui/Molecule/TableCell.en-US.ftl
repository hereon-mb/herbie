ui-molecule-table-cell--date = { DATETIME($date) }
ui-molecule-table-cell--posix = { DATETIME($posix, year: "numeric", month: "numeric", day: "numeric", hour: "numeric", minute: "numeric") }
ui-molecule-table-cell--quality--had-issues = Had issues
ui-molecule-table-cell--quality--ok = Ok
