{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Molecule.Dialog exposing
    ( Dialog, toElement
    , desktop, loading
    , wide, fullWidth, fillVertically
    , withActions, withScrimEvents
    )

{-|

@docs Dialog, toElement
@docs desktop, loading
@docs wide, fullWidth, fillVertically
@docs withActions, withScrimEvents

-}

import Element
    exposing
        ( Attribute
        , Element
        , centerX
        , centerY
        , column
        , el
        , fill
        , height
        , inFront
        , maximum
        , none
        , padding
        , paddingXY
        , row
        , scrollbarY
        , shrink
        , spacing
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Extra
    exposing
        ( loadingAnimation
        , pointerEvents
        , stopPropagationOnClick
        , style
        )
import Fluent exposing (Fluent)
import Ui.Theme.Color
    exposing
        ( scrim
        , surface
        )
import Ui.Theme.Elevation as Elevation
import Ui.Theme.Typography exposing (h5)


type Dialog msg
    = Desktop (Data msg)
    | Loading (DataLoading msg)


type alias Data msg =
    { title : Element msg
    , body : Element msg
    , actions : List (Element msg)
    , width : Width
    , fillVertically : Bool
    , scrimEvents : Maybe (ScrimEvents msg)
    }


type alias DataLoading msg =
    { width : Width
    , fillVertically : Bool
    , scrimEvents : Maybe (ScrimEvents msg)
    }


type Width
    = Narrow
    | Wide
    | FullWidth


type alias ScrimEvents msg =
    { userPressedScrim : msg
    , noOp : msg
    }


desktop : Fluent -> Element msg -> Dialog msg
desktop title body =
    Desktop
        { title =
            el
                [ paddingXY 16 0 ]
                (h5 title)
        , body = body
        , actions = []
        , width = Narrow
        , fillVertically = False
        , scrimEvents = Nothing
        }


loading : Dialog msg
loading =
    Loading
        { width = Narrow
        , fillVertically = False
        , scrimEvents = Nothing
        }


wide : Dialog msg -> Dialog msg
wide dialog =
    case dialog of
        Desktop data ->
            Desktop { data | width = Wide }

        Loading data ->
            Loading { data | width = Wide }


fullWidth : Dialog msg -> Dialog msg
fullWidth dialog =
    case dialog of
        Desktop data ->
            Desktop { data | width = FullWidth }

        Loading data ->
            Loading { data | width = FullWidth }


fillVertically : Dialog msg -> Dialog msg
fillVertically dialog =
    case dialog of
        Desktop data ->
            Desktop { data | fillVertically = True }

        Loading data ->
            Loading { data | fillVertically = True }


withActions : List (Element msg) -> Dialog msg -> Dialog msg
withActions actions dialog =
    case dialog of
        Desktop data ->
            Desktop { data | actions = actions }

        _ ->
            dialog


withScrimEvents : { userPressedScrim : msg, noOp : msg } -> Dialog msg -> Dialog msg
withScrimEvents scrimEvents dialog =
    case dialog of
        Desktop data ->
            Desktop { data | scrimEvents = Just scrimEvents }

        Loading data ->
            Loading { data | scrimEvents = Just scrimEvents }


toElement : Dialog msg -> Element msg
toElement dialog =
    let
        addUserPressedScrim data attrs =
            case data.scrimEvents of
                Nothing ->
                    attrs

                Just { userPressedScrim } ->
                    stopPropagationOnClick userPressedScrim :: attrs
    in
    case dialog of
        Desktop data ->
            el
                ([ width fill
                 , height fill
                 , Background.color scrim
                 , pointerEvents True
                 , style "z-index" "1"
                 , inFront (viewDialog data)
                 , scrollbarY
                 ]
                    |> addUserPressedScrim data
                )
                none

        Loading data ->
            el
                ([ width fill
                 , height fill
                 , Background.color scrim
                 , pointerEvents True
                 , style "z-index" "1"
                 , inFront (viewLoading data)
                 , scrollbarY
                 ]
                    |> addUserPressedScrim data
                )
                none


viewDialog : Data msg -> Element msg
viewDialog data =
    let
        addNoOp attrs =
            case data.scrimEvents of
                Nothing ->
                    attrs

                Just { noOp } ->
                    stopPropagationOnClick noOp :: attrs
    in
    (if List.isEmpty data.actions then
        [ viewMain data ]

     else
        [ viewMain data
        , viewActions data
        ]
    )
        |> column
            ([ width fill
             , height fill
             , paddingXY 0 8
             , spacing 8
             , Border.rounded 4
             , Elevation.z24
             , Background.color surface
             ]
                |> addNoOp
            )
        |> el
            [ centerX
            , centerY
            , widthDialog data
            , if data.fillVertically then
                height fill

              else
                height shrink
            , padding 16
            ]


viewLoading : DataLoading msg -> Element msg
viewLoading data =
    let
        addNoOp attrs =
            case data.scrimEvents of
                Nothing ->
                    attrs

                Just { noOp } ->
                    stopPropagationOnClick noOp :: attrs
    in
    none
        |> el
            [ width fill
            , if data.fillVertically then
                height fill

              else
                height shrink
            , paddingXY 0 16
            , spacing 24
            ]
        |> el
            ([ width fill
             , height fill
             , paddingXY 0 8
             , spacing 8
             , Border.rounded 4
             , Elevation.z24
             , Background.color surface
             , loadingAnimation
             ]
                |> addNoOp
            )
        |> el
            [ centerX
            , centerY
            , widthDialog data
            , if data.fillVertically then
                height fill

              else
                height shrink
            , padding 16
            ]


widthDialog : { rest | width : Width } -> Attribute msg
widthDialog data =
    fill
        |> maximum
            (case data.width of
                Narrow ->
                    560

                Wide ->
                    740

                FullWidth ->
                    1120
            )
        |> width


viewMain : Data msg -> Element msg
viewMain data =
    column
        [ width fill
        , if data.fillVertically then
            height fill

          else
            height shrink
        , paddingXY 0 16
        , spacing 24
        ]
        [ data.title
            |> el
                [ width fill
                , paddingXY 24 0
                ]
        , data.body
            |> el
                (if data.fillVertically then
                    [ width fill
                    , height fill
                    , paddingXY 24 0
                    , scrollbarY
                    ]

                 else
                    [ width fill
                    , paddingXY 24 0
                    ]
                )
        ]


viewActions : Data msg -> Element msg
viewActions data =
    row
        [ width fill
        , spacing 8
        , padding 8
        ]
        data.actions
