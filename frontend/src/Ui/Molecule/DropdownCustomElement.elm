{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Molecule.DropdownCustomElement exposing
    ( External
    , Label
    , Model
    , Msg
    , Option
    , main
    )

import Browser
import Browser.Dom
import Element as Ui
    exposing
        ( Attribute
        , Element
        , alignRight
        , below
        , centerY
        , column
        , el
        , fill
        , focusStyle
        , focused
        , height
        , htmlAttribute
        , inFront
        , map
        , moveDown
        , noStaticStyleSheet
        , none
        , padding
        , paddingEach
        , paddingXY
        , spacing
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Extra
    exposing
        ( id
        , style
        )
import Element.Font as Font
import Element.Input as Input
import Fluent
    exposing
        ( Fluent
        )
import Html exposing (Html)
import Html.Attributes
import Html.Events
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Decode
import Json.Encode as Encode exposing (Value)
import Maybe.Extra as Maybe
import Ports.CustomElement as Ports
import Task
import Ui.Atom.Icon as Icon
import Ui.Atom.Listbox as Listbox exposing (Listbox)
import Ui.Theme.Color as Color
    exposing
        ( outline
        )
import Ui.Theme.Elevation as Elevation
import Ui.Theme.Input
import Ui.Theme.Typography
    exposing
        ( body1
        )


nameNode : String
nameNode =
    "ui-molecule-dropdown"


type alias External =
    { id : String
    , label : Label
    , placeholder : Fluent
    , help : List Fluent
    , options : List Option
    , selection : Maybe Option
    }


type Label
    = LabelAbove Fluent
    | LabelledBy String


type alias Option =
    { id : String
    , label : Fluent
    }


encodeOption : Option -> Value
encodeOption option =
    Encode.object
        [ ( "id", Encode.string option.id )
        , ( "label", Fluent.encode option.label )
        ]


decoder : Decoder External
decoder =
    Decode.succeed External
        |> Decode.required "id" Decode.string
        |> Decode.required "label" labelDecoder
        |> Decode.required "placeholder" Fluent.decoder
        |> Decode.required "help" (Decode.list Fluent.decoder)
        |> Decode.required "options" (Decode.list optionDecoder)
        |> Decode.required "selection" (Decode.maybe optionDecoder)


labelDecoder : Decoder Label
labelDecoder =
    let
        help kind =
            case kind of
                "label-above" ->
                    Decode.succeed LabelAbove
                        |> Decode.required "fluent" Fluent.decoder

                "labelled-by" ->
                    Decode.succeed LabelledBy
                        |> Decode.required "id" Decode.string

                _ ->
                    Decode.fail ("unknown kind: '" ++ kind ++ "'")
    in
    Decode.andThen help (Decode.field "kind" Decode.string)


optionDecoder : Decoder Option
optionDecoder =
    Decode.succeed Option
        |> Decode.required "id" Decode.string
        |> Decode.required "label" Fluent.decoder


main : Program Value Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



---- MODEL


type Model
    = Running External Internal
    | Failed


type alias Internal =
    { open : Bool
    , preventBlur : Bool
    , listbox : Listbox
    }



---- INIT


init : Value -> ( Model, Cmd Msg )
init flags =
    case Decode.decodeValue decoder flags of
        Err _ ->
            ( Failed
            , Cmd.none
            )

        Ok external ->
            ( Running external
                { open = False
                , preventBlur = False
                , listbox = Listbox.init
                }
            , Cmd.none
            )



---- UPDATE


type Msg
    = NoOp
    | ElmDataChanged Value
      -- BUTTON
    | ButtonClicked String
    | ButtonArrowUpPressed String
    | ButtonArrowDownPressed String
      -- LISTBOX
    | ListboxMsg (Maybe String) (Listbox.Msg Option)
    | ListboxEscapePressed String
    | ListboxEnterPressed String
    | ListboxBlured
    | ListboxMousePressed
    | ListboxMouseReleased String


listboxUpdateConfig : Listbox.UpdateConfig Option
listboxUpdateConfig =
    { uniqueId = .id
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case model of
        Running external internal ->
            updateHelp msg external internal

        Failed ->
            ( model, Cmd.none )


updateHelp : Msg -> External -> Internal -> ( Model, Cmd Msg )
updateHelp msg external internal =
    case msg of
        NoOp ->
            ( Running external internal, Cmd.none )

        ElmDataChanged value ->
            case Decode.decodeValue decoder value of
                Err _ ->
                    ( Running external internal, Cmd.none )

                Ok newExternal ->
                    ( Running newExternal internal
                    , Cmd.none
                    )

        -- BUTTON
        ButtonClicked id ->
            if internal.open then
                ( Running external { internal | open = False }
                , Cmd.none
                )

            else
                ( Running external { internal | open = True }
                , focusListbox id
                )

        ButtonArrowUpPressed id ->
            let
                ( newListbox, newSelection ) =
                    Listbox.activatePreviousOrFirstOption
                        listboxUpdateConfig
                        external.options
                        internal.listbox
                        (Maybe.toList external.selection)
            in
            ( Running external
                { internal
                    | open = True
                    , listbox = newListbox
                }
            , Cmd.batch
                [ focusListbox id
                , updateSelection external (List.head newSelection)
                ]
            )

        ButtonArrowDownPressed id ->
            let
                ( newListbox, newSelection ) =
                    Listbox.activateNextOrFirstOption
                        listboxUpdateConfig
                        external.options
                        internal.listbox
                        (Maybe.toList external.selection)
            in
            ( Running external
                { internal
                    | open = True
                    , listbox = newListbox
                }
            , Cmd.batch
                [ focusListbox id
                , updateSelection external (List.head newSelection)
                ]
            )

        ListboxMsg maybeId listboxMsg ->
            let
                ( newListbox, listboxCmd, newSelection ) =
                    Listbox.update
                        listboxUpdateConfig
                        external.options
                        listboxMsg
                        internal.listbox
                        (Maybe.toList external.selection)
            in
            ( Running external { internal | listbox = newListbox }
            , Cmd.batch
                [ Cmd.map (ListboxMsg maybeId) listboxCmd
                , updateSelection external (List.head newSelection)
                ]
            )

        ListboxEscapePressed id ->
            ( Running external { internal | open = False }
            , focusButton id
            )

        ListboxEnterPressed id ->
            if internal.open then
                ( Running external { internal | open = False }
                , focusButton id
                )

            else
                ( Running external { internal | open = True }
                , focusListbox id
                )

        ListboxBlured ->
            ( Running external { internal | open = False }
            , Cmd.none
            )

        ListboxMousePressed ->
            ( Running external { internal | preventBlur = True }
            , Cmd.none
            )

        ListboxMouseReleased id ->
            ( Running external
                { internal
                    | open = False
                    , preventBlur = False
                }
            , focusButton id
            )



---- CMDS


focusListbox : String -> Cmd Msg
focusListbox id =
    Task.attempt (\_ -> NoOp) <|
        Listbox.focus
            { id = idListbox id
            , label = listboxLabel id
            , lift = ListboxMsg (Just id)
            }


focusButton : String -> Cmd Msg
focusButton id =
    Browser.Dom.focus (idButton id)
        |> Task.attempt (\_ -> NoOp)


updateSelection : External -> Maybe Option -> Cmd Msg
updateSelection external newSelection =
    if newSelection /= external.selection then
        newSelection
            |> Maybe.map encodeOption
            |> Maybe.withDefault Encode.null
            |> Ports.changeElmData

    else
        Cmd.none



---- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        Running _ _ ->
            Ports.elmDataChanged ElmDataChanged

        Failed ->
            Sub.none



---- VIEW


view : Model -> Html Msg
view model =
    case model of
        Running external internal ->
            let
                setZIndex attrs =
                    if internal.open then
                        htmlAttribute (Html.Attributes.style "z-index" "2") :: attrs

                    else
                        attrs
            in
            Html.node nameNode
                [ Html.Attributes.style "display" "contents" ]
                [ Ui.layoutWith
                    { options =
                        [ noStaticStyleSheet
                        , focusStyle
                            { borderColor = Nothing
                            , backgroundColor = Nothing
                            , shadow = Nothing
                            }
                        ]
                    }
                    ([ width fill
                     , height fill
                     , Font.family
                        [ Font.typeface "Work Sans"
                        , Font.sansSerif
                        ]
                     ]
                        |> setZIndex
                    )
                    (viewHelp external internal)
                ]

        Failed ->
            Html.node nameNode
                [ Html.Attributes.style "display" "contents" ]
                [ Html.text "Could not initialize custom element" ]


viewHelp : External -> Internal -> Element Msg
viewHelp external internal =
    column
        [ width fill
        , spacing 6
        ]
        [ case external.label of
            LabelAbove label ->
                map (\_ -> NoOp)
                    (Ui.Theme.Input.label
                        { id = Just (idLabel external.id)
                        , text = label
                        , help = external.help
                        }
                    )

            LabelledBy _ ->
                none
        , el
            [ width fill
            , on "mousedown" (Decode.succeed ListboxMousePressed)
            , on "mouseup" (Decode.succeed (ListboxMouseReleased external.id))
            , preventDefaultOn "keydown"
                (keyInfoDecoder
                    |> Decode.andThen
                        (\{ code, altDown, controlDown, metaDown, shiftDown } ->
                            case code of
                                "Escape" ->
                                    if
                                        not altDown
                                            && not controlDown
                                            && not metaDown
                                            && not shiftDown
                                    then
                                        Decode.succeed
                                            ( ListboxEscapePressed external.id, True )

                                    else
                                        Decode.fail "not handling that key combination"

                                " " ->
                                    if
                                        not altDown
                                            && not controlDown
                                            && not metaDown
                                            && not shiftDown
                                    then
                                        Decode.succeed
                                            ( ListboxEnterPressed external.id, True )

                                    else
                                        Decode.fail "not handling that key combination"

                                "Enter" ->
                                    if
                                        not altDown
                                            && not controlDown
                                            && not metaDown
                                            && not shiftDown
                                    then
                                        Decode.succeed
                                            ( ListboxEnterPressed external.id, True )

                                    else
                                        Decode.fail "not handling that key combination"

                                _ ->
                                    Decode.fail "not handling that key combination"
                        )
                )
            , below
                (if internal.open then
                    el
                        [ moveDown 4
                        , width fill
                        , on "focusout" (Decode.succeed ListboxBlured)
                        ]
                        (Listbox.view
                            listboxViewConfig
                            { id = idListbox external.id
                            , label =
                                case external.label of
                                    LabelAbove _ ->
                                        Listbox.labelledBy (idLabel external.id)

                                    LabelledBy id ->
                                        Listbox.labelledBy id
                            , lift = ListboxMsg (Just external.id)
                            }
                            external.options
                            internal.listbox
                            (Maybe.toList external.selection)
                        )

                 else
                    none
                )
            ]
            (viewButton external internal.open)
        ]


listboxViewConfig : Listbox.ViewConfig Option
listboxViewConfig =
    { uniqueId = .id
    , focusable = True
    , viewOption = \_ -> viewOption
    , heightMaximum = 256
    , attrsListbox =
        [ Border.rounded 4
        , Background.color Color.surface
        , Ui.clipX
        , Elevation.z8
        ]
    }


viewOption : Option -> Element msg
viewOption option =
    option.label
        |> body1
        |> el
            [ width fill
            , spacing 8
            ]


viewButton : External -> Bool -> Element Msg
viewButton external open =
    let
        setAriaExpanded attrs =
            if open then
                attribute "aria-expanded" "true" :: attrs

            else
                attribute "aria-expanded" "false" :: attrs

        addAriaLabelledBy attrs =
            attribute "aria-labelledby" (idLabel external.id) :: attrs
    in
    Input.button
        ([ width fill
         , paddingEach
            { top = 16
            , bottom = 16
            , left = 16
            , right = 64
            }
         , Font.size 18
         , style "letter-spacing" "0.15px"
         , style "line-height" "24px"
         , Border.rounded 4
         , Border.width 1
         , Border.color outline
         , focused
            [ Border.shadow
                { offset = ( 0, 0 )
                , size = 2
                , blur = 0
                , color = Color.primary
                }
            , Border.color Color.primary
            ]
         , inFront
            (el
                [ alignRight
                , centerY
                , paddingXY 5 0
                ]
                (el [ padding 8 ]
                    (if open then
                        Icon.viewNormal Icon.ArrowDropUp

                     else
                        Icon.viewNormal Icon.ArrowDropDown
                    )
                )
            )
         , id (idButton external.id)
         , attribute "aria-haspopup" "listbox"
         , attribute "tabindex" "0"
         , preventDefaultOn "keydown"
            (Decode.field "key" Decode.string
                |> Decode.andThen (buttonKeyDown external.id)
            )
         ]
            |> setAriaExpanded
            |> addAriaLabelledBy
        )
        { onPress = Just (ButtonClicked external.id)
        , label =
            external.selection
                |> Maybe.map viewOption
                |> Maybe.withDefault (viewPlaceholder external.placeholder)
        }


viewPlaceholder : Fluent -> Element msg
viewPlaceholder placeholder =
    placeholder
        |> body1
        |> el
            [ width fill
            , spacing 8
            ]


buttonKeyDown : String -> String -> Decoder ( Msg, Bool )
buttonKeyDown id code =
    case code of
        "ArrowUp" ->
            Decode.succeed ( ButtonArrowUpPressed id, True )

        "ArrowDown" ->
            Decode.succeed ( ButtonArrowDownPressed id, True )

        _ ->
            Decode.fail "not handling that key here"



-- IDS


idButton : String -> String
idButton id =
    id ++ "__button"


idListbox : String -> String
idListbox id =
    id ++ "-listbox"


idLabel : String -> String
idLabel id =
    id ++ "__label"


listboxLabel : String -> Listbox.Label
listboxLabel id =
    Listbox.labelledBy (idLabel id ++ idButton id)


attribute : String -> String -> Attribute msg
attribute name value =
    htmlAttribute (Html.Attributes.attribute name value)


on : String -> Decoder msg -> Attribute msg
on event =
    htmlAttribute << Html.Events.on event


preventDefaultOn : String -> Decoder ( msg, Bool ) -> Attribute msg
preventDefaultOn event =
    htmlAttribute << Html.Events.preventDefaultOn event


type alias KeyInfo =
    { code : String
    , altDown : Bool
    , controlDown : Bool
    , metaDown : Bool
    , shiftDown : Bool
    }


keyInfoDecoder : Decoder KeyInfo
keyInfoDecoder =
    Decode.succeed KeyInfo
        |> Decode.required "key" Decode.string
        |> Decode.optional "altKey" Decode.bool False
        |> Decode.optional "ctrlKey" Decode.bool False
        |> Decode.optional "metaKey" Decode.bool False
        |> Decode.optional "shiftKey" Decode.bool False
