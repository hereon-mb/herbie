{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Molecule.Tabs exposing (Config, view)

import Accessibility.Tabs
import Browser.Dom
import Element
    exposing
        ( Attribute
        , Element
        , clip
        , column
        , fill
        , focused
        , height
        , htmlAttribute
        , mouseDown
        , mouseOver
        , px
        , row
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Extra
    exposing
        ( ariaControls
        , ariaLabel
        , ariaLabelledby
        , ariaSelected
        , id
        , role
        , style
        , tabindex
        )
import Element.Font as Font
import Element.Input as Input
import Html.Events
import Task exposing (Task)
import Ui.Theme.Color as Color exposing (primary, surfaceVariant)


type alias Config tab msg =
    { all : List tab
    , label : String
    , tabLabel : tab -> Element msg
    , id : tab -> String
    , panel : tab -> Element msg
    , visible : tab
    , onSelect : tab -> Task Browser.Dom.Error () -> msg
    , tabPanelFocusable : Bool
    , tabAttributes : List (Attribute msg)
    }


view : Config tab msg -> Element msg
view config =
    Accessibility.Tabs.view (views config.tabAttributes)
        { label = Accessibility.Tabs.label config.label
        , tabs = List.map (toTab config) config.all
        , active = config.visible
        , onChange = config.onSelect
        , orientation = Accessibility.Tabs.Horizontal
        , activation = Accessibility.Tabs.Automatic
        }


toTab : Config tag msg -> tag -> Accessibility.Tabs.Tab (Element msg) tag
toTab config tag =
    { tag = tag
    , id = config.id tag
    , label = config.tabLabel tag
    , panel = config.panel tag
    , focusable = False
    }


views : List (Attribute msg) -> Accessibility.Tabs.Views (Element msg) msg
views tabAttributes =
    Accessibility.Tabs.custom
        { tabs =
            \attrs { tabs, panels } ->
                let
                    addAriaLabel elAttrs =
                        case attrs.ariaLabel of
                            Nothing ->
                                elAttrs

                            Just label ->
                                ariaLabel label :: elAttrs

                    addAriaLabelledby elAttrs =
                        case attrs.ariaLabelledBy of
                            Nothing ->
                                elAttrs

                            Just theId ->
                                ariaLabelledby theId :: elAttrs
                in
                column
                    [ width fill
                    , height fill
                    ]
                    [ row
                        ([ width fill
                         , Border.widthEach
                            { top = 0
                            , bottom = 1
                            , left = 0
                            , right = 0
                            }
                         , Border.color surfaceVariant
                         , role attrs.role
                         ]
                            |> addAriaLabel
                            |> addAriaLabelledby
                        )
                        tabs
                    , row
                        [ width fill
                        , height fill
                        ]
                        panels
                    ]
        , tab =
            \attrs { label, active } ->
                Input.button
                    ([ width fill
                     , height (px 48)
                     , Border.widthEach
                        { top = 0
                        , bottom = 2
                        , left = 0
                        , right = 0
                        }
                     , if active then
                        Border.color Color.primary

                       else
                        Border.color Color.transparent
                     , if active then
                        Font.color Color.primary

                       else
                        Font.color Color.onSurfaceVariant
                     , mouseDown
                        [ Font.color (Color.pressed Color.primary)
                        , Background.color (Color.overlayPressed primary)
                        ]
                     , mouseOver
                        [ Font.color (Color.hovered Color.primary)
                        , Background.color (Color.overlayHovered primary)
                        ]
                     , focused
                        [ Font.color (Color.focused Color.primary)
                        , Background.color (Color.overlayFocused primary)
                        ]
                     , role attrs.role
                     , ariaSelected attrs.ariaSelected
                     , ariaControls attrs.ariaControls
                     , id attrs.id
                     , htmlAttribute
                        (Html.Events.preventDefaultOn "keyup"
                            attrs.preventDefaultOnKeydown
                        )
                     , tabindex attrs.tabindex
                     ]
                        ++ tabAttributes
                    )
                    { onPress = Just attrs.onClick
                    , label = label
                    }
        , panel =
            \attrs { panel } ->
                let
                    addDisplayNone elAttrs =
                        if attrs.hidden then
                            style "display" "none" :: elAttrs

                        else
                            elAttrs

                    addTabindex elAttrs =
                        case attrs.tabindex of
                            Nothing ->
                                elAttrs

                            Just index ->
                                tabindex index :: elAttrs
                in
                panel
                    |> List.singleton
                    -- XXX we use column instead of el to ensure that panels
                    -- with scrollbarY are displayed with a scrollbar
                    |> column
                        ([ role attrs.role
                         , id attrs.id
                         , ariaLabelledby attrs.ariaLabelledby
                         , width fill
                         , height fill

                         -- XXX we need clip to ensure that panels with
                         -- scrollbarX with scrollbarY are displayed with
                         -- a scrollbar
                         , clip
                         ]
                            |> addDisplayNone
                            |> addTabindex
                        )
        }
