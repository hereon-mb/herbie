{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Molecule.FileUploadSingle exposing
    ( Config
    , Msg
    , update
    , view
    )

import Accessibility.Clipboard as Clipboard
import Element
    exposing
        ( Attribute
        , Element
        , centerX
        , centerY
        , column
        , el
        , fill
        , height
        , htmlAttribute
        , map
        , none
        , paddingEach
        , paddingXY
        , px
        , row
        , spacing
        , width
        )
import Element.Background as Background
import Element.Border as Border
import File exposing (File)
import File.Download
import File.Select
import Fluent
    exposing
        ( Fluent
        , verbatim
        )
import Html.Events
import Json.Decode as Decode exposing (Decoder)
import Maybe.Extra as Maybe
import Shacl.Form.Effect as Effect exposing (Effect)
import Ui.Atom.Button as Button exposing (button)
import Ui.Atom.ButtonIcon as ButtonIcon exposing (buttonIcon)
import Ui.Atom.Icon as Icon
import Ui.Theme.Color
    exposing
        ( error
        , hoveredWith
        , outline
        , primary
        , transparent
        )
import Ui.Theme.Input as Input
import Ui.Theme.Typography
    exposing
        ( body1Mono
        )


type Msg
    = UserEnteredDropzone
    | UserLeftDropzone
    | UserPressedUploadFile
    | UserSelectedFile File
    | UserPressedDownloadFile
    | UserPressedDeleteFile


update :
    { url : Maybe String
    , createFile : File -> Effect msg
    , deleteFile : Effect msg
    , liftMsg : Msg -> msg
    }
    -> Msg
    -> ( Bool, Effect msg )
update { url, createFile, deleteFile, liftMsg } msg =
    case msg of
        UserEnteredDropzone ->
            ( True
            , Effect.none
            )

        UserLeftDropzone ->
            ( False
            , Effect.none
            )

        UserPressedUploadFile ->
            ( False
            , Cmd.map liftMsg (File.Select.file [ "*" ] UserSelectedFile)
                |> Effect.fromCmd
            )

        UserSelectedFile file ->
            ( False
            , createFile file
            )

        UserPressedDownloadFile ->
            ( False
            , Maybe.unwrap Cmd.none File.Download.url url
                |> Effect.fromCmd
            )

        UserPressedDeleteFile ->
            ( False
            , deleteFile
            )


type alias Config msg =
    { label : Fluent
    , labelButton : Fluent
    , filename : Maybe String
    , dropzoneHovered : Bool
    , help : List Fluent
    , liftMsg : Msg -> msg
    , onCopy : Maybe msg
    , onPaste : Maybe ( String, msg, String -> Maybe msg )
    }


view : Config msg -> Element msg
view config =
    let
        addOnCopy attrs =
            case config.onCopy of
                Nothing ->
                    attrs

                Just msg ->
                    Clipboard.onCopy msg :: attrs

        addOnPaste attrs =
            case config.onPaste of
                Nothing ->
                    attrs

                Just ( format, msgPrevented, msg ) ->
                    Clipboard.onPaste format msgPrevented msg :: attrs
    in
    case config.filename of
        Just filename ->
            column
                ([ width fill
                 ]
                    |> addOnCopy
                    |> addOnPaste
                )
                [ Input.label
                    { id = Nothing
                    , text = config.label
                    , help = config.help
                    }
                , row
                    [ width fill
                    , spacing 8
                    , paddingEach
                        { top = 8
                        , bottom = 8
                        , left = 16
                        , right = 8
                        }
                    ]
                    [ filename
                        |> verbatim
                        |> body1Mono
                        |> el [ width fill ]
                    , Maybe.unwrap none
                        (\onCopy ->
                            buttonIcon onCopy Icon.ContentCopy
                                |> ButtonIcon.toElement
                        )
                        config.onCopy
                    , buttonIcon (config.liftMsg UserPressedDownloadFile) Icon.FileDownload
                        |> ButtonIcon.toElement
                    , buttonIcon (config.liftMsg UserPressedDeleteFile) Icon.RemoveCircleOutline
                        |> ButtonIcon.toElement
                    ]
                ]

        _ ->
            column
                ([ width fill
                 , spacing 8
                 , paddingEach
                    { top = 24
                    , bottom = 0
                    , left = 0
                    , right = 0
                    }
                 ]
                    |> addOnCopy
                    |> addOnPaste
                )
                [ if List.isEmpty config.help then
                    none

                  else
                    Input.help config.help
                        |> el [ paddingXY 16 0 ]
                , el
                    [ width fill
                    , height (px 80)
                    , Border.rounded 4
                    , Border.width 2
                    , Border.dashed
                    , if List.isEmpty config.help then
                        Border.color outline

                      else
                        Border.color error
                    , if config.dropzoneHovered then
                        Background.color (hoveredWith primary transparent)

                      else
                        Background.color transparent
                    , hijackOn "dragenter" (Decode.succeed UserEnteredDropzone)
                    , hijackOn "dragover" (Decode.succeed UserEnteredDropzone)
                    , hijackOn "dragleave" (Decode.succeed UserLeftDropzone)
                    , hijackOn "drop" dropDecoder
                    ]
                    (el
                        [ centerX
                        , centerY
                        , paddingXY 16 0
                        ]
                        (button UserPressedUploadFile config.labelButton
                            |> Button.withFormat Button.Text
                            |> Button.withLeadingIcon Icon.FileUpload
                            |> Button.view
                        )
                    )
                    |> map config.liftMsg
                ]


dropDecoder : Decoder Msg
dropDecoder =
    Decode.map UserSelectedFile
        (Decode.at [ "dataTransfer", "files" ]
            (Decode.index 0 File.decoder)
        )


hijackOn : String -> Decoder msg -> Attribute msg
hijackOn event decoder =
    htmlAttribute
        (Html.Events.preventDefaultOn event (Decode.map hijack decoder))


hijack : msg -> ( msg, Bool )
hijack msg =
    ( msg, True )
