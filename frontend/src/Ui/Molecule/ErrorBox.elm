module Ui.Molecule.ErrorBox exposing (ErrorBox, errorBox, view, withInfo)

import Element
    exposing
        ( Element
        , alignTop
        , column
        , el
        , fill
        , height
        , maximum
        , none
        , padding
        , paddingEach
        , paragraph
        , row
        , spacing
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Fluent exposing (verbatim)
import Ui.Atom.Icon as Icon
import Ui.Theme.Color exposing (error, onError)
import Ui.Theme.Typography
    exposing
        ( body1Strong
        , h5
        )


type ErrorBox
    = ErrorBox Data


type alias Data =
    { title : String
    , info : Maybe String
    }


errorBox : String -> ErrorBox
errorBox title =
    ErrorBox
        { title = title
        , info = Nothing
        }


withInfo : String -> ErrorBox -> ErrorBox
withInfo info (ErrorBox data) =
    ErrorBox { data | info = Just info }


view : ErrorBox -> Element msg
view (ErrorBox data) =
    column
        [ width (maximum 640 fill)
        , Border.rounded 12
        , Border.color error
        , Border.width 6
        ]
        [ viewTitle data
        , viewInfo data
        ]


viewTitle : Data -> Element msg
viewTitle data =
    el
        [ width fill
        , Background.color error
        , Font.color onError
        ]
        (row
            [ width fill
            , height fill
            , spacing 16
            , padding 32
            ]
            [ el
                [ alignTop ]
                (Icon.viewLarge Icon.Error)
            , el
                [ width fill ]
                (h5 (verbatim data.title))
            ]
        )


viewInfo : Data -> Element msg
viewInfo data =
    case data.info of
        Nothing ->
            none

        Just info ->
            paragraph
                [ width fill
                , paddingEach
                    { top = 32
                    , bottom = 32
                    , left = 32 + 32 + 16
                    , right = 32
                    }
                , Font.color error
                ]
                [ body1Strong (verbatim info)
                ]
