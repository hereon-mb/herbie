{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Molecule.ComboboxSingleselectCustomElement exposing
    ( Label, labelAbove
    , main, Model, Msg
    , External, changedDecoder, encode, nameNode
    )

{-|

@docs Label, labelAbove, labelledBy

@docs main, Model, Msg

-}

import Bool.Apply as Bool
import Browser
import Browser.Dom
import Element as Ui
    exposing
        ( Element
        , centerY
        , column
        , el
        , fill
        , focusStyle
        , height
        , noStaticStyleSheet
        , none
        , paddingXY
        , px
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Extra
    exposing
        ( id
        , onKeyDown
        , preventDefaultOnKeyDown
        , stopPropagationOnClick
        , style
        )
import Element.Font as Font
import Element.Input as Input
import Fluent exposing (Fluent)
import Html exposing (Html)
import Html.Attributes
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Decode
import Json.Encode as Encode exposing (Value)
import List.Extra as List
import Maybe.Extra as Maybe
import Platform.Cmd as Cmd
import Ports.CustomElement as Ports
import Task
import Ui.Atom.FocusBorder as FocusBorder exposing (focusBorder)
import Ui.Atom.Icon as Icon
import Ui.Atom.Listbox as Listbox exposing (Listbox)
import Ui.Atom.OptionCombobox as OptionCombobox exposing (OptionCombobox)
import Ui.Atom.Portal as Portal
import Ui.Theme.Color as Color
    exposing
        ( transparent
        )
import Ui.Theme.Elevation as Elevation


nameNode : String
nameNode =
    "ui-molecule-combobox-singleselect"


changedDecoder :
    { onChange : String -> msg
    , onSelect : OptionCombobox -> msg
    , onUnselect : msg
    }
    -> Decoder msg
changedDecoder config =
    let
        help kind =
            case kind of
                "on-change" ->
                    Decode.field "value" Decode.string
                        |> Decode.map config.onChange

                "on-select" ->
                    Decode.field "option" OptionCombobox.decoder
                        |> Decode.map config.onSelect

                "on-unselect" ->
                    Decode.succeed config.onUnselect

                _ ->
                    Decode.fail ("unknown kind '" ++ kind ++ "'")
    in
    Decode.andThen help (Decode.field "kind" Decode.string)


encodeOnChange : String -> Value
encodeOnChange value =
    Encode.object
        [ ( "kind", Encode.string "on-change" )
        , ( "value", Encode.string value )
        ]


encodeOnSelect : OptionCombobox -> Value
encodeOnSelect option =
    Encode.object
        [ ( "kind", Encode.string "on-select" )
        , ( "option", OptionCombobox.encode option )
        ]


encodeOnUnselect : Value
encodeOnUnselect =
    Encode.object
        [ ( "kind", Encode.string "on-unselect" )
        ]


type alias External =
    { id : String
    , label : Label
    , supportingText : Maybe Fluent
    , help : List Fluent
    , options : List OptionCombobox
    , selected : Maybe OptionCombobox
    , renderSubLabels : Bool
    , onCopy : Bool
    }


type Label
    = LabelAbove Fluent
    | LabelledBy String


labelAbove : Fluent -> Label
labelAbove =
    LabelAbove


encode : External -> Value
encode external =
    Encode.object
        [ ( "id", Encode.string external.id )
        , ( "label", encodeLabel external.label )
        , ( "supportingText", Maybe.unwrap Encode.null Fluent.encode external.supportingText )
        , ( "help", Encode.list Fluent.encode external.help )
        , ( "options", Encode.list OptionCombobox.encode external.options )
        , ( "selected", Maybe.unwrap Encode.null OptionCombobox.encode external.selected )
        , ( "renderSubLabels", Encode.bool external.renderSubLabels )
        , ( "onCopy", Encode.bool external.onCopy )
        ]


encodeLabel : Label -> Value
encodeLabel label =
    case label of
        LabelAbove fluent ->
            Encode.object
                [ ( "kind", Encode.string "label-above" )
                , ( "fluent", Fluent.encode fluent )
                ]

        LabelledBy id ->
            Encode.object
                [ ( "kind", Encode.string "labelled-by" )
                , ( "id", Encode.string id )
                ]


decoder : Decoder External
decoder =
    Decode.succeed External
        |> Decode.required "id" Decode.string
        |> Decode.required "label" labelDecoder
        |> Decode.required "supportingText" (Decode.maybe Fluent.decoder)
        |> Decode.required "help" (Decode.list Fluent.decoder)
        |> Decode.required "options" (Decode.list OptionCombobox.decoder)
        |> Decode.required "selected" (Decode.maybe OptionCombobox.decoder)
        |> Decode.required "renderSubLabels" Decode.bool
        |> Decode.required "onCopy" Decode.bool


labelDecoder : Decoder Label
labelDecoder =
    let
        help kind =
            case kind of
                "label-above" ->
                    Decode.succeed LabelAbove
                        |> Decode.required "fluent" Fluent.decoder

                "labelled-by" ->
                    Decode.succeed LabelledBy
                        |> Decode.required "id" Decode.string

                _ ->
                    Decode.fail ("unknown kind: '" ++ kind ++ "'")
    in
    Decode.andThen help (Decode.field "kind" Decode.string)


main : Program Value Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- MODEL


type Model
    = Running External Internal
    | Failed


type alias Internal =
    { query : String
    , showListbox : Bool
    , viewport : Maybe Portal.Viewport
    , listbox : Listbox
    , preventBlur : Bool
    }


init : Value -> ( Model, Cmd Msg )
init flags =
    case Decode.decodeValue decoder flags of
        Err _ ->
            ( Failed
            , Cmd.none
            )

        Ok external ->
            ( Running external
                { query = Maybe.unwrap "" .value external.selected
                , showListbox = False
                , viewport = Nothing
                , listbox = Listbox.init
                , preventBlur = False
                }
            , Cmd.none
            )



-- UPDATE


type Msg
    = NoOp
    | ElmDataChanged Value
    | UserCopied
      -- INPUT
    | UserClickedContainer
    | UserChangedQuery String
    | UserPressedClearQuery
    | UserPressedEscape
    | UserPressedArrowDown
    | OnViewport Portal.Viewport
    | UserUnfocusedQuery
    | UserPressedArrowDropDown
    | UserPressedArrowDropUp
      -- LISTBOX
    | ListboxMsg String (Listbox.Msg OptionCombobox)
    | UserPressedMouseOnListbox
    | UserReleasedMouseOnListbox


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case model of
        Running external internal ->
            updateHelp msg external internal

        Failed ->
            ( model, Cmd.none )


updateHelp : Msg -> External -> Internal -> ( Model, Cmd Msg )
updateHelp msg external internal =
    case msg of
        NoOp ->
            ( Running external internal, Cmd.none )

        ElmDataChanged value ->
            case Decode.decodeValue decoder value of
                Err _ ->
                    ( Running external internal, Cmd.none )

                Ok newExternal ->
                    case newExternal.selected of
                        Nothing ->
                            ( -- FIXME when and what to do here?
                              -- focusNextOrFirstEntry newExternal internal
                              Running newExternal internal
                            , Cmd.none
                            )

                        Just selected ->
                            ( Running newExternal { internal | query = selected.value }
                            , Cmd.none
                            )

        UserCopied ->
            ( Running external internal, Ports.copy () )

        UserClickedContainer ->
            ( Running external internal
            , focusInput external.id
            )

        UserChangedQuery newQuery ->
            case List.find (\option -> option.value == newQuery) external.options of
                Nothing ->
                    ( Running external
                        { internal
                            | query = newQuery
                            , showListbox = True
                        }
                    , [ Ports.changeElmData (encodeOnChange newQuery)
                      , Ports.changeElmData encodeOnUnselect
                      ]
                        |> Cmd.batch
                    )

                Just option ->
                    ( Running external
                        { internal
                            | query = newQuery
                            , showListbox = True
                        }
                    , [ Ports.changeElmData (encodeOnChange newQuery)
                      , Ports.changeElmData (encodeOnSelect option)
                      ]
                        |> Cmd.batch
                    )

        UserPressedClearQuery ->
            ( Running external
                { internal
                    | query = ""
                    , showListbox = False
                    , viewport = Nothing
                }
            , [ Ports.changeElmData (encodeOnChange "")
              , Ports.changeElmData encodeOnUnselect
              , focusInput external.id
              ]
                |> Cmd.batch
            )

        UserPressedEscape ->
            ( Running external { internal | showListbox = False }
            , Cmd.none
            )

        UserPressedArrowDown ->
            ( Running external { internal | showListbox = True }
            , Cmd.none
            )

        OnViewport viewport ->
            ( Running external { internal | viewport = Just viewport }
            , Cmd.none
            )

        UserUnfocusedQuery ->
            if internal.preventBlur then
                ( Running external internal, Cmd.none )

            else
                ( Running external
                    { internal
                        | showListbox = False
                        , viewport = Nothing
                    }
                , Cmd.none
                )

        UserPressedArrowDropDown ->
            ( Running external { internal | showListbox = True }
            , focusInput external.id
            )

        UserPressedArrowDropUp ->
            ( Running external { internal | showListbox = False }
            , focusInput external.id
            )

        ListboxMsg id listboxMsg ->
            let
                ( newListbox, listboxCmd, newSelection ) =
                    Listbox.update
                        { uniqueId = .id }
                        external.options
                        listboxMsg
                        internal.listbox
                        []
            in
            case List.head newSelection of
                Nothing ->
                    ( Running external { internal | listbox = newListbox }
                    , Cmd.map (ListboxMsg id) listboxCmd
                    )

                Just option ->
                    ( Running external
                        { internal
                            | listbox = newListbox
                            , query = option.value
                            , showListbox = False
                            , viewport = Nothing
                        }
                    , Cmd.batch
                        [ Cmd.map (ListboxMsg id) listboxCmd
                        , Ports.changeElmData (encodeOnSelect option)
                        , Ports.changeElmData (encodeOnChange option.value)
                        , focusInput id
                        ]
                    )

        UserPressedMouseOnListbox ->
            ( Running external { internal | preventBlur = True }
            , Cmd.none
            )

        UserReleasedMouseOnListbox ->
            ( Running external { internal | preventBlur = False }
            , Cmd.none
            )


focusInput : String -> Cmd Msg
focusInput id =
    Browser.Dom.focus (idInput id)
        |> Task.attempt (\_ -> NoOp)



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        Running external internal ->
            Sub.batch
                [ Ports.elmDataChanged ElmDataChanged
                , Sub.map (ListboxMsg (instance external).id) (Listbox.subscriptions internal.listbox)
                ]

        Failed ->
            Sub.none



-- VIEW


view : Model -> Html Msg
view model =
    case model of
        Running external internal ->
            Html.node nameNode
                [ Html.Attributes.style "display" "contents" ]
                [ Ui.layoutWith
                    { options =
                        [ noStaticStyleSheet
                        , focusStyle
                            { borderColor = Nothing
                            , backgroundColor = Nothing
                            , shadow = Nothing
                            }
                        ]
                    }
                    [ width fill
                    , height fill
                    , Font.family
                        [ Font.typeface "Work Sans"
                        , Font.sansSerif
                        ]
                    ]
                    (viewHelp external internal)
                ]

        Failed ->
            Html.node nameNode
                [ Html.Attributes.style "display" "contents" ]
                [ Html.text "Could not initialize custom element" ]


viewHelp : External -> Internal -> Element Msg
viewHelp external internal =
    let
        withSupportingText =
            case external.supportingText of
                Nothing ->
                    identity

                Just supportingText ->
                    FocusBorder.withSupportingText supportingText

        withLabel =
            case external.label of
                LabelAbove labelText ->
                    FocusBorder.withLabel
                        { id = idLabel external.id
                        , text = labelText
                        }

                _ ->
                    identity

        withEmpty =
            if internal.query == "" then
                FocusBorder.empty

            else
                identity

        withActionIcon =
            if internal.showListbox then
                FocusBorder.withActionIcon Icon.ArrowDropUp UserPressedArrowDropUp

            else
                FocusBorder.withActionIcon Icon.ArrowDropDown UserPressedArrowDropDown

        withMenu =
            FocusBorder.withMenu
                { onViewport = OnViewport
                , viewport = internal.viewport
                , menu =
                    if internal.showListbox then
                        viewMenu external internal

                    else
                        none
                }
    in
    column
        [ width fill
        , onKeyDown
            [ Decode.field "key" Decode.string
                |> Decode.andThen
                    (\key ->
                        case key of
                            "ArrowDown" ->
                                Decode.succeed UserPressedArrowDown

                            _ ->
                                Decode.fail "not handling that key here"
                    )
            ]
        ]
        [ viewInput external internal
            |> el
                [ width fill
                , height (px 56)
                ]
            |> focusBorder
            |> FocusBorder.withHelp external.help
            |> withSupportingText
            |> withLabel
            |> withEmpty
            |> FocusBorder.onClear UserPressedClearQuery
            |> withActionIcon
            |> Bool.apply (FocusBorder.withHoverableActionIcon Icon.ContentCopy UserCopied) external.onCopy
            |> withMenu
            |> FocusBorder.toElement
            |> el
                [ width fill
                , style "cursor" "text"
                , Events.onClick UserClickedContainer
                , Events.onMouseDown UserPressedMouseOnListbox
                , Events.onMouseUp UserReleasedMouseOnListbox
                ]
        ]


viewInput : External -> Internal -> Element Msg
viewInput external internal =
    Input.text
        [ width fill
        , paddingXY 16 0
        , centerY
        , style "letter-spacing" "0.15px"
        , style "line-height" "24px"
        , style "height" "24px"
        , Font.size 18
        , Background.color transparent
        , Border.width 0
        , Background.color Color.transparent
        , id (idInput external.id)
        , preventDefaultOnKeyDown
            [ Listbox.preventDefaultOnKeyDown (instance external)
                (Decode.field "key" Decode.string
                    |> Decode.andThen
                        (\key ->
                            case key of
                                "Escape" ->
                                    Decode.succeed ( UserPressedEscape, True )

                                _ ->
                                    Decode.fail "not handling that key here"
                        )
                )
            ]
        , Events.onLoseFocus UserUnfocusedQuery
        ]
        { onChange = UserChangedQuery
        , text = internal.query
        , placeholder = Nothing
        , label = Input.labelHidden "Query"
        }


instance : External -> Listbox.Instance OptionCombobox Msg
instance external =
    { id = external.id ++ "-listbox"
    , label =
        case external.label of
            LabelAbove _ ->
                Listbox.labelledBy (idLabel external.id)

            LabelledBy id ->
                Listbox.labelledBy id
    , lift = ListboxMsg external.id
    }


viewMenu : External -> Internal -> Element Msg
viewMenu external internal =
    el
        [ width fill
        , Events.onMouseDown UserPressedMouseOnListbox
        , Events.onMouseUp UserReleasedMouseOnListbox
        , stopPropagationOnClick NoOp
        ]
        (Listbox.view
            { uniqueId = .id
            , focusable = False
            , viewOption = \_ -> OptionCombobox.view external.renderSubLabels
            , heightMaximum = 160
            , attrsListbox =
                [ Border.rounded 4
                , Background.color Color.surface
                , Ui.clipX
                , Elevation.z8
                ]
            }
            (instance external)
            external.options
            internal.listbox
            []
        )


idInput : String -> String
idInput id =
    id


idLabel : String -> String
idLabel id =
    id ++ "-label"
