{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Molecule.NavigationRail exposing
    ( Item, item, active, newTab
    , NavigationRail, fromItems
    , view
    )

{-|

@docs Item, item, active, newTab
@docs NavigationRail, fromItems
@docs view

-}

import Element
    exposing
        ( Element
        , centerX
        , centerY
        , column
        , el
        , fill
        , focused
        , height
        , link
        , mouseDown
        , mouseOver
        , newTabLink
        , px
        , spacing
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Fluent
    exposing
        ( Fluent
        , toElement
        )
import Ui.Atom.Icon as Icon exposing (Icon)
import Ui.Theme.Color as Color
    exposing
        ( onSurface
        , onSurfaceVariant
        , secondaryContainer
        , transparent
        )


type Item
    = Item DataItem


type alias DataItem =
    { icon : Icon
    , label : Fluent
    , url : String
    , active : Bool
    , newTab : Bool
    }


item : String -> Fluent -> Icon -> Item
item url label icon =
    Item
        { icon = icon
        , label = label
        , url = url
        , active = False
        , newTab = False
        }


active : Item -> Item
active (Item data) =
    Item { data | active = True }


newTab : Item -> Item
newTab (Item data) =
    Item { data | newTab = True }


type NavigationRail
    = NavigationRail DataNavigationRail


type alias DataNavigationRail =
    { items : List Item
    }


fromItems : List Item -> NavigationRail
fromItems items =
    NavigationRail
        { items = items
        }


view : NavigationRail -> Element msg
view (NavigationRail data) =
    data.items
        |> List.map viewItem
        |> column
            [ width (px 80)
            , spacing 12
            , centerY
            ]
        |> el
            [ height fill
            ]


viewItem : Item -> Element msg
viewItem (Item data) =
    [ viewButton data
    , viewLabel data
    ]
        |> column
            [ spacing 4
            , centerX
            ]


viewButton : DataItem -> Element msg
viewButton data =
    let
        colorContainer =
            if data.active then
                secondaryContainer

            else
                transparent
    in
    { url = data.url
    , label =
        data.icon
            |> Icon.viewNormal
            |> el
                [ centerX
                , centerY
                ]
    }
        |> (if data.newTab then
                newTabLink

            else
                link
           )
            [ width (px 56)
            , height (px 32)
            , Border.rounded 16
            , Background.color colorContainer
            , mouseDown [ Background.color (Color.pressedWith onSurface colorContainer) ]
            , mouseOver [ Background.color (Color.hoveredWith onSurface colorContainer) ]
            , focused [ Background.color (Color.focusedWith onSurface colorContainer) ]
            , centerX
            ]


viewLabel : DataItem -> Element msg
viewLabel data =
    data.label
        |> toElement
        |> el
            [ Font.size 12
            , Font.color
                (if data.active then
                    onSurface

                 else
                    onSurfaceVariant
                )
            , Font.semiBold
            , centerX
            ]
