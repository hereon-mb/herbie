module Ui.Molecule.Breadcrumbs exposing
    ( Breadcrumb
    , Breadcrumbs
    , empty
    , from
    , link
    , static
    , toElement
    )

import Element exposing (Element, row, spacing)
import Element.Extra exposing (class)
import Element.Font as Font
import Fluent exposing (Fluent, verbatim)
import Ui.Theme.Typography exposing (body2, body2Strong)


type Breadcrumbs
    = Breadcrumbs Data


type alias Data =
    { breadcrumbs : List Breadcrumb
    , final : Fluent
    }


type Breadcrumb
    = Static Fluent
    | Link DataLink


type alias DataLink =
    { url : String
    , label : Fluent
    }


empty : Breadcrumbs
empty =
    Breadcrumbs
        { breadcrumbs = []
        , final = verbatim "Empty"
        }


from : List Breadcrumb -> Fluent -> Breadcrumbs
from breadcrumbs final =
    Breadcrumbs
        { breadcrumbs = breadcrumbs
        , final = final
        }


static : Fluent -> Breadcrumb
static =
    Static


link : { url : String, label : Fluent } -> Breadcrumb
link =
    Link


toElement : Breadcrumbs -> Element msg
toElement (Breadcrumbs data) =
    [ List.map viewBreadcrumb data.breadcrumbs
    , [ viewFinal data.final ]
    ]
        |> List.concat
        |> List.intersperse separator
        |> row
            [ spacing 8
            ]


viewBreadcrumb : Breadcrumb -> Element msg
viewBreadcrumb breadcrumb =
    case breadcrumb of
        Static label ->
            body2 label

        Link data ->
            Element.link
                [ Font.size 14
                , class "underline__pressed"
                , class "underline__hover"
                , class "underline__focus"
                ]
                { url = data.url
                , label = Fluent.toElement data.label
                }


viewFinal : Fluent -> Element msg
viewFinal label =
    body2Strong label


separator : Element msg
separator =
    "/"
        |> verbatim
        |> body2
