{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Molecule.FabMenu exposing (Config, Item, view)

import Element
    exposing
        ( Attribute
        , Element
        , column
        , el
        , fill
        , focused
        , height
        , mouseDown
        , mouseOver
        , paddingEach
        , paddingXY
        , px
        , row
        , spacing
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Extra exposing (stopPropagationOnClick)
import Element.Font as Font
import Element.Input as Input
import Fluent
    exposing
        ( Fluent
        , toElement
        )
import Ui.Atom.Divider
import Ui.Atom.Icon as Icon exposing (Icon)
import Ui.Theme.Color
    exposing
        ( focusedWith
        , hoveredWith
        , onPrimaryContainer
        , onSurface
        , onSurfaceVariant
        , pressedWith
        , primaryContainer
        , surfaceContainerHighest
        )
import Ui.Theme.Elevation as Elevation
import Ui.Theme.Typography exposing (caption)


type alias Config msg =
    { primaryItem : Item msg
    , secondaryItems : List (Item msg)
    , recentItems : List (Item msg)
    }


type alias Item msg =
    { icon : Icon
    , label : Fluent
    , onPress : msg
    }


view : Config msg -> Element msg
view config =
    column
        [ Border.rounded 4
        , Background.color surfaceContainerHighest
        , width fill
        , Elevation.z6
        ]
        (case ( config.secondaryItems, config.recentItems ) of
            ( [], [] ) ->
                [ viewPrimaryItem config.primaryItem ]

            ( _, [] ) ->
                [ viewPrimaryItem config.primaryItem ]
                    ++ List.map viewOtherItem config.secondaryItems

            ( [], _ ) ->
                [ viewPrimaryItem config.primaryItem ]
                    ++ List.map viewOtherItem config.recentItems

            _ ->
                List.concat
                    [ [ viewPrimaryItem config.primaryItem ]
                    , List.map viewOtherItem config.secondaryItems
                    , [ el
                            [ width fill
                            , paddingXY 0 8
                            ]
                            Ui.Atom.Divider.horizontal
                      ]
                    , [ el
                            [ paddingEach
                                { left = 24
                                , right = 24
                                , top = 0
                                , bottom = 8
                                }
                            , Font.color onSurfaceVariant
                            ]
                            (caption (t "caption--recent-protocols"))
                      ]
                    , List.map viewOtherItem config.recentItems
                    ]
        )


viewPrimaryItem : Item msg -> Element msg
viewPrimaryItem item =
    Input.button
        (stopPropagationOnClick item.onPress
            :: Background.color primaryContainer
            :: mouseDown [ Background.color (pressedWith onPrimaryContainer primaryContainer) ]
            :: mouseOver [ Background.color (hoveredWith onPrimaryContainer primaryContainer) ]
            :: focused [ Background.color (focusedWith onPrimaryContainer primaryContainer) ]
            :: Border.roundEach
                { topLeft = 4
                , topRight = 4
                , bottomLeft = 0
                , bottomRight = 0
                }
            :: itemAttributes
        )
        { onPress = Nothing
        , label = itemLabel item.icon item.label
        }


viewOtherItem : Item msg -> Element msg
viewOtherItem item =
    Input.button
        (stopPropagationOnClick item.onPress
            :: mouseDown [ Background.color (pressedWith onSurface surfaceContainerHighest) ]
            :: mouseOver [ Background.color (hoveredWith onSurface surfaceContainerHighest) ]
            :: focused [ Background.color (focusedWith onSurface surfaceContainerHighest) ]
            :: itemAttributes
        )
        { onPress = Nothing
        , label = itemLabel item.icon item.label
        }


itemAttributes : List (Attribute msg)
itemAttributes =
    [ width fill
    , height (px 56)
    , paddingXY 24 0
    ]


itemLabel : Icon -> Fluent -> Element msg
itemLabel icon label =
    row
        [ width fill
        , spacing 20
        ]
        [ icon
            |> Icon.viewNormal
            |> el [ Font.color onSurfaceVariant ]
        , label
            |> toElement
            |> el
                [ Font.size 16
                , Font.medium
                , Font.color onSurface
                ]
        ]


t : String -> Fluent
t key =
    Fluent.text ("ui-molecule-fab-menu--" ++ key)
