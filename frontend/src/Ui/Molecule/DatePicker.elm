{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Molecule.DatePicker exposing
    ( datePicker
    , main, Model, Msg
    )

{-|

@docs datePicker

@docs main, Model, Msg

-}

import Accessibility.Clipboard as Clipboard
import Accessibility.Key
import Bool.Apply as Bool
import Browser
import Browser.Dom
import Derberos.Date.Calendar
import Derberos.Date.Core
import Derberos.Date.Delta
import Derberos.Date.Utils
import Element as Ui exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Extra as Ui
import Element.Font as Font
import Element.Input as Input
import Element.Keyed as Keyed
import Fluent exposing (Fluent)
import Html exposing (Html)
import Html.Attributes
import Html.Events
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra as Decode
import Json.Decode.Pipeline as Decode
import Json.Encode as Encode exposing (Value)
import Json.Encode.Extra as Encode
import List.Extra
import Locale exposing (Locale)
import Maybe.Extra as Maybe
import Pointer
import Ports
import Ports.CustomElement as Ports
import Task
import Time exposing (Month(..), Posix, Weekday(..), Zone)
import Time.Format
import TimeZone
import Triple
import Ui.Atom.Button as Button exposing (button)
import Ui.Atom.ButtonIcon as ButtonIcon exposing (buttonIcon)
import Ui.Atom.ButtonToggle as ButtonToggle
import Ui.Atom.Divider as Divider
import Ui.Atom.FocusBorder as FocusBorder exposing (focusBorder)
import Ui.Atom.Icon as Icon
import Ui.Atom.Listbox as Listbox exposing (Listbox)
import Ui.Theme.Color as Color
import Ui.Theme.Elevation
import Ui.Theme.Input as Input
import Ui.Theme.Typography as Typography


nameNode : String
nameNode =
    "ui-molecule-date-picker"


datePicker :
    { id : Pointer.Pointer
    , label : Fluent
    , help : List Fluent
    , initialDate : Maybe Posix
    , locale : Locale
    , onChange : Maybe Posix -> msg
    , onCopy : Maybe msg
    , onPaste : Maybe ( String, msg, String -> Maybe msg )
    , withTime : Bool
    , supportingText : Maybe Fluent
    }
    -> Element msg
datePicker config =
    Ui.el
        [ Ui.width Ui.fill ]
        (Ui.html (datePickerHelp config))


datePickerHelp :
    { id : Pointer.Pointer
    , label : Fluent
    , help : List Fluent
    , initialDate : Maybe Posix
    , locale : Locale
    , onChange : Maybe Posix -> msg
    , onCopy : Maybe msg
    , onPaste : Maybe ( String, msg, String -> Maybe msg )
    , withTime : Bool
    , supportingText : Maybe Fluent
    }
    -> Html msg
datePickerHelp config =
    Html.node nameNode
        (List.concat
            [ [ Html.Attributes.property "elmData"
                    (encode
                        { id = Pointer.toString config.id
                        , label = config.label
                        , help = config.help
                        , initialDate = config.initialDate
                        , locale = config.locale
                        , withTime = config.withTime
                        , supportingText = config.supportingText
                        , onCopy = Maybe.isJust config.onCopy
                        }
                    )
              , Html.Events.on "elm-data-changed"
                    (Decode.oneOf
                        [ Decode.map Just Decode.posix
                        , Decode.succeed Nothing
                        ]
                        |> Decode.field "detail"
                        |> Decode.map config.onChange
                    )
              ]
            , Maybe.toList (Maybe.map Clipboard.onCopyHtml config.onCopy)
            , Maybe.toList (Maybe.map (Triple.apply Clipboard.onPasteHtml) config.onPaste)
            ]
        )
        []


type alias External =
    { id : String
    , label : Fluent
    , help : List Fluent
    , initialDate : Maybe Posix
    , locale : Locale
    , withTime : Bool
    , supportingText : Maybe Fluent
    , onCopy : Bool
    }


encode : External -> Value
encode external =
    Encode.object
        [ ( "id", Encode.string external.id )
        , ( "label", Fluent.encode external.label )
        , ( "help", Encode.list Fluent.encode external.help )
        , ( "initialDate"
          , case external.initialDate of
                Nothing ->
                    Encode.null

                Just initialDate ->
                    Encode.posix initialDate
          )
        , ( "locale", Locale.encode external.locale )
        , ( "withTime", Encode.bool external.withTime )
        , ( "supportingText"
          , case external.supportingText of
                Nothing ->
                    Encode.null

                Just supportingText ->
                    Fluent.encode supportingText
          )
        , ( "onCopy", Encode.bool external.onCopy )
        ]


decoder : Decoder External
decoder =
    Decode.succeed External
        |> Decode.required "id" Decode.string
        |> Decode.required "label" Fluent.decoder
        |> Decode.required "help" (Decode.list Fluent.decoder)
        |> Decode.required "initialDate" (Decode.maybe Decode.posix)
        |> Decode.required "locale" Locale.decoder
        |> Decode.required "withTime" Decode.bool
        |> Decode.required "supportingText" (Decode.maybe Fluent.decoder)
        |> Decode.required "onCopy" Decode.bool


main : Program Value Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



--  MODEL


type Model
    = Loading External
    | Running External Internal
    | Failed


type alias Internal =
    { zone : Time.Zone
    , now : Posix
    , raw : String
    , focusedDate : Posix
    , selectedDate : Maybe Posix
    , view : View
    , preventClose : Bool
    , hour : String
    , minute : String
    , period : Period
    }


type View
    = Closed
    | ViewMonth DataMonth
    | ViewYear DataYear
    | ViewDay


type alias DataMonth =
    { listbox : Listbox
    }


type alias DataYear =
    { listbox : Listbox
    }


initViewMonth : View
initViewMonth =
    ViewMonth
        { listbox = Listbox.init }


initViewYear : View
initViewYear =
    ViewYear
        { listbox = Listbox.init }


type Period
    = Am
    | Pm



--  INIT


type alias TimeInfo =
    { zone : Time.Zone
    , now : Posix
    }


init : Value -> ( Model, Cmd Msg )
init flags =
    case Decode.decodeValue decoder flags of
        Err _ ->
            ( Failed, Cmd.none )

        Ok external ->
            ( Loading external
            , Task.map2 TimeInfo
                (Task.map Tuple.second TimeZone.getZone
                    |> Task.onError (\_ -> Time.here)
                )
                Time.now
                |> Task.attempt BrowserReturnedTimeInfo
            )



--  UPDATE


type Msg
    = NoOp
    | BrowserReturnedTimeInfo (Result TimeZone.Error TimeInfo)
    | BrowserReturnedCurrentTime Posix
    | ElmDataChanged Value
    | UserChangedRaw String
    | UserPressedCalendar
    | UserPressedEscape String
    | UserPressedEnter String
    | UserPressedSpace String
    | UserPressedArrowUp
    | UserPressedArrowDown
    | UserPressedArrowLeft
    | UserPressedArrowRight
    | UserSelectedDate String Posix
    | UserPressedMonthCurrent
    | UserPressedMonthPrevious
    | UserPressedMonthNext
    | UserPressedYearCurrent
    | UserPressedYearPrevious
    | UserPressedYearNext
    | UserChangedHour String
    | UserChangedMinute String
    | UserChangedPeriod Period
    | UserPressedCancelOnTimePicker
    | UserPressedOkOnTimePicker
    | UserUnfocusedNode
    | UserTabbed
    | UserFocusedContainer
    | UserPressedMouse
    | UserReleasedMouse
    | UserCopied
    | MsgListboxMonth (Listbox.Msg Month)
    | MsgListboxYear (Listbox.Msg Int)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case model of
        Loading external ->
            case msg of
                BrowserReturnedTimeInfo (Err _) ->
                    ( Failed, Cmd.none )

                BrowserReturnedTimeInfo (Ok { zone, now }) ->
                    ( Running external
                        { zone = zone
                        , now = now
                        , raw =
                            external.initialDate
                                |> Maybe.map (posixToString external.withTime external.locale zone)
                                |> Maybe.withDefault ""
                        , focusedDate = Time.millisToPosix 0
                        , selectedDate = external.initialDate
                        , view = Closed
                        , preventClose = False
                        , hour = ""
                        , minute = ""
                        , period = Am
                        }
                    , Cmd.none
                    )

                _ ->
                    ( model, Cmd.none )

        Running external internal ->
            updateHelp msg external internal

        Failed ->
            ( model, Cmd.none )


updateHelp : Msg -> External -> Internal -> ( Model, Cmd Msg )
updateHelp msg external internal =
    case msg of
        NoOp ->
            ( Running external internal, Cmd.none )

        BrowserReturnedTimeInfo (Err _) ->
            ( Running external internal, Cmd.none )

        BrowserReturnedTimeInfo (Ok { zone, now }) ->
            ( Running external
                { internal
                    | zone = zone
                    , now = now
                }
            , Cmd.none
            )

        BrowserReturnedCurrentTime now ->
            ( Running external { internal | now = now }
            , Cmd.none
            )

        ElmDataChanged value ->
            case Decode.decodeValue decoder value of
                Err _ ->
                    ( Running external internal, Cmd.none )

                Ok newExternal ->
                    ( Running newExternal internal
                    , Cmd.none
                    )

        UserChangedRaw raw ->
            case posixFromString external internal raw of
                Nothing ->
                    ( Running external { internal | raw = raw }
                    , Cmd.none
                    )

                Just selectedDate ->
                    ( Running external
                        { internal
                            | raw = raw
                            , selectedDate = Just selectedDate
                        }
                    , selectedDate
                        |> Encode.posix
                        |> Ports.changeElmData
                    )

        UserPressedCalendar ->
            let
                selectedDate =
                    posixFromString external internal internal.raw

                focusedDate =
                    Maybe.withDefault internal.now selectedDate

                newInternal =
                    { internal
                        | view =
                            if internal.view == Closed then
                                ViewDay

                            else
                                Closed
                        , focusedDate = focusedDate
                        , selectedDate = selectedDate
                        , hour = hourString external internal focusedDate
                        , minute = minuteString internal focusedDate
                        , period = periodFromPosix internal focusedDate
                    }
            in
            ( Running external newInternal
            , focus (idFocusedDay newInternal)
            )

        UserPressedEscape idInput ->
            ( Running external { internal | view = Closed }
            , Cmd.batch
                [ focus idInput
                , Ports.selectInputTextOf idInput
                ]
            )

        UserPressedEnter idInput ->
            case internal.view of
                Closed ->
                    ( Running external internal, Cmd.none )

                ViewMonth _ ->
                    ( Running external internal, Cmd.none )

                ViewDay ->
                    selectFocusedDate external idInput internal

                ViewYear _ ->
                    ( Running external internal, Cmd.none )

        UserPressedSpace idInput ->
            case internal.view of
                Closed ->
                    ( Running external internal, Cmd.none )

                ViewMonth _ ->
                    ( Running external internal, Cmd.none )

                ViewDay ->
                    selectFocusedDate external idInput internal

                ViewYear _ ->
                    ( Running external internal, Cmd.none )

        UserPressedArrowUp ->
            case internal.view of
                Closed ->
                    ( Running external internal, Cmd.none )

                ViewMonth _ ->
                    ( Running external internal, Cmd.none )

                ViewDay ->
                    addDaysToFocusedDate external internal -7

                ViewYear _ ->
                    ( Running external internal, Cmd.none )

        UserPressedArrowDown ->
            case internal.view of
                Closed ->
                    ( Running external internal, Cmd.none )

                ViewMonth _ ->
                    ( Running external internal, Cmd.none )

                ViewDay ->
                    addDaysToFocusedDate external internal 7

                ViewYear _ ->
                    ( Running external internal, Cmd.none )

        UserPressedArrowLeft ->
            case internal.view of
                Closed ->
                    ( Running external internal, Cmd.none )

                ViewMonth _ ->
                    ( Running external internal, Cmd.none )

                ViewDay ->
                    addDaysToFocusedDate external internal -1

                ViewYear _ ->
                    ( Running external internal, Cmd.none )

        UserPressedArrowRight ->
            case internal.view of
                Closed ->
                    ( Running external internal, Cmd.none )

                ViewMonth _ ->
                    ( Running external internal, Cmd.none )

                ViewDay ->
                    addDaysToFocusedDate external internal 1

                ViewYear _ ->
                    ( Running external internal, Cmd.none )

        UserSelectedDate idInput posix ->
            if external.withTime then
                ( Running external
                    { internal
                        | focusedDate = posix
                        , selectedDate = Just posix
                    }
                , Cmd.none
                )

            else
                ( Running external
                    { internal
                        | raw = posixToString external.withTime external.locale internal.zone posix
                        , view = Closed
                    }
                , Cmd.batch
                    [ focus idInput
                    , Ports.selectInputTextOf idInput
                    , posix
                        |> Encode.posix
                        |> Ports.changeElmData
                    ]
                )

        UserPressedMonthCurrent ->
            case internal.view of
                Closed ->
                    ( Running external internal, Cmd.none )

                ViewMonth _ ->
                    let
                        newInternal =
                            { internal
                                | view = ViewDay
                                , preventClose = True
                            }
                    in
                    ( Running external newInternal
                    , focus (idFocusedDay newInternal)
                    )

                ViewYear _ ->
                    ( Running external internal, Cmd.none )

                ViewDay ->
                    ( Running
                        external
                        { internal
                            | view = initViewMonth
                            , preventClose = True
                        }
                    , Task.attempt (\_ -> UserFocusedContainer)
                        (Listbox.focus
                            { id = idListboxMonth external.id
                            , label = Listbox.labelledBy (idLabel external.id)
                            , lift = MsgListboxMonth
                            }
                        )
                    )

        UserPressedMonthPrevious ->
            ( addMonthsToFocusedDate external -1 internal
            , focus idPreviousMonth
            )

        UserPressedMonthNext ->
            ( addMonthsToFocusedDate external 1 internal
            , focus idNextMonth
            )

        UserPressedYearCurrent ->
            case internal.view of
                Closed ->
                    ( Running external internal, Cmd.none )

                ViewMonth _ ->
                    ( Running external internal, Cmd.none )

                ViewYear _ ->
                    let
                        newInternal =
                            { internal
                                | view = ViewDay
                                , preventClose = True
                            }
                    in
                    ( Running external newInternal
                    , focus (idFocusedDay newInternal)
                    )

                ViewDay ->
                    let
                        newInternal =
                            { internal
                                | view = initViewYear
                                , preventClose = True
                            }
                    in
                    ( Running external newInternal
                    , Task.attempt (\_ -> UserFocusedContainer)
                        (Listbox.focus
                            { id = idListboxYear external.id
                            , label = Listbox.labelledBy (idLabel external.id)
                            , lift = MsgListboxYear
                            }
                        )
                    )

        UserPressedYearPrevious ->
            addYearsToFocusedDate external internal -1

        UserPressedYearNext ->
            addYearsToFocusedDate external internal 1

        UserChangedHour hour ->
            ( Running external { internal | hour = hour }
            , Cmd.none
            )

        UserChangedMinute minute ->
            ( Running external { internal | minute = minute }
            , Cmd.none
            )

        UserChangedPeriod newPeriod ->
            ( Running external { internal | period = newPeriod }
            , Cmd.none
            )

        UserPressedCancelOnTimePicker ->
            let
                previousDate =
                    Maybe.withDefault internal.focusedDate
                        (posixFromString external internal internal.raw)
            in
            ( Running external
                { internal
                    | view = Closed
                    , hour = hourString external internal previousDate
                    , minute = minuteString internal previousDate
                    , period = periodFromPosix internal previousDate
                }
            , Cmd.none
            )

        UserPressedOkOnTimePicker ->
            case
                ( String.toInt internal.hour
                , String.toInt internal.minute
                )
            of
                ( Just hour, Just minute ) ->
                    if
                        (1 <= hour)
                            && (hour <= 24)
                            && (Time.Format.fromLocale external.locale /= Time.Format.EnUs || hour <= 12)
                            && (0 <= minute)
                            && (minute <= 59)
                    then
                        let
                            newPosix =
                                setTime external internal hour minute internal.period
                        in
                        ( Running external
                            { internal
                                | raw = posixToString external.withTime external.locale internal.zone newPosix
                                , selectedDate = Just newPosix
                                , view = Closed
                            }
                        , newPosix
                            |> Encode.posix
                            |> Ports.changeElmData
                        )

                    else
                        ( Running external internal, Cmd.none )

                _ ->
                    ( Running external internal, Cmd.none )

        UserUnfocusedNode ->
            if internal.preventClose then
                ( Running external internal, Cmd.none )

            else
                ( Running external { internal | view = Closed }
                , Cmd.none
                )

        UserTabbed ->
            ( Running external { internal | preventClose = True }
            , Cmd.none
            )

        UserFocusedContainer ->
            ( Running external { internal | preventClose = False }
            , Cmd.none
            )

        UserPressedMouse ->
            ( Running external { internal | preventClose = True }
            , Cmd.none
            )

        UserReleasedMouse ->
            ( Running external { internal | preventClose = False }
            , Cmd.none
            )

        UserCopied ->
            ( Running external internal, Ports.copy () )

        MsgListboxMonth msgListbox ->
            case internal.view of
                ViewMonth data ->
                    let
                        ( listboxUpdated, cmdListbox, selectionUpdated ) =
                            Listbox.update
                                { uniqueId = monthToInt >> String.fromInt }
                                monthsAll
                                msgListbox
                                data.listbox
                                [ month ]

                        newFocusedDate =
                            internal.focusedDate
                                |> Derberos.Date.Core.posixToCivil
                                |> setMonth
                                |> Derberos.Date.Core.civilToPosix

                        setMonth civil =
                            { civil | month = monthToInt monthUpdated }

                        monthUpdated =
                            selectionUpdated
                                |> List.head
                                |> Maybe.withDefault month

                        month =
                            Time.toMonth internal.zone internal.focusedDate
                    in
                    if month == monthUpdated then
                        ( Running external
                            { internal | view = ViewMonth { data | listbox = listboxUpdated } }
                        , Cmd.map MsgListboxMonth cmdListbox
                        )

                    else
                        let
                            internalUpdated =
                                { internal
                                    | view = ViewDay
                                    , focusedDate = newFocusedDate
                                }
                        in
                        ( Running external internalUpdated
                        , focus (idFocusedDay internalUpdated)
                        )

                _ ->
                    ( Running external internal, Cmd.none )

        MsgListboxYear msgListbox ->
            case internal.view of
                ViewYear data ->
                    let
                        ( listboxUpdated, cmdListbox, selectionUpdated ) =
                            Listbox.update
                                { uniqueId = String.fromInt }
                                (availableYears internal.zone internal.now)
                                msgListbox
                                data.listbox
                                [ year ]

                        newFocusedDate =
                            internal.focusedDate
                                |> Derberos.Date.Core.posixToCivil
                                |> setYear
                                |> Derberos.Date.Core.civilToPosix

                        setYear civil =
                            { civil | year = yearUpdated }

                        yearUpdated =
                            selectionUpdated
                                |> List.head
                                |> Maybe.withDefault year

                        year =
                            Time.toYear internal.zone internal.focusedDate
                    in
                    if year == yearUpdated then
                        ( Running external
                            { internal | view = ViewYear { data | listbox = listboxUpdated } }
                        , Cmd.map MsgListboxYear cmdListbox
                        )

                    else
                        let
                            internalUpdated =
                                { internal
                                    | view = ViewDay
                                    , focusedDate = newFocusedDate
                                }
                        in
                        ( Running external internalUpdated
                        , focus (idFocusedDay internalUpdated)
                        )

                _ ->
                    ( Running external internal, Cmd.none )


addDaysToFocusedDate : External -> Internal -> Int -> ( Model, Cmd Msg )
addDaysToFocusedDate external internal amount =
    let
        newInternal =
            { internal
                | focusedDate = Derberos.Date.Delta.addDays amount internal.focusedDate
                , preventClose = True
            }
    in
    ( Running external newInternal
    , focus (idFocusedDay newInternal)
    )


addYearsToFocusedDate : External -> Internal -> Int -> ( Model, Cmd Msg )
addYearsToFocusedDate external internal amount =
    let
        newInternal =
            { internal
                | focusedDate = Derberos.Date.Delta.addYears amount internal.focusedDate
                , preventClose = True
            }
    in
    ( Running external newInternal
    , focus (idFocusedYear newInternal)
    )


addMonthsToFocusedDate : External -> Int -> Internal -> Model
addMonthsToFocusedDate external amount internal =
    Running external
        { internal
            | focusedDate = Derberos.Date.Delta.addMonths amount internal.zone internal.focusedDate
            , preventClose = True
        }


selectFocusedDate : External -> String -> Internal -> ( Model, Cmd Msg )
selectFocusedDate external idInput internal =
    if external.withTime then
        ( Running external
            { internal | selectedDate = Just internal.focusedDate }
        , Cmd.none
        )

    else
        ( Running external
            { internal
                | raw =
                    posixToString
                        external.withTime
                        external.locale
                        internal.zone
                        internal.focusedDate
                , selectedDate = Just internal.focusedDate
                , view = Closed
            }
        , Cmd.batch
            [ focus idInput
            , Ports.selectInputTextOf idInput
            , internal.focusedDate
                |> Encode.posix
                |> Ports.changeElmData
            ]
        )


setTime : External -> Internal -> Int -> Int -> Period -> Posix
setTime external internal hour minute period =
    let
        adjustTimezoneOffset posix =
            let
                millis =
                    Time.posixToMillis posix
            in
            millis - (60 * 1000 * Derberos.Date.Core.getTzOffset internal.zone posix)

        addTime millis =
            millis
                + (60 * 60 * 1000 * hoursToAdd)
                + (60 * 1000 * minute)

        hoursToAdd =
            case Time.Format.fromLocale external.locale of
                Time.Format.Iso8601 ->
                    hour

                Time.Format.EnUs ->
                    if hour == 12 then
                        hoursOffsetFromPeriod

                    else
                        hour + hoursOffsetFromPeriod

                Time.Format.De ->
                    hour

        hoursOffsetFromPeriod =
            case period of
                Am ->
                    0

                Pm ->
                    12
    in
    internal.selectedDate
        |> Maybe.map
            (Derberos.Date.Utils.resetTime
                >> adjustTimezoneOffset
                >> addTime
                >> Time.millisToPosix
            )
        |> Maybe.withDefault internal.now


posixFromString : External -> Internal -> String -> Maybe Posix
posixFromString external internal =
    if external.withTime then
        Time.Format.posixFromDateTime (Time.Format.fromLocale external.locale) internal.zone

    else
        Time.Format.posixFromDate (Time.Format.fromLocale external.locale) internal.zone


posixToString : Bool -> Locale -> Time.Zone -> Posix -> String
posixToString withTime locale zone =
    if withTime then
        Time.Format.posixToDateTime (Time.Format.fromLocale locale) zone

    else
        Time.Format.posixToDate (Time.Format.fromLocale locale) zone


hourString : External -> Internal -> Posix -> String
hourString external internal posix =
    String.fromInt
        (case Time.Format.fromLocale external.locale of
            Time.Format.Iso8601 ->
                Time.toHour internal.zone posix

            Time.Format.EnUs ->
                case modBy 12 (Time.toHour internal.zone posix) of
                    0 ->
                        12

                    hour ->
                        hour

            Time.Format.De ->
                Time.toHour internal.zone posix
        )


minuteString : Internal -> Posix -> String
minuteString internal posix =
    String.fromInt (Time.toMinute internal.zone posix)


periodFromPosix : Internal -> Posix -> Period
periodFromPosix internal posix =
    if 0 <= Time.toHour internal.zone posix && Time.toHour internal.zone posix < 12 then
        Am

    else
        Pm


focus : String -> Cmd Msg
focus theId =
    Task.attempt (\_ -> NoOp) (Browser.Dom.focus theId)



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        Loading _ ->
            Sub.none

        Running _ internal ->
            Sub.batch
                [ Ports.elmDataChanged ElmDataChanged
                , Time.every (3 * 1000) BrowserReturnedCurrentTime
                , case internal.view of
                    ViewMonth data ->
                        Sub.map MsgListboxMonth (Listbox.subscriptions data.listbox)

                    _ ->
                        Sub.none
                ]

        Failed ->
            Sub.none



--  VIEW


view : Model -> Html Msg
view model =
    case model of
        Loading _ ->
            Html.node nameNode
                [ Html.Attributes.style "display" "contents" ]
                []

        Running external internal ->
            let
                setZIndex attrs =
                    if internal.view /= Closed then
                        Ui.htmlAttribute (Html.Attributes.style "z-index" "1") :: attrs

                    else
                        attrs
            in
            Html.node nameNode
                [ Html.Attributes.style "display" "contents" ]
                [ Ui.layoutWith
                    { options =
                        [ Ui.focusStyle
                            { borderColor = Nothing
                            , backgroundColor = Nothing
                            , shadow = Nothing
                            }
                        , Ui.noStaticStyleSheet
                        ]
                    }
                    ([ Ui.width Ui.fill
                     , Ui.height Ui.fill
                     , Font.family
                        [ Font.typeface "Work Sans"
                        , Font.sansSerif
                        ]
                     ]
                        |> setZIndex
                    )
                    (viewHelp external internal)
                ]

        Failed ->
            Html.node nameNode
                [ Html.Attributes.style "display" "contents" ]
                [ Html.text "Could not initialize custom element" ]


viewHelp : External -> Internal -> Element Msg
viewHelp external internal =
    let
        withSupportingText =
            case external.supportingText of
                Nothing ->
                    identity

                Just supportingText ->
                    FocusBorder.withSupportingText supportingText

        withLabel =
            FocusBorder.withLabel
                { id = idLabel external.id
                , text = external.label
                }

        withEmpty =
            if internal.raw == "" then
                FocusBorder.empty

            else
                identity
    in
    { onChange = UserChangedRaw
    , text = internal.raw
    , placeholder = Nothing
    , label = Input.labelHidden "Date"
    }
        |> Input.text
            [ Ui.paddingXY 16 19
            , Ui.id external.id
            , Ui.width Ui.fill
            , Ui.height Ui.fill
            , Ui.style "letter-spacing" "0.15px"
            , Ui.style "line-height" "24px"
            , Font.size 18
            , Border.width 0
            , Background.color Color.transparent
            ]
        |> Ui.el
            [ Ui.width Ui.fill
            , Ui.height (Ui.px 56)
            ]
        |> focusBorder
        |> FocusBorder.withHelp external.help
        |> FocusBorder.withActionIcon Icon.Today UserPressedCalendar
        |> withSupportingText
        |> withLabel
        |> withEmpty
        |> Bool.apply (FocusBorder.withHoverableActionIcon Icon.ContentCopy UserCopied) external.onCopy
        |> FocusBorder.toElement
        |> Ui.el
            [ Ui.width Ui.fill
            , Ui.below (viewPickerOverlay external internal)
            ]



--  PICKER BUTTON OVERLAY
--  PICKER OVERLAY


viewPickerOverlay : External -> Internal -> Element Msg
viewPickerOverlay external internal =
    Ui.el
        [ Ui.moveUp
            (if external.supportingText == Nothing then
                0

             else
                21
            )
        , Ui.alignRight
        , Ui.onMouseDown UserPressedMouse
        , Ui.onMouseUp UserReleasedMouse
        , Ui.onFocusIn UserFocusedContainer
        , Ui.preventDefaultOnKeyDown
            [ Accessibility.Key.escape ( UserPressedEscape external.id, True ) ]
        ]
        (Ui.el
            [ Ui.width Ui.fill
            , Ui.height Ui.fill
            , Border.rounded 4
            , Ui.Theme.Elevation.z4
            , Background.color Color.surface
            ]
            (viewPicker external internal)
        )


viewPicker : External -> Internal -> Element Msg
viewPicker external internal =
    case internal.view of
        Closed ->
            Ui.none

        ViewMonth data ->
            viewPickerMonth external internal data

        ViewYear data ->
            viewPickerYear external internal data

        ViewDay ->
            viewPickerDay external internal



-- PICKER MONTH


viewPickerMonth : External -> Internal -> DataMonth -> Element Msg
viewPickerMonth external internal data =
    Ui.column
        [ Ui.width (Ui.px (7 * 40 + 6 * 4 + 2 * 16))
        , Ui.height Ui.fill
        ]
        [ Keyed.el [ Ui.width Ui.fill ]
            ( "picker-month-menu"
            , viewPickerMonthMenu internal
            )
        , Divider.horizontal
        , Ui.el
            [ Ui.width Ui.fill
            , Ui.height Ui.fill
            , Ui.onFocusOut UserUnfocusedNode
            , Ui.preventDefaultOnKeyDown
                [ Accessibility.Key.tabBack ( UserTabbed, False )
                ]
            ]
            (viewMonths external internal data)
        ]


viewPickerMonthMenu : Internal -> Element Msg
viewPickerMonthMenu internal =
    Ui.row
        [ Ui.width Ui.fill
        , Ui.paddingEach
            { top = 16
            , bottom = 12
            , left = 16
            , right = 16
            }
        ]
        [ viewButtonMonthCurrentActive internal
        , Ui.el [ Ui.alignRight ]
            (viewInfoYearCurrent internal)
        ]


viewButtonMonthCurrentActive : Internal -> Element Msg
viewButtonMonthCurrentActive internal =
    Ui.row []
        [ selectSpacer
        , Time.toMonth internal.zone internal.focusedDate
            |> monthShort
            |> Fluent.verbatim
            |> button UserPressedMonthCurrent
            |> Button.withFollowingIcon Icon.ArrowDropUp
            |> Button.withFormat Button.Text
            |> Button.narrow
            |> Button.withWidth (Ui.minimum 80 Ui.shrink)
            |> Button.withAttributes
                [ Ui.onBlur UserUnfocusedNode
                , Ui.preventDefaultOnKeyDown
                    [ Accessibility.Key.tab ( UserTabbed, False )
                    , Accessibility.Key.enter ( UserPressedMonthCurrent, True )
                    , Accessibility.Key.space ( UserPressedMonthCurrent, True )
                    ]
                ]
            |> Button.view
        ]


viewInfoYearCurrent : Internal -> Element Msg
viewInfoYearCurrent internal =
    Ui.row []
        [ viewDisabledButton
            (String.fromInt (Time.toYear internal.zone internal.focusedDate))
        , selectSpacer
        ]


viewDisabledButton : String -> Element Msg
viewDisabledButton text =
    Ui.el
        [ Ui.height (Ui.px 40)
        , Ui.width (Ui.minimum 80 Ui.shrink)
        , Border.rounded 20
        , Font.color (Color.withOpacity 0.38 Color.onSurface)
        , Background.color Color.transparent
        ]
        (Ui.row
            [ Ui.centerY
            , Ui.centerX
            , Ui.paddingEach
                { top = 0
                , bottom = 0
                , left = 12
                , right = 4
                }
            , Ui.spacing 8
            ]
            [ Ui.el [ Ui.centerY ]
                (Typography.button
                    (Fluent.verbatim text)
                )
            , Ui.el [ Ui.width (Ui.px 18) ] Ui.none
            ]
        )


viewMonths : External -> Internal -> DataMonth -> Element Msg
viewMonths external internal data =
    Listbox.view
        { uniqueId = monthToInt >> String.fromInt
        , focusable = True
        , viewOption = viewMonth
        , heightMaximum = 6 * 40 + 5 * 4 + 8 - 1
        , attrsListbox = []
        }
        { id = idListboxMonth external.id
        , label = Listbox.labelledBy (idLabel external.id)
        , lift = MsgListboxMonth
        }
        monthsAll
        data.listbox
        [ Time.toMonth internal.zone internal.focusedDate ]


viewMonth : Bool -> Month -> Element Never
viewMonth selected month =
    Ui.row
        [ Ui.width Ui.fill
        , Ui.paddingEach
            { top = 0
            , bottom = 0
            , left = 4
            , right = 4
            }
        ]
        [ Ui.el
            [ Ui.width (Ui.px 48)
            , Ui.height (Ui.px 48)
            , Ui.paddingEach
                { top = 0
                , bottom = 0
                , left = 0
                , right = 16
                }
            , Ui.alignLeft
            , Ui.centerY
            ]
            (if selected then
                Ui.el [ Ui.centerY ] (Icon.viewNormal Icon.Check)

             else
                Ui.none
            )
        , Ui.paragraph
            [ Ui.width Ui.fill
            , Ui.centerY
            , Font.color Color.onSurface
            ]
            [ Typography.body1
                (Fluent.verbatim
                    (monthLong month)
                )
            ]
        ]



--  PICKER YEAR


viewPickerYear : External -> Internal -> DataYear -> Element Msg
viewPickerYear external internal data =
    Ui.column
        [ Ui.width (Ui.px (7 * 40 + 6 * 4 + 2 * 16))
        , Ui.height Ui.fill
        ]
        [ Keyed.el [ Ui.width Ui.fill ]
            ( "picker-year-menu"
            , viewPickerYearMenu internal
            )
        , Divider.horizontal
        , Ui.el
            [ Ui.width Ui.fill
            , Ui.height Ui.fill
            , Ui.onFocusOut UserUnfocusedNode
            , Ui.preventDefaultOnKeyDown
                [ Accessibility.Key.tabBack ( UserTabbed, False )
                ]
            ]
            (viewYears external internal data)
        ]


viewPickerYearMenu : Internal -> Element Msg
viewPickerYearMenu internal =
    Ui.row
        [ Ui.width Ui.fill
        , Ui.paddingEach
            { top = 16
            , bottom = 12
            , left = 16
            , right = 16
            }
        ]
        [ viewInfoMonthCurrent internal
        , Ui.el [ Ui.alignRight ]
            (viewButtonYearCurrentActive internal)
        ]


viewInfoMonthCurrent : Internal -> Element Msg
viewInfoMonthCurrent internal =
    Ui.row []
        [ selectSpacer
        , viewDisabledButton
            (monthShort (Time.toMonth internal.zone internal.focusedDate))
        ]


viewButtonYearCurrentActive : Internal -> Element Msg
viewButtonYearCurrentActive internal =
    Ui.row []
        [ Time.toYear internal.zone internal.focusedDate
            |> String.fromInt
            |> Fluent.verbatim
            |> button UserPressedYearCurrent
            |> Button.withFollowingIcon Icon.ArrowDropUp
            |> Button.withFormat Button.Text
            |> Button.narrow
            |> Button.withWidth (Ui.minimum 80 Ui.shrink)
            |> Button.withAttributes
                [ Ui.onBlur UserUnfocusedNode
                , Ui.preventDefaultOnKeyDown
                    [ Accessibility.Key.tab ( UserTabbed, False )
                    , Accessibility.Key.enter ( UserPressedYearCurrent, True )
                    , Accessibility.Key.space ( UserPressedYearCurrent, True )
                    ]
                ]
            |> Button.view
        , selectSpacer
        ]


selectSpacer : Element Msg
selectSpacer =
    Ui.el [ Ui.width (Ui.px 36) ] Ui.none


viewYears : External -> Internal -> DataYear -> Element Msg
viewYears external internal data =
    let
        yearCurrent =
            Time.toYear internal.zone internal.focusedDate
    in
    Listbox.view
        { uniqueId = String.fromInt
        , focusable = True
        , viewOption = viewYear
        , heightMaximum = 6 * 40 + 5 * 4 + 8 - 1
        , attrsListbox = []
        }
        { id = idListboxYear external.id
        , label = Listbox.labelledBy (idLabel external.id)
        , lift = MsgListboxYear
        }
        (availableYears internal.zone internal.now)
        data.listbox
        [ yearCurrent ]


viewYear : Bool -> Int -> Element Never
viewYear selected year =
    Ui.row
        [ Ui.width Ui.fill
        , Ui.paddingEach
            { top = 0
            , bottom = 0
            , left = 4
            , right = 4
            }
        ]
        [ Ui.el
            [ Ui.width (Ui.px 48)
            , Ui.height (Ui.px 48)
            , Ui.paddingEach
                { top = 0
                , bottom = 0
                , left = 0
                , right = 16
                }
            , Ui.alignLeft
            , Ui.centerY
            ]
            (if selected then
                Ui.el [ Ui.centerY ] (Icon.viewNormal Icon.Check)

             else
                Ui.none
            )
        , Ui.paragraph
            [ Ui.width Ui.fill
            , Ui.centerY
            , Font.color Color.onSurface
            ]
            [ Typography.body1
                (Fluent.verbatim
                    (String.fromInt year)
                )
            ]
        ]


availableYears : Time.Zone -> Posix -> List Int
availableYears zone focusedDate =
    let
        year =
            Time.toYear zone focusedDate
    in
    List.range (year - 10) (year + 10)



--  PICKER DAY


viewPickerDay : External -> Internal -> Element Msg
viewPickerDay external internal =
    Ui.column
        [ Ui.width (Ui.px (7 * 40 + 6 * 4 + 2 * 16))
        , Ui.height Ui.fill
        , Ui.spacing 12
        ]
        [ viewPickerDayMenu internal
        , Ui.column
            [ Ui.paddingEach
                { top = 0
                , bottom = 8
                , left = 16
                , right = 16
                }
            , Ui.height (Ui.px (6 * 40 + 5 * 4 + 8))
            ]
            [ Ui.row
                [ Ui.spacing 4 ]
                [ viewWeekday Sun
                , viewWeekday Mon
                , viewWeekday Tue
                , viewWeekday Wed
                , viewWeekday Thu
                , viewWeekday Fri
                , viewWeekday Sat
                ]
            , viewCalendar external internal
            ]
        , if external.withTime then
            viewPickerTime external internal

          else
            Ui.none
        ]


viewWeekday : Weekday -> Element Msg
viewWeekday weekday =
    Ui.el
        [ Ui.width (Ui.px 40)
        , Ui.height (Ui.px 40)
        , Font.size 14
        , Font.color Color.onSurface
        ]
        (Ui.el
            [ Ui.centerX
            , Ui.centerY
            ]
            (Ui.text
                (case weekday of
                    Sun ->
                        "S"

                    Mon ->
                        "M"

                    Tue ->
                        "T"

                    Wed ->
                        "W"

                    Thu ->
                        "T"

                    Fri ->
                        "F"

                    Sat ->
                        "S"
                )
            )
        )


viewPickerDayMenu : Internal -> Element Msg
viewPickerDayMenu internal =
    Ui.row
        [ Ui.width Ui.fill
        , Ui.paddingEach
            { top = 16
            , bottom = 0
            , left = 16
            , right = 16
            }
        ]
        [ viewButtonMonthCurrent internal
        , Ui.el [ Ui.alignRight ]
            (viewButtonYearCurrent internal)
        ]


viewButtonMonthCurrent : Internal -> Element Msg
viewButtonMonthCurrent internal =
    Ui.row []
        [ viewSelectPrevious False UserPressedMonthPrevious
        , Time.toMonth internal.zone internal.focusedDate
            |> monthShort
            |> Fluent.verbatim
            |> button UserPressedMonthCurrent
            |> Button.withFollowingIcon Icon.ArrowDropDown
            |> Button.withFormat Button.Text
            |> Button.narrow
            |> Button.withWidth (Ui.minimum 80 Ui.shrink)
            |> Button.withAttributes
                [ Ui.onBlur UserUnfocusedNode
                , Ui.preventDefaultOnKeyDown
                    [ Accessibility.Key.tab ( UserTabbed, False )
                    , Accessibility.Key.tabBack ( UserTabbed, False )
                    , Accessibility.Key.enter ( UserPressedMonthCurrent, True )
                    , Accessibility.Key.space ( UserPressedMonthCurrent, True )
                    ]
                ]
            |> Button.view
        , viewSelectNext UserPressedMonthNext
        ]


viewButtonYearCurrent : Internal -> Element Msg
viewButtonYearCurrent internal =
    Ui.row []
        [ viewSelectPrevious True UserPressedYearPrevious
        , Time.toYear internal.zone internal.focusedDate
            |> String.fromInt
            |> Fluent.verbatim
            |> button UserPressedYearCurrent
            |> Button.withFollowingIcon Icon.ArrowDropDown
            |> Button.withFormat Button.Text
            |> Button.narrow
            |> Button.withWidth (Ui.minimum 80 Ui.shrink)
            |> Button.withAttributes
                [ Ui.onBlur UserUnfocusedNode
                , Ui.preventDefaultOnKeyDown
                    [ Accessibility.Key.tab ( UserTabbed, False )
                    , Accessibility.Key.tabBack ( UserTabbed, False )
                    , Accessibility.Key.enter ( UserPressedYearCurrent, True )
                    , Accessibility.Key.space ( UserPressedYearCurrent, True )
                    ]
                ]
            |> Button.view
        , viewSelectNext UserPressedYearNext
        ]


viewSelectPrevious : Bool -> Msg -> Element Msg
viewSelectPrevious handleTabBack msg =
    buttonIcon msg Icon.ChevronLeft
        |> ButtonIcon.small
        |> ButtonIcon.withId idPreviousMonth
        |> ButtonIcon.withAttributes
            [ Ui.onBlur UserUnfocusedNode
            , Ui.preventDefaultOnKeyDown
                (if handleTabBack then
                    [ Accessibility.Key.tab ( UserTabbed, False )
                    , Accessibility.Key.tabBack ( UserTabbed, False )
                    , Accessibility.Key.enter ( msg, True )
                    , Accessibility.Key.space ( msg, True )
                    ]

                 else
                    [ Accessibility.Key.tab ( UserTabbed, False )
                    , Accessibility.Key.enter ( msg, True )
                    , Accessibility.Key.space ( msg, True )
                    ]
                )
            ]
        |> ButtonIcon.toElement


viewSelectNext : Msg -> Element Msg
viewSelectNext msg =
    buttonIcon msg Icon.ChevronRight
        |> ButtonIcon.small
        |> ButtonIcon.withId idNextMonth
        |> ButtonIcon.withAttributes
            [ Ui.onBlur UserUnfocusedNode
            , Ui.preventDefaultOnKeyDown
                [ Accessibility.Key.tab ( UserTabbed, False )
                , Accessibility.Key.tabBack ( UserTabbed, False )
                , Accessibility.Key.enter ( msg, True )
                , Accessibility.Key.space ( msg, True )
                ]
            ]
        |> ButtonIcon.toElement


monthLong : Month -> String
monthLong month =
    case month of
        Jan ->
            "January"

        Feb ->
            "Februrary"

        Mar ->
            "March"

        Apr ->
            "April"

        May ->
            "May"

        Jun ->
            "June"

        Jul ->
            "July"

        Aug ->
            "August"

        Sep ->
            "September"

        Oct ->
            "October"

        Nov ->
            "November"

        Dec ->
            "December"


monthShort : Month -> String
monthShort month =
    case month of
        Jan ->
            "Jan"

        Feb ->
            "Feb"

        Mar ->
            "Mar"

        Apr ->
            "Apr"

        May ->
            "May"

        Jun ->
            "Jun"

        Jul ->
            "Jul"

        Aug ->
            "Aug"

        Sep ->
            "Sep"

        Oct ->
            "Oct"

        Nov ->
            "Nov"

        Dec ->
            "Dec"


monthToInt : Month -> Int
monthToInt month =
    case month of
        Jan ->
            1

        Feb ->
            2

        Mar ->
            3

        Apr ->
            4

        May ->
            5

        Jun ->
            6

        Jul ->
            7

        Aug ->
            8

        Sep ->
            9

        Oct ->
            10

        Nov ->
            11

        Dec ->
            12


monthsAll : List Month
monthsAll =
    [ Jan
    , Feb
    , Mar
    , Apr
    , May
    , Jun
    , Jul
    , Aug
    , Sep
    , Oct
    , Nov
    , Dec
    ]



--  CALENDAR


viewCalendar : External -> Internal -> Element Msg
viewCalendar external internal =
    let
        viewDays days =
            Ui.row
                [ Ui.spacing 4 ]
                (List.map (viewDay external internal) days)
    in
    Ui.column
        [ Ui.spacing 4 ]
        (paddedDays internal.zone internal.focusedDate
            |> List.Extra.groupsOf 7
            |> List.map viewDays
        )



--  DAY


viewDay : External -> Internal -> Day -> Element Msg
viewDay external internal day =
    case day of
        DayBeforeMonth date ->
            viewDayBeforeMonth internal.zone date

        DayInMonth date ->
            viewDayInMonth external internal date

        DayAfterMonth date ->
            viewDayAfterMonth internal.zone date


viewDayBeforeMonth : Zone -> Posix -> Element Msg
viewDayBeforeMonth zone day =
    Ui.el
        [ Ui.width (Ui.px 40)
        , Ui.height (Ui.px 40)
        , Font.size 14
        , Font.color Color.outlineVariant
        ]
        (Ui.el
            [ Ui.centerX
            , Ui.centerY
            ]
            (Ui.text (String.fromInt (Time.toDay zone day)))
        )


viewDayInMonth : External -> Internal -> Posix -> Element Msg
viewDayInMonth external internal day =
    let
        selected =
            case internal.selectedDate of
                Just date ->
                    sameDate internal.zone day date

                _ ->
                    False

        isToday =
            sameDate internal.zone day internal.now

        isFocused =
            sameDate internal.zone day internal.focusedDate
    in
    Ui.el
        [ Ui.centerX
        , Ui.centerY
        , Ui.width (Ui.px 40)
        , Ui.height (Ui.px 40)
        , Border.rounded 20
        , Border.width 1
        , if isToday then
            Border.color Color.primary

          else
            Border.color Color.transparent
        , Ui.preventDefaultOnKeyDown
            [ Accessibility.Key.enter ( UserPressedEnter external.id, True )
            , Accessibility.Key.space ( UserPressedSpace external.id, True )
            , Accessibility.Key.up ( UserPressedArrowUp, True )
            , Accessibility.Key.down ( UserPressedArrowDown, True )
            , Accessibility.Key.left ( UserPressedArrowLeft, True )
            , Accessibility.Key.right ( UserPressedArrowRight, True )
            ]
        ]
        (Ui.el
            ([ Ui.centerX
             , Ui.centerY
             , Ui.width (Ui.px 36)
             , Ui.height (Ui.px 36)
             , Ui.id (idDay internal day)
             , Events.onClick (UserSelectedDate external.id day)
             , Ui.onBlur UserUnfocusedNode
             , Ui.onKeyDown
                (if external.withTime then
                    [ Accessibility.Key.tab UserTabbed
                    , Accessibility.Key.tabBack UserTabbed
                    ]

                 else
                    [ Accessibility.Key.tabBack UserTabbed ]
                )
             , Ui.pointer
             , Border.rounded 18
             , Font.size 14
             , Ui.tabindex
                (if isFocused then
                    0

                 else
                    -1
                )
             ]
                ++ (if selected then
                        [ Background.color Color.primary
                        , Font.color Color.onPrimary
                        , Ui.mouseOver
                            [ Background.color
                                (Color.hoveredWith Color.onSurfaceVariant Color.primary)
                            ]
                        , Ui.focused
                            [ Background.color
                                (Color.focusedWith Color.onSurfaceVariant Color.primary)
                            ]
                        ]

                    else
                        [ Font.color
                            (if isToday then
                                Color.primary

                             else
                                Color.onSurface
                            )
                        , Ui.mouseOver
                            [ Background.color
                                (Color.hoveredWith Color.onSurfaceVariant Color.transparent)
                            ]
                        , Ui.focused
                            [ Background.color
                                (Color.focusedWith Color.onSurfaceVariant Color.transparent)
                            ]
                        ]
                   )
            )
            (Ui.el
                [ Ui.centerX
                , Ui.centerY
                ]
                (Ui.text (String.fromInt (Time.toDay internal.zone day)))
            )
        )


viewDayAfterMonth : Zone -> Posix -> Element Msg
viewDayAfterMonth zone day =
    Ui.el
        [ Ui.width (Ui.px 40)
        , Ui.height (Ui.px 40)
        , Font.size 14
        , Font.color Color.outlineVariant
        ]
        (Ui.el
            [ Ui.centerX
            , Ui.centerY
            ]
            (Ui.text (String.fromInt (Time.toDay zone day)))
        )



--  PICKER TIME


viewPickerTime : External -> Internal -> Element Msg
viewPickerTime external internal =
    Ui.column
        [ Ui.paddingEach
            { top = 20
            , bottom = 0
            , left = 0
            , right = 0
            }
        , Ui.width Ui.fill
        ]
        [ Ui.column
            [ Ui.padding 24
            , Ui.spacing 24
            , Ui.width Ui.fill
            ]
            [ Ui.row
                [ Ui.spacing 12
                ]
                [ Ui.row [ Ui.spacing 4 ]
                    [ Ui.column
                        [ Ui.spacing 8 ]
                        [ Input.text
                            (Input.attributesText
                                ++ [ Font.size 48
                                   , Ui.centerX
                                   , Ui.ariaLabelledby "hour-label"
                                   , Ui.width (Ui.px 82)
                                   , Ui.height (Ui.px 70)
                                   , Ui.maxlength 2
                                   , Ui.onBlur UserUnfocusedNode
                                   , Ui.onKeyDown
                                        [ Accessibility.Key.tab UserTabbed
                                        , Accessibility.Key.tabBack UserTabbed
                                        ]
                                   ]
                                |> Input.withFocusBorder False
                            )
                            { onChange = UserChangedHour
                            , text = internal.hour
                            , placeholder = Nothing
                            , label = Input.labelHidden ""
                            }
                        , Ui.el
                            [ Font.color Color.onSurface
                            , Ui.id "hour-label"
                            ]
                            (Typography.caption (t "hour--label"))
                        ]
                    , Ui.el
                        [ Font.size 48
                        , Ui.width (Ui.px 24)
                        , Ui.alignTop
                        ]
                        (Ui.el
                            [ Ui.centerX
                            , Ui.paddingXY 0 8
                            ]
                            (Fluent.toElement (Fluent.verbatim ":"))
                        )
                    , Ui.column
                        [ Ui.spacing 8 ]
                        [ Input.text
                            (Input.attributesText
                                ++ [ Font.size 48
                                   , Ui.centerX
                                   , Ui.ariaLabelledby "minute-label"
                                   , Ui.width (Ui.px 82)
                                   , Ui.height (Ui.px 70)
                                   , Ui.maxlength 2
                                   , Ui.onBlur UserUnfocusedNode
                                   , Ui.onKeyDown
                                        [ Accessibility.Key.tab UserTabbed
                                        , Accessibility.Key.tabBack UserTabbed
                                        ]
                                   ]
                                |> Input.withFocusBorder False
                            )
                            { onChange = UserChangedMinute
                            , text = internal.minute
                            , placeholder = Nothing
                            , label = Input.labelHidden ""
                            }
                        , Ui.el
                            [ Font.color Color.onSurface
                            , Ui.id "minute-label"
                            ]
                            (Typography.caption (t "minute--label"))
                        ]
                    ]
                , case Time.Format.fromLocale external.locale of
                    Time.Format.Iso8601 ->
                        Ui.none

                    Time.Format.EnUs ->
                        Ui.el
                            [ Ui.alignTop ]
                            (ButtonToggle.view
                                { onChange = UserChangedPeriod
                                , selected = internal.period
                                , groups =
                                    [ ButtonToggle.group
                                        [ ButtonToggle.optionFluent Am (t "period--am")
                                        , ButtonToggle.optionFluent Pm (t "period--pm")
                                        ]
                                        |> ButtonToggle.withAttributes
                                            [ Ui.onBlur UserUnfocusedNode
                                            , Ui.onKeyDown
                                                [ Accessibility.Key.tab UserTabbed
                                                , Accessibility.Key.tabBack UserTabbed
                                                ]
                                            ]
                                    ]
                                , label = ""
                                , orientation = ButtonToggle.Vertical
                                }
                            )

                    Time.Format.De ->
                        Ui.none
                ]
            ]
        , Ui.row
            [ Ui.paddingEach
                { top = 8
                , bottom = 12
                , left = 0
                , right = 10
                }
            , Ui.spacing 8
            , Ui.alignRight
            ]
            [ button UserPressedCancelOnTimePicker (t "cancel")
                |> Button.withFormat Button.Text
                |> Button.withAttributes
                    [ Ui.onBlur UserUnfocusedNode
                    , Ui.onKeyDown
                        [ Accessibility.Key.tab UserTabbed
                        , Accessibility.Key.tabBack UserTabbed
                        ]
                    ]
                |> Button.view
            , button UserPressedOkOnTimePicker (t "ok")
                |> Button.withFormat Button.Text
                |> Button.withAttributes
                    [ Ui.onBlur UserUnfocusedNode
                    , Ui.onKeyDown [ Accessibility.Key.tabBack UserTabbed ]
                    ]
                |> Button.view
            ]
        ]



--  IDS


idLabel : String -> String
idLabel id =
    id ++ "-label"


idListboxMonth : String -> String
idListboxMonth id =
    id ++ "-listbox-month"


idListboxYear : String -> String
idListboxYear id =
    id ++ "-listbox-year"


idPreviousMonth : String
idPreviousMonth =
    "previous-month"


idNextMonth : String
idNextMonth =
    "next-month"


idFocusedDay : Internal -> String
idFocusedDay internal =
    String.join "-"
        [ "day"
        , String.fromInt (Time.toYear Time.utc internal.focusedDate)
        , String.fromInt
            (Derberos.Date.Utils.monthToNumber1 (Time.toMonth Time.utc internal.focusedDate))
        , String.fromInt (Time.toDay Time.utc internal.focusedDate)
        ]


idDay : Internal -> Posix -> String
idDay internal day =
    String.join "-"
        [ "day"
        , String.fromInt (Time.toYear Time.utc internal.focusedDate)
        , String.fromInt
            (Derberos.Date.Utils.monthToNumber1 (Time.toMonth Time.utc internal.focusedDate))
        , String.fromInt (Time.toDay Time.utc day)
        ]


idFocusedYear : Internal -> String
idFocusedYear internal =
    String.join "-"
        [ "year"
        , String.fromInt (Time.toYear Time.utc internal.focusedDate)
        ]



--  COMPUTATIONS


type Day
    = DayBeforeMonth Posix
    | DayInMonth Posix
    | DayAfterMonth Posix


paddedDays : Zone -> Posix -> List Day
paddedDays zone month =
    let
        daysBeforeMonth =
            List.range 1 daysBeforeMonthCount
                |> List.map
                    (\offset ->
                        Derberos.Date.Delta.addDays (-1 * offset) firstDayOfMonth
                    )
                |> List.map
                    (Time.posixToMillis
                        >> (+) (hour * 1000 * 60 * 60)
                        >> (+) (minute * 1000 * 60)
                        >> Time.millisToPosix
                        >> DayBeforeMonth
                    )
                |> List.reverse

        firstDayOfMonth =
            Derberos.Date.Calendar.getFirstDayOfMonth zone month

        daysBeforeMonthCount =
            firstDayOfMonth
                |> Time.toWeekday zone
                |> Derberos.Date.Utils.weekdayToNumber
                |> adjustWeekdayOffset

        daysInMonth =
            List.map DayInMonth
                (Derberos.Date.Calendar.getCurrentMonthDates zone month
                    |> List.map
                        (Time.posixToMillis
                            >> (+) (hour * 1000 * 60 * 60)
                            >> (+) (minute * 1000 * 60)
                            >> Time.millisToPosix
                        )
                )

        daysAfterMonth =
            List.range 1 daysAfterMonthCount
                |> List.map
                    (\offset ->
                        Derberos.Date.Delta.addDays offset lastDayOfMonth
                    )
                |> List.map
                    (Time.posixToMillis
                        >> (+) (hour * 1000 * 60 * 60)
                        >> (+) (minute * 1000 * 60)
                        >> Time.millisToPosix
                        >> DayAfterMonth
                    )

        daysAfterMonthCount =
            7
                - (lastDayOfMonth
                    |> Time.toWeekday zone
                    |> Derberos.Date.Utils.weekdayToNumber
                    |> adjustWeekdayOffset
                  )

        lastDayOfMonth =
            Derberos.Date.Calendar.getLastDayOfMonth zone month

        hour =
            Time.toHour zone month

        minute =
            Time.toMinute zone month
    in
    List.concat
        [ daysBeforeMonth
        , daysInMonth
        , daysAfterMonth
        ]


adjustWeekdayOffset : Int -> Int
adjustWeekdayOffset offsetWithMondayFirst =
    if offsetWithMondayFirst == 6 then
        0

    else
        offsetWithMondayFirst + 1


sameDate : Zone -> Posix -> Posix -> Bool
sameDate zone dateA dateB =
    (Time.toYear zone dateA == Time.toYear zone dateB)
        && (Time.toMonth zone dateA == Time.toMonth zone dateB)
        && (Time.toDay zone dateA == Time.toDay zone dateB)


t : String -> Fluent
t key =
    Fluent.text ("ui-molecule-date-picker--" ++ key)
