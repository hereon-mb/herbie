// Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
// a client-server based web application for research documentation
//
// Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
//
// Authors: Fabian Kirchner, Catriona Eschke
//
// This program is subject to the terms and conditions for non-commercial use of
// ELN. You can find the license text in the file LICENSE.en and under
// http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
// you have any questions or comments, you can contact us at hereon at
// herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
// Max-Planck-Straße 1, 21502 Geesthacht, Germany.

import { basicSetup } from "codemirror"
import { Annotation, EditorState } from "@codemirror/state"
import { EditorView } from "@codemirror/view"
import { turtle } from "codemirror-lang-turtle"

const lang = function(format) {
  if (format == "text/turtle" || format == "application/n-triples") {
    return turtle().extension;
  }
}

const changeAnnotation = Annotation.define()

export function registerCustomElementCodeMirror() {
  customElements.define("code-mirror", class extends HTMLElement {
    constructor() {
      super();
      this._elmData = null;
      this._state = null;
      this._view = null;
    }

    adoptedCallback() {
    }

    connectedCallback() {
      const that = this;
      const elmData = this.elmData || this._elmData
      this._state = EditorState.create({
        doc: elmData.content,
        extensions: [
          basicSetup,
          lang(elmData.format),
          EditorView.updateListener.of((v) => {
            if (v.docChanged) {
              for (const transaction of v.transactions) {
                for (const annotation of transaction.annotations) {
                  if (annotation.value == "from-app") {
                    return
                  }
                }
              }

              const content = v.state.doc.toString();
              this._elmData.content = content;
              const event = new CustomEvent("text-changed", { detail: content });
              that.dispatchEvent(event);
            }
          }),
          EditorView.theme({
            "&": {
              "width": "100%",
              "height": "100%",
            },
            ".cm-scroller": {overflow: "auto"},
            ".cm-content": {"font-size": "11pt"},
            ".cm-lineNumbers": {"font-size": "11pt"},
          }),
        ],
      });
      this._view = new EditorView({
        state: this._state,
        parent: this,
      })
    }

    disconnectedCallback() {
    }

    set elmData(value) {
      if (this._elmData && this._elmData.content == value.content) {
        return
      }

      this._elmData = value;
      if (this._view) {
        this._view.dispatch({
          changes: {
            from: 0,
            to: this._view.state.doc.length,
            insert: value.content,
          },
          annotations: changeAnnotation.of("from-app"),
        });
      }
    }

    get elmData() {
      return this._elmData
    }
  });
}
