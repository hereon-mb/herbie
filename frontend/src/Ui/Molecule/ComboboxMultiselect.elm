{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Molecule.ComboboxMultiselect exposing
    ( comboboxMultiselect
    , Label, labelAbove
    , main, Model, Msg
    )

{-|

@docs comboboxMultiselect
@docs Label, labelAbove, labelledBy

@docs main, Model, Msg

-}

import Accessibility.Clipboard as Clipboard
import Bool.Apply as Bool
import Browser
import Browser.Dom
import Element as Ui
    exposing
        ( Element
        , alignBottom
        , column
        , el
        , fill
        , focusStyle
        , height
        , html
        , inFront
        , noStaticStyleSheet
        , none
        , padding
        , paddingEach
        , paddingXY
        , row
        , spacing
        , transparent
        , width
        , wrappedRow
        )
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Extra
    exposing
        ( id
        , onKeyDown
        , pointerEvents
        , preventDefaultOnKeyDown
        , style
        )
import Element.Font as Font
import Element.Input as Input
import Fluent exposing (Fluent)
import Html exposing (Html)
import Html.Attributes
import Html.Events
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Decode
import Json.Encode as Encode exposing (Value)
import Maybe.Extra as Maybe
import Pointer
import Ports.CustomElement as Ports
import Task
import Ui.Atom.ChipInput as ChipInput exposing (chipInput)
import Ui.Atom.FocusBorder as FocusBorder exposing (focusBorder)
import Ui.Atom.Icon as Icon
import Ui.Atom.Listbox as Listbox exposing (Listbox)
import Ui.Atom.OptionCombobox as OptionCombobox exposing (OptionCombobox)
import Ui.Atom.Portal as Portal
import Ui.Theme.Color as Color
import Ui.Theme.Elevation as Elevation
import Ui.Theme.Input


nameNode : String
nameNode =
    "ui-molecule-combobox-multiselect"


comboboxMultiselect :
    { id : Pointer.Pointer
    , label : Label
    , initialQuery : String
    , supportingText : Maybe Fluent
    , help : List Fluent
    , options : List option
    , selected : List option
    , toOption : option -> OptionCombobox
    , fromOption : OptionCombobox -> Maybe option
    , onChange : String -> msg
    , onSelect : option -> msg
    , onUnselect : option -> msg
    , onCopy : Maybe msg
    , onPaste : Maybe ( String, msg, String -> Maybe msg )
    , noOp : msg
    , renderSubLabels : Bool
    }
    -> Element msg
comboboxMultiselect config =
    el
        [ width fill ]
        (html (comboboxMultiselectHelp config))


comboboxMultiselectHelp :
    { id : Pointer.Pointer
    , label : Label
    , initialQuery : String
    , supportingText : Maybe Fluent
    , help : List Fluent
    , options : List option
    , selected : List option
    , toOption : option -> OptionCombobox
    , fromOption : OptionCombobox -> Maybe option
    , onChange : String -> msg
    , onSelect : option -> msg
    , onUnselect : option -> msg
    , onCopy : Maybe msg
    , onPaste : Maybe ( String, msg, String -> Maybe msg )
    , noOp : msg
    , renderSubLabels : Bool
    }
    -> Html msg
comboboxMultiselectHelp config =
    let
        addOnCopy attrs =
            case config.onCopy of
                Nothing ->
                    attrs

                Just msg ->
                    Clipboard.onCopyHtml msg :: attrs

        addOnPaste attrs =
            case config.onPaste of
                Nothing ->
                    attrs

                Just ( format, msgPrevented, msg ) ->
                    Clipboard.onPasteHtml format msgPrevented msg :: attrs
    in
    Html.node nameNode
        ([ Html.Attributes.property "elmData"
            (encode
                { id = Pointer.toString config.id
                , label = config.label
                , initialQuery = config.initialQuery
                , supportingText = config.supportingText
                , help = config.help
                , options = List.map config.toOption config.options
                , selected = List.map config.toOption config.selected
                , renderSubLabels = config.renderSubLabels
                , onCopy = Maybe.isJust config.onCopy
                }
            )
         , Html.Events.stopPropagationOn "elm-data-changed"
            (changedDecoder
                { onChange = config.onChange
                , onSelect =
                    \optionSelected ->
                        case config.fromOption optionSelected of
                            Nothing ->
                                config.noOp

                            Just option ->
                                config.onSelect option
                , onUnselect =
                    \optionUnselected ->
                        case config.fromOption optionUnselected of
                            Nothing ->
                                config.noOp

                            Just option ->
                                config.onUnselect option
                }
                |> Decode.field "detail"
                |> Decode.map (\msg -> ( msg, True ))
            )
         ]
            |> addOnCopy
            |> addOnPaste
        )
        []


changedDecoder :
    { onChange : String -> msg
    , onSelect : OptionCombobox -> msg
    , onUnselect : OptionCombobox -> msg
    }
    -> Decoder msg
changedDecoder config =
    let
        help kind =
            case kind of
                "on-change" ->
                    Decode.field "value" Decode.string
                        |> Decode.map config.onChange

                "on-select" ->
                    Decode.field "option" OptionCombobox.decoder
                        |> Decode.map config.onSelect

                "on-unselect" ->
                    Decode.field "option" OptionCombobox.decoder
                        |> Decode.map config.onUnselect

                _ ->
                    Decode.fail ("unknown kind '" ++ kind ++ "'")
    in
    Decode.andThen help (Decode.field "kind" Decode.string)


encodeOnChange : String -> Value
encodeOnChange value =
    Encode.object
        [ ( "kind", Encode.string "on-change" )
        , ( "value", Encode.string value )
        ]


encodeOnSelect : OptionCombobox -> Value
encodeOnSelect option =
    Encode.object
        [ ( "kind", Encode.string "on-select" )
        , ( "option", OptionCombobox.encode option )
        ]


encodeOnUnselect : OptionCombobox -> Value
encodeOnUnselect option =
    Encode.object
        [ ( "kind", Encode.string "on-unselect" )
        , ( "option", OptionCombobox.encode option )
        ]


type alias External =
    { id : String
    , label : Label
    , initialQuery : String
    , supportingText : Maybe Fluent
    , help : List Fluent
    , options : List OptionCombobox
    , selected : List OptionCombobox
    , renderSubLabels : Bool
    , onCopy : Bool
    }


type Label
    = LabelAbove Fluent
    | LabelledBy String


labelAbove : Fluent -> Label
labelAbove =
    LabelAbove


encode : External -> Value
encode external =
    Encode.object
        [ ( "id", Encode.string external.id )
        , ( "label", encodeLabel external.label )
        , ( "initialQuery", Encode.string external.initialQuery )
        , ( "supportingText", Maybe.unwrap Encode.null Fluent.encode external.supportingText )
        , ( "help", Encode.list Fluent.encode external.help )
        , ( "options", Encode.list OptionCombobox.encode external.options )
        , ( "selected", Encode.list OptionCombobox.encode external.selected )
        , ( "renderSubLabels", Encode.bool external.renderSubLabels )
        , ( "onCopy", Encode.bool external.onCopy )
        ]


encodeLabel : Label -> Value
encodeLabel label =
    case label of
        LabelAbove fluent ->
            Encode.object
                [ ( "kind", Encode.string "label-above" )
                , ( "fluent", Fluent.encode fluent )
                ]

        LabelledBy id ->
            Encode.object
                [ ( "kind", Encode.string "labelled-by" )
                , ( "id", Encode.string id )
                ]


decoder : Decoder External
decoder =
    Decode.succeed External
        |> Decode.required "id" Decode.string
        |> Decode.required "label" labelDecoder
        |> Decode.required "initialQuery" Decode.string
        |> Decode.required "supportingText" (Decode.maybe Fluent.decoder)
        |> Decode.required "help" (Decode.list Fluent.decoder)
        |> Decode.required "options" (Decode.list OptionCombobox.decoder)
        |> Decode.required "selected" (Decode.list OptionCombobox.decoder)
        |> Decode.required "renderSubLabels" Decode.bool
        |> Decode.required "onCopy" Decode.bool


labelDecoder : Decoder Label
labelDecoder =
    let
        help kind =
            case kind of
                "label-above" ->
                    Decode.succeed LabelAbove
                        |> Decode.required "fluent" Fluent.decoder

                "labelled-by" ->
                    Decode.succeed LabelledBy
                        |> Decode.required "id" Decode.string

                _ ->
                    Decode.fail ("unknown kind: '" ++ kind ++ "'")
    in
    Decode.andThen help (Decode.field "kind" Decode.string)


main : Program Value Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- MODEL


type Model
    = Running External Internal
    | Failed


type alias Internal =
    { query : String
    , hideListbox : Bool
    , viewport : Maybe Portal.Viewport
    , listbox : Listbox
    , preventBlur : Bool
    , oneRow : Bool
    , selectedOld : Maybe (List OptionCombobox)
    }


init : Value -> ( Model, Cmd Msg )
init flags =
    case Decode.decodeValue decoder flags of
        Err _ ->
            ( Failed
            , Cmd.none
            )

        Ok external ->
            ( Running external
                { query = external.initialQuery
                , hideListbox = True
                , viewport = Nothing
                , listbox = Listbox.init
                , preventBlur = False
                , oneRow = True
                , selectedOld = Just []
                }
            , getElementOptions external
            )



-- UPDATE


type Msg
    = NoOp
    | ElmDataChanged Value
    | UserCopied
      -- INPUT
    | UserClickedContainer
    | UserChangedQuery String
    | UserPressedEscape
    | UserPressedArrowDown
    | OnViewport Portal.Viewport
    | UserUnfocusedQuery
    | UserPressedArrowDropDown
    | UserPressedArrowDropUp
      -- LISTBOX
    | ListboxMsg String (Listbox.Msg OptionCombobox)
    | UserPressedMouseOnListbox
    | UserReleasedMouseOnListbox
      -- SELECTED
    | UserPressedRemoveFilter OptionCombobox
      -- DOM
    | BrowserReturnedElementOptions (Result Browser.Dom.Error { widthOptions : Float, widthContainer : Float })


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case model of
        Running external internal ->
            updateHelp msg external internal

        Failed ->
            ( model, Cmd.none )


updateHelp : Msg -> External -> Internal -> ( Model, Cmd Msg )
updateHelp msg external internal =
    case msg of
        NoOp ->
            ( Running external internal, Cmd.none )

        ElmDataChanged value ->
            case Decode.decodeValue decoder value of
                Err _ ->
                    ( Running external internal, Cmd.none )

                Ok newExternal ->
                    ( -- FIXME when and what to do here?
                      -- focusNextOrFirstEntry newExternal internal
                      Running newExternal
                        (if List.map .id external.selected == List.map .id newExternal.selected then
                            internal

                         else
                            { internal | selectedOld = Just external.selected }
                        )
                    , getElementOptions external
                    )

        UserCopied ->
            ( Running external internal, Ports.copy () )

        UserClickedContainer ->
            ( Running external internal
            , focusInput external.id
            )

        UserChangedQuery newQuery ->
            ( Running external
                { internal
                    | query = newQuery
                    , hideListbox = False
                }
            , Ports.changeElmData (encodeOnChange newQuery)
            )

        UserPressedEscape ->
            ( Running external { internal | hideListbox = True }
            , Cmd.none
            )

        UserPressedArrowDown ->
            ( Running external { internal | hideListbox = False }
            , Cmd.none
            )

        OnViewport viewport ->
            ( Running external { internal | viewport = Just viewport }
            , Cmd.none
            )

        UserUnfocusedQuery ->
            if internal.preventBlur then
                ( Running external internal, Cmd.none )

            else
                ( Running external { internal | hideListbox = True }
                , Cmd.none
                )

        UserPressedArrowDropDown ->
            ( Running external { internal | hideListbox = False }
            , focusInput external.id
            )

        UserPressedArrowDropUp ->
            ( Running external { internal | hideListbox = True }
            , focusInput external.id
            )

        ListboxMsg id listboxMsg ->
            let
                ( newListbox, listboxCmd, newSelection ) =
                    Listbox.update
                        { uniqueId = .id }
                        external.options
                        listboxMsg
                        internal.listbox
                        []
            in
            case List.head newSelection of
                Nothing ->
                    ( Running external { internal | listbox = newListbox }
                    , Cmd.map (ListboxMsg id) listboxCmd
                    )

                Just option ->
                    ( Running external
                        { internal
                            | listbox = newListbox
                            , query = ""
                            , hideListbox = True
                        }
                    , Cmd.batch
                        [ Cmd.map (ListboxMsg id) listboxCmd
                        , Ports.changeElmData (encodeOnSelect option)
                        , Ports.changeElmData (encodeOnChange "")
                        , focusInput id
                        ]
                    )

        UserPressedMouseOnListbox ->
            ( Running external { internal | preventBlur = True }
            , Cmd.none
            )

        UserReleasedMouseOnListbox ->
            ( Running external { internal | preventBlur = False }
            , Cmd.none
            )

        UserPressedRemoveFilter option ->
            ( Running external internal
            , [ option
                    |> encodeOnUnselect
                    |> Ports.changeElmData
              , focusInput external.id
              ]
                |> Cmd.batch
            )

        BrowserReturnedElementOptions (Err _) ->
            ( Running external internal
            , Cmd.none
            )

        BrowserReturnedElementOptions (Ok data) ->
            ( Running external
                { internal
                    | oneRow = data.widthOptions + 90 < data.widthContainer
                    , selectedOld = Nothing
                }
            , Cmd.none
            )


focusInput : String -> Cmd Msg
focusInput id =
    Browser.Dom.focus (idInput id)
        |> Task.attempt (\_ -> NoOp)



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        Running external internal ->
            Sub.batch
                [ Ports.elmDataChanged ElmDataChanged
                , Sub.map (ListboxMsg (instance external).id) (Listbox.subscriptions internal.listbox)
                ]

        Failed ->
            Sub.none



-- VIEW


view : Model -> Html Msg
view model =
    case model of
        Running external internal ->
            Html.node nameNode
                [ Html.Attributes.style "display" "contents" ]
                [ Ui.layoutWith
                    { options =
                        [ noStaticStyleSheet
                        , focusStyle
                            { borderColor = Nothing
                            , backgroundColor = Nothing
                            , shadow = Nothing
                            }
                        ]
                    }
                    [ width fill
                    , height fill
                    , Font.family
                        [ Font.typeface "Work Sans"
                        , Font.sansSerif
                        ]
                    ]
                    (viewHelp external internal)
                ]

        Failed ->
            Html.node nameNode
                [ Html.Attributes.style "display" "contents" ]
                [ Html.text "Could not initialize custom element" ]


viewHelp : External -> Internal -> Element Msg
viewHelp external internal =
    let
        withSupportingText =
            case external.supportingText of
                Nothing ->
                    identity

                Just supportingText ->
                    FocusBorder.withSupportingText supportingText

        withLabel =
            case external.label of
                LabelAbove labelText ->
                    FocusBorder.withLabel
                        { id = idLabel external.id
                        , text = labelText
                        }

                _ ->
                    identity

        withEmpty =
            if internal.query == "" && List.isEmpty external.selected then
                FocusBorder.empty

            else
                identity

        withActionIcon =
            if internal.hideListbox then
                FocusBorder.withActionIcon Icon.ArrowDropDown UserPressedArrowDropDown

            else
                FocusBorder.withActionIcon Icon.ArrowDropUp UserPressedArrowDropUp

        withMenu =
            FocusBorder.withMenu
                { onViewport = OnViewport
                , viewport = internal.viewport
                , menu =
                    if not internal.hideListbox then
                        viewMenu external internal

                    else
                        none
                }
    in
    column
        [ width fill
        , spacing 4
        , style "cursor" "text"
        , Events.onClick UserClickedContainer
        , onKeyDown
            [ Decode.field "key" Decode.string
                |> Decode.andThen
                    (\key ->
                        case key of
                            "ArrowDown" ->
                                Decode.succeed UserPressedArrowDown

                            _ ->
                                Decode.fail "not handling that key here"
                    )
            ]
        ]
        [ [ if internal.oneRow then
                el [] none

            else
                internal.selectedOld
                    |> Maybe.withDefault external.selected
                    |> List.map viewRemovableTag
                    |> wrappedRow
                        [ width fill
                        , spacing 8
                        , paddingEach
                            { top = 0
                            , bottom = 8
                            , left = 0
                            , right = 0
                            }
                        ]
          , [ if internal.oneRow && not (List.isEmpty external.selected) then
                internal.selectedOld
                    |> Maybe.withDefault external.selected
                    |> List.map viewRemovableTag
                    |> row [ spacing 8 ]

              else
                el [] none
            , viewInput external internal
            ]
                |> row [ width fill ]
          ]
            |> column
                [ width fill
                , paddingXY 8 12
                , inFront
                    (case internal.selectedOld of
                        Nothing ->
                            none

                        Just _ ->
                            el
                                [ width fill
                                , padding 8
                                , id (idContainer external.id)
                                , transparent True
                                , pointerEvents False
                                ]
                                (List.map viewRemovableTag external.selected
                                    |> row
                                        [ spacing 8
                                        , id (idOptions external.id)
                                        ]
                                )
                    )
                ]
            |> focusBorder
            |> FocusBorder.withHelp external.help
            |> withSupportingText
            |> withLabel
            |> withEmpty
            |> withActionIcon
            |> Bool.apply (FocusBorder.withHoverableActionIcon Icon.ContentCopy UserCopied) external.onCopy
            |> withMenu
            |> FocusBorder.toElement
            |> el
                [ width fill
                , Events.onMouseDown UserPressedMouseOnListbox
                , Events.onMouseUp UserReleasedMouseOnListbox
                ]
        ]


viewInput : External -> Internal -> Element Msg
viewInput external internal =
    Input.text
        ([ padding 8
         , alignBottom
         , width fill
         , Border.width 0
         , Background.color Color.transparent
         , id (idInput external.id)
         , preventDefaultOnKeyDown
            [ Listbox.preventDefaultOnKeyDown (instance external)
                (Decode.field "key" Decode.string
                    |> Decode.andThen
                        (\key ->
                            case key of
                                "Escape" ->
                                    Decode.succeed ( UserPressedEscape, True )

                                _ ->
                                    Decode.fail "not handling that key here"
                        )
                )
            ]
         , Events.onLoseFocus UserUnfocusedQuery
         ]
            ++ Ui.Theme.Input.attributesText
        )
        { onChange = UserChangedQuery
        , text = internal.query
        , placeholder = Nothing
        , label = Input.labelHidden "Query"
        }


instance : External -> Listbox.Instance OptionCombobox Msg
instance external =
    { id = external.id ++ "-listbox"
    , label =
        case external.label of
            LabelAbove _ ->
                Listbox.labelledBy (idLabel external.id)

            LabelledBy id ->
                Listbox.labelledBy id
    , lift = ListboxMsg external.id
    }


viewRemovableTag : OptionCombobox -> Element Msg
viewRemovableTag option =
    option.label
        |> chipInput
        |> ChipInput.withOnPressRemove (UserPressedRemoveFilter option)
        |> ChipInput.toElement


getElementOptions : External -> Cmd Msg
getElementOptions external =
    Task.map2
        (\elementOptions elementContainer ->
            { widthOptions = elementOptions.element.width
            , widthContainer = elementContainer.element.width
            }
        )
        (Browser.Dom.getElement (idOptions external.id))
        (Browser.Dom.getElement (idContainer external.id))
        |> Task.attempt BrowserReturnedElementOptions


viewMenu : External -> Internal -> Element Msg
viewMenu external internal =
    el
        [ width fill
        , Events.onMouseDown UserPressedMouseOnListbox
        , Events.onMouseUp UserReleasedMouseOnListbox
        ]
        (Listbox.view
            { uniqueId = .id
            , focusable = False
            , viewOption = \_ -> OptionCombobox.view external.renderSubLabels
            , heightMaximum = 160
            , attrsListbox =
                [ Border.rounded 4
                , Background.color Color.surface
                , Ui.clipX
                , Elevation.z8
                ]
            }
            (instance external)
            external.options
            internal.listbox
            []
        )


idOptions : String -> String
idOptions id =
    id ++ "-options"


idContainer : String -> String
idContainer id =
    id ++ "-container"


idInput : String -> String
idInput id =
    id


idLabel : String -> String
idLabel id =
    id ++ "-label"
