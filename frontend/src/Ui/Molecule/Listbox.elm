{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Molecule.Listbox exposing
    ( Item
    , item, itemLink
    , withActionIconAfterText, withIconAfterText, withProgressAfterText
    , withSpaceBeforeText, withActionIconBeforeText, withIconBeforeText
    , withSecondaryLine, withSecondaryParagraph, withSecondaryParagraphElements
    , withSubItems
    , Group
    , group
    , Model
    , fromItems, fromGroups
    , withPaddingY
    , withBackgroundColor
    , toElement
    , active
    )

{-|

@docs Item
@docs item, itemLink
@docs withActionIconAfterText, withIconAfterText, withProgressAfterText
@docs withSpaceBeforeText, withActionIconBeforeText, withIconBeforeText
@docs withSecondaryLine, withSecondaryParagraph, withSecondaryParagraphElements
@docs withSubItems

@docs Group
@docs group

@docs Model
@docs fromItems, fromGroups
@docs withPaddingY
@docs withBackgroundColor
@docs toElement

-}

import Accessibility.Key
import Element
    exposing
        ( Attribute
        , Color
        , Element
        , alignLeft
        , alignRight
        , centerX
        , centerY
        , column
        , el
        , fill
        , focused
        , height
        , html
        , mouseDown
        , mouseOver
        , none
        , padding
        , paddingEach
        , paddingXY
        , paragraph
        , px
        , row
        , spacing
        , toRgb
        , width
        , wrappedRow
        )
import Element.Background as Background
import Element.Border as Border
import Element.Extra
    exposing
        ( disabled
        , preventDefaultOnClick
        , preventDefaultOnKeyDown
        , stopPropagationOnClick
        )
import Element.Font as Font
import Element.Input as Input
import Fluent
    exposing
        ( Fluent
        , int
        )
import Progress.Ring
import Ui.Atom.Divider
import Ui.Atom.Icon as Icon exposing (Icon)
import Ui.Theme.Color
    exposing
        ( error
        , focusedWith
        , hoveredWith
        , onSurface
        , onSurfaceVariant
        , pressedWith
        , primary
        , secondaryContainer
        , surface
        , transparent
        )
import Ui.Theme.Typography
    exposing
        ( body1
        , body2
        )


type Item msg
    = Item (ItemData msg)


type alias ItemData msg =
    { onPress : OnPress msg
    , title : Fluent
    , secondaryLine : Maybe (List (Element msg))
    , secondaryParagraphs : List (List (Element msg))
    , before : Maybe (Before msg)
    , after : Maybe (After msg)
    , errorCount : Maybe Int
    , active : Bool
    , items : List (Item msg)
    }


type OnPress msg
    = Msg msg
    | Url String


type Before msg
    = BeforeSpace
    | BeforeIcon Icon
    | BeforeActionIcon Icon msg


type After msg
    = AfterIcon Icon
    | ActionIcon Icon msg
    | Progress Float


item : msg -> Fluent -> Item msg
item onPress title =
    Item
        { onPress = Msg onPress
        , title = title
        , secondaryLine = Nothing
        , secondaryParagraphs = []
        , before = Nothing
        , after = Nothing
        , errorCount = Nothing
        , active = False
        , items = []
        }


itemLink : String -> Fluent -> Item msg
itemLink url title =
    Item
        { onPress = Url url
        , title = title
        , secondaryLine = Nothing
        , secondaryParagraphs = []
        , before = Nothing
        , after = Nothing
        , errorCount = Nothing
        , active = False
        , items = []
        }


withSecondaryLine : List Fluent -> Item msg -> Item msg
withSecondaryLine secondaryLine theItem =
    case theItem of
        Item data ->
            Item { data | secondaryLine = Just (List.map body2 secondaryLine) }


withSecondaryParagraph : Fluent -> Item msg -> Item msg
withSecondaryParagraph secondaryParagraph theItem =
    case theItem of
        Item data ->
            Item { data | secondaryParagraphs = [ body2 secondaryParagraph ] :: data.secondaryParagraphs }


withSecondaryParagraphElements : List (Element msg) -> Item msg -> Item msg
withSecondaryParagraphElements elements theItem =
    case theItem of
        Item data ->
            Item { data | secondaryParagraphs = elements :: data.secondaryParagraphs }


withSpaceBeforeText : Item msg -> Item msg
withSpaceBeforeText theItem =
    case theItem of
        Item data ->
            Item { data | before = Just BeforeSpace }


withIconBeforeText : Icon -> Item msg -> Item msg
withIconBeforeText icon theItem =
    case theItem of
        Item data ->
            Item { data | before = Just (BeforeIcon icon) }


withActionIconBeforeText : Icon -> msg -> Item msg -> Item msg
withActionIconBeforeText icon onPress theItem =
    case theItem of
        Item data ->
            Item { data | before = Just (BeforeActionIcon icon onPress) }


withIconAfterText : Icon -> Item msg -> Item msg
withIconAfterText icon theItem =
    case theItem of
        Item data ->
            Item { data | after = Just (AfterIcon icon) }


withActionIconAfterText : Icon -> msg -> Item msg -> Item msg
withActionIconAfterText icon onPress theItem =
    case theItem of
        Item data ->
            Item { data | after = Just (ActionIcon icon onPress) }


withProgressAfterText : Float -> Item msg -> Item msg
withProgressAfterText progress theItem =
    case theItem of
        Item data ->
            Item { data | after = Just (Progress progress) }


withSubItems : List (Item msg) -> Item msg -> Item msg
withSubItems items (Item data) =
    Item { data | items = items }


active : Item msg -> Item msg
active theItem =
    case theItem of
        Item data ->
            Item { data | active = True }


type Group msg
    = Group
        { title : Fluent
        , items : List (Item msg)
        }


group : Fluent -> List (Item msg) -> Group msg
group title items =
    Group
        { title = title
        , items = items
        }


type Model msg
    = Ungrouped
        { items : List (Item msg)
        , paddingX : Int
        , paddingY : Int
        , backgroundColor : Color
        }
    | Grouped
        { groups : List (Group msg)
        , paddingX : Int
        , paddingY : Int
        , backgroundColor : Color
        }


fromItems : List (Item msg) -> Model msg
fromItems items =
    Ungrouped
        { items = items
        , paddingX = 16
        , paddingY = 8
        , backgroundColor = surface
        }


fromGroups : List (Group msg) -> Model msg
fromGroups groups =
    Grouped
        { groups = groups
        , paddingX = 16
        , paddingY = 8
        , backgroundColor = surface
        }


withPaddingY : Int -> Model msg -> Model msg
withPaddingY paddingY model =
    case model of
        Ungrouped data ->
            Ungrouped { data | paddingY = paddingY }

        Grouped data ->
            Grouped { data | paddingY = paddingY }


withBackgroundColor : Color -> Model msg -> Model msg
withBackgroundColor backgroundColor model =
    case model of
        Ungrouped data ->
            Ungrouped { data | backgroundColor = backgroundColor }

        Grouped data ->
            Grouped { data | backgroundColor = backgroundColor }


toElement : Model msg -> Element msg
toElement model =
    case model of
        Ungrouped data ->
            column
                [ width fill
                , paddingXY 0 data.paddingY
                , Background.color data.backgroundColor
                ]
                (List.concatMap (viewItem 0 data.paddingX data.backgroundColor) data.items)

        Grouped data ->
            column
                [ width fill
                , paddingEach
                    { top = 0
                    , bottom = data.paddingY
                    , left = 0
                    , right = 0
                    }
                , spacing 16
                , Background.color data.backgroundColor
                ]
                (List.indexedMap (viewGroup data.paddingX data.paddingY data.backgroundColor) data.groups)


viewGroup : Int -> Int -> Color -> Int -> Group msg -> Element msg
viewGroup paddingX paddingY backgroundColor index (Group data) =
    column
        [ width fill
        , Background.color backgroundColor
        ]
        [ if index == 0 then
            none

          else
            Ui.Atom.Divider.horizontal
        , el
            [ paddingXY paddingX paddingY
            , Font.color onSurfaceVariant
            ]
            (Ui.Theme.Typography.caption data.title)
        , column
            [ width fill ]
            (List.concatMap (viewItem 0 paddingX backgroundColor) data.items)
        ]


viewItem : Int -> Int -> Color -> Item msg -> List (Element msg)
viewItem level paddingX backgroundColor theItem =
    case theItem of
        Item data ->
            case data.onPress of
                Msg onPress ->
                    [ viewButton level paddingX backgroundColor data onPress ]
                        ++ viewSubItems level paddingX backgroundColor data.items

                Url url ->
                    [ viewLink level paddingX backgroundColor data url ]
                        ++ viewSubItems level paddingX backgroundColor data.items


viewSubItems : Int -> Int -> Color -> List (Item msg) -> List (Element msg)
viewSubItems level paddingX backgroundColor items =
    List.concatMap (viewItem (level + 1) paddingX backgroundColor) items


viewButton : Int -> Int -> Color -> ItemData msg -> msg -> Element msg
viewButton level paddingX backgroundColor data onPress =
    Input.button
        (stopPropagationOnClick onPress
            :: preventDefaultOnKeyDown
                [ Accessibility.Key.enter ( onPress, True )
                , Accessibility.Key.space ( onPress, True )
                ]
            :: disabled False
            :: itemAttributes level paddingX backgroundColor data
        )
        { onPress = Nothing
        , label =
            row
                [ width fill
                , height fill
                , centerY
                ]
                [ viewBefore False data.before
                , viewItemBody data
                , viewErrorCount data.errorCount
                , viewAfter False data
                ]
        }


viewLink : Int -> Int -> Color -> ItemData msg -> String -> Element msg
viewLink level paddingX backgroundColor data url =
    Element.link (itemAttributes level paddingX backgroundColor data)
        { url = url
        , label =
            row
                [ width fill
                , height fill
                , centerY
                ]
                [ viewBefore True data.before
                , viewItemBody data
                , viewErrorCount data.errorCount
                , viewAfter True data
                ]
        }


itemAttributes : Int -> Int -> Color -> ItemData msg -> List (Attribute msg)
itemAttributes level paddingX backgroundColor data =
    let
        thePadding =
            if not hasSecondaryLine && not hasSecondaryParagraphs then
                if data.before == Nothing && data.after == Nothing then
                    paddingEach
                        { top = 13
                        , bottom = 14
                        , left = paddingX + 16 * level
                        , right = paddingX
                        }

                else
                    paddingEach
                        { top = 4
                        , bottom = 4
                        , left = paddingX + 16 * level
                        , right = paddingX
                        }

            else if data.before == Nothing && data.after == Nothing then
                paddingEach
                    { top = 11
                    , bottom = 12
                    , left = paddingX + 16 * level
                    , right = paddingX
                    }

            else
                paddingEach
                    { top = 11
                    , bottom = 12
                    , left = paddingX + 16 * level
                    , right = paddingX
                    }

        hasSecondaryLine =
            data.secondaryLine /= Nothing

        hasSecondaryParagraphs =
            data.secondaryParagraphs /= []
    in
    [ width fill
    , thePadding
    , Background.color
        (if data.active then
            secondaryContainer

         else
            backgroundColor
        )
    , mouseDown
        [ Background.color
            (pressedWith onSurface
                (if data.active then
                    secondaryContainer

                 else
                    backgroundColor
                )
            )
        ]
    , mouseOver
        [ Background.color
            (hoveredWith onSurface
                (if data.active then
                    secondaryContainer

                 else
                    backgroundColor
                )
            )
        ]
    , focused
        [ Background.color
            (focusedWith onSurface
                (if data.active then
                    secondaryContainer

                 else
                    backgroundColor
                )
            )
        ]
    ]


viewBefore : Bool -> Maybe (Before msg) -> Element msg
viewBefore isLink before =
    case before of
        Nothing ->
            none

        Just (BeforeIcon icon) ->
            el
                [ width (px 48)
                , height (px 48)
                , paddingEach
                    { top = 0
                    , bottom = 0
                    , left = 0
                    , right = 16
                    }
                , alignLeft
                , centerY
                ]
                (el [ centerY ] (Icon.viewNormal icon))

        Just (BeforeActionIcon icon onPress) ->
            el
                [ width (px 48)
                , height (px 48)
                , paddingEach
                    { top = 0
                    , bottom = 0
                    , left = 0
                    , right = 16
                    }
                , alignLeft
                , centerY
                ]
                (Input.button
                    [ centerX
                    , centerY
                    , Font.color onSurfaceVariant
                    , Border.rounded 24
                    , mouseDown [ Background.color (pressedWith onSurfaceVariant transparent) ]
                    , mouseOver [ Background.color (hoveredWith onSurfaceVariant transparent) ]
                    , focused [ Background.color (focusedWith onSurfaceVariant transparent) ]
                    , if isLink then
                        preventDefaultOnClick onPress

                      else
                        stopPropagationOnClick onPress
                    ]
                    { onPress = Nothing
                    , label = el [ padding 8 ] (Icon.viewNormal icon)
                    }
                )

        Just BeforeSpace ->
            el
                [ width (px 48)
                , height (px 48)
                , paddingEach
                    { top = 0
                    , bottom = 0
                    , left = 0
                    , right = 16
                    }
                , alignLeft
                , centerY
                ]
                none


viewItemBody : ItemData msg -> Element msg
viewItemBody data =
    [ data.title
        |> Ui.Theme.Typography.body1
        |> List.singleton
        |> paragraph
            [ width fill
            , Font.color onSurface
            ]
    , case data.secondaryLine of
        Nothing ->
            none

        Just secondaryLine ->
            if List.length secondaryLine == 0 then
                none

            else
                secondaryLine
                    |> List.indexedMap (viewSecondaryLineItem (List.length secondaryLine))
                    |> wrappedRow
                        [ width fill
                        , spacing 8
                        , Font.color onSurfaceVariant
                        ]
    , case data.secondaryParagraphs of
        [] ->
            none

        _ ->
            data.secondaryParagraphs
                |> List.reverse
                |> List.map
                    (paragraph
                        [ width fill
                        , Font.color onSurfaceVariant
                        ]
                    )
                |> column
                    [ width fill
                    , spacing 6
                    ]
    ]
        |> column
            [ width fill
            , spacing 6
            , paddingEach
                { top = 0
                , bottom = 0
                , left = 0
                , right = 16
                }
            ]


viewSecondaryLineItem : Int -> Int -> Element msg -> Element msg
viewSecondaryLineItem count index secondaryLineItem =
    [ secondaryLineItem
    , if index == count - 1 then
        none

      else
        body2 (t "item-separator")
    ]
        |> row [ spacing 8 ]


viewErrorCount : Maybe Int -> Element msg
viewErrorCount errorCount =
    case errorCount of
        Nothing ->
            none

        Just count ->
            row
                [ Font.color error
                , spacing 8
                , paddingXY 32 0
                ]
                [ Icon.Error
                    |> Icon.viewNormal
                    |> el [ centerY ]
                , "paragraph--error-count"
                    |> tr [ int "count" count ]
                    |> body1
                ]


viewAfter : Bool -> ItemData msg -> Element msg
viewAfter isLink data =
    let
        hasSecondaryLine =
            data.secondaryLine /= Nothing
    in
    case data.after of
        Nothing ->
            none

        Just after ->
            el
                [ alignRight
                , centerY
                ]
                (viewAfterText isLink hasSecondaryLine after)


viewAfterText : Bool -> Bool -> After msg -> Element msg
viewAfterText isLink hasSecondaryLine after =
    case after of
        AfterIcon icon ->
            el
                [ centerX
                , centerY
                ]
                (Icon.viewNormal icon)

        ActionIcon icon onPress ->
            Input.button
                [ centerX
                , centerY
                , Font.color onSurfaceVariant
                , Border.rounded 4
                , mouseDown [ Background.color (pressedWith onSurfaceVariant transparent) ]
                , mouseOver [ Background.color (hoveredWith onSurfaceVariant transparent) ]
                , focused [ Background.color (focusedWith onSurfaceVariant transparent) ]
                , if isLink then
                    preventDefaultOnClick onPress

                  else
                    stopPropagationOnClick onPress
                ]
                { onPress = Nothing
                , label = el [ padding 8 ] (Icon.viewNormal icon)
                }

        Progress progress ->
            el
                [ centerX
                , centerY
                ]
                (html
                    (Progress.Ring.view
                        { color = colorToHex primary
                        , progress = progress
                        , stroke = 4
                        , radius =
                            if hasSecondaryLine then
                                16

                            else
                                12
                        }
                    )
                )


colorToHex : Color -> String
colorToHex color =
    let
        { red, green, blue, alpha } =
            toRgb color
    in
    String.concat
        [ "rgba("
        , String.join ","
            [ String.fromFloat (red * 100) ++ "%"
            , String.fromFloat (green * 100) ++ "%"
            , String.fromFloat (blue * 100) ++ "%"
            , String.fromFloat alpha
            ]
        , ")"
        ]



---- FLUENT


t : String -> Fluent
t key =
    Fluent.text ("ui-molecule-list--" ++ key)


tr : List Fluent.Argument -> String -> Fluent
tr arguments id =
    Fluent.textWith arguments ("ui-molecule-list--" ++ id)
