{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Molecule.ComboboxSingleselect exposing
    ( comboboxSingleselect
    , Label, labelAbove
    )

{-|

@docs comboboxSingleselect
@docs Label, labelAbove, labelledBy

@docs main, Model, Msg

-}

import Accessibility.Clipboard as Clipboard
import Element
    exposing
        ( Element
        , el
        , fill
        , html
        , width
        )
import Fluent exposing (Fluent)
import Html exposing (Html)
import Html.Attributes
import Html.Events
import Json.Decode as Decode
import Maybe.Extra as Maybe
import Pointer
import Ui.Atom.OptionCombobox exposing (OptionCombobox)
import Ui.Molecule.ComboboxSingleselectCustomElement as CustomElement


comboboxSingleselect :
    { id : Pointer.Pointer
    , label : Label
    , supportingText : Maybe Fluent
    , help : List Fluent
    , options : List option
    , selected : Maybe option
    , toOption : option -> OptionCombobox
    , fromOption : OptionCombobox -> Maybe option
    , onChange : String -> msg
    , onSelect : option -> msg
    , onUnselect : msg
    , onCopy : Maybe msg
    , onPaste : Maybe ( String, msg, String -> Maybe msg )
    , noOp : msg
    , renderSubLabels : Bool
    }
    -> Element msg
comboboxSingleselect config =
    el
        [ width fill ]
        (html (comboboxSingleselectHelp config))


type alias Label =
    CustomElement.Label


labelAbove : Fluent -> Label
labelAbove =
    CustomElement.labelAbove


comboboxSingleselectHelp :
    { id : Pointer.Pointer
    , label : Label
    , supportingText : Maybe Fluent
    , help : List Fluent
    , options : List option
    , selected : Maybe option
    , toOption : option -> OptionCombobox
    , fromOption : OptionCombobox -> Maybe option
    , onChange : String -> msg
    , onSelect : option -> msg
    , onUnselect : msg
    , onCopy : Maybe msg
    , onPaste : Maybe ( String, msg, String -> Maybe msg )
    , noOp : msg
    , renderSubLabels : Bool
    }
    -> Html msg
comboboxSingleselectHelp config =
    let
        addOnCopy attrs =
            case config.onCopy of
                Nothing ->
                    attrs

                Just msg ->
                    Clipboard.onCopyHtml msg :: attrs

        addOnPaste attrs =
            case config.onPaste of
                Nothing ->
                    attrs

                Just ( format, msgPrevented, msg ) ->
                    Clipboard.onPasteHtml format msgPrevented msg :: attrs
    in
    Html.node CustomElement.nameNode
        ([ Html.Attributes.property "elmData"
            (CustomElement.encode
                { id = Pointer.toString config.id
                , label = config.label
                , supportingText = config.supportingText
                , help = config.help
                , options = List.map config.toOption config.options
                , selected = Maybe.map config.toOption config.selected
                , renderSubLabels = config.renderSubLabels
                , onCopy = Maybe.isJust config.onCopy
                }
            )
         , Html.Events.stopPropagationOn "elm-data-changed"
            (CustomElement.changedDecoder
                { onChange = config.onChange
                , onSelect =
                    \optionSelected ->
                        case config.fromOption optionSelected of
                            Nothing ->
                                config.noOp

                            Just option ->
                                config.onSelect option
                , onUnselect = config.onUnselect
                }
                |> Decode.field "detail"
                |> Decode.map (\msg -> ( msg, True ))
            )
         ]
            |> addOnCopy
            |> addOnPaste
        )
        []
