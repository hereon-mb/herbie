{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Molecule.Navigation exposing
    ( Item, itemLink, itemButton, itemGroup
    , isSelected, withLabel, newTab
    , Config, view
    )

{-|

@docs Item, itemLink, itemButton, itemGroup
@docs isSelected, withLabel, newTab
@docs Config, view

-}

import Context exposing (C)
import Element
    exposing
        ( Element
        , centerX
        , centerY
        , column
        , el
        , fill
        , focused
        , height
        , link
        , mouseDown
        , mouseOver
        , newTabLink
        , none
        , padding
        , paddingEach
        , paddingXY
        , px
        , row
        , scrollbarY
        , spacing
        , transparent
        , width
        , wrappedRow
        )
import Element.Background as Background
import Element.Border as Border
import Element.Extra exposing (pointerEvents, style)
import Element.Font as Font
import Element.Input as Input
import Fluent exposing (Fluent, toElement)
import Ui.Atom.Button as Button exposing (button)
import Ui.Atom.ButtonIcon as ButtonIcon exposing (buttonIcon)
import Ui.Atom.Divider as Divider
import Ui.Atom.Icon as Icon exposing (Icon)
import Ui.Theme.Color as Color
    exposing
        ( focusedWith
        , hoveredWith
        , onSecondaryContainer
        , onSurfaceVariant
        , outlineVariant
        , pressedWith
        , secondaryContainer
        , surfaceContainerLow
        )
import Ui.Theme.Logo as Logo
import Ui.Theme.Typography exposing (body2, newTabLink2)



-- ITEM


type Item msg
    = ItemLink DataItemLink
    | ItemButton (DataItemButton msg)
    | ItemGroup (DataItemGroup msg)


type alias DataItemLink =
    { label : Fluent
    , icon : Icon
    , url : String
    , newTab : Bool
    , selected : Bool
    }


type alias DataItemButton msg =
    { label : Fluent
    , icon : Icon
    , msg : msg
    , selected : Bool
    }


type alias DataItemGroup msg =
    { label : Maybe Fluent
    , items : List (Item msg)
    }


itemLink : String -> Icon -> Fluent -> Item msg
itemLink href icon label =
    ItemLink
        { label = label
        , icon = icon
        , url = href
        , newTab = False
        , selected = False
        }


itemButton : msg -> Icon -> Fluent -> Item msg
itemButton msg icon label =
    ItemButton
        { label = label
        , icon = icon
        , msg = msg
        , selected = False
        }


itemGroup : List (Item msg) -> Item msg
itemGroup items =
    ItemGroup
        { label = Nothing
        , items = items
        }


isSelected : Bool -> Item msg -> Item msg
isSelected selected item =
    case item of
        ItemLink data ->
            ItemLink { data | selected = selected }

        ItemButton data ->
            ItemButton { data | selected = selected }

        ItemGroup _ ->
            item


withLabel : Fluent -> Item msg -> Item msg
withLabel label item =
    case item of
        ItemLink data ->
            ItemLink { data | label = label }

        ItemButton data ->
            ItemButton { data | label = label }

        ItemGroup data ->
            ItemGroup { data | label = Just label }


newTab : Item msg -> Item msg
newTab item =
    case item of
        ItemLink data ->
            ItemLink { data | newTab = True }

        ItemButton _ ->
            item

        ItemGroup _ ->
            item



-- CONFIG


type alias Config msg =
    { items : List (Item msg)
    , expanded : Bool
    , fill : Bool
    , onToggleExpanded : msg
    , userPressedShowHelp : msg
    , userPressedSendFeedback : msg
    }



-- VIEW


view : C -> Config msg -> Element msg
view c config =
    column
        [ height fill
        , width
            (if config.fill then
                fill

             else if config.expanded then
                px 360

             else
                px (48 + 16)
            )
        , Background.color surfaceContainerLow
        , Border.width 0
        , Border.color outlineVariant
        ]
        [ viewTop config
        , viewMain c config
        ]


viewTop : Config msg -> Element msg
viewTop config =
    el
        [ width fill
        , height (px 48)
        , paddingXY 8 0
        ]
        (row
            [ spacing 8
            , centerY
            ]
            [ viewButtonHamburger config
            , viewLogo
            ]
        )


viewButtonHamburger : Config msg -> Element msg
viewButtonHamburger config =
    el
        [ width (px 64)
        , centerY
        ]
        (el
            [ centerX
            , centerY
            ]
            (Icon.MenuOpen
                |> buttonIcon config.onToggleExpanded
                |> ButtonIcon.toElement
            )
        )


viewLogo : Element msg
viewLogo =
    el
        [ height fill
        , width (px 96)
        , padding 8
        ]
        Logo.herbie


viewMain : C -> Config msg -> Element msg
viewMain c config =
    let
        addScrollbarY attrs =
            if lowScreen then
                scrollbarY :: attrs

            else
                attrs

        lowScreen =
            c.viewport.height < 900
    in
    column
        [ width fill
        , height fill
        , spacing 24
        , paddingEach
            { top = 20
            , bottom = 0
            , left = 11
            , right = 11
            }
        ]
        [ column
            ([ width fill
             , height fill
             , spacing 24
             ]
                |> addScrollbarY
            )
            [ viewDestinations config
            , if lowScreen then
                none

              else
                el [ height fill ] none
            , viewFeedbackActions c config
            ]
        ]


viewDestinations : Config msg -> Element msg
viewDestinations config =
    column
        [ width fill
        , spacing 4
        , paddingXY 8 0
        ]
        (List.indexedMap (viewDestination config.expanded) config.items)


viewDestination : Bool -> Int -> Item msg -> Element msg
viewDestination expanded index item =
    let
        colorLabel =
            if selected then
                onSecondaryContainer

            else
                onSurfaceVariant

        colorBackground =
            if selected then
                secondaryContainer

            else
                Color.transparent

        selected =
            case item of
                ItemLink data ->
                    data.selected

                ItemButton data ->
                    data.selected

                ItemGroup _ ->
                    False

        attributes =
            [ height (px 40)
            , width fill
            , Border.rounded 4
            , Background.color colorBackground
            , mouseDown [ Background.color (pressedWith colorLabel colorBackground) ]
            , mouseOver [ Background.color (hoveredWith colorLabel colorBackground) ]
            , focused [ Background.color (focusedWith colorLabel colorBackground) ]
            , Font.color colorLabel
            ]
    in
    case item of
        ItemLink data ->
            if data.newTab then
                newTabLink attributes
                    { url = data.url
                    , label = viewLabelDestination expanded data
                    }

            else
                link attributes
                    { url = data.url
                    , label = viewLabelDestination expanded data
                    }

        ItemButton data ->
            Input.button attributes
                { onPress = Just data.msg
                , label = viewLabelDestination expanded data
                }

        ItemGroup data ->
            column
                [ width fill ]
                [ viewDividerGroup index
                , viewLabelGroup data
                , column
                    [ width fill
                    , spacing 4
                    ]
                    (List.indexedMap (viewDestination expanded) data.items)
                ]


viewLabelDestination : Bool -> { rest | label : Fluent, icon : Icon } -> Element msg
viewLabelDestination expanded data =
    row
        [ width fill
        , spacing 24
        , padding 12
        ]
        [ Icon.viewSmall data.icon
        , if expanded then
            data.label
                |> toElement
                |> el
                    [ Font.size 16
                    , Font.medium
                    , width fill
                    ]

          else
            none
        ]


viewDividerGroup : Int -> Element msg
viewDividerGroup index =
    if index > 0 then
        el
            [ paddingEach
                { top = 4
                , bottom = 8
                , left = 0
                , right = 0
                }
            , width fill
            ]
            Divider.horizontal

    else
        none


viewLabelGroup : DataItemGroup msg -> Element msg
viewLabelGroup data =
    case data.label of
        Nothing ->
            none

        Just label ->
            el
                [ width fill
                , paddingEach
                    { top = 4
                    , bottom = 8
                    , left = 8
                    , right = 8
                    }
                ]
                (body2 label)


viewFeedbackActions : C -> Config msg -> Element msg
viewFeedbackActions c config =
    el
        [ paddingXY 8 0
        , width fill
        ]
        (if config.expanded then
            viewFeedbackActionsExpanded c config

         else
            viewFeedbackActionsCollapsed config
        )


viewFeedbackActionsExpanded : C -> Config msg -> Element msg
viewFeedbackActionsExpanded c config =
    column
        [ width fill
        , paddingEach
            { top = 8
            , bottom = 48
            , left = 8
            , right = 8
            }
        , spacing 16
        ]
        [ t "question-or-feedback"
            |> toElement
            |> el
                [ Font.color onSurfaceVariant
                , Font.size 18
                ]
        , t "show-help"
            |> button config.userPressedShowHelp
            |> Button.withFormat Button.Outlined
            |> Button.withColor onSurfaceVariant
            |> Button.withWidth fill
            |> Button.view
        , t "send-us-feedback"
            |> button config.userPressedSendFeedback
            |> Button.withFormat Button.Outlined
            |> Button.withColor onSurfaceVariant
            |> Button.withWidth fill
            |> Button.view
        , viewMetaLinks c
        ]


viewFeedbackActionsCollapsed : Config msg -> Element msg
viewFeedbackActionsCollapsed config =
    column
        [ spacing 4
        , paddingEach
            { top = 8
            , bottom = 48
            , left = 0
            , right = 0
            }
        , pointerEvents (not config.expanded)
        ]
        [ feedbackActionCollapsed config.userPressedShowHelp Icon.Help config.expanded
        , feedbackActionCollapsed config.userPressedSendFeedback Icon.Email config.expanded
        ]


feedbackActionCollapsed : msg -> Icon -> Bool -> Element msg
feedbackActionCollapsed onPress icon expanded =
    Input.button
        [ height (px 40)
        , width fill
        , Border.rounded 4
        , mouseDown [ Background.color (pressedWith onSurfaceVariant Color.transparent) ]
        , mouseOver [ Background.color (hoveredWith onSurfaceVariant Color.transparent) ]
        , focused [ Background.color (focusedWith onSurfaceVariant Color.transparent) ]
        , transparent expanded
        , style "transition" "opacity 300ms"
        ]
        { onPress = Just onPress
        , label =
            el
                [ width fill
                , spacing 24
                , padding 12
                , Font.color onSurfaceVariant
                ]
                (Icon.viewSmall icon)
        }


viewMetaLinks : C -> Element msg
viewMetaLinks c =
    wrappedRow
        [ centerX
        , spacing 8
        ]
        (List.intersperse
            (body2 (Fluent.verbatim "•"))
            (List.filterMap (\( label, url ) -> Maybe.map (viewLink label) url)
                [ ( "Imprint", c.imprintUrl )
                , ( "Data Protection", c.dataProtectionUrl )
                , ( "Accessibility", c.accessibilityUrl )
                ]
            )
        )


viewLink : String -> String -> Element msg
viewLink label url =
    newTabLink2
        { url = url
        , label = Fluent.verbatim label
        }



-- FLUENT


t : String -> Fluent
t key =
    Fluent.text ("ui-molecule-navigation--" ++ key)
