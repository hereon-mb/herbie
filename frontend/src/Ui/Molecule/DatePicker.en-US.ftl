ui-molecule-date-picker--cancel = Cancel
ui-molecule-date-picker--enter-time = ENTER TIME
ui-molecule-date-picker--hour--label = Hour
ui-molecule-date-picker--minute--label = Minute
ui-molecule-date-picker--ok = Ok
ui-molecule-date-picker--period--am = AM
ui-molecule-date-picker--period--pm = PM
