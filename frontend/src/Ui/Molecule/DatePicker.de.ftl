ui-molecule-date-picker--cancel = Abbrechen
ui-molecule-date-picker--enter-time = ZEIT EINGEBEN
ui-molecule-date-picker--hour--label = Stunde
ui-molecule-date-picker--minute--label = Minute
ui-molecule-date-picker--ok = O.K.
ui-molecule-date-picker--period--am = AM
ui-molecule-date-picker--period--pm = PM
