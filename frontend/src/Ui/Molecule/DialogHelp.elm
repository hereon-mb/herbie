{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Molecule.DialogHelp exposing
    ( Config
    , view
    )

import Element
    exposing
        ( Element
        , alignLeft
        , alignTop
        , column
        , el
        , fill
        , fillPortion
        , height
        , image
        , inFront
        , none
        , padding
        , paddingEach
        , paddingXY
        , paragraph
        , px
        , row
        , spacing
        , textColumn
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Extra
    exposing
        ( style
        )
import Fluent exposing (Fluent)
import Ui.Atom.Button as Button exposing (button)
import Ui.Atom.Scrolled as Scrolled
import Ui.Theme.Color
    exposing
        ( scrim
        , surface
        )
import Ui.Theme.Markdown
import Ui.Theme.Typography
    exposing
        ( body1
        )


type alias Config msg =
    { onClose : msg
    , onScroll : Scrolled.Offsets -> msg
    }


view : Config msg -> Scrolled.Offsets -> Element msg
view config offsets =
    el
        [ width fill
        , height fill
        , style "z-index" "1"
        , Background.color scrim
        , inFront (dialog config offsets)
        ]
        none


dialog : Config msg -> Scrolled.Offsets -> Element msg
dialog { onClose, onScroll } offsets =
    column
        [ width fill
        , height fill
        ]
        [ bufferHorizontal
        , row
            [ width fill
            , height (fillPortion 10)
            ]
            [ bufferVertical
            , column
                [ width (fillPortion 9)
                , height fill
                , Background.color surface
                , Border.rounded 4
                ]
                [ Scrolled.view
                    { content = content
                    , offsets = offsets
                    , onScroll = onScroll
                    , id = "help-scroll-container"
                    }
                , el
                    [ paddingXY 24 24 ]
                    (button onClose (t "help-dialog--close")
                        |> Button.view
                    )
                ]
            , bufferVertical
            ]
        , bufferHorizontal
        ]


bufferHorizontal : Element msg
bufferHorizontal =
    el
        [ width fill
        , height (fillPortion 1)
        ]
        none


bufferVertical : Element msg
bufferVertical =
    el
        [ width (fillPortion 4)
        , height fill
        ]
        none


content : Element msg
content =
    textColumn
        [ width (fillPortion 8)
        , height fill
        , spacing 32
        , padding 42
        ]
        [ Ui.Theme.Typography.h3 (t "help-dialog--heading")
        , Ui.Theme.Markdown.view introduction
        , section "what-is-it" whatIsIt
        , section "why" why
        , section "how" how
        , guide
        , section "features" features
        , section "language" language
        , section "devices" devices
        , section "login" login
        , section "security" security
        , section "technologies" technologies
        ]


section : String -> String -> Element msg
section name text =
    column
        [ paddingEach
            { top = 16
            , bottom = 0
            , left = 0
            , right = 0
            }
        , spacing 16
        ]
        [ Ui.Theme.Typography.h4 (t ("help-dialog--" ++ name ++ "--heading"))
        , Ui.Theme.Markdown.view text
        ]


guide : Element msg
guide =
    column
        [ spacing 32 ]
        [ row
            [ width fill
            , spacing 32
            ]
            [ image
                [ alignTop
                , width (px 200)
                , paddingEach
                    { top = 0
                    , bottom = 0
                    , left = 0
                    , right = 32
                    }
                ]
                { src = "/static/images/guide1.png"
                , description = ""
                }
            , paragraph
                [ alignTop ]
                [ body1 (Fluent.verbatim guide1)
                ]
            ]
        , row
            [ width fill
            , spacing 32
            ]
            [ image
                [ alignTop
                , width (px 128)
                , paddingEach
                    { top = 0
                    , bottom = 0
                    , left = 0
                    , right = 32
                    }
                ]
                { src = "/static/images/guide2.png"
                , description = ""
                }
            , paragraph
                [ alignTop ]
                [ body1 (Fluent.verbatim guide2)
                ]
            ]
        , row
            [ width fill
            , spacing 32
            ]
            [ image
                [ alignTop
                , width (px 300)
                , paddingEach
                    { top = 0
                    , bottom = 0
                    , left = 0
                    , right = 32
                    }
                ]
                { src = "/static/images/guide3.png"
                , description = ""
                }
            , paragraph
                [ alignTop ]
                [ body1 (Fluent.verbatim guide3) ]
            ]
        , image
            [ alignLeft
            , width fill
            ]
            { src = "/static/images/guide4.png"
            , description = ""
            }
        , paragraph
            [ width fill ]
            [ body1 (Fluent.verbatim guide4) ]
        , image
            [ alignLeft
            , width fill
            ]
            { src = "/static/images/guide5.png"
            , description = ""
            }
        , paragraph
            [ width fill ]
            [ body1 (Fluent.verbatim guide5) ]
        , image
            [ alignLeft
            , width fill
            ]
            { src = "/static/images/guide6.png"
            , description = ""
            }
        , paragraph
            [ width fill ]
            [ body1 (Fluent.verbatim guide6) ]
        , image
            [ alignLeft
            , width fill
            ]
            { src = "/static/images/guide7.png"
            , description = ""
            }
        , paragraph
            [ width fill ]
            [ body1 (Fluent.verbatim guide7) ]
        , image
            [ alignLeft
            , width fill
            ]
            { src = "/static/images/guide8.png"
            , description = ""
            }
        , paragraph
            [ width fill ]
            [ body1 (Fluent.verbatim guide8) ]
        ]



-- {{{ HELP CONTENT


introduction : String
introduction =
    """
*This version is a sandbox, feel free to try out all the functionalities. It is
not meant to contain actual real entries, so please note that the database will
be cleared weekly.*

*The research data platform is still under development and will be extended
continuously. Please keep in mind that some features will only be implemented
soon, like additional protocols, a proper change log or more complex access
rights mechanisms. Another version of the application is available for a small
group of people for already starting to collect data of actually performed
processes. We will inform you when this is available for you!*
"""


whatIsIt : String
whatIsIt =
    """
The research data platform is supposed to facilitate the documentation of
scientific processes at the institute. It is a combination of an Electronic
Laboratory Notebook (ELN) and a research database.

You can use it just like your usual paper lab notebook, write down whatever you
want, make simple drawings, create tables or attach images. You can also use it
as a device journal – make notes to a certain device (regarding experiments,
repairs, any kind of issues, …), tag the device and the entry will
automatically be visible in your personal notebook as well as in the respective
device journal! For standardized material fabrication processes or experiments,
there are pre-defined protocols that help you document all the important steps,
settings, and results. Primary data can simply be uploaded, so that you have
all your stuff together in one central system. The nice thing is, everything is
interconnected, so once a piece of information is inserted into the system, you
can access it from many perspectives.

Via the research database you can request all the entries made in the process
protocols. You can get information about the history of certain material
materials and details of individual analyses. You want to see whether a specific
alloy has already been drawn to wires? No problem, the research database offers
you an overview about what research has been done at the institute.

Data security and intellectual property regulations are carefully considered
throughout the whole development.
    """


why : String
why =
    """
The [FAIR principles][1] of data management are becoming more and more important
within the science community. FAIR stands for Findable, Accessible,
Interoperable and Reproducible. The idea is to increase the reproducibility and
reusability of scientific results by storing the corresponding data in such way
that other researchers can find and use them. At MB, we want to better
interconnect the scientists and to preserve knowledge of previous experiments
or employees by making all data available in one central system. Another
motivation is the increased demand to apply simulation or machine learning
methods on the basis of experimental data to gain insights from a new
perspective.

Already available ELN software solutions do not fit the specific needs at MB,
especially regarding the strong connection of materials science and biological
characterization as well as the customized integration of pre-defined process
protocols. In order to unite the documentation system of the three MB
facilities at Hereon in Geesthacht, DESY in Hamburg and MOIN CC in Kiel, it was
decided to build an own application. It started as a pilot project for MB, but
might be extended to further institutes if desired. Currently, we are two
people working on the ELN: Catriona Eschke and Fabian Kirchner, both members of
MBP.

[1]: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4792175/
"""


how : String
how =
    """
We hope that most functionalities are self-explanatory. With this sandbox
version, you can just try out all the buttons and entry fields without breaking
anything. However, here is a small guide to help you find your way around.
"""


features : String
features =
    """
We are continuously working on the research data platform and plan to extend it
step by step. These are some of the features that will be implemented soon to
make the research data platform even more useful and nice to use:

- More protocols (Obviously! We are currently focusing on completing the protocols of the materials development of cast alloys and will extend to powder-based methods and biological in-vitro characterization soon. Extensions to DESY and MOIN CC will follow right after.)
- Further  views of the research database
- Export functions to pdf
- Canvas for making simple drawings
- Planning of experiments and a calendar view
- Interfaces to other documentation systems and databases
- Inventories of the raw materials
- More complex access rights mechanisms
- A proper change log for verifiability of the entries
- And much more!
"""


language : String
language =
    """
The ELN is available in English and German. It is set according to your
preferences in your browser.
"""


devices : String
devices =
    """
The research data platform is implemented as a web application. That means that
you principally only need a web-enabled end device with a (not too old) web
browser. Your office or lab computer, notebook, tablet or even smartphone – all
is possible. All common web browsers should be able to run the application,
like Google Chrome, Mozilla Firefox or Windows Edge. If you encounter any
problems with your end device, just let us now.

For safety reasons, the ELN is only accessible via the Hereon network. If you
want to use the ELN from outside Hereon, you will need to use the VPN.
"""


login : String
login =
    """
For your convenience, we have implemented single-sign-on which means that you
can use your usual Hereon-account for logging in to the research data platform.
You do not need to create a new account or remember yet another password. Also,
this mechanism ensures that only people who are employed at Hereon can access the
application.
"""


security : String
security =
    """
Your data is protected by various safety measures. All data is stored safely at
Hereon servers which are regularly backed up. The research data platform can
only be used within the Hereon network, so far there is no connection to the
general internet. Only registered users, who all must be employed at Hereon,
have access to the contents of the application. The implementation of the
application complies with best practices of software development. We always
consider privacy of personal data and are in close contact to the responsible
contact persons at Hereon. The idea of the research data platform is that data
can easily be shared for collaborations and that scientists at MB are able to
get an overview of what has already been done at the institute. However, it is
guaranteed that a creator of a dataset can use it for own purposes before it is
openly available to others. Of course, sensitive data, for example from
projects with the industry, can be kept private as well.

We will soon publish a more detailed explanation of our data security and
intellectual property concept.
"""


technologies : String
technologies =
    """
We are building the research data platform as a web application. The backend is
programmed within the [Django framework][2] where we write our code in Python.
For the frontend, we use [Elm][3]. All the data is stored in a [PostGreSQL][4]
database which we extend with another fileserver system for large primary data.
If you are interested in learning more about the development, feel free to
contact us!

If you still have further questions or any kind of feedback, just contact us!
We are happy to help.

- [herbie@hereon.de](mailto:herbie@hereon.de)

[2]: https://www.djangoproject.com/
[3]: https://elm-lang.org/
[4]: https://www.postgresql.org/
"""


guide1 : String
guide1 =
    """
Navigate through the application with the menu on the left.  With the yellow
button, you can create new notes or protocols.  The icon in the top left corner
lets you collapse or expand the menu.
"""


guide2 : String
guide2 =
    """
These icons are in the top right corner.  The left one lets you filter the
journal entries.  The arrow indicates whether the newest journal entries are at
the top or the bottom of the page.
"""


guide3 : String
guide3 =
    """
When you picked the journal in the navigation menu, all your journal entries
are in the center of the screen. With the pen icon on the right, it is possible
to edit an entry after it has been published. The chips show which filters are
currently selected. With the filters, you can view your journal from different
perspectives. For example, you can filter by equipment, so that you see the
device journal of that specific piece of equipment. Also, all entries to
a certain material can be displayed here.
"""


guide4 : String
guide4 =
    """
This is the dialog for creating a new text note. You write your text on the
left.  Markdown is a way of formatting text. It is explained in the Markdown
cheatsheet, which you can open via the yellow link on the bottom left.
A preview of the formatted text is shown on the right.  The top left input
field lets you enter tags to link the whole note to a piece of equipment or
a material. These tags are also used for the filtering functionality.  Images
or any other type of document can be attached via the “Attach file” button in
the center.  You can publish, cancel or save the draft for later with the
buttons on the bottom left.  The “back” arrow at the top left navigates you to
a list of other drafts.
"""


guide5 : String
guide5 =
    """
The navigations principles of creating a protocol are the same as those for
textual notes. The difference is that here you have pre-defined entry fields
which only allow certain input.
"""


guide6 : String
guide6 =
    """
When you have saved drafts, you can access them via this box at the bottom of
the page.
"""


guide7 : String
guide7 =
    """
The entries made in a protocol directly feed into the research database which
you can access via the “Materials” button in the navigation menu. The existing
materials are listed here.  With the “gear” icon on the top right, you can
select which information to a material you want to see.  In the top left, you
can add filters to see only a subset of the materials with certain properties.
At the bottom, there is the option to download the shown materials information
to a .csv file.
"""


guide8 : String
guide8 =
    """
If you click on the link to a device or navigate the equipment page in the
menu, you can find information about the respective equipment in this view. The
right side shows the journal filtered by this equipment.
"""



-- }}}


t : String -> Fluent
t key =
    Fluent.text ("ui-molecule-dialog-help--" ++ key)
