ui-molecule-navigation--clear-store = Clear cache
ui-molecule-navigation--logout = Logout
ui-molecule-navigation--question-or-feedback = Got questions or ideas?
ui-molecule-navigation--send-us-feedback = Send us feedback
ui-molecule-navigation--show-help = Show help
