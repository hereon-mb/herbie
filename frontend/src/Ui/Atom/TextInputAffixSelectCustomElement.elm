{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Atom.TextInputAffixSelectCustomElement exposing
    ( Affix
    , Align(..)
    , External
    , Model
    , Msg
    , encode
    , main
    , nameNode
    , onElmDataChanged
    )

import Browser
import Browser.Dom
import Element
    exposing
        ( Element
        , centerX
        , centerY
        , el
        , fill
        , focusStyle
        , focused
        , height
        , htmlAttribute
        , mouseDown
        , mouseOver
        , noStaticStyleSheet
        , none
        , paddingXY
        , pointer
        , px
        , row
        , spacing
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Extra
    exposing
        ( ariaChecked
        , id
        , role
        , tabindex
        )
import Element.Font as Font
import Fluent
import Html exposing (Html)
import Html.Attributes
import Html.Events
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Decode
import Json.Encode as Encode exposing (Value)
import List.Extra
import Maybe.Extra as Maybe
import Ports.CustomElement as Ports
import Rdf exposing (Iri)
import Task
import Ui.Atom.Icon as Icon
import Ui.Theme.Color
    exposing
        ( focusedWith
        , hoveredWith
        , onSecondaryContainer
        , onSurface
        , outline
        , pressedWith
        , secondaryContainer
        , transparent
        )
import Ui.Theme.Typography


main : Program Value Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }


type Model
    = Running External
    | Failed


type alias External =
    { id : String
    , affixes : List Affix
    , selected : Affix
    , align : Align
    }


type Align
    = AlignLeft
    | AlignRight


type alias Affix =
    { label : String
    , value : Iri
    }


init : Value -> ( Model, Cmd Msg )
init flags =
    case Decode.decodeValue decoder flags of
        Err _ ->
            ( Failed
            , Cmd.none
            )

        Ok external ->
            ( Running external
            , Cmd.none
            )


type Msg
    = NoOp
    | ElmDataChanged Value
    | UserClickedButton Int
    | UserPressedSpace Int
    | UserPressedArrowUp
    | UserPressedArrowDown
    | UserPressedArrowLeft
    | UserPressedArrowRight


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case model of
        Running external ->
            updateHelp msg external

        Failed ->
            ( model, Cmd.none )


updateHelp : Msg -> External -> ( Model, Cmd Msg )
updateHelp msg external =
    case msg of
        NoOp ->
            ( Running external, Cmd.none )

        ElmDataChanged value ->
            case Decode.decodeValue decoder value of
                Err _ ->
                    ( Running external, Cmd.none )

                Ok newExternal ->
                    ( Running newExternal
                    , Cmd.none
                    )

        UserClickedButton index ->
            ( Running external, select external index )

        UserPressedSpace index ->
            ( Running external, select external index )

        UserPressedArrowUp ->
            ( Running external
            , external
                |> indexPrevious
                |> Maybe.unwrap Cmd.none (select external)
            )

        UserPressedArrowDown ->
            ( Running external
            , external
                |> indexNext
                |> Maybe.unwrap Cmd.none (select external)
            )

        UserPressedArrowLeft ->
            ( Running external
            , external
                |> indexPrevious
                |> Maybe.unwrap Cmd.none (select external)
            )

        UserPressedArrowRight ->
            ( Running external
            , external
                |> indexNext
                |> Maybe.unwrap Cmd.none (select external)
            )


indexNext : External -> Maybe Int
indexNext external =
    let
        affixesCount =
            List.length external.affixes
    in
    case List.Extra.findIndex (\affix -> affix == external.selected) external.affixes of
        Nothing ->
            Nothing

        Just index ->
            if index == affixesCount - 1 then
                Just 0

            else
                Just (index + 1)


indexPrevious : External -> Maybe Int
indexPrevious external =
    let
        affixesCount =
            List.length external.affixes
    in
    case List.Extra.findIndex (\affix -> affix == external.selected) external.affixes of
        Nothing ->
            Nothing

        Just index ->
            if index == 0 then
                Just (affixesCount - 1)

            else
                Just (index - 1)


select : External -> Int -> Cmd Msg
select external index =
    case List.Extra.getAt index external.affixes of
        Nothing ->
            Cmd.none

        Just affix ->
            Cmd.batch
                [ affix
                    |> encodeAffix
                    |> Ports.changeElmData
                , Task.attempt (\_ -> NoOp) (Browser.Dom.focus (idOption external.id index))
                ]


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        Running _ ->
            Ports.elmDataChanged ElmDataChanged

        Failed ->
            Sub.none


view : Model -> Html Msg
view model =
    case model of
        Running external ->
            Html.node nameNode
                [ Html.Attributes.style "display" "contents" ]
                [ Element.layoutWith
                    { options =
                        [ noStaticStyleSheet
                        , focusStyle
                            { borderColor = Nothing
                            , backgroundColor = Nothing
                            , shadow = Nothing
                            }
                        ]
                    }
                    [ width fill
                    , height fill
                    , Font.family
                        [ Font.typeface "Work Sans"
                        , Font.sansSerif
                        ]
                    ]
                    (viewHelp external)
                ]

        Failed ->
            Html.node nameNode
                [ Html.Attributes.style "display" "contents" ]
                [ Html.text "Could not initialize custom element" ]


viewHelp : External -> Element Msg
viewHelp config =
    config.affixes
        |> List.indexedMap (viewButton config)
        |> row
            [ width fill
            , height fill
            ]
        |> el
            [ width fill
            , height fill
            , role "radiogroup"
            , tabindex -1
            , id config.id
            ]


viewButton : External -> Int -> Affix -> Element Msg
viewButton external index affix =
    let
        checked =
            affix == external.selected

        theTabindex =
            if checked then
                0

            else
                -1

        addBackground attrs =
            if checked then
                Background.color secondaryContainer
                    :: mouseDown [ Background.color (pressedWith onSecondaryContainer secondaryContainer) ]
                    :: mouseOver [ Background.color (hoveredWith onSecondaryContainer secondaryContainer) ]
                    :: focused [ Background.color (focusedWith onSecondaryContainer secondaryContainer) ]
                    :: attrs

            else
                Background.color transparent
                    :: mouseDown [ Background.color (pressedWith onSurface transparent) ]
                    :: mouseOver [ Background.color (hoveredWith onSurface transparent) ]
                    :: focused [ Background.color (focusedWith onSurface transparent) ]
                    :: attrs

        addFont attrs =
            if checked then
                Font.color onSecondaryContainer :: attrs

            else
                Font.color onSurface :: attrs

        addBorderRadius attrs =
            if index == 0 then
                Border.roundEach
                    { topLeft = 20
                    , topRight = 0
                    , bottomRight = 0
                    , bottomLeft = 20
                    }
                    :: attrs

            else if index == List.length external.affixes - 1 then
                Border.roundEach
                    { topLeft = 0
                    , topRight = 20
                    , bottomRight = 20
                    , bottomLeft = 0
                    }
                    :: attrs

            else
                attrs

        addBorderWidth attrs =
            if index == 0 then
                Border.width 1
                    :: attrs

            else
                Border.widthEach
                    { top = 1
                    , bottom = 1
                    , left = 0
                    , right = 1
                    }
                    :: attrs

        handleKey rawKey =
            case rawKey of
                "Space" ->
                    if checked then
                        Decode.fail "not handling that key here"

                    else
                        Decode.succeed ( UserPressedSpace index, True )

                "ArrowUp" ->
                    Decode.succeed ( UserPressedArrowUp, True )

                "ArrowDown" ->
                    Decode.succeed ( UserPressedArrowDown, True )

                "ArrowLeft" ->
                    Decode.succeed ( UserPressedArrowLeft, True )

                "ArrowRight" ->
                    Decode.succeed ( UserPressedArrowRight, True )

                _ ->
                    Decode.fail "not handling that key here"
    in
    [ if checked then
        Icon.Done
            |> Icon.viewSmall

      else
        none
    , affix.label
        |> Fluent.verbatim
        |> Ui.Theme.Typography.button
    ]
        |> row
            [ centerY
            , centerX
            , spacing 4
            ]
        |> el
            ([ height fill
             , width (px 80)
             , paddingXY 12 0
             , Border.widthEach
                (case external.align of
                    AlignRight ->
                        { top = 0
                        , bottom = 0
                        , left = 1
                        , right = 0
                        }

                    AlignLeft ->
                        { top = 0
                        , bottom = 0
                        , left = 0
                        , right = 1
                        }
                )
             , if
                (external.align == AlignRight)
                    && (index == List.length external.affixes - 1)
               then
                Border.roundEach
                    { topLeft = 0
                    , topRight = 4
                    , bottomRight = 4
                    , bottomLeft = 0
                    }

               else if (external.align == AlignLeft) && (index == 0) then
                Border.roundEach
                    { topLeft = 4
                    , topRight = 0
                    , bottomRight = 0
                    , bottomLeft = 4
                    }

               else
                Border.rounded 0
             , Border.color outline
             , pointer
             , role "radio"
             , ariaChecked checked
             , tabindex theTabindex
             , htmlAttribute
                (Html.Events.preventDefaultOn "keydown"
                    (Decode.andThen handleKey (Decode.field "key" Decode.string))
                )
             , Events.onClick (UserClickedButton index)
             , id (idOption external.id index)
             ]
                |> addBackground
                |> addFont
                |> addBorderRadius
                |> addBorderWidth
            )


onElmDataChanged : (Affix -> msg) -> Html.Attribute msg
onElmDataChanged msg =
    Html.Events.on "elm-data-changed"
        (affixDecoder
            |> Decode.field "detail"
            |> Decode.map msg
        )


idOption : String -> Int -> String
idOption id index =
    id ++ "-option-" ++ String.fromInt index


decoder : Decoder External
decoder =
    Decode.succeed External
        |> Decode.required "id" Decode.string
        |> Decode.required "affixes" (Decode.list affixDecoder)
        |> Decode.required "selected" affixDecoder
        |> Decode.required "align" alignDecoder


affixDecoder : Decoder Affix
affixDecoder =
    Decode.succeed Affix
        |> Decode.required "label" Decode.string
        |> Decode.required "value" (Decode.map Rdf.iri Decode.string)


alignDecoder : Decoder Align
alignDecoder =
    Decode.string
        |> Decode.andThen
            (\string ->
                case string of
                    "left" ->
                        Decode.succeed AlignLeft

                    "right" ->
                        Decode.succeed AlignRight

                    _ ->
                        Decode.fail ("expected `left', `right', but got `" ++ string ++ "'")
            )


encode : External -> Value
encode external =
    Encode.object
        [ ( "id", Encode.string external.id )
        , ( "affixes", Encode.list encodeAffix external.affixes )
        , ( "selected", encodeAffix external.selected )
        , ( "align", encodeAlign external.align )
        ]


encodeAffix : Affix -> Value
encodeAffix affix =
    Encode.object
        [ ( "label", Encode.string affix.label )
        , ( "value", Encode.string (Rdf.toUrl affix.value) )
        ]


encodeAlign : Align -> Value
encodeAlign align =
    Encode.string
        (case align of
            AlignLeft ->
                "left"

            AlignRight ->
                "right"
        )


nameNode : String
nameNode =
    "ui-atom-text-input-affix-select"
