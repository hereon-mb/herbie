{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Atom.TextInput exposing
    ( TextInput, view
    , textInput, usernameInput, currentPasswordInput
    , Label, labelAbove
    , withId
    , withSupportingText
    , Affix, withAffix, withAffixes
    , withSuffix
    , withReadonly
    , ActionIcon, withHoverableActionIcon
    , onEnter
    , onPasteGraph
    , onCopy
    )

{-|

@docs TextInput, view

@docs textInput, usernameInput, currentPasswordInput

@docs Label, labelAbove

@docs withId
@docs withSupportingText
@docs Affix, withAffix, withAffixes
@docs withSuffix
@docs withReadonly
@docs ActionIcon, withHoverableActionIcon

@docs onEnter
@docs onPasteGraph

-}

import Accessibility.Clipboard as Clipboard
import Element
    exposing
        ( Attribute
        , Element
        , centerX
        , centerY
        , el
        , fill
        , height
        , html
        , htmlAttribute
        , minimum
        , none
        , paddingEach
        , paddingXY
        , px
        , row
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Extra
    exposing
        ( ariaLabelledby
        , class
        )
import Element.Font as Font
import Element.Input as Input
import Fluent exposing (Fluent, verbatim)
import Html
import Html.Attributes
import Html.Events
import Json.Decode as Decode
import List.Extra as List
import Rdf exposing (Iri)
import Rdf.Decode exposing (Decoder)
import Shacl.Clipboard as Clipboard
import Ui.Atom.FocusBorder as FocusBorder exposing (focusBorder)
import Ui.Atom.Icon as Icon exposing (Icon)
import Ui.Atom.TextInputAffixSelectCustomElement as TextInputAffixSelectCustomElement
import Ui.Theme.Color
    exposing
        ( onSurface
        , outline
        , transparent
        )
import Ui.Theme.Typography


type TextInput msg
    = TextInput (TextInputData msg)


type alias TextInputData msg =
    { kind : Kind
    , label : Label
    , text : String
    , onChange : String -> msg
    , onClear : Maybe msg
    , onEnter : Maybe msg
    , onBlur : Maybe msg
    , onPasteGraph : Maybe ( msg, Decoder msg )
    , onCopy : Maybe msg
    , id : Maybe String
    , help : List Fluent
    , affix : Maybe Affix
    , userChangedAffix : Maybe (Affix -> msg)
    , affixes : List Affix
    , prefix : Bool
    , supportingText : Maybe Fluent
    , leadingIcon : Maybe Icon
    , actionIcon : Maybe (ActionIcon msg)
    , actionIconHoverable : Maybe (ActionIcon msg)
    , density : Int
    , readonly : Bool
    , attributes : List (Attribute msg)
    }


type alias Affix =
    { label : String
    , value : Iri
    }


type alias ActionIcon msg =
    { icon : Icon
    , onClick : msg
    }


type Kind
    = Standard
    | Username
    | CurrentPassword Bool


type Label
    = LabelAbove Fluent


labelAbove : Fluent -> Label
labelAbove =
    LabelAbove


textInput : (String -> msg) -> Label -> String -> TextInput msg
textInput onChange label text =
    TextInput (init Standard onChange label text)


usernameInput : (String -> msg) -> Label -> String -> TextInput msg
usernameInput onChange label text =
    TextInput (init Username onChange label text)


currentPasswordInput : (String -> msg) -> Label -> String -> TextInput msg
currentPasswordInput onChange label text =
    TextInput (init (CurrentPassword False) onChange label text)


init : Kind -> (String -> msg) -> Label -> String -> TextInputData msg
init kind onChange label text =
    { kind = kind
    , label = label
    , text = text
    , onChange = onChange
    , onClear = Nothing
    , onEnter = Nothing
    , onBlur = Nothing
    , onPasteGraph = Nothing
    , onCopy = Nothing
    , id = Nothing
    , help = []
    , affix = Nothing
    , userChangedAffix = Nothing
    , affixes = []
    , prefix = True
    , supportingText = Nothing
    , leadingIcon = Nothing
    , actionIcon = Nothing
    , actionIconHoverable = Nothing
    , density = 0
    , readonly = False
    , attributes = []
    }


withId : String -> TextInput msg -> TextInput msg
withId id (TextInput data) =
    TextInput { data | id = Just id }


withAffix : Affix -> TextInput msg -> TextInput msg
withAffix affix (TextInput data) =
    TextInput
        { data
            | affix = Just affix
            , affixes =
                (affix :: data.affixes)
                    |> List.unique
        }


withAffixes : (Affix -> msg) -> List Affix -> TextInput msg -> TextInput msg
withAffixes userChangedAffix affixes (TextInput data) =
    TextInput
        { data
            | userChangedAffix = Just userChangedAffix
            , affixes = affixes
        }


withSuffix : TextInput msg -> TextInput msg
withSuffix (TextInput data) =
    TextInput { data | prefix = False }


withSupportingText : Fluent -> TextInput msg -> TextInput msg
withSupportingText supportingText (TextInput data) =
    TextInput { data | supportingText = Just supportingText }


onEnter : msg -> TextInput msg -> TextInput msg
onEnter msg (TextInput data) =
    TextInput { data | onEnter = Just msg }


onPasteGraph : msg -> Decoder msg -> TextInput msg -> TextInput msg
onPasteGraph msgIgnored decoderMsg (TextInput data) =
    TextInput { data | onPasteGraph = Just ( msgIgnored, decoderMsg ) }


onCopy : msg -> TextInput msg -> TextInput msg
onCopy msg (TextInput data) =
    TextInput { data | onCopy = Just msg }


withReadonly : Bool -> TextInput msg -> TextInput msg
withReadonly readonly (TextInput data) =
    TextInput { data | readonly = readonly }


withHoverableActionIcon : ActionIcon msg -> TextInput msg -> TextInput msg
withHoverableActionIcon actionIcon (TextInput data) =
    TextInput { data | actionIconHoverable = Just actionIcon }



---- VIEW


view : TextInput msg -> Element msg
view (TextInput data) =
    let
        addWithSupportingText =
            case data.supportingText of
                Nothing ->
                    identity

                Just supportingText ->
                    FocusBorder.withSupportingText supportingText

        addWithLeadingIcon =
            case data.leadingIcon of
                Nothing ->
                    identity

                Just _ ->
                    FocusBorder.withLeadingIcon

        addWithActionIcon =
            case data.actionIcon of
                Nothing ->
                    identity

                Just actionIcon ->
                    FocusBorder.withActionIcon actionIcon.icon actionIcon.onClick

        addWithHoverableActionIcon =
            case data.actionIconHoverable of
                Nothing ->
                    identity

                Just actionIcon ->
                    FocusBorder.withHoverableActionIcon actionIcon.icon actionIcon.onClick

        withLabel =
            case data.label of
                LabelAbove labelText ->
                    FocusBorder.withLabel
                        { id = idLabel data
                        , text = labelText
                        }

        withEmpty =
            if data.text == "" then
                FocusBorder.empty

            else
                identity

        withOnClear =
            case data.onClear of
                Nothing ->
                    identity

                Just msg ->
                    FocusBorder.onClear msg

        paddingPx =
            81 * List.length data.affixes

        paddingPxReadonlyIcon =
            if data.readonly then
                48

            else
                0
    in
    viewComponent data
        |> focusBorder
        |> FocusBorder.withDensity data.density
        |> FocusBorder.withHelp data.help
        |> (if data.prefix then
                FocusBorder.withPaddingLeftPx paddingPx
                    >> FocusBorder.withPaddingRightPx paddingPxReadonlyIcon

            else
                FocusBorder.withPaddingRightPx (paddingPx + paddingPxReadonlyIcon)
           )
        |> addWithSupportingText
        |> addWithLeadingIcon
        |> addWithActionIcon
        |> addWithHoverableActionIcon
        |> withLabel
        |> withEmpty
        |> withOnClear
        |> FocusBorder.toElement


viewComponent : TextInputData msg -> Element msg
viewComponent data =
    (if data.prefix then
        [ viewLeadingIcon data
        , viewAffixes data
        , viewInput data
        ]

     else
        [ viewLeadingIcon data
        , viewInput data
        , viewAffixes data
        ]
    )
        |> row
            [ width fill
            , height (px (56 - data.density * 4))
            , class "ui-atom-text-input"
            ]


viewLeadingIcon : TextInputData msg -> Element msg
viewLeadingIcon data =
    case data.leadingIcon of
        Nothing ->
            none

        Just leadingIcon ->
            leadingIcon
                |> Icon.viewNormal
                |> el
                    [ paddingEach
                        { top = 0
                        , bottom = 0
                        , left = 12
                        , right = 0
                        }
                    , centerY
                    ]


viewInput : TextInputData msg -> Element msg
viewInput data =
    (if data.readonly then
        viewReadonly data

     else
        viewInputHelp data
    )
        |> el
            [ width fill
            , height fill
            , case data.affix of
                Nothing ->
                    Border.rounded 4

                Just _ ->
                    Border.roundEach
                        { topLeft = 4
                        , topRight = 0
                        , bottomRight = 0
                        , bottomLeft = 4
                        }
            ]


viewReadonly : TextInputData msg -> Element msg
viewReadonly data =
    let
        attributes =
            ([ width (minimum 32 fill)
             , height fill
             , Background.color transparent
             , Font.size 18
             , paddingXY 16 (19 - data.density * 2)
             ]
                |> addId data
                |> addAriaLabelledBy data
            )
                ++ data.attributes
    in
    [ data.text
        |> Element.text
        |> el attributes
    , Icon.EditOff
        |> Icon.viewNormal
        |> el
            [ paddingEach
                { top = 0
                , bottom = 0
                , left = 0
                , right =
                    if List.isEmpty data.help then
                        16

                    else
                        48
                }
            , centerY
            ]
    ]
        |> row
            [ width fill
            , height fill
            ]


viewInputHelp : TextInputData msg -> Element msg
viewInputHelp data =
    let
        attributes =
            ([ width (minimum 32 fill)
             , Font.size 18
             , height fill
             , paddingXY 16 (19 - data.density * 2)
             , Background.color transparent
             , Border.width 0
             ]
                |> addId data
                |> addAriaLabelledBy data
                |> addOnEnter
                |> addOnBlur
                |> addOnPaste
                |> addOnCopy
            )
                ++ data.attributes

        addOnEnter attrs =
            case data.onEnter of
                Nothing ->
                    attrs

                Just msg ->
                    htmlAttribute
                        (Html.Events.on "keyup"
                            (Decode.field "key" Decode.string
                                |> Decode.andThen
                                    (\key ->
                                        if key == "Enter" then
                                            Decode.succeed msg

                                        else
                                            Decode.fail "not handling that key here"
                                    )
                            )
                        )
                        :: attrs

        addOnBlur attrs =
            case data.onBlur of
                Nothing ->
                    attrs

                Just msg ->
                    Events.onLoseFocus msg :: attrs

        addOnPaste attrs =
            case data.onPasteGraph of
                Nothing ->
                    attrs

                Just ( msgIgnored, decoderMsg ) ->
                    Clipboard.onPasteGraph msgIgnored decoderMsg :: attrs

        addOnCopy attrs =
            case data.onCopy of
                Nothing ->
                    attrs

                Just msg ->
                    Clipboard.onCopy msg :: attrs
    in
    case data.kind of
        Standard ->
            Input.text attributes
                { onChange = data.onChange
                , text = data.text
                , placeholder = Nothing
                , label = Input.labelHidden ""
                }

        Username ->
            Input.username attributes
                { onChange = data.onChange
                , text = data.text
                , placeholder = Nothing
                , label = Input.labelHidden ""
                }

        CurrentPassword show ->
            Input.currentPassword attributes
                { onChange = data.onChange
                , text = data.text
                , placeholder = Nothing
                , label = Input.labelHidden ""
                , show = show
                }


addId : TextInputData msg -> List (Element.Attribute msg) -> List (Element.Attribute msg)
addId data attrs =
    case data.id of
        Nothing ->
            attrs

        Just id ->
            Element.Extra.id id :: attrs


addAriaLabelledBy : TextInputData msg -> List (Element.Attribute msg) -> List (Element.Attribute msg)
addAriaLabelledBy data attrs =
    case data.label of
        LabelAbove _ ->
            ariaLabelledby (idLabel data) :: attrs


viewAffixes : TextInputData msg -> Element msg
viewAffixes data =
    case ( data.userChangedAffix, data.affixes, data.affix ) of
        ( Nothing, [ _ ], Just affix ) ->
            viewAffixSingle data affix

        ( Just userChangedAffix, affixes, Just affix ) ->
            viewAffixSelect data userChangedAffix affixes affix

        _ ->
            none


viewAffixSingle : TextInputData msg -> Affix -> Element msg
viewAffixSingle data affix =
    affix.label
        |> verbatim
        |> Ui.Theme.Typography.button
        |> el
            [ centerY
            , centerX
            ]
        |> el
            [ height fill
            , width (px 80)
            , Border.widthEach
                (if data.prefix then
                    { top = 0
                    , bottom = 0
                    , left = 0
                    , right = 1
                    }

                 else
                    { top = 0
                    , bottom = 0
                    , left = 1
                    , right = 0
                    }
                )
            , Border.roundEach
                (if data.prefix then
                    { topLeft = 4
                    , topRight = 0
                    , bottomRight = 0
                    , bottomLeft = 4
                    }

                 else
                    { topLeft = 0
                    , topRight = 4
                    , bottomRight = 4
                    , bottomLeft = 0
                    }
                )
            , Border.color outline
            , Font.color onSurface
            ]


viewAffixSelect : TextInputData msg -> (Affix -> msg) -> List Affix -> Affix -> Element msg
viewAffixSelect data userChangedAffix affixes affixSelected =
    Html.node TextInputAffixSelectCustomElement.nameNode
        [ Html.Attributes.property "elmData"
            (TextInputAffixSelectCustomElement.encode
                { id = idAffixSelect data
                , affixes = affixes
                , selected = affixSelected
                , align =
                    if data.prefix then
                        TextInputAffixSelectCustomElement.AlignLeft

                    else
                        TextInputAffixSelectCustomElement.AlignRight
                }
            )
        , TextInputAffixSelectCustomElement.onElmDataChanged userChangedAffix
        ]
        []
        |> html
        |> el [ height fill ]


idLabel : TextInputData msg -> String
idLabel data =
    idTextInput data ++ "--label"


idAffixSelect : TextInputData msg -> String
idAffixSelect data =
    idTextInput data ++ "--affix-select"


idTextInput : TextInputData msg -> String
idTextInput data =
    case data.id of
        Nothing ->
            case data.label of
                LabelAbove fluent ->
                    Fluent.toId fluent

        Just id ->
            id
