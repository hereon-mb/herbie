{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Atom.Labelled exposing
    ( bool
    , date
    , decimal
    , decimalWithAffix
    , download
    , float
    , floatWithAffix
    , int
    , intWithAffix
    , link
    , linkList
    , markdown
    , posix
    , posixWithDistance
    , string
    )

import Api.Date exposing (Date)
import Api.Decimal as Decimal exposing (Decimal)
import Context exposing (C)
import Element
    exposing
        ( Element
        , column
        , fill
        , none
        , paragraph
        , row
        , spacing
        , width
        )
import Fluent
    exposing
        ( Fluent
        , verbatim
        )
import Locale exposing (Locale)
import Time exposing (Posix)
import Time.Distance
import Ui.Theme.Markdown as Markdown
import Ui.Theme.Typography
    exposing
        ( body1
        , body1Mono
        , body1Strong
        , download1
        , link1
        )


string : Fluent -> String -> Element msg
string label value =
    value
        |> verbatim
        |> body1
        |> List.singleton
        |> withCaption label


markdown : Fluent -> String -> Element msg
markdown label value =
    value
        |> Markdown.view
        |> List.singleton
        |> withCaption label


bool :
    { label : Fluent
    , ifTrue : Fluent
    , ifFalse : Fluent
    }
    -> Bool
    -> Element msg
bool config value =
    let
        actualValue =
            if value then
                config.ifTrue

            else
                config.ifFalse
    in
    actualValue
        |> body1
        |> List.singleton
        |> withCaption config.label


int :
    { label : Fluent
    }
    -> Int
    -> Element msg
int config =
    String.fromInt
        >> verbatim
        >> body1Mono
        >> List.singleton
        >> withCaption config.label


intWithAffix : Fluent -> { affix | label : String } -> Int -> Element msg
intWithAffix label affix value =
    (String.fromInt value ++ " " ++ affix.label)
        |> verbatim
        |> body1Mono
        |> List.singleton
        |> withCaption label


float :
    { label : Fluent
    }
    -> Float
    -> Element msg
float config =
    String.fromFloat
        >> verbatim
        >> body1Mono
        >> List.singleton
        >> withCaption config.label


floatWithAffix : Fluent -> { affix | label : String } -> Float -> Element msg
floatWithAffix label affix value =
    (String.fromFloat value ++ " " ++ affix.label)
        |> verbatim
        |> body1Mono
        |> List.singleton
        |> withCaption label


decimal :
    { label : Fluent
    , locale : Locale
    }
    -> Decimal
    -> Element msg
decimal config =
    Decimal.toString config.locale
        >> verbatim
        >> body1Mono
        >> List.singleton
        >> withCaption config.label


decimalWithAffix : Locale -> Fluent -> { affix | label : String } -> Decimal -> Element msg
decimalWithAffix locale label affix value =
    (Decimal.toString locale value ++ " " ++ affix.label)
        |> verbatim
        |> body1Mono
        |> List.singleton
        |> withCaption label


date : Fluent -> Date -> Element msg
date label value =
    tr [ Fluent.posix "date" value ] "date"
        |> body1
        |> List.singleton
        |> withCaption label


posix : Fluent -> Posix -> Element msg
posix label value =
    tr [ Fluent.posix "posix" value ] "posix"
        |> body1
        |> List.singleton
        |> withCaption label


posixWithDistance : C -> Fluent -> Posix -> Element msg
posixWithDistance c label value =
    [ tr [ Fluent.posix "posix" value ] "posix"
        |> body1
    , " ("
        |> verbatim
        |> body1
    , Time.Distance.inWordsWithAffix value c.now
        |> body1
    , ")"
        |> verbatim
        |> body1
    ]
        |> withCaption label


link : Fluent -> { url : String, label : Fluent } -> Element msg
link label data =
    link1
        { url = data.url
        , label = data.label
        }
        |> List.singleton
        |> withCaption label


download : Fluent -> { url : String, label : Fluent } -> Element msg
download label data =
    download1
        { url = data.url
        , label = data.label
        }
        |> List.singleton
        |> withCaption label


linkList : Fluent -> List { url : String, label : Fluent } -> Element msg
linkList label values =
    case values of
        [] ->
            none

        [ value ] ->
            link label value

        _ ->
            let
                toLink data =
                    { url = data.url
                    , label = data.label
                    }
                        |> link1
                        |> List.singleton
                        |> paragraph [ width fill ]
            in
            [ [ body1Strong label
              , body1Strong (verbatim ": ")
              ]
                |> paragraph [ width fill ]
            , values
                |> List.map toLink
                |> column
                    [ width fill
                    , spacing 8
                    ]
            ]
                |> column
                    [ width fill
                    , spacing 8
                    ]


withCaption : Fluent -> List (Element msg) -> Element msg
withCaption label elements =
    [ ([ body1Strong label
       , body1Strong (verbatim ": ")
       ]
        ++ elements
      )
        |> paragraph [ width fill ]
    ]
        |> row
            [ width fill
            , spacing 8
            ]


tr : List Fluent.Argument -> String -> Fluent
tr args key =
    Fluent.textWith args ("ui-atom-labelled--" ++ key)
