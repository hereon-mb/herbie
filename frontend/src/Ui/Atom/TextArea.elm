{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Atom.TextArea exposing
    ( TextArea, textArea, toElement
    , withLabel
    , withId
    , onPasteGraph
    , ActionIcon, onCopy, withHoverableActionIcon, withReadonly, withSupportingText
    )

{-|

@docs TextArea, textArea, toElement
@docs withLabel
@docs withId
@docs onPasteGraph

-}

import Accessibility.Clipboard as Clipboard
import Element
    exposing
        ( Element
        , Length
        , centerY
        , clip
        , column
        , el
        , fill
        , height
        , minimum
        , padding
        , paddingEach
        , paddingXY
        , paragraph
        , row
        , scrollbarY
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Extra
    exposing
        ( ariaLabelledby
        , id
        , onScroll
        )
import Element.Font as Font
import Element.Input as Input exposing (Placeholder)
import Fluent exposing (Fluent)
import Rdf.Decode exposing (Decoder)
import Shacl.Clipboard as Clipboard
import Ui.Atom.FocusBorder as FocusBorder exposing (FocusBorder, focusBorder)
import Ui.Atom.Icon as Icon exposing (Icon)
import Ui.Theme.Color
    exposing
        ( onSurfaceVariant
        , transparent
        )
import Ui.Theme.Typography


type TextArea msg
    = TextArea (TextAreaData msg)


type alias TextAreaData msg =
    { text : String
    , withBorder : Bool
    , placeholder : Maybe Fluent
    , label : Maybe Fluent
    , help : List Fluent
    , id : Maybe String
    , height : Length
    , readonly : Bool
    , supportingText : Maybe Fluent
    , onChange : String -> msg
    , onFocus : Maybe msg
    , onLoseFocus : Maybe msg
    , onClick : Maybe msg
    , onScroll : Maybe msg
    , onPasteGraph : Maybe ( msg, Decoder msg )
    , onCopy : Maybe msg
    , actionIconHoverable : Maybe (ActionIcon msg)
    }


textArea : (String -> msg) -> String -> TextArea msg
textArea onChange text =
    TextArea
        { text = text
        , withBorder = True
        , placeholder = Nothing
        , label = Nothing
        , help = []
        , id = Nothing
        , height = fill
        , readonly = False
        , supportingText = Nothing
        , onChange = onChange
        , onFocus = Nothing
        , onLoseFocus = Nothing
        , onClick = Nothing
        , onScroll = Nothing
        , onPasteGraph = Nothing
        , onCopy = Nothing
        , actionIconHoverable = Nothing
        }


withLabel : Fluent -> TextArea msg -> TextArea msg
withLabel label (TextArea data) =
    TextArea { data | label = Just label }


withId : String -> TextArea msg -> TextArea msg
withId id (TextArea data) =
    TextArea { data | id = Just id }


withReadonly : Bool -> TextArea msg -> TextArea msg
withReadonly readonly (TextArea data) =
    TextArea { data | readonly = readonly }


type alias ActionIcon msg =
    { icon : Icon
    , onClick : msg
    }


withHoverableActionIcon : ActionIcon msg -> TextArea msg -> TextArea msg
withHoverableActionIcon actionIcon (TextArea data) =
    TextArea { data | actionIconHoverable = Just actionIcon }


withSupportingText : Fluent -> TextArea msg -> TextArea msg
withSupportingText supportingText (TextArea data) =
    TextArea { data | supportingText = Just supportingText }



---- TO ELEMENT


toElement : TextArea msg -> Element msg
toElement (TextArea data) =
    if data.readonly then
        viewReadonly data

    else
        viewInput data


viewReadonly : TextAreaData msg -> Element msg
viewReadonly data =
    let
        attributes =
            [ width (minimum 32 fill)
            , height fill
            , Background.color transparent
            , Font.size 18
            , paddingXY 16 19
            ]
                |> addId
                |> addAriaLabelledBy

        addId attrs =
            case data.id of
                Nothing ->
                    attrs

                Just id ->
                    Element.Extra.id id :: attrs

        addAriaLabelledBy attrs =
            case data.label of
                Just _ ->
                    ariaLabelledby (idLabel data) :: attrs

                Nothing ->
                    attrs

        addWithHoverableActionIcon =
            case data.actionIconHoverable of
                Nothing ->
                    identity

                Just actionIcon ->
                    FocusBorder.withHoverableActionIcon actionIcon.icon actionIcon.onClick
    in
    [ data.text
        |> Element.text
        |> List.singleton
        |> paragraph attributes
    , Icon.EditOff
        |> Icon.viewNormal
        |> el
            [ paddingEach
                { top = 0
                , bottom = 0
                , left = 0
                , right =
                    if List.isEmpty data.help then
                        16

                    else
                        48
                }
            , centerY
            ]
    ]
        |> row
            [ width fill
            , height fill
            ]
        |> focusBorder
        |> FocusBorder.withHelp data.help
        |> FocusBorder.withHeightFill
        |> focusBorderWithLabel data
        |> addWithSupportingText data
        |> addWithHoverableActionIcon
        |> focusBorderEmpty data
        |> FocusBorder.toElement


viewInput : TextAreaData msg -> Element msg
viewInput data =
    let
        addOnScroll attrs =
            case data.onScroll of
                Nothing ->
                    attrs

                Just msg ->
                    onScroll msg :: attrs

        addOnFocus attrs =
            case data.onFocus of
                Nothing ->
                    attrs

                Just onFocus ->
                    Events.onFocus onFocus :: attrs

        addOnLoseFocus attrs =
            case data.onLoseFocus of
                Nothing ->
                    attrs

                Just onLoseFocus ->
                    Events.onLoseFocus onLoseFocus :: attrs

        addOnClick attrs =
            case data.onClick of
                Nothing ->
                    attrs

                Just onClick ->
                    Events.onClick onClick :: attrs

        addOnPaste attrs =
            case data.onPasteGraph of
                Nothing ->
                    attrs

                Just ( msgIgnored, decoderMsg ) ->
                    Clipboard.onPasteGraph msgIgnored decoderMsg :: attrs

        addOnCopy attrs =
            case data.onCopy of
                Nothing ->
                    attrs

                Just msg ->
                    Clipboard.onCopy msg :: attrs

        addBorder =
            if data.withBorder then
                -- FIXME wrap in a column with fill on height and width as well
                -- as clip, so the scrollbarY is rendered correctly
                List.singleton
                    >> column
                        [ width fill
                        , height fill
                        , padding 2
                        , clip
                        ]
                    >> focusBorder
                    >> FocusBorder.withHelp data.help
                    >> FocusBorder.withHeightFill
                    >> focusBorderWithLabel data
                    >> addWithSupportingText data
                    >> addWithHoverableActionIcon
                    >> focusBorderEmpty data
                    >> FocusBorder.toElement

            else
                identity

        addWithHoverableActionIcon =
            case data.actionIconHoverable of
                Nothing ->
                    identity

                Just actionIcon ->
                    FocusBorder.withHoverableActionIcon actionIcon.icon actionIcon.onClick
    in
    { onChange = data.onChange
    , text = data.text
    , placeholder = Maybe.map viewPlaceholder data.placeholder
    , label = Input.labelHidden ""
    , spellcheck = False
    }
        |> Input.multiline
            ([ Font.size 18
             , width fill
             , height fill
             , if data.withBorder then
                padding 14

               else
                padding 16
             , Border.width 0
             , Border.rounded 4
             , id (idTextArea data)
             , ariaLabelledby (idLabel data)
             , Font.family [ Font.typeface "Source Code Pro" ]
             , Background.color transparent
             ]
                |> addOnFocus
                |> addOnLoseFocus
                |> addOnClick
                |> addOnPaste
                |> addOnCopy
            )
        |> el
            ([ width fill
             , height fill
             , scrollbarY
             ]
                |> addOnScroll
            )
        |> addBorder


addWithSupportingText : TextAreaData msg -> FocusBorder msg -> FocusBorder msg
addWithSupportingText data =
    case data.supportingText of
        Nothing ->
            identity

        Just supportingText ->
            FocusBorder.withSupportingText supportingText


focusBorderWithLabel : TextAreaData msg -> FocusBorder msg -> FocusBorder msg
focusBorderWithLabel data =
    case data.label of
        Nothing ->
            identity

        Just label ->
            FocusBorder.withLabel
                { id = idLabel data
                , text = label
                }


focusBorderEmpty : TextAreaData msg -> FocusBorder msg -> FocusBorder msg
focusBorderEmpty data =
    if data.text == "" then
        FocusBorder.empty

    else
        identity


viewPlaceholder : Fluent -> Placeholder msg
viewPlaceholder placeholder =
    placeholder
        |> Ui.Theme.Typography.body1
        |> Input.placeholder [ Font.color onSurfaceVariant ]


idLabel : TextAreaData msg -> String
idLabel data =
    idTextArea data ++ "--label"


idTextArea : TextAreaData msg -> String
idTextArea data =
    case data.id of
        Nothing ->
            case data.label of
                Nothing ->
                    -- FIXME this is probably bad, as this will result in
                    -- several text inputs with the same id
                    ""

                Just fluent ->
                    Fluent.toId fluent

        Just id ->
            id


onPasteGraph : msg -> Decoder msg -> TextArea msg -> TextArea msg
onPasteGraph msgIgnored decoderMsg (TextArea data) =
    TextArea { data | onPasteGraph = Just ( msgIgnored, decoderMsg ) }


onCopy : msg -> TextArea msg -> TextArea msg
onCopy msg (TextArea data) =
    TextArea { data | onCopy = Just msg }
