{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Atom.Button exposing
    ( Button, Format(..), button, buttonLink, view
    , withFormat, withLeadingIcon, withFollowingIcon, withId
    , withColor
    , withWidth
    , withEnabled
    , narrow
    , withAttributes
    )

{-|

@docs Button, Format, button, buttonLink, view
@docs withFormat, withLeadingIcon, withFollowingIcon, withId
@docs withColor
@docs withWidth
@docs withEnabled
@docs narrow

-}

import Accessibility.Key
import Element
    exposing
        ( Attribute
        , Color
        , Element
        , Length
        , centerX
        , centerY
        , el
        , focused
        , height
        , link
        , mouseDown
        , mouseOver
        , paddingEach
        , paddingXY
        , px
        , row
        , shrink
        , spacing
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Extra
    exposing
        ( disabled
        , id
        , preventDefaultAndStopPropagationOnClick
        , preventDefaultOnKeyDown
        )
import Element.Font as Font
import Element.Input as Input
import Fluent exposing (Fluent)
import Ui.Atom.Icon as Icon exposing (Icon)
import Ui.Theme.Color as Color
    exposing
        ( onPrimary
        , onSurface
        , primary
        , transparent
        , withOpacity
        )
import Ui.Theme.Elevation as Elevation
import Ui.Theme.Typography as Typography


type Button msg
    = Button
        { onPress : OnPress msg
        , label : Fluent
        , format : Format
        , id : Maybe String
        , icon : Maybe Icon
        , iconFollowing : Maybe Icon
        , color : Color
        , width : Length
        , narrow : Bool
        , enabled : Bool
        , attrs : List (Attribute msg)
        }


type OnPress msg
    = Msg msg
    | Href String


type Format
    = Contained
    | Outlined
    | Text


button : msg -> Fluent -> Button msg
button onPress text =
    Button
        { onPress = Msg onPress
        , label = text
        , format = Contained
        , id = Nothing
        , icon = Nothing
        , iconFollowing = Nothing
        , color = primary
        , width = shrink
        , narrow = False
        , enabled = True
        , attrs = []
        }


buttonLink : String -> Fluent -> Button msg
buttonLink href text =
    Button
        { onPress = Href href
        , label = text
        , format = Contained
        , id = Nothing
        , icon = Nothing
        , iconFollowing = Nothing
        , color = primary
        , width = shrink
        , narrow = False
        , enabled = True
        , attrs = []
        }


withFormat : Format -> Button msg -> Button msg
withFormat format (Button data) =
    Button { data | format = format }


withId : String -> Button msg -> Button msg
withId id (Button data) =
    Button { data | id = Just id }


withLeadingIcon : Icon -> Button msg -> Button msg
withLeadingIcon icon (Button data) =
    Button { data | icon = Just icon }


withFollowingIcon : Icon -> Button msg -> Button msg
withFollowingIcon icon (Button data) =
    Button { data | iconFollowing = Just icon }


withColor : Color -> Button msg -> Button msg
withColor color (Button data) =
    Button { data | color = color }


withWidth : Length -> Button msg -> Button msg
withWidth length (Button data) =
    Button { data | width = length }


withEnabled : Bool -> Button msg -> Button msg
withEnabled enabled (Button data) =
    Button { data | enabled = enabled }


withAttributes : List (Element.Attribute msg) -> Button msg -> Button msg
withAttributes attrs (Button data) =
    Button { data | attrs = attrs }


narrow : Button msg -> Button msg
narrow (Button data) =
    Button { data | narrow = True }



---- VIEW


view : Button msg -> Element msg
view (Button data) =
    let
        attrs =
            case data.id of
                Nothing ->
                    attributes data.enabled data.onPress data.format data.color data.width
                        ++ data.attrs

                Just theId ->
                    (id theId :: attributes data.enabled data.onPress data.format data.color data.width)
                        ++ data.attrs
    in
    case data.onPress of
        Msg _ ->
            Input.button attrs
                { onPress = Nothing
                , label = label data.narrow data.icon data.iconFollowing data.label
                }

        Href href ->
            link attrs
                { url = href
                , label = label data.narrow data.icon data.iconFollowing data.label
                }


attributes : Bool -> OnPress msg -> Format -> Color -> Length -> List (Attribute msg)
attributes enabled onPress format color theWidth =
    let
        withOnClick attrs =
            if enabled then
                case onPress of
                    Msg msg ->
                        preventDefaultAndStopPropagationOnClick msg
                            :: preventDefaultOnKeyDown
                                [ Accessibility.Key.enter ( msg, True )
                                , Accessibility.Key.space ( msg, True )
                                ]
                            :: disabled False
                            :: attrs

                    Href _ ->
                        attrs

            else
                attrs

        withElevation attrs =
            if enabled then
                attrs
                    ++ [ Elevation.z2
                       , Elevation.z4Hover
                       , Elevation.z4Focus
                       , Elevation.z8Pressed
                       ]

            else
                attrs
    in
    case format of
        Contained ->
            [ height (px 40)
            , width theWidth
            , Border.rounded 20
            , Font.color onPrimary
            , Background.color
                (if enabled then
                    color

                 else
                    onSurface
                        |> withOpacity 0.12
                )
            , mouseDown
                (if enabled then
                    [ Background.color (Color.pressed color) ]

                 else
                    []
                )
            , mouseOver
                (if enabled then
                    [ Background.color (Color.hovered color) ]

                 else
                    []
                )
            , focused
                (if enabled then
                    [ Background.color (Color.focused color) ]

                 else
                    []
                )
            ]
                |> withElevation
                |> withOnClick

        Outlined ->
            [ height (px 40)
            , width theWidth
            , Border.rounded 20
            , Font.color
                (if enabled then
                    color

                 else
                    onSurface
                        |> withOpacity 0.38
                )
            , Border.width 1
            , Border.color
                (if enabled then
                    color

                 else
                    onSurface
                        |> withOpacity 0.12
                )
            , Background.color transparent
            , mouseDown
                (if enabled then
                    [ Font.color (Color.pressed color)
                    , Border.color (Color.pressed color)
                    , Background.color (Color.overlayPressed color)
                    ]

                 else
                    []
                )
            , mouseOver
                (if enabled then
                    [ Font.color (Color.hovered color)
                    , Border.color (Color.hovered color)
                    , Background.color (Color.overlayHovered color)
                    ]

                 else
                    []
                )
            , focused
                (if enabled then
                    [ Font.color (Color.focused color)
                    , Border.color (Color.focused color)
                    , Background.color (Color.overlayFocused color)
                    ]

                 else
                    []
                )
            ]
                |> withOnClick

        Text ->
            [ height (px 40)
            , width theWidth
            , Border.rounded 20
            , Font.color
                (if enabled then
                    color

                 else
                    onSurface
                        |> withOpacity 0.38
                )
            , Background.color transparent
            , mouseDown
                (if enabled then
                    [ Font.color (Color.pressed color)
                    , Background.color (Color.overlayPressed color)
                    ]

                 else
                    []
                )
            , mouseOver
                (if enabled then
                    [ Font.color (Color.hovered color)
                    , Background.color (Color.overlayHovered color)
                    ]

                 else
                    []
                )
            , focused
                (if enabled then
                    [ Font.color (Color.focused color)
                    , Background.color (Color.overlayFocused color)
                    ]

                 else
                    []
                )
            ]
                |> withOnClick


label : Bool -> Maybe Icon -> Maybe Icon -> Fluent -> Element msg
label isNarrow maybeIcon maybeIconFollowing text =
    case ( maybeIcon, maybeIconFollowing ) of
        ( Nothing, Nothing ) ->
            el
                [ centerY
                , centerX
                , if isNarrow then
                    paddingXY 8 0

                  else
                    paddingXY 24 0
                ]
                (Typography.button text)

        ( Just icon, Nothing ) ->
            row
                [ centerY
                , centerX
                , if isNarrow then
                    paddingEach
                        { top = 0
                        , bottom = 0
                        , left = 4
                        , right = 8
                        }

                  else
                    paddingEach
                        { top = 0
                        , bottom = 0
                        , left = 16
                        , right = 24
                        }
                , spacing 8
                ]
                [ Icon.viewSmall icon
                , el [ centerY ] (Typography.button text)
                ]

        ( Nothing, Just iconFollowing ) ->
            row
                [ centerY
                , centerX
                , if isNarrow then
                    paddingEach
                        { top = 0
                        , bottom = 0
                        , left = 12
                        , right = 4
                        }

                  else
                    paddingEach
                        { top = 0
                        , bottom = 0
                        , left = 24
                        , right = 16
                        }
                , spacing 8
                ]
                [ el [ centerY ] (Typography.button text)
                , Icon.viewSmall iconFollowing
                ]

        ( Just icon, Just iconFollowing ) ->
            row
                [ centerY
                , centerX
                , if isNarrow then
                    paddingEach
                        { top = 0
                        , bottom = 0
                        , left = 4
                        , right = 8
                        }

                  else
                    paddingEach
                        { top = 0
                        , bottom = 0
                        , left = 16
                        , right = 16
                        }
                , spacing 8
                ]
                [ Icon.viewSmall icon
                , el [ centerY ] (Typography.button text)
                , Icon.viewSmall iconFollowing
                ]
