{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Atom.ChipAssist exposing
    ( ChipAssist, chipAssist, toElement
    , withIcon
    , elevated
    , withEnabled
    )

{-| Implementation of the [Assist
chip](https://m3.material.io/components/chips/guidelines#8d453d50-8d8e-43aa-9ae3-87ed134d2e64)
component.

@docs ChipAssist, chipAssist, toElement
@docs withIcon
@docs elevated
@docs withEnabled

-}

import Element exposing (Element)
import Fluent exposing (Fluent)
import Ui.Atom.Chip as Chip
import Ui.Atom.Icon exposing (Icon)


type ChipAssist msg
    = ChipAssist (Config msg)


type alias Config msg =
    { icon : Maybe Icon
    , elevated : Bool
    , label : Fluent
    , onPress : msg
    , enabled : Bool
    }


chipAssist : msg -> Fluent -> ChipAssist msg
chipAssist onPress label =
    ChipAssist
        { icon = Nothing
        , elevated = False
        , label = label
        , onPress = onPress
        , enabled = True
        }


withEnabled : Bool -> ChipAssist msg -> ChipAssist msg
withEnabled enabled (ChipAssist data) =
    ChipAssist { data | enabled = enabled }


withIcon : Icon -> ChipAssist msg -> ChipAssist msg
withIcon icon (ChipAssist data) =
    ChipAssist { data | icon = Just icon }


elevated : ChipAssist msg -> ChipAssist msg
elevated (ChipAssist data) =
    ChipAssist { data | elevated = True }


toElement : ChipAssist msg -> Element msg
toElement (ChipAssist data) =
    view data


view : Config msg -> Element msg
view config =
    Chip.view
        { iconLeft = config.icon
        , label = config.label
        , iconRight = Nothing
        , narrow = False
        , elevated = config.elevated
        , elevatedOnHover = False
        , primary = False
        , onPress =
            if config.enabled then
                Just config.onPress

            else
                Nothing
        , onPressRight = Nothing
        , connectedOnLeft = False
        , connectedOnRight = False
        }
