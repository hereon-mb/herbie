{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Atom.Chip exposing (Config, view)

import Accessibility.Key
import Element
    exposing
        ( Attribute
        , Element
        , centerY
        , el
        , focused
        , height
        , mouseDown
        , mouseOver
        , padding
        , paddingEach
        , paddingXY
        , px
        , row
        )
import Element.Background as Background
import Element.Border as Border
import Element.Extra
    exposing
        ( disabled
        , preventDefaultOnKeyDown
        , stopPropagationOnClick
        )
import Element.Font as Font
import Element.Input as Input
import Fluent exposing (Fluent)
import Ui.Atom.Icon as Icon exposing (Icon)
import Ui.Theme.Color
    exposing
        ( focusedWith
        , hoveredWith
        , onSecondaryContainer
        , onSurface
        , outline
        , pressedWith
        , secondaryContainer
        , transparent
        , withOpacity
        )
import Ui.Theme.Elevation as Elevation
import Ui.Theme.Typography
    exposing
        ( button
        )


type alias Config msg =
    { iconLeft : Maybe Icon
    , label : Fluent
    , iconRight : Maybe Icon
    , narrow : Bool
    , elevated : Bool
    , elevatedOnHover : Bool
    , primary : Bool
    , onPress : Maybe msg
    , onPressRight : Maybe msg
    , connectedOnLeft : Bool
    , connectedOnRight : Bool
    }


view : Config msg -> Element msg
view config =
    case config.onPress of
        Nothing ->
            el
                ([ attributesDimensions
                 , attributesBorder config
                 , attributesBackground config
                 , attributesElevation config
                 , attributesFont config
                 ]
                    |> List.concat
                )
                (viewLabel config)

        Just _ ->
            Input.button
                ([ attributesDimensions
                 , attributesBorder config
                 , attributesBackground config
                 , attributesElevation config
                 , attributesOnPress config
                 , attributesFont config
                 ]
                    |> List.concat
                )
                { onPress = Nothing
                , label = viewLabel config
                }


attributesDimensions : List (Attribute msg)
attributesDimensions =
    [ height (px 32)
    ]


attributesOnPress : Config msg -> List (Attribute msg)
attributesOnPress config =
    case config.onPress of
        Nothing ->
            []

        Just onPress ->
            [ stopPropagationOnClick onPress
            , preventDefaultOnKeyDown
                [ Accessibility.Key.enter ( onPress, True )
                , Accessibility.Key.space ( onPress, True )
                ]
            , disabled False
            ]


attributesBorder : Config msg -> List (Attribute msg)
attributesBorder config =
    [ Border.width 1
    , if config.elevated || config.primary then
        Border.color transparent

      else if config.onPress /= Nothing || config.onPressRight /= Nothing then
        Border.color outline

      else
        Border.color onSurface
    , if config.connectedOnLeft && config.connectedOnRight then
        Border.rounded 0

      else if config.connectedOnLeft then
        Border.roundEach
            { topLeft = 0
            , topRight = 8
            , bottomRight = 8
            , bottomLeft = 0
            }

      else if config.connectedOnRight then
        Border.roundEach
            { topLeft = 8
            , topRight = 0
            , bottomRight = 0
            , bottomLeft = 8
            }

      else
        Border.rounded 8
    ]


attributesBackground : Config msg -> List (Attribute msg)
attributesBackground config =
    case ( config.onPress /= Nothing, config.primary ) of
        ( True, True ) ->
            [ Background.color secondaryContainer
            , mouseDown [ Background.color (pressedWith onSecondaryContainer secondaryContainer) ]
            , mouseOver [ Background.color (hoveredWith onSecondaryContainer secondaryContainer) ]
            , focused [ Background.color (focusedWith onSecondaryContainer secondaryContainer) ]
            ]

        ( True, False ) ->
            [ Background.color transparent
            , mouseDown [ Background.color (pressedWith onSecondaryContainer transparent) ]
            , mouseOver [ Background.color (hoveredWith onSecondaryContainer transparent) ]
            , focused [ Background.color (focusedWith onSecondaryContainer transparent) ]
            ]

        ( False, True ) ->
            [ onSurface
                |> withOpacity 0.12
                |> Background.color
            ]

        ( False, False ) ->
            if config.elevated then
                [ onSurface
                    |> withOpacity 0.12
                    |> Background.color
                ]

            else
                []


attributesFont : Config msg -> List (Attribute msg)
attributesFont config =
    if config.onPress /= Nothing || config.onPressRight /= Nothing then
        []

    else
        [ Font.color onSurface ]


attributesElevation : Config msg -> List (Attribute msg)
attributesElevation config =
    if config.onPress /= Nothing || config.onPressRight /= Nothing then
        if config.elevated then
            [ Elevation.z1
            , Elevation.z2Hover
            ]

        else if config.elevatedOnHover then
            [ Elevation.z1Hover
            ]

        else
            []

    else
        []


viewLabel : Config msg -> Element msg
viewLabel config =
    case ( config.iconLeft, config.iconRight ) of
        ( Nothing, Nothing ) ->
            config.label
                |> button
                |> el
                    [ centerY
                    , if config.connectedOnLeft && config.connectedOnRight then
                        paddingXY paddingSmall 0

                      else if config.connectedOnLeft then
                        paddingEach
                            { top = 0
                            , bottom = 0
                            , left = paddingSmall
                            , right = paddingBig config
                            }

                      else if config.connectedOnRight then
                        paddingEach
                            { top = 0
                            , bottom = 0
                            , left = paddingBig config
                            , right = paddingSmall
                            }

                      else
                        paddingXY (paddingBig config) 0
                    ]

        ( Just iconLeft, Nothing ) ->
            [ iconLeft
                |> Icon.viewSmall
                |> el [ paddingXY paddingSmall 0 ]
            , config.label
                |> button
            ]
                |> row
                    [ centerY
                    , if config.connectedOnRight then
                        paddingEach
                            { top = 0
                            , bottom = 0
                            , left = 0
                            , right = paddingSmall
                            }

                      else
                        paddingEach
                            { top = 0
                            , bottom = 0
                            , left = 0
                            , right = paddingBig config
                            }
                    ]

        ( Nothing, Just iconRight ) ->
            [ config.label
                |> button
            , viewIconRight config iconRight
            ]
                |> row
                    [ centerY
                    , if config.connectedOnLeft then
                        paddingEach
                            { top = 0
                            , bottom = 0
                            , left = paddingSmall
                            , right = 0
                            }

                      else
                        paddingEach
                            { top = 0
                            , bottom = 0
                            , left = paddingBig config
                            , right = 0
                            }
                    ]

        ( Just iconLeft, Just iconRight ) ->
            [ iconLeft
                |> Icon.viewSmall
                |> el [ paddingXY paddingSmall 0 ]
            , config.label
                |> button
            , viewIconRight config iconRight
            ]
                |> row [ centerY ]


viewIconRight : Config msg -> Icon -> Element msg
viewIconRight config iconRight =
    case config.onPressRight of
        Nothing ->
            iconRight
                |> Icon.viewSmall
                |> el [ paddingXY paddingSmall 0 ]

        Just onPressRight ->
            Input.button
                [ stopPropagationOnClick onPressRight
                , preventDefaultOnKeyDown
                    [ Accessibility.Key.enter ( onPressRight, True )
                    , Accessibility.Key.space ( onPressRight, True )
                    ]
                , disabled False
                , padding 4
                , Border.rounded 13
                , mouseDown [ Background.color (pressedWith onSurface transparent) ]
                , mouseOver [ Background.color (hoveredWith onSurface transparent) ]
                , focused [ Background.color (focusedWith onSurface transparent) ]
                ]
                { onPress = Nothing
                , label =
                    iconRight
                        |> Icon.viewSmall
                }
                |> el [ paddingXY (paddingSmall - 4) 0 ]


paddingSmall : Int
paddingSmall =
    8


paddingBig : Config msg -> Int
paddingBig config =
    if config.narrow then
        12

    else
        16
