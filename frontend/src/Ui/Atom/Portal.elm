{- This code was taken from https://github.com/mdgriffith/elm-prefab and
   adapted.  The original code is licensed as follows:

   BSD 3-Clause License

   Copyright (c) 2023, Matthew Griffith

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this
      list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.

   3. Neither the name of the copyright holder nor the names of its
      contributors may be used to endorse or promote products derived from
      this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
   DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
   FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
   SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
   CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
   OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-}


module Ui.Atom.Portal exposing
    ( Viewport, viewportDecoder
    , target
    , view
    )

{-|

@docs Viewport, viewportDecoder
@docs target
@docs view

-}

import Element
    exposing
        ( Element
        , fill
        , focusStyle
        , height
        , html
        , noStaticStyleSheet
        , width
        )
import Element.Font as Font
import Html
import Html.Attributes
import Json.Decode as Decode exposing (Decoder)


{-| -}
type Viewport
    = Viewport DataViewport


type alias DataViewport =
    { parent : Parent
    , window : Window
    }


type alias Parent =
    { boundingClientRect : BoundingClientRect
    , offsetTop : Float
    , offsetLeft : Float
    }


type alias BoundingClientRect =
    { x : Float
    , y : Float
    , width : Float
    , height : Float
    }


type alias Window =
    { width : Float
    , height : Float
    }


{-| Decode the viewport from an event.
-}
viewportDecoder : Decoder Viewport
viewportDecoder =
    Decode.map Viewport
        (Decode.at [ "currentTarget", "__getPortalViewport" ]
            (Decode.map2 DataViewport
                (Decode.field "parent" parentDecoder)
                (Decode.field "window" windowDecoder)
            )
        )


parentDecoder : Decoder Parent
parentDecoder =
    Decode.map3 Parent
        (Decode.field "boundingClientRect"
            (Decode.map4 BoundingClientRect
                (Decode.field "x" Decode.float)
                (Decode.field "y" Decode.float)
                (Decode.field "width" Decode.float)
                (Decode.field "height" Decode.float)
            )
        )
        (Decode.field "offsetTop" Decode.float)
        (Decode.field "offsetLeft" Decode.float)


windowDecoder : Decoder Window
windowDecoder =
    Decode.map2 Window
        (Decode.field "width" Decode.float)
        (Decode.field "height" Decode.float)


{-| Put this at the bottom of the parent which is scrollable and closest to the
element inside the portal.
-}
target : Element msg
target =
    html (Html.div [ Html.Attributes.id "elm-portal-target" ] [])


{-| View the provided element below the given viewport.
-}
view : Viewport -> Element msg -> Element msg
view (Viewport viewport) element =
    html
        (Html.node "elm-portal"
            []
            [ Html.div
                [ Html.Attributes.style "position" "absolute"
                , Html.Attributes.style "top" "0"
                , Html.Attributes.style "bottom" "0"
                , Html.Attributes.style "left" "0"
                , Html.Attributes.style "right" "0"
                , Html.Attributes.style "pointer-events" "none"
                ]
                [ Html.div
                    [ Html.Attributes.style "position" "relative"
                    , Html.Attributes.style "top" (pxString viewport.parent.offsetTop)
                    , Html.Attributes.style "left" (pxString viewport.parent.offsetLeft)
                    , Html.Attributes.style "width" (pxString viewport.parent.boundingClientRect.width)
                    , Html.Attributes.style "height" (pxString viewport.parent.boundingClientRect.height)
                    ]
                    [ Html.div
                        [ Html.Attributes.style "position" "absolute"
                        , Html.Attributes.style "top" "100%"
                        , Html.Attributes.style "left" "0"
                        , Html.Attributes.style "transform" "none"
                        , Html.Attributes.style "right" "0"
                        , Html.Attributes.style "pointer-events" "auto"
                        ]
                        [ Element.layoutWith
                            { options =
                                [ noStaticStyleSheet
                                , focusStyle
                                    { borderColor = Nothing
                                    , backgroundColor = Nothing
                                    , shadow = Nothing
                                    }
                                ]
                            }
                            [ height fill
                            , width fill
                            , Font.family
                                [ Font.typeface "Work Sans"
                                , Font.sansSerif
                                ]
                            ]
                            element
                        ]
                    ]
                ]
            ]
        )


pxString : Float -> String
pxString float =
    String.fromFloat float ++ "px"
