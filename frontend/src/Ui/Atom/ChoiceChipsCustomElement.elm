{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Atom.ChoiceChipsCustomElement exposing
    ( Chip
    , External
    , Label
    , Model
    , Msg
    , encode
    , labelAbove
    , main
    , nameNode
    )

import Browser
import Browser.Dom
import Element
    exposing
        ( Element
        , alignTop
        , centerY
        , column
        , el
        , fill
        , focusStyle
        , focused
        , height
        , htmlAttribute
        , mouseDown
        , mouseOver
        , moveUp
        , noStaticStyleSheet
        , none
        , paddingEach
        , paddingXY
        , pointer
        , px
        , row
        , spacing
        , width
        , wrappedRow
        )
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Extra as Element
    exposing
        ( ariaChecked
        , ariaLabelledby
        , id
        , role
        , tabindex
        )
import Element.Font as Font
import Fluent
    exposing
        ( Fluent
        )
import Html exposing (Html)
import Html.Attributes
import Html.Events
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Decode
import Json.Encode as Encode exposing (Value)
import Json.Encode.Extra as Encode
import List.Extra
import Ports.CustomElement as Ports
import Task
import Ui.Atom.ButtonIcon as ButtonIcon
import Ui.Atom.Icon as Icon
import Ui.Theme.Color
    exposing
        ( focusedWith
        , hoveredWith
        , onSecondaryContainer
        , onSurfaceVariant
        , outline
        , pressedWith
        , secondaryContainer
        , transparent
        )
import Ui.Theme.Elevation as Elevation
import Ui.Theme.Input
import Ui.Theme.Typography


main : Program Value Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }


type Model
    = Running (External String)
    | Failed


type alias External option =
    { id : String
    , label : Label
    , help : List Fluent
    , chips : List (Chip option)
    , selected : Maybe option
    , onCopy : Bool
    }


type Label
    = LabelAbove Fluent
    | LabelledBy String


labelAbove : Fluent -> Label
labelAbove =
    LabelAbove


type alias Chip option =
    { option : option
    , label : Fluent
    }


init : Value -> ( Model, Cmd Msg )
init flags =
    case Decode.decodeValue decoder flags of
        Err _ ->
            ( Failed
            , Cmd.none
            )

        Ok external ->
            ( Running external
            , Cmd.none
            )


type Msg
    = NoOp
    | ElmDataChanged Value
    | UserClickedChip Int
    | UserPressedSpace Int
    | UserPressedArrowUp
    | UserPressedArrowDown
    | UserPressedArrowLeft
    | UserPressedArrowRight
    | UserCopied


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case model of
        Running external ->
            updateHelp msg external

        Failed ->
            ( model, Cmd.none )


updateHelp : Msg -> External String -> ( Model, Cmd Msg )
updateHelp msg external =
    case msg of
        NoOp ->
            ( Running external, Cmd.none )

        ElmDataChanged value ->
            case Decode.decodeValue decoder value of
                Err _ ->
                    ( Running external, Cmd.none )

                Ok newExternal ->
                    ( Running newExternal
                    , Cmd.none
                    )

        UserClickedChip index ->
            ( Running external, select external index )

        UserPressedSpace index ->
            ( Running external, select external index )

        UserPressedArrowUp ->
            ( Running external
            , external
                |> indexPrevious
                |> select external
            )

        UserPressedArrowDown ->
            ( Running external
            , external
                |> indexNext
                |> select external
            )

        UserPressedArrowLeft ->
            ( Running external
            , external
                |> indexPrevious
                |> select external
            )

        UserPressedArrowRight ->
            ( Running external
            , external
                |> indexNext
                |> select external
            )

        UserCopied ->
            ( Running external
            , Ports.copy ()
            )


indexNext : External String -> Int
indexNext external =
    let
        chipsCount =
            List.length external.chips
    in
    case
        external.selected
            |> Maybe.andThen
                (\selected ->
                    List.Extra.findIndex (\{ option } -> option == selected)
                        external.chips
                )
    of
        Nothing ->
            0

        Just index ->
            if index == chipsCount - 1 then
                0

            else
                index + 1


indexPrevious : External String -> Int
indexPrevious external =
    let
        chipsCount =
            List.length external.chips
    in
    case
        external.selected
            |> Maybe.andThen
                (\selected ->
                    List.Extra.findIndex (\{ option } -> option == selected)
                        external.chips
                )
    of
        Nothing ->
            chipsCount - 1

        Just index ->
            if index == 0 then
                chipsCount - 1

            else
                index - 1


select : External String -> Int -> Cmd Msg
select external index =
    case List.Extra.getAt index external.chips of
        Nothing ->
            Cmd.none

        Just chip ->
            Cmd.batch
                [ chip.option
                    |> Encode.string
                    |> Ports.changeElmData
                , Task.attempt (\_ -> NoOp) (Browser.Dom.focus (idOption external.id index))
                ]


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        Running _ ->
            Ports.elmDataChanged ElmDataChanged

        Failed ->
            Sub.none


view : Model -> Html Msg
view model =
    case model of
        Running external ->
            Html.node nameNode
                [ Html.Attributes.style "display" "contents" ]
                [ Element.layoutWith
                    { options =
                        [ noStaticStyleSheet
                        , focusStyle
                            { borderColor = Nothing
                            , backgroundColor = Nothing
                            , shadow = Nothing
                            }
                        ]
                    }
                    [ width fill
                    , height fill
                    , Font.family
                        [ Font.typeface "Work Sans"
                        , Font.sansSerif
                        ]
                    ]
                    (viewHelp external)
                ]

        Failed ->
            Html.node nameNode
                [ Html.Attributes.style "display" "contents" ]
                [ Html.text "Could not initialize custom element" ]


viewHelp : External String -> Element Msg
viewHelp config =
    let
        idLabel =
            case config.label of
                LabelledBy id ->
                    id

                _ ->
                    config.id ++ "-label"

        withHelp el =
            case config.label of
                LabelAbove _ ->
                    el

                _ ->
                    Ui.Theme.Input.helpAbove
                        { input = el
                        , help = config.help
                        }

        buttonCopy =
            if not config.onCopy then
                none

            else
                Icon.ContentCopy
                    |> ButtonIcon.buttonIcon UserCopied
                    |> ButtonIcon.withAttributes
                        [ Element.class "ui-atom-choice-chips__button-copy"
                        ]
                    |> ButtonIcon.excludeFromTabSequence
                    |> ButtonIcon.toElement
                    |> el
                        [ alignTop
                        , paddingXY 8 0
                        , moveUp 4
                        ]
    in
    column
        [ width fill
        , role "radiogroup"
        , ariaLabelledby idLabel
        , spacing 8
        , paddingEach
            { top = 0
            , bottom = 8
            , left = 0
            , right = 0
            }
        , tabindex -1
        , id config.id
        , Element.class "ui-atom-choice-chips"
        ]
        [ case config.label of
            LabelAbove label ->
                Ui.Theme.Input.label
                    { id = Just idLabel
                    , text = label
                    , help = config.help
                    }

            _ ->
                none
        , row
            [ width fill
            ]
            [ wrappedRow
                [ width fill
                , spacing 8
                , paddingXY 4 0
                ]
                (List.indexedMap (viewChip config) config.chips)
            , buttonCopy
            ]
        ]
        |> withHelp


viewChip : External String -> Int -> Chip String -> Element Msg
viewChip external index chip =
    let
        checked =
            Just chip.option == external.selected

        theTabindex =
            case external.selected of
                Nothing ->
                    if index == 0 then
                        0

                    else
                        -1

                Just selected ->
                    if selected == chip.option then
                        0

                    else
                        -1

        addBackground attrs =
            if checked then
                Background.color secondaryContainer
                    :: mouseDown [ Background.color (pressedWith onSecondaryContainer secondaryContainer) ]
                    :: mouseOver [ Background.color (hoveredWith onSecondaryContainer secondaryContainer) ]
                    :: focused [ Background.color (focusedWith onSecondaryContainer secondaryContainer) ]
                    :: attrs

            else
                Background.color transparent
                    :: mouseDown [ Background.color (pressedWith onSurfaceVariant transparent) ]
                    :: mouseOver [ Background.color (hoveredWith onSurfaceVariant transparent) ]
                    :: focused [ Background.color (focusedWith onSurfaceVariant transparent) ]
                    :: attrs

        addFont attrs =
            if checked then
                Font.color onSecondaryContainer :: attrs

            else
                Font.color onSurfaceVariant :: attrs

        addBorder attrs =
            if checked then
                Border.color secondaryContainer :: attrs

            else
                Border.color outline :: attrs

        addElevation attrs =
            if checked then
                Elevation.z1Hover :: attrs

            else
                attrs

        handleKey code =
            case code of
                "Space" ->
                    if checked then
                        Decode.fail "not handling that key here"

                    else
                        Decode.succeed ( UserPressedSpace index, True )

                "ArrowUp" ->
                    Decode.succeed ( UserPressedArrowUp, True )

                "ArrowDown" ->
                    Decode.succeed ( UserPressedArrowDown, True )

                "ArrowLeft" ->
                    Decode.succeed ( UserPressedArrowLeft, True )

                "ArrowRight" ->
                    Decode.succeed ( UserPressedArrowRight, True )

                _ ->
                    Decode.fail "not handling that key here"
    in
    chip.label
        |> Ui.Theme.Typography.body2
        |> el [ centerY ]
        |> el
            ([ paddingXY 12 0
             , height (px 32)
             , Border.rounded 16
             , Border.width 1
             , pointer
             , role "radio"
             , ariaChecked checked
             , tabindex theTabindex
             , htmlAttribute
                (Html.Events.preventDefaultOn "keydown"
                    (Decode.andThen handleKey (Decode.field "code" Decode.string))
                )
             , Events.onClick (UserClickedChip index)
             , id (idOption external.id index)
             ]
                |> addBackground
                |> addFont
                |> addBorder
                |> addElevation
            )


idOption : String -> Int -> String
idOption id index =
    id ++ "-option-" ++ String.fromInt index


decoder : Decoder (External String)
decoder =
    Decode.succeed External
        |> Decode.required "id" Decode.string
        |> Decode.required "label" labelDecoder
        |> Decode.required "help" (Decode.list Fluent.decoder)
        |> Decode.required "chips" (Decode.list chipDecoder)
        |> Decode.required "selected" (Decode.maybe Decode.string)
        |> Decode.required "onCopy" Decode.bool


labelDecoder : Decoder Label
labelDecoder =
    let
        help kind =
            case kind of
                "label-above" ->
                    Decode.succeed LabelAbove
                        |> Decode.required "fluent" Fluent.decoder

                "labelled-by" ->
                    Decode.succeed LabelledBy
                        |> Decode.required "id" Decode.string

                _ ->
                    Decode.fail ("unknown kind: '" ++ kind ++ "'")
    in
    Decode.andThen help (Decode.field "kind" Decode.string)


chipDecoder : Decoder (Chip String)
chipDecoder =
    Decode.succeed Chip
        |> Decode.required "option" Decode.string
        |> Decode.required "label" Fluent.decoder


encode : (option -> String) -> External option -> Value
encode optionToString external =
    Encode.object
        [ ( "id", Encode.string external.id )
        , ( "label", encodeLabel external.label )
        , ( "help", Encode.list Fluent.encode external.help )
        , ( "chips", Encode.list (encodeChip optionToString) external.chips )
        , ( "selected", Encode.maybe (optionToString >> Encode.string) external.selected )
        , ( "onCopy", Encode.bool external.onCopy )
        ]


encodeLabel : Label -> Value
encodeLabel label =
    case label of
        LabelAbove fluent ->
            Encode.object
                [ ( "kind", Encode.string "label-above" )
                , ( "fluent", Fluent.encode fluent )
                ]

        LabelledBy id ->
            Encode.object
                [ ( "kind", Encode.string "labelled-by" )
                , ( "id", Encode.string id )
                ]


encodeChip : (option -> String) -> Chip option -> Value
encodeChip optionToString chip =
    Encode.object
        [ ( "option", Encode.string (optionToString chip.option) )
        , ( "label", Fluent.encode chip.label )
        ]


nameNode : String
nameNode =
    "ui-atom-choice-chips"
