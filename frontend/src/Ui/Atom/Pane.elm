module Ui.Atom.Pane exposing
    ( Pane
    , pane
    , withSecondary, withSecondaryOnLeft
    , withBottom, maximize
    , loadingSecondary
    , toElement
    )

{-|

@docs Pane
@docs pane
@docs withSecondary, withSecondaryOnLeft
@docs withBottom, maximize
@docs loadingSecondary
@docs toElement

-}

import Context exposing (C)
import Element
    exposing
        ( Attribute
        , Element
        , alignBottom
        , alignTop
        , centerX
        , column
        , el
        , fill
        , fillPortion
        , height
        , maximum
        , none
        , padding
        , paddingEach
        , row
        , scrollbarY
        , spacing
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Extra
    exposing
        ( class
        , elWithAncestorWithScrollbarX
        , loadingAnimationContainerLow
        , pointerEvents
        , style
        , userSelect
        )
import Element.Keyed as Keyed
import Element.Lazy as Lazy
import Ui.Atom.Portal as Portal
import Ui.Theme.Color as Color
    exposing
        ( primarySurfaceContainerHighest
        , surface
        , surfaceContainerLow
        )
import Ui.Theme.Dimensions exposing (paneWidthMax)
import Ui.Theme.Elevation as Elevation


type Pane msg
    = Pane (Data msg)


type alias Data msg =
    { primary : Element msg
    , secondary : Maybe (Element msg)
    , secondaryOnLeft : Bool
    , secondaryLoading : Bool
    , bottom : Maybe (Element msg)
    , maximize : Bool
    }


pane : Element msg -> Pane msg
pane primary =
    Pane
        { primary = primary
        , secondary = Nothing
        , secondaryOnLeft = False
        , secondaryLoading = False
        , bottom = Nothing
        , maximize = False
        }


withSecondary : Element msg -> Pane msg -> Pane msg
withSecondary secondary (Pane data) =
    Pane { data | secondary = Just secondary }


withSecondaryOnLeft : Element msg -> Pane msg -> Pane msg
withSecondaryOnLeft secondary (Pane data) =
    Pane
        { data
            | secondary = Just secondary
            , secondaryOnLeft = True
        }


withBottom : Element msg -> Pane msg -> Pane msg
withBottom bottom (Pane data) =
    Pane { data | bottom = Just bottom }


maximize : Pane msg -> Pane msg
maximize (Pane data) =
    Pane { data | maximize = True }


loadingSecondary : Bool -> Pane msg -> Pane msg
loadingSecondary secondaryLoading (Pane data) =
    Pane { data | secondaryLoading = secondaryLoading }


toElement : C -> Pane msg -> Element msg
toElement c (Pane data) =
    column
        [ width fill
        , height fill
        , style "max-height" (String.fromInt (c.viewport.height - 48 - 4) ++ "px")
        ]
        [ Keyed.column
            [ width fill
            , height fill
            , scrollbarY
            , style "flex-basis" "unset"
            , style "min-height" "unset"
            , class "ui-atom-pane--container"
            ]
            [ ( "viewPanes"
              , viewPanes data
                    (if data.secondaryOnLeft then
                        [ viewSecondary c data
                        , viewPrimary data
                        ]

                     else
                        [ viewPrimary data
                        , viewSecondary c data
                        ]
                    )
              )
            , ( "viewBottom"
              , viewBottom data
              )
            , ( "Portal.target"
              , Lazy.lazy (\_ -> Portal.target) ()
              )
            ]
        ]


viewPrimary : Data msg -> Element msg
viewPrimary data =
    data.primary
        |> elWithAncestorWithScrollbarX
            [ widthPrimary data
            , height fill
            ]


viewSecondary : C -> Data msg -> Element msg
viewSecondary c data =
    case data.secondary of
        Nothing ->
            none

        Just secondary ->
            if data.secondaryLoading then
                el
                    [ widthSecondary
                    , height fill
                    , loadingAnimationContainerLow
                    , Border.rounded 12
                    , Border.width 1
                    , Border.color Color.outline
                    , Background.color surfaceContainerLow
                    ]
                    none

            else
                secondary
                    |> el
                        [ widthSecondary
                        , alignTop
                        , Border.rounded 12
                        , Border.width 1
                        , Border.color Color.outline
                        , Background.color surfaceContainerLow
                        , pointerEvents True
                        , userSelect True
                        , style "position" "sticky"
                        , style "top" "16px"
                        , style "bottom" "104px"
                        , if data.bottom == Nothing then
                            style "max-height" (String.fromInt (c.viewport.height - 64 - 16 - 4) ++ "px")

                          else
                            style "max-height" (String.fromInt (c.viewport.height - 64 - 16 - 88 - 4) ++ "px")
                        ]


viewPanes : Data msg -> List (Element msg) -> Element msg
viewPanes data =
    row
        [ widthEverything data
        , height fill
        , centerX
        , padding 16
        , spacing 16
        ]


viewBottom : Data msg -> Element msg
viewBottom data =
    case data.bottom of
        Nothing ->
            none

        Just bottom ->
            bottom
                |> el
                    [ width fill
                    , padding 16
                    , Border.rounded 12
                    , Background.color primarySurfaceContainerHighest
                    , Elevation.z6
                    , pointerEvents True
                    , userSelect True
                    ]
                |> el
                    [ width fill
                    , Background.color surface
                    , Border.roundEach
                        { topLeft = 12
                        , topRight = 12
                        , bottomLeft = 0
                        , bottomRight = 0
                        }
                    , paddingEach
                        { top = 0
                        , bottom = 16
                        , left = 0
                        , right = 0
                        }
                    ]
                |> el
                    [ widthEverything data
                    , alignBottom
                    , centerX
                    , paddingEach
                        { top = 0
                        , bottom = 0
                        , left = 16
                        , right = 16
                        }
                    ]
                |> el
                    [ width fill
                    , class "ui-atom-pane--bottom"
                    ]


widthEverything : Data msg -> Attribute msg
widthEverything data =
    if data.maximize then
        width fill

    else if data.secondary == Nothing then
        width (maximum 900 fill)

    else
        width (maximum (900 + 16 + 600) fill)


widthPrimary : Data msg -> Attribute msg
widthPrimary data =
    width
        (if data.maximize then
            fill

         else
            maximum paneWidthMax (fillPortion 5)
        )


widthSecondary : Attribute msg
widthSecondary =
    width (maximum 600 (fillPortion 3))
