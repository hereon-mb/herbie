{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Atom.Icon exposing
    ( Icon(..)
    , viewNormal, viewSmall, viewTiny, viewLarge
    )

{-|

@docs Icon
@docs viewNormal, viewSmall, viewTiny, viewLarge

-}

import Element
    exposing
        ( Element
        , el
        , height
        , html
        , px
        , width
        )
import Material.Icons
import Material.Icons.Outlined
import Material.Icons.Types exposing (Coloring(..))
import Svg exposing (Svg)


{-| -}
type Icon
    = Add
    | AdminPanelSettings
    | ArrowBack
    | ArrowDownward
    | ArrowDropDown
    | ArrowDropUp
    | ArrowCircleUp
    | ArrowUpward
    | Article
    | Assignment
    | AssignmentLate
    | AssignmentTurnedIn
    | AttachFile
    | Autorenew
    | Badge
    | Biotech
    | Book
    | Build
    | CameraAlt
    | CancelOutlined
    | Create
    | Clear
    | Check
    | CheckBox
    | CheckBoxOutlineBlank
    | CheckCircleOutline
    | ChevronLeft
    | ChevronRight
    | Close
    | ContentCopy
    | Delete
    | Description
    | DesktopWindows
    | DriveFolderUpload
    | Done
    | Edit
    | EditNote
    | EditOff
    | Email
    | Error
    | ExpandLess
    | ExpandMore
    | FileDownload
    | FileOpen
    | FileUpload
    | FilterAlt
    | FirstPage
    | Folder
    | Fullscreen
    | FullscreenExit
    | Groups
    | GroupWork
    | Help
    | HelpCenter
    | IndeterminateCheckBox
    | Interests
    | Inventory
    | Inventory2
    | LastPage
    | Link
    | Logout
    | Menu
    | MenuOpen
    | MoreVert
    | People
    | RadioButtonChecked
    | RadioButtonUnchecked
    | Recommend
    | Refresh
    | RemoveCircleOutline
    | Room
    | SavedSearch
    | ScatterPlot
    | Science
    | Search
    | Settings
    | Storage
    | Timeline
    | Today
    | TravelExplore
    | GeneratingTokens
    | Workspaces


toMaterialIcon : Int -> Icon -> Svg msg
toMaterialIcon size icon =
    case icon of
        Add ->
            Material.Icons.add size Inherit

        AdminPanelSettings ->
            Material.Icons.admin_panel_settings size Inherit

        ArrowBack ->
            Material.Icons.arrow_back size Inherit

        ArrowDownward ->
            Material.Icons.arrow_downward size Inherit

        ArrowDropDown ->
            Material.Icons.arrow_drop_down size Inherit

        ArrowDropUp ->
            Material.Icons.arrow_drop_up size Inherit

        ArrowCircleUp ->
            Material.Icons.arrow_circle_up size Inherit

        ArrowUpward ->
            Material.Icons.arrow_upward size Inherit

        Article ->
            Material.Icons.article size Inherit

        Assignment ->
            Material.Icons.assignment size Inherit

        AssignmentLate ->
            Material.Icons.assignment_late size Inherit

        AssignmentTurnedIn ->
            Material.Icons.assignment_turned_in size Inherit

        AttachFile ->
            Material.Icons.attach_file size Inherit

        Autorenew ->
            Material.Icons.autorenew size Inherit

        Badge ->
            Material.Icons.badge size Inherit

        Biotech ->
            Material.Icons.biotech size Inherit

        Build ->
            Material.Icons.build size Inherit

        Book ->
            Material.Icons.book size Inherit

        CameraAlt ->
            Material.Icons.camera_alt size Inherit

        CancelOutlined ->
            Material.Icons.Outlined.cancel size Inherit

        Clear ->
            Material.Icons.clear size Inherit

        Check ->
            Material.Icons.check size Inherit

        CheckBox ->
            Material.Icons.check_box size Inherit

        CheckBoxOutlineBlank ->
            Material.Icons.check_box_outline_blank size Inherit

        CheckCircleOutline ->
            Material.Icons.check_circle_outline size Inherit

        ChevronLeft ->
            Material.Icons.chevron_left size Inherit

        ChevronRight ->
            Material.Icons.chevron_right size Inherit

        Close ->
            Material.Icons.close size Inherit

        ContentCopy ->
            Material.Icons.content_copy size Inherit

        Delete ->
            Material.Icons.delete size Inherit

        Create ->
            Material.Icons.create size Inherit

        Description ->
            Material.Icons.description size Inherit

        DesktopWindows ->
            Material.Icons.desktop_windows size Inherit

        DriveFolderUpload ->
            Material.Icons.drive_folder_upload size Inherit

        Done ->
            Material.Icons.done size Inherit

        Edit ->
            Material.Icons.edit size Inherit

        EditNote ->
            Material.Icons.edit_note size Inherit

        EditOff ->
            Material.Icons.edit_off size Inherit

        Email ->
            Material.Icons.email size Inherit

        Error ->
            Material.Icons.error size Inherit

        ExpandLess ->
            Material.Icons.expand_less size Inherit

        ExpandMore ->
            Material.Icons.expand_more size Inherit

        FileDownload ->
            Material.Icons.file_download size Inherit

        FileOpen ->
            Material.Icons.file_open size Inherit

        FileUpload ->
            Material.Icons.file_upload size Inherit

        FilterAlt ->
            Material.Icons.filter_alt size Inherit

        FirstPage ->
            Material.Icons.first_page size Inherit

        Folder ->
            Material.Icons.folder size Inherit

        Fullscreen ->
            Material.Icons.fullscreen size Inherit

        FullscreenExit ->
            Material.Icons.fullscreen_exit size Inherit

        Groups ->
            Material.Icons.groups size Inherit

        GroupWork ->
            Material.Icons.group_work size Inherit

        Help ->
            Material.Icons.help size Inherit

        HelpCenter ->
            Material.Icons.help_center size Inherit

        IndeterminateCheckBox ->
            Material.Icons.indeterminate_check_box size Inherit

        Interests ->
            Material.Icons.interests size Inherit

        Inventory ->
            Material.Icons.inventory size Inherit

        Inventory2 ->
            Material.Icons.inventory_2 size Inherit

        LastPage ->
            Material.Icons.last_page size Inherit

        Link ->
            Material.Icons.link size Inherit

        Logout ->
            Material.Icons.logout size Inherit

        Menu ->
            Material.Icons.menu size Inherit

        MenuOpen ->
            Material.Icons.menu_open size Inherit

        MoreVert ->
            Material.Icons.more_vert size Inherit

        People ->
            Material.Icons.people size Inherit

        RadioButtonChecked ->
            Material.Icons.radio_button_checked size Inherit

        RadioButtonUnchecked ->
            Material.Icons.radio_button_unchecked size Inherit

        Recommend ->
            Material.Icons.recommend size Inherit

        Refresh ->
            Material.Icons.refresh size Inherit

        RemoveCircleOutline ->
            Material.Icons.remove_circle_outline size Inherit

        Room ->
            Material.Icons.room size Inherit

        SavedSearch ->
            Material.Icons.saved_search size Inherit

        ScatterPlot ->
            Material.Icons.scatter_plot size Inherit

        Science ->
            Material.Icons.science size Inherit

        Search ->
            Material.Icons.search size Inherit

        Settings ->
            Material.Icons.settings size Inherit

        Storage ->
            Material.Icons.storage size Inherit

        Timeline ->
            Material.Icons.timeline size Inherit

        Today ->
            Material.Icons.today size Inherit

        TravelExplore ->
            Material.Icons.travel_explore size Inherit

        GeneratingTokens ->
            Material.Icons.generating_tokens size Inherit

        Workspaces ->
            Material.Icons.workspaces size Inherit


{-| -}
viewTiny : Icon -> Element msg
viewTiny icon =
    el
        [ height (px 14)
        , width (px 14)
        ]
        (html (toMaterialIcon 14 icon))


{-| -}
viewSmall : Icon -> Element msg
viewSmall icon =
    el
        [ height (px 18)
        , width (px 18)
        ]
        (html (toMaterialIcon 18 icon))


{-| -}
viewNormal : Icon -> Element msg
viewNormal icon =
    el
        [ width (px 24)
        , height (px 24)
        ]
        (html (toMaterialIcon 24 icon))


{-| -}
viewLarge : Icon -> Element msg
viewLarge icon =
    el
        [ width (px 32)
        , height (px 32)
        ]
        (html (toMaterialIcon 32 icon))
