{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Atom.ChipSuggestion exposing
    ( ChipSuggestion, chipSuggestion, toElement
    , withIcon, primary
    , elevated
    , withEnabled
    )

{-| Implementation of the [Suggestion
chip](https://m3.material.io/components/chips/guidelines#36d7bb16-a9bf-4cf6-a73d-8e05510d66a7)
component.

@docs ChipSuggestion, chipSuggestion, toElement
@docs withIcon, primary
@docs elevated
@docs withEnabled

-}

import Element exposing (Element)
import Fluent exposing (Fluent)
import Ui.Atom.Chip as Chip
import Ui.Atom.Icon exposing (Icon)


type ChipSuggestion msg
    = ChipSuggestion (Config msg)


type alias Config msg =
    { icon : Maybe Icon
    , primary : Bool
    , elevated : Bool
    , label : Fluent
    , onPress : msg
    , enabled : Bool
    }


chipSuggestion : msg -> Fluent -> ChipSuggestion msg
chipSuggestion onPress label =
    ChipSuggestion
        { icon = Nothing
        , primary = False
        , elevated = False
        , label = label
        , onPress = onPress
        , enabled = True
        }


withEnabled : Bool -> ChipSuggestion msg -> ChipSuggestion msg
withEnabled enabled (ChipSuggestion data) =
    ChipSuggestion { data | enabled = enabled }


withIcon : Icon -> ChipSuggestion msg -> ChipSuggestion msg
withIcon icon (ChipSuggestion data) =
    ChipSuggestion { data | icon = Just icon }


primary : ChipSuggestion msg -> ChipSuggestion msg
primary (ChipSuggestion data) =
    ChipSuggestion { data | primary = True }


elevated : ChipSuggestion msg -> ChipSuggestion msg
elevated (ChipSuggestion data) =
    ChipSuggestion { data | elevated = True }


toElement : ChipSuggestion msg -> Element msg
toElement (ChipSuggestion data) =
    view data


view : Config msg -> Element msg
view config =
    Chip.view
        { iconLeft = config.icon
        , label = config.label
        , iconRight = Nothing
        , narrow = False
        , elevated = config.elevated
        , elevatedOnHover = False
        , primary = config.primary
        , onPress =
            if config.enabled then
                Just config.onPress

            else
                Nothing
        , onPressRight = Nothing
        , connectedOnLeft = False
        , connectedOnRight = False
        }
