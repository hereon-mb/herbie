module Ui.Atom.ProgressIndicatorLinear exposing
    ( State(..)
    , view
    )

import Element
    exposing
        ( Element
        , clipX
        , el
        , fill
        , height
        , none
        , px
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Extra
    exposing
        ( class
        )
import Ui.Theme.Color exposing (primary, primaryContainer)


type State
    = Indeterminate


view : State -> Element msg
view state =
    case state of
        Indeterminate ->
            el
                [ width fill
                , height (px 4)
                , Border.rounded 2
                , Background.color primaryContainer
                , class "progress-indicator-linear"
                , clipX
                ]
                (el
                    [ width fill
                    , class "indeterminate"
                    , Background.color primary
                    ]
                    none
                )
