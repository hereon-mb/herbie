{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Atom.SandboxBorder exposing (view)

import Element
    exposing
        ( Element
        , centerX
        , column
        , fill
        , height
        , padding
        , paragraph
        , spacing
        , width
        )
import Element.Background as Background
import Element.Extra
    exposing
        ( elWithAncestorWithScrollbarY
        )
import Element.Font as Font
import Fluent exposing (Fluent)
import Ui.Theme.Color
    exposing
        ( errorContainer
        )
import Ui.Theme.Typography
    exposing
        ( body1
        )


view : Element msg -> Element msg
view content =
    [ t "info"
        |> body1
        |> List.singleton
        |> paragraph
            [ centerX
            , Font.center
            ]
    , content
        |> elWithAncestorWithScrollbarY
            [ width fill
            , height fill
            ]
    ]
        |> column
            [ width fill
            , height fill
            , Background.color errorContainer
            , padding 16
            , spacing 16
            ]


t : String -> Fluent
t key =
    Fluent.text ("ui-atom-sandbox-border--" ++ key)
