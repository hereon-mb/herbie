// Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
// a client-server based web application for research documentation
//
// Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
//
// Authors: Fabian Kirchner, Catriona Eschke
//
// This program is subject to the terms and conditions for non-commercial use of
// ELN. You can find the license text in the file LICENSE.en and under
// http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
// you have any questions or comments, you can contact us at hereon at
// herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
// Max-Planck-Straße 1, 21502 Geesthacht, Germany.

export function registerCustomElementUiAtomSegmentedButtonsSingleSelect(Elm) {
  customElements.define("ui-atom-segmented-buttons-single-select", class extends HTMLElement {
    constructor() {
      super();
      this._elmData = null;
      this._app = null;
    }

    adoptedCallback() {
    }

    connectedCallback() {
      const that = this;
      this._app = Elm.Ui.Atom.SegmentedButtonsSingleSelectCustomElement.init({
        node: this,
        flags: this.elmData || this._elmData,
      });
      this._app.ports.changeElmData.subscribe(function(value) {
        const event = new CustomEvent("elm-data-changed", { detail: value });
        that.dispatchEvent(event);
      });
    }

    disconnectedCallback() {
    }

    set elmData(value) {
      this._elmData = value;
      if (this._app) {
        this._app.ports.elmDataChanged.send(value);
      }
    }

    get elmData() {
      return this._elmData
    }
  });
}
