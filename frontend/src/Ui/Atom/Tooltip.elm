{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Atom.Tooltip exposing
    ( Tooltip, init, Msg, update
    , add, Placement(..)
    , Config
    )

{-|

@docs Tooltip, init, Msg, update
@docs add, Placement

-}

import Element
    exposing
        ( Element
        , above
        , centerX
        , el
        , moveUp
        , padding
        , transparent
        )
import Element.Background as Background
import Element.Border as Border
import Element.Events
    exposing
        ( onMouseEnter
        )
import Element.Extra
    exposing
        ( class
        )
import Element.Font as Font
import Fluent exposing (Fluent)
import Ui.Theme.Color
    exposing
        ( inverseOnSurface
        , inverseSurface
        )
import Ui.Theme.Typography
    exposing
        ( body2
        )


type Tooltip
    = NoTooltip
    | Tooltip TooltipData


type alias TooltipData =
    { id : String
    }


type Placement
    = Above


init : Tooltip
init =
    NoTooltip


type alias Config msg =
    { label : Fluent
    , placement : Placement
    , tooltip : Tooltip
    , tooltipMsg : Msg -> msg
    , id : String
    }


add : Config msg -> Element msg -> Element msg
add config element =
    el
        [ above (viewContent config)
        , class "tooltip--container"
        , onMouseEnter (config.tooltipMsg (UserEnteredElement config.id))
        ]
        element


viewContent : Config msg -> Element msg
viewContent config =
    let
        userEnteredOtherTooltip =
            case config.tooltip of
                NoTooltip ->
                    False

                Tooltip data ->
                    data.id /= config.id
    in
    body2 config.label
        |> el
            [ padding 4
            , Border.rounded 4
            , Background.color inverseSurface
            , Font.color inverseOnSurface
            , transparent userEnteredOtherTooltip
            ]
        |> el
            [ centerX
            , moveUp 4
            , class "tooltip--content"
            ]


type Msg
    = UserEnteredElement String


update : Msg -> Tooltip -> Tooltip
update msg _ =
    case msg of
        UserEnteredElement id ->
            Tooltip { id = id }
