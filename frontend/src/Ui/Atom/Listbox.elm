{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Atom.Listbox exposing
    ( Listbox, Instance
    , init
    , Msg, UpdateConfig, update
    , activatePreviousOrFirstOption, activateNextOrFirstOption
    , focus
    , subscriptions
    , ViewConfig, view
    , Label, labelledBy
    , preventDefaultOnKeyDown
    )

{-|

@docs Listbox, Instance
@docs init
@docs Msg, UpdateConfig, update
@docs activatePreviousOrFirstOption, activateNextOrFirstOption
@docs focus
@docs subscriptions
@docs ViewConfig, view
@docs Label, labelledBy
@docs preventDefaultOnKeyDown

-}

import Browser.Dom as Dom
import Element as Ui exposing (Element)
import Element.Background as Background
import Element.Events as Events
import Element.Extra as Ui
import Json.Decode exposing (Decoder)
import Listbox as External
import Task exposing (Task)


{-| -}
type alias Listbox =
    External.Listbox


{-| -}
type alias Instance option msg =
    External.Instance option msg


{-| -}
init : Listbox
init =
    External.init


{-| -}
type alias Msg option =
    External.Msg option


{-| -}
type alias UpdateConfig option =
    { uniqueId : option -> String }


{-| -}
update :
    UpdateConfig option
    -> List option
    -> Msg option
    -> Listbox
    -> List option
    -> ( Listbox, Cmd (Msg option), List option )
update updateConfig options msg listbox selection =
    External.update
        (listboxUpdateConfig updateConfig)
        options
        msg
        listbox
        selection


listboxUpdateConfig :
    UpdateConfig a
    ->
        { uniqueId : a -> String
        , behaviour : External.Behaviour a
        }
listboxUpdateConfig config =
    { uniqueId = config.uniqueId
    , behaviour =
        { jumpAtEnds = True
        , separateFocus = True
        , handleHomeAndEnd = True
        , typeAhead = External.noTypeAhead
        , minimalGap = 0
        , initialGap = 0
        }
    }


{-| -}
activatePreviousOrFirstOption :
    UpdateConfig option
    -> List option
    -> Listbox
    -> List option
    -> ( Listbox, List option )
activatePreviousOrFirstOption updateConfig =
    External.activatePreviousOrFirstOption (listboxUpdateConfig updateConfig)


{-| -}
activateNextOrFirstOption :
    UpdateConfig option
    -> List option
    -> Listbox
    -> List option
    -> ( Listbox, List option )
activateNextOrFirstOption updateConfig =
    External.activateNextOrFirstOption (listboxUpdateConfig updateConfig)


{-| -}
focus : Instance option msg -> Task Dom.Error ()
focus =
    External.focus


{-| -}
subscriptions : Listbox -> Sub (Msg option)
subscriptions listbox =
    External.subscriptions listbox


{-| -}
type alias ViewConfig option =
    { uniqueId : option -> String
    , focusable : Bool
    , viewOption : Bool -> option -> Element Never
    , heightMaximum : Int
    , attrsListbox : List (Ui.Attribute Never)
    }


{-| -}
view :
    ViewConfig option
    -> Instance option msg
    -> List option
    -> Listbox
    -> List option
    -> Element msg
view viewConfig instance options listbox selection =
    External.view
        (views viewConfig)
        (listboxViewConfig viewConfig)
        instance
        options
        listbox
        selection


views : ViewConfig option -> External.Views option (Element msg) msg
views config =
    External.custom
        { listbox =
            \attributes { options } ->
                let
                    withAriaLabelledby attrs =
                        case attributes.ariaLabelledby of
                            Nothing ->
                                attrs

                            Just string ->
                                Ui.ariaLabelledby string :: attrs

                    withAriaLabel attrs =
                        case attributes.ariaLabel of
                            Nothing ->
                                attrs

                            Just string ->
                                Ui.ariaLabel string :: attrs

                    withAriaActivedescendant attrs =
                        case attributes.ariaActivedescendant of
                            Nothing ->
                                attrs

                            Just string ->
                                Ui.ariaActivedescendant string :: attrs

                    withTabindex attrs =
                        case attributes.tabindex of
                            Nothing ->
                                attrs

                            Just int ->
                                Ui.tabindex int :: attrs
                in
                Ui.column
                    (([ Ui.id attributes.id
                      , Ui.role attributes.role
                      , Ui.ariaMultiselectable attributes.ariaMultiselectable
                      , Ui.preventDefaultOnKeyDown [ attributes.preventDefaultOnKeydown ]
                      , Ui.onMouseDown attributes.onMousedown
                      , Ui.onMouseUp attributes.onMouseup
                      , Ui.onFocus attributes.onFocus
                      , Ui.onBlur attributes.onBlur
                      , Ui.width Ui.fill
                      , Ui.height (Ui.maximum config.heightMaximum Ui.shrink)
                      , Ui.scrollbarY
                      ]
                        ++ List.map (Ui.mapAttribute never) config.attrsListbox
                     )
                        |> withAriaLabelledby
                        |> withAriaLabel
                        |> withAriaActivedescendant
                        |> withTabindex
                    )
                    options
        , option =
            \attributes { focused, selected } option ->
                let
                    liAttributes =
                        [ Ui.id attributes.id
                        , Ui.role attributes.role
                        , Ui.ariaCheckedRaw attributes.ariaChecked
                        , Events.onMouseEnter attributes.onMouseenter
                        , Events.onMouseLeave attributes.onMouseleave
                        , Events.onClick attributes.onClick
                        , Ui.width Ui.fill
                        , Ui.paddingXY 16 8
                        , Ui.spacing 8
                        , Ui.pointer
                        ]
                in
                Ui.el
                    (if focused then
                        Background.color (Ui.rgba255 0 0 0 0.12)
                            :: liAttributes

                     else
                        Ui.inFront
                            (Ui.el
                                [ Ui.height Ui.fill
                                , Ui.width Ui.fill
                                , Ui.transparent True
                                , Ui.mouseOver
                                    [ Ui.transparent False
                                    , Background.color (Ui.rgba255 0 0 0 0.04)
                                    ]
                                ]
                                Ui.none
                            )
                            :: liAttributes
                    )
                    -- XXX Never needed?
                    (config.viewOption selected option
                        |> Ui.map never
                    )
        }


listboxViewConfig :
    ViewConfig option
    ->
        { uniqueId : option -> String
        , focusable : Bool
        , markActiveDescendant : Bool
        }
listboxViewConfig config =
    { uniqueId = config.uniqueId
    , focusable = config.focusable
    , markActiveDescendant = True
    }


{-| -}
type alias Label =
    External.Label


{-| -}
labelledBy : String -> Label
labelledBy =
    External.labelledBy


{-| -}
preventDefaultOnKeyDown : Instance option msg -> Decoder ( msg, Bool ) -> Decoder ( msg, Bool )
preventDefaultOnKeyDown =
    External.preventDefaultOnKeyDown
