{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Atom.SegmentedButtonsSingleSelect exposing
    ( segmentedButtonsSingleSelect
    , Button
    )

{-|

@docs segmentedButtonsSingleSelect
@docs Button

-}

import Element
    exposing
        ( Element
        , el
        , fill
        , html
        , width
        )
import Fluent exposing (Fluent)
import Html exposing (Html)
import Html.Attributes
import Html.Events
import Json.Decode as Decode
import Pointer
import Ui.Atom.SegmentedButtonsSingleSelectCustomElement as CustomElement


segmentedButtonsSingleSelect :
    { id : Pointer.Pointer
    , help : List Fluent
    , buttons : List (Button option)
    , selected : option
    , onChange : option -> msg
    , toString : option -> String
    , fromString : String -> Maybe option
    }
    -> Element msg
segmentedButtonsSingleSelect config =
    el
        [ width fill ]
        (html (segmentedButtonsHelp config))


segmentedButtonsHelp :
    { id : Pointer.Pointer
    , help : List Fluent
    , buttons : List (Button option)
    , selected : option
    , onChange : option -> msg
    , toString : option -> String
    , fromString : String -> Maybe option
    }
    -> Html msg
segmentedButtonsHelp config =
    Html.node CustomElement.nameNode
        [ Html.Attributes.property "elmData"
            (CustomElement.encode config.toString
                { id = Pointer.toString config.id
                , help = config.help
                , buttons = config.buttons
                , selected = config.selected
                }
            )
        , Html.Events.on "elm-data-changed"
            (Decode.string
                |> Decode.field "detail"
                |> Decode.andThen
                    (\raw ->
                        case config.fromString raw of
                            Nothing ->
                                Decode.fail ("not an option:" ++ raw)

                            Just option ->
                                Decode.succeed option
                    )
                |> Decode.map config.onChange
            )
        ]
        []


type alias Button option =
    CustomElement.Button option
