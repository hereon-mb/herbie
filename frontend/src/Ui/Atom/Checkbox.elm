{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Atom.Checkbox exposing
    ( Checkbox
    , checkbox
    , onCopy
    , onPasteGraph
    , toElement
    , withId
    , withSupportingText
    )

import Accessibility.Clipboard as Clipboard
import Element
    exposing
        ( Element
        , alignTop
        , column
        , el
        , fill
        , none
        , paddingEach
        , paddingXY
        , paragraph
        , row
        , spacing
        , width
        )
import Element.Extra as Element
    exposing
        ( id
        )
import Element.Font as Font
import Element.Input as Input
import Fluent exposing (Fluent)
import Pointer
import Rdf.Decode exposing (Decoder)
import Shacl.Clipboard as Clipboard
import Ui.Atom.ButtonIcon as ButtonIcon
import Ui.Atom.Icon as Icon
import Ui.Theme.Color
    exposing
        ( onSurfaceVariant
        )
import Ui.Theme.Input as Input
import Ui.Theme.Typography
    exposing
        ( body1
        , body2
        )


type Checkbox msg
    = Checkbox (Data msg)


type alias Data msg =
    { onChange : Bool -> msg
    , onCopy : Maybe msg
    , onPasteGraph : Maybe ( msg, Decoder msg )
    , checked : Bool
    , label : Fluent
    , supportingText : Maybe Fluent
    , id : Maybe Pointer.Pointer
    , whenChecked : Maybe (Element msg)
    , whenUnchecked : Maybe (Element msg)
    , elementOnRight : Maybe (Element msg)
    }


checkbox : (Bool -> msg) -> Fluent -> Bool -> Checkbox msg
checkbox onChange label checked =
    Checkbox
        { onChange = onChange
        , onCopy = Nothing
        , onPasteGraph = Nothing
        , checked = checked
        , label = label
        , supportingText = Nothing
        , id = Nothing
        , whenChecked = Nothing
        , whenUnchecked = Nothing
        , elementOnRight = Nothing
        }


withId : Pointer.Pointer -> Checkbox msg -> Checkbox msg
withId id (Checkbox data) =
    Checkbox { data | id = Just id }


onCopy : msg -> Checkbox msg -> Checkbox msg
onCopy msg (Checkbox data) =
    Checkbox { data | onCopy = Just msg }


onPasteGraph : msg -> Decoder msg -> Checkbox msg -> Checkbox msg
onPasteGraph msgIgnored decoderMsg (Checkbox data) =
    Checkbox { data | onPasteGraph = Just ( msgIgnored, decoderMsg ) }


withSupportingText : Fluent -> Checkbox msg -> Checkbox msg
withSupportingText supportingText (Checkbox data) =
    Checkbox { data | supportingText = Just supportingText }


toElement : Checkbox msg -> Element msg
toElement (Checkbox data) =
    let
        buttonCopy =
            case data.onCopy of
                Nothing ->
                    none

                Just msg ->
                    Icon.ContentCopy
                        |> ButtonIcon.buttonIcon msg
                        |> ButtonIcon.withAttributes
                            [ Element.class "ui-atom-checkbox__button-copy"
                            ]
                        |> ButtonIcon.excludeFromTabSequence
                        |> ButtonIcon.toElement
                        |> el
                            [ alignTop
                            , paddingXY 8 0
                            ]
    in
    [ case data.elementOnRight of
        Nothing ->
            [ viewCheckbox data
            , buttonCopy
            ]
                |> row [ width fill ]

        Just elementOnRight ->
            [ viewCheckbox data
            , buttonCopy
            , elementOnRight
            ]
                |> row [ width fill ]
    , case data.whenChecked of
        Nothing ->
            none

        Just whenChecked ->
            if data.checked then
                el
                    [ width fill
                    , paddingEach
                        { top = 0
                        , bottom = 0
                        , left = 56
                        , right = 0
                        }
                    ]
                    whenChecked

            else
                none
    , case data.whenUnchecked of
        Nothing ->
            none

        Just whenUnchecked ->
            if not data.checked then
                el
                    [ width fill
                    , paddingEach
                        { top = 0
                        , bottom = 0
                        , left = 56
                        , right = 0
                        }
                    ]
                    whenUnchecked

            else
                none
    ]
        |> column
            [ width fill
            , Element.class "ui-atom-checkbox"
            ]


viewCheckbox : Data msg -> Element msg
viewCheckbox data =
    let
        addId attrs =
            case data.id of
                Nothing ->
                    attrs

                Just theId ->
                    id (Pointer.toString theId) :: attrs

        addOnCopy attrs =
            case data.onCopy of
                Nothing ->
                    attrs

                Just msg ->
                    Clipboard.onCopy msg :: attrs

        addOnPaste attrs =
            case data.onPasteGraph of
                Nothing ->
                    attrs

                Just ( msgIgnored, decoderMsg ) ->
                    Clipboard.onPasteGraph msgIgnored decoderMsg :: attrs
    in
    { onChange = data.onChange
    , icon = Input.checkboxDefault
    , checked = data.checked
    , label =
        (case data.supportingText of
            Nothing ->
                viewLabel data.label

            Just text ->
                [ viewLabel data.label
                , viewSupportingText text
                ]
                    |> column
                        [ width fill
                        , spacing 8
                        ]
        )
            |> Input.labelRight
                [ paddingEach
                    { top = 6
                    , bottom = 0
                    , left = 20
                    , right = 0
                    }
                , width fill
                ]
    }
        |> Input.checkbox
            ([ paddingEach
                { top = 2
                , bottom = 2
                , left = 8
                , right = 16
                }
             , width fill
             ]
                |> addId
                |> addOnCopy
                |> addOnPaste
            )


viewLabel : Fluent -> Element msg
viewLabel label =
    label
        |> body1
        |> List.singleton
        |> paragraph [ width fill ]


viewSupportingText : Fluent -> Element msg
viewSupportingText text =
    text
        |> body2
        |> List.singleton
        |> paragraph
            [ width fill
            , Font.color onSurfaceVariant
            ]
