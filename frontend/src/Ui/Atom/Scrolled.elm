{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Atom.Scrolled exposing (Config, Offsets, initialOffsets, view)

import Element
    exposing
        ( Element
        , column
        , fill
        , height
        , htmlAttribute
        , scrollbarY
        , width
        )
import Element.Extra
    exposing
        ( class
        , elWithAncestorWithScrollbarX
        , id
        )
import Html.Events
import Json.Decode as Decode


type alias Offsets =
    { offsetTop : Float
    , offsetBottom : Float
    }


initialOffsets : Offsets
initialOffsets =
    { offsetTop = 0
    , offsetBottom = 0
    }


type alias Config msg =
    { content : Element msg
    , onScroll : Offsets -> msg
    , offsets : Offsets
    , id : String
    }


view : Config msg -> Element msg
view config =
    column
        [ width fill
        , height fill
        , class "mdc-elevation-inset--z4"
        , class "mdc-elevation-inset--top"
        , class "mdc-elevation-inset--bottom"
        , class
            (if config.offsets.offsetTop < 16 then
                "mdc-elevation-inset--top-hidden"

             else
                ""
            )
        , class
            (if config.offsets.offsetBottom < 16 then
                "mdc-elevation-inset--bottom-hidden"

             else
                ""
            )
        ]
        [ elWithAncestorWithScrollbarX
            [ width fill
            , height fill
            , scrollbarY
            , htmlAttribute
                (Html.Events.on "scroll"
                    (Decode.map config.onScroll
                        (Decode.map3
                            (\scrollTop scrollHeight clientHeight ->
                                { offsetTop = scrollTop
                                , offsetBottom = scrollHeight - clientHeight - scrollTop
                                }
                            )
                            (Decode.at [ "target", "scrollTop" ] Decode.float)
                            (Decode.at [ "target", "scrollHeight" ] Decode.float)
                            (Decode.at [ "target", "clientHeight" ] Decode.float)
                        )
                    )
                )
            , id config.id
            ]
            config.content
        ]
