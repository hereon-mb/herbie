{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Atom.OptionCombobox exposing
    ( OptionCombobox
    , decoder
    , encode
    , view
    )

import Element
    exposing
        ( Element
        , alignBottom
        , el
        , fill
        , row
        , spacing
        , width
        )
import Element.Font as Font
import Fluent exposing (Fluent)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Decode
import Json.Encode as Encode exposing (Value)
import Ui.Theme.Color
    exposing
        ( onSurface
        )
import Ui.Theme.Typography
    exposing
        ( body1
        , body2
        )


type alias OptionCombobox =
    { id : String
    , value : String
    , label : Fluent
    , subLabel : Fluent
    }


view : Bool -> OptionCombobox -> Element Never
view renderSubLabel { label, subLabel } =
    if renderSubLabel then
        [ label
            |> body1
        , subLabel
            |> body2
            |> el
                [ alignBottom
                , Font.color onSurface
                ]
        ]
            |> row
                [ width fill
                , spacing 8
                ]

    else
        [ label
            |> body1
        ]
            |> row
                [ width fill
                , spacing 8
                ]


decoder : Decoder OptionCombobox
decoder =
    Decode.succeed OptionCombobox
        |> Decode.required "id" Decode.string
        |> Decode.required "value" Decode.string
        |> Decode.required "label" Fluent.decoder
        |> Decode.required "subLabel" Fluent.decoder


encode : OptionCombobox -> Value
encode option =
    Encode.object
        [ ( "id", Encode.string option.id )
        , ( "value", Encode.string option.value )
        , ( "label", Fluent.encode option.label )
        , ( "subLabel", Fluent.encode option.subLabel )
        ]
