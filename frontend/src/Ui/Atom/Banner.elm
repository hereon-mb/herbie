module Ui.Atom.Banner exposing
    ( Banner, banner, toElement
    , Format(..), withFormat
    , withButton
    )

{-|

@docs Banner, banner, toElement
@docs Format, withFormat
@docs withButton

-}

import Element
    exposing
        ( Attribute
        , Element
        , alignBottom
        , alignRight
        , el
        , fill
        , none
        , paddingEach
        , paddingXY
        , paragraph
        , row
        , spacing
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Fluent exposing (Fluent)
import Ui.Atom.Button as Button exposing (Button)
import Ui.Theme.Color
    exposing
        ( inverseOnSurface
        , inversePrimary
        , inverseSurface
        , onSurface
        , primarySurfaceContainerHighest
        )
import Ui.Theme.Typography
    exposing
        ( body1
        )


type Banner msg
    = Banner (Data msg)


type alias Data msg =
    { content : Fluent
    , format : Format
    , button : Maybe (Button msg)
    }


type Format
    = Primary
    | Info


banner : Fluent -> Banner msg
banner content =
    Banner
        { content = content
        , format = Info
        , button = Nothing
        }


withFormat : Format -> Banner msg -> Banner msg
withFormat format (Banner data) =
    Banner { data | format = format }


withButton : Button msg -> Banner msg -> Banner msg
withButton button (Banner data) =
    Banner { data | button = Just button }


toElement : Banner msg -> Element msg
toElement (Banner data) =
    [ viewContent data
    , viewButton data
    ]
        |> row
            [ width fill
            , paddingEach
                { top = 6
                , bottom = 6
                , left = 16
                , right = 2
                }
            , spacing 8
            , backgroundColor data
            , fontColor data
            , Border.rounded 4
            ]


viewContent : Data msg -> Element msg
viewContent data =
    data.content
        |> body1
        |> List.singleton
        |> paragraph [ paddingXY 0 8 ]


viewButton : Data msg -> Element msg
viewButton data =
    case data.button of
        Nothing ->
            none

        Just button ->
            button
                |> Button.withFormat Button.Text
                |> (case data.format of
                        Primary ->
                            identity

                        Info ->
                            Button.withColor inversePrimary
                   )
                |> Button.view
                |> el
                    [ alignRight
                    , alignBottom
                    ]


backgroundColor : Data msg -> Attribute msg
backgroundColor data =
    Background.color
        (case data.format of
            Primary ->
                primarySurfaceContainerHighest

            Info ->
                inverseSurface
        )


fontColor : Data msg -> Attribute msg
fontColor data =
    Font.color
        (case data.format of
            Primary ->
                onSurface

            Info ->
                inverseOnSurface
        )
