{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Atom.ChipInput exposing
    ( ChipInput, chipInput, toElement
    , withIcon
    , withOnPress, withOnPressRemove
    , selected
    , withEnabled
    )

{-| Implementation of the [Filter
chip](https://m3.material.io/components/chips/guidelines#8d453d50-8d8e-43aa-9ae3-87ed134d2e64)
component.

@docs ChipInput, chipInput, toElement
@docs withIcon
@docs withOnPress, withOnPressRemove
@docs selected
@docs withEnabled

-}

import Element exposing (Element)
import Fluent exposing (Fluent)
import Ui.Atom.Chip as Chip
import Ui.Atom.Icon as Icon exposing (Icon)


type ChipInput msg
    = ChipInput (Config msg)


type alias Config msg =
    { icon : Maybe Icon
    , label : Fluent
    , onPress : Maybe msg
    , onPressRemove : Maybe msg
    , selected : Bool
    , enabled : Bool
    }


chipInput : Fluent -> ChipInput msg
chipInput label =
    ChipInput
        { icon = Nothing
        , label = label
        , onPress = Nothing
        , onPressRemove = Nothing
        , selected = False
        , enabled = True
        }


withIcon : Icon -> ChipInput msg -> ChipInput msg
withIcon icon (ChipInput data) =
    ChipInput { data | icon = Just icon }


withOnPress : msg -> ChipInput msg -> ChipInput msg
withOnPress msg (ChipInput data) =
    ChipInput { data | onPress = Just msg }


withOnPressRemove : msg -> ChipInput msg -> ChipInput msg
withOnPressRemove msg (ChipInput data) =
    ChipInput { data | onPressRemove = Just msg }


selected : ChipInput msg -> ChipInput msg
selected (ChipInput data) =
    ChipInput { data | selected = True }


withEnabled : Bool -> ChipInput msg -> ChipInput msg
withEnabled enabled (ChipInput data) =
    ChipInput { data | enabled = enabled }


toElement : ChipInput msg -> Element msg
toElement (ChipInput data) =
    view data


view : Config msg -> Element msg
view config =
    Chip.view
        { iconLeft = config.icon
        , label = config.label
        , iconRight =
            if config.onPressRemove /= Nothing then
                Just Icon.Close

            else
                Nothing
        , narrow = True
        , elevated = False
        , elevatedOnHover = False
        , primary = config.selected
        , onPress =
            if config.enabled then
                config.onPress

            else
                Nothing
        , onPressRight = config.onPressRemove
        , connectedOnLeft = False
        , connectedOnRight = False
        }
