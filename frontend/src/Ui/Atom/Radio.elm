{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Atom.Radio exposing
    ( Radio, radio, view
    , withId
    , onCopy, onPaste
    , option, withSupportingText
    , Option
    )

{-|

@docs Radio, radio, view

@docs withId
@docs onCopy, onPaste

@docs option, withSupportingText

-}

import Accessibility.Clipboard as Clipboard
import Element
    exposing
        ( Element
        , alignTop
        , centerX
        , centerY
        , column
        , el
        , fill
        , focused
        , height
        , mouseDown
        , mouseOver
        , paddingEach
        , paddingXY
        , paragraph
        , px
        , row
        , spacing
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Extra as Element
import Element.Font as Font
import Element.Input exposing (OptionState)
import Fluent exposing (Fluent)
import Ui.Atom.ButtonIcon as ButtonIcon
import Ui.Atom.Icon as Icon
import Ui.Theme.Color as Color
    exposing
        ( onSurfaceVariant
        , primary
        , transparent
        )
import Ui.Theme.Typography
    exposing
        ( body1
        , body2
        , caption
        )


type Radio option msg
    = Radio (DataRadio option msg)


type alias DataRadio option msg =
    { onChange : option -> msg
    , onCopy : Maybe msg
    , onPaste : Maybe ( String, msg, String -> Maybe msg )
    , selected : Maybe option
    , label : Fluent
    , options : List (Option option)
    , id : Maybe String
    }


type Option option
    = Option (DataOption option)


type alias DataOption option =
    { option : option
    , label : Fluent
    , supportingText : Maybe Fluent
    }


radio :
    (option -> msg)
    -> Fluent
    -> Maybe option
    -> List (Option option)
    -> Radio option msg
radio onChange label selected options =
    Radio
        { onChange = onChange
        , onCopy = Nothing
        , onPaste = Nothing
        , selected = selected
        , label = label
        , options = options
        , id = Nothing
        }


withId : String -> Radio option msg -> Radio option msg
withId id (Radio data) =
    Radio { data | id = Just id }


onCopy : msg -> Radio option msg -> Radio option msg
onCopy msg (Radio data) =
    Radio { data | onCopy = Just msg }


onPaste : String -> msg -> (String -> Maybe msg) -> Radio option msg -> Radio option msg
onPaste format msgPrevented msg (Radio data) =
    Radio { data | onPaste = Just ( format, msgPrevented, msg ) }


option : option -> Fluent -> Option option
option value label =
    Option
        { option = value
        , label = label
        , supportingText = Nothing
        }


withSupportingText : Fluent -> Option option -> Option option
withSupportingText supportingText (Option data) =
    Option { data | supportingText = Just supportingText }



---- VIEW


view : Radio option msg -> Element msg
view (Radio data) =
    let
        addId attrs =
            case data.id of
                Nothing ->
                    attrs

                Just id ->
                    Element.id id :: attrs

        addOnCopy attrs =
            case data.onCopy of
                Nothing ->
                    attrs

                Just msg ->
                    Clipboard.onCopy msg :: attrs

        addOnPaste attrs =
            case data.onPaste of
                Nothing ->
                    attrs

                Just ( format, msgPrevented, msg ) ->
                    Clipboard.onPaste format msgPrevented msg :: attrs

        buttonCopy =
            case data.onCopy of
                Nothing ->
                    Element.none

                Just msg ->
                    Icon.ContentCopy
                        |> ButtonIcon.buttonIcon msg
                        |> ButtonIcon.withAttributes
                            [ Element.class "ui-atom-radio__button-copy"
                            , Element.alignRight
                            ]
                        |> ButtonIcon.toElement
    in
    Element.Input.radio
        ([ paddingXY 16 0
         , spacing 8
         , Font.color onSurfaceVariant
         , Element.class "ui-atom-radio"
         , width fill
         ]
            |> addId
            |> addOnCopy
            |> addOnPaste
        )
        { onChange = data.onChange
        , selected = data.selected
        , label =
            Element.Input.labelAbove
                [ paddingEach
                    { top = 0
                    , bottom = 16
                    , left = 16
                    , right = 16
                    }
                ]
                (row
                    [ width fill ]
                    [ caption data.label
                    , buttonCopy
                    ]
                )
        , options = List.map viewOption data.options
        }


viewOption : Option option -> Element.Input.Option option msg
viewOption (Option data) =
    Element.Input.optionWith data.option
        (\optionState ->
            row
                [ width fill
                , spacing 28
                ]
                [ viewIcon optionState
                , viewText data
                ]
        )


viewIcon : OptionState -> Element msg
viewIcon optionState =
    el
        [ width (px 36)
        , height (px 36)
        , alignTop
        , Border.rounded 18
        , Background.color transparent
        , mouseOver
            [ Background.color
                (case optionState of
                    Element.Input.Idle ->
                        Color.overlayHovered primary

                    Element.Input.Focused ->
                        Color.overlayHovered primary

                    Element.Input.Selected ->
                        Color.overlayHovered primary
                )
            ]
        , focused
            [ Background.color
                (case optionState of
                    Element.Input.Idle ->
                        transparent

                    Element.Input.Focused ->
                        transparent

                    Element.Input.Selected ->
                        Color.overlayFocused primary
                )
            ]
        , mouseDown
            [ Background.color
                (case optionState of
                    Element.Input.Idle ->
                        transparent

                    Element.Input.Focused ->
                        transparent

                    Element.Input.Selected ->
                        Color.overlayPressed primary
                )
            ]
        ]
        (el
            [ centerX
            , centerY
            , Font.color
                (case optionState of
                    Element.Input.Idle ->
                        onSurfaceVariant

                    Element.Input.Focused ->
                        onSurfaceVariant

                    Element.Input.Selected ->
                        primary
                )
            ]
            (Icon.viewSmall
                (case optionState of
                    Element.Input.Idle ->
                        Icon.RadioButtonUnchecked

                    Element.Input.Focused ->
                        Icon.RadioButtonUnchecked

                    Element.Input.Selected ->
                        Icon.RadioButtonChecked
                )
            )
        )


viewText : DataOption option -> Element msg
viewText data =
    el
        [ width fill
        , paddingEach
            { top = 6
            , bottom = 0
            , left = 0
            , right = 0
            }
        ]
        (case data.supportingText of
            Nothing ->
                viewLabel data

            Just supportingText ->
                column
                    [ width fill
                    , spacing 8
                    ]
                    [ viewLabel data
                    , viewSupportingText supportingText
                    ]
        )


viewLabel : DataOption option -> Element msg
viewLabel data =
    paragraph
        [ width fill ]
        [ body1 data.label
        ]


viewSupportingText : Fluent -> Element msg
viewSupportingText supportingText =
    paragraph
        [ width fill
        , Font.color onSurfaceVariant
        ]
        [ body2 supportingText ]
