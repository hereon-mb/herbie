{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Atom.ButtonToggle exposing
    ( Config
    , Group
    , Option
    , Orientation(..)
    , group
    , optionFluent
    , optionIcon
    , view
    , withAttributes
    )

import Element
    exposing
        ( Attribute
        , Element
        , centerX
        , centerY
        , el
        , height
        , minimum
        , mouseOver
        , paddingXY
        , px
        , shrink
        , spacing
        , width
        , wrappedRow
        )
import Element.Background as Background
import Element.Border as Border
import Element.Input as Input
import Fluent exposing (Fluent)
import Ui.Atom.Icon as Icon exposing (Icon)
import Ui.Theme.Color as Color
    exposing
        ( onSecondaryContainer
        , outline
        , secondaryContainer
        , transparent
        , withOpacity
        )
import Ui.Theme.Typography as Typography


type alias Config option msg =
    { onChange : option -> msg
    , selected : option
    , groups : List (Group option msg)
    , label : String
    , orientation : Orientation
    }


type Group value msg
    = Group
        { options : List (Option value msg)
        , attrs : List (Attribute msg)
        }


group : List (Option value msg) -> Group value msg
group options =
    Group
        { options = options
        , attrs = []
        }


type Option value msg
    = Option
        { label : OptionLabel
        , value : value
        }


type OptionLabel
    = OptionFluent Fluent
    | OptionIcon Icon


optionFluent : value -> Fluent -> Option value msg
optionFluent value label =
    Option
        { value = value
        , label = OptionFluent label
        }


optionIcon : value -> Icon -> Option value msg
optionIcon value icon =
    Option
        { value = value
        , label = OptionIcon icon
        }


withAttributes : List (Attribute msg) -> Group value msg -> Group value msg
withAttributes attrs (Group data) =
    Group { data | attrs = attrs }


type Orientation
    = Vertical
    | Horizontal


view : Config option msg -> Element msg
view config =
    wrappedRow
        [ spacing 8 ]
        (List.map (viewGroup config) config.groups)


viewGroup : Config value msg -> Group value msg -> Element msg
viewGroup config (Group data) =
    (case config.orientation of
        Horizontal ->
            Input.radioRow

        Vertical ->
            Input.radio
    )
        data.attrs
        { onChange = config.onChange
        , options = List.indexedMap (viewOption config (List.length data.options)) data.options
        , selected = Just config.selected
        , label = Input.labelHidden config.label
        }


viewOption : Config value msg -> Int -> Int -> Option value msg -> Input.Option value msg
viewOption config count index (Option option) =
    Input.optionWith option.value
        (\optionState ->
            el
                [ paddingXY 12 0
                , width
                    (case config.orientation of
                        Horizontal ->
                            minimum 48 shrink

                        Vertical ->
                            px 48
                    )
                , height
                    (px
                        (case config.orientation of
                            Horizontal ->
                                48

                            Vertical ->
                                35
                        )
                    )
                , Background.color
                    (case optionState of
                        Input.Idle ->
                            transparent

                        Input.Focused ->
                            onSecondaryContainer
                                |> withOpacity 0.1

                        Input.Selected ->
                            secondaryContainer
                    )
                , Border.roundEach
                    (if index == 0 then
                        case config.orientation of
                            Horizontal ->
                                { topLeft = 4
                                , topRight = 0
                                , bottomRight = 0
                                , bottomLeft = 4
                                }

                            Vertical ->
                                { topLeft = 4
                                , topRight = 4
                                , bottomRight = 0
                                , bottomLeft = 0
                                }

                     else if index == count - 1 then
                        case config.orientation of
                            Horizontal ->
                                { topLeft = 0
                                , topRight = 4
                                , bottomRight = 4
                                , bottomLeft = 0
                                }

                            Vertical ->
                                { topLeft = 0
                                , topRight = 0
                                , bottomRight = 4
                                , bottomLeft = 4
                                }

                     else
                        { topLeft = 0
                        , topRight = 0
                        , bottomRight = 0
                        , bottomLeft = 0
                        }
                    )
                , Border.widthEach
                    (if index == count - 1 then
                        { left = 1
                        , right = 1
                        , top = 1
                        , bottom = 1
                        }

                     else
                        case config.orientation of
                            Horizontal ->
                                { left = 1
                                , right = 0
                                , top = 1
                                , bottom = 1
                                }

                            Vertical ->
                                { left = 1
                                , right = 1
                                , top = 1
                                , bottom = 0
                                }
                    )
                , Border.color outline
                , mouseOver
                    [ Background.color
                        (Color.hovered
                            (case optionState of
                                Input.Idle ->
                                    transparent

                                Input.Focused ->
                                    onSecondaryContainer
                                        |> withOpacity 0.1

                                Input.Selected ->
                                    secondaryContainer
                            )
                        )
                    ]
                ]
                (el
                    [ centerX
                    , centerY
                    ]
                    (case option.label of
                        OptionFluent label ->
                            Typography.body1 label

                        OptionIcon icon ->
                            Icon.viewNormal icon
                    )
                )
        )
