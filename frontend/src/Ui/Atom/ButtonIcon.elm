{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Atom.ButtonIcon exposing
    ( ButtonIcon, buttonIcon, toElement
    , withEnabled, withId
    , excludeFromTabSequence
    , small, onDark
    , withAttributes
    )

{-|

@docs ButtonIcon, buttonIcon, toElement
@docs withEnabled, withId
@docs excludeFromTabSequence
@docs small, onDark

-}

import Accessibility.Key
import Element
    exposing
        ( Attribute
        , Element
        , centerX
        , centerY
        , el
        , focused
        , height
        , mouseDown
        , mouseOver
        , padding
        , px
        , shrink
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Extra
    exposing
        ( disabled
        , id
        , preventDefaultOnKeyDown
        , stopPropagationAndPreventDefaultOnClick
        , tabindex
        )
import Element.Font as Font
import Element.Input as Input
import Ui.Atom.Icon as Icon exposing (Icon)
import Ui.Theme.Color
    exposing
        ( onSurface
        , onSurfaceVariant
        , withOpacity
        )


type ButtonIcon msg
    = ButtonIcon (Data msg)


type alias Data msg =
    { onPress : OnPress msg
    , icon : Icon
    , id : Maybe String
    , onDark : Bool
    , small : Bool
    , enabled : Bool
    , excludeFromTabSequence : Bool
    , attrs : List (Element.Attribute msg)
    }


type OnPress msg
    = Msg msg


buttonIcon : msg -> Icon -> ButtonIcon msg
buttonIcon onPress icon =
    ButtonIcon
        { onPress = Msg onPress
        , icon = icon
        , id = Nothing
        , onDark = False
        , small = False
        , enabled = True
        , excludeFromTabSequence = False
        , attrs = []
        }


withEnabled : Bool -> ButtonIcon msg -> ButtonIcon msg
withEnabled enabled (ButtonIcon data) =
    ButtonIcon { data | enabled = enabled }


withId : String -> ButtonIcon msg -> ButtonIcon msg
withId id (ButtonIcon data) =
    ButtonIcon { data | id = Just id }


onDark : ButtonIcon msg -> ButtonIcon msg
onDark (ButtonIcon data) =
    ButtonIcon { data | onDark = True }


small : ButtonIcon msg -> ButtonIcon msg
small (ButtonIcon data) =
    ButtonIcon { data | small = True }


excludeFromTabSequence : ButtonIcon msg -> ButtonIcon msg
excludeFromTabSequence (ButtonIcon data) =
    ButtonIcon { data | excludeFromTabSequence = True }


withAttributes : List (Element.Attribute msg) -> ButtonIcon msg -> ButtonIcon msg
withAttributes attrs (ButtonIcon data) =
    ButtonIcon { data | attrs = attrs }


toElement : ButtonIcon msg -> Element msg
toElement (ButtonIcon data) =
    case data.onPress of
        Msg _ ->
            Input.button (attributes data)
                { onPress = Nothing
                , label = viewLabel data
                }


attributes : Data msg -> List (Attribute msg)
attributes data =
    [ [ Border.rounded 18 ]
    , case data.id of
        Nothing ->
            []

        Just theId ->
            [ id theId ]
    , if data.enabled then
        case data.onPress of
            Msg msg ->
                [ stopPropagationAndPreventDefaultOnClick msg
                , preventDefaultOnKeyDown
                    [ Accessibility.Key.enter ( msg, True )
                    , Accessibility.Key.space ( msg, True )
                    ]
                , disabled False
                ]

      else
        []
    , if data.small then
        [ width (px 36)
        , height (px 36)
        ]

      else
        [ width shrink
        , height shrink
        ]
    , [ if data.enabled then
            Font.color onSurfaceVariant

        else
            onSurface
                |> withOpacity 0.38
                |> Font.color
      , mouseDown
            [ onSurfaceVariant
                |> withOpacity 0.1
                |> Background.color
            ]
      , mouseOver
            [ onSurfaceVariant
                |> withOpacity 0.08
                |> Background.color
            ]
      , focused
            [ onSurfaceVariant
                |> withOpacity 0.1
                |> Background.color
            ]
      ]
    , if data.excludeFromTabSequence then
        [ tabindex -1
        ]

      else
        []
    , data.attrs
    ]
        |> List.concat


viewLabel : Data msg -> Element msg
viewLabel data =
    if data.small then
        data.icon
            |> Icon.viewSmall
            |> el
                [ centerX
                , centerY
                ]

    else
        data.icon
            |> Icon.viewNormal
            |> el [ padding 8 ]
