{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Atom.ChipFilter exposing
    ( ChipFilter, chipFilter, toElement
    , withIcon
    , elevated
    , withEnabled
    , connectedOnLeft, connectedOnRight
    , withDropdown
    )

{-| Implementation of the [Filter
chip](https://m3.material.io/components/chips/guidelines#8d453d50-8d8e-43aa-9ae3-87ed134d2e64)
component.

@docs ChipFilter, chipFilter, toElement
@docs withIcon
@docs elevated
@docs withEnabled
@docs connectedOnLeft, connectedOnRight
@docs withDropdown

-}

import Element exposing (Element)
import Fluent exposing (Fluent)
import Ui.Atom.Chip as Chip
import Ui.Atom.Icon as Icon exposing (Icon)


type ChipFilter msg
    = ChipFilter (Config msg)


type alias Config msg =
    { icon : Maybe Icon
    , elevated : Bool
    , label : Fluent
    , onPress : msg
    , selected : Bool
    , enabled : Bool
    , connectedOnLeft : Bool
    , connectedOnRight : Bool
    , dropdown : Maybe Bool
    }


chipFilter : msg -> Bool -> Fluent -> ChipFilter msg
chipFilter onPress selected label =
    ChipFilter
        { icon = Nothing
        , elevated = False
        , label = label
        , onPress = onPress
        , selected = selected
        , enabled = True
        , connectedOnLeft = False
        , connectedOnRight = False
        , dropdown = Nothing
        }


connectedOnRight : ChipFilter msg -> ChipFilter msg
connectedOnRight (ChipFilter data) =
    ChipFilter { data | connectedOnRight = True }


connectedOnLeft : ChipFilter msg -> ChipFilter msg
connectedOnLeft (ChipFilter data) =
    ChipFilter { data | connectedOnLeft = True }


withEnabled : Bool -> ChipFilter msg -> ChipFilter msg
withEnabled enabled (ChipFilter data) =
    ChipFilter { data | enabled = enabled }


withIcon : Icon -> ChipFilter msg -> ChipFilter msg
withIcon icon (ChipFilter data) =
    ChipFilter { data | icon = Just icon }


elevated : ChipFilter msg -> ChipFilter msg
elevated (ChipFilter data) =
    ChipFilter { data | elevated = True }


withDropdown : Bool -> ChipFilter msg -> ChipFilter msg
withDropdown open (ChipFilter data) =
    ChipFilter { data | dropdown = Just open }


toElement : ChipFilter msg -> Element msg
toElement (ChipFilter data) =
    view data


view : Config msg -> Element msg
view config =
    Chip.view
        { iconLeft = config.icon
        , label = config.label
        , iconRight =
            case config.dropdown of
                Nothing ->
                    Nothing

                Just True ->
                    Just Icon.ArrowDropUp

                Just False ->
                    Just Icon.ArrowDropDown
        , narrow = False
        , elevated = config.elevated
        , elevatedOnHover = config.selected
        , primary = config.selected
        , onPress =
            if config.enabled then
                Just config.onPress

            else
                Nothing
        , onPressRight = Nothing
        , connectedOnLeft = config.connectedOnLeft
        , connectedOnRight = config.connectedOnRight
        }
