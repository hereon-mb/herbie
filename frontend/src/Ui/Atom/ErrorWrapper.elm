module Ui.Atom.ErrorWrapper exposing (errorWrapper)

import Element
    exposing
        ( Element
        , alignTop
        , column
        , el
        , fill
        , height
        , none
        , paragraph
        , px
        , row
        , spacing
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Fluent exposing (Fluent)
import Ui.Atom.Icon as Icon
import Ui.Theme.Color exposing (error)
import Ui.Theme.Typography exposing (body1)


errorWrapper : List Fluent -> Element msg -> Element msg
errorWrapper problems element =
    if List.isEmpty problems then
        element

    else
        [ el
            [ width (px 6)
            , height fill
            , Border.rounded 3
            , Background.color error
            ]
            none
        , [ [ Icon.Error
                |> Icon.viewNormal
                |> el [ alignTop ]
            , problems
                |> List.map
                    (body1
                        >> List.singleton
                        >> paragraph
                            [ width fill
                            , alignTop
                            ]
                    )
                |> column
                    [ width fill
                    , spacing 4
                    ]
            ]
                |> row
                    [ width fill
                    , spacing 6
                    , Font.color error
                    ]
          , element
          ]
            |> column
                [ width fill
                , spacing 4
                ]
        ]
            |> row
                [ width fill
                , spacing 8
                ]
