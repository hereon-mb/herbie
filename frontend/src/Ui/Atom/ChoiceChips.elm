{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Atom.ChoiceChips exposing
    ( choiceChips
    , Chip
    , Label, labelAbove
    )

{-|

@docs choiceChips
@docs Chip
@docs Label, labelAbove

-}

import Accessibility.Clipboard as Clipboard
import Element
    exposing
        ( Element
        , el
        , fill
        , html
        , width
        )
import Fluent exposing (Fluent)
import Html exposing (Html)
import Html.Attributes
import Html.Events
import Json.Decode as Decode
import Maybe.Extra as Maybe
import Pointer
import Ui.Atom.ChoiceChipsCustomElement as CustomElement


choiceChips :
    { id : Pointer.Pointer
    , label : Label
    , help : List Fluent
    , chips : List (Chip option)
    , selected : Maybe option
    , onChange : option -> msg
    , onCopy : Maybe msg
    , onPaste : Maybe ( String, msg, String -> Maybe msg )
    , toString : option -> String
    , fromString : String -> Maybe option
    }
    -> Element msg
choiceChips config =
    el
        [ width fill ]
        (html (choiceChipsHelp config))


choiceChipsHelp :
    { id : Pointer.Pointer
    , label : Label
    , help : List Fluent
    , chips : List (Chip option)
    , selected : Maybe option
    , onChange : option -> msg
    , onCopy : Maybe msg
    , onPaste : Maybe ( String, msg, String -> Maybe msg )
    , toString : option -> String
    , fromString : String -> Maybe option
    }
    -> Html msg
choiceChipsHelp config =
    let
        addOnCopy attrs =
            case config.onCopy of
                Nothing ->
                    attrs

                Just msg ->
                    Clipboard.onCopyHtml msg :: attrs

        addOnPaste attrs =
            case config.onPaste of
                Nothing ->
                    attrs

                Just ( format, msgPrevented, msg ) ->
                    Clipboard.onPasteHtml format msgPrevented msg :: attrs
    in
    Html.node CustomElement.nameNode
        ([ Html.Attributes.property "elmData"
            (CustomElement.encode config.toString
                { id = Pointer.toString config.id
                , label = config.label
                , help = config.help
                , chips = config.chips
                , selected = config.selected
                , onCopy = Maybe.isJust config.onCopy
                }
            )
         , Html.Events.on "elm-data-changed"
            (Decode.string
                |> Decode.field "detail"
                |> Decode.andThen
                    (\raw ->
                        case config.fromString raw of
                            Nothing ->
                                Decode.fail ("not an option:" ++ raw)

                            Just option ->
                                Decode.succeed option
                    )
                |> Decode.map config.onChange
            )
         ]
            |> addOnCopy
            |> addOnPaste
        )
        []


type alias Label =
    CustomElement.Label


type alias Chip option =
    CustomElement.Chip option


labelAbove : Fluent -> Label
labelAbove =
    CustomElement.labelAbove
