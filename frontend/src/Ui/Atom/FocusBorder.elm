{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Atom.FocusBorder exposing
    ( FocusBorder
    , Label
    , Menu
    , empty
    , focusBorder
    , onClear
    , toElement
    , withActionIcon
    , withDensity
    , withHeightFill
    , withHelp
    , withHoverableActionIcon
    , withLabel
    , withLeadingIcon
    , withMenu
    , withPaddingLeftPx
    , withPaddingRightPx
    , withSupportingText
    )

import Element
    exposing
        ( Element
        , alignRight
        , centerY
        , column
        , el
        , fill
        , height
        , htmlAttribute
        , inFront
        , moveDown
        , none
        , paddingEach
        , paddingXY
        , paragraph
        , px
        , row
        , shrink
        , width
        )
import Element.Border as Border
import Element.Extra as Element
    exposing
        ( class
        , pointerEvents
        )
import Element.Font as Font
import Fluent exposing (Fluent)
import Html.Events
import Json.Decode as Decode
import Maybe.Extra as Maybe
import Ui.Atom.ButtonIcon as ButtonIcon exposing (buttonIcon)
import Ui.Atom.Icon as Icon exposing (Icon)
import Ui.Atom.Portal as Portal
import Ui.Theme.Color
    exposing
        ( error
        , onSurfaceVariant
        , outline
        )
import Ui.Theme.Typography


type FocusBorder msg
    = FocusBorder (FocusBorderData msg)


type alias FocusBorderData msg =
    { label : Maybe Label
    , content : Element msg
    , empty : Bool
    , density : Int
    , paddingRightPx : Int
    , paddingLeftPx : Int
    , heightFill : Bool
    , supportingText : Maybe Fluent
    , help : List Fluent
    , withLeadingIcon : Bool
    , onClear : Maybe msg
    , actionIcon : Maybe (ActionIcon msg)
    , actionIconHoverable : Maybe (ActionIcon msg)
    , menu : Maybe (Menu msg)
    }


type alias Menu msg =
    { onViewport : Portal.Viewport -> msg
    , viewport : Maybe Portal.Viewport
    , menu : Element msg
    }


type alias Label =
    { id : String
    , text : Fluent
    }


type alias ActionIcon msg =
    { icon : Icon
    , onClick : msg
    }


focusBorder : Element msg -> FocusBorder msg
focusBorder content =
    FocusBorder
        { label = Nothing
        , content = content
        , empty = False
        , density = 0
        , paddingRightPx = 0
        , paddingLeftPx = 0
        , heightFill = False
        , supportingText = Nothing
        , help = []
        , withLeadingIcon = False
        , onClear = Nothing
        , actionIcon = Nothing
        , actionIconHoverable = Nothing
        , menu = Nothing
        }


withLabel : Label -> FocusBorder msg -> FocusBorder msg
withLabel label (FocusBorder data) =
    FocusBorder { data | label = Just label }


onClear : msg -> FocusBorder msg -> FocusBorder msg
onClear msg (FocusBorder data) =
    FocusBorder { data | onClear = Just msg }


empty : FocusBorder msg -> FocusBorder msg
empty (FocusBorder data) =
    FocusBorder { data | empty = True }


withDensity : Int -> FocusBorder msg -> FocusBorder msg
withDensity density (FocusBorder data) =
    FocusBorder { data | density = density }


withPaddingRightPx : Int -> FocusBorder msg -> FocusBorder msg
withPaddingRightPx paddingRightPx (FocusBorder data) =
    FocusBorder { data | paddingRightPx = paddingRightPx }


withPaddingLeftPx : Int -> FocusBorder msg -> FocusBorder msg
withPaddingLeftPx paddingLeftPx (FocusBorder data) =
    FocusBorder { data | paddingLeftPx = paddingLeftPx }


withHeightFill : FocusBorder msg -> FocusBorder msg
withHeightFill (FocusBorder data) =
    FocusBorder { data | heightFill = True }


withSupportingText : Fluent -> FocusBorder msg -> FocusBorder msg
withSupportingText supportingText (FocusBorder data) =
    FocusBorder { data | supportingText = Just supportingText }


withLeadingIcon : FocusBorder msg -> FocusBorder msg
withLeadingIcon (FocusBorder data) =
    FocusBorder { data | withLeadingIcon = True }


withActionIcon : Icon -> msg -> FocusBorder msg -> FocusBorder msg
withActionIcon icon onClick (FocusBorder data) =
    FocusBorder
        { data
            | actionIcon =
                Just
                    { icon = icon
                    , onClick = onClick
                    }
        }


withHoverableActionIcon : Icon -> msg -> FocusBorder msg -> FocusBorder msg
withHoverableActionIcon icon onClick (FocusBorder data) =
    FocusBorder
        { data
            | actionIconHoverable =
                Just
                    { icon = icon
                    , onClick = onClick
                    }
        }


withHelp : List Fluent -> FocusBorder msg -> FocusBorder msg
withHelp help (FocusBorder data) =
    FocusBorder { data | help = help }


withMenu : Menu msg -> FocusBorder msg -> FocusBorder msg
withMenu menu (FocusBorder data) =
    FocusBorder { data | menu = Just menu }


toElement : FocusBorder msg -> Element msg
toElement (FocusBorder data) =
    let
        withMenuAttributes attrs =
            case data.menu of
                Nothing ->
                    attrs

                Just { onViewport } ->
                    htmlAttribute
                        (Html.Events.on "focusin"
                            (Decode.map onViewport Portal.viewportDecoder)
                        )
                        :: attrs
    in
    [ el
        ([ width fill
         , if data.heightFill then
            height fill

           else
            height shrink
         , class "ui-atom-focus-border"
         , inFront (viewBorder data)
         , inFront (viewLabel data)
         , inFront (viewIcons data)
         ]
            |> withMenuAttributes
        )
        data.content
    , case data.menu of
        Nothing ->
            none

        Just { viewport, menu } ->
            case viewport of
                Nothing ->
                    none

                Just theViewport ->
                    -- We need to render the portal as soon as possible to
                    -- prevent possible exceptions in elms dom due to
                    -- missing nodes
                    Portal.view theViewport
                        (el
                            [ width fill
                            , moveDown 1
                            ]
                            menu
                        )
    , if List.isEmpty data.help then
        case data.supportingText of
            Nothing ->
                none

            Just supportingText ->
                supportingText
                    |> Ui.Theme.Typography.body2
                    |> List.singleton
                    |> paragraph
                        [ width fill
                        , paddingEach
                            { top = 0
                            , bottom = 0
                            , left = 16
                            , right = 16
                            }
                        , Font.color onSurfaceVariant
                        ]

      else
        data.help
            |> List.map Ui.Theme.Typography.body2
            |> paragraph
                [ width fill
                , paddingXY 16 0
                , Font.color error
                ]
    ]
        |> column
            [ width fill
            , if data.heightFill then
                height fill

              else
                height shrink
            , paddingEach
                { top = 8
                , bottom = 0
                , left = 0
                , right = 0
                }
            ]


viewBorder : FocusBorderData msg -> Element msg
viewBorder data =
    let
        withError =
            not (List.isEmpty data.help)
    in
    [ viewBorderLeading data withError
    , [ viewBorderNotchTop data withError
      , viewBorderSpacerForLabel data
      , viewBorderNotchBottom withError
      ]
        |> column [ height fill ]
    , viewBorderTrailing withError
    ]
        |> row
            [ height fill
            , width fill
            , pointerEvents False
            ]


viewBorderLeading : FocusBorderData msg -> Bool -> Element msg
viewBorderLeading data withError =
    let
        addClassError attrs =
            if withError then
                class "ui-atom-focus-border__border-leading--with-error" :: attrs

            else
                attrs
    in
    el
        ([ height fill
         , width (px (12 + data.paddingLeftPx))
         , Border.roundEach
            { topLeft = 4
            , topRight = 0
            , bottomRight = 0
            , bottomLeft = 4
            }
         , borderColor withError
         , class "ui-atom-focus-border__border-leading"
         ]
            |> addClassError
        )
        none


viewBorderNotchTop : FocusBorderData msg -> Bool -> Element msg
viewBorderNotchTop data withError =
    let
        addClassFilled attrs =
            if data.empty then
                attrs

            else
                class "ui-atom-focus-border__border-notch-top--filled" :: attrs

        addClassError attrs =
            if withError then
                class "ui-atom-focus-border__border-notch-top--with-error" :: attrs

            else
                attrs
    in
    el
        ([ width fill
         , borderColor withError
         , class "ui-atom-focus-border__border-notch-top"
         ]
            |> addClassFilled
            |> addClassError
        )
        none


viewBorderSpacerForLabel : FocusBorderData msg -> Element msg
viewBorderSpacerForLabel data =
    case data.label of
        Nothing ->
            none

        Just { text } ->
            text
                |> Fluent.toElement
                |> el
                    [ paddingXY 4 0
                    , Font.size 14
                    , centerY
                    ]
                |> el
                    [ height fill
                    , Element.transparent True
                    ]


viewBorderNotchBottom : Bool -> Element msg
viewBorderNotchBottom withError =
    let
        addClassError attrs =
            if withError then
                class "ui-atom-focus-border__border-notch-bottom--with-error" :: attrs

            else
                attrs
    in
    el
        ([ width fill
         , borderColor withError
         , class "ui-atom-focus-border__border-notch-bottom"
         ]
            |> addClassError
        )
        none


viewBorderTrailing : Bool -> Element msg
viewBorderTrailing withError =
    let
        addClassError attrs =
            if withError then
                class "ui-atom-focus-border__border-trailing--with-error" :: attrs

            else
                attrs
    in
    el
        ([ height fill
         , width fill
         , Border.roundEach
            { topLeft = 0
            , topRight = 4
            , bottomRight = 4
            , bottomLeft = 0
            }
         , borderColor withError
         , class "ui-atom-focus-border__border-trailing"
         ]
            |> addClassError
        )
        none


borderColor : Bool -> Element.Attribute msg
borderColor withError =
    if withError then
        Border.color error

    else
        Border.color outline


viewLabel : FocusBorderData msg -> Element msg
viewLabel data =
    let
        addClassWithIcon attrs =
            if data.withLeadingIcon then
                class "ui-atom-focus-border__label--with-icon" :: attrs

            else
                attrs

        addClassFilled attrs =
            if data.empty then
                attrs

            else
                class "ui-atom-focus-border__label--filled" :: attrs
    in
    case data.label of
        Nothing ->
            none

        Just { id, text } ->
            text
                |> Fluent.toElement
                |> el
                    ([ paddingEach
                        { top = 0
                        , bottom = 0
                        , left =
                            data.paddingLeftPx
                                + (if data.withLeadingIcon then
                                    16 + 12 + 24

                                   else
                                    16
                                  )
                        , right = 0
                        }
                     , centerY
                     , if List.isEmpty data.help then
                        Font.color onSurfaceVariant

                       else
                        Font.color error
                     , Element.id id
                     , class "ui-atom-focus-border__label"
                     , class ("ui-atom-focus-border__label--density-" ++ String.fromInt data.density)
                     ]
                        |> addClassWithIcon
                        |> addClassFilled
                    )
                |> el
                    [ height (px (56 - 4 * data.density))
                    , pointerEvents False
                    ]


viewIcons : FocusBorderData msg -> Element msg
viewIcons data =
    let
        errorIcon =
            if (not << List.isEmpty) data.help then
                viewIconError

            else
                none

        actionIconHoverable =
            Maybe.unwrap none
                (\{ icon, onClick } ->
                    viewIconActionHoverable icon onClick
                )
                data.actionIconHoverable

        actionIcon =
            Maybe.unwrap none
                (\{ icon, onClick } ->
                    viewIconAction icon onClick
                )
                data.actionIcon

        clearIcon =
            Maybe.unwrap none
                (\msg ->
                    if data.empty then
                        none

                    else
                        viewIconClear msg
                )
                data.onClear
    in
    [ errorIcon
    , actionIconHoverable
    , clearIcon
    , actionIcon
    ]
        |> List.map (el [ pointerEvents True ])
        |> row
            [ height (px (56 - 4 * data.density))
            , alignRight
            , paddingEach
                { top = 0
                , bottom = 0
                , left = data.paddingLeftPx
                , right = data.paddingRightPx
                }
            , pointerEvents False
            ]


viewIconClear : msg -> Element msg
viewIconClear msg =
    Icon.CancelOutlined
        |> buttonIcon msg
        |> ButtonIcon.excludeFromTabSequence
        |> ButtonIcon.toElement
        |> el
            [ centerY
            , paddingEach
                { top = 0
                , bottom = 0
                , left = 0
                , right = 8
                }
            , Font.color onSurfaceVariant
            ]


viewIconAction : Icon -> msg -> Element msg
viewIconAction =
    viewIconActionHelp "ui-atom-focus-border__action-icon"


viewIconActionHoverable : Icon -> msg -> Element msg
viewIconActionHoverable =
    viewIconActionHelp "ui-atom-focus-border__action-icon ui-atom-focus-border__action-icon--hoverable"


viewIconActionHelp : String -> Icon -> msg -> Element msg
viewIconActionHelp class_ icon onClick =
    icon
        |> buttonIcon onClick
        |> ButtonIcon.excludeFromTabSequence
        |> ButtonIcon.toElement
        |> el
            [ centerY
            , paddingEach
                { top = 0
                , bottom = 0
                , left = 0
                , right = 8
                }
            , Font.color onSurfaceVariant
            , class class_
            ]


viewIconError : Element msg
viewIconError =
    Icon.Error
        |> Icon.viewNormal
        |> el
            [ centerY
            , paddingEach
                { top = 0
                , bottom = 0
                , left = 0
                , right = 12
                }
            , Font.color error
            ]
