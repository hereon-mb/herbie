module Ui.Organism.FormPreview exposing (view)

import Context exposing (C)
import Element
    exposing
        ( Element
        , column
        , el
        , fill
        , paddingEach
        , paragraph
        , row
        , spacing
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Fluent
    exposing
        ( verbatim
        )
import Shacl.Form.Viewed as Form exposing (Form)
import Shacl.Report as Report
import Ui.Atom.Icon as Icon
import Ui.Theme.Color
    exposing
        ( hereonGelbContainer
        )
import Ui.Theme.Typography
    exposing
        ( body1Strong
        )


view : C -> Form -> Element Form.Msg
view c form =
    [ [ Icon.viewNormal Icon.AssignmentLate
      , verbatim "Preview of form"
            |> body1Strong
            |> List.singleton
            |> paragraph
                [ width fill
                ]
      ]
        |> row
            [ width fill
            , paddingEach
                { top = 12
                , bottom = 16
                , left = 16
                , right = 16
                }
            , spacing 16
            , Background.color hereonGelbContainer
            ]
    , form
        |> Form.view c.cShacl Report.empty
        |> el
            [ width fill
            , paddingEach
                { top = 16
                , bottom = 16
                , left = 16
                , right = 16
                }
            ]
    ]
        |> column
            [ width fill
            , Border.color hereonGelbContainer
            , Border.width 6
            , Border.rounded 12
            ]
