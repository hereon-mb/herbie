ui-organism-document-versions--draft--created-at = Created at { DATETIME($createdAt, year: "numeric", month: "numeric", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric") }
ui-organism-document-versions--draft--modified-at = Updated at { DATETIME($modifiedAt, year: "numeric", month: "numeric", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric") }
ui-organism-document-versions--heading--draft-editing = Editing draft
ui-organism-document-versions--heading--draft-initial = Initial draft
ui-organism-document-versions--heading--version-current = Current version
ui-organism-document-versions--heading--version-previous = Previous version
ui-organism-document-versions--version-current--created-at = Created at { DATETIME($createdAt, year: "numeric", month: "numeric", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric") }
ui-organism-document-versions--version-current--published-at = Published at { DATETIME($publishedAt, year: "numeric", month: "numeric", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric") }
ui-organism-document-versions--version-previous--created-at = Created at { DATETIME($createdAt, year: "numeric", month: "numeric", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric") }
ui-organism-document-versions--version-previous--published-at = Published at { DATETIME($publishedAt, year: "numeric", month: "numeric", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric") }
