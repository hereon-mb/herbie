module Ui.Organism.CardDataset.Folder exposing
    ( CardDatasetFolder, cardDatasetFolder
    , leadsToParent
    , toElement
    )

{-|

@docs CardDatasetFolder, cardDatasetFolder
@docs leadsToParent
@docs toElement

-}

import Element
    exposing
        ( Element
        , alignTop
        , el
        , fill
        , link
        , padding
        , paragraph
        , pointer
        , row
        , spacing
        , width
        )
import Element.Border as Border
import Fluent exposing (Fluent)
import Ui.Atom.Icon as Icon
import Ui.Theme.Color as Color
import Ui.Theme.Elevation as Elevation
import Ui.Theme.Typography as Typography


{-| -}
type CardDatasetFolder
    = CardDatasetFolder Data


type alias Data =
    { url : String
    , label : Fluent
    , leadsToParent : Bool
    }


{-| -}
cardDatasetFolder : String -> Fluent -> CardDatasetFolder
cardDatasetFolder url label =
    CardDatasetFolder
        { url = url
        , label = label
        , leadsToParent = False
        }


{-| Do we lead to the (logical) parent folder?
-}
leadsToParent : CardDatasetFolder -> CardDatasetFolder
leadsToParent (CardDatasetFolder data) =
    CardDatasetFolder { data | leadsToParent = True }


{-| -}
toElement : CardDatasetFolder -> Element msg
toElement (CardDatasetFolder data) =
    link [ width fill ]
        { url = data.url
        , label = viewLabel data
        }


viewLabel : Data -> Element msg
viewLabel data =
    row
        [ width fill
        , spacing 16
        , padding 16
        , pointer
        , Border.rounded 12
        , Border.dashed
        , Border.width 2
        , Border.color Color.outline
        , Elevation.z1Hover
        ]
        [ el [ alignTop ]
            (viewIcon data)
        , paragraph [ width fill ]
            [ Typography.body1Strong data.label ]
        ]


viewIcon : Data -> Element msg
viewIcon data =
    if data.leadsToParent then
        Icon.viewNormal Icon.DriveFolderUpload

    else
        Icon.viewNormal Icon.Folder
