module Ui.Organism.CardDataset.Meta exposing (Config, Data, view)

import Context exposing (C)
import Element
    exposing
        ( Element
        , column
        , el
        , fill
        , paddingEach
        , paragraph
        , spacing
        , width
        )
import Element.Extra exposing (style)
import Fluent exposing (verbatim)
import Ontology.Herbie as Herbie
import Rdf exposing (Iri)
import Shacl.Form.Localize as Localize
import Ui.Atom.Icon as Icon
import Ui.Theme.Typography exposing (body2, body2Strong, link2StoppingPropagation)
import Url.Extra as Url


type alias Config msg =
    { noOp : msg
    }


type alias Data =
    { iri : Iri
    , dctermsSource : List Iri
    , conformsTo : Maybe Herbie.DocumentVersionReference
    , hasPart : List Herbie.DocumentVersionReference
    }


view : C -> Config msg -> Data -> Element msg
view c config data =
    column
        [ width fill
        , paddingEach
            { top = 8
            , bottom = 0
            , left = 16
            , right = 16
            }
        , spacing 8
        ]
        [ column
            [ width fill
            , spacing 4
            ]
            (List.concat
                [ viewUrl c config data
                , viewDctermsSources data
                , viewConformsTo c config data
                , viewHasParts c config data
                ]
            )
        ]


viewUrl : C -> Config msg -> Data -> List (Element msg)
viewUrl c config data =
    [ paragraph
        [ width fill ]
        [ Icon.Link
            |> Icon.viewTiny
            |> el [ style "bottom" "-2px" ]
        , " IRI "
            |> verbatim
            |> body2Strong
        , viewLinkUnlabeled c config data.iri
        ]
    ]


viewDctermsSources : Data -> List (Element msg)
viewDctermsSources data =
    List.map viewDctermsSource data.dctermsSource


viewDctermsSource : Iri -> Element msg
viewDctermsSource dctermsSource =
    paragraph
        [ width fill ]
        [ Icon.Badge
            |> Icon.viewTiny
            |> el [ style "bottom" "-1px" ]
        , " Aliased to "
            |> verbatim
            |> body2Strong
        , dctermsSource
            |> Rdf.toUrl
            |> verbatim
            |> body2
        ]


viewConformsTo : C -> Config msg -> Data -> List (Element msg)
viewConformsTo c config data =
    case data.conformsTo of
        Nothing ->
            []

        Just conformsTo ->
            [ paragraph
                [ width fill ]
                [ Icon.Assignment
                    |> Icon.viewTiny
                    |> el [ style "bottom" "-2px" ]
                , " Conforms to "
                    |> verbatim
                    |> body2Strong
                , viewLink c config conformsTo
                ]
            ]


viewHasParts : C -> Config msg -> Data -> List (Element msg)
viewHasParts c config data =
    List.map (viewHasPart c config) data.hasPart


viewHasPart : C -> Config msg -> Herbie.DocumentVersionReference -> Element msg
viewHasPart c config hasPart =
    paragraph
        [ width fill ]
        [ Icon.FileOpen
            |> Icon.viewTiny
            |> el [ style "bottom" "-2px" ]
        , " Has part "
            |> verbatim
            |> body2Strong
        , viewLink c config hasPart
        ]


viewLink : C -> Config msg -> Herbie.DocumentVersionReference -> Element msg
viewLink c config reference =
    link2StoppingPropagation config.noOp
        { url = Url.setProtocol c (Rdf.toUrl reference.this)
        , label =
            verbatim
                (case reference.rdfs.label of
                    Nothing ->
                        Rdf.toUrl reference.this

                    Just label ->
                        Localize.forLabel c label
                )
        }


viewLinkUnlabeled : C -> Config msg -> Iri -> Element msg
viewLinkUnlabeled c config iri =
    link2StoppingPropagation config.noOp
        { url = Url.setProtocol c (Rdf.toUrl iri)
        , label = verbatim (Rdf.toUrl iri)
        }
