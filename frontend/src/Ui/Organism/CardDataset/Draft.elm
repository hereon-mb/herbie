module Ui.Organism.CardDataset.Draft exposing
    ( Config
    , Data
    , view
    )

import Api.Me
import Context exposing (C)
import Element
    exposing
        ( Element
        , centerY
        , column
        , el
        , fill
        , height
        , link
        , none
        , paddingEach
        , paddingXY
        , paragraph
        , pointer
        , px
        , row
        , spacing
        , width
        )
import Element.Border as Border
import Fluent exposing (verbatim)
import Ontology.Herbie as Herbie
import Rdf
import Shacl.Form.Localize as Localize
import Time.Distance
import Ui.Atom.ButtonIcon as ButtonIcon exposing (buttonIcon)
import Ui.Atom.Icon as Icon
import Ui.Organism.CardDataset.Header as Header
import Ui.Organism.CardDataset.Meta as Meta
import Ui.Theme.Card as Card exposing (card)
import Ui.Theme.Color exposing (surface)
import Ui.Theme.Typography exposing (body2, body2Strong)
import Url.Extra as Url


type alias Config msg =
    { userPressedDownload : msg
    , userPressedDelete : msg
    , noOp : msg
    }


type alias Data =
    { document : Herbie.Document
    , draft : Herbie.DocumentDraft
    }


view : C -> Config msg -> Data -> Element msg
view c config data =
    link
        [ width fill ]
        { url = Url.setProtocol c (Rdf.toUrl data.draft.this)
        , label =
            [ case
                Header.view c
                    { userPressedBadge = \_ -> config.noOp }
                    { badges = []
                    , label = data.draft.rdfs.label
                    , comment = data.draft.rdfs.comment
                    }
              of
                Nothing ->
                    []

                Just header ->
                    [ header
                    , divider
                    ]
            , [ Meta.view c
                    { noOp = config.noOp }
                    { iri = data.draft.this
                    , dctermsSource = data.draft.terms.source
                    , conformsTo = data.draft.terms.conformsTo
                    , hasPart = []
                    }
              , viewRowBottom c config data
              ]
                |> column
                    [ width fill
                    , spacing 4
                    ]
                |> List.singleton
            ]
                |> List.concat
                |> column [ width fill ]
                |> card
                |> Card.withType Card.Filled
                |> Card.withoutPadding
                |> Card.toElement
                |> el
                    [ width fill
                    , pointer
                    ]
        }


divider : Element msg
divider =
    el
        [ width fill
        , height (px 0)
        , Border.color surface
        , Border.widthEach
            { top = 1
            , bottom = 0
            , left = 0
            , right = 0
            }
        ]
        none


viewRowBottom : C -> Config msg -> Data -> Element msg
viewRowBottom c config data =
    [ [ data.document.prov.wasAttributedTo.rdfs.label
            |> Localize.verbatim c
            |> verbatim
            |> body2Strong
      , " "
            |> verbatim
            |> body2
      , Time.Distance.inWordsWithAffix data.document.terms.modified c.now
            |> body2
      , if List.isEmpty data.document.versions then
            " · new graph"
                |> verbatim
                |> body2

        else
            " · editing graph"
                |> verbatim
                |> body2
      ]
        |> paragraph
            [ width fill
            , centerY
            , paddingXY 16 0
            ]
    , [ buttonIcon config.userPressedDownload Icon.FileDownload
            |> ButtonIcon.small
            |> ButtonIcon.toElement
      , if data.document.prov.wasAttributedTo.this == Api.Me.iri c c.author then
            buttonIcon config.userPressedDelete Icon.Delete
                |> ButtonIcon.small
                |> ButtonIcon.toElement

        else
            none
      ]
        |> row
            [ spacing 2
            , paddingEach
                { top = 4
                , bottom = 4
                , left = 0
                , right = 8
                }
            ]
    ]
        |> row [ width fill ]
