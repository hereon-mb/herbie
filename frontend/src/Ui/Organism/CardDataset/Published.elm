module Ui.Organism.CardDataset.Published exposing
    ( Config
    , Data
    , view
    )

import Context exposing (C)
import Element
    exposing
        ( Element
        , alignTop
        , column
        , el
        , fill
        , link
        , none
        , paddingEach
        , paragraph
        , pointer
        , row
        , spacing
        , width
        )
import Fluent
import Maybe.Pipeline as Maybe
import Ontology.Herbie as Herbie
import Rdf
import String.Extra as String
import Time
import Ui.Atom.Icon as Icon
import Ui.Organism.CardDataset.Header as Header
import Ui.Organism.CardDataset.Meta as Meta
import Ui.Organism.CardDataset.RowBottom as RowBottom
import Ui.Theme.Card as Card exposing (card)
import Ui.Theme.Typography as Typography
import Url.Extra as Url


type alias Config msg =
    { userPressedDownload : Rdf.Iri -> msg
    , userPressedEdit : Rdf.Iri -> msg
    , noOp : msg
    }


type alias Data =
    { parent : Maybe String
    , url : String
    , document : Herbie.Document
    , version : Herbie.DocumentVersion
    , publishedAt : Time.Posix
    }


view : C -> Config msg -> Maybe String -> String -> Herbie.Document -> Maybe (Element msg)
view c config parent url document =
    Just Data
        |> Maybe.hardcoded parent
        |> Maybe.hardcoded url
        |> Maybe.hardcoded document
        |> Maybe.required (List.head document.versions)
        |> Maybe.required document.publishedAt
        |> Maybe.map (viewHelp c config)


viewHelp : C -> Config msg -> Data -> Element msg
viewHelp c config data =
    link
        [ width fill ]
        { url = Url.setProtocol c (Rdf.toUrl data.version.this)
        , label = viewLabel c config data
        }


viewLabel : C -> Config msg -> Data -> Element msg
viewLabel c config data =
    el
        [ width fill
        , pointer
        ]
        (viewCard c config data)


viewCard : C -> Config msg -> Data -> Element msg
viewCard c config data =
    viewContent c config data
        |> card
        |> Card.withType Card.Outlined
        |> Card.withoutPadding
        |> Card.toElement


viewContent : C -> Config msg -> Data -> Element msg
viewContent c config data =
    column
        [ width fill ]
        [ viewUrls data
        , viewHeader c config data
        , viewBody c config data
        ]


viewUrls : Data -> Element msg
viewUrls data =
    column
        [ width fill ]
        [ row
            [ width fill
            , paddingEach
                { top = 16
                , bottom = 0
                , left = 16
                , right = 16
                }
            , spacing 16
            ]
            [ el [ alignTop ]
                (Icon.viewNormal Icon.Description)
            , viewUrl data
            ]
        ]


viewUrl : Data -> Element msg
viewUrl data =
    case data.parent of
        Nothing ->
            paragraph
                [ width fill ]
                [ Typography.body1Strong (Fluent.verbatim data.url) ]

        Just urlParent ->
            paragraph
                [ width fill ]
                [ Typography.body1 (Fluent.verbatim urlParent)
                , Typography.body1Strong (Fluent.verbatim (String.rightOf urlParent data.url))
                ]


badgesFromVersion : Herbie.DocumentVersion -> List Header.Badge
badgesFromVersion version =
    List.filterMap identity
        [ if List.isEmpty version.ontologies then
            Nothing

          else
            Just Header.BadgeOntology
        , if List.isEmpty version.nodeShapes then
            Nothing

          else
            Just Header.BadgeSHACLDocument
        , if version.terms.conformsTo == Nothing then
            Nothing

          else
            Just Header.BadgeConforming
        , if List.isEmpty version.schema.hasPart then
            Nothing

          else
            Just Header.BadgeRoCrate
        ]


viewHeader : C -> Config msg -> Data -> Element msg
viewHeader c config data =
    case
        Header.view c
            { userPressedBadge = \_ -> config.noOp }
            { badges = badgesFromVersion data.version
            , label = data.version.rdfs.label
            , comment = data.version.rdfs.comment
            }
    of
        Nothing ->
            none

        Just header ->
            header


viewBody : C -> Config msg -> Data -> Element msg
viewBody c config data =
    column
        [ width fill
        , spacing 4
        ]
        [ Meta.view c
            { noOp = config.noOp }
            { iri = data.version.this
            , dctermsSource = data.version.terms.source
            , conformsTo = data.version.terms.conformsTo
            , hasPart = data.version.schema.hasPart
            }
        , RowBottom.view c
            { userPressedDownload = config.userPressedDownload data.document.this
            , userPressedEdit = config.userPressedEdit data.document.this
            }
            { document = data.document
            , publishedAt = data.publishedAt
            }
        ]
