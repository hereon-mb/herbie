module Ui.Organism.CardDataset.Header exposing
    ( Data, Badge(..)
    , Config, view
    )

{-|

@docs Data, Badge
@docs Config, view

-}

import Context exposing (C)
import Element
    exposing
        ( Element
        , alignRight
        , alignTop
        , centerY
        , column
        , el
        , fill
        , height
        , none
        , padding
        , paddingEach
        , paragraph
        , px
        , row
        , spacing
        , width
        , wrappedRow
        )
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Fluent exposing (Fluent, verbatim)
import Rdf exposing (StringOrLangString)
import Shacl.Form.Localize as Localize
import Ui.Atom.Icon as Icon exposing (Icon)
import Ui.Theme.Color as Color
import Ui.Theme.Typography as Typography exposing (body1, h5)


type alias Config msg =
    { userPressedBadge : Badge -> msg
    }


type Badge
    = BadgeOntology
    | BadgeSHACLDocument
    | BadgeConforming
    | BadgeRoCrate


type alias Data =
    { badges : List Badge
    , label : Maybe StringOrLangString
    , comment : Maybe StringOrLangString
    }


view : C -> Config msg -> Data -> Maybe (Element msg)
view c config data =
    case ( data.label, data.comment ) of
        ( Nothing, Nothing ) ->
            if List.isEmpty data.badges then
                Nothing

            else
                Just
                    (el
                        [ width fill
                        , padding 16
                        ]
                        (viewBadges config data)
                    )

        _ ->
            Just
                (column
                    [ width fill
                    , padding 16
                    , spacing 4
                    ]
                    [ viewTitle c config data
                    , viewComment c data
                    ]
                )


viewTitle : C -> Config msg -> Data -> Element msg
viewTitle c config data =
    wrappedRow
        [ width fill
        , spacing 8
        ]
        [ viewLabel c data
        , viewBadges config data
        ]


viewLabel : C -> Data -> Element msg
viewLabel c data =
    case data.label of
        Nothing ->
            none

        Just label ->
            paragraph
                [ width fill ]
                [ Localize.forLabel c label
                    |> verbatim
                    |> h5
                ]


viewBadges : Config msg -> Data -> Element msg
viewBadges config data =
    case data.badges of
        [] ->
            none

        badges ->
            row
                [ spacing 8
                , alignTop
                , alignRight
                ]
                (List.map (viewBadge config) badges)


viewBadge : Config msg -> Badge -> Element msg
viewBadge config badge =
    Input.button
        [ height (px 32)
        , Background.color (backgroundColorBadge badge)
        , Border.rounded 8
        ]
        { label = viewBadgeLabel badge
        , onPress = Just (config.userPressedBadge badge)
        }


viewBadgeLabel : Badge -> Element msg
viewBadgeLabel badge =
    row
        [ centerY
        , paddingEach
            { top = 0
            , bottom = 0
            , left = 8
            , right = 12
            }
        , spacing 8
        , Font.color (fontColorBadge badge)
        ]
        [ Icon.viewSmall (iconBadge badge)
        , Typography.button (labelBadge badge)
        ]


backgroundColorBadge : Badge -> Element.Color
backgroundColorBadge badge =
    case badge of
        BadgeOntology ->
            Color.hereonGelbContainer

        BadgeSHACLDocument ->
            Color.hereonOrangeContainer

        BadgeConforming ->
            Color.hereonGruenContainer

        BadgeRoCrate ->
            Color.hereonLilaContainer


fontColorBadge : Badge -> Element.Color
fontColorBadge badge =
    case badge of
        BadgeOntology ->
            Color.onHereonGelbContainer

        BadgeSHACLDocument ->
            Color.onHereonOrangeContainer

        BadgeConforming ->
            Color.onHereonGruenContainer

        BadgeRoCrate ->
            Color.onHereonLilaContainer


iconBadge : Badge -> Icon
iconBadge badge =
    case badge of
        BadgeOntology ->
            Icon.Book

        BadgeSHACLDocument ->
            Icon.Interests

        BadgeConforming ->
            Icon.Recommend

        BadgeRoCrate ->
            Icon.Inventory2


labelBadge : Badge -> Fluent
labelBadge badge =
    Fluent.verbatim
        (case badge of
            BadgeOntology ->
                "Ontology"

            BadgeSHACLDocument ->
                "SHACL"

            BadgeConforming ->
                "Conforming"

            BadgeRoCrate ->
                "RO-Crate"
        )


viewComment : C -> Data -> Element msg
viewComment c data =
    case data.comment of
        Nothing ->
            none

        Just comment ->
            paragraph
                [ width fill ]
                [ comment
                    |> Localize.verbatim c
                    |> verbatim
                    |> body1
                ]
