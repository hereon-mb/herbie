module Ui.Organism.CardDataset.RowBottom exposing (Config, Data, view)

import Api.Datasets as Datasets
import Context exposing (C)
import Element
    exposing
        ( Element
        , centerY
        , fill
        , none
        , paddingEach
        , paddingXY
        , paragraph
        , row
        , spacing
        , width
        )
import Fluent exposing (verbatim)
import Maybe.Extra as Maybe
import Ontology.Herbie as Herbie
import Shacl.Form.Localize as Localize
import Time
import Time.Distance
import Ui.Atom.ButtonIcon as ButtonIcon exposing (buttonIcon)
import Ui.Atom.Icon as Icon
import Ui.Theme.Typography exposing (body2, body2Strong)


type alias Config msg =
    { userPressedDownload : msg
    , userPressedEdit : msg
    }


type alias Data =
    { document : Herbie.Document
    , publishedAt : Time.Posix
    }


view : C -> Config msg -> Data -> Element msg
view c config data =
    let
        countVersions =
            List.length data.document.versions

        hasDraft =
            Maybe.isJust data.document.draft
    in
    [ [ data.document.prov.wasAttributedTo.rdfs.label
            |> Localize.verbatim c
            |> verbatim
            |> body2Strong
      , " "
            |> verbatim
            |> body2
      , Time.Distance.inWordsWithAffix data.publishedAt c.now
            |> body2
      , if countVersions > 1 then
            (" · edited " ++ times (countVersions - 1))
                |> verbatim
                |> body2

        else
            none
      , if hasDraft then
            " · being edited"
                |> verbatim
                |> body2

        else
            none
      ]
        |> paragraph
            [ width fill
            , centerY
            , paddingXY 16 0
            ]
    , [ buttonIcon config.userPressedDownload Icon.FileDownload
            |> ButtonIcon.small
            |> ButtonIcon.toElement
      , if Maybe.unwrap False (Datasets.canEdit c c.author data.document) c.workspace then
            buttonIcon config.userPressedEdit Icon.Edit
                |> ButtonIcon.small
                |> ButtonIcon.withEnabled (not hasDraft)
                |> ButtonIcon.toElement

        else
            none
      ]
        |> row
            [ spacing 2
            , paddingEach
                { top = 4
                , bottom = 4
                , left = 0
                , right = 8
                }
            ]
    ]
        |> row [ width fill ]


times : Int -> String
times count =
    if count == 0 then
        "never"

    else if count == 1 then
        "once"

    else if count == 2 then
        "twice"

    else
        String.fromInt count ++ " times"
