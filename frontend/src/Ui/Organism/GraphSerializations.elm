module Ui.Organism.GraphSerializations exposing (Config, Data, view)

import Element
    exposing
        ( Element
        , column
        , el
        , fill
        , height
        , padding
        , paddingXY
        , row
        , scrollbarX
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Fluent exposing (verbatim)
import Pointer
import Rdf.Format as Format exposing (Format)
import Ui.Atom.SegmentedButtonsSingleSelect exposing (segmentedButtonsSingleSelect)
import Ui.Theme.Color exposing (surface)
import Ui.Theme.Typography exposing (body2Mono)


type alias Config msg =
    { userSelectedFormatVisible : Format -> msg
    }


type alias Data =
    { turtle : String
    , nTriples : String
    , jsonLd : String
    , formatVisible : Format
    }


view : Config msg -> Data -> Element msg
view config data =
    [ { id = Pointer.field "format"
      , help = []
      , buttons =
            [ { option = Format.Turtle
              , label = verbatim "Turtle"
              }
            , { option = Format.NTriples
              , label = verbatim "N-Triples"
              }
            , { option = Format.JsonLd
              , label = verbatim "JSON-LD"
              }
            ]
      , selected = data.formatVisible
      , onChange = config.userSelectedFormatVisible
      , toString = Format.toMime
      , fromString = Format.fromMime
      }
        |> segmentedButtonsSingleSelect
        |> el
            [ width fill
            , paddingXY 16 0
            ]
    , (case data.formatVisible of
        Format.Turtle ->
            data.turtle

        Format.NTriples ->
            data.nTriples

        Format.JsonLd ->
            data.jsonLd
      )
        |> verbatim
        |> body2Mono
        |> el
            [ width fill
            , height fill
            , padding 16
            ]
        |> el
            [ width fill
            , height fill
            , scrollbarX
            , Background.color surface
            , Border.rounded 12
            ]
        |> List.singleton
        |> row
            [ width fill
            , height fill
            ]
    ]
        |> column
            [ width fill
            , height fill
            ]
