module Ui.Organism.DocumentVersions exposing (Config, Active(..), view)

{-|

@docs Config, Active, view

-}

import Context exposing (C)
import Dict exposing (Dict)
import Element
    exposing
        ( Element
        )
import Fluent
    exposing
        ( Fluent
        , posix
        , verbatim
        )
import Gen.Route as Route
import Href
import Maybe.Extra as Maybe
import Ontology.Herbie as Herbie
import Shacl.Form.Localize as Localize
import Ui.Atom.Icon as Icon
import Ui.Molecule.Listbox as Listbox


type alias Config =
    { uuid : String
    , document : Herbie.Document
    , active : Active
    , query : Dict String String
    }


type Active
    = Draft
    | VersionCurrent
    | VersionPrevious Int


view : C -> Config -> Element msg
view c config =
    [ config.document.draft
        |> Maybe.map (listboxItemDraft c config)
        |> Maybe.toList
    , config.document.versions
        |> List.head
        |> Maybe.map (listboxItemVersionCurrent c config)
        |> Maybe.toList
    , config.document.versions
        |> List.drop 1
        |> List.map (listboxItemVersionPrevious c config)
    ]
        |> List.concat
        |> Listbox.fromItems
        |> Listbox.toElement


listboxItemDraft : C -> Config -> Herbie.DocumentDraft -> Listbox.Item msg
listboxItemDraft c config draft =
    let
        href =
            { uuid = config.uuid }
                |> Route.Datasets__Uuid___Draft
                |> Href.fromRouteWith c config.query

        active =
            if config.active == Draft then
                Listbox.active

            else
                identity
    in
    headingItemDraft config.document
        |> Listbox.itemLink href
        |> Listbox.withIconBeforeText Icon.EditNote
        |> Listbox.withSecondaryLine
            [ tr [ posix "createdAt" draft.prov.generatedAtTime ] "draft--created-at"
            , tr [ posix "modifiedAt" draft.terms.modified ] "draft--modified-at"
            , draft.prov.wasAttributedTo.rdfs.label
                |> Localize.verbatim c
                |> verbatim
            ]
        |> active


headingItemDraft : Herbie.Document -> Fluent
headingItemDraft document =
    if List.isEmpty document.versions then
        t "heading--draft-initial"

    else
        t "heading--draft-editing"


listboxItemVersionCurrent : C -> Config -> Herbie.DocumentVersion -> Listbox.Item msg
listboxItemVersionCurrent c config version =
    let
        indexVersion =
            List.length config.document.versions

        href =
            { uuid = config.uuid
            , index = String.fromInt indexVersion
            }
                |> Route.Datasets__Uuid___Versions__Index_
                |> Href.fromRouteWith c config.query

        active =
            if config.active == VersionCurrent then
                Listbox.active

            else
                identity
    in
    t "heading--version-current"
        |> Listbox.itemLink href
        |> Listbox.withIconBeforeText Icon.AssignmentTurnedIn
        |> Listbox.withSecondaryLine
            [ tr [ posix "createdAt" version.createdAt ] "version-current--created-at"
            , tr [ posix "publishedAt" version.publishedAt ] "version-current--published-at"
            , version.createdBy.rdfs.label
                |> Localize.verbatim c
                |> verbatim
            ]
        |> active


listboxItemVersionPrevious : C -> Config -> Herbie.DocumentVersion -> Listbox.Item msg
listboxItemVersionPrevious c config version =
    let
        href =
            { uuid = config.uuid
            , index = String.fromInt version.index
            }
                |> Route.Datasets__Uuid___Versions__Index_
                |> Href.fromRouteWith c config.query

        active =
            if config.active == VersionPrevious version.index then
                Listbox.active

            else
                identity
    in
    t "heading--version-previous"
        |> Listbox.itemLink href
        |> Listbox.withIconBeforeText Icon.Assignment
        |> Listbox.withSecondaryLine
            [ tr [ posix "createdAt" version.createdAt ] "version-previous--created-at"
            , tr [ posix "publishedAt" version.publishedAt ] "version-previous--published-at"
            , version.createdBy.rdfs.label
                |> Localize.verbatim c
                |> verbatim
            ]
        |> active


t : String -> Fluent
t key =
    Fluent.text ("ui-organism-document-versions--" ++ key)


tr : List Fluent.Argument -> String -> Fluent
tr args key =
    Fluent.textWith args ("ui-organism-document-versions--" ++ key)
