module Ui.Organism.InstancesList exposing
    ( Config, view
    , default
    , includeAll, includeOntology, includeShacl, includeOther
    , toggleOntology, toggleShacl, toggleOther
    )

{-|

@docs Config, view
@docs default
@docs includeAll, includeOntology, includeShacl, includeOther
@docs toggleOntology, toggleShacl, toggleOther

-}

import Context exposing (C)
import Dict
import Element
    exposing
        ( Element
        )
import Fluent
    exposing
        ( verbatim
        )
import Gen.Route as Route
import Href
import Rdf exposing (Iri, StringOrLangString)
import Rdf.Graph exposing (Graph)
import Rdf.Namespaces exposing (a)
import Rdf.Namespaces.OWL as OWL
import Rdf.Namespaces.RDF as RDF
import Rdf.Namespaces.RDFS as RDFS
import Rdf.Namespaces.SH as SH
import Rdf.Query as Query
import Shacl.Form.Localize as Localize
import Ui.Atom.Icon as Icon
import Ui.Molecule.Listbox as Listbox


type alias Instance =
    { iri : Iri
    , label : Maybe StringOrLangString
    , comment : Maybe StringOrLangString
    }


type Config
    = IncludeAll
    | IncludeSome DataIncludeSome


type alias DataIncludeSome =
    { includeOntology : Bool
    , includeShacl : Bool
    , includeOther : Bool
    }


default : Config
default =
    IncludeAll


includeAll : Config -> Bool
includeAll config =
    case config of
        IncludeAll ->
            True

        IncludeSome _ ->
            False


includeOntology : Bool -> Config -> Bool
includeOntology explicit config =
    case config of
        IncludeAll ->
            if explicit then
                False

            else
                True

        IncludeSome data ->
            data.includeOntology


includeShacl : Bool -> Config -> Bool
includeShacl explicit config =
    case config of
        IncludeAll ->
            if explicit then
                False

            else
                True

        IncludeSome data ->
            data.includeShacl


includeOther : Bool -> Config -> Bool
includeOther explicit config =
    case config of
        IncludeAll ->
            if explicit then
                False

            else
                True

        IncludeSome data ->
            data.includeOther


none : DataIncludeSome
none =
    { includeOntology = False
    , includeShacl = False
    , includeOther = False
    }


toggleOntology : Config -> Config
toggleOntology config =
    case config of
        IncludeAll ->
            IncludeSome { none | includeOntology = True }

        IncludeSome data ->
            if data == { none | includeOntology = True } then
                IncludeAll

            else
                IncludeSome { data | includeOntology = not data.includeOntology }


toggleShacl : Config -> Config
toggleShacl config =
    case config of
        IncludeAll ->
            IncludeSome { none | includeShacl = True }

        IncludeSome data ->
            if data == { none | includeShacl = True } then
                IncludeAll

            else
                IncludeSome { data | includeShacl = not data.includeShacl }


toggleOther : Config -> Config
toggleOther config =
    case config of
        IncludeAll ->
            IncludeSome { none | includeOther = True }

        IncludeSome data ->
            if data == { none | includeOther = True } then
                IncludeAll

            else
                IncludeSome { data | includeOther = not data.includeOther }


view : C -> (Iri -> msg) -> Config -> Graph -> Element msg
view c userPressedCopyIri config graph =
    let
        classes =
            Query.emptyQuery
                |> Query.withPredicate a
                |> Query.getIriObjects graph

        instancesByClass =
            classes
                |> List.map
                    (\class ->
                        { class = class
                        , instances =
                            Query.emptyQuery
                                |> Query.withPredicate a
                                |> Query.withObject class
                                |> Query.getIriSubjects graph
                                |> List.map
                                    (\iri ->
                                        { iri = iri
                                        , label = RDFS.labelFor iri graph
                                        , comment = RDFS.commentFor iri graph
                                        }
                                    )
                        }
                    )
    in
    instancesByClass
        |> List.filter
            (\{ class, instances } ->
                (if
                    (class == OWL.ontology)
                        || (class == RDFS.class)
                        || (class == OWL.class)
                        || (class == RDF.property)
                        || (class == OWL.objectProperty)
                        || (class == OWL.datatypeProperty)
                 then
                    includeOntology False config

                 else if
                    (class == SH.nodeShape)
                        || (class == SH.propertyShape)
                        || (class == SH.propertyGroup)
                 then
                    includeShacl False config

                 else
                    includeOther False config
                )
                    && not (List.isEmpty instances)
            )
        |> List.map
            (\{ class, instances } ->
                instances
                    |> List.map (itemInstance c userPressedCopyIri class)
                    |> Listbox.group
                        ([ Rdf.toUrl class
                         , " ("
                         , String.fromInt (List.length instances)
                         , " instances)"
                         ]
                            |> String.concat
                            |> verbatim
                        )
            )
        |> Listbox.fromGroups
        |> Listbox.toElement


itemInstance : C -> (Iri -> msg) -> Iri -> Instance -> Listbox.Item msg
itemInstance c userPressedCopyIri class instance =
    let
        url =
            Rdf.toUrl instance.iri

        withSecondaryParagraph =
            case instance.comment of
                Nothing ->
                    identity

                Just comment ->
                    comment
                        |> Localize.verbatim c
                        |> verbatim
                        |> Listbox.withSecondaryParagraph
    in
    instance.label
        |> Maybe.map (Localize.forLabel c)
        |> Maybe.withDefault url
        |> verbatim
        |> Listbox.itemLink
            (Href.fromRouteWith c
                (Dict.fromList
                    [ ( "class", Rdf.toUrl class )
                    , ( "instance", url )
                    , ( "workspace", Maybe.withDefault "" c.workspaceUuid )
                    ]
                )
                Route.Graph
            )
        |> Listbox.withActionIconAfterText Icon.Link (userPressedCopyIri instance.iri)
        |> withSecondaryParagraph
