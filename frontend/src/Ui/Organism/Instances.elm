module Ui.Organism.Instances exposing (Config, view)

import Context exposing (C)
import Element
    exposing
        ( Element
        , alignRight
        , alignTop
        , centerY
        , column
        , el
        , fill
        , none
        , paddingXY
        , paragraph
        , row
        , spacing
        , spacingXY
        , width
        , wrappedRow
        )
import Element.Background as Background
import Element.Border as Border
import Fluent
    exposing
        ( verbatim
        )
import Rdf exposing (Iri)
import Rdf.Graph exposing (Graph)
import Ui.Atom.ButtonIcon as ButtonIcon exposing (buttonIcon)
import Ui.Atom.Chip as Chip
import Ui.Atom.Icon as Icon
import Ui.Organism.InstancesList as InstancesList
import Ui.Theme.Color
    exposing
        ( outlineVariant
        , surfaceContainer
        )
import Ui.Theme.Typography
    exposing
        ( body1Strong
        )


type alias Config msg =
    { userPressedInstancesFilterAll : msg
    , userPressedInstancesFilterOntology : msg
    , userPressedInstancesFilterShacl : msg
    , userPressedInstancesFilterOther : msg
    , userPressedCopyIri : Iri -> msg
    , userToggledExpansion : msg
    , expanded : Bool
    }


view : C -> Config msg -> InstancesList.Config -> Graph -> Element msg
view c config instancesListConfig graph =
    [ [ [ Icon.viewNormal Icon.Book
        , "Instances"
            |> verbatim
            |> body1Strong
            |> List.singleton
            |> paragraph
                [ paddingXY 0 8
                , width fill
                , centerY
                ]
        , if config.expanded then
            instancesListConfig
                |> viewInstancesFilters config

          else
            none
        ]
            |> wrappedRow
                [ width fill
                , spacingXY 16 8
                , paddingXY 0 4
                ]
      , (if config.expanded then
            Icon.ExpandLess

         else
            Icon.ExpandMore
        )
            |> buttonIcon config.userToggledExpansion
            |> ButtonIcon.toElement
            |> el
                [ alignRight
                , alignTop
                ]
      ]
        |> row
            [ width fill
            , paddingXY 16 8
            , spacing 16
            , Background.color surfaceContainer
            , if config.expanded then
                Border.roundEach
                    { topLeft = 12
                    , topRight = 12
                    , bottomLeft = 0
                    , bottomRight = 0
                    }

              else
                Border.rounded 12
            ]
    , if config.expanded then
        graph
            |> InstancesList.view c config.userPressedCopyIri instancesListConfig
            |> el
                [ width fill
                , paddingXY 0 16
                ]

      else
        none
    ]
        |> column
            [ width fill
            , Border.width 1
            , Border.color outlineVariant
            , Border.rounded 12
            ]


viewInstancesFilters : Config msg -> InstancesList.Config -> Element msg
viewInstancesFilters config instancesListConfig =
    [ Chip.view
        { iconLeft = Nothing
        , label = verbatim "All"
        , iconRight = Nothing
        , narrow = False
        , elevated = False
        , elevatedOnHover = False
        , primary = InstancesList.includeAll instancesListConfig
        , onPress = Just config.userPressedInstancesFilterAll
        , onPressRight = Nothing
        , connectedOnLeft = False
        , connectedOnRight = False
        }
    , [ Chip.view
            { iconLeft = Nothing
            , label = verbatim "Ontology"
            , iconRight = Nothing
            , narrow = False
            , elevated = False
            , elevatedOnHover = False
            , primary = InstancesList.includeOntology True instancesListConfig
            , onPress = Just config.userPressedInstancesFilterOntology
            , onPressRight = Nothing
            , connectedOnLeft = False
            , connectedOnRight = False
            }
      , Chip.view
            { iconLeft = Nothing
            , label = verbatim "SHACL"
            , iconRight = Nothing
            , narrow = False
            , elevated = False
            , elevatedOnHover = False
            , primary = InstancesList.includeShacl True instancesListConfig
            , onPress = Just config.userPressedInstancesFilterShacl
            , onPressRight = Nothing
            , connectedOnLeft = False
            , connectedOnRight = False
            }
      , Chip.view
            { iconLeft = Nothing
            , label = verbatim "Other"
            , iconRight = Nothing
            , narrow = False
            , elevated = False
            , elevatedOnHover = False
            , primary = InstancesList.includeOther True instancesListConfig
            , onPress = Just config.userPressedInstancesFilterOther
            , onPressRight = Nothing
            , connectedOnLeft = False
            , connectedOnRight = False
            }
      ]
        |> row [ spacing 8 ]
    ]
        |> row [ spacing 16 ]
