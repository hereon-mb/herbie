{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Theme.Markdown exposing (view)

import Element
    exposing
        ( Attribute
        , Element
        , alignLeft
        , alignRight
        , alignTop
        , centerX
        , column
        , el
        , fill
        , height
        , html
        , inFront
        , mouseOver
        , moveDown
        , moveRight
        , newTabLink
        , none
        , padding
        , paddingEach
        , paddingXY
        , paragraph
        , px
        , rotate
        , row
        , scrollbarX
        , spacing
        , text
        , textColumn
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Extra exposing (class)
import Element.Font as Font
import Element.Region as Region
import Html
import Html.Attributes
import Markdown.Block exposing (Block)
import Markdown.Html
import Markdown.Parser
import Markdown.Renderer
import Ui.Theme.Color
    exposing
        ( onSurface
        , outline
        , secondary
        , surface
        , surfaceContainerLow
        , withOpacity
        )


view : String -> Element msg
view =
    viewWith identity


viewWith : (List Block -> List Block) -> String -> Element msg
viewWith transform content =
    let
        deadEndsToString deadEnds =
            deadEnds
                |> List.map Markdown.Parser.deadEndToString
                |> String.join "\n"
    in
    content
        |> Markdown.Parser.parse
        |> Result.map transform
        |> Result.mapError deadEndsToString
        |> Result.andThen (Markdown.Renderer.render renderer)
        |> Result.withDefault [ text "could not parse markdown" ]
        |> textColumn
            [ spacing 16
            , width fill
            , Font.color onSurface
            ]



-- RENDER


renderer : Markdown.Renderer.Renderer (Element msg)
renderer =
    { heading = heading
    , paragraph = paragraph []
    , blockQuote = renderBlockQuote
    , html = Markdown.Html.oneOf []
    , text = text
    , codeSpan = codeSpan
    , strong = paragraph [ Font.bold ]
    , emphasis = paragraph [ Font.italic ]
    , strikethrough = paragraph [ Font.strike ]
    , hardLineBreak = html (Html.br [] [])
    , link = link
    , image = \_ -> none
    , unorderedList = unorderedList
    , orderedList = orderedList
    , codeBlock = codeBlock
    , thematicBreak = none
    , table = table
    , tableHeader = tableHeader
    , tableBody = tableBody
    , tableRow = tableRow
    , tableCell = tableCell
    , tableHeaderCell = tableHeaderCell
    }


heading :
    { level : Markdown.Block.HeadingLevel
    , rawText : String
    , children : List (Element msg)
    }
    -> Element msg
heading { level, children } =
    case level of
        Markdown.Block.H1 ->
            paragraph
                [ Region.heading 1
                , Font.size 46
                , Font.light
                , paddingEach
                    { top = 32
                    , bottom = 0
                    , left = 0
                    , right = 0
                    }
                , class "heading"
                ]
                children

        Markdown.Block.H2 ->
            paragraph
                [ Region.heading 2
                , Font.size 38
                , Font.light
                , paddingEach
                    { top = 16
                    , bottom = 0
                    , left = 0
                    , right = 0
                    }
                , class "heading"
                ]
                children

        Markdown.Block.H3 ->
            paragraph
                [ Region.heading 3
                , Font.size 30
                , Font.regular
                , paddingEach
                    { top = 16
                    , bottom = 0
                    , left = 0
                    , right = 0
                    }
                , class "heading"
                ]
                children

        Markdown.Block.H4 ->
            paragraph
                [ Region.heading 4
                , Font.size 24
                , Font.medium
                , paddingEach
                    { top = 16
                    , bottom = 0
                    , left = 0
                    , right = 0
                    }
                , class "heading"
                ]
                children

        Markdown.Block.H5 ->
            paragraph
                [ Region.heading 5
                , Font.size 22
                , Font.medium
                , paddingEach
                    { top = 8
                    , bottom = 0
                    , left = 0
                    , right = 0
                    }
                , class "heading"
                ]
                children

        Markdown.Block.H6 ->
            paragraph
                [ Region.heading 6
                , Font.size 20
                , Font.medium
                , paddingEach
                    { top = 8
                    , bottom = 0
                    , left = 0
                    , right = 0
                    }
                , class "heading"
                ]
                children


renderBlockQuote : List (Element msg) -> Element msg
renderBlockQuote children =
    paragraph
        [ Border.widthEach
            { top = 0
            , bottom = 0
            , left = 2
            , right = 0
            }
        , Border.color surfaceContainerLow
        , paddingEach
            { top = 0
            , bottom = 0
            , left = 16
            , right = 0
            }
        ]
        children


link :
    { title : Maybe String
    , destination : String
    }
    -> List (Element msg)
    -> Element msg
link { destination } children =
    let
        chooseLinkType =
            if String.startsWith "http" destination then
                newTabLink

            else
                Element.link
    in
    chooseLinkType
        [ class "underline__hover"
        , class "underline__focus"
        , class "underline__pressed"
        , Font.color secondary
        ]
        { url = destination
        , label = paragraph [] children
        }



-- CODE


codeSpan : String -> Element msg
codeSpan code =
    el
        [ Background.color surfaceContainerLow
        , Border.rounded 4
        , Font.family
            [ Font.typeface "Source Code Pro" ]
        ]
        (text code)


codeBlock : { body : String, language : Maybe String } -> Element msg
codeBlock { body } =
    el
        [ width fill
        , scrollbarX
        , Background.color surfaceContainerLow
        , Border.rounded 4
        , Font.family
            [ Font.typeface "Source Code Pro" ]
        ]
        (el
            [ padding 16 ]
            (html
                (Html.div
                    [ Html.Attributes.style "white-space" "pre"
                    , Html.Attributes.style "line-height" "1.3"
                    ]
                    [ Html.text body ]
                )
            )
        )



-- LISTS


unorderedList : List (Markdown.Block.ListItem (Element msg)) -> Element msg
unorderedList listItems =
    column
        [ spacing 8
        , paddingEach
            { top = 0
            , bottom = 0
            , left = 16
            , right = 0
            }
        ]
        (List.map renderUnorderedListItem listItems)


renderUnorderedListItem : Markdown.Block.ListItem (Element msg) -> Element msg
renderUnorderedListItem (Markdown.Block.ListItem task children) =
    row []
        [ case task of
            Markdown.Block.NoTask ->
                el
                    [ alignTop
                    , width (px 24)
                    ]
                    (text "•")

            Markdown.Block.IncompleteTask ->
                el
                    [ alignTop
                    , width (px 24)
                    , moveDown 2
                    ]
                    (el
                        [ Border.width 2
                        , Border.rounded 2
                        , Border.color outline
                        , width (px 18)
                        , height (px 18)
                        ]
                        none
                    )

            Markdown.Block.CompletedTask ->
                el
                    [ alignTop
                    , width (px 24)
                    , moveDown 2
                    ]
                    (el
                        [ Border.width 2
                        , Border.rounded 2
                        , Border.color secondary
                        , Background.color secondary
                        , width (px 18)
                        , height (px 18)
                        , inFront <|
                            el
                                [ width (px 13)
                                , height (px 7)
                                , rotate (pi / -3.8)
                                , moveDown 1.5
                                , moveRight 0.5
                                , Border.widthEach
                                    { top = 0
                                    , bottom = 2
                                    , left = 2
                                    , right = 0
                                    }
                                , Border.color surface
                                ]
                                none
                        ]
                        none
                    )
        , paragraph [] children
        ]


orderedList : Int -> List (List (Element msg)) -> Element msg
orderedList startingIndex children =
    column
        [ spacing 8
        , paddingEach
            { top = 0
            , bottom = 0
            , left = 16
            , right = 0
            }
        ]
        (List.indexedMap (renderOrderedListItem startingIndex) children)


renderOrderedListItem : Int -> Int -> List (Element msg) -> Element msg
renderOrderedListItem startingIndex index children =
    let
        ordinal num =
            String.fromInt num ++ "."
    in
    row []
        [ el
            [ alignTop
            , width (px 24)
            ]
            (text (ordinal (index + startingIndex)))
        , paragraph [] children
        ]



-- TABLES


table : List (Element msg) -> Element msg
table children =
    el
        [ paddingXY 0 16 ]
        (column
            [ width fill
            , Border.width 1
            , Border.color outline
            , Border.rounded 4
            ]
            children
        )


tableHeader : List (Element msg) -> Element msg
tableHeader children =
    column
        [ width fill
        , Border.widthEach
            { top = 0
            , bottom = 1
            , left = 0
            , right = 0
            }
        , Border.color outline
        ]
        children


tableBody : List (Element msg) -> Element msg
tableBody children =
    column
        [ width fill
        ]
        children


tableRow : List (Element msg) -> Element msg
tableRow children =
    row
        [ width fill
        , spacing 32
        , paddingXY 16 16
        , class "table-row"
        , Border.color outline
        , mouseOver
            [ onSurface
                |> withOpacity 0.08
                |> Background.color
            ]
        ]
        children


tableCell : Maybe Markdown.Block.Alignment -> List (Element msg) -> Element msg
tableCell maybeAlignment children =
    el
        [ width fill ]
        (el (alignmentAttr maybeAlignment [])
            (paragraph (alignmentAttr maybeAlignment []) children)
        )


tableHeaderCell :
    Maybe Markdown.Block.Alignment
    -> List (Element msg)
    -> Element msg
tableHeaderCell maybeAlignment children =
    el
        [ width fill ]
        (el (alignmentAttr maybeAlignment [])
            (paragraph (alignmentAttr maybeAlignment [ Font.medium ]) children)
        )


alignmentAttr :
    Maybe Markdown.Block.Alignment
    -> List (Attribute msg)
    -> List (Attribute msg)
alignmentAttr maybeAlignment attrs =
    case maybeAlignment of
        Nothing ->
            attrs

        Just Markdown.Block.AlignLeft ->
            alignLeft :: attrs

        Just Markdown.Block.AlignRight ->
            alignRight :: attrs

        Just Markdown.Block.AlignCenter ->
            centerX :: attrs
