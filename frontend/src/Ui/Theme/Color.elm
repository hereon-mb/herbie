{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Theme.Color exposing
    ( transparent
    , withOpacity
    , primary, onPrimary, primaryContainer, onPrimaryContainer
    , secondary, onSecondary, secondaryContainer, onSecondaryContainer
    , hereonLilaContainer, onHereonLilaContainer
    , hereonOrangeContainer, onHereonOrangeContainer
    , hereonGruenContainer, onHereonGruenContainer
    , hereonGelbContainer, onHereonGelbContainer
    , error, onError, errorContainer, onErrorContainer
    , surface, onSurface, surfaceVariant, onSurfaceVariant
    , surfaceContainerLowest, surfaceContainerLow, surfaceContainer, surfaceContainerHigh, surfaceContainerHighest
    , inverseSurface, inverseOnSurface, inversePrimary
    , outline, outlineVariant
    , surfaceBright, surfaceDim
    , scrim
    , focused, focusedWith, hovered, hoveredWith, overlayFocused, overlayHovered, overlayPressed, pressed, pressedWith, primarySurfaceContainerHighest
    )

{-| <http://paletton.com/#uid=33x0u0k++obZBKX+WtZ+YiL+Nco>

@docs transparent
@docs withOpacity


# Primary colors

@docs primary, onPrimary, primaryContainer, onPrimaryContainer


# Secondary colors

@docs secondary, onSecondary, secondaryContainer, onSecondaryContainer


# Tertiary colors

@docs hereonLilaContainer, onHereonLilaContainer
@docs hereonOrangeContainer, onHereonOrangeContainer
@docs hereonGruenContainer, onHereonGruenContainer
@docs hereonGelbContainer, onHereonGelbContainer

TODO


# Error colors

@docs error, onError, errorContainer, onErrorContainer


# Surface colors

@docs surface, onSurface, surfaceVariant, onSurfaceVariant
@docs surfaceContainerLowest, surfaceContainerLow, surfaceContainer, surfaceContainerHigh, surfaceContainerHighest
@docs inverseSurface, inverseOnSurface, inversePrimary
@docs surfaceTint, surfaceTintColor


# Outline colors

@docs outline, outlineVariant


# Add-ons


## Add-on surface colors

@docs background, onBackground
@docs surfaceBright, surfaceDim
@docs scrim, shadow

-}

import Color
import Color.Blending
import Element
    exposing
        ( Color
        , fromRgb
        , rgba
        , toRgb
        )
import Ui.Theme.Light exposing (light)



-- TONES


primary : Color
primary =
    light.primary


onPrimary : Color
onPrimary =
    light.onPrimary


primaryContainer : Color
primaryContainer =
    light.primaryContainer


onPrimaryContainer : Color
onPrimaryContainer =
    light.onPrimaryContainer


secondary : Color
secondary =
    light.secondary


onSecondary : Color
onSecondary =
    light.onSecondary


secondaryContainer : Color
secondaryContainer =
    light.secondaryContainer


onSecondaryContainer : Color
onSecondaryContainer =
    light.onSecondaryContainer


hereonLilaContainer : Color
hereonLilaContainer =
    light.hereonLilaContainer


onHereonLilaContainer : Color
onHereonLilaContainer =
    light.onHereonLilaContainer


hereonOrangeContainer : Color
hereonOrangeContainer =
    light.hereonOrangeContainer


onHereonOrangeContainer : Color
onHereonOrangeContainer =
    light.onHereonOrangeContainer


hereonGruenContainer : Color
hereonGruenContainer =
    light.hereonGruenContainer


onHereonGruenContainer : Color
onHereonGruenContainer =
    light.onHereonGruenContainer


hereonGelbContainer : Color
hereonGelbContainer =
    light.hereonGelbContainer


onHereonGelbContainer : Color
onHereonGelbContainer =
    light.onHereonGelbContainer


surfaceDim : Color
surfaceDim =
    light.surfaceDim


surface : Color
surface =
    light.surface


surfaceBright : Color
surfaceBright =
    light.surfaceBright


surfaceContainerLowest : Color
surfaceContainerLowest =
    light.surfaceContainerLowest


surfaceContainerLow : Color
surfaceContainerLow =
    light.surfaceContainerLow


surfaceContainer : Color
surfaceContainer =
    light.surfaceContainer


surfaceContainerHigh : Color
surfaceContainerHigh =
    light.surfaceContainerHigh


surfaceContainerHighest : Color
surfaceContainerHighest =
    light.surfaceContainerHighest


primarySurfaceContainerHighest : Color
primarySurfaceContainerHighest =
    light.primarySurfaceContainerHighest


onSurface : Color
onSurface =
    light.onSurface


onSurfaceVariant : Color
onSurfaceVariant =
    light.onSurfaceVariant


outline : Color
outline =
    light.outline


outlineVariant : Color
outlineVariant =
    light.outlineVariant


inverseSurface : Color
inverseSurface =
    light.inverseSurface


inverseOnSurface : Color
inverseOnSurface =
    light.inverseOnSurface


inversePrimary : Color
inversePrimary =
    light.inversePrimary


error : Color
error =
    light.error


onError : Color
onError =
    light.onError


errorContainer : Color
errorContainer =
    light.errorContainer


onErrorContainer : Color
onErrorContainer =
    light.onErrorContainer


surfaceVariant : Color
surfaceVariant =
    light.surface



-- INTERACTION STATES


hoveredWith : Color -> Color -> Color
hoveredWith content container =
    tint content 0.08 container


focusedWith : Color -> Color -> Color
focusedWith content container =
    tint content 0.12 container


pressedWith : Color -> Color -> Color
pressedWith content container =
    tint content 0.12 container



-- TONE
-- OPACITY


withOpacity : Float -> Color -> Color
withOpacity opacity color =
    setAlpha opacity color



-- TINT


tint : Color -> Float -> Color -> Color
tint colorOverlay amount color =
    Color.Blending.overlay
        (colorOverlay
            |> setAlpha amount
            |> Element.toRgb
            |> Color.fromRgba
        )
        (color
            |> Element.toRgb
            |> Color.fromRgba
        )
        |> Color.toRgba
        |> Element.fromRgb



-- LEGACY


hovered : Color -> Color
hovered =
    overlay 0.04


focused : Color -> Color
focused =
    overlay 0.08


pressed : Color -> Color
pressed =
    overlay 0.12


overlay : Float -> Color -> Color
overlay amount color =
    let
        rgba =
            Element.toRgb color
    in
    Element.fromRgb <|
        Color.toRgba <|
            Color.Blending.hardlight
                (Color.fromRgba rgba)
                (Color.rgba 1 1 1 amount)


overlayHovered : Color -> Color
overlayHovered =
    setAlpha 0.04


overlayFocused : Color -> Color
overlayFocused =
    setAlpha 0.08


overlayPressed : Color -> Color
overlayPressed =
    setAlpha 0.12


setAlpha : Float -> Color -> Color
setAlpha alpha color_ =
    let
        rgb =
            toRgb color_
    in
    fromRgb
        { rgb | alpha = alpha }


transparent : Color
transparent =
    rgba 0 0 0 0


scrim : Color
scrim =
    rgba 0 0 0 0.64
