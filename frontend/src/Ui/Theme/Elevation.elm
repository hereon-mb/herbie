{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Theme.Elevation exposing
    ( z1
    , z12
    , z12Pressed
    , z16
    , z1Hover
    , z2
    , z24
    , z2Focus
    , z2Hover
    , z4
    , z4Focus
    , z4Hover
    , z6
    , z8
    , z8Focus
    , z8Hover
    , z8Pressed
    )

import Element exposing (Attribute)
import Element.Extra exposing (class)


z1 : Attribute msg
z1 =
    class "mdc-elevation--z1"


z1Hover : Attribute msg
z1Hover =
    class "mdc-elevation--z1__hover"


z2 : Attribute msg
z2 =
    class "mdc-elevation--z2"


z2Hover : Attribute msg
z2Hover =
    class "mdc-elevation--z2__hover"


z2Focus : Attribute msg
z2Focus =
    class "mdc-elevation--z2__focus"


z4 : Attribute msg
z4 =
    class "mdc-elevation--z4"


z4Hover : Attribute msg
z4Hover =
    class "mdc-elevation--z4__hover"


z4Focus : Attribute msg
z4Focus =
    class "mdc-elevation--z4__focus"


z6 : Attribute msg
z6 =
    class "mdc-elevation--z6"


z8 : Attribute msg
z8 =
    class "mdc-elevation--z8"


z8Hover : Attribute msg
z8Hover =
    class "mdc-elevation--z8__hover"


z8Focus : Attribute msg
z8Focus =
    class "mdc-elevation--z8__focus"


z8Pressed : Attribute msg
z8Pressed =
    class "mdc-elevation--z8__pressed"


z12 : Attribute msg
z12 =
    class "mdc-elevation--z12"


z12Pressed : Attribute msg
z12Pressed =
    class "mdc-elevation--z12__pressed"


z16 : Attribute msg
z16 =
    class "mdc-elevation--z16"


z24 : Attribute msg
z24 =
    class "mdc-elevation--z24"
