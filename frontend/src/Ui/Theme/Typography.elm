{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Theme.Typography exposing
    ( h1, h2, h3, h4, h5, h6
    , subtitle1, subtitle2
    , body1, body1Strong, body1Mono, body1MonoStrong, body2, body2Strong, body2Mono
    , LinkConfig, link1, newTabLink1, download1, link2, newTabLink2, link2StoppingPropagation
    , button
    , caption
    , overline
    )

{-|

@docs h1, h2, h3, h4, h5, h6
@docs subtitle1, subtitle2
@docs body1, body1Strong, body1Mono, body1MonoStrong, body2, body2Strong, body2Mono
@docs LinkConfig, link1, newTabLink1, download1, link2, newTabLink2, link2StoppingPropagation
@docs button
@docs caption
@docs overline

-}

import Element
    exposing
        ( Element
        , download
        , el
        , link
        , newTabLink
        , paragraph
        , row
        , spacing
        )
import Element.Extra exposing (class, stopPropagationOnClick)
import Element.Font as Font
import Element.Region as Region
import Fluent
    exposing
        ( Fluent
        , toElement
        )
import Ui.Atom.Icon as Icon
import Ui.Theme.Color
    exposing
        ( primary
        )


h1 : Fluent -> Element msg
h1 content =
    paragraph
        [ Region.heading 1
        , Font.size 96
        , Font.light
        ]
        [ toElement content ]


h2 : Fluent -> Element msg
h2 content =
    paragraph
        [ Region.heading 2
        , Font.size 60
        , Font.light
        ]
        [ toElement content ]


h3 : Fluent -> Element msg
h3 content =
    paragraph
        [ Region.heading 3
        , Font.size 48
        , Font.light
        ]
        [ toElement content ]


h4 : Fluent -> Element msg
h4 content =
    paragraph
        [ Region.heading 4
        , Font.size 34
        , Font.regular
        ]
        [ toElement content ]


h5 : Fluent -> Element msg
h5 content =
    paragraph
        [ Region.heading 5
        , Font.size 24
        , Font.medium
        ]
        [ toElement content ]


h6 : Fluent -> Element msg
h6 content =
    paragraph
        [ Region.heading 6
        , Font.size 20
        , Font.medium
        ]
        [ toElement content ]


subtitle1 : Fluent -> Element msg
subtitle1 content =
    paragraph
        [ Region.heading 7
        , Font.size 16
        , Font.regular
        ]
        [ toElement content ]


subtitle2 : Fluent -> Element msg
subtitle2 content =
    paragraph
        [ Region.heading 8
        , Font.size 14
        , Font.medium
        ]
        [ toElement content ]


body1 : Fluent -> Element msg
body1 content =
    el
        [ Font.size 18
        , Font.regular
        ]
        (toElement content)


body1Strong : Fluent -> Element msg
body1Strong content =
    el
        [ Font.size 18
        , Font.medium
        ]
        (toElement content)


body1Mono : Fluent -> Element msg
body1Mono content =
    el
        [ Font.size 18
        , Font.regular
        , Font.family
            [ Font.typeface "Source Code Pro" ]
        ]
        (toElement content)


body1MonoStrong : Fluent -> Element msg
body1MonoStrong content =
    el
        [ Font.size 18
        , Font.bold
        , Font.family
            [ Font.typeface "Source Code Pro" ]
        ]
        (toElement content)


body2 : Fluent -> Element msg
body2 content =
    el
        [ Font.size 14
        , Font.regular
        ]
        (toElement content)


body2Mono : Fluent -> Element msg
body2Mono content =
    el
        [ Font.size 14
        , Font.regular
        , Font.family
            [ Font.typeface "Source Code Pro" ]
        ]
        (toElement content)


body2Strong : Fluent -> Element msg
body2Strong content =
    el
        [ Font.size 14
        , Font.medium
        ]
        (toElement content)


button : Fluent -> Element msg
button content =
    el
        [ Font.size 14
        , Font.medium
        ]
        (toElement content)


caption : Fluent -> Element msg
caption content =
    el
        [ Font.size 14
        , Font.regular
        ]
        (toElement content)


overline : Fluent -> Element msg
overline content =
    el
        [ Font.size 14
        , Font.semiBold
        , Font.variant Font.smallCaps
        ]
        (toElement content)


type alias LinkConfig =
    { url : String
    , label : Fluent
    }


link1 : LinkConfig -> Element msg
link1 config =
    link
        [ Font.color primary
        , Font.size 18
        , class "underline__pressed"
        , class "underline__hover"
        , class "underline__focus"
        ]
        { url = config.url
        , label = toElement config.label
        }


newTabLink1 : LinkConfig -> Element msg
newTabLink1 config =
    newTabLink
        [ Font.color primary
        , Font.size 18
        , class "underline__pressed"
        , class "underline__hover"
        , class "underline__focus"
        ]
        { url = config.url
        , label = toElement config.label
        }


download1 : LinkConfig -> Element msg
download1 config =
    download
        [ Font.color primary
        , Font.size 18
        , class "underline__pressed"
        , class "underline__hover"
        , class "underline__focus"
        ]
        { url = config.url
        , label =
            [ toElement config.label
            , Icon.FileDownload
                |> Icon.viewSmall
            ]
                |> row [ spacing 8 ]
        }


link2 : LinkConfig -> Element msg
link2 config =
    link
        [ Font.color primary
        , Font.size 14
        , class "underline__pressed"
        , class "underline__hover"
        , class "underline__focus"
        ]
        { url = config.url
        , label = toElement config.label
        }


newTabLink2 : LinkConfig -> Element msg
newTabLink2 config =
    newTabLink
        [ Font.color primary
        , Font.size 14
        , class "underline__pressed"
        , class "underline__hover"
        , class "underline__focus"
        ]
        { url = config.url
        , label = toElement config.label
        }


link2StoppingPropagation : msg -> LinkConfig -> Element msg
link2StoppingPropagation noOp config =
    link
        [ Font.color primary
        , Font.size 14
        , class "underline__pressed"
        , class "underline__hover"
        , class "underline__focus"
        , stopPropagationOnClick noOp
        ]
        { url = config.url
        , label = toElement config.label
        }
