{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Theme.Input exposing
    ( attributesText, withFocusBorder
    , label
    , help, helpAbove
    , checkboxDefault
    )

{-|

@docs attributesText, withFocusBorder

@docs label

@docs help, helpAbove

@docs checkboxDefault

-}

import Element
    exposing
        ( Attribute
        , Element
        , alignTop
        , centerX
        , centerY
        , column
        , el
        , fill
        , focused
        , height
        , minimum
        , mouseDown
        , mouseOver
        , none
        , paddingXY
        , paragraph
        , px
        , row
        , spacing
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Extra
    exposing
        ( class
        , id
        , style
        , tabindex
        )
import Element.Font as Font
import Fluent exposing (Fluent)
import Ui.Atom.Icon as Icon
import Ui.Theme.Color
    exposing
        ( error
        , onSurface
        , onSurfaceVariant
        , outline
        , primary
        , transparent
        , withOpacity
        )
import Ui.Theme.Typography



---- ATTRIBUTES


attributesText : List (Attribute msg)
attributesText =
    [ width (minimum 32 fill)
    , Font.size 18
    , style "letter-spacing" "0.15px"
    , style "line-height" "24px"
    , Background.color transparent
    ]


withFocusBorder : Bool -> List (Attribute msg) -> List (Attribute msg)
withFocusBorder hasHelp attrs =
    if hasHelp then
        [ Border.rounded 4
        , Border.width 1
        , Border.color error
        , focused [ Border.color error ]
        , class "ui-theme-input--focus-border-outline--with-errors"
        ]
            ++ attrs

    else
        [ Border.rounded 4
        , Border.width 1
        , Border.color outline
        , focused [ Border.color primary ]
        , mouseOver [ Border.color outline ]
        , class "ui-theme-input--focus-border-outline"
        ]
            ++ attrs



---- ELEMENTS


label :
    { id : Maybe String
    , text : Fluent
    , help : List Fluent
    }
    -> Element msg
label config =
    let
        withId attrs =
            case config.id of
                Nothing ->
                    attrs

                Just theId ->
                    tabindex -1 :: id theId :: attrs
    in
    column
        ([ width fill
         , paddingXY 16 6
         , spacing 8
         ]
            |> withId
        )
        [ el
            [ Font.color onSurfaceVariant ]
            (Ui.Theme.Typography.caption config.text)
        , help config.help
        ]


help : List Fluent -> Element msg
help texts =
    case texts of
        [] ->
            none

        _ ->
            row
                [ width fill
                , spacing 12
                , Font.color error
                ]
                [ Icon.Error
                    |> Icon.viewNormal
                    |> el [ alignTop ]
                , texts
                    |> List.map Ui.Theme.Typography.body1
                    |> paragraph
                        [ width fill
                        , alignTop
                        ]
                ]


helpAbove :
    { help : List Fluent
    , input : Element msg
    }
    -> Element msg
helpAbove config =
    column
        [ width fill
        , spacing 12
        ]
        [ case config.help of
            [] ->
                none

            _ ->
                el
                    [ paddingXY 16 0 ]
                    (help config.help)
        , config.input
        ]


checkboxDefault : Bool -> Element msg
checkboxDefault selected =
    let
        icon =
            if selected then
                Icon.CheckBox

            else
                Icon.CheckBoxOutlineBlank

        colorIcon =
            if selected then
                primary

            else
                onSurface

        colorStateLayer =
            if selected then
                primary

            else
                onSurface
    in
    icon
        |> Icon.viewSmall
        |> el
            [ centerX
            , centerY
            , Font.color colorIcon
            ]
        |> el
            [ width (px 36)
            , height (px 36)
            , Border.rounded 18
            , mouseOver
                [ colorStateLayer
                    |> withOpacity 0.08
                    |> Background.color
                ]
            , focused
                [ colorStateLayer
                    |> withOpacity 0.1
                    |> Background.color
                ]
            , mouseDown
                [ colorStateLayer
                    |> withOpacity 0.1
                    |> Background.color
                ]
            ]
