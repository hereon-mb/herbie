{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ui.Theme.Card exposing
    ( Card, card, toElement
    , CardType(..)
    , withoutPadding
    , withType
    )

{-|

@docs Card, card, toElement
@docs withCardType, CardType
@docs withoutPadding

-}

import Element
    exposing
        ( Element
        , fill
        , mouseOver
        , padding
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Extra exposing (elWithAncestorWithScrollbarX)
import Ui.Theme.Color as Color
    exposing
        ( onSurface
        , outline
        , surface
        , surfaceContainerHighest
        , surfaceContainerLow
        )
import Ui.Theme.Elevation as Elevation


type Card msg
    = Card (Data msg)


type alias Data msg =
    { padding : Int
    , cardType : CardType
    , content : Element msg
    , joinBelow : Bool
    , joinAbove : Bool
    }


type CardType
    = Elevated
    | Filled
    | Outlined


card : Element msg -> Card msg
card content =
    Card
        { padding = 16
        , cardType = Filled
        , content = content
        , joinBelow = False
        , joinAbove = False
        }


withoutPadding : Card msg -> Card msg
withoutPadding (Card data) =
    Card { data | padding = 0 }


withType : CardType -> Card msg -> Card msg
withType cardType (Card data) =
    Card { data | cardType = cardType }


toElement : Card msg -> Element msg
toElement (Card data) =
    let
        rounded =
            { topLeft =
                if data.joinAbove then
                    0

                else
                    12
            , topRight =
                if data.joinAbove then
                    0

                else
                    12
            , bottomLeft =
                if data.joinBelow then
                    0

                else
                    12
            , bottomRight =
                if data.joinBelow then
                    0

                else
                    12
            }
    in
    case data.cardType of
        Elevated ->
            elWithAncestorWithScrollbarX
                [ width fill
                , padding data.padding
                , Border.roundEach rounded
                , Background.color surfaceContainerLow
                , Elevation.z1
                , Elevation.z2Hover
                ]
                data.content

        Filled ->
            elWithAncestorWithScrollbarX
                [ width fill
                , padding data.padding
                , Border.roundEach rounded
                , Background.color surfaceContainerHighest
                , Elevation.z1Hover
                ]
                data.content

        Outlined ->
            elWithAncestorWithScrollbarX
                [ width fill
                , padding data.padding
                , Border.width 1
                , Border.roundEach rounded
                , Border.color outline
                , Background.color surface
                , Elevation.z1Hover
                , mouseOver
                    [ Background.color (Color.overlayHovered onSurface) ]
                ]
                data.content
