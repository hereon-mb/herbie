-casting-type--permanent-mould-casting = Permanent mould casting
-casting-type--permanent-mould-direct-chill-casting = Permanent mould direct chill casting
-casting-type--sand-casting = Sand casting
-casting-type--tp1 = TP-1
-casting-type--vacuum-casting = Vacuum casting
-grain-refinement =
    { $capitalization ->
       *[false] grain refinement
        [true] Grain refinement
    }
-protocol--clerks =
    { $capitalization ->
       *[false] clerks
        [true] Clerks
    }
-protocol--clients =
    { $capitalization ->
       *[false] clients
        [true] Clients
    }
-protocol--external-partner =
    { $capitalization ->
       *[false] external partner
        [true] External partner
    }
