module Query exposing
    ( scoreFor
    , sortByScore
    )

{-|

@docs scoreFor
@docs sortByScore

-}

import Simple.Fuzzy as Fuzzy


{-| TODO
-}
scoreFor : List (a -> Maybe String) -> String -> a -> Int
scoreFor accessors query a =
    if String.trim query == "" then
        0

    else
        let
            words : List String
            words =
                query
                    |> String.toLower
                    |> String.words

            fields : List (List String)
            fields =
                List.filterMap toField accessors

            toField : (a -> Maybe String) -> Maybe (List String)
            toField accessor =
                a
                    |> accessor
                    |> Maybe.map (String.toLower >> String.words)
        in
        words
            |> List.map (scoreForWord fields)
            |> List.sum
            |> floor


scoreForWord : List (List String) -> String -> Float
scoreForWord fields word =
    let
        scoreForField wordsField =
            wordsField
                |> List.map
                    (\wordField ->
                        if String.startsWith word wordField then
                            3

                        else if String.contains word wordField then
                            1

                        else if Fuzzy.match word wordField then
                            0.5

                        else
                            0
                    )
                |> List.sum
    in
    fields
        |> List.map scoreForField
        |> List.sum


{-| Filter out non matching elements and sort resulting list by score. Returns
the original list if the query is empty.
-}
sortByScore : List (a -> Maybe String) -> String -> List a -> List a
sortByScore fields query listA =
    if String.trim query == "" then
        listA

    else
        let
            addScore a =
                ( scoreFor fields query a
                , a
                )

            negativeScore ( score, _ ) =
                -1 * score

            scoreIsPositive ( score, _ ) =
                score > 0

            dropScore =
                Tuple.second
        in
        listA
            |> List.map addScore
            |> List.sortBy negativeScore
            |> List.filter scoreIsPositive
            |> List.map dropScore
