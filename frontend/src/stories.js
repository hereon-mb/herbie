// Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
// a client-server based web application for research documentation
//
// Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
//
// Authors: Fabian Kirchner, Catriona Eschke
//
// This program is subject to the terms and conditions for non-commercial use of
// ELN. You can find the license text in the file LICENSE.en and under
// http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
// you have any questions or comments, you can contact us at hereon at
// herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
// Max-Planck-Straße 1, 21502 Geesthacht, Germany.

import { Elm } from "./Stories.elm";
import { getBundles } from "./fluent.js";
import { registerCustomElements } from "./custom-elements.js";

var SETTINGS_KEY = "bulletproof_settings";

var app = Elm.Stories.init({
  flags: localStorage.getItem(SETTINGS_KEY),
});

app.ports.saveSettings.subscribe(
  settings => localStorage.setItem(SETTINGS_KEY, settings)
);

window.requestAnimationFrame(function() {
  getBundles().then((fluentBundles) => {
    registerCustomElements();
    document.getElementsByTagName("fluent-provider")[0].bundles = fluentBundles;
  });
});
