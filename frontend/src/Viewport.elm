{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Viewport exposing
    ( Viewport
    , get
    , init
    , onResize
    )

import Browser.Dom
import Browser.Events
import Task


type alias Viewport =
    { width : Int
    , height : Int
    }


init : Viewport
init =
    { width = 0
    , height = 0
    }


get : (Result Browser.Dom.Error Viewport -> msg) -> Cmd msg
get toMsg =
    let
        toViewport { viewport } =
            { width = floor viewport.width
            , height = floor viewport.height
            }
    in
    Browser.Dom.getViewportOf "main-content"
        |> Task.map toViewport
        |> Task.attempt toMsg


onResize : msg -> Sub msg
onResize toMsg =
    Browser.Events.onResize (\_ _ -> toMsg)
