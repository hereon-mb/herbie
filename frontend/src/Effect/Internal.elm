{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Effect.Internal exposing
    ( Effect, none, map, batch
    , fromCmd, fromAction, fromMsg
    , toCmd
    )

{-|

@docs Effect, none, map, batch
@docs fromCmd, fromAction, fromMsg
@docs toCmd

-}

import Task


type Effect action msg
    = None
    | Cmd (Cmd msg)
    | Action action
    | Msg msg
    | Batch (List (Effect action msg))


none : Effect action msg
none =
    None


map : (a -> b) -> (actionA -> actionB) -> Effect actionA a -> Effect actionB b
map fn fnAction effect =
    case effect of
        None ->
            None

        Cmd cmd ->
            Cmd (Cmd.map fn cmd)

        Action action ->
            Action (fnAction action)

        Msg msg ->
            Msg (fn msg)

        Batch list ->
            Batch (List.map (map fn fnAction) list)


fromCmd : Cmd msg -> Effect action msg
fromCmd =
    Cmd


fromAction : action -> Effect action msg
fromAction =
    Action


fromMsg : msg -> Effect action msg
fromMsg =
    Msg


batch : List (Effect action msg) -> Effect action msg
batch =
    Batch



-- Used by Main.elm


toCmd : (action -> msg) -> Effect action msg -> Cmd msg
toCmd msgFromAction effect =
    case effect of
        None ->
            Cmd.none

        Cmd cmd ->
            cmd

        Action action ->
            action
                |> msgFromAction
                |> Task.succeed
                |> Task.perform identity

        Msg msg ->
            msg
                |> Task.succeed
                |> Task.perform identity

        Batch list ->
            Cmd.batch (List.map (toCmd msgFromAction) list)
