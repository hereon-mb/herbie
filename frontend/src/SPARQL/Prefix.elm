module SPARQL.Prefix exposing
    ( Prefix
    , prefix
    , toString, compare
    )

{-|

@docs Prefix

@docs prefix

@docs toString, compare

-}

import Rdf exposing (Iri)


type Prefix
    = Prefix String Iri


prefix : String -> Iri -> Prefix
prefix name iri =
    Prefix name iri


toString : Prefix -> String
toString (Prefix name iri) =
    [ "PREFIX "
    , name
    , ": <"
    , Rdf.toUrl iri
    , ">"
    ]
        |> String.concat


compare : Prefix -> Prefix -> Order
compare (Prefix nameA _) (Prefix nameB _) =
    Basics.compare nameA nameB
