module SPARQL.Node exposing
    ( var, iri, path
    , toString
    , Node
    )

{-|

@docs Term

@docs var, iri, path

@docs toString

-}

import Rdf exposing (Iri)


type Node
    = NodeVar String
    | NodeIri Iri
    | NodePath (List Iri)


var : String -> Node
var =
    NodeVar


iri : Iri -> Node
iri =
    NodeIri


path : List Iri -> Node
path =
    NodePath


toString : Node -> String
toString node =
    case node of
        NodeVar value ->
            "?" ++ value

        NodeIri value ->
            "<" ++ Rdf.toUrl value ++ ">"

        NodePath iris ->
            iris
                |> List.map (\value -> "<" ++ Rdf.toUrl value ++ ">")
                |> String.join " / "
