module Url.Extra exposing (setProtocol)

{-|

@docs setProtocol

-}

import Api.C exposing (C)
import Regex
import Url


{-| Set the protocol (https or http) of the provided URL to the protocol in the
current context's URL.
-}
setProtocol : C rest -> String -> String
setProtocol c url =
    case c.url.protocol of
        Url.Http ->
            Regex.replace
                ("^https://"
                    |> Regex.fromString
                    |> Maybe.withDefault Regex.never
                )
                (\_ -> "http://")
                url

        Url.Https ->
            Regex.replace
                ("^http://"
                    |> Regex.fromString
                    |> Maybe.withDefault Regex.never
                )
                (\_ -> "https://")
                url
