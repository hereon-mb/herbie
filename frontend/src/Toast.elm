{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Toast exposing
    ( persistent, expireIn, expireOnBlur
    , withExitTransition
    , Toast, Tray, tray
    , Content
    , add, addUnique, addUniqueBy, addUniqueWith
    , Msg, update, tuple
    , render
    , Phase(..), Interaction(..), Info
    , remove, exit
    , Id, Lifespan, Private, Toast_, Tray_
    )

{-| All you need to create, append and render toast stacks
in the Elm architecture.


# Pick one kind of toast

@docs persistent, expireIn, expireOnBlur


# Set an exit transition length

@docs withExitTransition


# Start with an empty tray, add your toasts

@docs Toast, Tray, tray
@docs Content
@docs add, addUnique, addUniqueBy, addUniqueWith


# Forward messages and update toast's tray

@docs Msg, update, tuple


# Customize & render toasts

@docs render
@docs Phase, Interaction, Info


# Remove toasts

@docs remove, exit

-}

{- This is based on https://github.com/emilianobovetti/elm-toast/ with original license:

   BSD 3-Clause License

   Copyright (c) 2018, Emiliano
   All rights reserved.

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer.

   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.

   * Neither the name of the copyright holder nor the names of its
     contributors may be used to endorse or promote products derived from
     this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
   DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
   FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
   SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
   CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
   OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-}

import Element
    exposing
        ( Element
        , alignBottom
        , alignRight
        , centerX
        , el
        , fill
        , height
        , htmlAttribute
        , moveUp
        , paddingEach
        , paddingXY
        , paragraph
        , px
        , row
        , spacing
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Extra
    exposing
        ( pointerEvents
        , tabindex
        )
import Element.Font as Font
import Element.Keyed as Keyed
import Fluent
    exposing
        ( Fluent
        , verbatim
        )
import Html.Events
import Process
import Task
import Ui.Atom.Button as Button exposing (button)
import Ui.Theme.Color
    exposing
        ( inverseOnSurface
        , inversePrimary
        , inverseSurface
        )
import Ui.Theme.Elevation as Elevation
import Ui.Theme.Typography exposing (body1)


type Private a
    = Private a


type alias Toast_ =
    { id : Id
    , blurCount : Int
    , phase : Phase
    , interaction : Interaction
    , lifespan : Lifespan
    , exitTransition : Int
    , content : Content
    }


type alias Content =
    { message : Fluent
    }


{-| `Toast.Toast` is something you'll need if you have to reference
the output type of [persistent](#persistent), [expireIn](#expireIn), [expireOnBlur](#expireOnBlur),
this is one of those things you'll know when you need it, so don't worry about this.
-}
type alias Toast =
    Private Toast_


type alias Id =
    Int


{-| A toast go through three phases:

  - `Toast.Enter` when it's just been added to a tray
  - `Toast.In` during the next render, immediately after enter phase
  - `Toast.Exit` when it's about to be removed

You can control how much time a toast is kept in
`Toast.Exit` phase through [withExitTransition](#withExitTransition).

Both `Toast.Enter` and `Toast.Exit` are considered [transition phases](#withTransitionAttributes).

-}
type Phase
    = Enter
    | In
    | Exit


{-| Can be `Toast.Focus` or `Toast.Blur`, just like [Toast.Phase](#Phase)
you'll have this information while rendering a toast through [Toast.Info](#Info).

    viewToast :
        List (Html.Attribute Msg)
        -> Toast.Info Toast
        -> Html Msg
    viewToast attributes toast =
        Html.div
            (if toast.interaction == Toast.Focus then
                class "toast-active" :: attributes

             else
                attributes
            )
            [ Html.text toast.content.message ]

    view : Model -> Html Msg
    view model =
        Toast.config ToastMsg
            |> Toast.render viewToast model.tray

-}
type Interaction
    = Focus
    | Blur


type Lifespan
    = Persistent
    | ExpireIn Int
    | ExpireOnBlur Int


new : Lifespan -> Content -> Toast
new lifespan content =
    Private
        { id = -1
        , blurCount = 0
        , phase = Enter
        , interaction = Blur
        , lifespan = lifespan
        , exitTransition = 0
        , content = content
        }


{-| Create a new toast that won't be automatically removed,
it will stay visible until you explicitly remove it.

    Toast.persistent
        { message = "hello, world"
        , color = "#7f7"
        }

-}
persistent : Content -> Toast
persistent =
    new Persistent


{-| Create a new toast with a fixed expiry.
Toast's lifespan is expressed in milliseconds.

    Toast.expireIn 5000 "I'll disappear in five seconds"

-}
expireIn : Int -> Content -> Toast
expireIn ttl =
    new (ExpireIn ttl)


{-| This kind of toast has an interaction-based expiration.
It'll be removed automatically in given time if user doesn't
interact with the toast, but it'll stay visible if receives focus
or mouse over.

When the interaction has ended and the toast lost both focus
and mouse over the given amount of milliseconds is awaited before
it's removed.

    Toast.expireOnBlur 5000 "I won't go away while I'm focused!"

-}
expireOnBlur : Int -> Content -> Toast
expireOnBlur ttl =
    new (ExpireOnBlur ttl)


{-| Add a delay between toast exit phase and actual removal.

    Toast.persistent { message = "hello, world", color = "#7f7" }
        |> Toast.withExitTransition 1000

-}
withExitTransition : Int -> Toast -> Toast
withExitTransition ttl (Private toast) =
    Private { toast | exitTransition = ttl }


type alias Tray_ =
    { currentId : Id
    , toasts : List Toast_
    }


{-| `Toast.Tray` represents the stack where toasts are stored.
You probably want to use this opaque type in your model:

    type alias MyToast =
        { message : String
        , color : String
        }

    type alias Model =
        { tray : Toast.Tray MyToast

        -- model fields...
        }

-}
type alias Tray =
    Private Tray_


{-| An empty tray, it's a thing you can put in an `init`.

    init : anything -> ( Model, Cmd msg )
    init _ =
        ( { tray = Toast.tray }, Cmd.none )

-}
tray : Tray
tray =
    Private
        { currentId = 0
        , toasts = []
        }


fire : msg -> Cmd msg
fire msg =
    Task.perform identity (Task.succeed msg)


delay : Int -> msg -> Cmd msg
delay ms msg =
    Task.perform (always msg) (Process.sleep <| toFloat ms)


withoutCmd : model -> ( model, Cmd msg )
withoutCmd model =
    ( model, Cmd.none )


withCmds : List (Cmd msg) -> model -> ( model, Cmd msg )
withCmds cmds model =
    ( model, Cmd.batch cmds )


{-| Internal message, you probably want to do something like

    type Msg
        = ToastMsg Toast.Msg
          -- other stuff...
        | AddToast MyToastContent

in your app `Msg`.

-}
type Msg
    = Transition Phase Id
    | Interaction Interaction Id
    | PrepareExit Id
    | Remove Id


onEnter : Id -> Lifespan -> List (Cmd Msg)
onEnter id lifespan =
    case lifespan of
        Persistent ->
            []

        ExpireIn ttl ->
            [ delay ttl (Transition Exit id) ]

        ExpireOnBlur ttl ->
            [ delay ttl (PrepareExit id) ]


internalAdd : Tray_ -> Toast_ -> ( Tray, Cmd Msg )
internalAdd model toast =
    let
        id : Id
        id =
            model.currentId

        toasts : List Toast_
        toasts =
            model.toasts ++ [ { toast | id = id } ]
    in
    { model | currentId = id + 1, toasts = toasts }
        |> Private
        |> withCmds (delay 100 (Transition In id) :: onEnter id toast.lifespan)


{-| Add a toast to a tray, produces an updated tray and a `Cmd Toast.Msg`.

    updateTuple :
        ( Toast.Tray { message : String, color : String }
        , Cmd Toast.Msg
        )
    updateTuple =
        Toast.persistent { message = "hello, world", color = "#7f7" }
            |> Toast.withExitTransition 1000
            |> Toast.add currentTray

-}
add : Tray -> Toast -> ( Tray, Cmd Msg )
add (Private model) (Private toast) =
    internalAdd model toast


{-| Add a toast only if its content is not already in the tray.

Toast contents are compared with [structural equality](https://package.elm-lang.org/packages/elm/core/latest/Basics#==).

    -- if currentTray already contains a toast with the same
    -- message and color it won't be added again
    Toast.persistent { message = "hello, world", color = "#7f7" }
        |> Toast.addUnique currentTray

-}
addUnique : Tray -> Toast -> ( Tray, Cmd Msg )
addUnique =
    addUniqueWith (==)


{-| This is what you need if, for example, you want to have toast
with unique `content.message`.

    -- no two "hello, world" in the same tray
    Toast.persistent { message = "hello, world", color = "#7f7" }
        |> Toast.addUniqueBy .message currentTray

-}
addUniqueBy : (Content -> a) -> Tray -> Toast -> ( Tray, Cmd Msg )
addUniqueBy cmp =
    addUniqueWith (\x y -> cmp x == cmp y)


{-| Most powerful `addUnique` version: it takes a function that
compares two toast contents.

    type alias MyToast =
        { message : String
        , color : String
        }

    sameMessageLength : MyToast -> MyToast -> Bool
    sameMessageLength t1 t2 =
        String.length t1.message == String.length t2.message

    -- we can't have two toast with same message length
    -- for some reason...
    Toast.persistent { message = "hello, world", color = "#7f7" }
        |> Toast.addUniqueWith sameMessageLength currentTray

-}
addUniqueWith : (Content -> Content -> Bool) -> Tray -> Toast -> ( Tray, Cmd Msg )
addUniqueWith cmp (Private model) (Private toast) =
    if List.any (.content >> cmp toast.content) model.toasts then
        withoutCmd (Private model)

    else
        internalAdd model toast


setToasts : { model | toasts : t } -> t -> { model | toasts : t }
setToasts model toasts =
    { model | toasts = toasts }


removeToast : Id -> Tray_ -> ( Tray_, Cmd msg )
removeToast id model =
    model.toasts
        |> List.filter (\toast -> toast.id /= id)
        |> setToasts model
        |> withoutCmd


findToastAndUpdate : (model -> Maybe ( model, cmd )) -> List model -> List model -> ( List model, Maybe cmd )
findToastAndUpdate updater dest src =
    case src of
        hd :: tl ->
            case updater hd of
                Just ( toast, cmd ) ->
                    ( List.reverse dest ++ toast :: tl, Just cmd )

                Nothing ->
                    findToastAndUpdate updater (hd :: dest) tl

        [] ->
            ( List.reverse dest, Nothing )


updateToastWithCmd : (Toast_ -> ( Toast_, Cmd Msg )) -> Id -> Tray_ -> ( Tray_, Cmd Msg )
updateToastWithCmd updater id model =
    let
        doUpdate : Toast_ -> Maybe ( Toast_, Cmd Msg )
        doUpdate toast =
            if toast.id == id then
                Just (updater toast)

            else
                Nothing
    in
    model.toasts
        |> findToastAndUpdate doUpdate []
        |> Tuple.mapBoth (setToasts model) (Maybe.withDefault Cmd.none)


updateToast : (Toast_ -> Toast_) -> Id -> Tray_ -> ( Tray_, Cmd Msg )
updateToast updater =
    updateToastWithCmd (updater >> withoutCmd)


onBlur : Toast_ -> List (Cmd Msg)
onBlur toast =
    case toast.lifespan of
        ExpireOnBlur ttl ->
            [ delay ttl (PrepareExit toast.id) ]

        _ ->
            []


handleBlur : Toast_ -> ( Toast_, Cmd Msg )
handleBlur toast =
    { toast | interaction = Blur, blurCount = toast.blurCount + 1 }
        |> withCmds (onBlur toast)


handlePrepareExit : Toast_ -> ( Toast_, Cmd Msg )
handlePrepareExit toast =
    if toast.interaction == Focus || toast.blurCount > 0 then
        withoutCmd { toast | blurCount = toast.blurCount - 1 }

    else
        ( toast, fire (Transition Exit toast.id) )


handleStartExit : Toast_ -> ( Toast_, Cmd Msg )
handleStartExit toast =
    ( { toast | phase = Exit }, delay toast.exitTransition (Remove toast.id) )


internalUpdate : Msg -> Tray_ -> ( Tray_, Cmd Msg )
internalUpdate msg =
    case msg of
        Transition In id ->
            updateToast (\toast -> { toast | phase = In }) id

        Transition Enter id ->
            updateToast (\toast -> { toast | phase = Enter }) id

        Transition Exit id ->
            updateToastWithCmd handleStartExit id

        Interaction Focus id ->
            updateToast (\toast -> { toast | interaction = Focus }) id

        Interaction Blur id ->
            updateToastWithCmd handleBlur id

        PrepareExit id ->
            updateToastWithCmd handlePrepareExit id

        Remove id ->
            removeToast id


{-| Nothing fancy here: given a [Toast.Msg](#Msg) and a [Toast.Tray](#Tray)
updates tray's state and produces a `Cmd`.
-}
update : Msg -> Tray -> ( Tray, Cmd Msg )
update msg (Private model) =
    Tuple.mapFirst Private (internalUpdate msg model)


{-| Helps in conversion between
`( Toast.Tray, Cmd Toast.Msg )` and `( Model, Cmd Msg )`.

    update : Msg -> Model -> ( Model, Cmd Msg )
    update msg model =
        case msg of
            AddToast content ->
                Toast.persistent content
                    |> Toast.add model.tray
                    |> Toast.tuple ToastMsg model

            ToastMsg tmsg ->
                Toast.update tmsg model.tray
                    |> Toast.tuple ToastMsg model

-}
tuple : (Msg -> msg) -> { model | tray : Tray } -> ( Tray, Cmd Msg ) -> ( { model | tray : Tray }, Cmd msg )
tuple toAppMsg appModel ( model, cmd ) =
    ( { appModel | tray = model }, Cmd.map toAppMsg cmd )


{-| `Toast.Info` represent data publicly exposed about a toast.

You already know [Toast.Phase](#Phase) and [Toast.Interaction](#Interaction),
of course you also know `content` since this is your own data.

Meet `id`, this little field contains a unique value for each toast
that you need to pass to [Toast.remove](#remove) and [Toast.exit](#exit).

-}
type alias Info =
    { id : Private Id
    , phase : Phase
    , interaction : Interaction
    , content : Content
    }


toInfo : Toast_ -> Info
toInfo toast =
    { id = Private toast.id
    , phase = toast.phase
    , interaction = toast.interaction
    , content = toast.content
    }


renderToast : Toast_ -> ( String, Element Msg )
renderToast toast =
    ( "toast-" ++ String.fromInt toast.id
    , viewToast (toInfo toast)
    )


{-| This function is where our money are: all our data shrunk down
to a beautiful `Html msg`, ready to be served.

The first thing needed to make this magic is a `viewToast` function
that I'll try to explain how it works:

  - It takes all the html attributes compiled by `elm-toast`
    which you need to remember to attach to some node
  - Takes a [Toast.Info](#Info) so you can access to your
    toast's `content` and other stuff
  - Return an `Html msg` that represent your toast,
    or something that contains your toast like a wrapper
    or whatever

Secondly you have to provide a [Toast.Tray](#Tray)
and last but not least your [Toast.Config](#Config).

    viewToast : List (Html.Attribute Msg) -> Toast.Info Toast -> Html Msg
    viewToast attrs toast =
        Html.div
            attrs -- do not forget this little friend!
            [ Html.text toast.content.message ]

    Toast.render viewToast model.tray toastConfig

-}
render : Tray -> Element Msg
render (Private model) =
    model.toasts
        |> List.map renderToast
        |> Keyed.column
            [ alignBottom
            , centerX
            , spacing 8
            , moveUp 16
            ]


viewToast : Info -> Element Msg
viewToast toast =
    let
        (Private id) =
            toast.id
    in
    [ toast.content.message
        |> body1
        |> List.singleton
        |> paragraph
            [ paddingXY 0 8
            ]
    , verbatim "Close"
        |> button (exit toast.id)
        |> Button.withFormat Button.Text
        |> Button.withColor inversePrimary
        |> Button.view
        |> el
            [ alignRight
            , alignBottom
            ]
    ]
        |> row
            [ pointerEvents True
            , width (px 512)
            , height fill
            , paddingEach
                { top = 6
                , bottom = 6
                , left = 16
                , right = 2
                }
            , spacing 8
            , Background.color inverseSurface
            , Font.color inverseOnSurface
            , Border.rounded 4
            , Elevation.z6
            , tabindex 0
            , htmlAttribute (Html.Events.onMouseEnter (Interaction Focus id))
            , htmlAttribute (Html.Events.onMouseLeave (Interaction Blur id))
            , htmlAttribute (Html.Events.onFocus (Interaction Focus id))
            , htmlAttribute (Html.Events.onBlur (Interaction Blur id))
            ]


{-| Inside your [viewToast](#render) you may want to remove
your toast programmatically.
Remove means that the toast is deleted right away.
If you want to go through the exit transition use [exit](#exit).

    closeButton : Toast.Info Toast -> Html Msg
    closeButton toast =
        Html.div
            [ onClick <| ToastMsg (Toast.exit toast.id) ]
            [ Html.text "✘" ]

    viewToast : List (Html.Attribute Msg) -> Toast.Info Toast -> Html Msg
    viewToast attrs toast =
        Html.div
            attrs
            [ Html.text toast.content.message
            , closeButton toast
            ]

-}
remove : Private Id -> Msg
remove (Private id) =
    Remove id


{-| Same as [remove](#remove), but the toast goes through
its exit transition phase.
If you have a fade-out animation it'll be showed.
-}
exit : Private Id -> Msg
exit (Private id) =
    Transition Exit id
