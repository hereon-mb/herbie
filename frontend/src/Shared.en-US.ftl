shared--action--dialog--edit-permissions-of-entry-text--close = Close
shared--create = Create
shared--heading--dialog--edit-permissions-of-entry-text = Edit permissions of journal entry
shared--input--permissions--equipment--label = Users of { $equipmentName }
shared--input--permissions--equipment--sub-label = Grant read access to all users of the equipment
shared--input--permissions--label = Additional visibility
shared--input--permissions--material--label = Material { $materialPublicId }
shared--input--permissions--material--sub-label = Grant read access to all users who can see the material
shared--input--permissions--placeholder = Type to grant access to (groups of) users
shared--input--permissions--project--label = Members of { $projectName }
shared--input--permissions--project--sub-label = Grant read access to all members of the project
shared--navigation-item--aggregation = Data aggregation
shared--navigation-item--documents = Documents
shared--navigation-item--equipment = Equipment
shared--navigation-item--experiments = Experiments
shared--navigation-item--graph = Explorer
shared--navigation-item--groups = Groups
shared--navigation-item--journal = Journal
shared--navigation-item--locations = Locations
shared--navigation-item--logout = Logout
shared--navigation-item--maintenances = Maintenances
shared--navigation-item--materials = Materials/Products
shared--navigation-item--projects = Projects
shared--navigation-item--projects = Projects
shared--navigation-item--protocols = Protocol types
shared--navigation-item--query = Search
shared--navigation-item--sensors = Sensors
shared--navigation-item--settings = Preferences
shared--navigation-item--sops = SOPs
shared--navigation-item--sparklis = Sparklis
shared--navigation-item--users = Users
shared--paragraph--fetching-data-failed = Could not fetch data from server.
