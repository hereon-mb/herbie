module Expect.Shacl.Form.Viewed exposing
    ( Raw
    , document
    , expectAll
    , expectAllWithDefaultPopulation
    , expectGraphAllWithDefaultPopulation
    , showcase
    , xsd
    )

import Dict
import Element
import Expect exposing (Expectation)
import Fixture.Context as Context
import Random
import Rdf exposing (Iri)
import Rdf.Decode as Decode
import Rdf.Graph as Graph exposing (Graph, serializeTurtle)
import Result.Extra as Result
import Result.Pipeline as Result
import Shacl.Form.Viewed as Viewed
import Shacl.Report as Report
import Test.Html.Query as Query
import Tuple.Extra as Tuple


type alias Raw =
    { shape : String
    , data : String
    }


{-| Given a shapes and a data graph, instantiate a `Shacl.Form.Viewed.Form` and
evaluate the provided expectations on it's `view`. Things to note:

  - The shapes graph has the IRI <http://example.org/shapes/> and the base of
    the document will be set to this.

  - The data graph has the IRI <http://example.org/data/> and the base of the
    document will be set to this.

  - The following triples will be added to the shapes graph:

    ```turtle
    <> a owl:Ontology ;
       dcterms:conformsTo <http://example.org/shapes/> ;
    .
    ```

  - The following prefixes will be added to both graphs:

    ```turtle
    @prefix dash: <http://datashapes.org/dash#> .
    @prefix dcterms: <http://purl.org/dc/terms/> .
    @prefix hash: <http://purls.helmholtz-metadaten.de/herbie/hash/#> .
    @prefix owl: <http://www.w3.org/2002/07/owl#> .
    @prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
    @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
    @prefix sh: <http://www.w3.org/ns/shacl#> .
    @prefix showcase: <http://purls.helmholtz-metadaten.de/herbie/showcase/ont/#> .
    @prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
    ```

-}
expectAll :
    Bool
    -> List (Query.Single Viewed.Msg -> Expectation)
    -> Raw
    -> Expect.Expectation
expectAll infoForm toExpectations raw =
    case toForm raw of
        Err error ->
            Expect.fail (messageError raw error)

        Ok { form, data } ->
            form
                |> Viewed.expandAll
                |> Viewed.view Context.default.cShacl Report.empty
                |> Element.layoutWith { options = [ Element.noStaticStyleSheet ] } []
                |> Query.fromHtml
                |> Expect.all toExpectations
                |> (if infoForm then
                        Expect.onFail (messageInfoForm form data)

                    else
                        identity
                   )


expectAllWithDefaultPopulation :
    Bool
    -> List (Query.Single Viewed.Msg -> Expectation)
    -> Raw
    -> Expect.Expectation
expectAllWithDefaultPopulation infoForm toExpectations raw =
    case toForm raw of
        Err error ->
            Expect.fail (messageError raw error)

        Ok { form, data } ->
            form
                |> Viewed.view Context.default.cShacl Report.empty
                |> Element.layoutWith { options = [ Element.noStaticStyleSheet ] } []
                |> Query.fromHtml
                |> Expect.all toExpectations
                |> (if infoForm then
                        Expect.onFail (messageInfoForm form data)

                    else
                        identity
                   )


expectGraphAllWithDefaultPopulation :
    List (Graph -> Result Decode.Error Expectation)
    -> Raw
    -> Expect.Expectation
expectGraphAllWithDefaultPopulation expectationDecoders raw =
    case toForm raw of
        Err error ->
            Expect.fail (messageError raw error)

        Ok { form, data } ->
            case Viewed.graphFrontend form of
                Nothing ->
                    Expect.fail "Could not serialize the graph."

                Just graph ->
                    let
                        toExpectation expectationDecoder subject =
                            case expectationDecoder subject of
                                Err e ->
                                    Expect.fail
                                        (Decode.errorToString e
                                            ++ messageInfoForm form data
                                        )

                                Ok e ->
                                    e
                    in
                    Expect.all (List.map toExpectation expectationDecoders) graph


messageError : Raw -> Error -> String
messageError raw error =
    case error of
        ErrorGraphShape errorGraph ->
            "Could not parse the shape graph: " ++ Graph.errorToString raw.shape errorGraph

        ErrorGraphData errorGraph ->
            "Could not parse the data graph: " ++ Graph.errorToString raw.data errorGraph

        ErrorForm errorForm ->
            Viewed.errorToString errorForm


messageInfoForm : Viewed.Form -> Graph -> String
messageInfoForm form data =
    let
        prefixes =
            Graph.getPrefixes data

        base =
            Graph.getBase data

        generated =
            case Viewed.graphFrontend form of
                Nothing ->
                    "Could not serialize the graph."

                Just graph ->
                    List.foldl
                        (Tuple.apply Graph.addPrefix)
                        (case base of
                            Nothing ->
                                graph

                            Just value ->
                                Graph.setBase value graph
                        )
                        (Dict.toList prefixes)
                        |> serializeTurtle
    in
    "The form was:\n\n"
        ++ indent (Viewed.logForm Context.default.cShacl form)
        ++ "\n\nThe input graph was:\n\n"
        ++ indent (serializeTurtle data)
        ++ "\n\nThe generated graph was:\n\n"
        ++ indent generated


indent : String -> String
indent =
    String.lines
        >> List.map (\line -> "    " ++ line)
        >> String.join "\n"


type Error
    = ErrorGraphShape Graph.Error
    | ErrorGraphData Graph.Error
    | ErrorForm Viewed.Error


type alias Form =
    { form : Viewed.Form
    , shacl : Graph
    , data : Graph
    }


toForm : Raw -> Result Error Form
toForm raw =
    let
        shape : String
        shape =
            [ "@base <" ++ Rdf.toUrl iriShapes ++ "> ."
            , prefixes
            , raw.shape
            ]
                |> String.join "\n"

        data : String
        data =
            [ "@base <" ++ Rdf.toUrl iriDocument ++ "> ."
            , prefixes
            , "<> a owl:Ontology ."
            , "<> dcterms:conformsTo <" ++ Rdf.toUrl iriShapes ++ "> ."
            , raw.data
            ]
                |> String.join "\n"

        prefixes : String
        prefixes =
            """
            @prefix dash: <http://datashapes.org/dash#> .
            @prefix dcterms: <http://purl.org/dc/terms/> .
            @prefix hash: <http://purls.helmholtz-metadaten.de/herbie/hash/#> .
            @prefix owl: <http://www.w3.org/2002/07/owl#> .
            @prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
            @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
            @prefix sh: <http://www.w3.org/ns/shacl#> .
            @prefix showcase: <""" ++ Rdf.toUrl (showcase "") ++ """> .
            @prefix xsd: <""" ++ Rdf.toUrl (xsd "") ++ """> .
            """

        iriShapes : Iri
        iriShapes =
            Rdf.iri "http://example.org/shapes/"

        iriDocument : Iri
        iriDocument =
            document ""
    in
    Ok
        (\graphShape graphData ->
            { iri = iriDocument
            , workspaceUuid = "workspace-uuid"
            , preview = False
            , strict = False
            , graph = graphData
            , graphEntered = graphData
            , graphPersisted = graphData
            , graphGenerated = Graph.emptyGraph
            , graphSupport = Graph.emptyGraph
            , graphOntology = graphShape
            , populateDefaultValues = { populateDefaultValues = True }
            , seed =
                -- FIXME Hack to force a different seed for the blank
                -- node generation within the form
                Random.initialSeed 42
                    |> Random.step Graph.seedGenerator
                    |> Tuple.first
            }
                |> Viewed.initStatic Context.default.cShacl
                |> Result.mapError ErrorForm
                |> Result.map
                    (\( f, _, _ ) ->
                        { form = f
                        , shacl = graphShape
                        , data = graphData
                        }
                    )
        )
        |> Result.required (Result.mapError ErrorGraphShape (Graph.parse shape))
        |> Result.required (Result.mapError ErrorGraphData (Graph.parse data))
        |> Result.join


showcase : String -> Iri
showcase name =
    Rdf.iri ("http://purls.helmholtz-metadaten.de/herbie/showcase/ont/#" ++ name)


document : String -> Iri
document name =
    Rdf.iri ("http://example.org/document/" ++ name)


xsd : String -> Iri
xsd name =
    Rdf.iri ("http://www.w3.org/2001/XMLSchema#" ++ name)
