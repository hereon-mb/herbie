{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Stories.Molecule.Listbox exposing (story)

import Bulletproof exposing (Renderer, Story)
import Bulletproof.Extra exposing (fromElmUi)
import Bulletproof.Knob
import Element
    exposing
        ( el
        , px
        , width
        )
import Fluent exposing (verbatim)
import Ui.Atom.Icon as Icon
import Ui.Molecule.Listbox as Listbox


story : Story
story =
    Bulletproof.story "Listbox" listFromItemsRenderer
        |> Bulletproof.Knob.select "below"
            [ ( "with nothing below text", identity )
            , ( "withSecondaryLine", Listbox.withSecondaryLine [ verbatim "Mg-2Ca", verbatim "18 h at 300 C" ] )
            ]
        |> Bulletproof.Knob.select "before"
            [ ( "with nothing before text", identity )
            , ( "withSpaceBeforeText", Listbox.withSpaceBeforeText )
            , ( "withIconBeforeText", Listbox.withIconBeforeText Icon.People )
            ]
        |> Bulletproof.Knob.select "after"
            [ ( "with nothing after text", identity )
            , ( "withIconAfterText", Listbox.withIconAfterText Icon.Clear )
            , ( "withActionIconAfterText", Listbox.withActionIconAfterText Icon.MoreVert () )
            , ( "withProgressAfterText", Listbox.withProgressAfterText 0.8 )
            ]


listFromItemsRenderer : (Listbox.Item () -> Listbox.Item ()) -> (Listbox.Item () -> Listbox.Item ()) -> (Listbox.Item () -> Listbox.Item ()) -> Renderer
listFromItemsRenderer belowText beforeText afterText =
    let
        toItem title =
            verbatim title
                |> Listbox.item ()
                |> belowText
                |> beforeText
                |> afterText
    in
    [ "210002"
    , "210003"
    , "210002.A1"
    , "210002.A2"
    , "210002.A1.B1"
    ]
        |> List.map toItem
        |> Listbox.fromItems
        |> Listbox.toElement
        |> el
            [ width (px 640)
            ]
        |> fromElmUi
