{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Stories.Molecule.Note exposing (story)

import Bulletproof exposing (Story)


story : Story
story =
    Bulletproof.folder "Note"
        [-- FIXME
         --            Bulletproof.story "with markdown"
         --            (\content username ->
         --                Ui.Organism.Note.card Fixture.context
         --                    { userPressedUpload = \_ -> ()
         --                    , userPressedDownloadOnUpload = \_ -> ()
         --                    , userPressedEdit = ()
         --                    , userPressedCard = ()
         --                    , tooltipMsg = \_ -> ()
         --                    }
         --                    { createdAt = Time.millisToPosix 300000
         --                    , updatedAt = Time.millisToPosix 300000
         --                    , id = Id.fromInt 1
         --                    , url = "#"
         --                    , createdBy =
         --                        { id = Id.fromInt 1
         --                        , url = "#"
         --                        , user =
         --                            { id = Id.fromInt 1
         --                            , url = "#"
         --                            , username = username
         --                            , email = "alice@examle.org"
         --                            , firstName = "Alice"
         --                            , lastName = "Wonderland"
         --                            }
         --                        }
         --                    , text =
         --                        { content = content
         --                        , uploads = []
         --                        , tags = []
         --                        }
         --                    }
         --                    |> fromElmUi
         --            )
         --            |> Bulletproof.Knob.string "Content" defaultContent
         --            |> Bulletproof.Knob.string "Username" "alice"
         --        , Bulletproof.story "with tags"
         --            (\tagCount ->
         --                Ui.Organism.Note.card Fixture.context
         --                    { userPressedUpload = \_ -> ()
         --                    , userPressedDownloadOnUpload = \_ -> ()
         --                    , userPressedEdit = ()
         --                    , userPressedCard = ()
         --                    , tooltipMsg = \_ -> ()
         --                    }
         --                    { createdAt = Time.millisToPosix 300000
         --                    , updatedAt = Time.millisToPosix 300000
         --                    , id = Id.fromInt 1
         --                    , url = "#"
         --                    , author =
         --                        { id = Id.fromInt 1
         --                        , url = "#"
         --                        , user =
         --                            { id = Id.fromInt 1
         --                            , url = "#"
         --                            , username = "alice"
         --                            , email = "alice@examle.org"
         --                            , firstName = "Alice"
         --                            , lastName = "Wonderland"
         --                            }
         --                        }
         --                    , content = "Note content."
         --                    , uploads = []
         --                    , tags =
         --                        List.range 1 (tagCount + 1)
         --                            |> List.map (\index -> "Tag " ++ String.fromInt index)
         --                    , editAllowed = True
         --                    , taggables = Dict.empty
         --                    }
         --                    |> fromElmUi
         --            )
         --            |> Bulletproof.Knob.int "# Tags"
         --                25
         --                [ Bulletproof.Knob.min 1
         --                , Bulletproof.Knob.max 50
         --                ]
        ]
