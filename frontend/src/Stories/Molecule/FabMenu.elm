{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Stories.Molecule.FabMenu exposing (story)

import Bulletproof exposing (Story)
import Bulletproof.Extra exposing (fromElmUi)
import Fluent
import Ui.Atom.Icon as Icon
import Ui.Molecule.FabMenu


story : Story
story =
    Ui.Molecule.FabMenu.view
        { primaryItem =
            { icon = Icon.Create
            , label = Fluent.text "navigation-action-note"
            , onPress = ()
            }
        , secondaryItems =
            [ { icon = Icon.AttachFile
              , label = Fluent.text "navigation-action-file"
              , onPress = ()
              }
            , { icon = Icon.CameraAlt
              , label = Fluent.text "navigation-action-photo"
              , onPress = ()
              }
            , { icon = Icon.Assignment
              , label = Fluent.text "navigation-action-protocol"
              , onPress = ()
              }
            ]
        , recentItems =
            [ { icon = Icon.Assignment
              , label = Fluent.text "navigation-action-cast"
              , onPress = ()
              }
            , { icon = Icon.Assignment
              , label = Fluent.text "navigation-action-heat-treatment"
              , onPress = ()
              }
            ]
        }
        |> fromElmUi
        |> Bulletproof.story "FabMenu"
