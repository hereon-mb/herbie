{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Stories.Molecule.ComboboxMultiselect exposing (story)

import Bulletproof exposing (Story)
import Bulletproof.Extra exposing (fromElmUi, smallWidth)
import Bulletproof.Knob
import Fluent exposing (verbatim)
import Pointer
import Ui.Molecule.ComboboxMultiselect as ComboboxMultiselect exposing (comboboxMultiselect)


story : Story
story =
    let
        options =
            [ "0", "1", "2", "42" ]

        toOptionCombobox option =
            { id = option
            , value = option
            , label = verbatim ("Option " ++ option)
            , subLabel = verbatim ("More info about " ++ option)
            }
    in
    (\withSupportingText ->
        comboboxMultiselect
            { id = Pointer.field ""
            , label = ComboboxMultiselect.labelAbove (verbatim "Values")
            , initialQuery = ""
            , supportingText =
                if withSupportingText then
                    Just (verbatim "Some additional information.")

                else
                    Nothing
            , help = []
            , options = options
            , toOption = toOptionCombobox
            , fromOption = \_ -> Nothing
            , selected = []
            , onChange = \_ -> ()
            , onSelect = \_ -> ()
            , onUnselect = \_ -> ()
            , onCopy = Nothing
            , onPaste = Nothing
            , noOp = ()
            , renderSubLabels = False
            }
            |> smallWidth
            |> fromElmUi
    )
        |> Bulletproof.story "Combobox Multiselect"
        |> Bulletproof.Knob.bool "withSupportingText" False
