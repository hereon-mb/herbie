{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Stories.Molecule.DatePicker exposing (story)

import Bulletproof exposing (Story)
import Bulletproof.Extra exposing (fromElmUi)
import Fluent
import Locale
import Pointer
import Ui.Molecule.DatePicker as DatePicker


story : Story
story =
    DatePicker.datePicker
        { label = Fluent.verbatim "Performed on"
        , help = []
        , id = Pointer.verbatim "performed-on"
        , locale = Locale.EnUS
        , onChange = always ()
        , withTime = True
        , initialDate = Nothing
        , supportingText = Nothing
        , onCopy = Nothing
        , onPaste = Nothing
        }
        |> fromElmUi
        |> Bulletproof.story "DatePicker"
