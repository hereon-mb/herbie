{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Stories.Molecule.Navigation exposing (story)

import Bulletproof exposing (Story)
import Bulletproof.Extra exposing (fromElmUi)
import Bulletproof.Knob
import Element
    exposing
        ( clip
        , el
        , fill
        , fillPortion
        , height
        , minimum
        , none
        , px
        , row
        , width
        )
import Element.Extra
    exposing
        ( style
        )
import Fluent
import Stories.Fixture as Fixture
import Ui.Atom.Icon as Icon
import Ui.Molecule.Navigation


story : Story
story =
    Bulletproof.story "Navigation"
        (\_ expanded ->
            row
                [ width fill
                , height fill
                , clip
                ]
                [ el
                    [ width
                        (if expanded then
                            fillPortion 2
                                |> minimum 74

                         else
                            px 74
                        )
                    , style "transition" "flex 300ms"
                    , height fill
                    , clip
                    ]
                    (Ui.Molecule.Navigation.view Fixture.context
                        { items = navigationItems
                        , fill = False
                        , expanded = expanded
                        , onToggleExpanded = ()
                        , userPressedShowHelp = ()
                        , userPressedSendFeedback = ()
                        }
                    )
                , el
                    [ width (fillPortion 6)
                    , height fill
                    ]
                    none
                ]
                |> fromElmUi
        )
        |> Bulletproof.Knob.int "Current"
            0
            [ Bulletproof.Knob.min 0
            , Bulletproof.Knob.max (List.length navigationItems - 1)
            ]
        |> Bulletproof.Knob.bool "Expanded" True


navigationItems : List (Ui.Molecule.Navigation.Item msg)
navigationItems =
    [ Ui.Molecule.Navigation.itemLink "#" Icon.Book (Fluent.text "navigation-item-journal")
    , Ui.Molecule.Navigation.itemLink "#" Icon.ScatterPlot (Fluent.text "navigation-item-materials")
    , Ui.Molecule.Navigation.itemLink "#" Icon.Science (Fluent.text "navigation-item-experiments")
    , Ui.Molecule.Navigation.itemLink "#" Icon.Biotech (Fluent.text "navigation-item-equipment")
    , Ui.Molecule.Navigation.itemLink "#" Icon.Build (Fluent.text "navigation-item-maintenances")
    , Ui.Molecule.Navigation.itemLink "#" Icon.Timeline (Fluent.text "navigation-item-sensors")
    , Ui.Molecule.Navigation.itemLink "#" Icon.Assignment (Fluent.text "navigation-item-sops")
    , Ui.Molecule.Navigation.itemLink "#" Icon.Room (Fluent.text "navigation-item-locations")
    , Ui.Molecule.Navigation.itemLink "#" Icon.GroupWork (Fluent.text "navigation-item-projects")
    , Ui.Molecule.Navigation.itemLink "#" Icon.People (Fluent.text "navigation-item-users")
    , Ui.Molecule.Navigation.itemLink "#" Icon.Groups (Fluent.text "navigation-item-groups")
    ]
