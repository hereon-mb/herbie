{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Stories.Molecule.Dialog exposing (story)

import Bulletproof exposing (Story)
import Bulletproof.Extra exposing (fromElmUi)
import Bulletproof.Knob
import Element
    exposing
        ( alignRight
        , column
        , el
        , fill
        , paddingXY
        , spacing
        , width
        )
import Fluent exposing (verbatim)
import Ui.Atom.Button as Button exposing (button)
import Ui.Atom.TextInput as TextInput exposing (textInput)
import Ui.Molecule.Dialog as Dialog exposing (desktop)


story : Story
story =
    Bulletproof.story "Dialog"
        (\wide fullWidth fillVertically withActions ->
            [ ""
                |> textInput (\_ -> ()) (TextInput.labelAbove (verbatim "Value A"))
                |> TextInput.view
            , ""
                |> textInput (\_ -> ()) (TextInput.labelAbove (verbatim "Value B"))
                |> TextInput.view
            ]
                |> column
                    [ width fill
                    , spacing 16
                    ]
                |> desktop (verbatim "Dialog title")
                |> (if wide then
                        Dialog.wide

                    else
                        identity
                   )
                |> (if fullWidth then
                        Dialog.fullWidth

                    else
                        identity
                   )
                |> (if fillVertically then
                        Dialog.fillVertically

                    else
                        identity
                   )
                |> (if withActions then
                        Dialog.withActions
                            [ verbatim "Submit"
                                |> button ()
                                |> Button.view
                                |> el [ paddingXY 16 0 ]
                            , verbatim "Cancel"
                                |> button ()
                                |> Button.withFormat Button.Text
                                |> Button.view
                                |> el [ alignRight ]
                            ]

                    else
                        identity
                   )
                |> Dialog.toElement
                |> fromElmUi
        )
        |> Bulletproof.Knob.bool "wide" False
        |> Bulletproof.Knob.bool "fullWidth" False
        |> Bulletproof.Knob.bool "fillVertically" False
        |> Bulletproof.Knob.bool "withActions" True
