{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Stories.Fixture exposing
    ( context
    , units
    )

import Api.Id as Id
import Context exposing (C)
import Dict
import Element
import Gen.Route as Route
import Locale
import Rdf
import RemoteData
import Time
import Ui.Atom.Tooltip as Tooltip
import Url exposing (Protocol(..))


context : C
context =
    { sandbox = False
    , useTripleStore = True
    , useSparklis = True
    , allowUploads = True
    , imprintUrl = Nothing
    , dataProtectionUrl = Nothing
    , accessibilityUrl = Nothing
    , csrfToken = ""
    , url =
        { protocol = Https
        , host = "example.org"
        , port_ = Nothing
        , path = "/journal"
        , query = Nothing
        , fragment = Nothing
        }
    , route = Route.Datasets
    , query = Dict.empty
    , workspaceUuid = Nothing
    , workspace = Nothing
    , author =
        { id = Id.fromInt 1
        , user =
            { id = Id.fromInt 1
            , url = "#"
            , username = "alice"
            , firstName = "Alice"
            , lastName = "Wonderland"
            , email = "alice@example.com"
            }
        , permissions =
            { superuser = False
            , canSendSparqlQueries = True
            , canUploadRawFiles = True
            }
        , preferencesAuthor =
            { renderIrisInDropdownMenus = False
            }
        , authTokens = []
        }
    , zone = Time.utc
    , now = Time.millisToPosix 430000
    , locale = Locale.EnUS
    , viewport =
        { width = 1024
        , height = 960
        }
    , device =
        { class = Element.Desktop
        , orientation = Element.Landscape
        }
    , navigationVisible = False
    , tooltip = Tooltip.init
    , authors =
        RemoteData.Success
            [ { id = Id.fromInt 1
              , url = "#"
              , user =
                    { id = Id.fromInt 1
                    , url = "#"
                    , username = "alice"
                    , firstName = "Alice"
                    , lastName = "Wonderland"
                    , email = "alice@example.com"
                    }
              }
            ]
    , workspaces = RemoteData.Success []
    , cShacl =
        { tooltip = Tooltip.init
        , locale = Locale.EnUS
        , allowUploads = True
        , permissions =
            { superuser = False
            , canSendSparqlQueries = True
            , canUploadRawFiles = True
            }
        , preferences =
            { renderIrisInDropdownMenus = False
            }
        , toUrl = \_ -> Rdf.toUrl
        }
    }



{-
   dataSemMaterials : List SemMaterial
   dataSemMaterials =
       [{- { id = 1
             , url = "#"
             , material =
                   { id = 1
                   , url = "#"
                   , publicId = "200401$ZZ109-3_wf"
                   , protocol = Nothing
                   , materialExternal =
                       Just
                           { id = 1
                           , url = "#"
                           , createdAt = now
                           , updatedAt = now
                           , author = authorAlice
                           , materialType = Membrane
                           , comment = ""
                           , editAllowed = True
                           }
                   }
             , comment = "after waterflux measurement; take piece from middle"
             , preparations =
                   [ { id = 1
                     , url = "#"
                     , cryoSem = "frozen in lN2, freeze dried in MED020"
                     , criticalPointDrying = "not done"
                     , comment = "Material unstable in SEM -> silver paint for stabilization"
                     , surfacePreparation =
                           Just
                               { cutType = Just Razorblade
                               , imsAnalysis = False
                               }
                     , crossSectionPreparation = Nothing
                     , semMeasurements = []
                     , edxMeasurements = []
                     }
                   , { id = 1
                     , url = "#"
                     , cryoSem = ""
                     , criticalPointDrying = ""
                     , comment = "Delamination -> preparation without support"
                     , surfacePreparation = Nothing
                     , crossSectionPreparation =
                           Just
                               { breakType = Just LiquidNitrogenIsopropanol
                               , ionMillingDevice = Nothing
                               , ionMillingEnergyEletronvolt = Nothing
                               , ionMillingTemperatureCelsius = Nothing
                               , ionMillingTimeMinute = Nothing
                               , ultramicrotomeDevice = Just equipmentUct
                               , ultramicrotomeType = Just RoomTemp
                               , diamondWireSawDevice = Nothing
                               , diamondWireSawThicknessNm = Nothing
                               , diamondWireSawGrain = Nothing
                               }
                     , semMeasurements =
                           [ { id = 1
                             , url = "#"
                             , equipment = equipmentMerlin
                             }
                           ]
                     , edxMeasurements =
                           [ { id = 1
                             , url = "#"
                             , equipment = equipmentEdxExtreme
                             }
                           , { id = 1
                             , url = "#"
                             , equipment = equipmentEdxXmax150
                             }
                           ]
                     }
                   ]
             }
           , { id = 1
             , url = "#"
             , material =
                   { id = 1
                   , url = "#"
                   , publicId = "200401$ZZ109-4"
                   , protocol = Nothing
                   , materialExternal =
                       Just
                           { id = 1
                           , url = "#"
                           , createdAt = now
                           , updatedAt = now
                           , author = authorAlice
                           , materialType = Membrane
                           , comment = ""
                           , editAllowed = True
                           }
                   }
             , comment = ""
             , preparations =
                   [ Surface
                       { position = 2
                       , materialId = "200401$ZZ109-4"
                       , comment = Nothing
                       }
                       { imsAnalysis = True
                       , cut = Just Scissors
                       , cryoSem = Nothing
                       , criticalPointDrying = Nothing
                       , coating =
                           Just
                               { coatingDevices = [ equipmentMed, equipmentAce, equipmentSafematic ]
                               , coatingPlatinum = Just "3"
                               , coatingCarbon = Just "8"
                               , coatingChromium = Nothing
                               , coatingOther =
                                   Just
                                       { otherMaterial = "Au/Pd"
                                       , otherThickness = "2"
                                       }
                               }
                       , sem = Just [ equipmentMerlin, equipmentLeo, equipmentLeoCryo ]
                       , edx = Just [ equipmentEdxXmax150, equipmentEdxXmax80 ]
                       }
                   , Crosssection
                       { position = 2
                       , materialId = "200401$ZZ109-4"
                       , comment = Just "Delamination -> preparation without support"
                       }
                       { imsAnalysis = True
                       , break = Just LiquidNitrogen
                       , ionMilling = Nothing
                       , ultramicrotome =
                           Just
                               { ultramicrotomeDevice = equipmentUct
                               , ultramicrotomeType = Cryo
                               }
                       , diamondWireSaw = Nothing
                       , cryoSem = Just "freeze"
                       , criticalPointDrying = Nothing
                       , coating =
                           Just
                               { coatingDevices = [ equipmentMed ]
                               , coatingPlatinum = Just "2"
                               , coatingCarbon = Nothing
                               , coatingChromium = Nothing
                               , coatingOther = Nothing
                               }
                       , sem = Just [ equipmentMerlin ]
                       , edx = Nothing
                       }
                   ]
             }
           , { id = 1
             , url = "#"
             , material =
                   { id = 1
                   , url = "#"
                   , publicId = "200401$ZZ109-5"
                   , protocol = Nothing
                   , materialExternal =
                       Just
                           { id = 1
                           , url = "#"
                           , createdAt = now
                           , updatedAt = now
                           , author = authorAlice
                           , materialType = Membrane
                           , comment = ""
                           , editAllowed = True
                           }
                   }
             , comment = ""
             , preparations =
                   [ Crosssection
                       { position = 3
                       , materialId = "200401$ZZ109-5"
                       , comment = Nothing
                       }
                       { imsAnalysis = False
                       , break = Just RoomTemperature
                       , ionMilling =
                           Just
                               { ionMillingDevice = equipmentPecs
                               , ionMillingEnergy = "4/4,5"
                               , ionMillingTemperature = "-120"
                               , ionMillingTime = "1,5/5,5"
                               }
                       , ultramicrotome = Nothing
                       , diamondWireSaw =
                           Just
                               { diamondWireSawDevice = equipmentWell3032
                               , wireThickness = "0.3"
                               , wireGrain = "60"
                               }
                       , cryoSem = Nothing
                       , criticalPointDrying = Nothing
                       , coating = Nothing
                       , sem = Just [ equipmentLeo ]
                       , edx = Just [ equipmentEdxXmax80 ]
                       }
                   ]
             }
        -}
       ]
-}
{-
   equipmentMed : Equipment
   equipmentMed =
       { id = 1
       , kind = Api.Equipment.CoatingDevice
       , location = { name = "Building 46, Room 019" }
       , name = "BAL-TEC MED 020"
       , slug = "med020"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }


   equipmentSafematic : Equipment
   equipmentSafematic =
       { id = 1
       , kind = Api.Equipment.CoatingDevice
       , location = { name = "Building 46, Room 019" }
       , name = "Safematic CCU-010"
       , slug = "safematic"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }


   equipmentAce : Equipment
   equipmentAce =
       { id = 1
       , kind = Api.Equipment.CoatingDevice
       , location = { name = "Building 46, Room 019" }
       , name = "Leica EM ACE 600"
       , slug = "ace600"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }


   equipmentPecs : Equipment
   equipmentPecs =
       { id = 1
       , kind = Api.Equipment.CoatingDevice
       , location = { name = "Building 46, Room 019" }
       , name = "Gatan PECS II"
       , slug = "ionMilling"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }


   equipmentMerlin : Equipment
   equipmentMerlin =
       { id = 1
       , kind = Api.Equipment.Sem
       , location = { name = "Building 46, Room 015" }
       , name = "ZEISS Merlin"
       , slug = "merlin"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }


   equipmentLeo : Equipment
   equipmentLeo =
       { id = 1
       , kind = Api.Equipment.Sem
       , location = { name = "Building 46, Room 013" }
       , name = "ZEISS LEO 1550 VP"
       , slug = "leo"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }


   equipmentLeoCryo : Equipment
   equipmentLeoCryo =
       { id = 1
       , kind = Api.Equipment.Sem
       , location = { name = "Building 46, Room 013" }
       , name = "ZEISS LEO 1550 VP in cryo mode"
       , slug = "leo-cryo"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }


   equipmentEdxExtreme : Equipment
   equipmentEdxExtreme =
       { id = 1
       , kind = Api.Equipment.Edx
       , location = { name = "Building 46, Room 015" }
       , name = "Oxford Instruments X-Max Extreme"
       , slug = "x-max-extreme"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }


   equipmentEdxXmax150 : Equipment
   equipmentEdxXmax150 =
       { id = 1
       , kind = Api.Equipment.Edx
       , location = { name = "Building 46, Room 015" }
       , name = "Oxford Instruments X-Max 150 mm²"
       , slug = "x-max-150"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }


   equipmentEdxXmax80 : Equipment
   equipmentEdxXmax80 =
       { id = 1
       , kind = Api.Equipment.Edx
       , location = { name = "Building 46, Room 013" }
       , name = "Oxford Instruments X-Max 80 mm²"
       , slug = "x-max-80"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }


   equipmentUct : Equipment
   equipmentUct =
       { id = 1
       , kind = Api.Equipment.Ultramicrotome
       , location = { name = "Building 46, Room 014" }
       , name = "Leica Ultracut UCT + EM FCS"
       , slug = "uct"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }


   equipmentWell3032 : Equipment
   equipmentWell3032 =
       { id = 1
       , kind = Api.Equipment.DiamondWireSaw
       , location = { name = "Building 46, Room 020" }
       , name = "Well Diamantdrahtsäge Typ 3032-4"
       , slug = "well-3032"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }
-}
{- new?
   equipmentMed : Equipment.Data
   equipmentMed =
       { id = Id.fromInt 1
       , kind = Equipment.CoatingDevice
       , location = { name = "Building 46, Room 019" }
       , name = "BAL-TEC MED 020"
       , slug = "med020"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }


   equipmentSafematic : Equipment.Data
   equipmentSafematic =
       { id = Id.fromInt 1
       , kind = Equipment.CoatingDevice
       , location = { name = "Building 46, Room 019" }
       , name = "Safematic CCU-010"
       , slug = "safematic"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }


   equipmentAce : Equipment.Data
   equipmentAce =
       { id = Id.fromInt 1
       , kind = Equipment.CoatingDevice
       , location = { name = "Building 46, Room 019" }
       , name = "Leica EM ACE 600"
       , slug = "ace600"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }


   equipmentPecs : Equipment.Data
   equipmentPecs =
       { id = Id.fromInt 1
       , kind = Equipment.CoatingDevice
       , location = { name = "Building 46, Room 019" }
       , name = "Gatan PECS II"
       , slug = "ionMilling"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }


   equipmentMerlin : Equipment.Data
   equipmentMerlin =
       { id = Id.fromInt 1
       , kind = Equipment.Sem
       , location = { name = "Building 46, Room 015" }
       , name = "ZEISS Merlin"
       , slug = "merlin"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }


   equipmentLeo : Equipment.Data
   equipmentLeo =
       { id = Id.fromInt 1
       , kind = Equipment.Sem
       , location = { name = "Building 46, Room 013" }
       , name = "ZEISS LEO 1550 VP"
       , slug = "leo"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }


   equipmentLeoCryo : Equipment.Data
   equipmentLeoCryo =
       { id = Id.fromInt 1
       , kind = Equipment.Sem
       , location = { name = "Building 46, Room 013" }
       , name = "ZEISS LEO 1550 VP in cryo mode"
       , slug = "leo-cryo"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }


   equipmentEdxExtreme : Equipment.Data
   equipmentEdxExtreme =
       { id = Id.fromInt 1
       , kind = Equipment.SpectrometerEdx
       , location = { name = "Building 46, Room 015" }
       , name = "Oxford Instruments X-Max Extreme"
       , slug = "x-max-extreme"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }


   equipmentEdxXmax150 : Equipment.Data
   equipmentEdxXmax150 =
       { id = Id.fromInt 1
       , kind = Equipment.SpectrometerEdx
       , location = { name = "Building 46, Room 015" }
       , name = "Oxford Instruments X-Max 150 mm²"
       , slug = "x-max-150"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }


   equipmentEdxXmax80 : Equipment.Data
   equipmentEdxXmax80 =
       { id = Id.fromInt 1
       , kind = Equipment.SpectrometerEdx
       , location = { name = "Building 46, Room 013" }
       , name = "Oxford Instruments X-Max 80 mm²"
       , slug = "x-max-80"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }


   equipmentUct : Equipment.Data
   equipmentUct =
       { id = Id.fromInt 1
       , kind = Equipment.Ultramicrotome
       , location = { name = "Building 46, Room 014" }
       , name = "Leica Ultracut UCT + EM FCS"
       , slug = "uct"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }


   equipmentWell3032 : Equipment.Data
   equipmentWell3032 =
       { id = Id.fromInt 1
       , kind = Equipment.DiamondWireSaw
       , location = { name = "Building 46, Room 020" }
       , name = "Well Diamantdrahtsäge Typ 3032-4"
       , slug = "well-3032"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }


   equipmentTecnaiF20 : Equipment.Data
   equipmentTecnaiF20 =
       { id = Id.fromInt 1
       , kind = Equipment.Tem
       , location = { name = "Building 46, Room 012" }
       , name = "FEI Tecnai F20 G2 D433"
       , slug = "tecnai"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }


   equipmentSolarus : Equipment.Data
   equipmentSolarus =
       { id = Id.fromInt 1
       , kind = Equipment.Plasmacleaner
       , location = { name = "Building 46, Room 019" }
       , name = "Gatan Solarus"
       , slug = "plasmacleaner"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }


   equipmentElma : Equipment.Data
   equipmentElma =
       { id = Id.fromInt 1
       , kind = Equipment.UltrasonicBath
       , location = { name = "Building 46, Room 020" }
       , name = "Elma Schmidbauer GmbH Elmasonic P"
       , slug = "ultrasonic-bath"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }


   equipmentCryoplunger : Equipment.Data
   equipmentCryoplunger =
       { id = Id.fromInt 1
       , kind = Equipment.Cryoplunger
       , location = { name = "Building 46, Room 019" }
       , name = "Gatan CP3"
       , slug = "cryoplunger"
       , authors = []
       , image = Nothing
       , furnaceMaxTemperatureCelsius = Nothing
       , furnaceMaxTemperatureMeltCelsius = Nothing
       , furnaceCrucibleVolumeLitre = Nothing
       , createDocumentAllowed = True
    , createUserAllowed = True
       }

-}
{-
   authorAlice : Author
   authorAlice =
       { id = 1
       , user = userAlice
       }
-}
{-
   authorDetailAlice : AuthorDetail
   authorDetailAlice =
       { id = Id.fromInt 1
       , user = userAlice
       , permissions =
           { materialExternal = True
           }
       }
-}


units : List String
units =
    [ "ui-atom-unit--min"
    , "ui-atom-unit--mm-per-min"
    , "ui-atom-unit--rpm"
    , "ui-atom-unit--celcius"
    , "ui-atom-unit--kg"
    , "ui-atom-unit--percent"
    ]
