{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Stories.Theme.Typography exposing (story)

import Bulletproof exposing (Story)
import Bulletproof.Extra exposing (fromElmUi)
import Element
    exposing
        ( column
        , fill
        , spacing
        , width
        )
import Fluent
import Ui.Theme.Typography


story : Story
story =
    Bulletproof.story "Typography"
        (column
            [ spacing 16
            , width fill
            ]
            [ Ui.Theme.Typography.h1 (Fluent.verbatim "h1")
            , Ui.Theme.Typography.h2 (Fluent.verbatim "h2")
            , Ui.Theme.Typography.h3 (Fluent.verbatim "h3")
            , Ui.Theme.Typography.h4 (Fluent.verbatim "h4")
            , Ui.Theme.Typography.h5 (Fluent.verbatim "h5")
            , Ui.Theme.Typography.h6 (Fluent.verbatim "h6")
            , Ui.Theme.Typography.subtitle1 (Fluent.verbatim "subtitle1")
            , Ui.Theme.Typography.subtitle2 (Fluent.verbatim "subtitle2")
            , Ui.Theme.Typography.body1 (Fluent.verbatim "body1")
            , Ui.Theme.Typography.link1 { url = "#", label = Fluent.verbatim "link1" }
            , Ui.Theme.Typography.body2 (Fluent.verbatim "body2")
            , Ui.Theme.Typography.link2 { url = "#", label = Fluent.verbatim "link2" }
            , Ui.Theme.Typography.button (Fluent.verbatim "button")
            , Ui.Theme.Typography.caption (Fluent.verbatim "caption")
            , Ui.Theme.Typography.overline (Fluent.verbatim "overline")
            ]
            |> fromElmUi
        )
