{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Stories.Theme.Card exposing (story)

import Bulletproof exposing (Story)
import Bulletproof.Extra exposing (fromElmUi)
import Bulletproof.Knob
import Element
    exposing
        ( el
        , fill
        , height
        , none
        , px
        , width
        )
import Ui.Theme.Card as Card exposing (card)


story : Story
story =
    Bulletproof.story "Card"
        (\cardType ->
            none
                |> el
                    [ width fill
                    , height (px 360)
                    ]
                |> card
                |> Card.withType cardType
                |> Card.toElement
                |> fromElmUi
        )
        |> Bulletproof.Knob.radio "withType"
            [ ( "Elevated", Card.Elevated )
            , ( "Filled", Card.Filled )
            , ( "Outlined", Card.Outlined )
            ]
