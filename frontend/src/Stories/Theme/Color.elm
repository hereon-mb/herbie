{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Stories.Theme.Color exposing (story)

import Bulletproof exposing (Story)
import Bulletproof.Extra exposing (fromElmUi)
import Element
    exposing
        ( Color
        , Element
        , alignBottom
        , alignLeft
        , alignRight
        , alignTop
        , column
        , el
        , fill
        , fillPortion
        , height
        , padding
        , px
        , row
        , spacing
        , text
        , width
        )
import Element.Background as Background
import Element.Font as Font
import Ui.Theme.Color
    exposing
        ( error
        , errorContainer
        , onError
        , onErrorContainer
        , onPrimary
        , onPrimaryContainer
        , onSecondary
        , onSecondaryContainer
        , onSurface
        , onSurfaceVariant
        , outline
        , outlineVariant
        , primary
        , primaryContainer
        , secondary
        , secondaryContainer
        , surface
        , surfaceBright
        , surfaceContainer
        , surfaceContainerHigh
        , surfaceContainerHighest
        , surfaceContainerLow
        , surfaceContainerLowest
        , surfaceDim
        , surfaceVariant
        )


story : Story
story =
    [ colorsAccent
    , colorsSurface
    ]
        |> column
            [ width fill
            , spacing 16
            ]
        |> el
            [ width fill
            , height fill
            , padding 16
            , Background.color surface
            ]
        |> fromElmUi
        |> Bulletproof.story "Color"


colorsAccent : Element msg
colorsAccent =
    [ [ [ boxDouble "Primary" "P-40" primary "On Primary" "P-100" onPrimary
        , boxDouble "Secondary" "S-40" secondary "On Secondary" "S-100" onSecondary
        ]
            |> row
                [ width (fillPortion 2)
                , spacing 8
                ]
      , boxDouble "Error" "E-40" error "On Error" "E-100" onError
            |> el [ width (fillPortion 1) ]
      ]
        |> row
            [ width fill
            , spacing 16
            ]
    , [ [ boxDouble "Primary Container" "P-90" primaryContainer "On Primary Container" "P-10" onPrimaryContainer
        , boxDouble "Secondary Container" "S-90" secondaryContainer "On Secondary Container" "S-10" onSecondaryContainer
        ]
            |> row
                [ width (fillPortion 2)
                , spacing 8
                ]
      , boxDouble "Error Container" "E-90" errorContainer "On Error Container" "E-10" onErrorContainer
            |> el [ width (fillPortion 1) ]
      ]
        |> row
            [ width fill
            , spacing 16
            ]
    ]
        |> column
            [ width fill
            , spacing 8
            ]


colorsSurface : Element msg
colorsSurface =
    [ [ box "Surface Dim" "N-87" surfaceDim onSurface
      , box "Surface" "N-98" surface onSurface
      , box "Surface Bright" "N-98" surfaceBright onSurface
      ]
        |> row [ width fill ]
    , [ box "Surface Container Lowest" "N-100" surfaceContainerLowest onSurface
      , box "Surface Container Low" "N-96" surfaceContainerLow onSurface
      , box "Surface Container " "N-94" surfaceContainer onSurface
      , box "Surface Container High" "N-92" surfaceContainerHigh onSurface
      , box "Surface Container Highest" "N-90" surfaceContainerHighest onSurface
      ]
        |> row [ width fill ]
    , [ box "On Surface" "N-10" onSurface surface
      , box "On Surface Variant" "NV-30" onSurfaceVariant surfaceVariant
      , box "Outline" "NV-50" outline surfaceVariant
      , box "Outline Variant" "NV-80" outlineVariant surfaceVariant
      ]
        |> row [ width fill ]
    ]
        |> column
            [ width fill
            , spacing 8
            ]


boxDouble : String -> String -> Color -> String -> String -> Color -> Element msg
boxDouble label labelSecondary color labelOn labelOnSecondary colorOn =
    [ box label labelSecondary color colorOn
    , boxLow labelOn labelOnSecondary colorOn color
    ]
        |> column [ width fill ]


box : String -> String -> Color -> Color -> Element msg
box label labelSecondary color colorSecondary =
    [ text label
        |> el
            [ alignTop
            , alignLeft
            , Font.color colorSecondary
            , padding 8
            ]
    , text labelSecondary
        |> el
            [ alignBottom
            , alignRight
            , Font.color colorSecondary
            , padding 8
            ]
    ]
        |> row
            [ width fill
            , height (px 96)
            , Background.color color
            , Font.color colorSecondary
            , Font.size 16
            ]


boxLow : String -> String -> Color -> Color -> Element msg
boxLow label labelSecondary color colorSecondary =
    [ text label
        |> el
            [ alignLeft
            , Font.color colorSecondary
            , padding 8
            ]
    , text labelSecondary
        |> el
            [ alignRight
            , Font.color colorSecondary
            , padding 8
            ]
    ]
        |> row
            [ width fill
            , height (px 48)
            , Background.color color
            , Font.color colorSecondary
            , Font.size 16
            ]
