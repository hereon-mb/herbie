{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Stories.Sandbox.Animations exposing (story)

import Bulletproof exposing (Story)
import Bulletproof.Extra exposing (fromElmUi)
import Bulletproof.Knob
import Element
    exposing
        ( clipX
        , el
        , fill
        , height
        , none
        , px
        , row
        , text
        , width
        )
import Element.Background as Background
import Element.Extra
    exposing
        ( style
        )
import Ui.Theme.Color
    exposing
        ( primary
        )


story : Story
story =
    Bulletproof.folder "Animations"
        [ Bulletproof.story "expand-collapse"
            (\expanded ->
                row
                    [ width fill
                    , height (px 128)
                    ]
                    (if expanded then
                        [ el
                            [ width fill
                            , height fill
                            , Background.color primary
                            , clipX
                            ]
                            (text "aldskfj aldkf jdalfkjda flakd jfldakf jaldkf jadlkf jadlfk ajdlfka djfla kdfja ldskfja lfkjadslkfjads lfkajds flakjds f")
                        , el
                            [ width (px 32)
                            , height fill
                            , style "transition" "flex-grow 300ms"
                            ]
                            none
                        ]

                     else
                        [ el
                            [ width fill
                            , height fill
                            , Background.color primary
                            , clipX
                            ]
                            (text "aldskfj aldkf jdalfkjda flakd jfldakf jaldkf jadlkf jadlfk ajdlfka djfla kdfja ldskfja lfkjadslkfjads lfkajds flakjds f")
                        , el
                            [ width fill
                            , height fill
                            , style "transition" "flex-grow 300ms"
                            ]
                            none
                        ]
                    )
                    |> fromElmUi
            )
            |> Bulletproof.Knob.bool "Expanded" False
        ]
