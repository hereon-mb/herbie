{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Stories.Sandbox.ElmUiBugs exposing (story)

import Bulletproof exposing (Story)
import Bulletproof.Extra exposing (fromElmUi)
import Element
    exposing
        ( el
        , fill
        , height
        , image
        , none
        , padding
        , px
        , row
        , scrollbarX
        , spacing
        , width
        )
import Element.Background as Background
import Element.Extra
    exposing
        ( columnWithAncestorWithScrollbarX
        )
import Ui.Theme.Color
    exposing
        ( primary
        , secondary
        )


story : Story
story =
    let
        box =
            el
                [ width (px 256)
                , height fill
                , Background.color primary
                ]
                none

        theImage =
            image
                [ height (px 64)
                , Background.color primary
                ]
                { src = "/static/images/logo.png"
                , description = "Logo"
                }
    in
    Bulletproof.folder "Elm-UI-Bugs"
        [ Bulletproof.folder "horizontal-scrolling"
            [ Bulletproof.story "el"
                (row
                    [ width fill
                    , height fill
                    ]
                    [ columnWithAncestorWithScrollbarX
                        [ width fill
                        , height fill
                        ]
                        [ row
                            [ width fill ]
                            [ el
                                [ width fill
                                , Background.color secondary
                                , scrollbarX
                                ]
                                (row
                                    [ spacing 16
                                    , padding 16
                                    , height (px 128)
                                    ]
                                    (List.repeat 8 box)
                                )
                            ]
                        ]
                    , el [ width Element.fill ] Element.none
                    ]
                    |> fromElmUi
                )
            , Bulletproof.story "image"
                (row
                    [ width fill ]
                    [ el
                        [ width fill
                        , Background.color secondary
                        , scrollbarX
                        ]
                        (row
                            [ spacing 16
                            , padding 16
                            , height (px 128)
                            ]
                            (List.repeat 8 theImage)
                        )
                    , el [ width Element.fill ] Element.none
                    ]
                    |> fromElmUi
                )
            ]
        ]
