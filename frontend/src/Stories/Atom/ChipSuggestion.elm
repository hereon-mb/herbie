{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Stories.Atom.ChipSuggestion exposing (story)

import Bulletproof exposing (Story)
import Bulletproof.Extra exposing (fromElmUi)
import Bulletproof.Knob
import Fluent
import Ui.Atom.ChipSuggestion as ChipSuggestion exposing (chipSuggestion)
import Ui.Atom.Icon as Icon


story : Story
story =
    Bulletproof.story "ChipSuggestion"
        (\withIcon elevated enabled primary ->
            Fluent.verbatim "Suggestion chip"
                |> chipSuggestion ()
                |> (if withIcon then
                        ChipSuggestion.withIcon Icon.FilterAlt

                    else
                        identity
                   )
                |> (if elevated then
                        ChipSuggestion.elevated

                    else
                        identity
                   )
                |> ChipSuggestion.withEnabled enabled
                |> (if primary then
                        ChipSuggestion.primary

                    else
                        identity
                   )
                |> ChipSuggestion.toElement
                |> fromElmUi
        )
        |> Bulletproof.Knob.bool "withIcon" False
        |> Bulletproof.Knob.bool "elevated" False
        |> Bulletproof.Knob.bool "enabled" True
        |> Bulletproof.Knob.bool "primary" False
