{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Stories.Atom.Button exposing (story)

import Bulletproof exposing (Renderer, Story)
import Bulletproof.Extra exposing (fromElmUi)
import Bulletproof.Knob as Knob
import Element
import Fluent
import Ui.Atom.Button as Button exposing (button)
import Ui.Atom.Icon as Icon


story : Story
story =
    Bulletproof.story "Button" buttonRenderer
        |> Knob.radio "Format"
            [ ( "Contained", Button.Contained )
            , ( "Outlined", Button.Outlined )
            , ( "Text", Button.Text )
            ]
        |> Knob.bool "LeadingIcon" False
        |> Knob.bool "Fill" False
        |> Knob.bool "Enabled" True
        |> Knob.bool "Narrow" False


buttonRenderer : Button.Format -> Bool -> Bool -> Bool -> Bool -> Renderer
buttonRenderer format withLeadingIcon fill enabled narrow =
    let
        addLeadingIcon =
            if withLeadingIcon then
                Button.withLeadingIcon Icon.Create

            else
                identity

        addNarrow =
            if narrow then
                Button.narrow

            else
                identity
    in
    button () (Fluent.verbatim "Action")
        |> Button.withFormat format
        |> addLeadingIcon
        |> Button.withWidth
            (if fill then
                Element.fill

             else
                Element.shrink
            )
        |> Button.withEnabled enabled
        |> addNarrow
        |> Button.view
        |> fromElmUi
