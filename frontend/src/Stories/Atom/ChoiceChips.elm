{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Stories.Atom.ChoiceChips exposing (story)

import Bulletproof exposing (Renderer, Story)
import Bulletproof.Extra exposing (fromElmUi)
import Bulletproof.Knob
import Fluent
import Pointer
import Stories.Fixture as Fixture
import Ui.Atom.ChoiceChips as ChoiceChips exposing (choiceChips)


story : Story
story =
    let
        selections =
            ( "Nothing", Nothing )
                :: List.map toSelection Fixture.units

        toSelection fruit =
            ( fruit, Just fruit )
    in
    Bulletproof.story "ChoiceChips" choiceChipsRenderer
        |> Bulletproof.Knob.select "Selection" selections


choiceChipsRenderer : Maybe String -> Renderer
choiceChipsRenderer selected =
    let
        chips =
            List.map toChip Fixture.units

        toChip unit =
            { option = unit
            , label = Fluent.text unit
            }
    in
    choiceChips
        { chips = chips
        , label = ChoiceChips.labelAbove (Fluent.text "ui-molecule-protocols-casting--quality--heading")
        , selected = selected
        , onChange = \_ -> ()
        , onCopy = Nothing
        , onPaste = Nothing
        , toString = identity
        , fromString = Just
        , id = Pointer.field "fuits-choice-chips"
        , help = []
        }
        |> fromElmUi
