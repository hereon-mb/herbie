{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Stories.Atom.ChipInput exposing (story)

import Bulletproof exposing (Story)
import Bulletproof.Extra exposing (fromElmUi)
import Bulletproof.Knob
import Fluent
import Ui.Atom.ChipInput as ChipInput exposing (chipInput)
import Ui.Atom.Icon as Icon


story : Story
story =
    Bulletproof.story "ChipInput"
        (\selected withIcon withOnPress withOnPressRemove enabled ->
            Fluent.verbatim "Filter chip"
                |> chipInput
                |> (if selected then
                        ChipInput.selected

                    else
                        identity
                   )
                |> (if withIcon then
                        ChipInput.withIcon Icon.FilterAlt

                    else
                        identity
                   )
                |> (if withOnPress then
                        ChipInput.withOnPress ()

                    else
                        identity
                   )
                |> (if withOnPressRemove then
                        ChipInput.withOnPressRemove ()

                    else
                        identity
                   )
                |> ChipInput.withEnabled enabled
                |> ChipInput.toElement
                |> fromElmUi
        )
        |> Bulletproof.Knob.bool "selected" False
        |> Bulletproof.Knob.bool "withIcon" False
        |> Bulletproof.Knob.bool "withOnPress" True
        |> Bulletproof.Knob.bool "withOnPressRemove" True
        |> Bulletproof.Knob.bool "enabled" True
