{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Stories.Atom.ButtonToggle exposing (story)

import Bulletproof exposing (Story)
import Bulletproof.Extra exposing (fromElmUi)
import Bulletproof.Knob as Knob
import Fluent
import Ui.Atom.ButtonToggle as ButtonToggle
import Ui.Atom.Icon as Icon exposing (Icon)


story : Story
story =
    let
        toOption option =
            ButtonToggle.optionFluent option (Fluent.verbatim option)

        toOptionIcon ( option, icon ) =
            ButtonToggle.optionIcon option icon

        toRadioOption option =
            ( option, option )
    in
    Bulletproof.folder "ButtonToggle"
        [ (\selected ->
            ButtonToggle.view
                { onChange = always ()
                , selected = selected
                , groups =
                    [ ButtonToggle.group (List.map toOption optionsShort)
                    , ButtonToggle.group (List.map toOption optionsLong)
                    ]
                , label = "Filter type"
                , orientation = ButtonToggle.Horizontal
                }
                |> fromElmUi
          )
            |> Bulletproof.story "Fluent"
            |> Knob.radio "Selected" (List.map toRadioOption (optionsShort ++ optionsLong))
        , (\selected ->
            ButtonToggle.view
                { onChange = always ()
                , selected = selected
                , groups = [ ButtonToggle.group (List.map toOptionIcon optionsIcon) ]
                , label = "Filter type"
                , orientation = ButtonToggle.Horizontal
                }
                |> fromElmUi
          )
            |> Bulletproof.story "Icon"
            |> Knob.radio "Selected" (List.map (Tuple.first >> toRadioOption) optionsIcon)
        ]


optionsShort : List String
optionsShort =
    [ "="
    , "≠"
    , "≤"
    , "<"
    , ">"
    , "≥"
    ]


optionsLong : List String
optionsLong =
    [ "< … <"
    , "≤ … ≤"
    ]


optionsIcon : List ( String, Icon )
optionsIcon =
    [ ( "IsTrue", Icon.CheckBox )
    , ( "IsFalse", Icon.CheckBoxOutlineBlank )
    , ( "IsUnknown", Icon.IndeterminateCheckBox )
    ]
