{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Stories.Atom.Icon exposing (story)

import Bulletproof exposing (Story)
import Bulletproof.Extra exposing (fromElmUi)
import Bulletproof.Knob
import Ui.Atom.Icon as Icon


story : Story
story =
    let
        menuIcons =
            [ ( "Journal", Icon.Book )
            , ( "Materials", Icon.ScatterPlot )
            , ( "Equipment", Icon.DesktopWindows )
            , ( "Maintenances", Icon.Build )
            , ( "Sensors", Icon.Timeline )
            , ( "SOPs", Icon.Assignment )
            , ( "Locations", Icon.Room )
            , ( "Projects", Icon.GroupWork )
            , ( "Users", Icon.People )
            , ( "Groups", Icon.Groups )
            ]
    in
    Bulletproof.folder "Icon"
        [ Bulletproof.story "small" (Icon.viewSmall >> fromElmUi)
            |> Bulletproof.Knob.select "Name" menuIcons
        , Bulletproof.story "normal" (Icon.viewNormal >> fromElmUi)
            |> Bulletproof.Knob.select "Name" menuIcons
        ]
