{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Stories.Atom.ButtonIcon exposing (story)

import Bulletproof exposing (Renderer, Story)
import Bulletproof.Extra exposing (fromElmUi)
import Bulletproof.Knob as Knob
import Ui.Atom.ButtonIcon as ButtonIcon exposing (buttonIcon)
import Ui.Atom.Icon as Icon


story : Story
story =
    Bulletproof.story "ButtonIcon" buttonIconRenderer
        |> Knob.bool "Small" False
        |> Knob.bool "Enabled" True
        |> Knob.bool "OnDark" False


buttonIconRenderer : Bool -> Bool -> Bool -> Renderer
buttonIconRenderer small enabled onDark =
    let
        addOnDark =
            if onDark then
                ButtonIcon.onDark

            else
                identity

        addSmall =
            if small then
                ButtonIcon.small

            else
                identity
    in
    buttonIcon () Icon.Book
        |> addOnDark
        |> ButtonIcon.withEnabled enabled
        |> addSmall
        |> ButtonIcon.toElement
        |> fromElmUi
