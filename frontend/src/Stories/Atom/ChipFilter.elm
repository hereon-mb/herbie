{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Stories.Atom.ChipFilter exposing (story)

import Bulletproof exposing (Story)
import Bulletproof.Extra exposing (fromElmUi)
import Bulletproof.Knob
import Fluent
import Ui.Atom.ChipFilter as ChipFilter exposing (chipFilter)
import Ui.Atom.Icon as Icon


story : Story
story =
    Bulletproof.story "ChipFilter"
        (\selected withIcon elevated connectedOnLeft connectedOnRight enabled dropdown ->
            Fluent.verbatim "Filter chip"
                |> chipFilter () selected
                |> (if withIcon then
                        ChipFilter.withIcon Icon.FilterAlt

                    else
                        identity
                   )
                |> (if elevated then
                        ChipFilter.elevated

                    else
                        identity
                   )
                |> (if connectedOnLeft then
                        ChipFilter.connectedOnLeft

                    else
                        identity
                   )
                |> (if connectedOnRight then
                        ChipFilter.connectedOnRight

                    else
                        identity
                   )
                |> ChipFilter.withEnabled enabled
                |> (case dropdown of
                        Nothing ->
                            identity

                        Just open ->
                            ChipFilter.withDropdown open
                   )
                |> ChipFilter.toElement
                |> fromElmUi
        )
        |> Bulletproof.Knob.bool "selected" False
        |> Bulletproof.Knob.bool "withIcon" False
        |> Bulletproof.Knob.bool "elevated" False
        |> Bulletproof.Knob.bool "connectedOnLeft" False
        |> Bulletproof.Knob.bool "connectedOnRight" False
        |> Bulletproof.Knob.bool "enabled" True
        |> Bulletproof.Knob.radio "withDropdown"
            [ ( "no", Nothing )
            , ( "open", Just True )
            , ( "closed", Just False )
            ]
