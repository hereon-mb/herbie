{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Effect exposing
    ( Effect, none, map, batch
    , fromCmd, fromAction, fromMsg
    )

{-|

@docs Effect, none, map, batch
@docs fromCmd, fromAction, fromMsg

-}

import Action exposing (Action)
import Effect.Internal


type alias Effect msg =
    Effect.Internal.Effect (Action msg) msg


none : Effect msg
none =
    Effect.Internal.none


map : (a -> b) -> Effect a -> Effect b
map fn =
    Effect.Internal.map fn (Action.map fn)


fromCmd : Cmd msg -> Effect msg
fromCmd =
    Effect.Internal.fromCmd


fromAction : Action msg -> Effect msg
fromAction =
    Effect.Internal.fromAction


fromMsg : msg -> Effect msg
fromMsg =
    Effect.Internal.fromMsg


batch : List (Effect msg) -> Effect msg
batch =
    Effect.Internal.batch
