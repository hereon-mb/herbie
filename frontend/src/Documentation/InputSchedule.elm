module Documentation.InputSchedule exposing (all)

import Shacl.Documentation as Documentation exposing (Documentation)
import Tree exposing (Tree, tree)


all : Tree Documentation
all =
    tree
        (Documentation.fromInfo
            { headingFull = "Schedule"
            , headingShort = "Schedule"
            , description = ""
            }
        )
        [ tree required []
        , tree withDate []
        ]


required : Documentation
required =
    { headingFull = "Required Schedule"
    , headingShort = "Required"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShapeDate> ;
                        sh:property <PropertyShapeNested> ;
                    .
                        <PropertyShapeDate> sh:order 0 .
                        <PropertyShapeNested> sh:order 1 .

                    <PropertyShapeDate> a sh:PropertyShape ;
                        sh:path showcase:hasDate ;
                        sh:name "date"@en ;
                        sh:datatype xsd:date ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    <PropertyShapeNested> a sh:PropertyShape ;
                        sh:path showcase:hasNested ;
                        sh:name "name"@en ;
                        sh:class showcase:Nested ;
                        sh:node <NodeShapeNested> ;
                        dash:editor [
                            a hash:ScheduleEditor ;
                            hash:scheduleEditorDayOffsetPath showcase:hasDayOffset ;
                            hash:scheduleEditorOrderPath showcase:hasOrder ;
                            hash:scheduleEditorTitlePath rdfs:label ;
                            hash:scheduleEditorDatePath showcase:hasDate ;
                        ] ;
                        sh:minCount 1 ;
                    .

                    <NodeShapeNested> a sh:NodeShape ;
                        sh:property [
                            sh:path rdfs:label ;
                            sh:name "label"@en ;
                            sh:order 0 ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                        sh:property [
                            sh:path showcase:hasValueA ;
                            sh:name "name property a"@en ;
                            sh:order 1 ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                        sh:property [
                            sh:path showcase:hasValueB ;
                            sh:name "name property b"@en ;
                            sh:order 2 ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }


withDate : Documentation
withDate =
    { headingFull = "Schedule with Date"
    , headingShort = "With Date"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShapeDate> ;
                        sh:property <PropertyShapeNested> ;
                    .
                        <PropertyShapeDate> sh:order 0 .
                        <PropertyShapeNested> sh:order 1 .

                    <PropertyShapeDate> a sh:PropertyShape ;
                        sh:path showcase:hasDate ;
                        sh:name "date"@en ;
                        sh:datatype xsd:date ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    <PropertyShapeNested> a sh:PropertyShape ;
                        sh:path showcase:hasNested ;
                        sh:name "name"@en ;
                        sh:class showcase:Nested ;
                        sh:node <NodeShapeNested> ;
                        dash:editor [
                            a hash:ScheduleEditor ;
                            hash:scheduleEditorDayOffsetPath showcase:hasDayOffset ;
                            hash:scheduleEditorOrderPath showcase:hasOrder ;
                            hash:scheduleEditorTitlePath rdfs:label ;
                            hash:scheduleEditorDatePath showcase:hasDate ;
                        ] ;
                        sh:minCount 1 ;
                    .

                    <NodeShapeNested> a sh:NodeShape ;
                        sh:property [
                            sh:path rdfs:label ;
                            sh:name "label"@en ;
                            sh:order 0 ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                        sh:property [
                            sh:path showcase:hasValueA ;
                            sh:name "name property a"@en ;
                            sh:order 1 ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                        sh:property [
                            sh:path showcase:hasValueB ;
                            sh:name "name property b"@en ;
                            sh:order 2 ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasDate "2025-03-03"^^xsd:date ;
                    .
                """
            }
