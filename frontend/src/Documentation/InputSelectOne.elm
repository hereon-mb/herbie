module Documentation.InputSelectOne exposing (all)

import Expect
import Shacl.Documentation as Documentation exposing (Documentation)
import Test.Html.Query as Query
import Test.Html.Query.Extra as Query
import Tree exposing (Tree, tree)


all : Tree Documentation
all =
    tree
        (Documentation.fromInfo
            { headingFull = "Single Instance Selection Fields"
            , headingShort = "Single Instance Selection"
            , description = ""
            }
        )
        [ tree
            (Documentation.fromInfo
                { headingFull = "Radio Buttons"
                , headingShort = "Radio Buttons"
                , description = ""
                }
            )
            [ tree
                (Documentation.fromInfo
                    { headingFull = "Required Radio Buttons"
                    , headingShort = "Required"
                    , description = ""
                    }
                )
                [ tree asRadioButtonsRequiredViaShIn []
                , tree asRadioButtonsRequiredViaDashEditor []
                , tree asRadioButtonsRequiredWithUnqualifiedNesting []
                , tree asRadioButtonsRequiredWithQualifiedNesting []
                ]
            ]
        , tree
            (Documentation.fromInfo
                { headingFull = "Choice Chips"
                , headingShort = "Choice Chips"
                , description = ""
                }
            )
            [ tree
                (Documentation.fromInfo
                    { headingFull = "Required Choice Chips"
                    , headingShort = "Required"
                    , description = ""
                    }
                )
                [ tree asChoiceChipsRequiredViaDashEditor []
                , tree asChoiceChipsRequiredWithNestingUnqualified []
                , tree asChoiceChipsRequiredWithNestingQualified []
                ]
            ]
        , tree
            (Documentation.fromInfo
                { headingFull = "Dropdown Fields"
                , headingShort = "Dropdown"
                , description = ""
                }
            )
            [ tree
                (Documentation.fromInfo
                    { headingFull = "Required Dropdown Fields"
                    , headingShort = "Required"
                    , description = ""
                    }
                )
                [ tree asDropdownRequiredViaDashEditor []
                , tree asDropdownRequiredWithUnqualifiedNesting []
                , tree asDropdownRequiredWithQualifiedNesting []
                ]
            ]
        ]


asRadioButtonsRequiredViaShIn : Documentation
asRadioButtonsRequiredViaShIn =
    { headingFull = "Required Radio Buttons via sh:in"
    , headingShort = "Via sh:in"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:class showcase:Leaf ;
                        sh:in (
                            <leafA>
                            <leafB>
                            <leafC>
                        ) ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .

                    <leafA> a showcase:Leaf ; rdfs:label "leaf A"@en .
                    <leafB> a showcase:Leaf ; rdfs:label "leaf B"@en .
                    <leafC> a showcase:Leaf ; rdfs:label "leaf C"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUi
            (Query.findAllRadioGroups
                { id = "defd251"
                , label = "Name"
                , info = Just "description"
                }
                >> Query.count (Expect.equal 1)
            )
        -- FIXME This does not work yet, as the test does not execute the
        -- effects.
        -- |> Documentation.expectUi
        --     (Query.findAllRadioGroups
        --         { id = "0"
        --         , label = "Name"
        --         , info = Just "description"
        --         }
        --         >> Query.each
        --             (Expect.all
        --                 [ Query.findAllRadioButtons
        --                     { label = "Leaf A"
        --                     , checked = False
        --                     }
        --                     >> Query.count (Expect.equal 1)
        --                 , Query.findAllRadioButtons
        --                     { label = "Leaf B"
        --                     , checked = False
        --                     }
        --                     >> Query.count (Expect.equal 1)
        --                 , Query.findAllRadioButtons
        --                     { label = "Leaf C"
        --                     , checked = False
        --                     }
        --                     >> Query.count (Expect.equal 1)
        --                 ]
        --             )
        --     )
        |> identity


asRadioButtonsRequiredViaDashEditor : Documentation
asRadioButtonsRequiredViaDashEditor =
    { headingFull = "Required Radio Buttons via dash:editor"
    , headingShort = "Via dash:editor"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:class showcase:Leaf ;
                        dash:editor dash:RadioButtonsEditor ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .

                    <leafA> a showcase:Leaf ; rdfs:label "leaf A"@en .
                    <leafB> a showcase:Leaf ; rdfs:label "leaf B"@en .
                    <leafC> a showcase:Leaf ; rdfs:label "leaf C"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUi
            (Query.findAllRadioGroups
                { id = "2e7f00d5"
                , label = "Name"
                , info = Just "description"
                }
                >> Query.count (Expect.equal 1)
            )
        -- FIXME This does not work yet, as the test does not execute the
        -- effects.
        -- |> Documentation.expectUi
        --     (Query.findAllRadioGroups
        --         { id = "0"
        --         , label = "Name"
        --         , info = Just "description"
        --         }
        --         >> Query.each
        --             (Expect.all
        --                 [ Query.findAllRadioButtons
        --                     { label = "Leaf A"
        --                     , checked = False
        --                     }
        --                     >> Query.count (Expect.equal 1)
        --                 , Query.findAllRadioButtons
        --                     { label = "Leaf B"
        --                     , checked = False
        --                     }
        --                     >> Query.count (Expect.equal 1)
        --                 , Query.findAllRadioButtons
        --                     { label = "Leaf C"
        --                     , checked = False
        --                     }
        --                     >> Query.count (Expect.equal 1)
        --                 ]
        --             )
        --     )
        |> identity


asRadioButtonsRequiredWithUnqualifiedNesting : Documentation
asRadioButtonsRequiredWithUnqualifiedNesting =
    { headingFull = "Required Radio Buttons With Unqualified Nesting"
    , headingShort = "With Unqualified Nesting"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:node [
                            sh:class showcase:Leaf ;
                        ] ;
                        sh:in (
                            <leafA>
                            <leafB>
                            <leafC>
                        ) ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .

                    <leafA> a showcase:Leaf ; rdfs:label "leaf A"@en .
                    <leafB> a showcase:Leaf ; rdfs:label "leaf B"@en .
                    <leafC> a showcase:Leaf ; rdfs:label "leaf C"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUi
            (Query.findAllRadioGroups
                { id = "defd251"
                , label = "Name"
                , info = Just "description"
                }
                >> Query.count (Expect.equal 1)
            )
        -- FIXME This does not work yet, as the test does not execute the
        -- effects.
        --|> Documentation.expectUi
        --    (Query.findAllRadioGroups
        --        { id = "0"
        --        , label = "Name"
        --        , info = Just "description"
        --        }
        --        >> Query.each
        --            (Expect.all
        --                [ Query.findAllRadioButtons
        --                    { label = "Leaf A"
        --                    , checked = False
        --                    }
        --                    >> Query.count (Expect.equal 1)
        --                , Query.findAllRadioButtons
        --                    { label = "Leaf B"
        --                    , checked = False
        --                    }
        --                    >> Query.count (Expect.equal 1)
        --                , Query.findAllRadioButtons
        --                    { label = "Leaf C"
        --                    , checked = False
        --                    }
        --                    >> Query.count (Expect.equal 1)
        --                ]
        --            )
        --    )
        |> identity


asRadioButtonsRequiredWithQualifiedNesting : Documentation
asRadioButtonsRequiredWithQualifiedNesting =
    { headingFull = "Required Radio Buttons With Qualified Nesting"
    , headingShort = "With Qualified Nesting"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:qualifiedValueShape [
                            sh:class showcase:Leaf ;
                        ] ;
                        sh:in (
                            <leafA>
                            <leafB>
                            <leafC>
                        ) ;
                        sh:qualifiedMinCount 1 ;
                        sh:qualifiedMaxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .

                    <leafA> a showcase:Leaf ; rdfs:label "leaf A"@en .
                    <leafB> a showcase:Leaf ; rdfs:label "leaf B"@en .
                    <leafC> a showcase:Leaf ; rdfs:label "leaf C"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUi
            (Query.findAllRadioGroups
                { id = "defd251"
                , label = "Name"
                , info = Just "description"
                }
                >> Query.count (Expect.equal 1)
            )
        -- FIXME This does not work yet, as the test does not execute the
        -- effects.
        --|> Documentation.expectUi
        --    (Query.findAllRadioGroups
        --        { id = "0"
        --        , label = "Name"
        --        , info = Just "description"
        --        }
        --        >> Query.each
        --            (Expect.all
        --                [ Query.findAllRadioButtons
        --                    { label = "Leaf A"
        --                    , checked = False
        --                    }
        --                    >> Query.count (Expect.equal 1)
        --                , Query.findAllRadioButtons
        --                    { label = "Leaf B"
        --                    , checked = False
        --                    }
        --                    >> Query.count (Expect.equal 1)
        --                , Query.findAllRadioButtons
        --                    { label = "Leaf C"
        --                    , checked = False
        --                    }
        --                    >> Query.count (Expect.equal 1)
        --                ]
        --            )
        --    )
        |> identity


asChoiceChipsRequiredViaDashEditor : Documentation
asChoiceChipsRequiredViaDashEditor =
    { headingFull = "Required Choice Chips via dash:editor"
    , headingShort = "Via dash:editor"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:class showcase:Leaf ;
                        dash:editor dash:InstancesSelectEditor ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .

                    <leafA> a showcase:Leaf ; rdfs:label "leaf A"@en .
                    <leafB> a showcase:Leaf ; rdfs:label "leaf B"@en .
                    <leafC> a showcase:Leaf ; rdfs:label "leaf C"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUi
            (Query.findAllChoiceChipGroups {}
                >> Query.count (Expect.equal 1)
            )


asChoiceChipsRequiredWithNestingUnqualified : Documentation
asChoiceChipsRequiredWithNestingUnqualified =
    { headingFull = "Required Choice Chips With Unqualified Nesting"
    , headingShort = "With Unqualified Nesting"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:node [
                            sh:class showcase:Leaf ;
                        ] ;
                        dash:editor dash:InstancesSelectEditor ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .

                    <leafA> a showcase:Leaf ; rdfs:label "leaf A"@en .
                    <leafB> a showcase:Leaf ; rdfs:label "leaf B"@en .
                    <leafC> a showcase:Leaf ; rdfs:label "leaf C"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUi
            (Query.findAllChoiceChipGroups {}
                >> Query.count (Expect.equal 1)
            )


asChoiceChipsRequiredWithNestingQualified : Documentation
asChoiceChipsRequiredWithNestingQualified =
    { headingFull = "Required Choice Chips With Qualified Nesting"
    , headingShort = "With Qualified Nesting"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:qualifiedValueShape [
                            sh:class showcase:Leaf ;
                        ] ;
                        dash:editor dash:InstancesSelectEditor ;
                        sh:qualifiedMinCount 1 ;
                        sh:qualifiedMaxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .

                    <leafA> a showcase:Leaf ; rdfs:label "leaf A"@en .
                    <leafB> a showcase:Leaf ; rdfs:label "leaf B"@en .
                    <leafC> a showcase:Leaf ; rdfs:label "leaf C"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUi
            (Query.findAllChoiceChipGroups {}
                >> Query.count (Expect.equal 1)
            )


asDropdownRequiredViaDashEditor : Documentation
asDropdownRequiredViaDashEditor =
    { headingFull = "Required Dropdown Fields via dash:editor"
    , headingShort = "Via dash:editor"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:class showcase:Leaf ;
                        dash:editor dash:InstancesSelectEditor ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .

                    <leafA> a showcase:Leaf ; rdfs:label "leaf A"@en .
                    <leafB> a showcase:Leaf ; rdfs:label "leaf B"@en .
                    <leafC> a showcase:Leaf ; rdfs:label "leaf C"@en .
                    <leafD> a showcase:Leaf ; rdfs:label "leaf D"@en .
                    <leafE> a showcase:Leaf ; rdfs:label "leaf E"@en .
                    <leafF> a showcase:Leaf ; rdfs:label "leaf F"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        -- FIXME This does not work yet, as the test does not execute the
        -- effects.
        --|> Documentation.expectUi
        --    (Query.findAllComboboxSingleSelects {}
        --        >> Query.count (Expect.equal 1)
        --    )
        |> identity


asDropdownRequiredWithUnqualifiedNesting : Documentation
asDropdownRequiredWithUnqualifiedNesting =
    { headingFull = "Required Dropdown Fields With Unqualified Nesting"
    , headingShort = "With Unqualified Nesting"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:node [
                            sh:class showcase:Leaf ;
                        ] ;
                        dash:editor dash:InstancesSelectEditor ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .

                    <leafA> a showcase:Leaf ; rdfs:label "leaf A"@en .
                    <leafB> a showcase:Leaf ; rdfs:label "leaf B"@en .
                    <leafC> a showcase:Leaf ; rdfs:label "leaf C"@en .
                    <leafD> a showcase:Leaf ; rdfs:label "leaf D"@en .
                    <leafE> a showcase:Leaf ; rdfs:label "leaf E"@en .
                    <leafF> a showcase:Leaf ; rdfs:label "leaf F"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        -- FIXME This does not work yet, as the test does not execute the
        -- effects.
        --|> Documentation.expectUi
        --    (Query.findAllComboboxSingleSelects {}
        --        >> Query.count (Expect.equal 1)
        --    )
        |> identity


asDropdownRequiredWithQualifiedNesting : Documentation
asDropdownRequiredWithQualifiedNesting =
    { headingFull = "Required Dropdown With Qualified Nesting"
    , headingShort = "With Qualified Nesting"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:qualifiedValueShape [
                            sh:class showcase:Leaf ;
                        ] ;
                        dash:editor dash:InstancesSelectEditor ;
                        sh:qualifiedMinCount 1 ;
                        sh:qualifiedMaxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .

                    <leafA> a showcase:Leaf ; rdfs:label "leaf A"@en .
                    <leafB> a showcase:Leaf ; rdfs:label "leaf B"@en .
                    <leafC> a showcase:Leaf ; rdfs:label "leaf C"@en .
                    <leafD> a showcase:Leaf ; rdfs:label "leaf D"@en .
                    <leafE> a showcase:Leaf ; rdfs:label "leaf E"@en .
                    <leafF> a showcase:Leaf ; rdfs:label "leaf F"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        -- FIXME This does not work yet, as the test does not execute the
        -- effects.
        -- |> Documentation.expectUi
        --     (Query.findAllComboboxSingleSelects {}
        --         >> Query.count (Expect.equal 1)
        --     )
        |> identity
