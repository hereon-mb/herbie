module Documentation.InputCheckbox exposing (all)

import Shacl.Documentation as Documentation exposing (Documentation)
import Tree exposing (Tree, tree)


all : Tree Documentation
all =
    tree
        ({ headingFull = "Checkbox"
         , headingShort = "Checkbox"
         , description = ""
         }
            |> Documentation.fromInfo
        )
        [ tree boolean []
        , tree booleanWithData []
        , tree
            (Documentation.fromInfo
                { headingFull = "Checkbox For Instance Selection"
                , headingShort = "For Instance Selection"
                , description = ""
                }
            )
            [ tree instance []
            , tree instanceNestedUnqualified []
            , tree instanceNestedQualified []
            ]
        ]


boolean : Documentation
boolean =
    { headingFull = "Checkbox For Boolean Property"
    , headingShort = "For Boolean Property"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasBoolean ;
                        sh:name "boolean value"@en ;
                        sh:datatype xsd:boolean ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }


booleanWithData : Documentation
booleanWithData =
    { headingFull = "Checkbox For Boolean Property with Initial Data"
    , headingShort = "For Boolean Property with Initial Data"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasBoolean ;
                        sh:name "boolean value"@en ;
                        sh:datatype xsd:boolean ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasBoolean true ;
                    .
                """
            }


instance : Documentation
instance =
    { headingFull = "Checkbox For Instance Selection - Unnested"
    , headingShort = "Unnested"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasInstance ;
                        sh:name "instance selected"@en ;
                        sh:class showcase:Node ;
                        sh:hasValue showcase:instance ;
                        sh:minCount 0 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }


instanceNestedUnqualified : Documentation
instanceNestedUnqualified =
    { headingFull = "Checkbox For Instance Selection - Unqualified Nested"
    , headingShort = "Unqualified Nested"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasInstance ;
                        sh:name "instance selected"@en ;
                        sh:node [
                            sh:class showcase:Node ;
                            sh:hasValue showcase:instance ;
                        ] ;
                        sh:minCount 0 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }


instanceNestedQualified : Documentation
instanceNestedQualified =
    { headingFull = "Checkbox For Instance Selection - Qualified Nested"
    , headingShort = "Qualified Nested"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasInstance ;
                        sh:name "instance selected"@en ;
                        sh:qualifiedValueShape [
                            sh:class showcase:Node ;
                            sh:hasValue showcase:instance ;
                        ] ;
                        sh:qualifiedMinCount 0 ;
                        sh:qualifiedMaxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
