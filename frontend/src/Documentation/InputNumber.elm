module Documentation.InputNumber exposing (all)

import Expect
import Expect.Shacl.Form.Viewed as Viewed
import Rdf exposing (Iri)
import Rdf.Decode as Decode exposing (Decoder)
import Rdf.Namespaces as Rdf
import Shacl.Documentation as Documentation exposing (Documentation)
import Test.Html.Query as Query
import Test.Html.Query.Extra as Query
import Test.Html.Selector as Selector
import Tree exposing (Tree, tree)


all : Tree Documentation
all =
    tree
        (Documentation.fromInfo
            { headingFull = "Numerical Fields"
            , headingShort = "Numerical"
            , description = ""
            }
        )
        [ tree
            (Documentation.fromInfo
                { headingFull = "Integer Fields"
                , headingShort = "Integer"
                , description = ""
                }
            )
            [ tree integerRequired []
            , tree integerOptional []
            , tree integerWithData []
            , tree integerWithDefault []
            , tree integerWithDefaultAndData []
            ]
        , tree
            (Documentation.fromInfo
                { headingFull = "Decimal Fields"
                , headingShort = "Decimal"
                , description = ""
                }
            )
            [ tree decimalRequired []
            , tree decimalOptional []
            , tree decimalWithData []
            , tree decimalWithDefault []
            , tree decimalWithDefaultAndData []
            ]
        , tree
            (Documentation.fromInfo
                { headingFull = "Float Fields"
                , headingShort = "Float"
                , description = ""
                }
            )
            [ tree floatRequired []
            , tree floatOptional []
            , tree floatWithData []
            , tree floatWithDefault []
            , tree floatWithDefaultAndData []
            ]
        , tree
            (Documentation.fromInfo
                { headingFull = "Numerical Fields With Suffix"
                , headingShort = "With Suffix"
                , description = ""
                }
            )
            [ tree withSuffixRequired []
            , tree withSuffixOptional []
            , tree withSuffixAndClasses []
            , tree withSuffixWithData []
            ]
        , tree
            (Documentation.fromInfo
                { headingFull = "Numerical Fields With Prefix"
                , headingShort = "With Prefix"
                , description = ""
                }
            )
            [ tree withPrefixRequired []
            , tree withPrefixOptional []
            ]
        , tree
            (Documentation.fromInfo
                { headingFull = "Numerical Fields With Injected Constant"
                , headingShort = "With Constant"
                , description = ""
                }
            )
            [ tree withConstantSimple []
            , tree withConstantNested []
            ]
        , tree affixedWithinNested []
        , tree affixedWithMultipleChoices []
        , tree
            (Documentation.fromInfo
                { headingFull = "Numerical Fields for Specific Ontologies"
                , headingShort = "For Specific Ontologies"
                , description = ""
                }
            )
            [ tree unitViaOm []
            , tree unitViaQudt []
            ]
        ]


integerRequired : Documentation
integerRequired =
    { headingFull = "Required Integer Fields"
    , headingShort = "Required"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:integer ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "d634c104"
                , label = "Name"
                , info = Just "description"
                , value = ""
                }
                >> Query.count (Expect.equal 1)
            )


integerOptional : Documentation
integerOptional =
    { headingFull = "Optional Integer Fields"
    , headingShort = "Optional"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:integer ;
                        sh:minCount 0 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "d634c104"
                , label = "Name"
                , info = Just "description (Optional)"
                , value = ""
                }
                >> Query.count (Expect.equal 1)
            )


integerWithData : Documentation
integerWithData =
    { headingFull = "Integer Fields With Data"
    , headingShort = "With Data"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:integer ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasValue 42 ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "d634c104"
                , label = "Name"
                , info = Just "description"
                , value = "42"
                }
                >> Query.count (Expect.equal 1)
            )


integerWithDefault : Documentation
integerWithDefault =
    { headingFull = "Integer Fields With Default"
    , headingShort = "With Default"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:integer ;
                        sh:defaultValue 42 ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUiWithDefaultPopulation
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "d634c104"
                , label = "Name"
                , info = Just "description"
                , value = "42"
                }
                >> Query.count (Expect.equal 1)
            )


integerWithDefaultAndData : Documentation
integerWithDefaultAndData =
    { headingFull = "Integer Fields With Default and Data"
    , headingShort = "With Default and Data"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:integer ;
                        sh:defaultValue 42 ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasValue 84 ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "d634c104"
                , label = "Name"
                , info = Just "description"
                , value = "84"
                }
                >> Query.count (Expect.equal 1)
            )


decimalRequired : Documentation
decimalRequired =
    { headingFull = "Required Decimal Fields"
    , headingShort = "Required"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:decimal ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "ccb6cce1"
                , label = "Name"
                , info = Just "description"
                , value = ""
                }
                >> Query.count (Expect.equal 1)
            )


decimalOptional : Documentation
decimalOptional =
    { headingFull = "Optional Decimal Fields"
    , headingShort = "Optional"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:decimal ;
                        sh:minCount 0 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "ccb6cce1"
                , label = "Name"
                , info = Just "description (Optional)"
                , value = ""
                }
                >> Query.count (Expect.equal 1)
            )


decimalWithData : Documentation
decimalWithData =
    { headingFull = "Decimal Fields With Data"
    , headingShort = "With Data"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:decimal ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasValue 3.14 ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "ccb6cce1"
                , label = "Name"
                , info = Just "description"
                , value = "3.14"
                }
                >> Query.count (Expect.equal 1)
            )


decimalWithDefault : Documentation
decimalWithDefault =
    { headingFull = "Decimal Fields With Default"
    , headingShort = "With Default"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:decimal ;
                        sh:defaultValue 3.14 ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUiWithDefaultPopulation
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "ccb6cce1"
                , label = "Name"
                , info = Just "description"
                , value = "3.14"
                }
                >> Query.count (Expect.equal 1)
            )


decimalWithDefaultAndData : Documentation
decimalWithDefaultAndData =
    { headingFull = "Decimal Fields With Default and Data"
    , headingShort = "With Default and Data"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:decimal ;
                        sh:defaultValue 3.14 ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasValue 6.28 ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "ccb6cce1"
                , label = "Name"
                , info = Just "description"
                , value = "6.28"
                }
                >> Query.count (Expect.equal 1)
            )


floatRequired : Documentation
floatRequired =
    { headingFull = "Required Float Fields"
    , headingShort = "Required"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:double ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "77401355"
                , label = "Name"
                , info = Just "description"
                , value = ""
                }
                >> Query.count (Expect.equal 1)
            )


floatOptional : Documentation
floatOptional =
    { headingFull = "Optional Float Fields"
    , headingShort = "Optional"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:double ;
                        sh:minCount 0 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "77401355"
                , label = "Name"
                , info = Just "description (Optional)"
                , value = ""
                }
                >> Query.count (Expect.equal 1)
            )


floatWithData : Documentation
floatWithData =
    { headingFull = "Float Fields With Data"
    , headingShort = "With Data"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:double ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasValue 3.14E0 ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "77401355"
                , label = "Name"
                , info = Just "description"
                , value = "3.14"
                }
                >> Query.count (Expect.equal 1)
            )


floatWithDefault : Documentation
floatWithDefault =
    { headingFull = "Float Fields With Default"
    , headingShort = "With Default"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:double ;
                        sh:defaultValue 3.14E0 ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUiWithDefaultPopulation
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "77401355"
                , label = "Name"
                , info = Just "description"
                , value = "3.14"
                }
                >> Query.count (Expect.equal 1)
            )


floatWithDefaultAndData : Documentation
floatWithDefaultAndData =
    { headingFull = "Float Fields With Default and Data"
    , headingShort = "With Default and Data"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:double ;
                        sh:defaultValue 3.14E0 ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasValue 6.28E0 ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "77401355"
                , label = "Name"
                , info = Just "description"
                , value = "6.28"
                }
                >> Query.count (Expect.equal 1)
            )


withSuffixRequired : Documentation
withSuffixRequired =
    { headingFull = "Required Numerical Fields With Suffix"
    , headingShort = "Required"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        sh:targetClass showcase:Root ;
                        hash:documentRoot true ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasDecimalSuffixed ;
                        sh:name "suffixed decimal"@en ;
                        sh:node [
                            sh:property [
                                sh:path showcase:hasDecimal ;
                                sh:datatype xsd:decimal ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                            sh:property [
                                sh:path showcase:hasSuffix ;
                                sh:hasValue showcase:suffix ;
                                hash:include hash:Suffix ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                        ] ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .

                    showcase:suffix
                        rdfs:label "suffix"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasDecimalSuffixed [
                            rdfs:isDefinedBy <#d1f63552> ;
                            showcase:hasDecimal 3.14 ;
                            showcase:hasSuffix showcase:suffix ;
                        ] ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "d1f63552-a50414b3"
                , label = "Suffixed decimal"
                , info = Nothing
                , value = "3.14"
                }
                >> Query.count (Expect.equal 1)
            )
        |> Documentation.expectUi
            (Query.findAll
                [ Selector.exactText "Suffix" ]
                >> Query.count (Expect.equal 1)
            )


withSuffixOptional : Documentation
withSuffixOptional =
    { headingFull = "Optional Numerical Fields With Suffix"
    , headingShort = "Optional"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        sh:targetClass showcase:Root ;
                        hash:documentRoot true ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasDecimalSuffixed ;
                        sh:name "suffixed decimal"@en ;
                        sh:node [
                            sh:property [
                                sh:path showcase:hasDecimal ;
                                sh:datatype xsd:decimal ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                            sh:property [
                                sh:path showcase:hasSuffix ;
                                sh:hasValue showcase:suffix ;
                                hash:include hash:Suffix ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                        ] ;
                        sh:minCount 0 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .

                    showcase:suffix
                        rdfs:label "suffix"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasDecimalSuffixed [
                            rdfs:isDefinedBy <#d1f63552> ;
                            showcase:hasDecimal 3.14 ;
                            showcase:hasSuffix hash:suffix ;
                        ] ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "d1f63552-a50414b3"
                , label = "Suffixed decimal"
                , info = Just "Optional"
                , value = "3.14"
                }
                >> Query.count (Expect.equal 1)
            )
        |> Documentation.expectUi
            (Query.findAll
                [ Selector.exactText "Suffix" ]
                >> Query.count (Expect.equal 1)
            )


withSuffixAndClasses : Documentation
withSuffixAndClasses =
    { headingFull = "Numerical Fields With Suffix and a Specified Class"
    , headingShort = "With Suffix and Class"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        sh:targetClass showcase:Root ;
                        hash:documentRoot true ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasNested ;
                        sh:name "suffixed decimal"@en ;
                        sh:node [
                            sh:class showcase:Nested ;
                            sh:property [
                                sh:path showcase:hasDecimalSuffixed ;
                                sh:node [
                                    sh:class showcase:Node ;
                                    sh:property [
                                        sh:path showcase:hasDecimal ;
                                        sh:datatype xsd:decimal ;
                                        sh:minCount 1 ;
                                        sh:maxCount 1 ;
                                    ] ;
                                    sh:property [
                                        sh:path showcase:hasSuffix ;
                                        sh:hasValue showcase:suffix ;
                                        hash:include hash:Suffix ;
                                        sh:minCount 1 ;
                                        sh:maxCount 1 ;
                                    ] ;
                                ] ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                        ] ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .

                    showcase:suffix
                        rdfs:label "suffix"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasNested [
                            a showcase:Nested ;
                            rdfs:isDefinedBy <#c10886d8> ;
                            showcase:hasDecimalSuffixed [
                                a showcase:Node ;
                                rdfs:isDefinedBy <#c10886d8-bd98d6b9> ;
                                showcase:hasDecimal 3.14 ;
                                showcase:hasSuffix showcase:suffix ;
                            ] ;
                        ] ;
                    .
                """
            }
        |> Documentation.expectGraph
            (Decode.decode
                (Decode.from
                    (Viewed.document "root")
                    (decodeType (Viewed.showcase "Root"))
                )
                >> Result.map (Expect.equal ())
            )
        |> Documentation.expectGraph
            (Decode.decode
                (Decode.from
                    (Viewed.document "root")
                    (Decode.predicate (Viewed.showcase "hasNested")
                        (decodeType (Viewed.showcase "Nested"))
                    )
                )
                >> Result.map (Expect.equal ())
            )
        |> Documentation.expectGraph
            (Decode.decode
                (Decode.from
                    (Viewed.document "root")
                    (Decode.predicate (Viewed.showcase "hasNested")
                        (Decode.predicate (Viewed.showcase "hasDecimalSuffixed")
                            (decodeType (Viewed.showcase "Node"))
                        )
                    )
                )
                >> Result.map (Expect.equal ())
            )


withSuffixWithData : Documentation
withSuffixWithData =
    { headingFull = "Required Numerical Fields With Suffix and Initial Data"
    , headingShort = "Required & Data"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        sh:targetClass showcase:Root ;
                        hash:documentRoot true ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasDecimalSuffixed ;
                        sh:name "suffixed decimal"@en ;
                        sh:node [
                            sh:property [
                                sh:path showcase:hasDecimal ;
                                sh:datatype xsd:decimal ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                            sh:property [
                                sh:path showcase:hasSuffix ;
                                sh:hasValue showcase:suffix ;
                                hash:include hash:Suffix ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                        ] ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .

                    showcase:suffix
                        rdfs:label "suffix"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasDecimalSuffixed [
                            showcase:hasDecimal 3.14 ;
                            showcase:hasSuffix showcase:suffix ;
                        ] ;
                    .
                """
            }


decodeType : Iri -> Decoder ()
decodeType iri =
    Decode.predicate Rdf.a
        (Decode.andThen
            (\iriActual ->
                if iriActual == iri then
                    Decode.succeed ()

                else
                    Decode.fail "invalid class"
            )
            Decode.iri
        )


withPrefixRequired : Documentation
withPrefixRequired =
    { headingFull = "Required Numerical Fields With Prefix"
    , headingShort = "Required"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        sh:targetClass showcase:Root ;
                        hash:documentRoot true ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasDecimalPrefixed ;
                        sh:name "prefixed decimal"@en ;
                        sh:node [
                            sh:property [
                                sh:path showcase:hasDecimal ;
                                sh:datatype xsd:decimal ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                            sh:property [
                                sh:path showcase:hasPrefix ;
                                sh:hasValue showcase:prefix ;
                                hash:include hash:Prefix ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                        ] ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .

                    showcase:prefix
                        rdfs:label "prefix"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasDecimalPrefixed [
                            rdfs:isDefinedBy <#4153b257> ;
                            showcase:hasDecimal 3.14 ;
                            showcase:hasPrefix showcase:prefix ;
                        ] ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "4153b257-a50414b3"
                , label = "Prefixed decimal"
                , info = Nothing
                , value = "3.14"
                }
                >> Query.count (Expect.equal 1)
            )
        |> Documentation.expectUi
            (Query.findAll
                [ Selector.exactText "Prefix" ]
                >> Query.count (Expect.equal 1)
            )


withPrefixOptional : Documentation
withPrefixOptional =
    { headingFull = "Optional Numerical Fields With Prefix"
    , headingShort = "Optional"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        sh:targetClass showcase:Root ;
                        hash:documentRoot true ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasDecimalPrefixed ;
                        sh:name "prefixed decimal"@en ;
                        sh:node [
                            sh:property [
                                sh:path showcase:hasDecimal ;
                                sh:datatype xsd:decimal ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                            sh:property [
                                sh:path showcase:hasPrefix ;
                                sh:hasValue showcase:prefix ;
                                hash:include hash:Prefix ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                        ] ;
                        sh:minCount 0 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .

                    showcase:prefix
                        rdfs:label "prefix"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasDecimalPrefixed [
                            rdfs:isDefinedBy <#4153b257> ;
                            showcase:hasDecimal 3.14 ;
                            showcase:hasPrefix showcase:prefix ;
                        ] ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "4153b257-a50414b3"
                , label = "Prefixed decimal"
                , info = Just "Optional"
                , value = "3.14"
                }
                >> Query.count (Expect.equal 1)
            )
        |> Documentation.expectUi
            (Query.findAll
                [ Selector.exactText "Prefix" ]
                >> Query.count (Expect.equal 1)
            )


withConstantSimple : Documentation
withConstantSimple =
    { headingFull = "Simply Nested Numerical Fields With Injected Constant"
    , headingShort = "Simply Nested"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        sh:targetClass showcase:Root ;
                        hash:documentRoot true ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasDecimalSuffixed ;
                        sh:name "suffixed decimal"@en ;
                        sh:node [
                            sh:property [
                                sh:path showcase:hasDecimal ;
                                sh:datatype xsd:decimal ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                            sh:property [
                                sh:path showcase:hasSuffix ;
                                sh:hasValue showcase:suffix ;
                                hash:include hash:Suffix ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                            sh:property [
                                sh:path showcase:hasConstant ;
                                sh:hasValue "something"^^xsd:string ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                        ] ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                        sh:defaultValue [
                            showcase:hasDecimal "1"^^xsd:decimal ;
                            showcase:hasSuffix showcase:suffix ;
                        ] ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .

                    showcase:suffix
                        rdfs:label "suffix"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasDecimalSuffixed [
                            rdfs:isDefinedBy <#46426108> ;
                            showcase:hasDecimal 3.14 ;
                            showcase:hasSuffix showcase:suffix ;
                            showcase:hasConstant "something" ;
                        ] ;
                    .
                """
            }
        |> Documentation.expectGraph
            (Decode.decode
                (Decode.from
                    (Viewed.document "root")
                    (Decode.predicate (Viewed.showcase "hasDecimalSuffixed")
                        (Decode.succeed
                            (\hasDecimal hasPrefix hasConstant ->
                                { hasDecimal = hasDecimal
                                , hasPrefix = hasPrefix
                                , hasConstant = hasConstant
                                }
                            )
                            |> Decode.required (Viewed.showcase "hasDecimal") (Decode.literal (Viewed.xsd "decimal"))
                            |> Decode.required (Viewed.showcase "hasSuffix") Decode.iri
                            |> Decode.required (Viewed.showcase "hasConstant") (Decode.literal (Viewed.xsd "string"))
                        )
                    )
                )
                >> Result.map
                    (Expect.equal
                        { hasDecimal = "3.14"
                        , hasPrefix = Viewed.showcase "suffix"
                        , hasConstant = "something"
                        }
                    )
            )


withConstantNested : Documentation
withConstantNested =
    { headingFull = "Multiply Nested Numerical Fields With Injected Constant"
    , headingShort = "Multiply Nested"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        sh:targetClass showcase:Root ;
                        hash:documentRoot true ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasDecimalSuffixed ;
                        sh:name "suffixed decimal"@en ;
                        sh:node [
                            sh:property [
                                sh:path showcase:hasDecimal ;
                                sh:datatype xsd:decimal ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                            sh:property [
                                sh:path showcase:hasNested ;
                                sh:node [
                                    sh:property [
                                        sh:path showcase:hasSuffix ;
                                        sh:hasValue showcase:suffix ;
                                        hash:include hash:Suffix ;
                                        sh:minCount 1 ;
                                        sh:maxCount 1 ;
                                    ] ;
                                    sh:property [
                                        sh:path showcase:hasConstant ;
                                        sh:hasValue "something"^^xsd:string ;
                                        sh:minCount 1 ;
                                        sh:maxCount 1 ;
                                    ] ;
                                ] ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                        ] ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                        sh:defaultValue [
                            showcase:hasDecimal "1"^^xsd:decimal ;
                            showcase:hasNested [
                                showcase:hasSuffix showcase:suffix ;
                            ] ;
                        ] ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .

                    showcase:suffix
                        rdfs:label "suffix"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasDecimalSuffixed [
                            rdfs:isDefinedBy <#3065770e> ;
                            showcase:hasDecimal 3.14 ;
                            showcase:hasNested [
                                rdfs:isDefinedBy <#3065770e-fda48ad6> ;
                                showcase:hasSuffix showcase:suffix ;
                                showcase:hasConstant "something" ;
                            ] ;
                        ] ;
                    .
                """
            }
        |> Documentation.expectGraph
            (Decode.decode
                (Decode.from
                    (Viewed.document "root")
                    (Decode.predicate (Viewed.showcase "hasDecimalSuffixed")
                        (Decode.succeed
                            (\hasDecimal ( hasSuffix, hasConstant ) ->
                                { hasDecimal = hasDecimal
                                , hasSuffix = hasSuffix
                                , hasConstant = hasConstant
                                }
                            )
                            |> Decode.required (Viewed.showcase "hasDecimal") (Decode.literal (Viewed.xsd "decimal"))
                            |> Decode.required
                                (Viewed.showcase "hasNested")
                                (Decode.succeed Tuple.pair
                                    |> Decode.required (Viewed.showcase "hasSuffix") Decode.iri
                                    |> Decode.required (Viewed.showcase "hasConstant") (Decode.literal (Viewed.xsd "string"))
                                )
                        )
                    )
                )
                >> Result.map
                    (Expect.equal
                        { hasDecimal = "3.14"
                        , hasSuffix = Viewed.showcase "suffix"
                        , hasConstant = "something"
                        }
                    )
            )


affixedWithinNested : Documentation
affixedWithinNested =
    { headingFull = "Numerical Fields With Affix Within Nested Shape"
    , headingShort = "Affixed Within Nested Shape"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        sh:targetClass showcase:Root ;
                        hash:documentRoot true ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasNested ;
                        sh:name "name nested"@en ;
                        sh:node [
                            sh:property [
                                sh:path showcase:hasAffixed ;
                                sh:name "name affixed"@en ;
                                sh:order 0 ;
                                sh:node [
                                    sh:property [
                                        sh:path showcase:hasDecimal ;
                                        sh:datatype xsd:decimal ;
                                        sh:minCount 1 ;
                                        sh:maxCount 1 ;
                                    ] ;
                                    sh:property [
                                        sh:path showcase:hasSuffix ;
                                        sh:hasValue showcase:suffix ;
                                        hash:include hash:Suffix ;
                                        sh:minCount 1 ;
                                        sh:maxCount 1 ;
                                    ] ;
                                ] ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                            sh:property [
                                sh:path showcase:hasOther ;
                                sh:name "name other"@en ;
                                sh:order 1 ;
                                sh:datatype xsd:string ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                        ] ;
                        dash:editor dash:DetailsEditor ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .

                    showcase:suffix
                        rdfs:label "suffix"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasNested [
                            rdfs:isDefinedBy <#513a5406> ;
                            showcase:hasAffixed [
                                rdfs:isDefinedBy <#513a5406-7c51b152> ;
                                showcase:hasDecimal 3.14 ;
                                showcase:hasSuffix showcase:suffix ;
                            ] ;
                            showcase:hasOther "value" ;
                        ] ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "513a5406-7c51b152-a50414b3"
                , label = "Name affixed"
                , info = Nothing
                , value = "3.14"
                }
                >> Query.count (Expect.equal 1)
            )
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "513a5406-a05bb1a6"
                , label = "Name other"
                , info = Nothing
                , value = "value"
                }
                >> Query.count (Expect.equal 1)
            )


affixedWithMultipleChoices : Documentation
affixedWithMultipleChoices =
    { headingFull = "Numerical Fields With Affix and Multiple Choices"
    , headingShort = "With Affix and Multiple Choices"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        sh:targetClass showcase:Root ;
                        hash:documentRoot true ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasAffixed ;
                        sh:name "name affixed"@en ;
                        sh:order 0 ;
                        sh:node [
                            sh:property [
                                sh:path showcase:hasDecimal ;
                                sh:datatype xsd:decimal ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                            sh:property [
                                sh:path showcase:hasSuffix ;
                                sh:in ( showcase:suffixA showcase:suffixB ) ;
                                hash:include hash:Suffix ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                        ] ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .

                    showcase:suffixA
                        rdfs:label "suffix a"@en ;
                    .

                    showcase:suffixB
                        rdfs:label "suffix b"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasAffixed [
                            rdfs:isDefinedBy <#e967672a> ;
                            showcase:hasDecimal 3.14 ;
                            showcase:hasSuffix showcase:suffixA ;
                        ] ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "e967672a-a50414b3"
                , label = "Name affixed"
                , info = Nothing
                , value = "3.14"
                }
                >> Query.count (Expect.equal 1)
            )


unitViaOm : Documentation
unitViaOm =
    { headingFull = "Numerical Fields with Unit via OM"
    , headingShort = "With Unit via OM"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    @prefix om: <http://www.ontology-of-units-of-measure.org/resource/om-2/> .

                    <RootShape> a sh:NodeShape ;
                        sh:targetClass showcase:Root ;
                        hash:documentRoot true ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasNestedUnitsOm ;
                        sh:name "OM value"@en ;
                        sh:class showcase:Class ;
                        sh:node [
                          sh:property [
                            sh:path om:hasValue ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                            sh:node [
                              sh:property [
                                sh:path om:value ;
                                sh:datatype xsd:decimal ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                              ] ;
                              sh:property [
                                sh:path om:unit ;
                                sh:hasValue showcase:MilliM2-PER-SEC2 ;
                                hash:include hash:Suffix ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                              ] ;
                            ] ;
                          ] ;
                        ] ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .

                    showcase:MilliM2-PER-SEC2 a showcase:Unit ;
                        rdfs:label "mm²/s²" ;
                    .
                """
            , documentData =
                """
                    @prefix om: <http://www.ontology-of-units-of-measure.org/resource/om-2/> .

                    <root> a showcase:Root ;
                        showcase:hasNestedUnitsOm [
                            rdfs:isDefinedBy <#cad70c94> ;
                            a showcase:Class ;
                            om:hasValue [
                                rdfs:isDefinedBy <#cad70c94-e679db07> ;
                                om:value 3.14 ;
                                om:unit showcase:MilliM2-PER-SEC2 ;
                            ] ;
                        ] ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "cad70c94-e679db07-f48d1fc"
                , label = "OM value"
                , info = Nothing
                , value = "3.14"
                }
                >> Query.count (Expect.equal 1)
            )
        |> Documentation.expectUi
            (Query.findAll
                [ Selector.exactText "mm²/s²" ]
                >> Query.count (Expect.equal 1)
            )


unitViaQudt : Documentation
unitViaQudt =
    { headingFull = "Numerical Fields With Unit via QUDT"
    , headingShort = "With Unit via QUDT"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    @prefix qudt: <http://qudt.org/schema/qudt/> .
                    @prefix unit: <http://qudt.org/vocab/unit/> .

                    <RootShape> a sh:NodeShape ;
                        sh:targetClass showcase:Root ;
                        hash:documentRoot true ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasNestedUnitsQudt ;
                        sh:name "QUDT value"@en ;
                        sh:class qudt:Quantity ;
                        sh:node [
                            sh:property [
                                sh:path qudt:unit ;
                                sh:hasValue unit:NUM ;
                                hash:include hash:Suffix ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                            sh:property [
                                sh:path qudt:value ;
                                sh:datatype xsd:integer ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                        ];
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .

                    unit:NUM rdfs:label "#" .
                """
            , documentData =
                """
                    @prefix qudt: <http://qudt.org/schema/qudt/> .
                    @prefix unit: <http://qudt.org/vocab/unit/> .

                    <root> a showcase:Root ;
                        showcase:hasNestedUnitsQudt [
                            rdfs:isDefinedBy <#8cebb23f> ;
                            a qudt:Quantity ;
                            qudt:unit unit:NUM ;
                            qudt:value 42 ;
                        ] ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "8cebb23f-3f47c44c"
                , label = "QUDT value"
                , info = Nothing
                , value = "42"
                }
                >> Query.count (Expect.equal 1)
            )
        |> Documentation.expectUi
            (Query.findAll
                [ Selector.exactText "#" ]
                >> Query.count (Expect.equal 1)
            )
