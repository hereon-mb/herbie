module Documentation.InputGroup exposing (all)

import Shacl.Documentation as Documentation exposing (Documentation)
import Tree exposing (Tree, tree)


all : Tree Documentation
all =
    tree
        (Documentation.fromInfo
            { headingFull = "Groups"
            , headingShort = "Groups"
            , description = ""
            }
        )
        [ tree simple []
        , tree nested []
        ]


simple : Documentation
simple =
    { headingFull = "Simple Groups"
    , headingShort = "Simple"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    @prefix herbie: <http://purls.helmholtz-metadaten.de/herbie/core/#> .

                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShapeUngroupedField> ;
                        sh:property <PropertyShapeGroupAField1> ;
                        sh:property <PropertyShapeGroupAField2> ;
                        sh:property <PropertyShapeGroupBField1> ;
                        sh:property <PropertyShapeGroupBField2> ;
                        sh:property <PropertyShapeGroupBField3> ;
                    .
                        <PropertyShapeUngrouped> sh:order 0 .
                        <GroupA> sh:order 1 .
                            <PropertyShapeGroupAField1> sh:group <GroupA> ; sh:order 0 .
                            <PropertyShapeGroupAField2> sh:group <GroupA> ; sh:order 1 .
                        <GroupB> sh:order 2 .
                            <PropertyShapeGroupBField1> sh:group <GroupB> ; sh:order 0 .
                            <PropertyShapeGroupBField2> sh:group <GroupB> ; sh:order 1 .
                            <PropertyShapeGroupBField3> sh:group <GroupB> ; sh:order 2 .

                    <GroupA> a sh:PropertyGroup ;
                        rdfs:label "group A"@en ;
                        rdfs:comment "A comment on the group A."@en ;
                    .

                    <GroupB> a sh:PropertyGroup ;
                        rdfs:label "group B"@en ;
                        rdfs:comment "A comment on the group B."@en ;
                    .

                    <PropertyShapeUngroupedField> a sh:PropertyShape ;
                        sh:path showcase:hasUngroupedField ;
                        sh:name "ungrouped field"@en ;
                        sh:datatype xsd:string ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    <PropertyShapeGroupAField1> a sh:PropertyShape ;
                        sh:path showcase:hasGroupAField1 ;
                        sh:name "field 1 in group A"@en ;
                        sh:datatype xsd:string ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    <PropertyShapeGroupAField2> a sh:PropertyShape ;
                        sh:path showcase:hasGroupAField2 ;
                        sh:name "field 2 in group A"@en ;
                        sh:datatype xsd:string ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    <PropertyShapeGroupBField1> a sh:PropertyShape ;
                        sh:path showcase:hasGroupBField1 ;
                        sh:name "field 1 in group B"@en ;
                        sh:datatype xsd:string ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    <PropertyShapeGroupBField2> a sh:PropertyShape ;
                        sh:path showcase:hasGroupBField2 ;
                        sh:name "field 2 in group B"@en ;
                        sh:datatype xsd:string ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    <PropertyShapeGroupBField3> a sh:PropertyShape ;
                        sh:path showcase:hasGroupBField3 ;
                        sh:name "field 3 in group B"@en ;
                        sh:datatype xsd:string ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }


nested : Documentation
nested =
    { headingFull = "Nested Groups"
    , headingShort = "Nested"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    @prefix herbie: <http://purls.helmholtz-metadaten.de/herbie/core/#> .

                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .
                        <GroupA> sh:order 0 .
                            <GroupB> sh:order 0 ; sh:group <GroupA> .
                                <GroupC> sh:order 0 ; sh:group <GroupB> .
                                    <PropertyShape> sh:order 0 ; sh:group <GroupC> .

                    <GroupA> a sh:PropertyGroup ;
                        rdfs:label "group A"@en ;
                        rdfs:comment "A comment on the group A."@en ;
                    .

                    <GroupB> a sh:PropertyGroup ;
                        rdfs:label "group B"@en ;
                        rdfs:comment "A comment on the group B."@en ;
                    .

                    <GroupC> a sh:PropertyGroup ;
                        rdfs:label "group C"@en ;
                        rdfs:comment "A comment on the group C."@en ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:datatype xsd:string ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
