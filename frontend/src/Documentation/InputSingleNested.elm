module Documentation.InputSingleNested exposing (all)

import Expect
import Expect.Shacl.Form.Viewed as Viewed
import Rdf.Decode as Decode
import Rdf.Decode.Extra as Decode
import Rdf.SetIri as SetIri
import Shacl.Documentation as Documentation exposing (Documentation)
import Test.Html.Query as Query
import Test.Html.Query.Extra as Query
import Test.Html.Selector as Selector
import Tree exposing (Tree, tree)


all : Tree Documentation
all =
    tree
        (Documentation.fromInfo
            { headingFull = "Single Nested"
            , headingShort = "Single Nested"
            , description = ""
            }
        )
        [ tree
            (Documentation.fromInfo
                { headingFull = "Optional via Checkbox"
                , headingShort = "Optional via Checkbox"
                , description = ""
                }
            )
            [ tree optionalViaCheckboxOneProperty []
            , tree optionalViaCheckboxTwoProperties []
            , tree
                (Documentation.fromInfo
                    { headingFull = "Containing Two Qualified Properties"
                    , headingShort = "Containing Two Qualified Properties"
                    , description = ""
                    }
                )
                [ tree twoQualifiedDataForOneProperty []
                , tree twoQualifiedDataForTwoProperties []
                ]
            ]
        , tree
            (Documentation.fromInfo
                { headingFull = "One Level"
                , headingShort = "One Level"
                , description = ""
                }
            )
            [ tree
                (Documentation.fromInfo
                    { headingFull = "Text"
                    , headingShort = "Text"
                    , description = ""
                    }
                )
                [ tree oneLevelInputTextRequired []
                , tree oneLevelInputTextOptional []
                , tree
                    (Documentation.fromInfo
                        { headingFull = "Qualified"
                        , headingShort = "Qualified"
                        , description = ""
                        }
                    )
                    [ tree oneLevelInputTextQualifiedRequired []
                    , tree oneLevelInputTextQualifiedOptional []
                    ]
                ]
            , tree oneLevelWithClass []
            ]
        , tree
            (Documentation.fromInfo
                { headingFull = "One Level with IRI"
                , headingShort = "One Level with IRI"
                , description = ""
                }
            )
            [ tree
                (Documentation.fromInfo
                    { headingFull = "Text"
                    , headingShort = "Text"
                    , description = ""
                    }
                )
                [ tree oneLevelWithIRIInputTextRequired []
                ]
            ]
        , tree
            (Documentation.fromInfo
                { headingFull = "Two Levels"
                , headingShort = "Two Levels"
                , description = ""
                }
            )
            [ tree
                (Documentation.fromInfo
                    { headingFull = "Text"
                    , headingShort = "Text"
                    , description = ""
                    }
                )
                [ tree twoLevelsInputTextRequired []
                , tree twoLevelsInputTextOptional []
                , tree
                    (Documentation.fromInfo
                        { headingFull = "Qualified"
                        , headingShort = "Qualified"
                        , description = ""
                        }
                    )
                    [ tree twoLevelsInputTextQualifiedRequired []
                    , tree twoLevelsInputTextQualifiedOptional []
                    ]
                ]
            ]
        ]


optionalViaCheckboxOneProperty : Documentation
optionalViaCheckboxOneProperty =
    { headingFull = "One Property"
    , headingShort = "One Property"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                  <RootShape> a sh:NodeShape ;
                    hash:documentRoot true ;
                    sh:targetClass showcase:Root ;
                    sh:property <PropertyShape> ;
                  .

                  <PropertyShape> a sh:PropertyShape ;
                       sh:path showcase:property ;
                       sh:name "name property"@en ;
                       sh:node [
                           sh:property [
                               sh:path rdfs:comment ;
                               sh:datatype xsd:string ;
                               sh:minCount 1 ;
                               sh:maxCount 1 ;
                           ] ;
                       ] ;
                       sh:minCount 0 ;
                       sh:maxCount 1 ;
                  .

                  showcase:Root a rdfs:Class ;
                    rdfs:label "root"@en ;
                  .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:property [
                            rdfs:isDefinedBy <#24627844> ;
                        ] ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "24627844-937c9975"
                , label = "Name property"
                , info = Just "Optional"
                , value = ""
                }
                >> Query.count (Expect.equal 1)
            )


optionalViaCheckboxTwoProperties : Documentation
optionalViaCheckboxTwoProperties =
    { headingFull = "Two Properties"
    , headingShort = "Two Properties"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                  <RootShape> a sh:NodeShape ;
                    hash:documentRoot true ;
                    sh:targetClass showcase:Root ;
                    sh:property <PropertyShape> ;
                  .

                  <PropertyShape> a sh:PropertyShape ;
                       sh:path showcase:property ;
                       sh:name "property"@en ;
                       sh:node [
                           sh:property <PropertyShapeNestedA> ;
                           sh:property <PropertyShapeNestedB> ;
                       ] ;
                       sh:minCount 0 ;
                       sh:maxCount 1 ;
                  .

                  <PropertyShapeNestedA> a sh:PropertyShape ;
                       sh:path showcase:propertyA ;
                       sh:name "property a"@en ;
                       sh:datatype xsd:string ;
                       sh:minCount 1 ;
                       sh:maxCount 1 ;
                  .

                  <PropertyShapeNestedB> a sh:PropertyShape ;
                       sh:path showcase:propertyB ;
                       sh:name "property b"@en ;
                       sh:datatype xsd:string ;
                       sh:minCount 1 ;
                       sh:maxCount 1 ;
                  .

                  showcase:Root a rdfs:Class ;
                    rdfs:label "root"@en ;
                  .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:property [
                            rdfs:isDefinedBy <#11e4dc41> ;
                        ] ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAllCheckboxes
                { label = "Provide property"
                , checked = False
                }
                >> Query.count (Expect.equal 1)
            )


twoQualifiedDataForOneProperty : Documentation
twoQualifiedDataForOneProperty =
    { headingFull = "With Initial Data For One Property"
    , headingShort = "With Initial Data For One property"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl = shapeTwoQualified
            , documentData =
                """
                    <root> a showcase:Root .
                    <root>
                        showcase:property [
                            a showcase:PropertyA ;
                            rdfs:isDefinedBy <#2b7df848> ;
                            rdfs:comment "value for a" ;
                        ] ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAll [ Selector.tag "input" ]
                >> Query.count (Expect.equal 1)
            )


twoQualifiedDataForTwoProperties : Documentation
twoQualifiedDataForTwoProperties =
    { headingFull = "With Initial Data For Two Properties"
    , headingShort = "With Initial Data For Two properties"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl = shapeTwoQualified
            , documentData =
                """
                    <root> a showcase:Root .
                    <root>
                        showcase:property [
                            a showcase:PropertyA ;
                            rdfs:isDefinedBy <#2b7df848> ;
                            rdfs:comment "value for a" ;
                        ] ;
                        showcase:property [
                            a showcase:PropertyB ;
                            rdfs:isDefinedBy <#73ba38a> ;
                            rdfs:comment "value for b" ;
                        ] ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAll [ Selector.tag "input" ]
                >> Query.count (Expect.equal 2)
            )


shapeTwoQualified : String
shapeTwoQualified =
    """
        <RootShape> a sh:NodeShape ;
          hash:documentRoot true ;
          sh:targetClass showcase:Root ;
          sh:property <PropertyShapeA> ;
          sh:property <PropertyShapeB> ;
        .

        <PropertyShapeA> a sh:PropertyShape ;
             sh:path showcase:property ;
             sh:name "property A"@en ;
             sh:order 0 ;
             sh:qualifiedValueShape [
                 sh:class showcase:PropertyA ;
                 sh:property [
                     sh:path rdfs:comment ;
                     sh:datatype xsd:string ;
                     sh:minCount 1 ;
                     sh:maxCount 1 ;
                 ] ;
             ] ;
             dash:editor dash:DetailsEditor ;
             sh:qualifiedMinCount 0 ;
             sh:qualifiedMaxCount 1 ;
             hash:optionalViaCheckbox true ;
        .

        <PropertyShapeB> a sh:PropertyShape ;
             sh:path showcase:property ;
             sh:name "property B"@en ;
             sh:order 1 ;
             sh:qualifiedValueShape [
                 sh:class showcase:PropertyB ;
                 sh:property [
                     sh:path rdfs:comment ;
                     sh:datatype xsd:string ;
                     sh:minCount 1 ;
                     sh:maxCount 1 ;
                 ] ;
             ] ;
             dash:editor dash:DetailsEditor ;
             sh:qualifiedMinCount 0 ;
             sh:qualifiedMaxCount 1 ;
             hash:optionalViaCheckbox true ;
        .

        showcase:Root a rdfs:Class ;
          rdfs:label "root"@en ;
        .
    """


oneLevelInputTextRequired : Documentation
oneLevelInputTextRequired =
    { headingFull = "Required"
    , headingShort = "Required"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasNested ;
                        sh:name "name nested"@en ;
                        sh:node [
                            sh:property [
                                sh:path showcase:hasValue ;
                                sh:name "name property"@en ;
                                sh:datatype xsd:string ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                        ] ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasNested [
                            rdfs:isDefinedBy <#5a412361> ;
                            showcase:hasValue "value" ;
                        ] ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "5a412361-72860c15"
                , label = "Name nested"
                , info = Nothing
                , value = "value"
                }
                >> Query.count (Expect.equal 1)
            )
        |> Documentation.expectGraph
            (Decode.decode
                (Decode.from (Viewed.document "root")
                    (Decode.isInstanceOf (SetIri.singleton (Viewed.showcase "Root"))
                        |> Decode.andThen
                            (\_ ->
                                Decode.predicate (Viewed.showcase "hasNested")
                                    (Decode.predicate (Viewed.showcase "hasValue")
                                        Decode.string
                                    )
                            )
                    )
                )
                >> Result.map (Expect.equal "value")
            )


oneLevelInputTextOptional : Documentation
oneLevelInputTextOptional =
    { headingFull = "Optional"
    , headingShort = "Optional"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasNested ;
                        sh:name "name nested"@en ;
                        sh:node [
                            sh:property [
                                sh:path showcase:hasValue ;
                                sh:name "name property"@en ;
                                sh:datatype xsd:string ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                        ] ;
                        sh:minCount 0 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasNested [
                            rdfs:isDefinedBy <#5a412361> ;
                        ] ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "5a412361-72860c15"
                , label = "Name nested"
                , info = Just "Optional"
                , value = ""
                }
                >> Query.count (Expect.equal 1)
            )


oneLevelInputTextQualifiedRequired : Documentation
oneLevelInputTextQualifiedRequired =
    { headingFull = "Required"
    , headingShort = "Required"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasNested ;
                        sh:name "name nested"@en ;
                        sh:qualifiedValueShape [
                            sh:class showcase:Nested ;
                            sh:property [
                                sh:path showcase:hasValue ;
                                sh:name "name property"@en ;
                                sh:datatype xsd:string ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                        ] ;
                        sh:qualifiedMinCount 1 ;
                        sh:qualifiedMaxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasNested [
                            rdfs:isDefinedBy <#1337ea2e> ;
                            a showcase:Nested ;
                            showcase:hasValue "value" ;
                        ] .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "1337ea2e-72860c15"
                , label = "Name nested"
                , info = Nothing
                , value = "value"
                }
                >> Query.count (Expect.equal 1)
            )


oneLevelWithClass : Documentation
oneLevelWithClass =
    { headingFull = "With Class"
    , headingShort = "With Class"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasNested ;
                        sh:class showcase:Nested ;
                        sh:name "name nested"@en ;
                        sh:node [
                            sh:property [
                                sh:path showcase:hasValue ;
                                sh:name "name property"@en ;
                                sh:datatype xsd:string ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                        ] ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasNested [
                            a showcase:Nested ;
                            rdfs:isDefinedBy <#4aa8e174> ;
                            showcase:hasValue "test" ;
                        ] .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "4aa8e174-72860c15"
                , label = "Name nested"
                , info = Nothing
                , value = "test"
                }
                >> Query.count (Expect.equal 1)
            )
        |> Documentation.expectGraph
            (Decode.decode
                (Decode.from (Viewed.document "root")
                    (Decode.isInstanceOf (SetIri.singleton (Viewed.showcase "Root"))
                        |> Decode.andThen
                            (\_ ->
                                Decode.predicate (Viewed.showcase "hasNested")
                                    (Decode.isInstanceOf (SetIri.singleton (Viewed.showcase "Nested")))
                            )
                    )
                )
                >> Result.map (Expect.equal ())
            )


oneLevelInputTextQualifiedOptional : Documentation
oneLevelInputTextQualifiedOptional =
    { headingFull = "Optional"
    , headingShort = "Optional"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasNested ;
                        sh:name "name nested"@en ;
                        sh:qualifiedValueShape [
                            sh:class showcase:Nested ;
                            sh:property [
                                sh:path showcase:hasValue ;
                                sh:name "name property"@en ;
                                sh:datatype xsd:string ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                        ] ;
                        sh:qualifiedMinCount 0 ;
                        sh:qualifiedMaxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasNested [
                            a showcase:Nested ;
                            rdfs:isDefinedBy <#1337ea2e> ;
                            showcase:hasValue "value" ;
                        ] .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "1337ea2e-72860c15"
                , label = "Name nested"
                , info = Just "Optional"
                , value = "value"
                }
                >> Query.count (Expect.equal 1)
            )


oneLevelWithIRIInputTextRequired : Documentation
oneLevelWithIRIInputTextRequired =
    { headingFull = "Required"
    , headingShort = "Required"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasNested ;
                        sh:name "name nested"@en ;
                        sh:nodeKind sh:IRI ;
                        sh:node [
                            sh:property [
                                sh:path showcase:hasValue ;
                                sh:name "name property"@en ;
                                sh:datatype xsd:string ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                        ] ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }


twoLevelsInputTextRequired : Documentation
twoLevelsInputTextRequired =
    { headingFull = "Required"
    , headingShort = "Required"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasNestedOuter ;
                        sh:name "name nested outer"@en ;
                        sh:node [
                            sh:property [
                                sh:path showcase:hasNestedInner ;
                                sh:name "name nested inner"@en ;
                                sh:node [
                                    sh:property [
                                        sh:path showcase:hasValue ;
                                        sh:name "name property"@en ;
                                        sh:datatype xsd:string ;
                                        sh:minCount 1 ;
                                        sh:maxCount 1 ;
                                    ] ;
                                ] ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                        ] ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasNestedOuter [
                            rdfs:isDefinedBy <#80c1e351> ;
                            showcase:hasNestedInner [
                                rdfs:isDefinedBy <#80c1e351-874c567e> ;
                            ] ;
                        ] .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "80c1e351-874c567e-72860c15"
                , label = "Name nested outer"
                , info = Nothing
                , value = ""
                }
                >> Query.count (Expect.equal 1)
            )


twoLevelsInputTextOptional : Documentation
twoLevelsInputTextOptional =
    { headingFull = "Optional"
    , headingShort = "Optional"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasNestedOuter ;
                        sh:name "name nested outer"@en ;
                        sh:node [
                            sh:property [
                                sh:path showcase:hasNestedInner ;
                                sh:name "name nested inner"@en ;
                                sh:node [
                                    sh:property [
                                        sh:path showcase:hasValue ;
                                        sh:name "name property"@en ;
                                        sh:datatype xsd:string ;
                                        sh:minCount 1 ;
                                        sh:maxCount 1 ;
                                    ] ;
                                ] ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                        ] ;
                        sh:minCount 0 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasNestedOuter [
                            rdfs:isDefinedBy <#80c1e351> ;
                            showcase:hasNestedInner [
                                rdfs:isDefinedBy <#80c1e351-874c567e> ;
                            ] ;
                        ] .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "80c1e351-874c567e-72860c15"
                , label = "Name nested outer"
                , info = Nothing
                , value = ""
                }
                >> Query.count (Expect.equal 1)
            )


twoLevelsInputTextQualifiedRequired : Documentation
twoLevelsInputTextQualifiedRequired =
    { headingFull = "Required"
    , headingShort = "Required"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasNestedOuter ;
                        sh:name "name nested outer"@en ;
                        sh:qualifiedValueShape [
                            sh:class showcase:NestedOuter ;
                            sh:property [
                                sh:path showcase:hasNestedInner ;
                                sh:name "name nested inner"@en ;
                                sh:node [
                                    sh:property [
                                        sh:path showcase:hasValue ;
                                        sh:name "name property"@en ;
                                        sh:datatype xsd:string ;
                                        sh:minCount 1 ;
                                        sh:maxCount 1 ;
                                    ] ;
                                ] ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                        ] ;
                        sh:qualifiedMinCount 1 ;
                        sh:qualifiedMaxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasNestedOuter [
                            rdfs:isDefinedBy <#552e710f> ;
                            a showcase:NestedOuter ;
                            showcase:hasNestedInner [
                                rdfs:isDefinedBy <#552e710f-874c567e> ;
                                showcase:hasValue "value" ;
                            ] ;
                        ] .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "552e710f-874c567e-72860c15"
                , label = "Name nested outer"
                , info = Nothing
                , value = "value"
                }
                >> Query.count (Expect.equal 1)
            )


twoLevelsInputTextQualifiedOptional : Documentation
twoLevelsInputTextQualifiedOptional =
    { headingFull = "Optional"
    , headingShort = "Optional"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasNestedOuter ;
                        sh:name "name nested outer"@en ;
                        sh:qualifiedValueShape [
                            sh:class showcase:NestedOuter ;
                            sh:property [
                                sh:path showcase:hasNestedInner ;
                                sh:name "name nested inner"@en ;
                                sh:node [
                                    sh:property [
                                        sh:path showcase:hasValue ;
                                        sh:name "name property"@en ;
                                        sh:datatype xsd:string ;
                                        sh:minCount 1 ;
                                        sh:maxCount 1 ;
                                    ] ;
                                ] ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                        ] ;
                        sh:qualifiedMinCount 0 ;
                        sh:qualifiedMaxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasNestedOuter [
                            rdfs:isDefinedBy <#552e710f> ;
                            a showcase:NestedOuter ;
                            showcase:hasNestedInner [
                                rdfs:isDefinedBy <#552e710f-874c567e> ;
                                showcase:hasValue "" ;
                            ] ;
                        ] ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "552e710f-874c567e-72860c15"
                , label = "Name nested outer"
                , info = Just "Optional"
                , value = ""
                }
                >> Query.count (Expect.equal 1)
            )
