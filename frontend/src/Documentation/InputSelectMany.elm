module Documentation.InputSelectMany exposing (all)

import Expect
import Expect.Shacl.Form.Viewed as Viewed
import Rdf.Decode as Decode
import Shacl.Documentation as Documentation exposing (Documentation)
import Test.Html.Query as Query
import Test.Html.Query.Extra as Query
import Tree exposing (Tree, tree)


all : Tree Documentation
all =
    tree
        (Documentation.fromInfo
            { headingFull = "Multiple Instance Selection Fields"
            , headingShort = "Multiple Instance Selection"
            , description = ""
            }
        )
        [ tree required []
        , tree withDefaults []
        , tree withNesting []
        , tree withNestingAndConstant []
        ]


required : Documentation
required =
    { headingFull = "Required Multiple Instance Selection Fields"
    , headingShort = "Required"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:class showcase:Leaf ;
                        dash:editor dash:InstancesSelectEditor ;
                        sh:minCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUi
            (Query.findAllComboboxMultiSelects {}
                >> Query.count (Expect.equal 1)
            )


withDefaults : Documentation
withDefaults =
    { headingFull = "Multiple Instanced Selection Fields With Defaults"
    , headingShort = "With Defaults"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:class showcase:Leaf ;
                        dash:editor dash:InstancesSelectEditor ;
                        sh:minCount 1 ;
                        sh:defaultValue showcase:leafA ;
                        sh:defaultValue showcase:leafB ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                    showcase:leafA a showcase:Leaf ; rdfs:label "leaf a"@en .
                    showcase:leafB a showcase:Leaf ; rdfs:label "leaf b"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectGraph
            (Decode.decode
                (Decode.from
                    (Viewed.document "root")
                    (Decode.predicate (Viewed.showcase "hasValue") (Decode.many Decode.iri))
                )
                >> Result.map
                    (Expect.equal
                        [ Viewed.showcase "leafA"
                        , Viewed.showcase "leafB"
                        ]
                    )
            )


withNesting : Documentation
withNesting =
    { headingFull = "Multiple Instance Selection Fields with Nesting"
    , headingShort = "With Nesting"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property [
                            sh:path showcase:hasNesting ;
                            sh:name "name"@en ;
                            sh:description "description"@en ;
                            sh:node [
                                sh:class showcase:Nested ;
                                sh:property [
                                    sh:path showcase:hasValue ;
                                    sh:class showcase:Leaf ;
                                    sh:name "name"@en ;
                                    sh:description "description"@en ;
                                    sh:minCount 1 ;
                                    sh:maxCount 1 ;
                                    dash:editor dash:InstancesSelectEditor ;
                                ] ;
                            ] ;
                            sh:minCount 1 ;
                            dash:editor dash:InstancesSelectEditor ;
                        ] ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                    showcase:leafA a showcase:Leaf ; rdfs:label "leaf a"@en .
                    showcase:leafB a showcase:Leaf ; rdfs:label "leaf b"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }


withNestingAndConstant : Documentation
withNestingAndConstant =
    { headingFull = "Multiple Instance Selection Fields with Nesting and Constant"
    , headingShort = "With Nesting and Constant"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property [
                            sh:path showcase:hasNesting ;
                            sh:name "name"@en ;
                            sh:description "description"@en ;
                            sh:node [
                                sh:class showcase:NestedOuter ;
                                sh:property [
                                    sh:path showcase:hasNesting ;
                                    sh:node [
                                        sh:property [
                                            sh:path showcase:hasValue ;
                                            sh:class showcase:Leaf ;
                                            sh:name "name"@en ;
                                            sh:description "description"@en ;
                                            sh:minCount 1 ;
                                            sh:maxCount 1 ;
                                            dash:editor dash:InstancesSelectEditor ;
                                        ] ;
                                    ] ;
                                    sh:minCount 1 ;
                                    sh:maxCount 1 ;
                                ] ;
                                sh:property [
                                    sh:path showcase:hasConstant ;
                                    sh:hasValue "constant" ;
                                    sh:minCount 1 ;
                                    sh:maxCount 1 ;
                                ] ;
                            ] ;
                            sh:minCount 1 ;
                            dash:editor dash:InstancesSelectEditor ;
                        ] ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                    showcase:leafA a showcase:Leaf ; rdfs:label "leaf a"@en .
                    showcase:leafB a showcase:Leaf ; rdfs:label "leaf b"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
