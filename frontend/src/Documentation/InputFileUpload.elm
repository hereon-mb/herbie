module Documentation.InputFileUpload exposing (all)

import Shacl.Documentation as Documentation exposing (Documentation)
import Tree exposing (Tree, tree)


all : Tree Documentation
all =
    tree
        (Documentation.fromInfo
            { headingFull = "File Upload Fields"
            , headingShort = "File Upload"
            , description = ""
            }
        )
        [ tree required []
        , tree optional []
        ]


required : Documentation
required =
    { headingFull = "Required File Upload Fields"
    , headingShort = "Required"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        a sh:PropertyShape ;
                        sh:path showcase:hasConstant ;
                        sh:name "name" ;
                        sh:nodeKind sh:IRI ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .

                    <PropertyShape> a sh:PropertyShape ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }


optional : Documentation
optional =
    { headingFull = "Optional File Upload Fields"
    , headingShort = "Optional"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        a sh:PropertyShape ;
                        sh:path showcase:hasConstant ;
                        sh:name "name" ;
                        sh:nodeKind sh:IRI ;
                        sh:minCount 0 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .

                    <PropertyShape> a sh:PropertyShape ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
