module Documentation.InputVariants exposing (all)

import Expect
import Shacl.Documentation as Documentation exposing (Documentation)
import Test.Html.Query as Query
import Test.Html.Query.Extra as Query
import Tree exposing (Tree, tree)


all : Tree Documentation
all =
    tree
        (Documentation.fromInfo
            { headingFull = "Variants"
            , headingShort = "Variants"
            , description = ""
            }
        )
        [ tree atRootShape []
        , tree
            (Documentation.fromInfo
                { headingFull = "At Nested Shape"
                , headingShort = "At Nested Shape"
                , description = ""
                }
            )
            [ tree atNestedShapeWithoutShClass []
            , tree atNestedShapeWithShClass []
            ]
        ]


atRootShape : Documentation
atRootShape =
    { headingFull = "At Root Shape"
    , headingShort = "At Root Shape"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:or (
                            <VariantA>
                            <VariantB>
                        ) ;
                    .

                    <VariantA> a sh:NodeShape ;
                        sh:name "variant A"@en ;
                        sh:property [
                            sh:path showcase:hasValueA ;
                            sh:name "name property a"@en ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    .

                    <VariantB> a sh:NodeShape ;
                        sh:name "variant B"@en ;
                        sh:property [
                            sh:path showcase:hasValueB ;
                            sh:name "name property b"@en ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUi
            (Query.findAllSegmentedButtonsGroups {}
                >> Query.count (Expect.equal 1)
            )


atNestedShapeWithoutShClass : Documentation
atNestedShapeWithoutShClass =
    { headingFull = "Without sh:class"
    , headingShort = "Without sh:class"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasNested ;
                        sh:name "name nested"@en ;
                        sh:class showcase:Nested ;
                        sh:node [
                            sh:or (
                                <VariantA>
                                <VariantB>
                            ) ;
                        ] ;
                        dash:editor dash:DetailsEditor ;
                        sh:minCount 1 ;
                    .

                    <VariantA> a sh:NodeShape ;
                        sh:name "variant A"@en ;
                        sh:property [
                            sh:path showcase:hasValueA ;
                            sh:name "name property a"@en ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    .

                    <VariantB> a sh:NodeShape ;
                        sh:name "variant B"@en ;
                        sh:property [
                            sh:path showcase:hasValueB ;
                            sh:name "name property b"@en ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                      showcase:hasNested [
                          rdfs:isDefinedBy <#db0e7bf7-0> ;
                          a showcase:Nested ;
                          a <VariantA> ;
                      ] ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAllSegmentedButtonsGroups {}
                >> Query.count (Expect.equal 1)
            )


atNestedShapeWithShClass : Documentation
atNestedShapeWithShClass =
    { headingFull = "With sh:class"
    , headingShort = "With sh:class"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasNested ;
                        sh:name "name nested"@en ;
                        sh:class showcase:Nested ;
                        sh:node [
                            sh:class showcase:Nested ;
                            sh:or (
                                <VariantA>
                                <VariantB>
                            ) ;
                        ] ;
                        dash:editor dash:DetailsEditor ;
                        sh:minCount 1 ;
                    .

                    <VariantA> a sh:NodeShape ;
                        sh:class showcase:NestedA ;
                        sh:name "variant A"@en ;
                        sh:property [
                            sh:path showcase:hasValueA ;
                            sh:name "name property a"@en ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    .

                    <VariantB> a sh:NodeShape ;
                        sh:class showcase:NestedB ;
                        sh:name "variant B"@en ;
                        sh:property [
                            sh:path showcase:hasValueB ;
                            sh:name "name property b"@en ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                      showcase:hasNested [
                          a showcase:Nested, showcase:NestedA ;
                          rdfs:isDefinedBy <#fa738b9c-0> ;
                      ] ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAllSegmentedButtonsGroups {}
                >> Query.count (Expect.equal 1)
            )
