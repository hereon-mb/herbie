module Documentation.InputGrid exposing (all)

import Shacl.Documentation as Documentation exposing (Documentation)
import Tree exposing (Tree, tree)


all : Tree Documentation
all =
    tree
        (Documentation.fromInfo
            { headingFull = "Grid"
            , headingShort = "Grid"
            , description = ""
            }
        )
        [ tree required []
        ]


required : Documentation
required =
    { headingFull = "Required Grid"
    , headingShort = "Required"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    @prefix herbie: <http://purls.helmholtz-metadaten.de/herbie/core/#> .

                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShapeLayout> ;
                        sh:property <PropertyShapeGrid> ;
                    .
                        <PropertyShapeLayout> sh:order 0 .
                        <PropertyShapeGrid> sh:order 1 .

                    <PropertyShapeLayout> a sh:PropertyShape ;
                        sh:path showcase:hasLayout ;
                        sh:name "layout"@en ;
                        sh:class showcase:Layout ;
                        dash:editor dash:InstancesSelectEditor ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    <PropertyShapeGrid> a sh:PropertyShape ;
                        sh:path showcase:hasNested ;
                        sh:name "name nested"@en ;
                        sh:class showcase:Nested ;
                        sh:node <NodeShapeNested> ;
                        dash:editor [
                            a hash:GridEditor ;
                            hash:gridEditorAvailableRowsPath ( showcase:hasLayout showcase:layoutRow ) ;
                            hash:gridEditorAvailableColumnsPath ( showcase:hasLayout showcase:layoutColumn ) ;
                            hash:gridEditorRowPath showcase:hasRow ;
                            hash:gridEditorColumnPath showcase:hasColumn ;
                            hash:gridEditorCellViewer <NodeShapeNested> ;
                        ] ;
                        sh:minCount 1 ;
                    .

                    <NodeShapeNested> a sh:NodeShape ;
                        sh:property [
                            sh:path showcase:hasValueA ;
                            sh:name "name property a"@en ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                        sh:property [
                            sh:path showcase:hasValueB ;
                            sh:name "name property b"@en ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                    showcase:Nested a rdfs:Class ; rdfs:label "nested"@en .

                    <layout6> a showcase:Layout ;
                      rdfs:label "layout6" ;
                      showcase:layoutRow <layout6-A> ;
                      showcase:layoutRow <layout6-B> ;
                      showcase:layoutColumn <layout6-1> ;
                      showcase:layoutColumn <layout6-2> ;
                      showcase:layoutColumn <layout6-3> ;
                    .
                    <layout6-A> a showcase:LayoutRow ; rdfs:label "A" ; herbie:order 0 .
                    <layout6-B> a showcase:LayoutRow ; rdfs:label "B" ; herbie:order 1 .
                    <layout6-1> a showcase:LayoutColumn ; rdfs:label "1" ; herbie:order 0 .
                    <layout6-2> a showcase:LayoutColumn ; rdfs:label "2" ; herbie:order 1 .
                    <layout6-3> a showcase:LayoutColumn ; rdfs:label "3" ; herbie:order 2 .

                    <layout12> a showcase:Layout ;
                      rdfs:label "layout12" ;
                      showcase:layoutRow <layout12-A> ;
                      showcase:layoutRow <layout12-B> ;
                      showcase:layoutRow <layout12-C> ;
                      showcase:layoutColumn <layout12-1> ;
                      showcase:layoutColumn <layout12-2> ;
                      showcase:layoutColumn <layout12-3> ;
                      showcase:layoutColumn <layout12-4> ;
                    .
                    <layout12-A> a showcase:LayoutRow ; rdfs:label "A" ; herbie:order 0 .
                    <layout12-B> a showcase:LayoutRow ; rdfs:label "B" ; herbie:order 1 .
                    <layout12-C> a showcase:LayoutRow ; rdfs:label "C" ; herbie:order 2 .
                    <layout12-1> a showcase:LayoutColumn ; rdfs:label "1" ; herbie:order 0 .
                    <layout12-2> a showcase:LayoutColumn ; rdfs:label "2" ; herbie:order 1 .
                    <layout12-3> a showcase:LayoutColumn ; rdfs:label "3" ; herbie:order 2 .
                    <layout12-4> a showcase:LayoutColumn ; rdfs:label "4" ; herbie:order 3 .

                    <layout24> a showcase:Layout ;
                      rdfs:label "layout24" ;
                      showcase:layoutRow <layout24-A> ;
                      showcase:layoutRow <layout24-B> ;
                      showcase:layoutRow <layout24-C> ;
                      showcase:layoutRow <layout24-D> ;
                      showcase:layoutColumn <layout24-1> ;
                      showcase:layoutColumn <layout24-2> ;
                      showcase:layoutColumn <layout24-3> ;
                      showcase:layoutColumn <layout24-4> ;
                      showcase:layoutColumn <layout24-5> ;
                      showcase:layoutColumn <layout24-6> ;
                    .
                    <layout24-A> a showcase:LayoutRow ; rdfs:label "A" ; herbie:order 0 .
                    <layout24-B> a showcase:LayoutRow ; rdfs:label "B" ; herbie:order 1 .
                    <layout24-C> a showcase:LayoutRow ; rdfs:label "C" ; herbie:order 2 .
                    <layout24-D> a showcase:LayoutRow ; rdfs:label "D" ; herbie:order 3 .
                    <layout24-1> a showcase:LayoutColumn ; rdfs:label "1" ; herbie:order 0 .
                    <layout24-2> a showcase:LayoutColumn ; rdfs:label "2" ; herbie:order 1 .
                    <layout24-3> a showcase:LayoutColumn ; rdfs:label "3" ; herbie:order 2 .
                    <layout24-4> a showcase:LayoutColumn ; rdfs:label "4" ; herbie:order 3 .
                    <layout24-5> a showcase:LayoutColumn ; rdfs:label "5" ; herbie:order 4 .
                    <layout24-6> a showcase:LayoutColumn ; rdfs:label "6" ; herbie:order 5 .

                    <layout96> a showcase:Layout ;
                      rdfs:label "layout96" ;
                      showcase:layoutRow <layout96-A> ;
                      showcase:layoutRow <layout96-B> ;
                      showcase:layoutRow <layout96-C> ;
                      showcase:layoutRow <layout96-D> ;
                      showcase:layoutRow <layout96-E> ;
                      showcase:layoutRow <layout96-F> ;
                      showcase:layoutRow <layout96-G> ;
                      showcase:layoutRow <layout96-H> ;
                      showcase:layoutColumn <layout96-1> ;
                      showcase:layoutColumn <layout96-2> ;
                      showcase:layoutColumn <layout96-3> ;
                      showcase:layoutColumn <layout96-4> ;
                      showcase:layoutColumn <layout96-5> ;
                      showcase:layoutColumn <layout96-6> ;
                      showcase:layoutColumn <layout96-7> ;
                      showcase:layoutColumn <layout96-8> ;
                      showcase:layoutColumn <layout96-9> ;
                      showcase:layoutColumn <layout96-10> ;
                      showcase:layoutColumn <layout96-11> ;
                      showcase:layoutColumn <layout96-12> ;
                    .
                    <layout96-A> a showcase:LayoutRow ; rdfs:label "A" ; herbie:order 0 .
                    <layout96-B> a showcase:LayoutRow ; rdfs:label "B" ; herbie:order 1 .
                    <layout96-C> a showcase:LayoutRow ; rdfs:label "C" ; herbie:order 2 .
                    <layout96-D> a showcase:LayoutRow ; rdfs:label "D" ; herbie:order 3 .
                    <layout96-E> a showcase:LayoutRow ; rdfs:label "E" ; herbie:order 4 .
                    <layout96-F> a showcase:LayoutRow ; rdfs:label "F" ; herbie:order 5 .
                    <layout96-G> a showcase:LayoutRow ; rdfs:label "G" ; herbie:order 6 .
                    <layout96-H> a showcase:LayoutRow ; rdfs:label "H" ; herbie:order 7 .
                    <layout96-1> a showcase:LayoutColumn ; rdfs:label "1" ; herbie:order 0 .
                    <layout96-2> a showcase:LayoutColumn ; rdfs:label "2" ; herbie:order 1 .
                    <layout96-3> a showcase:LayoutColumn ; rdfs:label "3" ; herbie:order 2 .
                    <layout96-4> a showcase:LayoutColumn ; rdfs:label "4" ; herbie:order 3 .
                    <layout96-5> a showcase:LayoutColumn ; rdfs:label "5" ; herbie:order 4 .
                    <layout96-6> a showcase:LayoutColumn ; rdfs:label "6" ; herbie:order 5 .
                    <layout96-7> a showcase:LayoutColumn ; rdfs:label "7" ; herbie:order 6 .
                    <layout96-8> a showcase:LayoutColumn ; rdfs:label "8" ; herbie:order 7 .
                    <layout96-9> a showcase:LayoutColumn ; rdfs:label "9" ; herbie:order 8 .
                    <layout96-10> a showcase:LayoutColumn ; rdfs:label "10" ; herbie:order 9 .
                    <layout96-11> a showcase:LayoutColumn ; rdfs:label "11" ; herbie:order 10 .
                    <layout96-12> a showcase:LayoutColumn ; rdfs:label "12" ; herbie:order 11 .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
