module Documentation.InputCollection exposing (all)

import Expect
import Shacl.Documentation as Documentation exposing (Documentation)
import Test.Html.Query as Query
import Test.Html.Query.Extra as Query
import Test.Html.Selector as Selector
import Tree exposing (Tree, tree)


all : Tree Documentation
all =
    tree
        (Documentation.fromInfo
            { headingFull = "Collection"
            , headingShort = "Collection"
            , description = ""
            }
        )
        [ tree
            (Documentation.fromInfo
                { headingFull = "Required Collection"
                , headingShort = "Required"
                , description = ""
                }
            )
            [ tree requiredEmpty []
            , tree requiredWithOneInstance []
            , tree requiredWithSelectOne []
            ]
        , tree
            (Documentation.fromInfo
                { headingFull = "Optional"
                , headingShort = "Optional"
                , description = ""
                }
            )
            [ tree optionalEmpty []
            , tree optionalWithOneInstance []
            ]
        , tree withLabel []
        , tree withOrder []
        , tree withTable []
        ]


requiredEmpty : Documentation
requiredEmpty =
    { headingFull = "Empty"
    , headingShort = "Empty"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasNested ;
                        sh:name "name nested"@en ;
                        sh:class showcase:Nested ;
                        sh:node <NodeShapeNested> ;
                        dash:editor dash:DetailsEditor ;
                        sh:minCount 1 ;
                    .

                    <NodeShapeNested> a sh:NodeShape ;
                        sh:property [
                            sh:path showcase:hasValueA ;
                            sh:name "name property a"@en ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                        sh:property [
                            sh:path showcase:hasValueB ;
                            sh:name "name property b"@en ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUi
            (Query.findAll
                [ Selector.exactText "There are no name nesteds, yet" ]
                >> Query.count (Expect.equal 1)
            )
        |> Documentation.expectUi
            (Query.findAllButtons
                { id = "8b4dd15e--add"
                , label = "Add first Name nested"
                }
                >> Query.count (Expect.equal 1)
            )


requiredWithOneInstance : Documentation
requiredWithOneInstance =
    { headingFull = "With One Instance"
    , headingShort = "With One Instance"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasNested ;
                        sh:name "name nested"@en ;
                        sh:node <NodeShapeNested> ;
                        dash:editor dash:DetailsEditor ;
                        sh:minCount 1 ;
                    .

                    <NodeShapeNested> a sh:NodeShape ;
                        sh:class showcase:Nested ;
                        sh:property [
                            sh:path showcase:hasValueA ;
                            sh:name "name property a"@en ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                            sh:order 0 ;
                        ] ;
                        sh:property [
                            sh:path showcase:hasValueB ;
                            sh:name "name property b"@en ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                            sh:order 1 ;
                        ] ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasNested <nestedA> ;
                    .

                    <nestedA> a showcase:Nested ;
                        rdfs:isDefinedBy <#cb3aac4-0> ;
                        showcase:hasValueA "a" ;
                        showcase:hasValueB "b" ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAll
                [ Selector.exactText "Name nesteds (#1)"
                ]
                >> Query.count (Expect.equal 1)
            )
        |> Documentation.expectUi
            (Query.findAllButtons
                { id = "cb3aac4--add"
                , label = "Add Name nested"
                }
                >> Query.count (Expect.equal 1)
            )


requiredWithSelectOne : Documentation
requiredWithSelectOne =
    { headingFull = "With Select One"
    , headingShort = "With Select One"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasNested ;
                        sh:name "name nested"@en ;
                        sh:node <NodeShapeNested> ;
                        dash:editor dash:DetailsEditor ;
                        sh:minCount 1 ;
                    .

                    <NodeShapeNested> a sh:NodeShape ;
                        sh:class showcase:Nested ;
                        sh:property [
                            sh:path showcase:hasValueA ;
                            sh:name "name property a"@en ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                            sh:order 0 ;
                        ] ;
                        sh:property [
                            sh:path showcase:hasValueB ;
                            sh:name "name property b"@en ;
                            sh:class showcase:Leaf ;
                            dash:editor dash:InstancesSelectEditor ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                            sh:order 1 ;
                        ] ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .

                    <leafA> a showcase:Leaf ; rdfs:label "leaf A"@en .
                    <leafB> a showcase:Leaf ; rdfs:label "leaf B"@en .
                    <leafC> a showcase:Leaf ; rdfs:label "leaf C"@en .
                    <leafD> a showcase:Leaf ; rdfs:label "leaf D"@en .
                    <leafE> a showcase:Leaf ; rdfs:label "leaf E"@en .
                    <leafF> a showcase:Leaf ; rdfs:label "leaf F"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasNested <nestedA> ;
                    .

                    <nestedA> a showcase:Nested ;
                        rdfs:isDefinedBy <#cb3aac4-0> ;
                        showcase:hasValueA "a" ;
                        showcase:hasValueB "b" ;
                    .
                """
            }


optionalEmpty : Documentation
optionalEmpty =
    { headingFull = "Empty"
    , headingShort = "Empty"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasNested ;
                        sh:name "name nested"@en ;
                        sh:class showcase:Nested ;
                        sh:node <NodeShapeNested> ;
                        dash:editor dash:DetailsEditor ;
                    .

                    <NodeShapeNested> a sh:NodeShape ;
                        sh:property [
                            sh:path showcase:hasValueA ;
                            sh:name "name property a"@en ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                        sh:property [
                            sh:path showcase:hasValueB ;
                            sh:name "name property b"@en ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUi
            (Query.findAll
                [ Selector.exactText "There are no name nesteds, yet" ]
                >> Query.count (Expect.equal 1)
            )
        |> Documentation.expectUi
            (Query.findAllButtons
                { id = "8b4dd15e--add"
                , label = "Add first Name nested"
                }
                >> Query.count (Expect.equal 1)
            )


optionalWithOneInstance : Documentation
optionalWithOneInstance =
    { headingFull = "With One Instance"
    , headingShort = "With One Instance"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasNested ;
                        sh:name "name nested"@en ;
                        sh:class showcase:Nested ;
                        sh:node <NodeShapeNested> ;
                        dash:editor dash:DetailsEditor ;
                    .

                    <NodeShapeNested> a sh:NodeShape ;
                        sh:property [
                            sh:path showcase:hasValueA ;
                            sh:name "name property a"@en ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                        sh:property [
                            sh:path showcase:hasValueB ;
                            sh:name "name property b"@en ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasNested <nestedA> ;
                    .

                    <nestedA> a showcase:Nested ;
                        rdfs:isDefinedBy <#8b4dd15e-0> ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAll
                [ Selector.exactText "Name nesteds (#1)"
                ]
                >> Query.count (Expect.equal 1)
            )
        |> Documentation.expectUi
            (Query.findAllButtons
                { id = "8b4dd15e--add"
                , label = "Add Name nested"
                }
                >> Query.count (Expect.equal 1)
            )


withLabel : Documentation
withLabel =
    { headingFull = "Collection With Label"
    , headingShort = "With Label"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasNested ;
                        sh:name "name nested"@en ;
                        sh:class showcase:Nested ;
                        sh:node <NodeShapeNested> ;
                        dash:editor dash:DetailsEditor ;
                        sh:minCount 1 ;
                    .

                    <NodeShapeNested> a sh:NodeShape ;
                        sh:property [
                            sh:path rdfs:label ;
                            sh:name "label"@en ;
                            sh:order 0 ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                        sh:property [
                            sh:path showcase:hasValueA ;
                            sh:name "name property a"@en ;
                            sh:order 1 ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                        sh:property [
                            sh:path showcase:hasValueB ;
                            sh:name "name property b"@en ;
                            sh:order 2 ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUi
            (Query.findAll
                [ Selector.exactText "There are no name nesteds, yet" ]
                >> Query.count (Expect.equal 1)
            )
        |> Documentation.expectUi
            (Query.findAllButtons
                { id = "2ce90bce--add"
                , label = "Add first Name nested"
                }
                >> Query.count (Expect.equal 1)
            )


withOrder : Documentation
withOrder =
    { headingFull = "Collection With Order"
    , headingShort = "With Order"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasNested ;
                        sh:name "name nested"@en ;
                        sh:class showcase:Nested ;
                        sh:node <NodeShapeNested> ;
                        dash:editor dash:DetailsEditor ;
                        hash:detailsEditorOrderPath showcase:hasPosition ;
                        sh:minCount 1 ;
                    .

                    <NodeShapeNested> a sh:NodeShape ;
                        sh:property [
                            sh:path showcase:hasValueA ;
                            sh:name "name property a"@en ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                        sh:property [
                            sh:path showcase:hasValueB ;
                            sh:name "name property b"@en ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUi
            (Query.findAll
                [ Selector.exactText "There are no name nesteds, yet" ]
                >> Query.count (Expect.equal 1)
            )
        |> Documentation.expectUi
            (Query.findAllButtons
                { id = "8b4dd15e--add"
                , label = "Add first Name nested"
                }
                >> Query.count (Expect.equal 1)
            )


withTable : Documentation
withTable =
    { headingFull = "Collection With Table"
    , headingShort = "With Table"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasNested ;
                        sh:name "name nested"@en ;
                        sh:class showcase:Nested ;
                        sh:node <NodeShapeNested> ;
                        dash:editor dash:DetailsEditor ;
                        dash:viewer dash:ValueTableViewer ;
                        sh:minCount 1 ;
                    .

                    <NodeShapeNested> a sh:NodeShape ;
                        sh:property [
                            sh:path showcase:hasValueA ;
                            sh:name "name property a"@en ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                        sh:property [
                            sh:path showcase:hasValueB ;
                            sh:name "name property b"@en ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUi
            (Query.findAll
                [ Selector.exactText "There are no name nesteds, yet" ]
                >> Query.count (Expect.equal 1)
            )
        |> Documentation.expectUi
            (Query.findAllButtons
                { id = "8b4dd15e--add"
                , label = "Add first Name nested"
                }
                >> Query.count (Expect.equal 1)
            )
