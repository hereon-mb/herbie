module Documentation.Combinations exposing (all)

import Shacl.Documentation as Documentation exposing (Documentation)
import Tree exposing (Tree, tree)


all : Tree Documentation
all =
    tree
        (Documentation.fromInfo
            { headingFull = "Combinations"
            , headingShort = "Combinations"
            , description = ""
            }
        )
        [ tree numberWithAffixAndVariants []
        ]


numberWithAffixAndVariants : Documentation
numberWithAffixAndVariants =
    { headingFull = "Number With Affix And Variants"
    , headingShort = "Number With Affix And Variants"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasNested ;
                        sh:name "used"@en ;
                        sh:qualifiedValueShape <NodeShape> ;
                        sh:qualifiedMinCount 0 ;
                        sh:qualifiedMaxCount 1 ;
                    .

                    <NodeShape> a sh:NodeShape ;
                        sh:class showcase:Nested ;
                        sh:property <PropertyShapeNumberWithAffix> ;
                        sh:property <PropertyShapeVariants> ;
                    .
                        <PropertyShapeNumberWithAffix> sh:order 0 .
                        <PropertyShapeVariants> sh:order 1 .

                    <PropertyShapeNumberWithAffix> a sh:PropertyShape ;
                        sh:path showcase:hasNumber ;
                        sh:name "number"@en ;
                        sh:qualifiedValueShape [
                            sh:class showcase:Number ;
                            sh:property [
                                sh:path showcase:hasValue ;
                                sh:node [
                                    sh:property [
                                        sh:path showcase:hasNumericalValue ;
                                        sh:datatype xsd:decimal ;
                                        sh:minCount 1 ;
                                        sh:maxCount 1 ;
                                    ] ;
                                    sh:property [
                                        sh:path showcase:hasUnit ;
                                        sh:hasValue showcase:percent ;
                                        hash:include hash:Suffix ;
                                        sh:minCount 1 ;
                                        sh:maxCount 1 ;
                                    ] ;
                                ] ;
                                sh:minCount 1 ;
                                sh:maxCount 1 ;
                            ] ;
                        ] ;
                        sh:qualifiedMinCount 1 ;
                        sh:qualifiedMaxCount 1 ;
                    .

                    <PropertyShapeVariants> a sh:PropertyShape ;
                        sh:path showcase:hasVariants ;
                        sh:name "perm curve"@en ;
                        sh:qualifiedValueShape [
                            sh:class showcase:Variant ;
                            sh:or (
                                [ sh:class showcase:VariantA ]
                                [ sh:class showcase:VariantB ]
                            ) ;
                        ] ;
                        sh:qualifiedMinCount 1 ;
                        sh:qualifiedMaxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                    showcase:VariantA a rdfs:Class ; rdfs:label "variant A"@en .
                    showcase:VariantB a rdfs:Class ; rdfs:label "variant B"@en .

                    showcase:percent rdfs:label "%" .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
