module Documentation.InputDateTime exposing (all)

import Shacl.Documentation as Documentation exposing (Documentation)
import Tree exposing (Tree, tree)


all : Tree Documentation
all =
    tree
        (Documentation.fromInfo
            { headingFull = "Date and Time Fields"
            , headingShort = "Date and Time"
            , description = ""
            }
        )
        [ tree
            (Documentation.fromInfo
                { headingFull = "Date Fields"
                , headingShort = "Date"
                , description = ""
                }
            )
            [ tree dateRequired []
            , tree dateOptional []
            ]
        , tree
            (Documentation.fromInfo
                { headingFull = "Datetime Fields"
                , headingShort = "Datetime"
                , description = ""
                }
            )
            [ tree dateTimeRequired []
            , tree dateTimeOptional []
            ]
        ]


dateRequired : Documentation
dateRequired =
    { headingFull = "Required Date Fields"
    , headingShort = "Required"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:date ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }


dateOptional : Documentation
dateOptional =
    { headingFull = "Optional Date Fields"
    , headingShort = "Optional"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:date ;
                        sh:minCount 0 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }


dateTimeRequired : Documentation
dateTimeRequired =
    { headingFull = "Required Datetime Fields"
    , headingShort = "Required"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:dateTime ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }


dateTimeOptional : Documentation
dateTimeOptional =
    { headingFull = "Optional Datetime Fields"
    , headingShort = "Optional"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:dateTime ;
                        sh:minCount 0 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
