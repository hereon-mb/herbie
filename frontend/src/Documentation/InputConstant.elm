module Documentation.InputConstant exposing (all)

import Expect
import Expect.Shacl.Form.Viewed as Viewed
import Rdf.Decode as Decode
import Shacl.Documentation as Documentation exposing (Documentation)
import Tree exposing (Tree, tree)


all : Tree Documentation
all =
    tree
        (Documentation.fromInfo
            { headingFull = "Constant Fields"
            , headingShort = "Constant"
            , description = ""
            }
        )
        [ tree constantProperty []
        ]


constantProperty : Documentation
constantProperty =
    { headingFull = "Constant Property"
    , headingShort = "Property"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        a sh:PropertyShape ;
                        sh:path showcase:hasConstant ;
                        sh:hasValue "something" ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .

                    <PropertyShape> a sh:PropertyShape ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectGraph
            (Decode.decode
                (Decode.from
                    (Viewed.document "root")
                    (Decode.predicate (Viewed.showcase "hasConstant") Decode.string)
                )
                >> Result.map (Expect.equal "something")
            )
