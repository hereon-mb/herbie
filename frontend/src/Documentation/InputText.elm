module Documentation.InputText exposing (all)

import Expect
import Shacl.Documentation as Documentation exposing (Documentation)
import Test.Html.Query as Query
import Test.Html.Query.Extra as Query
import Tree exposing (Tree, tree)


all : Tree Documentation
all =
    tree
        (Documentation.fromInfo
            { headingFull = "Text Fields"
            , headingShort = "Text"
            , description = ""
            }
        )
        [ tree
            (Documentation.fromInfo
                { headingFull = "Single-Line Text Input Fields"
                , headingShort = "Single-Line"
                , description = ""
                }
            )
            [ tree singleLineRequired []
            , tree singleLineOptional []
            , tree singleLineWithData []
            , tree singleLineWithDefault []
            , tree singleLineWithDefaultAndData []
            ]
        , tree
            (Documentation.fromInfo
                { headingFull = "Multi-Line Text Input Fields"
                , headingShort = "Multi-Line"
                , description = ""
                }
            )
            [ tree multipleLinesRequired []
            , tree multipleLinesOptional []
            , tree multipleLinesWithData []
            , tree multipleLinesWithDefault []
            , tree multipleLinesWithDefaultAndData []
            ]
        ]


singleLineRequired : Documentation
singleLineRequired =
    { headingFull = "Required Single-Line Text Input Fields"
    , headingShort = "Required"
    , description = "A simple single-line input field which has to be filled out."
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "Description of the field."@en ;
                        sh:datatype xsd:string ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "72860c15"
                , label = "Name"
                , info = Just "Description of the field."
                , value = ""
                }
                >> Query.count (Expect.equal 1)
            )


singleLineOptional : Documentation
singleLineOptional =
    { headingFull = "Optional Single-Line Text Input Fields"
    , headingShort = "Optional"
    , description = "A simple single-line input field which does not have to be filled out."
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "Description of the field."@en ;
                        sh:datatype xsd:string ;
                        sh:minCount 0 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "72860c15"
                , label = "Name"
                , info = Just "Description of the field. (Optional)"
                , value = ""
                }
                >> Query.count (Expect.equal 1)
            )


singleLineWithData : Documentation
singleLineWithData =
    { headingFull = "Single-Line Text Input Fields With Data"
    , headingShort = "With Data"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:string ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasValue "value"
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "72860c15"
                , label = "Name"
                , info = Just "description"
                , value = "value"
                }
                >> Query.count (Expect.equal 1)
            )


singleLineWithDefault : Documentation
singleLineWithDefault =
    { headingFull = "Single-Line Text Input Fields With Default"
    , headingShort = "With Default"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:string ;
                        sh:defaultValue "default" ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUiWithDefaultPopulation
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "72860c15"
                , label = "Name"
                , info = Just "description"
                , value = "default"
                }
                >> Query.count (Expect.equal 1)
            )


singleLineWithDefaultAndData : Documentation
singleLineWithDefaultAndData =
    { headingFull = "Single-Line Text Input Fields With Default and Data"
    , headingShort = "With Default and Data"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:string ;
                        sh:defaultValue "default" ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasValue "value" ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputText
                , id = "72860c15"
                , label = "Name"
                , info = Just "description"
                , value = "value"
                }
                >> Query.count (Expect.equal 1)
            )


multipleLinesRequired : Documentation
multipleLinesRequired =
    { headingFull = "Required Multi-Line Text Input Fields"
    , headingShort = "Required"
    , description = "A simple multi-line text input field which has to be filled out."
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:string ;
                        dash:singleLine false ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputTextarea
                , id = "72860c15"
                , label = "Name"
                , info = Just "description"
                , value = ""
                }
                >> Query.count (Expect.equal 1)
            )


multipleLinesOptional : Documentation
multipleLinesOptional =
    { headingFull = "Optional Multi-Line Text Input Fields"
    , headingShort = "Optional"
    , description = "A simple multi-line text input field which does not have to be filled out."
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:string ;
                        dash:singleLine false ;
                        sh:minCount 0 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ; rdfs:label "root"@en .
                """
            , documentData =
                """
                <root> a showcase:Root .
            """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputTextarea
                , id = "72860c15"
                , label = "Name"
                , info = Just "description (Optional)"
                , value = ""
                }
                >> Query.count (Expect.equal 1)
            )


multipleLinesWithData : Documentation
multipleLinesWithData =
    { headingFull = "Multi-Line Text Input Fields With Data"
    , headingShort = "With Data"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:string ;
                        dash:singleLine false ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasValue "value" ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputTextarea
                , id = "72860c15"
                , label = "Name"
                , info = Just "description"
                , value = "value"
                }
                >> Query.count (Expect.equal 1)
            )


multipleLinesWithDefault : Documentation
multipleLinesWithDefault =
    { headingFull = "Multi-Line Text Input Fields With Default"
    , headingShort = "With Default"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:string ;
                        sh:defaultValue "default" ;
                        dash:singleLine false ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root .
                """
            }
        |> Documentation.expectUiWithDefaultPopulation
            (Query.findAllInputs
                { inputType = Query.InputTextarea
                , id = "72860c15"
                , label = "Name"
                , info = Just "description"
                , value = "default"
                }
                >> Query.count (Expect.equal 1)
            )


multipleLinesWithDefaultAndData : Documentation
multipleLinesWithDefaultAndData =
    { headingFull = "Multi-Line Text Input Fields With Default and Data"
    , headingShort = "With Default and Data"
    , description = ""
    }
        |> Documentation.fromInfo
        |> Documentation.withShacl
            { documentShacl =
                """
                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass showcase:Root ;
                        sh:property <PropertyShape> ;
                    .

                    <PropertyShape> a sh:PropertyShape ;
                        sh:path showcase:hasValue ;
                        sh:name "name"@en ;
                        sh:description "description"@en ;
                        sh:datatype xsd:string ;
                        sh:defaultValue "default" ;
                        dash:singleLine false ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    .

                    showcase:Root a rdfs:Class ;
                        rdfs:label "root"@en ;
                    .
                """
            , documentData =
                """
                    <root> a showcase:Root ;
                        showcase:hasValue "value" ;
                    .
                """
            }
        |> Documentation.expectUi
            (Query.findAllInputs
                { inputType = Query.InputTextarea
                , id = "72860c15"
                , label = "Name"
                , info = Just "description"
                , value = "value"
                }
                >> Query.count (Expect.equal 1)
            )
