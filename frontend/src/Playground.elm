module Playground exposing (Error, Flags, Model, Msg, main)

import Api
import Browser exposing (Document)
import Browser.Dom
import Element as Ui exposing (Element)
import Element.Background as Background
import Element.Extra as Ui
import Element.Font as Font
import Fluent
import Html
import Html.Attributes
import Locale
import Maybe.Extra as Maybe
import Random
import Rdf exposing (Iri)
import Rdf.Format as Format
import Rdf.Graph as Graph
import Rdf.Mint as Mint
import Result.Extra as Result
import Result.Pipeline as Result
import Shacl.Form.Context exposing (C)
import Shacl.Form.Decoded as Decoded
import Shacl.Form.Effect as Effect exposing (Effect)
import Shacl.Form.Viewed as Viewed exposing (Form)
import Shacl.Report as Report exposing (Report)
import String.Extra as String
import Task
import Triple
import UUID
import Ui.Atom.Button as Button exposing (button)
import Ui.Atom.Icon as Icon
import Ui.Atom.Tooltip as Tooltip
import Ui.Molecule.Breadcrumbs as Breadcrumbs
import Ui.Molecule.CodeMirror exposing (codeMirror)
import Ui.Theme.Color as Color
import View


main : Program Flags Model Msg
main =
    Browser.document
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }


type alias Model =
    { flags : Flags
    , c : C
    , seed : Graph.Seed
    , seeds : UUID.Seeds
    , rawGraphShapes : String
    , rawGraphShapesPrevious : Maybe String
    , rawGraphData : String
    , rawGraphDataPrevious : Maybe String
    , form : Result Error Form
    , report : Report
    }


type Error
    = ErrorGraphShapes String Graph.Error
    | ErrorGraphData String Graph.Error
    | ErrorForm Viewed.Error


errorToString : Error -> String
errorToString error =
    case error of
        ErrorGraphShapes raw errorGraph ->
            String.softWrap 80
                ("Could not parse the shape graph: " ++ Graph.errorToString raw errorGraph)

        ErrorGraphData raw errorGraph ->
            String.softWrap 80
                ("Could not parse the data graph: " ++ Graph.errorToString raw errorGraph)

        ErrorForm errorForm ->
            Viewed.errorToString errorForm


type alias Flags =
    { fluentBundles : Fluent.Bundles
    }


init : Flags -> ( Model, Cmd Msg )
init flags =
    let
        c =
            initC

        seed =
            Random.initialSeed 42
                |> Random.step Graph.seedGenerator
                |> Tuple.first

        initFormResult =
            initForm c seed rawGraphShapesDefault rawGraphDataDefault
    in
    ( { flags = flags
      , c = c
      , seed = seed
      , seeds =
            -- FIXME make this random
            { seed1 = Random.initialSeed 1
            , seed2 = Random.initialSeed 2
            , seed3 = Random.initialSeed 3
            , seed4 = Random.initialSeed 4
            }
      , rawGraphShapes = rawGraphShapesDefault
      , rawGraphShapesPrevious = Nothing
      , rawGraphData = rawGraphDataDefault
      , rawGraphDataPrevious = Nothing
      , form = Result.map Triple.first initFormResult
      , report = Report.empty
      }
    , case initFormResult of
        Err _ ->
            Cmd.none

        Ok _ ->
            Cmd.none
    )


initC : C
initC =
    { tooltip = Tooltip.init
    , locale = Locale.EnUS
    , allowUploads = False
    , permissions =
        { superuser = False
        , canSendSparqlQueries = False
        , canUploadRawFiles = False
        }
    , preferences =
        { renderIrisInDropdownMenus = False
        }
    , toUrl = \_ -> Rdf.toUrl
    }


rawGraphShapesDefault : String
rawGraphShapesDefault =
    cleanUpRawGraph
        """
        @base <http://example.org/showcase/> .
        @prefix dash: <http://datashapes.org/dash#> .
        @prefix hash: <http://purls.helmholtz-metadaten.de/herbie/hash/#> .
        @prefix owl: <http://www.w3.org/2002/07/owl#> .
        @prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
        @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
        @prefix sh: <http://www.w3.org/ns/shacl#> .
        @prefix showcase: <http://example.org/showcase/#> .
        @prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

        <RootShape> a sh:NodeShape ;
            hash:documentRoot true ;
            sh:targetClass showcase:Root ;
            sh:property <PropertyShape> ;
        .

        <PropertyShape> a sh:PropertyShape ;
            sh:path showcase:hasValue ;
            sh:name "name"@en ;
            sh:description "Description of the field."@en ;
            sh:datatype xsd:string ;
            sh:minCount 1 ;
            sh:maxCount 1 ;
        .

        showcase:Root a rdfs:Class ;
            rdfs:label "root"@en ;
        .
        """


rawGraphDataDefault : String
rawGraphDataDefault =
    cleanUpRawGraph
        """
        @base <http://example.org/document/> .
        @prefix dcterms: <http://purl.org/dc/terms/> .
        @prefix showcase: <http://example.org/showcase/#> .

        <> dcterms:conformsTo <http://example.org/showcase/> .

        <root> a showcase:Root .
        """


cleanUpRawGraph : String -> String
cleanUpRawGraph =
    String.unindent >> String.trim



-- UPDATE


type Msg
    = UserChangedRawGraphShapes String
    | UserChangedRawGraphData String
    | UserPressedRebuildForm
    | UserPressedReloadData
    | BrowserFocused
    | MsgForm Viewed.Msg
    | CreatedField
        (Api.Response Decoded.Field -> Viewed.Msg)
        (Api.Response
            ( Decoded.Field
            , UUID.Seeds
            )
        )
    | DuplicatedField
        (Api.Response Decoded.Field -> Viewed.Msg)
        (Api.Response
            ( Decoded.Field
            , UUID.Seeds
            )
        )
    | TooltipMsg Tooltip.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UserChangedRawGraphShapes text ->
            ( { model
                | rawGraphShapes = text
                , rawGraphShapesPrevious =
                    if Maybe.isNothing model.rawGraphShapesPrevious then
                        Just model.rawGraphShapes

                    else
                        model.rawGraphShapesPrevious
              }
            , Cmd.none
            )

        UserChangedRawGraphData text ->
            ( { model
                | rawGraphData = text
                , rawGraphDataPrevious =
                    if Maybe.isNothing model.rawGraphDataPrevious then
                        Just model.rawGraphData

                    else
                        model.rawGraphDataPrevious
              }
            , Cmd.none
            )

        UserPressedRebuildForm ->
            reinitForm model

        UserPressedReloadData ->
            reinitForm model

        BrowserFocused ->
            ( model, Cmd.none )

        MsgForm msgViewed ->
            case model.form of
                Err _ ->
                    ( model, Cmd.none )

                Ok form ->
                    let
                        ( formUpdated, effectForm, outMsg ) =
                            Viewed.update { populateDefaultValues = True }
                                model.c
                                msgViewed
                                form
                    in
                    ( case outMsg of
                        Nothing ->
                            { model | form = Ok formUpdated }

                        Just Viewed.GraphChanged ->
                            { model
                                | form = Ok formUpdated
                                , rawGraphData =
                                    Viewed.graphFrontend formUpdated
                                        |> Maybe.map Graph.serializeTurtle
                                        |> Maybe.withDefault model.rawGraphData
                                , rawGraphDataPrevious = Nothing
                            }
                    , effectToCmd model effectForm
                    )

        CreatedField onField result ->
            ( { model
                | seeds =
                    Result.withDefault model.seeds
                        (Result.map Tuple.second result)
              }
            , result
                |> Result.map Tuple.first
                |> onField
                |> emitMsg
                |> Cmd.map MsgForm
            )

        DuplicatedField onField result ->
            ( { model
                | seeds =
                    Result.withDefault model.seeds
                        (Result.map Tuple.second result)
              }
            , result
                |> Result.map Tuple.first
                |> onField
                |> emitMsg
                |> Cmd.map MsgForm
            )

        TooltipMsg msgTooltip ->
            let
                { c } =
                    model
            in
            ( { model
                | c = { c | tooltip = Tooltip.update msgTooltip model.c.tooltip }
              }
            , Cmd.none
            )


reinitForm : Model -> ( Model, Cmd Msg )
reinitForm model =
    let
        initFormResult =
            initForm model.c model.seed model.rawGraphShapes model.rawGraphData
    in
    ( { model
        | rawGraphShapesPrevious = Nothing
        , rawGraphDataPrevious = Nothing
        , form = Result.map Triple.first initFormResult
        , report = Report.empty
      }
    , case initFormResult of
        Err _ ->
            Cmd.none

        Ok _ ->
            Cmd.none
    )


subscriptions : Model -> Sub Msg
subscriptions model =
    case model.form of
        Err _ ->
            Sub.none

        Ok form ->
            Sub.map MsgForm (Viewed.subscriptions form)


view : Model -> Document Msg
view model =
    View.toBrowserDocument
        { fluentBundles = model.flags.fluentBundles
        , sandbox = False
        , dragging = False
        }
        { title = "Herbie's SHACL Playground"
        , element = viewElement model
        , overlays = []
        , breadcrumbs = Breadcrumbs.empty
        , fabVisible = False
        , navigationRailVisible = False
        , loading = False
        }


viewElement : Model -> Element Msg
viewElement model =
    Ui.row
        [ Ui.width Ui.fill
        , Ui.height Ui.fill
        , Ui.padding 16
        , Ui.spacing 16

        -- XXX This fixes vertical scrollbars
        , Ui.style "height" "100%"
        ]
        [ Ui.el
            [ Ui.width Ui.fill
            , Ui.height Ui.fill

            -- XXX This fixes horizontal scrollbars
            , Ui.style "width" "30%"
            ]
            (viewRawGraphShapes model)
        , viewFormArea model
        , Ui.el
            [ Ui.width Ui.fill
            , Ui.height Ui.fill

            -- XXX This fixes horizontal scrollbars
            , Ui.style "width" "30%"
            ]
            (viewRawGraphData model)
        ]


viewRawGraphShapes : Model -> Element Msg
viewRawGraphShapes model =
    codeMirror
        { content = model.rawGraphShapes
        , format = Format.Turtle
        , onChange = UserChangedRawGraphShapes
        }


viewRawGraphData : Model -> Element Msg
viewRawGraphData model =
    codeMirror
        { content = model.rawGraphData
        , format = Format.Turtle
        , onChange = UserChangedRawGraphData
        }


viewFormArea : Model -> Element Msg
viewFormArea model =
    Ui.column
        [ Ui.width Ui.fill
        , Ui.height Ui.fill
        , Ui.spacing 16
        ]
        [ viewActions model
        , viewForm model
        ]


viewActions : Model -> Element Msg
viewActions model =
    Ui.row
        [ Ui.width Ui.fill
        , Ui.spacing 16
        , Ui.paddingXY 0 8
        ]
        [ viewActionRebuildForm model
        , Ui.el
            [ Ui.alignRight ]
            (viewActionReloadData model)
        ]


viewActionRebuildForm : Model -> Element Msg
viewActionRebuildForm model =
    Fluent.verbatim "Rebuild form"
        |> button UserPressedRebuildForm
        |> Button.withFormat Button.Contained
        |> Button.withLeadingIcon Icon.Refresh
        |> Button.withEnabled (Maybe.isJust model.rawGraphShapesPrevious)
        |> Button.view


viewActionReloadData : Model -> Element Msg
viewActionReloadData model =
    Fluent.verbatim "Reload data"
        |> button UserPressedReloadData
        |> Button.withFormat Button.Contained
        |> Button.withLeadingIcon Icon.Refresh
        |> Button.withEnabled (Maybe.isJust model.rawGraphDataPrevious)
        |> Button.view


viewForm : Model -> Element Msg
viewForm model =
    case model.form of
        Err error ->
            Ui.el
                [ Ui.width Ui.fill
                , Background.color Color.surfaceContainerLow
                , Font.family [ Font.typeface "Source Code Pro" ]
                ]
                (Ui.el
                    [ Ui.padding 16 ]
                    (Ui.html
                        (Html.div
                            [ Html.Attributes.style "white-space" "pre"
                            , Html.Attributes.style "line-height" "1.3"
                            ]
                            [ Html.text (errorToString error) ]
                        )
                    )
                )

        Ok form ->
            Ui.el
                [ Ui.width Ui.fill
                , Ui.height Ui.fill
                ]
                (Ui.map MsgForm
                    (Viewed.view model.c model.report form)
                )



-- FORM


initForm :
    C
    -> Graph.Seed
    -> String
    -> String
    ->
        Result
            Error
            ( Form
            , Effect Viewed.Msg
            , Maybe Viewed.OutMsg
            )
initForm c seed rawGraphShapes rawGraphData =
    Ok
        (\graphShapes graphData ->
            { iri = iriDocument
            , preview = False
            , strict = False
            , workspaceUuid = workspaceUuid
            , graph = graphData
            , graphEntered = graphData
            , graphPersisted = graphData
            , graphGenerated = Graph.emptyGraph
            , graphSupport = Graph.emptyGraph
            , graphOntology = graphShapes
            , populateDefaultValues = { populateDefaultValues = True }
            , seed = seed
            }
                |> Viewed.initStatic c
                |> Result.mapError ErrorForm
        )
        |> Result.required
            (Result.mapError (ErrorGraphShapes rawGraphShapes)
                (Graph.parse rawGraphShapes)
            )
        |> Result.required
            (Result.mapError (ErrorGraphData rawGraphData)
                (Graph.parse rawGraphData)
            )
        |> Result.join


iriDocument : Iri
iriDocument =
    Rdf.iri "http://example.org/document/"


workspaceUuid : String
workspaceUuid =
    "workspace-uuid"


effectToCmd : Model -> Effect Viewed.Msg -> Cmd Msg
effectToCmd model effect =
    case effect of
        Effect.None ->
            Cmd.none

        Effect.Batch effects ->
            Cmd.batch (List.map (effectToCmd model) effects)

        Effect.Cmd cmd ->
            Cmd.map MsgForm cmd

        Effect.Msg msg ->
            Cmd.map MsgForm (emitMsg msg)

        -- FORM
        Effect.CreateField onField _ path mint field ->
            model.seeds
                |> Mint.run (Decoded.create path mint field)
                |> Task.attempt (CreatedField onField)

        Effect.DuplicateField onField _ path mint field ->
            model.seeds
                |> Mint.run (Decoded.duplicate path mint field)
                |> Task.attempt (DuplicatedField onField)

        Effect.UserIntentsToCreateDatasetFor _ ->
            -- FIXME Send info to user that this is not available in playground
            Cmd.none

        -- RESOURCES
        Effect.GetResource _ _ ->
            -- FIXME Send info to user that this is not available in playground
            Cmd.none

        Effect.GetSubclassesWithShaclShapes onClasses _ ->
            Cmd.map MsgForm (emitMsg (onClasses (Ok [])))

        Effect.CreateFile _ _ ->
            -- FIXME Send info to user that this is not available in playground
            Cmd.none

        Effect.GetGraph _ _ _ ->
            -- FIXME Send info to user that this is not available in playground
            Cmd.none

        -- UI
        Effect.Focus id ->
            Task.attempt (\_ -> BrowserFocused) (Browser.Dom.focus id)

        Effect.CopyToClipboard _ ->
            -- FIXME Implement this
            Cmd.none

        Effect.AddToast _ ->
            -- FIXME Implement this
            Cmd.none

        Effect.AddToastError _ ->
            -- FIXME Implement this
            Cmd.none

        Effect.TooltipMsg msg ->
            emitMsg (TooltipMsg msg)


emitMsg : msg -> Cmd msg
emitMsg =
    Task.succeed
        >> Task.perform identity
