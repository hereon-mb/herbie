shared-workspace--cast-draft-in-progress = Gussprotokollentwurf vorhanden.  Klicke hier, um damit weiterzumachen.
shared-workspace--create = Erstelle
shared-workspace--draft-list--cast-clerk = { $firstName } { $lastName }
shared-workspace--draft-list--cast-department-client = { $abbreviation }
shared-workspace--draft-list--cast-internal-client = { $firstName } { $lastName }
shared-workspace--draft-list--heading =
    { $draft-count ->
        [one] Ein Entwurf
       *[other] { $draft-count } Entwürfe
    } vorhanden.
shared-workspace--draft-list--heading--collapsed =
    { $draft-count ->
        [one] Ein Entwurf
       *[other] { $draft-count } Entwürfe
    } vorhanden. Klicke hier, um damit weiterzumachen.
shared-workspace--draft-list--heat-treatment-performed-on = Durchgeführt am { DATETIME($date) }
shared-workspace--draft-list--info = Wähle einen Entwurf aus der Liste aus, um weiter daran zu arbeiten.
shared-workspace--error--note-cannot-be-edited = Die Notiz kann nicht editiert werden.
shared-workspace--error--project-cannot-be-edited = Das Projekt kann nicht editiert werden.
shared-workspace--error--protocol-cannot-be-edited = Das Protokoll kann nicht editiert werden.
shared-workspace--file = Datei
shared-workspace--hardness-draft-in-progress = Härteprotokoll vorhanden.  Klicke hier, um damit weiterzumachen.
shared-workspace--heading--workspace = Arbeitsplatz
shared-workspace--heat-treatment-draft-in-progress = Wärmebehandlungsprotokoll vorhanden.  Klicke hier, um damit weiterzumachen.
shared-workspace--material = Material/Produkt
shared-workspace--note = Notiz
shared-workspace--note-draft-in-progress = Notizentwurf vorhanden.  Klicke hier, um damit weiterzumachen.
shared-workspace--photo = Foto
shared-workspace--protocol = Protokoll
shared-workspace--rem = REM
shared-workspace--tem = TEM
