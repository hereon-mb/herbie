shared-workspace--action--cancel = Cancel
shared-workspace--cast-draft-in-progress = Cast protocol draft in progress. Tap to resume.
shared-workspace--chemical-analysis-draft-in-progress = Chemical analysis protocol draft in progress. Tap to resume.
shared-workspace--create = Create
shared-workspace--draft-list--cast-clerk = { $firstName } { $lastName }
shared-workspace--draft-list--cast-department-client = { $abbreviation }
shared-workspace--draft-list--cast-internal-client = { $firstName } { $lastName }
shared-workspace--draft-list--heading =
    { $draft-count ->
        [one] One draft
       *[other] { $draft-count } drafts
    } in progress.
shared-workspace--draft-list--heading--collapsed =
    { $draft-count ->
        [one] One draft
       *[other] { $draft-count } drafts
    } in progress. Tap to resume.
shared-workspace--draft-list--heat-treatment-clerk = { $firstName } { $lastName }
shared-workspace--draft-list--heat-treatment-department-client = { $abbreviation }
shared-workspace--draft-list--heat-treatment-internal-client = { $firstName } { $lastName }
shared-workspace--draft-list--heat-treatment-material-count = { NUMBER($materialCount, useGrouping: "false") } ingots total
shared-workspace--draft-list--info = Select a draft in the list to continue working on it.
shared-workspace--entry-project-draft-in-progress = Project draft in progress. Tap to resume.
shared-workspace--error--note-cannot-be-edited = The note cannot be edited.
shared-workspace--error--project-cannot-be-edited = The project cannot be edited.
shared-workspace--error--protocol-cannot-be-edited = The protocol cannot be edited.
shared-workspace--file = File
shared-workspace--ftir-draft-in-progress = Ftir protocol draft in progress. Tap to resume.
shared-workspace--gpc-draft-in-progress = Gpc protocol draft in progress. Tap to resume.
shared-workspace--hardness-draft-in-progress = Hardness protocol draft in progress. Tap to resume.
shared-workspace--heading--select-class = Create document
shared-workspace--heading--workspace = Workspace
shared-workspace--heat-treatment-draft-in-progress = Heat treatment protocol draft in progress. Tap to resume.
shared-workspace--import-graph = Import graph
shared-workspace--import-ro-crate = Import RO-Crate
shared-workspace--material = Material/Product
shared-workspace--membrane-casting-draft-in-progress = Membrane casting protocol draft in progress. Tap to resume.
shared-workspace--new-powder-draft-in-progress = New Powder protocol draft in progress. Tap to resume.
shared-workspace--note = Note
shared-workspace--note-draft-in-progress = Note draft in progress. Tap to resume.
shared-workspace--photo = Photo
shared-workspace--post-modification-draft-in-progress = Post-modification protocol draft in progress. Tap to resume.
shared-workspace--protocol = Protocol
shared-workspace--rdf-document = Document
shared-workspace--sample-fabrication-draft-in-progress = Sample fabrication protocol draft in progress. Tap to resume.
shared-workspace--sem-draft-in-progress = SEM protocol draft in progress. Tap to resume.
shared-workspace--tem = TEM
