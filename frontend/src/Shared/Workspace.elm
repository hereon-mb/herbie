{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Shared.Workspace exposing
    ( Model, init
    , Msg, update, events, subscriptions
    , view
    , openDialogCreateDocument
    , Overlay, viewOverlayFabMenu
    )

{-|

@docs Model, init
@docs Msg, update, events, subscriptions
@docs view

@docs openDialogCreateDocument

-}

import Action
import Action.DatasetCreate as DatasetCreate
import Api
import Api.Workspace as Workspace
import Browser.Navigation
import Context exposing (C)
import Effect exposing (Effect)
import Element
    exposing
        ( DeviceClass(..)
        , Element
        , alignBottom
        , alignLeft
        , alignRight
        , alignTop
        , centerX
        , centerY
        , column
        , el
        , fill
        , focused
        , height
        , inFront
        , minimum
        , mouseDown
        , mouseOver
        , moveDown
        , moveLeft
        , moveRight
        , moveUp
        , none
        , paddingEach
        , paddingXY
        , paragraph
        , px
        , row
        , scrollbarY
        , shrink
        , spacing
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Extra
    exposing
        ( pointerEvents
        , userSelect
        )
import Element.Font as Font
import Element.Input as Input
import Element.Keyed as Keyed
import Event exposing (Event)
import File exposing (File)
import File.Select
import Fluent
    exposing
        ( Fluent
        , verbatim
        )
import Gen.Route as Route
import Href
import Http
import List.Extra as List
import Maybe.Extra as Maybe
import Ontology.Shacl as Shacl
import Query
import Rdf
import Rdf.Extra as Rdf
import Rdf.Format as Format exposing (Format)
import Rdf.Graph exposing (Graph)
import Shacl.Form.Localize as Localize
import Time
import Time.Distance
import Ui.Atom.Button as Button exposing (button)
import Ui.Atom.Icon as Icon
import Ui.Atom.ProgressIndicatorLinear as ProgressIndicatorLinear
import Ui.Atom.Radio as Radio
import Ui.Atom.TextInput as TextInput exposing (textInput)
import Ui.Atom.Tooltip as Tooltip exposing (Tooltip)
import Ui.Molecule.Dialog as Dialog
import Ui.Molecule.FabMenu
import Ui.Molecule.Listbox as Listbox
import Ui.Theme.Card as Card exposing (card)
import Ui.Theme.Color as Color
    exposing
        ( onPrimaryContainer
        , onSurface
        , onSurfaceVariant
        , primaryContainer
        , scrim
        )
import Ui.Theme.Elevation
import Ui.Theme.Input as Input
import Ui.Theme.Typography
    exposing
        ( body1
        , body1Mono
        , body2
        , body2Strong
        )
import Url.Builder as Url



-- MODEL


type alias Model =
    { tooltip : Tooltip
    , overlay : Overlay
    }


type Overlay
    = NoOverlay
    | ScrimLoading
    | MenuFab
    | DialogCreateDocument DialogCreateDocument
    | DialogImportGraph DataDialogImportGraph


type DialogCreateDocument
    = DialogCreateDocumentLoadingClasses DataDialogCreateDocumentLoadingClasses
    | DialogCreateDocumentFailed Api.Error
    | DialogCreateDocumentChooseClass DataDialogCreateDocumentChooseClass
    | DialogCreateDocumentChooseShape DataDialogCreateDocumentChooseShape


type alias DataDialogCreateDocumentLoadingClasses =
    { workspaceUuid : String
    }


type alias DataDialogCreateDocumentChooseClass =
    { open : Bool
    , workspaceUuid : String
    , classes : List Shacl.Class
    , query : String
    }


type alias DataDialogCreateDocumentChooseShape =
    { open : Bool
    , workspaceUuid : String
    , classes : List Shacl.Class
    , class : Shacl.Class
    , query : String
    }


type alias DataDialogImportGraph =
    { uuid : String
    , file : File
    , format : Maybe Format
    }



-- INIT


init : C -> ( Model, Effect Msg )
init _ =
    ( { tooltip = Tooltip.init
      , overlay = NoOverlay
      }
    , Effect.none
    )



-- UPDATE


type Msg
    = NoOp
      -- FAB MENU
    | UserPressedFab
    | UserPressedFabMenuScrim
      -- FAB ACTION: CREATE DOCUMENT
    | UserPressedCreateDocument
    | ApiReturnedClassesWithShaclShapes (Api.Response Graph)
    | UserChangedQueryClass String
    | UserChangedQueryShape String
    | UserPressedCancelInDialogCreateDocument
    | UserSelectedClassInDialogCreateDocument Shacl.Class
    | UserPressedSelectClassInDialogCreateDocument
    | UserSelectedShapeInDialogCreateDocument Shacl.NodeShape
    | ApiCreatedDataset Bool (Result DatasetCreate.Error String)
      -- FAB ACTION: IMPORT GRAPH
    | UserPressedImportGraph
    | BrowserLoadedGraphFile String File
    | UserSelectedFormat Format
    | UserPressedImportGraphCancel
    | UserPressedImportGraphImport
    | ApiReturnedNewDatasetForImportGraph (Api.Response String)
      -- FAB ACTION: IMPORT RO CRATE
    | UserPressedImportRoCrate
    | BrowserLoadedRoCrateFile String File
    | ApiReturnedNewDatasetForImportRoCrate String (Api.Response ())


update : C -> Browser.Navigation.Key -> Msg -> Model -> ( Model, Effect Msg )
update c key msg model =
    case msg of
        NoOp ->
            ( model, Effect.none )

        -- FAB MENU
        UserPressedFab ->
            ( { model | overlay = MenuFab }
            , Effect.none
            )

        UserPressedFabMenuScrim ->
            ( { model | overlay = NoOverlay }
            , Effect.none
            )

        -- FAB ACTIONS
        UserPressedCreateDocument ->
            case c.workspaceUuid of
                Nothing ->
                    ( model, Effect.none )

                Just uuid ->
                    let
                        dialog =
                            DialogCreateDocumentLoadingClasses { workspaceUuid = uuid }
                    in
                    ( { model | overlay = DialogCreateDocument dialog }
                    , getClassesWithShaclShapes c uuid
                    )

        ApiReturnedClassesWithShaclShapes (Err error) ->
            case model.overlay of
                DialogCreateDocument (DialogCreateDocumentLoadingClasses _) ->
                    ( { model | overlay = DialogCreateDocument (DialogCreateDocumentFailed error) }
                    , Effect.none
                    )

                _ ->
                    ( model, Effect.none )

        ApiReturnedClassesWithShaclShapes (Ok graph) ->
            case model.overlay of
                DialogCreateDocument (DialogCreateDocumentLoadingClasses data) ->
                    let
                        dialog =
                            DialogCreateDocumentChooseClass
                                { open = False
                                , workspaceUuid = data.workspaceUuid
                                , classes = Shacl.fromGraph graph
                                , query = ""
                                }
                    in
                    ( { model | overlay = DialogCreateDocument dialog }
                    , Effect.none
                    )

                _ ->
                    ( model, Effect.none )

        UserPressedCancelInDialogCreateDocument ->
            ( { model | overlay = NoOverlay }
            , Effect.none
            )

        UserChangedQueryClass query ->
            case model.overlay of
                DialogCreateDocument (DialogCreateDocumentChooseClass data) ->
                    let
                        dialog =
                            DialogCreateDocumentChooseClass { data | query = query }
                    in
                    ( { model | overlay = DialogCreateDocument dialog }
                    , Effect.none
                    )

                _ ->
                    ( model, Effect.none )

        UserSelectedClassInDialogCreateDocument class ->
            case model.overlay of
                DialogCreateDocument (DialogCreateDocumentChooseClass data) ->
                    case class.nodeShapes of
                        [ nodeShape ] ->
                            ( { model | overlay = ScrimLoading }
                            , nodeShape.rdfs.isDefinedBy.this
                                |> Action.UserIntentsToCreateDataset
                                    data.open
                                    (ApiCreatedDataset data.open)
                                    data.workspaceUuid
                                |> Effect.fromAction
                            )

                        _ ->
                            let
                                dialog =
                                    DialogCreateDocumentChooseShape
                                        { open = data.open
                                        , workspaceUuid = data.workspaceUuid
                                        , classes = data.classes
                                        , class = class
                                        , query = ""
                                        }
                            in
                            ( { model | overlay = DialogCreateDocument dialog }
                            , Effect.none
                            )

                _ ->
                    ( model, Effect.none )

        UserPressedSelectClassInDialogCreateDocument ->
            case model.overlay of
                DialogCreateDocument (DialogCreateDocumentChooseShape data) ->
                    let
                        dialog =
                            DialogCreateDocumentChooseClass
                                { open = data.open
                                , workspaceUuid = data.workspaceUuid
                                , classes = data.classes
                                , query = ""
                                }
                    in
                    ( { model | overlay = DialogCreateDocument dialog }
                    , Effect.none
                    )

                _ ->
                    ( model, Effect.none )

        UserChangedQueryShape query ->
            case model.overlay of
                DialogCreateDocument (DialogCreateDocumentChooseShape data) ->
                    let
                        dialog =
                            DialogCreateDocumentChooseShape { data | query = query }
                    in
                    ( { model | overlay = DialogCreateDocument dialog }
                    , Effect.none
                    )

                _ ->
                    ( model, Effect.none )

        UserSelectedShapeInDialogCreateDocument nodeShape ->
            case model.overlay of
                DialogCreateDocument (DialogCreateDocumentChooseShape data) ->
                    ( { model | overlay = ScrimLoading }
                    , nodeShape.rdfs.isDefinedBy.this
                        |> Action.UserIntentsToCreateDataset
                            data.open
                            (ApiCreatedDataset data.open)
                            data.workspaceUuid
                        |> Effect.fromAction
                    )

                _ ->
                    ( model, Effect.none )

        ApiCreatedDataset _ (Err error) ->
            ( { model | overlay = NoOverlay }
            , { message = verbatim (DatasetCreate.toString c error)
              }
                |> Action.AddToast
                |> Effect.fromAction
            )

        ApiCreatedDataset open (Ok uuid) ->
            ( { model | overlay = NoOverlay }
            , if open then
                Effect.none

              else
                { uuid = uuid }
                    |> Route.Datasets__Uuid___Draft
                    |> Href.fromRoute c
                    |> Browser.Navigation.pushUrl key
                    |> Effect.fromCmd
            )

        UserPressedImportGraph ->
            case c.workspaceUuid of
                Nothing ->
                    ( model, Effect.none )

                Just uuid ->
                    ( model
                    , File.Select.file
                        [ "application/n-triples"
                        , "text/turtle"
                        , "application/ld+json"
                        ]
                        (BrowserLoadedGraphFile uuid)
                        |> Effect.fromCmd
                    )

        BrowserLoadedGraphFile uuid file ->
            ( { model
                | overlay =
                    DialogImportGraph
                        { uuid = uuid
                        , file = file
                        , format =
                            file
                                |> File.mime
                                |> Format.fromMime
                        }
              }
            , Effect.none
            )

        UserSelectedFormat format ->
            case model.overlay of
                DialogImportGraph data ->
                    ( { model | overlay = DialogImportGraph { data | format = Just format } }
                    , Effect.none
                    )

                _ ->
                    ( model, Effect.none )

        UserPressedImportGraphCancel ->
            ( { model | overlay = NoOverlay }
            , Effect.none
            )

        UserPressedImportGraphImport ->
            case model.overlay of
                DialogImportGraph data ->
                    case data.format of
                        Nothing ->
                            -- FIXME show validation error
                            ( { model | overlay = NoOverlay }
                            , Effect.none
                            )

                        Just format ->
                            ( { model | overlay = ScrimLoading }
                            , Api.request c
                                { method = "POST"
                                , headers =
                                    [ Http.header "Content-Type" (Format.toMime format)
                                    , Http.header "Accept" "application/n-triples"
                                    ]
                                , pathSegments = [ "datasets" ]
                                , queryParameters = [ Url.string "workspace" data.uuid ]
                                , body = Http.fileBody data.file
                                , expect = Api.expectLocation ApiReturnedNewDatasetForImportGraph
                                , tracker = Nothing
                                }
                            )

                _ ->
                    ( model, Effect.none )

        ApiReturnedNewDatasetForImportGraph (Err error) ->
            ( { model | overlay = NoOverlay }
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiReturnedNewDatasetForImportGraph (Ok urlLocation) ->
            let
                uuid =
                    urlLocation
                        |> String.split "/"
                        |> List.reverse
                        |> List.getAt 1
                        |> Maybe.withDefault ""
            in
            ( { model | overlay = NoOverlay }
            , [ Effect.fromAction Action.UpdateTriples
              , { uuid = uuid }
                    |> Route.Datasets__Uuid___Draft
                    |> Href.fromRoute c
                    |> Browser.Navigation.pushUrl key
                    |> Effect.fromCmd
              ]
                |> Effect.batch
            )

        UserPressedImportRoCrate ->
            case c.workspaceUuid of
                Nothing ->
                    ( model, Effect.none )

                Just uuid ->
                    ( model
                    , File.Select.file [ "application/zip" ] (BrowserLoadedRoCrateFile uuid)
                        |> Effect.fromCmd
                    )

        BrowserLoadedRoCrateFile uuid file ->
            let
                contentDisposition =
                    String.join "; "
                        [ "form-data"
                        , "name=\"crate\""
                        , String.join "=" [ "filename", File.name file ]
                        ]
            in
            ( { model | overlay = ScrimLoading }
            , Api.request c
                { method = "POST"
                , headers =
                    [ Http.header "Content-Type" "application/zip"
                    , Http.header "Content-Disposition" contentDisposition
                    ]
                , pathSegments = [ "ro-crate-imports" ]
                , queryParameters = [ Url.string "workspace" uuid ]
                , body = Http.fileBody file
                , expect = Api.expectWhatever (ApiReturnedNewDatasetForImportRoCrate uuid)
                , tracker = Nothing
                }
            )

        ApiReturnedNewDatasetForImportRoCrate _ (Err error) ->
            ( { model | overlay = NoOverlay }
            , error
                |> Action.toastFromError c
                |> Effect.fromAction
            )

        ApiReturnedNewDatasetForImportRoCrate uuid (Ok ()) ->
            ( { model | overlay = NoOverlay }
            , [ Effect.fromAction Action.UpdateTriples
              , { message = verbatim "Importing RO-Crate started." }
                    |> Action.AddToastExpireIn 1500
                    |> Effect.fromAction
              , case c.route of
                    Route.Workspaces__Uuid_ _ ->
                        Effect.fromCmd Browser.Navigation.reload

                    _ ->
                        { uuid = uuid }
                            |> Route.Workspaces__Uuid_
                            |> Href.fromRoute c
                            |> Browser.Navigation.pushUrl key
                            |> Effect.fromCmd
              ]
                |> Effect.batch
            )


getClassesWithShaclShapes : C -> String -> Effect Msg
getClassesWithShaclShapes c uuid =
    let
        iri =
            Api.iriHttpWith c
                [ "construct", "classes-with-shacl-shapes" ]
                [ Url.string "workspace" uuid ]
    in
    iri
        |> Action.GetGraph
            { store = False
            , reload = True
            , closure = False
            }
            ApiReturnedClassesWithShaclShapes
        |> Effect.fromAction


openDialogCreateDocument : String -> List Shacl.Class -> Model -> ( Model, Effect Msg )
openDialogCreateDocument workspaceUuid classes model =
    case model.overlay of
        NoOverlay ->
            case classes of
                [] ->
                    ( model, Effect.none )

                [ class ] ->
                    case class.nodeShapes of
                        [] ->
                            ( model, Effect.none )

                        [ nodeShape ] ->
                            ( model
                            , nodeShape.rdfs.isDefinedBy.this
                                |> Action.UserIntentsToCreateDataset True (ApiCreatedDataset True) workspaceUuid
                                |> Effect.fromAction
                            )

                        _ ->
                            ( { model
                                | overlay =
                                    DialogCreateDocument
                                        (DialogCreateDocumentChooseShape
                                            { open = True
                                            , workspaceUuid = workspaceUuid
                                            , classes = classes
                                            , class = class
                                            , query = ""
                                            }
                                        )
                              }
                            , Effect.none
                            )

                _ ->
                    ( { model
                        | overlay =
                            DialogCreateDocument
                                (DialogCreateDocumentChooseClass
                                    { open = True
                                    , workspaceUuid = workspaceUuid
                                    , classes = classes
                                    , query = ""
                                    }
                                )
                      }
                    , Effect.none
                    )

        _ ->
            ( model, Effect.none )



-- EVENTS


events : Model -> Event Msg
events _ =
    Event.none



-- SUBSCRIPTIONS


subscriptions : C -> Model -> Sub Msg
subscriptions _ _ =
    Sub.none



-- VIEW


view : C -> Model -> Element Msg
view _ _ =
    none


viewOverlayFabMenu : C -> Model -> Element Msg
viewOverlayFabMenu c model =
    if Maybe.unwrap False (Workspace.canCreateDocuments c.author) c.workspace then
        Keyed.el
            [ width fill
            , height fill
            , pointerEvents False
            , inFront <|
                case model.overlay of
                    NoOverlay ->
                        el [] none

                    ScrimLoading ->
                        el
                            [ width fill
                            , height fill
                            , Background.color scrim
                            , pointerEvents True
                            , userSelect True
                            , inFront <|
                                ProgressIndicatorLinear.view ProgressIndicatorLinear.Indeterminate
                            ]
                            none

                    MenuFab ->
                        el [] none

                    DialogCreateDocument dialog ->
                        viewDialogCreateDocument c dialog

                    DialogImportGraph data ->
                        viewDialogImportGraph data
            ]
            ( "fab-menu"
            , if model.overlay == MenuFab then
                viewFabMenu c

              else
                viewOverlayFab c
            )

    else
        none


viewDialogImportGraph : DataDialogImportGraph -> Element Msg
viewDialogImportGraph data =
    [ column
        [ width fill ]
        [ Input.label
            { id = Nothing
            , text = verbatim "File"
            , help = []
            }
        , row
            [ width fill
            , spacing 8
            , paddingEach
                { top = 8
                , bottom = 8
                , left = 16
                , right = 8
                }
            ]
            [ data.file
                |> File.name
                |> verbatim
                |> body1Mono
                |> el [ width fill ]
            ]
        ]
    , Radio.view
        (Radio.radio
            UserSelectedFormat
            (verbatim "File format")
            data.format
            [ Radio.option Format.Turtle (verbatim "Turtle")
            , Radio.option Format.NTriples (verbatim "N-Triples")
            , Radio.option Format.JsonLd (verbatim "JSON-LD")
            ]
        )
    ]
        |> column
            [ width fill
            , spacing 16
            ]
        |> Dialog.desktop (verbatim "Import graph")
        |> Dialog.withActions
            [ "Import"
                |> verbatim
                |> button UserPressedImportGraphImport
                |> Button.view
                |> el [ paddingXY 8 0 ]
            , t "action--cancel"
                |> button UserPressedImportGraphCancel
                |> Button.withFormat Button.Text
                |> Button.view
                |> el [ alignRight ]
            ]
        |> Dialog.withScrimEvents
            { userPressedScrim = UserPressedImportGraphCancel
            , noOp = NoOp
            }
        |> Dialog.toElement


labelClass : C -> Shacl.Class -> String
labelClass c class =
    class.rdfs.label
        |> Maybe.map (Localize.forLabel c)
        |> Maybe.orElse (Rdf.defaultLabel class.this)
        |> Maybe.withDefault (Rdf.toUrl class.this)


viewDialogCreateDocument : C -> DialogCreateDocument -> Element Msg
viewDialogCreateDocument c dialog =
    case dialog of
        DialogCreateDocumentLoadingClasses _ ->
            Dialog.loading
                |> Dialog.wide
                |> Dialog.fillVertically
                |> Dialog.withScrimEvents
                    { userPressedScrim = UserPressedCancelInDialogCreateDocument
                    , noOp = NoOp
                    }
                |> Dialog.toElement

        DialogCreateDocumentFailed error ->
            error
                |> Api.errorToMessages
                |> Localize.verbatims c
                |> verbatim
                |> body1
                |> List.singleton
                |> paragraph
                    [ width fill
                    , centerY
                    , Font.color Color.error
                    , Font.center
                    ]
                |> Dialog.desktop (t "heading--select-class")
                |> Dialog.withActions
                    [ t "action--cancel"
                        |> button UserPressedCancelInDialogCreateDocument
                        |> Button.withFormat Button.Text
                        |> Button.view
                        |> el [ alignRight ]
                    ]
                |> Dialog.wide
                |> Dialog.fillVertically
                |> Dialog.withScrimEvents
                    { userPressedScrim = UserPressedCancelInDialogCreateDocument
                    , noOp = NoOp
                    }
                |> Dialog.toElement

        DialogCreateDocumentChooseClass data ->
            viewDialogCreateDocumentChooseClass c data
                |> Dialog.desktop (t "heading--select-class")
                |> Dialog.withActions
                    [ t "action--cancel"
                        |> button UserPressedCancelInDialogCreateDocument
                        |> Button.withFormat Button.Text
                        |> Button.view
                        |> el [ alignRight ]
                    ]
                |> Dialog.wide
                |> Dialog.fillVertically
                |> Dialog.withScrimEvents
                    { userPressedScrim = UserPressedCancelInDialogCreateDocument
                    , noOp = NoOp
                    }
                |> Dialog.toElement

        DialogCreateDocumentChooseShape data ->
            viewDialogCreateDocumentChooseShape c data
                |> Dialog.desktop (t "heading--select-class")
                |> Dialog.withActions
                    [ "Select different class"
                        |> verbatim
                        |> button UserPressedSelectClassInDialogCreateDocument
                        |> Button.withFormat Button.Text
                        |> Button.withLeadingIcon Icon.ArrowBack
                        |> Button.view
                    , t "action--cancel"
                        |> button UserPressedCancelInDialogCreateDocument
                        |> Button.withFormat Button.Text
                        |> Button.view
                        |> el [ alignRight ]
                    ]
                |> Dialog.wide
                |> Dialog.fillVertically
                |> Dialog.withScrimEvents
                    { userPressedScrim = UserPressedCancelInDialogCreateDocument
                    , noOp = NoOp
                    }
                |> Dialog.toElement


viewDialogCreateDocumentChooseClass : C -> DataDialogCreateDocumentChooseClass -> Element Msg
viewDialogCreateDocumentChooseClass c data =
    [ [ "Select a class you want to create an instance for."
            |> verbatim
            |> body1
            |> List.singleton
            |> paragraph
                [ width fill
                , paddingXY 16 0
                ]
      , data.query
            |> textInput UserChangedQueryClass (TextInput.labelAbove (verbatim "Filter"))
            |> TextInput.view
      ]
        |> column
            [ width fill
            , spacing 16
            ]
    , data.classes
        |> Query.sortByScore (fieldsClass c) data.query
        |> List.sortBy (labelClass c)
        |> List.map (itemClass c)
        |> Listbox.fromItems
        |> Listbox.toElement
        |> el
            [ width fill
            , height fill
            , scrollbarY
            ]
        |> List.singleton
        |> column
            [ width fill
            , height fill
            ]
    ]
        |> column
            [ width fill
            , height fill
            ]


fieldsClass : C -> List (Shacl.Class -> Maybe String)
fieldsClass c =
    [ .rdfs >> .label >> Maybe.map (Localize.forLabel c)
    , .this >> Rdf.toUrl >> Just
    ]


itemClass : C -> Shacl.Class -> Listbox.Item Msg
itemClass c class =
    let
        withSecondaryParagraph =
            case class.rdfs.comment of
                Nothing ->
                    identity

                Just comment ->
                    comment
                        |> Localize.verbatim c
                        |> verbatim
                        |> Listbox.withSecondaryParagraph
    in
    class
        |> labelClass c
        |> verbatim
        |> Listbox.item (UserSelectedClassInDialogCreateDocument class)
        |> withSecondaryParagraph


viewDialogCreateDocumentChooseShape : C -> DataDialogCreateDocumentChooseShape -> Element Msg
viewDialogCreateDocumentChooseShape c data =
    let
        nodeShapes =
            data.class.nodeShapes
                |> Query.sortByScore (fieldsShape c) data.query
                |> List.sortBy (.rdfs >> .isDefinedBy >> .prov >> .generatedAtTime >> Time.posixToMillis)
                |> List.reverse
    in
    [ [ "Selected class:"
            |> verbatim
            |> body1
            |> List.singleton
            |> paragraph
                [ width fill
                , paddingXY 16 0
                ]
      , [ data.class
            |> labelClass c
            |> verbatim
            |> body1
            |> List.singleton
            |> paragraph
                [ width fill
                , Font.color onSurface
                ]
        , data.class.rdfs.comment
            |> Maybe.map
                (Localize.verbatim c
                    >> verbatim
                    >> body2
                    >> List.singleton
                    >> paragraph
                        [ width fill
                        , Font.color onSurfaceVariant
                        ]
                )
            |> Maybe.withDefault none
        ]
            |> column
                [ width fill
                , spacing 6
                ]
            |> card
            |> Card.withType Card.Outlined
            |> Card.toElement
      , "There are several node shapes available for this class. Please select one which should be used to generate the form."
            |> verbatim
            |> body1
            |> List.singleton
            |> paragraph
                [ width fill
                , paddingXY 16 0
                ]
      , if List.length data.class.nodeShapes > 3 then
            data.query
                |> textInput UserChangedQueryShape (TextInput.labelAbove (verbatim "Filter"))
                |> TextInput.view

        else
            none
      ]
        |> column
            [ width fill
            , spacing 16
            ]
    , nodeShapes
        |> List.map (itemShape c)
        |> Listbox.fromItems
        |> Listbox.toElement
        |> el
            [ width fill
            , height fill
            , scrollbarY
            ]
        |> List.singleton
        |> column
            [ width fill
            , height fill
            ]
    ]
        |> column
            [ width fill
            , height fill
            ]


fieldsShape : C -> List (Shacl.NodeShape -> Maybe String)
fieldsShape c =
    [ .rdfs >> .isDefinedBy >> .rdfs >> .label >> Maybe.map (Localize.forLabel c)
    , .this >> Rdf.toUrl >> Just
    ]


itemShape : C -> Shacl.NodeShape -> Listbox.Item Msg
itemShape c nodeShape =
    let
        withSecondaryParagraph =
            case nodeShape.rdfs.isDefinedBy.rdfs.comment of
                Nothing ->
                    identity

                Just comment ->
                    comment
                        |> Localize.verbatim c
                        |> verbatim
                        |> Listbox.withSecondaryParagraph
    in
    nodeShape.rdfs.isDefinedBy.rdfs.label
        |> Maybe.map (Localize.forLabel c)
        |> Maybe.orElse (Rdf.defaultLabel nodeShape.this)
        |> Maybe.withDefault (Rdf.toUrl nodeShape.this)
        |> verbatim
        |> Listbox.item (UserSelectedShapeInDialogCreateDocument nodeShape)
        |> withSecondaryParagraph
        |> Listbox.withSecondaryParagraph (verbatim (Rdf.toUrl nodeShape.rdfs.isDefinedBy.this))
        |> Listbox.withSecondaryParagraphElements
            [ nodeShape.rdfs.isDefinedBy.prov.wasAttributedTo.rdfs.label
                |> Localize.verbatim c
                |> verbatim
                |> body2Strong
            , " "
                |> verbatim
                |> body2
            , Time.Distance.inWordsWithAffix nodeShape.rdfs.isDefinedBy.prov.generatedAtTime c.now
                |> body2
            ]



-- FAB MENU


viewFabMenu : C -> Element Msg
viewFabMenu _ =
    el
        [ width fill
        , height fill
        , pointerEvents True
        , Events.onClick UserPressedFabMenuScrim
        , el
            [ width
                (shrink
                    |> minimum 256
                )
            ]
            (Ui.Molecule.FabMenu.view
                { primaryItem =
                    { icon = Icon.Create
                    , label = t "rdf-document"
                    , onPress = UserPressedCreateDocument
                    }
                , secondaryItems =
                    [ { icon = Icon.FileUpload
                      , label = t "import-graph"
                      , onPress = UserPressedImportGraph
                      }
                    , { icon = Icon.Inventory
                      , label = t "import-ro-crate"
                      , onPress = UserPressedImportRoCrate
                      }
                    ]
                , recentItems = []
                }
            )
            |> el
                [ alignTop
                , alignLeft
                , moveRight 12
                , moveDown 64
                ]
            |> inFront
        ]
        none


viewOverlayFab : C -> Element Msg
viewOverlayFab c =
    { onPress = Just UserPressedFab
    , label =
        Icon.viewNormal Icon.Add
            |> el
                [ centerX
                , centerY
                ]
    }
        |> Input.button
            [ height (px 56)
            , width (px 56)
            , Ui.Theme.Elevation.z6
            , Ui.Theme.Elevation.z8Hover
            , Ui.Theme.Elevation.z8Focus
            , Font.color onPrimaryContainer
            , Ui.Theme.Elevation.z12Pressed
            , Background.color primaryContainer
            , mouseDown [ Background.color (Color.pressedWith onPrimaryContainer primaryContainer) ]
            , mouseOver [ Background.color (Color.hoveredWith onPrimaryContainer primaryContainer) ]
            , focused [ Background.color (Color.focusedWith onPrimaryContainer primaryContainer) ]
            , Border.rounded 16
            , pointerEvents True
            ]
        |> el
            (case c.device.class of
                Phone ->
                    [ alignBottom
                    , alignRight
                    , moveUp 16
                    , moveLeft 16
                    ]

                _ ->
                    [ alignTop
                    , alignLeft
                    , moveRight 12
                    , moveDown 64
                    ]
            )



-- FLUENT


t : String -> Fluent
t key =
    Fluent.text ("shared-workspace--" ++ key)
