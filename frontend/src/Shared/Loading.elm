module Shared.Loading exposing
    ( Loading
    , empty
    , addMany
    , asked, loaded, failed
    , notAsked
    , allLoaded
    , viewMany
    , askNext
    )

{-|

@docs Loading


# Create

@docs empty


# Add

@docs addMany


# Update

@docs asked, loaded, failed


# Query

@docs notAsked
@docs allLoaded


# View

@docs viewMany

-}

import Api
import Context exposing (C)
import Element
    exposing
        ( Element
        , alignTop
        , column
        , el
        , fill
        , height
        , none
        , paddingXY
        , row
        , spacing
        , width
        )
import Element.Font as Font
import Fluent exposing (Fluent, verbatim)
import List.Extra as List
import Rdf exposing (Iri)
import Shacl.Form.Localize as Localize
import Ui.Atom.Icon as Icon
import Ui.Theme.Color exposing (error)
import Ui.Theme.Typography exposing (body2, caption)


type Loading
    = Loading DataLoading


type alias DataLoading =
    List Entry


type alias Entry =
    { iri : Iri
    , asked : Bool
    , loaded : Bool
    , error : Maybe Api.Error
    }


empty : Loading
empty =
    Loading []


addMany : List Iri -> Loading -> Loading
addMany iris (Loading data) =
    let
        irisAdded =
            data
                |> List.map .iri
                |> List.unique
    in
    Loading
        ((iris
            |> List.filter (\iri -> not (List.member iri irisAdded))
            |> List.map init
         )
            ++ data
        )


init : Iri -> Entry
init iri =
    { iri = iri
    , asked = False
    , loaded = False
    , error = Nothing
    }


asked : Loading -> Loading
asked (Loading data) =
    let
        setAsked entry =
            { entry | asked = True }
    in
    data
        |> List.map setAsked
        |> Loading


askNext : Loading -> Maybe ( Iri, Loading )
askNext (Loading data) =
    let
        ( maybeNext, dataUpdated ) =
            List.foldl
                (\entry ( maybeIri, collected ) ->
                    if maybeIri == Nothing then
                        if entry.asked then
                            ( maybeIri, entry :: collected )

                        else if entry.loaded then
                            ( maybeIri, entry :: collected )

                        else if entry.error /= Nothing then
                            ( maybeIri, entry :: collected )

                        else
                            ( Just entry.iri, { entry | asked = True } :: collected )

                    else
                        ( maybeIri, entry :: collected )
                )
                ( Nothing, [] )
                data
    in
    case maybeNext of
        Nothing ->
            Nothing

        Just iri ->
            Just ( iri, Loading dataUpdated )


loaded : Iri -> Loading -> Loading
loaded iri (Loading data) =
    let
        setLoaded entry =
            if entry.iri == iri then
                { entry | loaded = True }

            else
                entry
    in
    data
        |> List.map setLoaded
        |> Loading


failed : Iri -> Api.Error -> Loading -> Loading
failed iri error (Loading data) =
    let
        setFailed entry =
            if entry.iri == iri then
                { entry | error = Just error }

            else
                entry
    in
    data
        |> List.map setFailed
        |> Loading


{-| Return `True` if all entries are loaded or failed with an error.
-}
allLoaded : Loading -> Bool
allLoaded (Loading data) =
    List.all (\entry -> entry.loaded || entry.error /= Nothing) data


notAsked : Loading -> List Iri
notAsked (Loading data) =
    data
        |> List.filter (.asked >> not)
        |> List.map .iri


viewMany : C -> List { label : Fluent, loading : Loading } -> Element msg
viewMany c lists =
    lists
        |> List.map (viewOne c)
        |> column
            [ width fill
            , height fill
            , spacing 8
            ]


viewOne : C -> { label : Fluent, loading : Loading } -> Element msg
viewOne c { label, loading } =
    let
        (Loading data) =
            loading
    in
    if List.isEmpty data then
        none

    else
        [ caption label
        , data
            |> List.reverse
            |> List.map (viewEntry c)
            |> column
                [ width fill
                , spacing 8
                ]
        ]
            |> column
                [ width fill
                , spacing 8
                ]


viewEntry : C -> Entry -> Element msg
viewEntry c entry =
    [ if entry.loaded then
        Icon.CheckCircleOutline
            |> Icon.viewSmall

      else
        case entry.error of
            Nothing ->
                Icon.RadioButtonUnchecked
                    |> Icon.viewSmall

            Just _ ->
                Icon.Error
                    |> Icon.viewSmall
                    |> el
                        [ Font.color error
                        , alignTop
                        ]
    , case entry.error of
        Nothing ->
            entry.iri
                |> Rdf.toUrl
                |> verbatim
                |> body2

        Just e ->
            [ entry.iri
                |> Rdf.toUrl
                |> verbatim
                |> body2
            , errorToString c e
                |> verbatim
                |> body2
                |> el [ Font.color error ]
            ]
                |> column
                    [ width fill
                    , spacing 4
                    , paddingXY 0 2
                    ]
    ]
        |> row
            [ width fill
            , spacing 8
            ]


errorToString : C -> Api.Error -> String
errorToString c e =
    e
        |> Api.errorToMessages
        |> Localize.verbatims c
