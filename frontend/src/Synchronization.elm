module Synchronization exposing
    ( Synchronization
    , synced, changed, failed, finished
    , Msg, Request(..), update
    , subscriptions
    , State(..), state
    )

{-|

@docs Synchronization
@docs synced, changed, failed, finished

@docs Msg, Request, update
@docs subscriptions

@docs State, state

-}

import Api
import Browser.Events


preventSyncForInitial : number
preventSyncForInitial =
    300


type State
    = Synced
    | SyncNeeded
    | Syncing
    | SyncFailed Api.Error


state : Synchronization -> State
state (Synchronization data) =
    case data.error of
        Nothing ->
            case ( data.syncNeeded, data.syncInProgress ) of
                ( False, False ) ->
                    Synced

                ( True, False ) ->
                    SyncNeeded

                ( _, True ) ->
                    Syncing

        Just error ->
            SyncFailed error


type Synchronization
    = Synchronization Data


type alias Data =
    { preventSyncFor : Maybe Float
    , syncNeeded : Bool
    , syncInProgress : Bool
    , error : Maybe Api.Error
    }


synced : Synchronization
synced =
    Synchronization
        { preventSyncFor = Nothing
        , syncNeeded = False
        , syncInProgress = False
        , error = Nothing
        }


changed : Synchronization -> Synchronization
changed (Synchronization data) =
    Synchronization
        { data
            | preventSyncFor = Just preventSyncForInitial
            , syncNeeded = True
        }


failed : Api.Error -> Synchronization -> Synchronization
failed error (Synchronization data) =
    Synchronization
        { data
            | syncInProgress = False
            , error = Just error
        }


finished : Synchronization -> ( Synchronization, Request )
finished (Synchronization data) =
    if data.syncNeeded && data.preventSyncFor == Nothing then
        ( Synchronization
            { data
                | syncNeeded = False
                , syncInProgress = True
            }
        , Sync
        )

    else
        ( Synchronization { data | syncInProgress = False }
        , NoRequest
        )


type Msg
    = OnAnimationFrameDelta Float


type Request
    = NoRequest
    | Sync


update : Msg -> Synchronization -> ( Synchronization, Request )
update msg ((Synchronization data) as synchronization) =
    case msg of
        OnAnimationFrameDelta delta ->
            case data.preventSyncFor of
                Nothing ->
                    ( synchronization, NoRequest )

                Just preventSyncFor ->
                    let
                        preventSyncForNew =
                            preventSyncFor - delta
                    in
                    if preventSyncForNew <= 0 then
                        if data.syncInProgress then
                            ( Synchronization { data | preventSyncFor = Nothing }
                            , NoRequest
                            )

                        else
                            ( Synchronization
                                { data
                                    | preventSyncFor = Nothing
                                    , syncNeeded = False
                                    , syncInProgress = True
                                }
                            , Sync
                            )

                    else
                        ( Synchronization { data | preventSyncFor = Just preventSyncForNew }
                        , NoRequest
                        )


subscriptions : Synchronization -> Sub Msg
subscriptions (Synchronization data) =
    case data.preventSyncFor of
        Nothing ->
            Sub.none

        Just _ ->
            Browser.Events.onAnimationFrameDelta OnAnimationFrameDelta
