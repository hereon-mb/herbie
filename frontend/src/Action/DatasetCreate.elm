module Action.DatasetCreate exposing
    ( Error(..)
    , toString
    )

import Api
import Context exposing (C)
import Rdf exposing (Iri)
import Shacl.Extra as Shacl
import Shacl.Form.Localize as Localize


type Error
    = CouldNotLoadOntology
        { iri : Iri
        , error : Api.Error
        }
    | CouldNotCreateDataset
        { error : Api.Error
        }
    | ShaclError Shacl.Error


toString : C -> Error -> String
toString c error =
    case error of
        CouldNotLoadOntology data ->
            "Could not load the ontology <"
                ++ Rdf.toUrl data.iri
                ++ ">: "
                ++ (data.error
                        |> Api.errorToMessages
                        |> Localize.verbatims c
                   )

        CouldNotCreateDataset data ->
            "Could not create dataset: "
                ++ (data.error
                        |> Api.errorToMessages
                        |> Localize.verbatims c
                   )

        ShaclError errorShacl ->
            "The selected SHACL document cannot be used: " ++ Shacl.errorToString errorShacl
