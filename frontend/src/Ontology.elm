{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ontology exposing
    ( Meta, metaFromGraph
    , Ontology, Class, Property
    , empty, fromGraph
    , classes, getClass
    , properties, getProperty
    , MetaOwl, MetaRdfs
    )

{-|

@docs Meta, metaFromGraph
@docs Ontology, Class, Property


# Create

@docs empty, fromGraph


# Query

@docs classes, getClass
@docs properties, getProperty

-}

import Maybe.Pipeline as Maybe
import Rdf exposing (Iri, StringOrLangString)
import Rdf.Decode as Decode exposing (Decoder)
import Rdf.Decode.Extra as Decode
import Rdf.DictIri as DictIri exposing (DictIri)
import Rdf.Graph exposing (Graph)
import Rdf.Namespaces exposing (a)
import Rdf.Namespaces.OWL as OWL
import Rdf.Namespaces.RDF as RDF
import Rdf.Namespaces.RDFS as RDFS
import Rdf.Query as RDF


type alias Meta =
    { rdfs : MetaRdfs
    , owl : MetaOwl
    }


type alias MetaRdfs =
    { label : Maybe StringOrLangString
    , comment : Maybe StringOrLangString
    }


type alias MetaOwl =
    { versionInfo : Maybe String
    }


metaFromGraph : Iri -> Graph -> Maybe Meta
metaFromGraph iri graph =
    graph
        |> Decode.decode (Decode.from iri decoderMeta)
        |> Result.toMaybe


decoderMeta : Decoder Meta
decoderMeta =
    Decode.predicate a (Decode.exact OWL.ontology)
        |> Decode.andThen
            (\() ->
                Decode.succeed Meta
                    |> Decode.custom
                        (Decode.succeed MetaRdfs
                            |> Decode.optionalStringOrLangString RDFS.label
                            |> Decode.optionalStringOrLangString RDFS.comment
                        )
                    |> Decode.custom
                        (Decode.succeed MetaOwl
                            |> Decode.optional OWL.versionInfo (Decode.map Just Decode.string) Nothing
                        )
            )


type Ontology
    = Ontology OntologyData


type alias OntologyData =
    { classes : DictIri Class
    , properties : DictIri Property
    }


type alias Class =
    { iri : Iri
    , label : Maybe StringOrLangString
    , comment : Maybe StringOrLangString
    }


type alias Property =
    { iri : Iri
    , label : Maybe StringOrLangString
    , comment : Maybe StringOrLangString
    }


empty : Ontology
empty =
    Ontology
        { classes = DictIri.empty
        , properties = DictIri.empty
        }


fromGraph : Graph -> Ontology
fromGraph graph =
    Ontology
        { classes =
            [ RDFS.instancesOfClass graph
            , OWL.instancesOfClass graph
            ]
                |> List.concat
                |> List.filterMap (classFor graph)
                |> DictIri.fromList
        , properties =
            [ RDF.instancesOfProperty graph
            , OWL.instancesOfObjectProperty graph
            , OWL.instancesOfDatatypeProperty graph
            ]
                |> List.concat
                |> List.filterMap (propertyFor graph)
                |> DictIri.fromList
        }


classFor : Graph -> Iri -> Maybe ( Iri, Class )
classFor graph iri =
    Just Class
        |> Maybe.hardcoded iri
        |> Maybe.hardcoded (RDF.rdfsLabelFor graph iri)
        |> Maybe.hardcoded (RDF.rdfsCommentFor graph iri)
        |> Maybe.map (Tuple.pair iri)


propertyFor : Graph -> Iri -> Maybe ( Iri, Property )
propertyFor graph iri =
    Just Property
        |> Maybe.hardcoded iri
        |> Maybe.hardcoded (RDF.rdfsLabelFor graph iri)
        |> Maybe.hardcoded (RDF.rdfsCommentFor graph iri)
        |> Maybe.map (Tuple.pair iri)


classes : Ontology -> List Class
classes (Ontology data) =
    DictIri.values data.classes


getClass : Ontology -> Iri -> Maybe Class
getClass (Ontology data) iri =
    DictIri.get iri data.classes


properties : Ontology -> List Property
properties (Ontology data) =
    DictIri.values data.properties


getProperty : Ontology -> Iri -> Maybe Property
getProperty (Ontology data) iri =
    DictIri.get iri data.properties
