{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Api.Auth exposing (AccessToken, login)

import Api
import Api.C exposing (C)
import Effect exposing (Effect)
import Http
import Iso8601
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Decode
import Time exposing (Posix)


type alias AccessToken =
    { expiry : Posix
    , token : String
    }


decoder : Decoder AccessToken
decoder =
    Decode.succeed AccessToken
        |> Decode.required "expiry" Iso8601.decoder
        |> Decode.required "token" Decode.string


login : C rest -> (Api.Response AccessToken -> msg) -> Effect msg
login c onResponse =
    { method = "POST"
    , headers = [ Http.header "Accept" "application/json" ]
    , pathSegments = [ "auth", "login" ]
    , queryParameters = []
    , body = Http.emptyBody
    , expect = Api.expectJson onResponse decoder
    , tracker = Nothing
    }
        |> Api.request c
