{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Api.Decimal exposing
    ( Decimal
    , fromString, toString, toStringWith
    , fromInt, fromFloat, toFloat
    , decimalPlacesCount
    )

{-|

@docs Decimal
@docs fromString, toString, toStringWith
@docs fromInt, fromFloat, toFloat
@docs decimalPlacesCount

-}

import Decimal as D
import Locale exposing (Locale)


type Decimal
    = Decimal D.Decimal


toFloat : Decimal -> Float
toFloat (Decimal decimal) =
    D.toFloat decimal


fromInt : Int -> Decimal
fromInt int =
    int
        |> D.fromInt
        |> Decimal


fromFloat : Float -> Maybe Decimal
fromFloat float =
    float
        |> D.fromFloat
        |> Maybe.map Decimal


toString : Locale -> Decimal -> String
toString locale (Decimal decimal) =
    String.replace "." (decimalDivider locale) (D.toString decimal)


{-| Convert a `Decimal` to a `String` with at most the given number of decimal
places.
-}
toStringWith : Locale -> Int -> Decimal -> String
toStringWith locale count (Decimal decimal) =
    let
        adjustDecimals strings =
            case strings of
                front :: back :: [] ->
                    if count == 0 then
                        front

                    else if count <= String.length back then
                        front ++ decimalDivider locale ++ String.left count back

                    else
                        front ++ decimalDivider locale ++ back ++ String.repeat (count - String.length back) "0"

                _ ->
                    String.join (decimalDivider locale) strings
    in
    decimal
        |> D.toString
        |> String.split "."
        |> adjustDecimals


decimalDivider : Locale -> String
decimalDivider locale =
    case locale of
        Locale.EnUS ->
            "."

        Locale.De ->
            ","


fromString : Locale -> String -> Maybe Decimal
fromString locale decimal =
    decimal
        |> String.replace (decimalDivider locale) "."
        |> D.fromString
        |> Maybe.map Decimal


decimalPlacesCount : Decimal -> Int
decimalPlacesCount (Decimal decimal) =
    case String.split "." (D.toString decimal) of
        _ :: decimalPlaces :: [] ->
            String.length decimalPlaces

        _ ->
            0
