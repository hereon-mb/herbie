module Api.RoCrateImport exposing (RoCrateImport, Status(..), list)

import Api
import Api.Author as Author exposing (Author)
import Api.C exposing (C)
import Api.Id as Id exposing (Id)
import Api.Tag as Tag
import Effect.Internal exposing (Effect)
import Http
import Iso8601
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Decode
import Time
import Url.Builder


type alias RoCrateImport =
    { id : Id Tag.RoCrateImport
    , url : String
    , createdAt : Time.Posix
    , startedAt : Maybe Time.Posix
    , stoppedAt : Maybe Time.Posix
    , status : Status
    , createdBy : Author
    , filename : String
    }


type Status
    = Pending
    | Started
    | Success
    | Failure


list : C rest -> (Api.Response (List RoCrateImport) -> msg) -> String -> Effect action msg
list c onResponse workspaceUuid =
    Api.request c
        { method = "GET"
        , headers = []
        , pathSegments = [ "ro-crate-imports" ]
        , queryParameters =
            [ Url.Builder.string "workspace" workspaceUuid
            ]
        , body = Http.emptyBody
        , expect = Api.expectJson onResponse (Decode.list decoder)
        , tracker = Nothing
        }


decoder : Decoder RoCrateImport
decoder =
    Decode.succeed RoCrateImport
        |> Decode.required "id" Id.decoder
        |> Decode.required "url" Decode.string
        |> Decode.required "created_at" Iso8601.decoder
        |> Decode.required "started_at"
            (Decode.oneOf
                [ Decode.map Just Iso8601.decoder
                , Decode.null Nothing
                ]
            )
        |> Decode.required "stopped_at"
            (Decode.oneOf
                [ Decode.map Just Iso8601.decoder
                , Decode.null Nothing
                ]
            )
        |> Decode.required "status" decoderStatus
        |> Decode.required "created_by" Author.decoder
        |> Decode.required "filename" Decode.string


decoderStatus : Decoder Status
decoderStatus =
    Decode.andThen
        (\raw ->
            case raw of
                "pending" ->
                    Decode.succeed Pending

                "started" ->
                    Decode.succeed Started

                "success" ->
                    Decode.succeed Success

                "failure" ->
                    Decode.succeed Failure

                _ ->
                    Decode.fail ("unknown status: " ++ raw)
        )
        Decode.string
