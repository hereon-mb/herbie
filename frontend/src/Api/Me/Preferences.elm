module Api.Me.Preferences exposing
    ( Preferences, default
    , decoder
    , update
    )

{-|

@docs Preferences, default
@docs decoder
@docs update

-}

import Api
import Api.C exposing (C)
import Effect.Internal exposing (Effect)
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Decode
import Json.Encode as Encode exposing (Value)


type alias Preferences =
    { renderIrisInDropdownMenus : Bool
    }


default : Preferences
default =
    { renderIrisInDropdownMenus = False
    }


decoder : Decoder Preferences
decoder =
    Decode.succeed Preferences
        |> Decode.hardcoded False


encode : Preferences -> Value
encode _ =
    []
        |> Encode.object


{-| -}
update :
    C rest
    ->
        { preferences : Preferences
        , onResponse : Result Http.Error Preferences -> msg
        }
    -> Effect action msg
update c config =
    Api.request c
        { method = "PUT"
        , headers = [ Http.header "Accept" "application/json" ]
        , pathSegments = [ "me", "preferences" ]
        , queryParameters = []
        , body =
            config.preferences
                |> encode
                |> Http.jsonBody
        , expect = Http.expectJson config.onResponse decoder
        , tracker = Nothing
        }
