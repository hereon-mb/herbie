{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Api.Graph exposing
    ( get
    , mintTask
    )

import Api
import Api.C exposing (C)
import Effect exposing (Effect)
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode
import Rdf exposing (Iri)
import Task exposing (Task)


mintTask : Api.C.C rest -> List String -> String -> Task Api.Error Iri
mintTask c pathSegments nameClass =
    Api.task c
        { method = "POST"
        , headers = []
        , pathSegments = pathSegments ++ [ "mint" ]
        , queryParameters = []
        , body =
            [ [ ( "rdf_class", Encode.string nameClass )
              ]
            ]
                |> Encode.list Encode.object
                |> Http.jsonBody
        , resolver =
            Api.resolveJson
                (Decode.andThen
                    (\strings ->
                        case List.head strings of
                            Nothing ->
                                Decode.fail "no IRI returned"

                            Just string ->
                                Decode.succeed (Rdf.iri string)
                    )
                    decoder
                )
        }


decoder : Decoder (List String)
decoder =
    Decode.list Decode.string


get :
    C rest
    ->
        { iri : Rdf.Iri
        , onResponse : Api.Response String -> msg
        }
    -> Effect msg
get c config =
    Api.requestIri c
        { method = "GET"
        , headers = [ Http.header "Accept" "text/turtle" ]
        , iri = config.iri
        , queryParameters = []
        , body = Http.emptyBody
        , expect = Api.expectString config.onResponse
        , tracker = Nothing
        }
