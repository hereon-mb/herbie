module Api.Workspace exposing
    ( Workspace
    , Membership, MembershipType(..)
    , list, get
    , create, ParamsCreate
    , update, ParamsUpdate
    , addMemberships, ParamsAddMembership
    , removeMembership
    , canEdit, canAddMembers, canRemoveMember, canCreateDocuments
    )

{-|

@docs Workspace
@docs Membership, MembershipType

@docs list, get
@docs create, ParamsCreate
@docs update, ParamsUpdate
@docs addMemberships, ParamsAddMembership
@docs removeMembership

@docs canEdit, canAddMembers, canRemoveMember, canCreateDocuments

-}

import Api
import Api.Author as Author exposing (Author)
import Api.C exposing (C)
import Api.Id as Id exposing (Id)
import Api.Me exposing (AuthorDetail)
import Api.Tag as Tag
import Effect.Internal exposing (Effect)
import Http
import Iso8601
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Decode
import Json.Encode as Encode exposing (Value)
import Time


type alias Workspace =
    { uuid : String
    , url : String
    , createdAt : Time.Posix
    , updatedAt : Time.Posix
    , deletedAt : Maybe Time.Posix
    , createdBy : Author
    , slug : String
    , name : String
    , description : String
    , memberships : List Membership
    }


type alias Membership =
    { id : Id Tag.Membership
    , url : String
    , createdAt : Time.Posix
    , updatedAt : Time.Posix
    , deletedAt : Maybe Time.Posix
    , createdBy : Author
    , author : Author
    , membershipType : MembershipType
    }


type MembershipType
    = Owner
    | Admin
    | Manager
    | Editor
    | Guest


decoderWorkspace : Decoder Workspace
decoderWorkspace =
    Decode.succeed Workspace
        |> Decode.required "uuid" Decode.string
        |> Decode.required "url" Decode.string
        |> Decode.required "created_at" Iso8601.decoder
        |> Decode.required "updated_at" Iso8601.decoder
        |> Decode.required "deleted_at"
            (Decode.oneOf
                [ Decode.map Just Iso8601.decoder
                , Decode.null Nothing
                ]
            )
        |> Decode.required "created_by" Author.decoder
        |> Decode.required "slug" Decode.string
        |> Decode.required "name" Decode.string
        |> Decode.required "description" Decode.string
        |> Decode.required "memberships" (Decode.list decoderMembership)


decoderMembership : Decoder Membership
decoderMembership =
    Decode.succeed Membership
        |> Decode.required "id" Id.decoder
        |> Decode.required "url" Decode.string
        |> Decode.required "created_at" Iso8601.decoder
        |> Decode.required "updated_at" Iso8601.decoder
        |> Decode.required "deleted_at"
            (Decode.oneOf
                [ Decode.map Just Iso8601.decoder
                , Decode.null Nothing
                ]
            )
        |> Decode.required "created_by" Author.decoder
        |> Decode.required "author" Author.decoder
        |> Decode.required "membership_type" membershipType


membershipType : Decoder MembershipType
membershipType =
    Decode.andThen
        (\raw ->
            case raw of
                "owner" ->
                    Decode.succeed Owner

                "admin" ->
                    Decode.succeed Admin

                "manager" ->
                    Decode.succeed Manager

                "editor" ->
                    Decode.succeed Editor

                "guest" ->
                    Decode.succeed Guest

                _ ->
                    Decode.fail ("invalid membership type: " ++ raw)
        )
        Decode.string


list : C rest -> (Api.Response (List Workspace) -> msg) -> Effect action msg
list c onResponse =
    Api.request c
        { method = "GET"
        , headers = []
        , pathSegments = [ "workspaces" ]
        , queryParameters = []
        , body = Http.emptyBody
        , expect = Api.expectJson onResponse (Decode.list decoderWorkspace)
        , tracker = Nothing
        }


get : C rest -> (Api.Response Workspace -> msg) -> String -> Effect action msg
get c onResponse uuid =
    Api.request c
        { method = "GET"
        , headers = []
        , pathSegments = [ "workspaces", uuid ]
        , queryParameters = []
        , body = Http.emptyBody
        , expect = Api.expectJson onResponse decoderWorkspace
        , tracker = Nothing
        }


type alias ParamsCreate =
    { name : String
    , slug : String
    , description : String
    }


create : C rest -> (Api.Response Workspace -> msg) -> ParamsCreate -> Effect action msg
create c onResponse params =
    Api.request c
        { method = "POST"
        , headers = []
        , pathSegments = [ "workspaces" ]
        , queryParameters = []
        , body = Http.jsonBody (encodeParamsCreate params)
        , expect = Api.expectJson onResponse decoderWorkspace
        , tracker = Nothing
        }


encodeParamsCreate : ParamsCreate -> Value
encodeParamsCreate params =
    Encode.object
        [ ( "name", Encode.string params.name )
        , ( "slug", Encode.string params.slug )
        , ( "description", Encode.string params.description )
        ]


type alias ParamsUpdate =
    { name : String
    , slug : String
    , description : String
    }


update : C rest -> (Api.Response Workspace -> msg) -> Workspace -> ParamsUpdate -> Effect action msg
update c onResponse workspace_ params =
    Api.request c
        { method = "PATCH"
        , headers = []
        , pathSegments = [ "workspaces", workspace_.uuid ]
        , queryParameters = []
        , body = Http.jsonBody (encodeParamsUpdate params)
        , expect = Api.expectJson onResponse decoderWorkspace
        , tracker = Nothing
        }


encodeParamsUpdate : ParamsUpdate -> Value
encodeParamsUpdate params =
    Encode.object
        [ ( "name", Encode.string params.name )
        , ( "slug", Encode.string params.slug )
        , ( "description", Encode.string params.description )
        ]


type alias ParamsAddMembership =
    { author : Author
    , membershipType : MembershipType
    }


addMemberships : C rest -> (Api.Response Workspace -> msg) -> String -> List ParamsAddMembership -> Effect action msg
addMemberships c onResponse uuid paramses =
    Api.request c
        { method = "PATCH"
        , headers = []
        , pathSegments = [ "workspaces", uuid ]
        , queryParameters = []
        , body = Http.jsonBody (encodeParamsAddMemberships paramses)
        , expect = Api.expectJson onResponse decoderWorkspace
        , tracker = Nothing
        }


encodeParamsAddMemberships : List ParamsAddMembership -> Value
encodeParamsAddMemberships paramses =
    Encode.object
        [ ( "memberships"
          , Encode.list encodeParamsAddMembership paramses
          )
        ]


encodeParamsAddMembership : ParamsAddMembership -> Value
encodeParamsAddMembership params =
    Encode.object
        [ ( "author_id", Id.encode params.author.id )
        , ( "membership_type", encodeMembershipType params.membershipType )
        ]


encodeMembershipType : MembershipType -> Value
encodeMembershipType membershipType_ =
    Encode.string
        (case membershipType_ of
            Owner ->
                "owner"

            Admin ->
                "admin"

            Manager ->
                "manager"

            Editor ->
                "editor"

            Guest ->
                "guest"
        )


removeMembership : C rest -> (Api.Response Workspace -> msg) -> Membership -> Effect action msg
removeMembership c onResponse membership_ =
    Api.request c
        { method = "DELETE"
        , headers = []
        , pathSegments = [ "memberships", Id.toString membership_.id ]
        , queryParameters = []
        , body = Http.emptyBody
        , expect = Api.expectJson onResponse decoderWorkspace
        , tracker = Nothing
        }


{-| -}
canEdit : AuthorDetail -> Workspace -> Bool
canEdit =
    isAtLeastAdmin


{-| -}
canAddMembers : AuthorDetail -> Workspace -> Bool
canAddMembers =
    isAtLeastAdmin


{-| -}
canRemoveMember : AuthorDetail -> Workspace -> Membership -> Bool
canRemoveMember author workspace membership =
    let
        isOwnMembership =
            membership.author.id == author.id
    in
    not isOwnMembership && isAtLeastAdmin author workspace


isAtLeastAdmin : AuthorDetail -> Workspace -> Bool
isAtLeastAdmin author =
    .memberships
        >> List.any
            (\membership ->
                (membership.author.id == author.id)
                    && (membership.deletedAt == Nothing)
                    && ((membership.membershipType == Owner)
                            || (membership.membershipType == Admin)
                       )
            )


{-| -}
canCreateDocuments : AuthorDetail -> Workspace -> Bool
canCreateDocuments author =
    .memberships
        >> List.any
            (\membership ->
                (membership.author.id == author.id)
                    && (membership.deletedAt == Nothing)
                    && ((membership.membershipType == Owner)
                            || (membership.membershipType == Admin)
                            || (membership.membershipType == Manager)
                            || (membership.membershipType == Editor)
                       )
            )
