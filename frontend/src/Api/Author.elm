module Api.Author exposing
    ( Author
    , list
    , decoder
    )

{-|

@docs Author

@docs list

@docs decoder

-}

import Api
import Api.C exposing (C)
import Api.Id as Id exposing (Id)
import Api.Tag as Tag
import Api.User as User exposing (User)
import Effect.Internal exposing (Effect)
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Decode


type alias Author =
    { id : Id Tag.Author
    , url : String
    , user : User
    }


decoder : Decoder Author
decoder =
    Decode.succeed Author
        |> Decode.required "id" Id.decoder
        |> Decode.required "url" Decode.string
        |> Decode.required "user" User.decoder


list : C rest -> (Api.Response (List Author) -> msg) -> Effect action msg
list c onResponse =
    Api.request c
        { method = "GET"
        , headers = []
        , pathSegments = [ "authors" ]
        , queryParameters = []
        , body = Http.emptyBody
        , expect = Api.expectJson onResponse (Decode.list decoder)
        , tracker = Nothing
        }
