module Api.User exposing (User, decoder)

{-| -}

import Api.Id as Id exposing (Id)
import Api.Tag as Tag
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Decode


type alias User =
    { id : Id Tag.User
    , url : String
    , username : String
    , email : String
    , firstName : String
    , lastName : String
    }


decoder : Decoder User
decoder =
    Decode.succeed User
        |> Decode.required "id" Id.decoder
        |> Decode.required "url" Decode.string
        |> Decode.required "username" Decode.string
        |> Decode.required "email" Decode.string
        |> Decode.required "first_name" Decode.string
        |> Decode.required "last_name" Decode.string
