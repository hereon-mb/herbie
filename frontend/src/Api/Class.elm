module Api.Class exposing (list)

{-|

@docs list

-}

import Action
import Api
import Context exposing (C)
import Effect exposing (Effect)
import Ontology.Class as Class exposing (Class)
import Url.Builder


list : C -> (Api.Response (List Class) -> msg) -> String -> Effect msg
list c onResponse workspaceUuid =
    let
        iri =
            Api.iriHttpWith c
                [ "construct", "classes" ]
                [ Url.Builder.string "workspace" workspaceUuid ]
    in
    iri
        |> Action.GetGraph
            { store = False
            , reload = True
            , closure = False
            }
            (Result.map Class.fromGraph >> onResponse)
        |> Effect.fromAction
