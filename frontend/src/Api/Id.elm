{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Api.Id exposing
    ( Id
    , decoder, encode
    , fromInt
    , toString
    )

{-|

@docs Id
@docs decoder, encode
@docs fromInt
@docs toString

@docs Dict, dictEmpty

-}

import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode exposing (Value)


type Id tag
    = Id Int


toString : Id tag -> String
toString (Id theId) =
    String.fromInt theId


{-| FIXME we want to remove this eventually!
-}
fromInt : Int -> Id tag
fromInt =
    Id


decoder : Decoder (Id tag)
decoder =
    Decode.map Id Decode.int


encode : Id tag -> Value
encode (Id id) =
    Encode.int id
