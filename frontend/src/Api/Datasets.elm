module Api.Datasets exposing
    ( create
    , aliasDelete, aliasCreate
    , publish
    , edit, editWith
    , cancel
    , canUpdate, canEdit, canUpdateDraft, canDelete
    , isAtLeastManager
    )

{-|

@docs create
@docs aliasDelete, aliasCreate
@docs publish
@docs edit, editWith
@docs cancel

@docs canUpdate, canEdit, canUpdateDraft, canDelete
@docs isAtLeastManager

-}

import Api
import Api.Me as Me exposing (AuthorDetail)
import Api.Workspace as Workspace exposing (Workspace)
import Context exposing (C)
import Effect exposing (Effect)
import File exposing (File)
import Http
import Maybe.Extra as Maybe
import Ontology.Herbie as Herbie
import Rdf
import Rdf.Format as Rdf
import Rdf.Graph as Rdf
import Rdf.Namespaces.DCTERMS as DCTERMS
import Url.Builder


{-| -}
create :
    C
    ->
        { workspaceUuid : String
        , targetClass : Rdf.Iri
        , conformsTo : Rdf.Iri
        , onResponse : Api.Response String -> msg
        }
    -> Effect msg
create c config =
    Api.request c
        { method = "POST"
        , headers = [ acceptTextTurtle ]
        , pathSegments = [ "datasets" ]
        , queryParameters =
            [ Url.Builder.string "mint" (Rdf.toUrl config.targetClass)
            , Url.Builder.string "workspace" config.workspaceUuid
            ]
        , body =
            Rdf.emptyGraph
                |> Rdf.insert (Rdf.iri "") DCTERMS.conformsTo config.conformsTo
                |> Rdf.serialize
                -- We have to specify text/turtle as
                -- application/n-triples does not support relative
                -- IRI's
                |> Http.stringBody "text/turtle"
        , expect = Api.expectLocation config.onResponse
        , tracker = Nothing
        }


{-| -}
aliasCreate :
    C
    ->
        { uuid : String
        , alias : Rdf.Iri
        , onResponse : Rdf.Iri -> Api.Response Rdf.Graph -> msg
        }
    -> Effect msg
aliasCreate c config =
    Api.request c
        { method = "POST"
        , headers = [ acceptTextTurtle ]
        , pathSegments = [ "datasets", config.uuid, "draft", "aliases" ]
        , queryParameters = [ Url.Builder.string "iri" (Rdf.toUrl config.alias) ]
        , body = Http.emptyBody
        , expect = Api.expectGraph (config.onResponse (iriDataset c config))
        , tracker = Nothing
        }


{-| -}
aliasDelete :
    C
    ->
        { uuid : String
        , alias : Rdf.Iri
        , onResponse : Rdf.Iri -> Api.Response Rdf.Graph -> msg
        }
    -> Effect msg
aliasDelete c config =
    Api.request c
        { method = "DELETE"
        , headers = [ acceptTextTurtle ]
        , pathSegments = [ "datasets", config.uuid, "draft", "aliases" ]
        , queryParameters = [ Url.Builder.string "iri" (Rdf.toUrl config.alias) ]
        , body = Http.emptyBody
        , expect = Api.expectGraph (config.onResponse (iriDataset c config))
        , tracker = Nothing
        }


{-| -}
publish :
    C
    ->
        { uuid : String
        , onResponse : Rdf.Iri -> Api.Response Rdf.Graph -> msg
        }
    -> Effect msg
publish c config =
    Api.request c
        { method = "POST"
        , headers = [ acceptTextTurtle ]
        , pathSegments = [ "datasets", config.uuid, "draft", "publish" ]
        , queryParameters = []
        , body = Http.emptyBody
        , expect = Api.expectGraphOrValidationReport (config.onResponse (iriDataset c config))
        , tracker = Nothing
        }


{-| -}
edit :
    C
    ->
        { uuid : String
        , onResponse : Rdf.Iri -> Api.Response () -> msg
        }
    -> Effect msg
edit c config =
    Api.request c
        { method = "POST"
        , headers = [ acceptTextTurtle ]
        , pathSegments = [ "datasets", config.uuid, "edit" ]
        , queryParameters = []
        , body = Http.emptyBody
        , expect = Api.expectWhatever (config.onResponse (iriDataset c config))
        , tracker = Nothing
        }


{-| -}
editWith :
    C
    ->
        { uuid : String
        , onResponse : Rdf.Iri -> Api.Response () -> msg
        , file : File
        , format : Rdf.Format
        }
    -> Effect msg
editWith c config =
    Api.request c
        { method = "POST"
        , headers =
            [ Http.header "Content-Type" (Rdf.toMime config.format)
            , acceptTextTurtle
            ]
        , pathSegments = [ "datasets", config.uuid, "edit" ]
        , queryParameters = []
        , body = Http.fileBody config.file
        , expect = Api.expectWhatever (config.onResponse (iriDataset c config))
        , tracker = Nothing
        }


{-| -}
cancel :
    C
    ->
        { uuid : String
        , onResponse : Rdf.Iri -> Api.Response () -> msg
        }
    -> Effect msg
cancel c config =
    Api.request c
        { method = "POST"
        , headers = [ acceptTextTurtle ]
        , pathSegments = [ "datasets", config.uuid, "draft", "cancel" ]
        , queryParameters = []
        , body = Http.emptyBody
        , expect = Api.expectWhatever (config.onResponse (iriDataset c config))
        , tracker = Nothing
        }


iriDataset : C -> { rest | uuid : String } -> Rdf.Iri
iriDataset c { uuid } =
    Api.iriHttp c [ "datasets", uuid ]


acceptTextTurtle : Http.Header
acceptTextTurtle =
    Http.header "Accept" "text/turtle"


canDelete : AuthorDetail -> Workspace -> Bool
canDelete =
    isAtLeastManager


canEdit : C -> AuthorDetail -> Herbie.Document -> Workspace -> Bool
canEdit c author document workspace =
    not (draftExists document)
        && (isAtLeastManager author workspace || isCreator c author document)


canUpdate : C -> AuthorDetail -> Herbie.Document -> Workspace -> Bool
canUpdate c author document workspace =
    isAtLeastManager author workspace || isCreator c author document


isAtLeastManager : AuthorDetail -> Workspace -> Bool
isAtLeastManager author workspace =
    workspace.memberships
        |> List.any
            (\membership ->
                (membership.author.id == author.id)
                    && (membership.deletedAt == Nothing)
                    && ((membership.membershipType == Workspace.Owner)
                            || (membership.membershipType == Workspace.Admin)
                            || (membership.membershipType == Workspace.Manager)
                       )
            )


canUpdateDraft : C -> AuthorDetail -> Herbie.Document -> Bool
canUpdateDraft c author document =
    draftExists document
        && isCreator c author document


isCreator : C -> AuthorDetail -> Herbie.Document -> Bool
isCreator c author document =
    document.prov.wasAttributedTo.this == Me.iri c author


draftExists : Herbie.Document -> Bool
draftExists document =
    Maybe.isJust document.draft
