{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Api.Me exposing
    ( AuthToken
    , AuthorDetail
    , Permissions
    , getAuthor
    , iri
    )

import Api
import Api.C exposing (C)
import Api.Id as Id exposing (Id)
import Api.Me.Preferences as Preferences exposing (Preferences)
import Api.Tag as Tag
import Api.User as User exposing (User)
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra as Decode
import Json.Decode.Pipeline as Decode
import Rdf exposing (Iri)
import Time exposing (Posix)


type alias AuthorDetail =
    { id : Id Tag.Author
    , user : User
    , permissions : Permissions
    , preferencesAuthor : Preferences
    , authTokens : List AuthToken
    }


type alias Permissions =
    { superuser : Bool
    , canSendSparqlQueries : Bool
    , canUploadRawFiles : Bool
    }


type alias AuthToken =
    { tokenKey : String
    , expiry : Posix
    }


decoder : Decoder AuthorDetail
decoder =
    Decode.succeed AuthorDetail
        |> Decode.required "id" Id.decoder
        |> Decode.required "user" User.decoder
        |> Decode.required "permissions" permissionsDecoder
        |> Decode.required "preferences_author"
            (Decode.oneOf
                [ Preferences.decoder
                , Decode.succeed Preferences.default
                ]
            )
        |> Decode.required "auth_tokens" (Decode.list authTokenDecoder)


permissionsDecoder : Decoder Permissions
permissionsDecoder =
    Decode.succeed Permissions
        |> Decode.required "superuser" Decode.bool
        |> Decode.required "can_send_sparql_queries" Decode.bool
        |> Decode.required "can_upload_raw_files" Decode.bool


authTokenDecoder : Decoder AuthToken
authTokenDecoder =
    Decode.succeed AuthToken
        |> Decode.required "token_key" Decode.string
        |> Decode.required "expiry" Decode.posix


getAuthor : (Api.Response AuthorDetail -> msg) -> Cmd msg
getAuthor onResponse =
    { method = "GET"
    , headers = [ Http.header "Accept" "application/json" ]
    , url = Api.absolute [ "me", "author" ] []
    , body = Http.emptyBody
    , expect = Api.expectJson onResponse decoder
    , timeout = Nothing
    , tracker = Nothing
    }
        |> Http.request


iri : C rest -> AuthorDetail -> Iri
iri c { id } =
    Api.iriHttp c [ "authors", Id.toString id ]
