module Api.Instance exposing (list, get)

{-|

@docs list, get

-}

import Action
import Api
import Context exposing (C)
import Effect exposing (Effect)
import List.Extra as List
import List.NonEmpty as NonEmpty
import Ontology.Instance as Instance exposing (Instance)
import Rdf exposing (Iri)
import Rdf.Decode as Decode exposing (Decoder)
import Rdf.Graph as Rdf exposing (Graph)
import Rdf.Namespaces exposing (a)
import Rdf.Namespaces.RDFS as RDFS
import Rdf.PropertyPath as Rdf
import Url.Builder


list : C -> (Api.Response (List Instance) -> msg) -> Action.ConfigGetInstances -> Effect msg
list c onResponse config =
    let
        iri =
            Api.iriHttpWith c
                [ "construct", "instances" ]
                (Url.Builder.string "workspace" config.workspaceUuid
                    :: Url.Builder.string "class" (Rdf.toUrl config.class)
                    :: List.map (Rdf.toUrl >> Url.Builder.string "includes") config.includes
                )
    in
    iri
        |> Action.GetGraph
            { store = False
            , reload = True
            , closure = False
            }
            (Result.map (instancesFromGraph config.class) >> onResponse)
        |> Effect.fromAction


instancesFromGraph : Iri -> Rdf.Graph -> List Instance
instancesFromGraph class graph =
    graph
        |> Decode.decode (Decode.map (List.uniqueBy .this) (decoderInstancesOf class))
        |> Result.withDefault []


decoderInstancesOf : Iri -> Decoder (List Instance)
decoderInstancesOf class =
    let
        decoderInstance =
            Decode.oneOf
                [ Decode.property
                    (Rdf.InversePath (Rdf.PredicatePath a))
                    (Decode.map (List.filterMap Rdf.toIri) (Decode.many Decode.blankNodeOrIri)
                        |> Decode.andThen
                            (List.foldl
                                (\iri -> Decode.map2 (::) (Decode.map adjustClasses (Decode.from iri Instance.decoder)))
                                (Decode.succeed [])
                            )
                    )
                , Decode.succeed []
                ]

        adjustClasses instance =
            case
                ( NonEmpty.find (.this >> (==) class) instance.a
                , NonEmpty.filter (.this >> (/=) class) instance.a
                )
            of
                ( Just aClass, Just aWithoutClass ) ->
                    { instance | a = NonEmpty.cons aClass aWithoutClass }

                _ ->
                    instance
    in
    Decode.andThen
        (List.unique
            >> List.filter ((/=) class)
            >> List.foldl
                (\classChild -> Decode.map2 (++) (Decode.from classChild decoderInstance))
                (Decode.from class decoderInstance)
        )
        (Decode.from class
            (Decode.property (Rdf.InversePath (Rdf.PredicatePath RDFS.subClassOf))
                (Decode.many Decode.iri)
            )
        )


get : C -> (Api.Response Instance.Instance -> msg) -> Iri -> Effect msg
get c onResponse iri =
    let
        iriGraph =
            Api.iriHttpWith c
                [ "graph" ]
                [ Url.Builder.string "iri" (Rdf.toUrl iri)
                , Url.Builder.string "workspace" (Maybe.withDefault "" c.workspaceUuid)
                ]
    in
    iriGraph
        |> Action.GetGraph
            { store = False
            , reload = True
            , closure = False
            }
            (Result.andThen (detailsFromGraph iri >> Result.mapError Api.CouldNotDecodeRdf) >> onResponse)
        |> Effect.fromAction


detailsFromGraph : Iri -> Graph -> Result Decode.Error Instance.Instance
detailsFromGraph iri graph =
    Decode.decode (Decode.from iri Instance.decoder) graph
