module Api.Data exposing
    ( ApiData(..)
    , fromResult
    , toMaybe
    )


type ApiData error a
    = NotAsked
    | Loading
    | Failure error
    | Success a
    | Reloading a


toMaybe : ApiData error a -> Maybe a
toMaybe apiData =
    case apiData of
        NotAsked ->
            Nothing

        Loading ->
            Nothing

        Failure _ ->
            Nothing

        Success a ->
            Just a

        Reloading a ->
            Just a


fromResult : Result error a -> ApiData error a
fromResult result =
    case result of
        Err error ->
            Failure error

        Ok a ->
            Success a
