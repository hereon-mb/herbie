module Api.Triples exposing (get, getPrev, getNext)

{-|

@docs get, getPrev, getNext

-}

import Api
import Api.C exposing (C)
import Effect exposing (Effect)
import Http
import Rdf
import Rdf.Graph exposing (Graph)
import Url.Builder


get : C rest -> (Api.Response ( Graph, Maybe String, String ) -> msg) -> Effect msg
get c onResponse =
    Api.request c
        { method = "GET"
        , headers = [ Http.header "Accept" "application/n-triples" ]
        , pathSegments = [ "datasets" ]
        , queryParameters = [ Url.Builder.int "page_size" 8 ]
        , body = Http.emptyBody
        , expect = Api.expectGraphWithPrevAndNext onResponse
        , tracker = Nothing
        }


getPrev : C rest -> (Api.Response ( Graph, Maybe String ) -> msg) -> String -> Effect msg
getPrev c onResponse prev =
    Api.requestIri c
        { method = "GET"
        , headers = [ Http.header "Accept" "application/n-triples" ]
        , iri = Rdf.iri prev
        , queryParameters = []
        , body = Http.emptyBody
        , expect = Api.expectGraphWithPrev onResponse
        , tracker = Nothing
        }


getNext : C rest -> (Api.Response ( Graph, String ) -> msg) -> String -> Effect msg
getNext c onResponse next =
    Api.requestIri c
        { method = "GET"
        , headers = [ Http.header "Accept" "application/n-triples" ]
        , iri = Rdf.iri next
        , queryParameters = []
        , body = Http.emptyBody
        , expect = Api.expectGraphWithNext onResponse
        , tracker = Nothing
        }
