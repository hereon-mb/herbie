{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Api.Login exposing (create)

import Api
import Http


create :
    { onResponse : Api.Response () -> msg
    , csrfToken : String
    , username : String
    , password : String
    }
    -> Cmd msg
create { onResponse, csrfToken, username, password } =
    { url = "/login/"
    , body =
        Http.multipartBody
            [ Http.stringPart "csrfmiddlewaretoken" csrfToken
            , Http.stringPart "username" username
            , Http.stringPart "password" password
            ]
    , expect = Api.expectWhatever onResponse
    }
        |> Http.post
