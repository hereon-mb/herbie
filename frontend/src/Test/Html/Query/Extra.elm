module Test.Html.Query.Extra exposing
    ( findAllButtons
    , findAllInputs, InputType(..)
    , findAllRadioGroups, findAllRadioButtons
    , findAllChoiceChipGroups
    , findAllComboboxSingleSelects
    , findAllComboboxMultiSelects
    , findAllSegmentedButtonsGroups
    , findAllCheckboxes
    )

{-|

@docs findAllButtons
@docs findAllInputs, InputType
@docs findAllRadioGroups, findAllRadioButtons
@docs findAllChoiceChipGroups
@docs findAllComboboxSingleSelects
@docs findAllComboboxMultiSelects
@docs findAllSegmentedButtonsGroups
@docs findAllCheckboxes

-}

import Html.Attributes as Attributes
import Test.Html.Query as Query
import Test.Html.Selector as Selector


type InputType
    = InputText
    | InputTextarea


findAllButtons :
    { id : String
    , label : String
    }
    -> Query.Single msg
    -> Query.Multiple msg
findAllButtons config =
    Query.findAll
        [ Selector.id config.id
        , Selector.containing
            [ Selector.exactText config.label ]
        ]


findAllInputs :
    { inputType : InputType
    , id : String
    , label : String
    , info : Maybe String
    , value : String
    }
    -> Query.Single msg
    -> Query.Multiple msg
findAllInputs config =
    let
        idLabel =
            config.id ++ "--label"

        selectorLabel =
            Selector.containing
                [ Selector.all
                    [ Selector.id idLabel
                    , Selector.containing
                        [ Selector.exactText config.label ]
                    ]
                ]

        selectorInfo info =
            Selector.containing
                [ Selector.all
                    [ Selector.exactText info ]
                ]
    in
    case config.inputType of
        InputText ->
            let
                selectorInput =
                    Selector.containing
                        [ Selector.all
                            [ Selector.tag "input"
                            , Selector.id config.id
                            , Selector.attribute (Attributes.type_ "text")
                            , Selector.attribute (Attributes.attribute "aria-labelledby" idLabel)
                            , Selector.attribute (Attributes.value config.value)
                            ]
                        ]
            in
            Query.findAll
                [ Selector.all
                    (List.filterMap identity
                        [ Just selectorInput
                        , Just selectorLabel
                        , Maybe.map selectorInfo config.info
                        ]
                    )
                ]

        InputTextarea ->
            let
                selectorTextarea =
                    Selector.containing
                        [ Selector.all
                            [ Selector.tag "textarea"
                            , Selector.id config.id
                            , Selector.attribute (Attributes.attribute "aria-labelledby" idLabel)
                            , Selector.attribute (Attributes.value config.value)
                            ]
                        ]
            in
            Query.findAll
                [ Selector.all
                    (List.filterMap identity
                        [ Just selectorTextarea
                        , Just selectorLabel
                        , Maybe.map selectorInfo config.info
                        ]
                    )
                ]


findAllRadioGroups :
    { id : String
    , label : String
    , info : Maybe String
    }
    -> Query.Single msg
    -> Query.Multiple msg
findAllRadioGroups config =
    Query.findAll
        [ Selector.tag "label"
        , Selector.attribute (Attributes.attribute "role" "radiogroup")
        , Selector.containing
            [ Selector.id config.id ]
        ]


findAllRadioButtons :
    { label : String
    , checked : Bool
    }
    -> Query.Single msg
    -> Query.Multiple msg
findAllRadioButtons config =
    let
        checked =
            if config.checked then
                "true"

            else
                "false"
    in
    Query.findAll
        [ Selector.attribute (Attributes.attribute "role" "radio")
        , Selector.attribute (Attributes.attribute "aria-checked" checked)
        , Selector.containing
            [ Selector.exactText config.label ]
        ]


findAllChoiceChipGroups :
    {}
    -> Query.Single msg
    -> Query.Multiple msg
findAllChoiceChipGroups _ =
    Query.findAll
        [ Selector.tag "ui-atom-choice-chips"
        ]


findAllComboboxSingleSelects :
    {}
    -> Query.Single msg
    -> Query.Multiple msg
findAllComboboxSingleSelects _ =
    Query.findAll
        [ Selector.tag "ui-molecule-combobox-singleselect"
        ]


findAllComboboxMultiSelects :
    {}
    -> Query.Single msg
    -> Query.Multiple msg
findAllComboboxMultiSelects _ =
    Query.findAll
        [ Selector.tag "ui-molecule-combobox-multiselect"
        ]


findAllSegmentedButtonsGroups :
    {}
    -> Query.Single msg
    -> Query.Multiple msg
findAllSegmentedButtonsGroups _ =
    Query.findAll
        [ Selector.tag "ui-atom-segmented-buttons-single-select"
        ]


findAllCheckboxes :
    { label : String
    , checked : Bool
    }
    -> Query.Single msg
    -> Query.Multiple msg
findAllCheckboxes config =
    let
        checked =
            if config.checked then
                "true"

            else
                "false"
    in
    Query.findAll
        [ Selector.all
            [ Selector.tag "label"
            , Selector.attribute (Attributes.attribute "role" "checkbox")
            , Selector.attribute (Attributes.attribute "aria-checked" checked)
            , Selector.containing
                [ Selector.exactText config.label ]
            ]
        ]
