module DocumentTree exposing
    ( DocumentTree(..)
    , fromList
    )

{-|

@docs DocumentTree
@docs fromList

-}


{-| TODO
-}
type DocumentTree
    = Folder String (List DocumentTree)
    | Document String


{-| TODO
-}
fromList : List String -> List DocumentTree
fromList urls =
    urls
        |> List.sort
        |> List.filterMap
            (\url ->
                case String.split "//" url of
                    [ protocol, rest ] ->
                        Just ( protocol, String.split "/" rest )

                    _ ->
                        Nothing
            )
        |> collect []


collect : List String -> List ( String, List String ) -> List DocumentTree
collect root urls =
    case urls of
        [] ->
            []

        ( _, [] ) :: rest ->
            collect root rest

        ( protocol, [ segment ] ) :: rest ->
            [ (segment :: root)
                |> List.reverse
                |> String.join "/"
                |> addProtocol protocol
                |> Document
            ]
                ++ collect root rest

        ( protocol, [ segment, "" ] ) :: rest ->
            [ ("" :: segment :: root)
                |> List.reverse
                |> String.join "/"
                |> addProtocol protocol
                |> Document
            ]
                ++ collect root rest

        ( protocol, segment :: segments ) :: rest ->
            let
                urlNextStartsWithSegment ( protocolNext, urlNext ) =
                    (List.head urlNext == Just segment)
                        && (protocolNext == protocol)

                ( withSegment, withoutSegment ) =
                    List.partition urlNextStartsWithSegment rest

                allWithSegment =
                    List.filterMap
                        (\( protocolNested, urlNested ) ->
                            List.tail urlNested
                                |> Maybe.map (Tuple.pair protocolNested)
                        )
                        (( protocol, segment :: segments ) :: withSegment)

                urlFolder =
                    ("" :: segment :: root)
                        |> List.reverse
                        |> String.join "/"
                        |> addProtocol protocol
            in
            case collect (segment :: root) allWithSegment of
                [] ->
                    collect root withoutSegment

                [ nestedSingle ] ->
                    nestedSingle :: collect root withoutSegment

                nestedMany ->
                    Folder urlFolder nestedMany :: collect root withoutSegment


addProtocol : String -> String -> String
addProtocol protocol path =
    protocol ++ "//" ++ path
