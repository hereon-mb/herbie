module Date.Format exposing (shortWithWeekday)

import Date exposing (Date)
import Time.Format exposing (Format(..))


shortWithWeekday : Format -> Date -> String
shortWithWeekday format =
    Date.format
        (case format of
            Iso8601 ->
                "E, d-M"

            EnUs ->
                "E, M/d"

            De ->
                "E, d.M"
        )
