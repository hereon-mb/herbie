{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


port module Ports exposing
    ( selectInputTextOf
    , copyToClipboard
    , send, receive
    )

{-|

@docs selectInputTextOf

@docs copyToClipboard


# Quadstore


# Concurrent tasks

@docs send, receive

-}

import Json.Encode as Encode exposing (Value)
import List.NonEmpty as NonEmpty exposing (NonEmpty)


port selectInputTextOf : String -> Cmd msg


port copyToClipboard_ : Value -> Cmd msg


copyToClipboard :
    { mimeType : String
    , parts : NonEmpty String
    }
    -> Cmd msg
copyToClipboard =
    let
        encode blob =
            Encode.object
                [ ( "mimeType", Encode.string blob.mimeType )
                , ( "parts", Encode.list Encode.string (NonEmpty.toList blob.parts) )
                ]
    in
    copyToClipboard_ << encode



-- CONCURRENT TASKS


port send : Value -> Cmd msg


port receive : (Value -> msg) -> Sub msg
