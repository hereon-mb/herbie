module Pretty.Shacl exposing
    ( propertyShape
    , Context, Prefix, prefixes
    )

{-|

@docs propertyShape
@docs Context, Prefix, prefixes

-}

import Dict
import List
import List.NonEmpty as NonEmpty
import Maybe.Extra as Maybe
import Pretty exposing (Doc)
import Rdf
import Rdf.Predicate as Predicate
import Rdf.PropertyPath as PropertyPath exposing (PropertyPath)
import Shacl


{-| TODO
-}
type alias Context =
    { prefixes : List Prefix
    , shapes : Shacl.Shapes
    }


{-| TODO
-}
type alias Prefix =
    { name : String
    , url : String
    }


{-| TODO
-}
prefixes : List Shacl.PropertyShape -> ( List { name : String, url : String }, Doc t )
prefixes propertyShapes =
    let
        ps =
            [ List.map .node propertyShapes
            , propertyShapes
                |> List.filterMap
                    (.shPath
                        >> Predicate.fromPropertyPath
                        >> Maybe.map
                            (NonEmpty.toList
                                >> List.map
                                    (Predicate.toIri
                                        >> Rdf.asBlankNodeOrIri
                                    )
                            )
                    )
                |> List.concat
            ]
                |> List.concat
                |> List.filterMap toPrefix
    in
    ( ps
    , Pretty.lines (List.map prefix ps)
    )


toPrefix : Rdf.BlankNodeOrIri -> Maybe Prefix
toPrefix n =
    case Rdf.toIri n of
        Nothing ->
            Nothing

        Just iri ->
            let
                url =
                    Rdf.toUrl iri
            in
            case String.split "#" url of
                [ path, _ ] ->
                    case List.reverse (String.split "/" path) of
                        "" :: segment :: _ ->
                            Just
                                { name = segment ++ ":"
                                , url = path ++ "#"
                                }

                        segment :: _ ->
                            Just
                                { name = segment ++ ":"
                                , url = path ++ "#"
                                }

                        _ ->
                            Nothing

                _ ->
                    Nothing


{-| TODO
-}
propertyShape : Context -> Shacl.PropertyShape -> Doc t
propertyShape context data =
    propertyShapeHelp False context data


propertyShapeHelp : Bool -> Context -> Shacl.PropertyShape -> Doc t
propertyShapeHelp nested context data =
    let
        tuples =
            Pretty.lines
                [ shPath context data.shPath
                , Maybe.unwrap Pretty.empty shName data.shName
                , Maybe.unwrap Pretty.empty shDescription data.shDescription
                , Pretty.lines (List.map (shTargetClass context) data.shTargetClass)
                , Pretty.lines (List.map (shClass context) data.shClass)
                , Pretty.lines (List.map (shNode context) data.shNode)
                , Maybe.unwrap Pretty.empty (shQualifiedValueShape context) data.shQualifiedValueShape
                , Maybe.unwrap Pretty.empty shQualifiedMinCount data.shQualifiedMinCount
                , Maybe.unwrap Pretty.empty shQualifiedMaxCount data.shQualifiedMaxCount
                , Pretty.lines (List.map shMinCount data.shMinCount)
                , Pretty.lines (List.map shMaxCount data.shMaxCount)
                ]
    in
    if nested then
        Pretty.nest 2
            tuples

    else
        Pretty.lines
            [ Pretty.nest 2
                (Pretty.lines
                    [ Pretty.words
                        [ node context data.node
                        , tuple
                            (Pretty.string "a")
                            (Pretty.string "sh:PropertyShape")
                        ]
                    , tuples
                    ]
                )
            , Pretty.string "."
            ]


{-| TODO
-}
nodeShape : Context -> Shacl.NodeShape -> Doc t
nodeShape context data =
    nodeShapeHelp False context data


nodeShapeHelp : Bool -> Context -> Shacl.NodeShape -> Doc t
nodeShapeHelp nested context data =
    let
        tuples =
            Pretty.lines
                [ Maybe.unwrap Pretty.empty shName data.shName
                , Pretty.lines (List.map (shTargetClass context) data.shTargetClass)
                , Pretty.lines (List.map (shClass context) data.shClass)
                , Pretty.lines (List.map (shProperty context) data.shProperty)
                ]
    in
    if nested then
        Pretty.indent 2 tuples

    else
        Pretty.lines
            [ Pretty.nest 2
                (Pretty.lines
                    [ Pretty.words
                        [ node context data.node
                        , tuple
                            (Pretty.string "a")
                            (Pretty.string "sh:PropertyShape")
                        ]
                    , tuples
                    ]
                )
            , Pretty.string "."
            ]


shName : Rdf.StringOrLangString -> Doc t
shName s =
    let
        info =
            Rdf.stringOrLangStringInfo s

        langString ( lang, string ) =
            tuple
                (Pretty.string "sh:name")
                (Pretty.string ("\"" ++ string ++ "\"@" ++ lang))
    in
    if info.string == Nothing && Dict.isEmpty info.langStrings then
        Pretty.empty

    else
        Pretty.lines
            [ case info.string of
                Nothing ->
                    Pretty.empty

                Just string ->
                    tuple
                        (Pretty.string "sh:name")
                        (Pretty.string ("\"" ++ string ++ "\""))
            , info.langStrings
                |> Dict.toList
                |> List.map langString
                |> Pretty.lines
            ]


shDescription : Rdf.StringOrLangString -> Doc t
shDescription s =
    let
        info =
            Rdf.stringOrLangStringInfo s

        langString ( lang, string ) =
            tuple
                (Pretty.string "sh:description")
                (Pretty.string ("\"" ++ string ++ "\"@" ++ lang))
    in
    if info.string == Nothing && Dict.isEmpty info.langStrings then
        Pretty.empty

    else
        Pretty.lines
            [ case info.string of
                Nothing ->
                    Pretty.empty

                Just string ->
                    tuple
                        (Pretty.string "sh:description")
                        (Pretty.string ("\"" ++ string ++ "\""))
            , info.langStrings
                |> Dict.toList
                |> List.map langString
                |> Pretty.lines
            ]


shTargetClass : Context -> Rdf.Iri -> Doc t
shTargetClass context t =
    tuple
        (Pretty.string "sh:targetClass")
        (node context t)


shClass : Context -> Rdf.Iri -> Doc t
shClass context c =
    tuple
        (Pretty.string "sh:class")
        (node context c)


shNode : Context -> Rdf.IsBlankNodeOrIri compatible -> Doc t
shNode context n =
    case Rdf.toIri n of
        Nothing ->
            case Shacl.getNodeShape context.shapes n of
                Nothing ->
                    tuple
                        (Pretty.string "sh:node")
                        (node context n)

                Just nodeShapeNested ->
                    Pretty.lines
                        [ Pretty.nest 2
                            (Pretty.lines
                                [ Pretty.string "sh:node ["
                                , nodeShape context nodeShapeNested
                                ]
                            )
                        , Pretty.string "] ;"
                        ]

        Just iri ->
            tuple
                (Pretty.string "sh:node")
                (node context iri)


shProperty : Context -> Rdf.IsBlankNodeOrIri compatible -> Doc t
shProperty context p =
    let
        simple =
            tuple
                (Pretty.string "sh:property")
                (node context p)
    in
    case Rdf.toIri p of
        Nothing ->
            case Shacl.getPropertyShape context.shapes p of
                Nothing ->
                    simple

                Just propertyShapeNested ->
                    Pretty.lines
                        [ Pretty.nest 2
                            (Pretty.lines
                                [ Pretty.string "sh:property ["
                                , propertyShape context propertyShapeNested
                                ]
                            )
                        , Pretty.string "] ;"
                        ]

        Just _ ->
            simple


shPath : Context -> PropertyPath -> Doc t
shPath context p =
    tuple
        (Pretty.string "sh:path")
        (propertyPath context p)


shQualifiedValueShape : Context -> Rdf.IsBlankNodeOrIri compatible -> Doc t
shQualifiedValueShape context q =
    let
        simple =
            tuple
                (Pretty.string "sh:qualifiedValueShape")
                (node context q)
    in
    case Rdf.toIri q of
        Nothing ->
            case Shacl.getNodeShape context.shapes q of
                Nothing ->
                    simple

                Just nodeShapeNested ->
                    Pretty.lines
                        [ Pretty.lines
                            [ Pretty.string "sh:shQualifiedValueShape ["
                            , nodeShapeHelp True context nodeShapeNested
                            ]
                        , Pretty.string "] ;"
                        ]

        Just _ ->
            simple


shQualifiedMinCount : Int -> Doc t
shQualifiedMinCount count =
    tuple
        (Pretty.string "sh:qualifiedMinCount")
        (Pretty.string (String.fromInt count))


shQualifiedMaxCount : Int -> Doc t
shQualifiedMaxCount count =
    tuple
        (Pretty.string "sh:qualifiedMaxCount")
        (Pretty.string (String.fromInt count))


shMinCount : Int -> Doc t
shMinCount count =
    tuple
        (Pretty.string "sh:minCount")
        (Pretty.string (String.fromInt count))


shMaxCount : Int -> Doc t
shMaxCount count =
    tuple
        (Pretty.string "sh:maxCount")
        (Pretty.string (String.fromInt count))


tuple : Doc t -> Doc t -> Doc t
tuple predicate object =
    Pretty.words
        [ predicate
        , object
        , Pretty.string ";"
        ]


propertyPath : Context -> PropertyPath -> Doc t
propertyPath context p =
    case p of
        PropertyPath.PredicatePath iri ->
            node context iri

        _ ->
            Pretty.string "TODO"


node : Context -> Rdf.IsBlankNodeOrIri compatible -> Doc t
node context n =
    let
        applyPrefix iri dataPrefix =
            let
                url =
                    Rdf.toUrl iri
            in
            if String.startsWith dataPrefix.url url then
                Just (String.replace dataPrefix.url dataPrefix.name url)

            else
                Nothing
    in
    case Rdf.toIri n of
        Nothing ->
            Pretty.string (Rdf.serializeNode n)

        Just iri ->
            context.prefixes
                |> List.map (applyPrefix iri)
                |> Maybe.orList
                |> Maybe.withDefault (Rdf.serializeNode iri)
                |> Pretty.string


prefix : Prefix -> Doc t
prefix data =
    Pretty.words
        [ Pretty.string "@prefix"
        , Pretty.string data.name
        , Pretty.string ("<" ++ data.url ++ "> .")
        ]
