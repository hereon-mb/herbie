api--validation--blank = Enter a value.
api--validation--blank-alloy-base-material = Enter the main alloying element.
api--validation--empty = This list may not be empty.
api--validation--end-value-must-be-greater-than-start-value = The end value must be greater than the start value.
api--validation--invalid = This is not a valid value.
api--validation--invalid-alloy-base-material = This material is not allowed as an alloy base.
api--validation--invalid-choice = This is not a valid choice.
api--validation--invalid-component-material = This material is not allowed as a component.
api--validation--invalid-decimal-format = Please provide a numeric value.
api--validation--max-digits = The value cannot contain more than { $maxDigits } digits.
api--validation--max-length = This list must contain at most { $maxLength } elements.
api--validation--max-value = The value must be lower than or equal to { $maxValue }.
api--validation--max-whole-digits = The value cannot contain more than { $wholeDigits } whole digits.
api--validation--min-length = This list must contain at least { $minLength } elements.
api--validation--min-value = The value must be greater than or equal to { $minValue }.
api--validation--no-date = Please provide a date.
api--validation--no-decimal = Please provide a numeric value.
api--validation--null = Enter a value.
api--validation--required = You must provide at least one entity.
api--validation--too-far-in-the-future = This date is in the future.
api--validation--unique = An entry with this value already exists.
api--validation--unique-for-date = An entry with this value already exists.
