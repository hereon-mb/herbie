module Rdf.DictIri.ApiData exposing
    ( DictIriApiData
    , empty, insert
    , get
    )

{-|


# Dictionaries

@docs DictIriApiData


# Build

@docs empty, insert, update, remove


# Query

@docs isEmpty, member, get, size


# Lists

@docs values


# Transform

@docs map, foldl, foldr, filter, partition


# Combine

@docs union, intersect, diff, merge

-}

import Api.Data exposing (ApiData(..))
import Rdf exposing (Iri)
import Rdf.DictIri as DictIri exposing (DictIri)


type DictIriApiData error a
    = DictIriApiData (DictIri (ApiData error a))


empty : DictIriApiData error a
empty =
    DictIriApiData DictIri.empty


insert : Iri -> ApiData error v -> DictIriApiData error v -> DictIriApiData error v
insert iri value (DictIriApiData dict) =
    DictIriApiData (DictIri.insert iri value dict)


get : Iri -> DictIriApiData error v -> ApiData error v
get iri (DictIriApiData dict) =
    dict
        |> DictIri.get iri
        |> Maybe.withDefault NotAsked
