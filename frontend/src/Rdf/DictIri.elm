module Rdf.DictIri exposing
    ( DictIri
    , empty, insert, update, remove
    , isEmpty, member, get
    , values, toList, fromList
    , map
    )

{-|


# Dictionaries

@docs DictIri


# Build

@docs empty, insert, update, remove


# Query

@docs isEmpty, member, get, size


# Lists

@docs values, toList, fromList


# Transform

@docs map, foldl, foldr, partition


# Combine

@docs union, intersect, diff, merge

-}

import Dict exposing (Dict)
import Rdf exposing (Iri)


type DictIri v
    = DictIri (Dict String v)


empty : DictIri v
empty =
    DictIri Dict.empty


insert : Iri -> v -> DictIri v -> DictIri v
insert iri value (DictIri dict) =
    dict
        |> Dict.insert (Rdf.toUrl iri) value
        |> DictIri


update : Iri -> (Maybe v -> Maybe v) -> DictIri v -> DictIri v
update iri alter (DictIri dict) =
    dict
        |> Dict.update (Rdf.toUrl iri) alter
        |> DictIri


remove : Iri -> DictIri v -> DictIri v
remove iri (DictIri dict) =
    dict
        |> Dict.remove (Rdf.toUrl iri)
        |> DictIri


isEmpty : DictIri v -> Bool
isEmpty (DictIri dict) =
    Dict.isEmpty dict


member : Iri -> DictIri v -> Bool
member iri (DictIri dict) =
    Dict.member (Rdf.toUrl iri) dict


get : Iri -> DictIri v -> Maybe v
get iri (DictIri dict) =
    Dict.get (Rdf.toUrl iri) dict


toList : DictIri v -> List ( Iri, v )
toList (DictIri dict) =
    dict
        |> Dict.toList
        |> List.map (Tuple.mapFirst Rdf.iri)


fromList : List ( Iri, v ) -> DictIri v
fromList =
    List.map (Tuple.mapFirst Rdf.toUrl)
        >> Dict.fromList
        >> DictIri


values : DictIri v -> List v
values (DictIri dict) =
    Dict.values dict


map : (Iri -> a -> b) -> DictIri a -> DictIri b
map func (DictIri dict) =
    dict
        |> Dict.map (\url -> func (Rdf.iri url))
        |> DictIri
