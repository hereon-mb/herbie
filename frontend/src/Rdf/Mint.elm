module Rdf.Mint exposing
    ( Minter, run, runWith
    , Config
    , static, andThen, map, sequence
    , blankNode, iri
    )

{-|

@docs Minter, run, runWith
@docs Config, configPreview
@docs static, andThen, map, sequence
@docs blankNode, iri

-}

import List.Extra as List
import Rdf exposing (BlankNode, BlankNodeOrIri, Iri)
import Rdf.Extra as Rdf
import String.Extra as String
import Task exposing (Task)
import UUID exposing (Seeds)


type Minter err a
    = Minter (Config err -> Seeds -> Task err ( a, Seeds ))


run : Minter err a -> Seeds -> Task err ( a, Seeds )
run (Minter step) seeds =
    step configPreview seeds


runWith : Config err -> Minter err a -> Seeds -> Task err ( a, Seeds )
runWith config (Minter step) seeds =
    step config seeds



-- CONFIG


type alias Config err =
    { mintIri : Seeds -> BlankNodeOrIri -> Iri -> Task err ( Iri, Seeds )
    }


configPreview : Config err
configPreview =
    { mintIri =
        \seeds nodeFocus class ->
            let
                pathSegments =
                    nodeFocus
                        |> Rdf.toIri
                        |> Maybe.map
                            (Rdf.toUrl
                                >> String.split "/"
                                >> List.dropWhile ((/=) "graph")
                                >> List.takeWhile ((/=) "")
                            )
                        |> Maybe.withDefault []

                nameClass =
                    class
                        |> Rdf.defaultLabel
                        |> Maybe.map String.dasherize
                        |> Maybe.withDefault "resource"

                ( uuid, seedsUpdated ) =
                    UUID.step seeds
            in
            Task.succeed
                ( uuid
                    |> UUID.toString
                    |> (\string ->
                            "http://example.org"
                                ++ String.join "/" pathSegments
                                ++ "/"
                                ++ nameClass
                                ++ "/"
                                ++ string
                       )
                    |> Rdf.iri
                , seedsUpdated
                )
    }



-- COMBINATORS


static : a -> Minter err a
static a =
    Minter (\_ seeds -> Task.succeed ( a, seeds ))


andThen : (a -> Minter err b) -> Minter err a -> Minter err b
andThen toB (Minter stepA) =
    Minter
        (\config seeds ->
            Task.andThen
                (\( a, seedsUpdated ) ->
                    case toB a of
                        Minter stepB ->
                            stepB config seedsUpdated
                )
                (stepA config seeds)
        )


map : (a -> b) -> Minter err a -> Minter err b
map func creatorA =
    creatorA
        |> andThen (\a -> static (func a))


map2 : (a -> b -> result) -> Minter err a -> Minter err b -> Minter err result
map2 func creatorA creatorB =
    creatorA
        |> andThen
            (\a ->
                creatorB
                    |> andThen (\b -> static (func a b))
            )


sequence : List (Minter err a) -> Minter err (List a)
sequence creators =
    List.foldr (map2 (::)) (static []) creators


blankNode : Minter err BlankNode
blankNode =
    Minter
        (\_ seeds ->
            let
                ( uuid, seedsUpdated ) =
                    UUID.step seeds
            in
            Task.succeed ( Rdf.blankNode (UUID.toString uuid), seedsUpdated )
        )


iri : BlankNodeOrIri -> Iri -> Minter err Iri
iri nodeFocus class =
    Minter
        (\config seeds ->
            config.mintIri seeds nodeFocus class
        )
