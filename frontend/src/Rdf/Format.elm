module Rdf.Format exposing
    ( Format(..)
    , fromString, toString
    , fromMime, toMime
    )

{-|

@docs Format
@docs fromString, toString
@docs fromMime, toMime

-}


type Format
    = Turtle
    | NTriples
    | JsonLd


toString : Format -> String
toString format =
    case format of
        Turtle ->
            "turtle"

        NTriples ->
            "n-triples"

        JsonLd ->
            "json-ld"


fromString : String -> Maybe Format
fromString raw =
    case raw of
        "turtle" ->
            Just Turtle

        "n-triples" ->
            Just NTriples

        "json-ld" ->
            Just JsonLd

        _ ->
            Nothing


fromMime : String -> Maybe Format
fromMime mime =
    case mime of
        "text/turtle" ->
            Just Turtle

        "application/n-triples" ->
            Just NTriples

        "application/ld+json" ->
            Just JsonLd

        _ ->
            Nothing


toMime : Format -> String
toMime format =
    case format of
        Turtle ->
            "text/turtle"

        NTriples ->
            "application/n-triples"

        JsonLd ->
            "application/ld+json"
