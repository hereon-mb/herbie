module Rdf.Namespaces.OWL exposing
    ( ontology, class, objectProperty, datatypeProperty, versionInfo
    , importsFor
    , instancesOfClass, instancesOfObjectProperty, instancesOfDatatypeProperty
    )

{-|

@docs ontology, class, objectProperty, datatypeProperty, versionInfo

@docs importsFor

@docs instancesOfClass, instancesOfObjectProperty, instancesOfDatatypeProperty

-}

import Rdf exposing (Iri)
import Rdf.Graph exposing (Graph)
import Rdf.Namespaces exposing (a, owl)
import Rdf.Query as Query


ontology : Iri
ontology =
    owl "Ontology"


class : Iri
class =
    owl "Class"


objectProperty : Iri
objectProperty =
    owl "ObjectProperty"


datatypeProperty : Iri
datatypeProperty =
    owl "DatatypeProperty"


versionInfo : Iri
versionInfo =
    owl "versionInfo"


imports : Iri
imports =
    owl "imports"


importsFor : Iri -> Graph -> List Iri
importsFor iri graph =
    Query.emptyQuery
        |> Query.withSubject iri
        |> Query.withPredicate imports
        |> Query.getIriObjects graph


instancesOfClass : Graph -> List Iri
instancesOfClass graph =
    Query.emptyQuery
        |> Query.withPredicate a
        |> Query.withObject class
        |> Query.getIriSubjects graph


instancesOfObjectProperty : Graph -> List Iri
instancesOfObjectProperty graph =
    Query.emptyQuery
        |> Query.withPredicate a
        |> Query.withObject objectProperty
        |> Query.getIriSubjects graph


instancesOfDatatypeProperty : Graph -> List Iri
instancesOfDatatypeProperty graph =
    Query.emptyQuery
        |> Query.withPredicate a
        |> Query.withObject datatypeProperty
        |> Query.getIriSubjects graph
