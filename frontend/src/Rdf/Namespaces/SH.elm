module Rdf.Namespaces.SH exposing
    ( nodeShape, propertyShape, propertyGroup
    , alternativePath, blankNode, blankNodeOrIri, blankNodeOrLiteral, class, datatype, defaultValue, description, group, hasValue, in_, inverse, inversePath, iri, iriOrLiteral, literal, maxCount, maxExclusive, maxInclusive, minCount, minExclusive, minInclusive, name, node, nodeKind, oneOrMorePath, or, order, path, property, qualifiedMaxCount, qualifiedMinCount, qualifiedValueShape, targetClass, this, values, xone, zeroOrMorePath, zeroOrOnePath
    )

{-|

@docs nodeShape, propertyShape, propertyGroup

-}

import Rdf exposing (Iri)
import Rdf.Namespaces exposing (sh)
import Rdf.PropertyPath exposing (PropertyPath(..))


nodeShape : Iri
nodeShape =
    sh "NodeShape"


propertyShape : Iri
propertyShape =
    sh "PropertyShape"


propertyGroup : Iri
propertyGroup =
    sh "PropertyGroup"


blankNode : Iri
blankNode =
    sh "BlankNode"


iri : Iri
iri =
    sh "IRI"


literal : Iri
literal =
    sh "Literal"


blankNodeOrIri : Iri
blankNodeOrIri =
    sh "BlankNodeOrIRI"


blankNodeOrLiteral : Iri
blankNodeOrLiteral =
    sh "BlankNodeOrLiteral"


iriOrLiteral : Iri
iriOrLiteral =
    sh "IRIOrLiteral"


property : Iri
property =
    sh "property"


targetClass : Iri
targetClass =
    sh "targetClass"


defaultValue : Iri
defaultValue =
    sh "defaultValue"


hasValue : Iri
hasValue =
    sh "hasValue"


in_ : Iri
in_ =
    sh "in"


values : Iri
values =
    sh "values"


this : Iri
this =
    sh "this"


or : Iri
or =
    sh "or"


xone : Iri
xone =
    sh "xone"


group : Iri
group =
    sh "group"


order : Iri
order =
    sh "order"


name : Iri
name =
    sh "name"


description : Iri
description =
    sh "description"


path : Iri
path =
    sh "path"


class : Iri
class =
    sh "class"


node : Iri
node =
    sh "node"


datatype : Iri
datatype =
    sh "datatype"


nodeKind : Iri
nodeKind =
    sh "nodeKind"


minExclusive : Iri
minExclusive =
    sh "minExclusive"


minInclusive : Iri
minInclusive =
    sh "minInclusive"


maxExclusive : Iri
maxExclusive =
    sh "maxExclusive"


maxInclusive : Iri
maxInclusive =
    sh "maxInclusive"


minCount : Iri
minCount =
    sh "minCount"


maxCount : Iri
maxCount =
    sh "maxCount"


qualifiedValueShape : Iri
qualifiedValueShape =
    sh "qualifiedValueShape"


qualifiedMinCount : Iri
qualifiedMinCount =
    sh "qualifiedMinCount"


qualifiedMaxCount : Iri
qualifiedMaxCount =
    sh "qualifiedMaxCount"


alternativePath : Iri
alternativePath =
    sh "alternativePath"


inversePath : Iri
inversePath =
    sh "inversePath"


zeroOrMorePath : Iri
zeroOrMorePath =
    sh "zeroOrMorePath"


oneOrMorePath : Iri
oneOrMorePath =
    sh "oneOrMorePath"


zeroOrOnePath : Iri
zeroOrOnePath =
    sh "zeroOrOnePath"


inverse : { targetClass : PropertyPath }
inverse =
    { targetClass = InversePath (PredicatePath targetClass)
    }
