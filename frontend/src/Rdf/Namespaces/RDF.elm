module Rdf.Namespaces.RDF exposing
    ( property
    , instancesOfProperty
    )

{-|

@docs property

@docs instancesOfProperty

-}

import Rdf exposing (Iri)
import Rdf.Graph exposing (Graph)
import Rdf.Namespaces exposing (a, rdf)
import Rdf.Query as Query


property : Iri
property =
    rdf "Property"


instancesOfProperty : Graph -> List Iri
instancesOfProperty graph =
    Query.emptyQuery
        |> Query.withPredicate a
        |> Query.withObject property
        |> Query.getIriSubjects graph
