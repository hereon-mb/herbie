module Rdf.Namespaces.DCTERMS exposing (conformsTo, created, modified, source)

{-|

@docs conformsTo, created, modified, source

-}

import Rdf exposing (Iri)
import Rdf.Namespaces exposing (dcterms)


conformsTo : Iri
conformsTo =
    dcterms "conformsTo"


created : Iri
created =
    dcterms "created"


modified : Iri
modified =
    dcterms "modified"


source : Iri
source =
    dcterms "source"
