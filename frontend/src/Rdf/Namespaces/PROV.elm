module Rdf.Namespaces.PROV exposing (wasAttributedTo, specializationOf, endedAtTime, startedAtTime, wasEndedBy, generatedAtTime, generated)

{-|

@docs wasAttributedTo, specializationOf, endedAtTime, startedAtTime, wasEndedBy, generatedAtTime, generated

-}

import Rdf exposing (Iri)
import Rdf.Namespaces exposing (prov)


generatedAtTime : Iri
generatedAtTime =
    prov "generatedAtTime"


wasAttributedTo : Iri
wasAttributedTo =
    prov "wasAttributedTo"


specializationOf : Iri
specializationOf =
    prov "specializationOf"


wasEndedBy : Iri
wasEndedBy =
    prov "wasEndedBy"


startedAtTime : Iri
startedAtTime =
    prov "startedAtTime"


endedAtTime : Iri
endedAtTime =
    prov "endedAtTime"


generated : Iri
generated =
    prov "generated"
