module Rdf.Namespaces.RDFS exposing
    ( label, comment, resource, class, subClassOf, isDefinedBy, seeAlso
    , labelFor, commentFor
    , instancesOfClass
    )

{-|

@docs label, comment, resource, class, subClassOf, isDefinedBy, seeAlso
@docs labelFor, commentFor
@docs instancesOfClass

-}

import Rdf exposing (Iri, IsBlankNodeOrIri, StringOrLangString)
import Rdf.Graph exposing (Graph)
import Rdf.Namespaces exposing (a, rdfs)
import Rdf.Query as Query


label : Iri
label =
    rdfs "label"


comment : Iri
comment =
    rdfs "comment"


resource : Iri
resource =
    rdfs "Resource"


class : Iri
class =
    rdfs "Class"


subClassOf : Iri
subClassOf =
    rdfs "subClassOf"


isDefinedBy : Iri
isDefinedBy =
    rdfs "isDefinedBy"


seeAlso : Iri
seeAlso =
    rdfs "seeAlso"


labelFor : IsBlankNodeOrIri compatible -> Graph -> Maybe StringOrLangString
labelFor blankNodeOrIri graph =
    Query.emptyQuery
        |> Query.withSubject blankNodeOrIri
        |> Query.withPredicate label
        |> Query.getStringOrLangString graph


commentFor : IsBlankNodeOrIri compatible -> Graph -> Maybe StringOrLangString
commentFor blankNodeOrIri graph =
    Query.emptyQuery
        |> Query.withSubject blankNodeOrIri
        |> Query.withPredicate comment
        |> Query.getStringOrLangString graph


instancesOfClass : Graph -> List Iri
instancesOfClass graph =
    Query.emptyQuery
        |> Query.withPredicate a
        |> Query.withObject class
        |> Query.getIriSubjects graph
