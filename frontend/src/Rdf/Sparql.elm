module Rdf.Sparql exposing
    ( Results
    , resultsDecoder
    )

import Dict exposing (Dict)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Decode
import Rdf exposing (BlankNodeOrIriOrAnyLiteral)
import Rdf.Namespaces exposing (xsd)


type alias Results =
    List (Dict String BlankNodeOrIriOrAnyLiteral)


resultsDecoder : Decoder Results
resultsDecoder =
    Decode.list (Decode.dict nodeDecoder)


nodeDecoder : Decoder BlankNodeOrIriOrAnyLiteral
nodeDecoder =
    Decode.oneOf
        [ literalDecoder
        , namedNodeDecoder
        , blankNodeDecoder
        ]


literalDecoder : Decoder BlankNodeOrIriOrAnyLiteral
literalDecoder =
    let
        toNode value maybeDatatype maybeLanguage =
            case maybeLanguage of
                Nothing ->
                    let
                        datatype =
                            maybeDatatype
                                |> Maybe.map Rdf.iri
                                |> Maybe.withDefault (xsd "string")
                    in
                    value
                        |> Rdf.literal datatype
                        |> Rdf.asBlankNodeOrIriOrAnyLiteral

                Just language ->
                    value
                        |> Rdf.langString language
                        |> Rdf.asBlankNodeOrIriOrAnyLiteral
    in
    Decode.oneOf
        [ Decode.field "termType" Decode.string
            |> Decode.andThen
                (\termType ->
                    if termType == "Literal" then
                        Decode.succeed toNode
                            |> Decode.required "value" Decode.string
                            |> Decode.requiredAt [ "datatype", "value" ] (Decode.map Just Decode.string)
                            |> Decode.optional "language" (Decode.map Just Decode.string) Nothing

                    else
                        Decode.fail "not a Literal"
                )
        , Decode.field "type" Decode.string
            |> Decode.andThen
                (\type_ ->
                    if type_ == "literal" || type_ == "typed-literal" then
                        Decode.succeed toNode
                            |> Decode.required "value" Decode.string
                            |> Decode.optional "datatype" (Decode.map Just Decode.string) Nothing
                            |> Decode.optional "xml:lang" (Decode.map Just Decode.string) Nothing

                    else
                        Decode.fail "not a Literal"
                )
        ]


namedNodeDecoder : Decoder BlankNodeOrIriOrAnyLiteral
namedNodeDecoder =
    let
        toNode value =
            value
                |> Rdf.iri
                |> Rdf.asBlankNodeOrIriOrAnyLiteral
    in
    Decode.oneOf
        [ Decode.field "termType" Decode.string
            |> Decode.andThen
                (\termType ->
                    if termType == "NamedNode" then
                        Decode.succeed toNode
                            |> Decode.required "value" Decode.string

                    else
                        Decode.fail "not an IRI"
                )
        , Decode.field "type" Decode.string
            |> Decode.andThen
                (\type_ ->
                    if type_ == "uri" then
                        Decode.succeed toNode
                            |> Decode.required "value" Decode.string

                    else
                        Decode.fail "not an IRI"
                )
        ]


blankNodeDecoder : Decoder BlankNodeOrIriOrAnyLiteral
blankNodeDecoder =
    let
        toNode value =
            case String.split ":" value of
                [ _, blankNode ] ->
                    blankNode
                        |> Rdf.blankNode
                        |> Rdf.asBlankNodeOrIriOrAnyLiteral
                        |> Decode.succeed

                _ ->
                    Decode.fail "not a Blank Node"
    in
    Decode.field "type" Decode.string
        |> Decode.andThen
            (\type_ ->
                if type_ == "bnode" then
                    Decode.field "value" Decode.string
                        |> Decode.andThen toNode

                else
                    Decode.fail "not a Blank Node"
            )
