module Rdf.Hash exposing
    ( initial
    , iris, maybeIri
    , propertyPath
    , combine, combineMany
    )

{-|

@docs initial
@docs iris, maybeIri
@docs propertyPath
@docs combine, combineMany

-}

import FNV1a
import Rdf exposing (Iri)
import Rdf.PropertyPath exposing (PropertyPath(..))


initial : Int
initial =
    0


iri : String -> Iri -> Int -> Int
iri kind theIri =
    FNV1a.hashWithSeed kind
        << FNV1a.hashWithSeed (Rdf.serializeNode theIri)


maybeIri : String -> Maybe Iri -> Int -> Int
maybeIri kind theMaybeIri =
    case theMaybeIri of
        Nothing ->
            identity

        Just theIri ->
            FNV1a.hashWithSeed kind
                << FNV1a.hashWithSeed (Rdf.serializeNode theIri)


iris : String -> List Iri -> Int -> Int
iris kind values hashPrevious =
    values
        |> List.sortBy Rdf.toUrl
        |> List.foldl (iri kind) hashPrevious


propertyPath : String -> PropertyPath -> Int -> Int
propertyPath kind thePropertyPath hashPrevious =
    FNV1a.hashWithSeed kind
        (case thePropertyPath of
            PredicatePath theIri ->
                iri "predicate-path" theIri hashPrevious

            SequencePath first rest ->
                List.foldl
                    (propertyPath "sequence-path-first")
                    (propertyPath "sequence-path-rest" first hashPrevious)
                    rest

            AlternativePath first rest ->
                List.foldl
                    (propertyPath "alternative-path-first")
                    (propertyPath "alternative-path-rest" first hashPrevious)
                    rest

            InversePath nested ->
                propertyPath "inverse-path" nested hashPrevious

            ZeroOrMorePath nested ->
                propertyPath "zero-or-more-path" nested hashPrevious

            OneOrMorePath nested ->
                propertyPath "one-or-more-path" nested hashPrevious

            ZeroOrOnePath nested ->
                propertyPath "zero-or-one-path" nested hashPrevious
        )


combine : { r | hash : Int } -> Int -> Int
combine previous previousHash =
    if previous.hash > previousHash then
        FNV1a.hashWithSeed (String.fromInt previous.hash) previousHash

    else
        FNV1a.hashWithSeed (String.fromInt previousHash) previous.hash


combineMany : List { r | hash : Int } -> Int -> Int
combineMany previouses hashPrevious =
    previouses
        |> List.sortBy .hash
        |> List.foldl combine hashPrevious
