module Rdf.Decode.Extra exposing
    ( optionalStringOrLangString
    , manySafe
    , exact
    , oneAtAnyPredicate
    , oneAt, zeroOrOneAt, zeroOrOne, zeroOrOneStrictAt
    , zeroOrManyAt, zeroOrMany
    , oneOrMore
    , isInstanceOf
    , instanceOf, instanceOfAll
    , instancesOfStrict
    , fromInstancesOf
    )

{-|

@docs optionalStringOrLangString
@docs manySafe
@docs exact
@docs oneAtAnyPredicate
@docs oneAt, zeroOrOneAt, zeroOrOne, zeroOrOneStrictAt
@docs zeroOrManyAt, zeroOrMany
@docs oneOrMore
@docs isInstanceOf
@docs instanceOf, instanceOfAll
@docs instancesOfStrict
@docs fromInstancesOf

-}

import List.NonEmpty as NonEmpty exposing (NonEmpty)
import Rdf exposing (Iri, StringOrLangString)
import Rdf.Decode as Decode exposing (Decoder)
import Rdf.Namespaces exposing (a)
import Rdf.PropertyPath exposing (PropertyPath(..))
import Rdf.SetIri as SetIri exposing (SetIri)


{-| This works like `Decode.many`, but never fails. It will remove instances
which do not decode successfully from the list and may succeed with an empty
list.
-}
manySafe : Decoder a -> Decoder (List a)
manySafe decoderA =
    Decode.map (List.filterMap identity)
        (Decode.many
            (Decode.oneOf
                [ Decode.map Just decoderA
                , Decode.succeed Nothing
                ]
            )
        )


{-| Run the decoder at all predicates of all subjects and succeed if it
succeeds exactly once.
-}
oneAtAnyPredicate : Decoder a -> Decoder a
oneAtAnyPredicate decoderA =
    Decode.fromSubject
        (manySafe
            (Decode.anyPredicate
                (manySafe decoderA)
            )
            |> Decode.andThen
                (\results ->
                    case List.concat results of
                        [] ->
                            Decode.fail "not enough results"

                        [ result ] ->
                            Decode.succeed result

                        _ ->
                            Decode.fail "too many result"
                )
        )


optionalStringOrLangString :
    Rdf.IsIri compatible
    -> Decoder (Maybe StringOrLangString -> a)
    -> Decoder a
optionalStringOrLangString iri =
    Decode.optional iri (Decode.map Just Decode.stringOrLangString) Nothing


exact : Rdf.IsBlankNodeOrIriOrAnyLiteral compatible -> Decoder ()
exact node =
    Decode.andThen
        (\actualNode ->
            if Rdf.asBlankNodeOrIriOrAnyLiteral node == actualNode then
                Decode.succeed ()

            else
                Decode.fail ("expected " ++ Rdf.serializeNode node ++ " but found " ++ Rdf.serializeNode actualNode)
        )
        Decode.object


{-| Try to decode the object at the given path with the following result
handling:

  - no object => `Decode.fail "..."`

  - several objects
      - none decodes successfull => `Decode.fail "..."`
      - exactly one decodes successfull => `Decode.succeed (...)`
      - more then one decodes successfull => `Decode.fail "..."`

-}
oneAt : PropertyPath -> Decoder a -> Decoder (Maybe a)
oneAt path decoderA =
    Decode.oneOf
        [ Decode.property path
            (manySafe decoderA
                |> Decode.andThen
                    (\results ->
                        case results of
                            [] ->
                                Decode.fail "Found no decodable values"

                            [ value ] ->
                                Decode.succeed (Just value)

                            _ ->
                                Decode.fail "Found more then one decodable values"
                    )
            )
        ]


{-| Try to decode the object at the given path with the following result
handling:

  - no object => `Decode.succeed Nothing`

  - several objects
      - none decodes successfull => `Decode.succeed Nothing`
      - exactly one decodes successfull => `Decode.succeed (Just ...)`
      - more then one decodes successfull => `Decode.fail "..."`

-}
zeroOrOneAt : PropertyPath -> Decoder a -> Decoder (Maybe a)
zeroOrOneAt path decoderA =
    Decode.oneOf
        [ Decode.map (\_ -> Nothing) (Decode.noProperty path)
        , Decode.property path
            (manySafe decoderA
                |> Decode.andThen
                    (\results ->
                        case results of
                            [] ->
                                Decode.succeed Nothing

                            [ value ] ->
                                Decode.succeed (Just value)

                            _ ->
                                Decode.fail "Found more then one decodable values"
                    )
            )
        ]


zeroOrOne : Iri -> Decoder a -> Decoder (Maybe a)
zeroOrOne iri decoderA =
    zeroOrOneAt (PredicatePath iri) decoderA


{-| Try to decode the object at the given path with the following result
handling:

  - no object => `Decode.succeed Nothing`

  - several objects
      - none decodes successfull => `Decode.fail "..."`
      - exactly one decodes successfull => `Decode.succeed (Just ...)`
      - more then one decodes successfull => `Decode.fail "..."`

This is more strict then `zeroOrOneAt` in the sense that **if** there is
a value at the property shape, it must decode successfully.

-}
zeroOrOneStrictAt : PropertyPath -> Decoder a -> Decoder (Maybe a)
zeroOrOneStrictAt path decoderA =
    Decode.oneOf
        [ Decode.map (\_ -> Nothing) (Decode.noProperty path)
        , Decode.property path
            (manySafe decoderA
                |> Decode.andThen
                    (\results ->
                        case results of
                            [] ->
                                Decode.fail "Found no decodable value"

                            [ value ] ->
                                Decode.succeed (Just value)

                            _ ->
                                Decode.fail "Found more then one decodable values"
                    )
            )
        ]


{-| Try to decoder several objects at the given path. This decoder will always
succeed with a list of all objects which could be successfully decoded.
-}
zeroOrManyAt : PropertyPath -> Decoder a -> Decoder (List a)
zeroOrManyAt path decoderA =
    Decode.oneOf
        [ Decode.map (\_ -> []) (Decode.noProperty path)
        , Decode.property path (manySafe decoderA)
        ]


zeroOrMany : Iri -> Decoder a -> Decoder (List a)
zeroOrMany iri decoderA =
    zeroOrManyAt (PredicatePath iri) decoderA


oneOrMore : Decoder a -> Decoder (NonEmpty a)
oneOrMore decoderA =
    Decode.andThen toNonEmpty (Decode.many decoderA)


toNonEmpty : List a -> Decoder (NonEmpty a)
toNonEmpty listA =
    case NonEmpty.fromList listA of
        Nothing ->
            Decode.fail "List is empty"

        Just nonEmpty ->
            Decode.succeed nonEmpty


{-| Run a decoder on an instance of the given class. This decoder will fail
if there is more then one instance for this class.
-}
instanceOf : Iri -> Decoder a -> Decoder a
instanceOf iri =
    instanceOfAll [ iri ]


{-| Run a decoder on an instance of the given classes. This decoder will fail
if there is more then one instance for these classes.
-}
instanceOfAll : List Iri -> Decoder a -> Decoder a
instanceOfAll iris decoderA =
    manySafe
        (isInstanceOf (SetIri.fromList iris)
            |> Decode.andThen (\_ -> decoderA)
        )
        |> Decode.andThen
            (\listA ->
                case listA of
                    [] ->
                        Decode.fail
                            ("Could not find any instance of "
                                ++ String.join ", " (List.map Rdf.serializeNode iris)
                            )

                    [ a ] ->
                        Decode.succeed a

                    _ ->
                        Decode.fail
                            ("Found more then one instance of "
                                ++ String.join ", " (List.map Rdf.serializeNode iris)
                            )
            )


{-| Run a decoder on all instances of the given class. This decoder will
fail if it fails for one instance.
-}
instancesOfStrict : Iri -> Decoder a -> Decoder (List a)
instancesOfStrict iri decoderA =
    -- FIXME This is not yet strict!
    manySafe
        (isInstanceOf (SetIri.singleton iri)
            |> Decode.andThen (\_ -> decoderA)
        )


{-| Start a decoder at all instances of the given class. This decoder will
always succeed. If the decoder fails for one instance, this result will be
dropped from the result, so you might end up with an empty list.
-}
fromInstancesOf : Iri -> Decoder a -> Decoder (List a)
fromInstancesOf iri decoderA =
    Decode.oneOf
        [ Decode.map (List.filterMap identity)
            (fromInstancesOfStrict iri
                (Decode.oneOf
                    [ Decode.map Just decoderA
                    , Decode.succeed Nothing
                    ]
                )
            )
        , Decode.succeed []
        ]


{-| Start a decoder at all instances of the given class. This will fail, if one
decoder fails.
-}
fromInstancesOfStrict : Iri -> Decoder a -> Decoder (List a)
fromInstancesOfStrict iri decoderA =
    Decode.from iri
        (Decode.property (InversePath (PredicatePath a))
            (Decode.many decoderA)
        )


{-| Decoder which fails if the current focus node is not an instance of the
given classes.
-}
isInstanceOf : SetIri -> Decoder ()
isInstanceOf classesRequired =
    Decode.andThen
        (\classesActual ->
            if
                SetIri.isEmpty
                    (SetIri.diff classesRequired
                        (SetIri.fromList classesActual)
                    )
            then
                Decode.succeed ()

            else
                Decode.failWith
                    (\thises ->
                        "The nodes\n\n"
                            ++ indent
                                (String.join "\n"
                                    (List.map
                                        (\this -> Rdf.serializeNode this)
                                        thises
                                    )
                                )
                            ++ "\n\nare not instances of the classes\n\n"
                            ++ indent
                                (String.join "\n"
                                    (List.map (\class -> Rdf.serializeNode class)
                                        (SetIri.toList classesRequired)
                                    )
                                )
                            ++ "\n\nbut instead of\n\n"
                            ++ indent
                                (String.join ", "
                                    (List.map (\class -> Rdf.serializeNode class)
                                        classesActual
                                    )
                                )
                    )
        )
        (Decode.oneOf
            [ Decode.predicate a (Decode.many Decode.iri)
            , Decode.succeed []
            ]
        )


indent : String -> String
indent lines =
    lines
        |> String.lines
        |> List.map (\line -> "    " ++ line)
        |> String.join "\n"
