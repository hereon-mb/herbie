module Rdf.SetIri exposing
    ( SetIri
    , empty, singleton, insert, remove
    , isEmpty, member, size
    , union, intersect, diff
    , toList, fromList
    )

{-|


# Sets

@docs SetIri


# Build

@docs empty, singleton, insert, remove


# Query

@docs isEmpty, member, size


# Combine

@docs union, intersect, diff


# Lists

@docs toList, fromList


# Transform

@docs map, foldl, foldr, filter, partition

-}

import Rdf exposing (Iri)
import Set exposing (Set)


type SetIri
    = SetIri (Set String)


empty : SetIri
empty =
    SetIri Set.empty


singleton : Iri -> SetIri
singleton =
    Rdf.toUrl >> Set.singleton >> SetIri


insert : Iri -> SetIri -> SetIri
insert iri (SetIri set) =
    set
        |> Set.insert (Rdf.toUrl iri)
        |> SetIri


remove : Iri -> SetIri -> SetIri
remove iri (SetIri set) =
    set
        |> Set.remove (Rdf.toUrl iri)
        |> SetIri


toList : SetIri -> List Iri
toList (SetIri set) =
    set
        |> Set.toList
        |> List.map Rdf.iri


fromList : List Iri -> SetIri
fromList =
    List.map Rdf.toUrl >> Set.fromList >> SetIri


isEmpty : SetIri -> Bool
isEmpty (SetIri set) =
    Set.isEmpty set


member : Iri -> SetIri -> Bool
member iri (SetIri set) =
    Set.member (Rdf.toUrl iri) set


size : SetIri -> Int
size (SetIri set) =
    Set.size set


union : SetIri -> SetIri -> SetIri
union (SetIri setA) (SetIri setB) =
    SetIri (Set.union setA setB)


intersect : SetIri -> SetIri -> SetIri
intersect (SetIri setA) (SetIri setB) =
    SetIri (Set.intersect setA setB)


diff : SetIri -> SetIri -> SetIri
diff (SetIri setA) (SetIri setB) =
    SetIri (Set.diff setA setB)
