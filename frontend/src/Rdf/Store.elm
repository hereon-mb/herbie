{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Rdf.Store exposing
    ( Error(..)
    , parseTrig
    , storeGraph
    , getGraph
    , getGraphTurtle, getGraphNTriples, getGraphJsonLd
    , deleteGraph
    , query
    , clear
    , parseTurtle
    )

{-|

@docs Error
@docs parseTrig
@docs storeGraph
@docs getGraph
@docs getGraphTurtle, getGraphNTriples, getGraphJsonLd
@docs deleteGraph
@docs query
@docs clear

@docs Error

-}

import ConcurrentTask exposing (ConcurrentTask)
import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode
import Rdf exposing (Iri)
import Rdf.DictIri as DictIri exposing (DictIri)
import Rdf.Graph as Rdf exposing (Graph)
import Rdf.Sparql as Sparql


type Error
    = Error String


clear : ConcurrentTask Error ()
clear =
    ConcurrentTask.define
        { function = "quadstore:clear"
        , expect = ConcurrentTask.expectWhatever
        , errors = ConcurrentTask.expectThrows Error
        , args = Encode.null
        }


parseTurtle : Iri -> String -> ConcurrentTask Error Graph
parseTurtle iri raw =
    ConcurrentTask.define
        { function = "quadstore:parseTurtle"
        , expect = ConcurrentTask.expectJson Rdf.decoder
        , errors = ConcurrentTask.expectThrows Error
        , args =
            [ ( "iri"
              , iri
                    |> Rdf.toUrl
                    |> Encode.string
              )
            , ( "raw", Encode.string raw )
            ]
                |> Encode.object
        }


parseTrig : Iri -> String -> ConcurrentTask Error (DictIri Graph)
parseTrig iri raw =
    ConcurrentTask.define
        { function = "quadstore:parseTrig"
        , expect = ConcurrentTask.expectJson graphsDecoder
        , errors = ConcurrentTask.expectThrows Error
        , args =
            [ ( "iri"
              , iri
                    |> Rdf.toUrl
                    |> Encode.string
              )
            , ( "raw", Encode.string raw )
            ]
                |> Encode.object
        }


graphsDecoder : Decoder (DictIri Graph)
graphsDecoder =
    Rdf.decoder
        |> Decode.keyValuePairs
        |> Decode.map (List.map (Tuple.mapFirst Rdf.iri) >> DictIri.fromList)


storeGraph : Iri -> Graph -> ConcurrentTask Error ()
storeGraph iri graph =
    ConcurrentTask.define
        { function = "quadstore:storeGraph"
        , expect = ConcurrentTask.expectWhatever
        , errors = ConcurrentTask.expectThrows Error
        , args =
            [ ( "iri"
              , iri
                    |> Rdf.toUrl
                    |> Encode.string
              )
            , ( "triples"
              , Rdf.encode graph
              )
            ]
                |> Encode.object
        }


getGraph : Iri -> ConcurrentTask Error Graph
getGraph iri =
    ConcurrentTask.define
        { function = "quadstore:getGraph"
        , expect = ConcurrentTask.expectJson Rdf.decoder
        , errors = ConcurrentTask.expectThrows Error
        , args =
            [ ( "iri"
              , iri
                    |> Rdf.toUrl
                    |> Encode.string
              )
            ]
                |> Encode.object
        }


getGraphTurtle : Iri -> ConcurrentTask Error String
getGraphTurtle iri =
    ConcurrentTask.define
        { function = "quadstore:getGraphTurtle"
        , expect = ConcurrentTask.expectString
        , errors = ConcurrentTask.expectThrows Error
        , args =
            [ ( "iri"
              , iri
                    |> Rdf.toUrl
                    |> Encode.string
              )
            ]
                |> Encode.object
        }


getGraphNTriples : Iri -> ConcurrentTask Error String
getGraphNTriples iri =
    ConcurrentTask.define
        { function = "quadstore:getGraphNTriples"
        , expect = ConcurrentTask.expectString
        , errors = ConcurrentTask.expectThrows Error
        , args =
            [ ( "iri"
              , iri
                    |> Rdf.toUrl
                    |> Encode.string
              )
            ]
                |> Encode.object
        }


getGraphJsonLd : Iri -> ConcurrentTask Error String
getGraphJsonLd iri =
    ConcurrentTask.define
        { function = "quadstore:getGraphJsonLd"
        , expect = ConcurrentTask.expectString
        , errors = ConcurrentTask.expectThrows Error
        , args =
            [ ( "iri"
              , iri
                    |> Rdf.toUrl
                    |> Encode.string
              )
            ]
                |> Encode.object
        }


deleteGraph : Iri -> ConcurrentTask Error ()
deleteGraph iri =
    ConcurrentTask.define
        { function = "quadstore:deleteGraph"
        , expect = ConcurrentTask.expectWhatever
        , errors = ConcurrentTask.expectThrows Error
        , args =
            [ ( "iri"
              , iri
                    |> Rdf.toUrl
                    |> Encode.string
              )
            ]
                |> Encode.object
        }


query : String -> ConcurrentTask Error Sparql.Results
query queryString =
    ConcurrentTask.define
        { function = "quadstore:query"
        , expect = ConcurrentTask.expectJson Sparql.resultsDecoder
        , errors = ConcurrentTask.expectThrows Error
        , args =
            [ ( "queryString"
              , queryString
                    |> Encode.string
              )
            ]
                |> Encode.object
        }
