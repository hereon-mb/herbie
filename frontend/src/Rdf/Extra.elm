module Rdf.Extra exposing
    ( defaultLabel
    , labelFallback
    , toAnyLiteralOrIri
    )

import Rdf exposing (AnyLiteral, Iri, Node(..), NodeInternal(..))
import String.Extra as String


defaultLabel : Iri -> Maybe String
defaultLabel iri =
    let
        url =
            Rdf.toUrl iri

        humanize string =
            case String.humanize string of
                "" ->
                    Nothing

                humanized ->
                    Just humanized
    in
    case String.split "#" url of
        [ _ ] ->
            case List.reverse (String.split "/" url) of
                last :: secondLast :: _ ->
                    if last == "" then
                        humanize secondLast

                    else
                        humanize last

                last :: _ ->
                    humanize last

                _ ->
                    Nothing

        [ _, fragment ] ->
            humanize fragment

        _ ->
            Nothing


labelFallback : Iri -> Maybe String
labelFallback iri =
    let
        url =
            Rdf.toUrl iri
    in
    case String.split "#" url of
        [ _ ] ->
            case List.reverse (String.split "/" url) of
                last :: secondLast :: _ ->
                    if last == "" then
                        Just secondLast

                    else
                        Just last

                last :: _ ->
                    Just last

                _ ->
                    Nothing

        [ _, fragment ] ->
            Just fragment

        _ ->
            Nothing


toAnyLiteralOrIri : Node compatible -> Maybe AnyLiteral
toAnyLiteralOrIri (Node node) =
    case node of
        BlankNode _ ->
            Nothing

        Iri _ ->
            Just (Node node)

        Literal _ ->
            Just (Node node)
