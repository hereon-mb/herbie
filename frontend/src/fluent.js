// Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
// a client-server based web application for research documentation
//
// Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
//
// Authors: Fabian Kirchner, Catriona Eschke
//
// This program is subject to the terms and conditions for non-commercial use of
// ELN. You can find the license text in the file LICENSE.en and under
// http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
// you have any questions or comments, you can contact us at hereon at
// herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
// Max-Planck-Straße 1, 21502 Geesthacht, Germany.

import "@wolfadex/fluent-web";
import { FluentBundle, FluentResource } from "@fluent/bundle";
import { negotiateLanguages } from "@fluent/langneg";

export async function getBundles() {
  return Promise.all([
    fetch("/static/frontend/messages.de.ftl").then((response) => response.text()),
    fetch("/static/frontend/messages.en-US.ftl").then((response) => response.text()),
  ]).then(([deRaw, enUSRaw]) => {
    const fluentResources = {
      "de": new FluentResource(deRaw),
      "en-US": new FluentResource(enUSRaw),
    };
    const supportedLocales = Object.keys(fluentResources);

    const currentLocales = negotiateLanguages(
      navigator.languages,
      supportedLocales,
      { defaultLocale: 'en-US' },
    );

    const bundles = [];
    for (const locale of currentLocales) {
      const bundle = new FluentBundle(locale);
      bundle.addResource(fluentResources[locale]);
      bundles.push(bundle)
    }

    return bundles;
  })
}
