{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Api exposing
    ( request, requestIri
    , absolute
    , iriHttp, iriHttpWith
    , Response, Error(..), errorToMessages
    , expectWhatever
    , expectString, expectJson
    , expectGraph, expectGraphOrValidationReport
    , expectGraphWithNext, expectGraphWithPrev, expectGraphWithPrevAndNext
    , expectLocation
    , resolveJson, task
    )

{-|

@docs request, requestIri

@docs absolute
@docs iriHttp, iriHttpWith

@docs Response, Error, errorToMessages
@docs expectWhatever
@docs expectString, expectJson
@docs expectGraph, expectGraphOrValidationReport
@docs expectGraphWithNext, expectGraphWithPrev, expectGraphWithPrevAndNext
@docs expectLocation

-}

import Api.C
import Dict
import Effect.Internal
import Http
import Json.Decode as Decode exposing (Decoder)
import Rdf exposing (Iri, StringOrLangString)
import Rdf.Decode as RdfDecode
import Rdf.Decode.Extra as RdfDecode
import Rdf.Graph as Rdf exposing (Graph)
import Rdf.Namespaces exposing (sh)
import Shacl.Report as Report exposing (Report)
import Task exposing (Task)
import Url.Builder exposing (QueryParameter)
import Url.Extra as Url


type alias Response a =
    Result Error a


type Error
    = BadUrl String
    | Timeout
    | NetworkError
    | BadRequestRdf Http.Metadata (List StringOrLangString)
    | BadRequestValidationReport Report
    | BadRequestJson Http.Metadata String
    | CouldNotParseErrorRdf Http.Metadata String Rdf.Error
    | CouldNotParseErrorJson Http.Metadata String Decode.Error
    | BadErrorRdf Http.Metadata Graph
    | BadBodyRdf Http.Metadata String
    | BadBodyJson Http.Metadata String Decode.Error
    | BadStatus Http.Metadata String
    | MissingHeader Http.Metadata String
    | CouldNotDecodeRdf RdfDecode.Error


errorToMessages : Error -> List StringOrLangString
errorToMessages error =
    case error of
        BadUrl url ->
            [ fromString ("The url '" ++ url ++ "' does not work.") ]

        Timeout ->
            [ fromString "The server timed out." ]

        NetworkError ->
            [ fromString "Your computer is offline." ]

        BadRequestRdf _ messages ->
            messages

        BadRequestValidationReport _ ->
            [ fromString "SHACL validation was not successfull." ]

        BadRequestJson _ message ->
            [ fromString message ]

        CouldNotParseErrorRdf _ body _ ->
            [ fromString ("Could not make sense of the provided error '" ++ body ++ "'.") ]

        CouldNotParseErrorJson _ body _ ->
            [ fromString ("Could not make sense of the provided error '" ++ body ++ "'.") ]

        BadErrorRdf _ _ ->
            [ fromString "Could not make sense of the provided error graph." ]

        BadBodyRdf _ body ->
            [ fromString ("Could not make sense of the provided data '" ++ body ++ "'.") ]

        BadBodyJson _ body errorDecode ->
            [ fromString ("Could not make sense of the provided data '" ++ body ++ "':" ++ Decode.errorToString errorDecode ++ ".") ]

        BadStatus { statusCode } _ ->
            [ fromString ("The request failed with status code " ++ String.fromInt statusCode ++ ".") ]

        MissingHeader _ header ->
            [ fromString ("The response does not contain a '" ++ header ++ "' header.") ]

        CouldNotDecodeRdf errorDecode ->
            [ fromString ("Could not decode the RDF graph: " ++ RdfDecode.errorToString errorDecode ++ ".") ]


fromString : String -> StringOrLangString
fromString string =
    Rdf.stringOrLangStringFrom (Just string) []


request :
    Api.C.C rest
    ->
        { method : String
        , headers : List Http.Header
        , pathSegments : List String
        , queryParameters : List QueryParameter
        , body : Http.Body
        , expect : Http.Expect msg
        , tracker : Maybe String
        }
    -> Effect.Internal.Effect action msg
request c config =
    { method = config.method
    , headers =
        if config.method == "GET" then
            config.headers

        else
            Http.header "X-CSRFToken" c.csrfToken :: config.headers
    , url = absolute config.pathSegments config.queryParameters
    , body = config.body
    , expect = config.expect
    , timeout = Nothing
    , tracker = config.tracker
    }
        |> Http.request
        |> Effect.Internal.fromCmd


requestIri :
    Api.C.C rest
    ->
        { method : String
        , headers : List Http.Header
        , iri : Iri
        , queryParameters : List QueryParameter
        , body : Http.Body
        , expect : Http.Expect msg
        , tracker : Maybe String
        }
    -> Effect.Internal.Effect action msg
requestIri c config =
    let
        appendQueryParams url =
            url ++ query

        query =
            Url.Builder.toQuery config.queryParameters
    in
    { method = config.method
    , headers =
        if config.method == "GET" then
            config.headers

        else
            Http.header "X-CSRFToken" c.csrfToken :: config.headers
    , url =
        -- FIXME Check that IRI is local
        config.iri
            |> Rdf.toUrl
            |> Url.setProtocol c
            |> appendQueryParams
    , body = config.body
    , expect = config.expect
    , timeout = Nothing
    , tracker = config.tracker
    }
        |> Http.request
        |> Effect.Internal.fromCmd


task :
    Api.C.C rest
    ->
        { method : String
        , headers : List Http.Header
        , pathSegments : List String
        , queryParameters : List QueryParameter
        , body : Http.Body
        , resolver : Http.Resolver x a
        }
    -> Task x a
task c config =
    Http.task
        { method = config.method
        , headers =
            if config.method == "GET" then
                config.headers

            else
                Http.header "X-CSRFToken" c.csrfToken :: config.headers
        , url = absolute config.pathSegments config.queryParameters
        , body = config.body
        , resolver = config.resolver
        , timeout = Nothing
        }


resolveJson : Decoder a -> Http.Resolver Error a
resolveJson decoder =
    Http.stringResolver (fromJsonResponse (parseBodyJson decoder))


absolute : List String -> List Url.Builder.QueryParameter -> String
absolute pathSegments parameters =
    "/" ++ String.join "/" pathSegments ++ "/" ++ Url.Builder.toQuery parameters


iriHttp : Api.C.C rest -> List String -> Rdf.Iri
iriHttp c pathSegments =
    [ "http://"
    , c.url.host
    , c.url.port_
        |> Maybe.map (\port_ -> ":" ++ String.fromInt port_)
        |> Maybe.withDefault ""
    , if List.isEmpty pathSegments then
        "/"

      else
        "/" ++ String.join "/" pathSegments ++ "/"
    ]
        |> String.concat
        |> Rdf.iri


iriHttpWith : Api.C.C rest -> List String -> List Url.Builder.QueryParameter -> Rdf.Iri
iriHttpWith c pathSegments parameters =
    [ "http://"
    , c.url.host
    , c.url.port_
        |> Maybe.map (\port_ -> ":" ++ String.fromInt port_)
        |> Maybe.withDefault ""
    , if List.isEmpty pathSegments then
        "/"

      else
        "/" ++ String.join "/" pathSegments ++ "/"
    , Url.Builder.toQuery parameters
    ]
        |> String.concat
        |> Rdf.iri


expectWhatever : (Response () -> msg) -> Http.Expect msg
expectWhatever toMsg =
    Http.expectStringResponse toMsg <|
        fromRdfResponse <|
            \_ _ ->
                Ok ()


expectLocation : (Response String -> msg) -> Http.Expect msg
expectLocation toMsg =
    Http.expectStringResponse toMsg <|
        fromRdfResponse <|
            \metadata _ ->
                case Dict.get "location" metadata.headers of
                    Nothing ->
                        Err (MissingHeader metadata "location")

                    Just location ->
                        Ok location


expectString : (Response String -> msg) -> Http.Expect msg
expectString toMsg =
    Http.expectStringResponse toMsg <|
        fromRdfResponse <|
            \_ body ->
                Ok body


expectGraph : (Response Graph -> msg) -> Http.Expect msg
expectGraph toMsg =
    Http.expectStringResponse toMsg <|
        fromRdfResponse parseBodyRdf


expectGraphWithNext : (Response ( Graph, String ) -> msg) -> Http.Expect msg
expectGraphWithNext toMsg =
    Http.expectStringResponse toMsg <|
        fromRdfResponse <|
            \metadata body ->
                Result.map2 Tuple.pair
                    (parseBodyRdf metadata body)
                    (metadata.headers
                        |> Dict.get "link"
                        |> Maybe.andThen
                            (String.split ","
                                >> List.map String.trim
                                >> List.filterMap nextFromString
                                >> List.head
                            )
                        |> Result.fromMaybe (MissingHeader metadata "link")
                    )


nextFromString : String -> Maybe String
nextFromString link =
    case
        link
            |> String.split ";"
            |> List.map String.trim
    of
        [ url, rel ] ->
            if rel == "rel=next" then
                url
                    |> String.dropLeft 1
                    |> String.dropRight 1
                    |> Just

            else
                Nothing

        _ ->
            Nothing


expectGraphWithPrev : (Response ( Graph, Maybe String ) -> msg) -> Http.Expect msg
expectGraphWithPrev toMsg =
    Http.expectStringResponse toMsg <|
        fromRdfResponse <|
            \metadata body ->
                case Rdf.parse body of
                    Err _ ->
                        Err (BadBodyRdf metadata body)

                    Ok graph ->
                        Ok
                            ( graph
                            , metadata.headers
                                |> Dict.get "link"
                                |> Maybe.andThen
                                    (String.split ","
                                        >> List.map String.trim
                                        >> List.filterMap prevFromString
                                        >> List.head
                                    )
                            )


prevFromString : String -> Maybe String
prevFromString link =
    case
        link
            |> String.split ";"
            |> List.map String.trim
    of
        [ url, rel ] ->
            if rel == "rel=prev" then
                url
                    |> String.dropLeft 1
                    |> String.dropRight 1
                    |> Just

            else
                Nothing

        _ ->
            Nothing


expectGraphWithPrevAndNext : (Response ( Graph, Maybe String, String ) -> msg) -> Http.Expect msg
expectGraphWithPrevAndNext toMsg =
    Http.expectStringResponse toMsg <|
        fromRdfResponse <|
            \metadata body ->
                Result.map2
                    (\graph next ->
                        ( graph
                        , metadata.headers
                            |> Dict.get "link"
                            |> Maybe.andThen
                                (String.split ","
                                    >> List.map String.trim
                                    >> List.filterMap prevFromString
                                    >> List.head
                                )
                        , next
                        )
                    )
                    (parseBodyRdf metadata body)
                    (metadata.headers
                        |> Dict.get "link"
                        |> Maybe.andThen
                            (String.split ","
                                >> List.map String.trim
                                >> List.filterMap nextFromString
                                >> List.head
                            )
                        |> Result.fromMaybe (MissingHeader metadata "link")
                    )


expectJson : (Response a -> msg) -> Decoder a -> Http.Expect msg
expectJson toMsg decoder =
    Http.expectStringResponse toMsg <|
        fromJsonResponse (parseBodyJson decoder)


expectGraphOrValidationReport : (Result Error Graph -> msg) -> Http.Expect msg
expectGraphOrValidationReport toMsg =
    Http.expectStringResponse toMsg <|
        fromResponse
            { onGoodStatus = parseBodyRdf
            , onBadRequest = parseErrorValidationReport
            }


fromRdfResponse : (Http.Metadata -> String -> Result Error a) -> Http.Response String -> Result Error a
fromRdfResponse onGoodStatus =
    fromResponse
        { onGoodStatus = onGoodStatus
        , onBadRequest = parseErrorRdf
        }


fromJsonResponse : (Http.Metadata -> String -> Result Error a) -> Http.Response String -> Result Error a
fromJsonResponse onGoodStatus =
    fromResponse
        { onGoodStatus = onGoodStatus
        , onBadRequest = parseErrorJson
        }


fromResponse :
    { onGoodStatus : Http.Metadata -> String -> Result Error a
    , onBadRequest : Http.Metadata -> String -> Error
    }
    -> Http.Response String
    -> Result Error a
fromResponse { onGoodStatus, onBadRequest } response =
    case response of
        Http.BadUrl_ url ->
            Err (BadUrl url)

        Http.Timeout_ ->
            Err Timeout

        Http.NetworkError_ ->
            Err NetworkError

        Http.BadStatus_ metadata body ->
            if metadata.statusCode == 400 then
                Err (onBadRequest metadata body)

            else
                Err (BadStatus metadata body)

        Http.GoodStatus_ metadata body ->
            onGoodStatus metadata body


parseBodyRdf : Http.Metadata -> String -> Result Error Graph
parseBodyRdf metadata body =
    case Rdf.parse body of
        Err _ ->
            Err (BadBodyRdf metadata body)

        Ok graph ->
            Ok graph


parseBodyJson : Decoder a -> Http.Metadata -> String -> Result Error a
parseBodyJson decoder metadata body =
    case Decode.decodeString decoder body of
        Ok value ->
            Ok value

        Err err ->
            Err (BadBodyJson metadata body err)


parseErrorRdf : Http.Metadata -> String -> Error
parseErrorRdf metadata body =
    case Rdf.parse body of
        Err error ->
            CouldNotParseErrorRdf metadata body error

        Ok graph ->
            graph
                |> RdfDecode.decode decoderValidationReport
                |> Result.toMaybe
                |> Maybe.map (BadRequestRdf metadata)
                |> Maybe.withDefault (BadErrorRdf metadata graph)


decoderValidationReport : RdfDecode.Decoder (List StringOrLangString)
decoderValidationReport =
    RdfDecode.instanceOf (sh "ValidationReport")
        (RdfDecode.predicate (sh "result")
            (RdfDecode.many
                (RdfDecode.predicate (sh "resultMessage")
                    RdfDecode.stringOrLangString
                )
            )
        )


parseErrorValidationReport : Http.Metadata -> String -> Error
parseErrorValidationReport metadata body =
    case Rdf.parse body of
        Err error ->
            CouldNotParseErrorRdf metadata body error

        Ok graph ->
            BadRequestValidationReport (Report.fromGraph graph)


parseErrorJson : Http.Metadata -> String -> Error
parseErrorJson metadata body =
    case Decode.decodeString (Decode.index 0 Decode.string) body of
        Err err ->
            CouldNotParseErrorJson metadata body err

        Ok reason ->
            BadRequestJson metadata reason
