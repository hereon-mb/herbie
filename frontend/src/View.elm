{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module View exposing
    ( Config
    , View
    , map
    , none
    , toBrowserDocument
    )

import Browser
import Element
    exposing
        ( Attribute
        , Element
        , column
        , fill
        , height
        , inFront
        , width
        )
import Element.Extra
    exposing
        ( elWithAncestorWithScrollbarY
        , id
        , pointerEvents
        , style
        , userSelect
        )
import Fluent
import Ui.Atom.Portal as Portal
import Ui.Atom.SandboxBorder as SandboxBorder
import Ui.Molecule.Breadcrumbs as Breadcrumbs exposing (Breadcrumbs)
import Ui.Theme.Layout as Layout


type alias View msg =
    { title : String
    , element : Element msg
    , overlays : List (Element msg)
    , breadcrumbs : Breadcrumbs
    , fabVisible : Bool
    , navigationRailVisible : Bool
    , loading : Bool
    }


placeholder : String -> View msg
placeholder str =
    { title = str
    , element = Element.text str
    , overlays = []
    , breadcrumbs = Breadcrumbs.empty
    , fabVisible = False
    , navigationRailVisible = False
    , loading = False
    }


none : View msg
none =
    placeholder ""


map : (a -> b) -> View a -> View b
map fn view =
    { title = view.title
    , element = Element.map fn view.element
    , overlays = List.map (Element.map fn) view.overlays
    , breadcrumbs = view.breadcrumbs
    , fabVisible = view.fabVisible
    , navigationRailVisible = view.navigationRailVisible
    , loading = view.loading
    }


type alias Config =
    { fluentBundles : Fluent.Bundles
    , sandbox : Bool
    , dragging : Bool
    }


toBrowserDocument : Config -> View msg -> Browser.Document msg
toBrowserDocument config view =
    { title = view.title
    , body =
        [ Fluent.provider config.fluentBundles
            [ Layout.default
                (if config.sandbox then
                    SandboxBorder.view (viewToElement config view)

                 else
                    viewToElement config view
                )
            ]
        ]
    }


viewToElement : Config -> View msg -> Element msg
viewToElement config view =
    let
        addOverlays attrs =
            attrs ++ List.map viewOverlay view.overlays
    in
    elWithAncestorWithScrollbarY
        ([ width fill
         , height fill
         , userSelect (not config.dragging)
         , pointerEvents (not config.dragging)
         , cursor config.dragging
         , id "main-content"
         ]
            |> addOverlays
        )
        view.element


viewOverlay : Element msg -> Attribute msg
viewOverlay element =
    inFront
        (column
            [ width fill
            , height fill
            , style "z-index" "1"
            , pointerEvents False
            ]
            [ element
            , Portal.target
            ]
        )


cursor : Bool -> Attribute msg
cursor dragging =
    style "cursor"
        (if dragging then
            "pointer"

         else
            "auto"
        )
