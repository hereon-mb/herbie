// Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
// a client-server based web application for research documentation
//
// Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
//
// Authors: Fabian Kirchner, Catriona Eschke
//
// This program is subject to the terms and conditions for non-commercial use of
// ELN. You can find the license text in the file LICENSE.en and under
// http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
// you have any questions or comments, you can contact us at hereon at
// herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
// Max-Planck-Straße 1, 21502 Geesthacht, Germany.

import * as ElmDebugger from 'elm-debug-transformer';
import { Elm } from "./Main.elm";
import { getBundles } from "./fluent.js";
import './main.scss';
import './quirks.css';
import './patches.development.css';
import { registerCustomElements } from "./custom-elements.js";
import { N3, DataFactory, Quadstore, BrowserLevel, Engine } from './quadstore-bundle.js';
const ConcurrentTask = require("@andrewmacmurray/elm-concurrent-task");
import rdf from 'rdf-ext';
import SerializerTurtle from '@rdfjs/serializer-turtle';
import SerializerNTriples from '@rdfjs/serializer-ntriples';
import SerializerJsonLd from '@rdfjs/serializer-jsonld';
import ElmPortal from "./elm-portal";

if (process.env.NODE_ENV === "development") {
  ElmDebugger.register({simple_mode: false});

  function perfObserver(list, observer) {
    list.getEntries().forEach((entry) => {
      if (entry.entryType === "measure") {
        console.debug(`${entry.name} took ${entry.duration} ms`);
      }
    });
  }
  const observer = new PerformanceObserver(perfObserver);
  observer.observe({ entryTypes: ["measure", "mark"] });
}

// Log translation errors
window.addEventListener("fluent-web-error", function(event) {
  console.warn(`An error for the i18n key "${event.detail.messageId}" occured: ${event.detail.errors}`)
})

var csrfToken = document.querySelector('[name=csrfmiddlewaretoken]').value;


// Service worker

const registerServiceWorker = async () => {
  navigator.serviceWorker.register(
    "/sw.js",
    { scope: "/" },
  ).then((registration) => {
    var serviceWorker;

    if (registration.installing) {
      serviceWorker = registration.installing;
    } else if (registration.waiting) {
      serviceWorker = registration.waiting;
    } else if (registration.active) {
      serviceWorker = registration.active;
    }

    console.debug(`Service worker state is ${serviceWorker.state}`);

    if (serviceWorker) {
      serviceWorker.addEventListener("statechange", (event) => {
        console.debug(`Service worker changed state to ${event.target.state}`);
      });
    }
  }).catch((error) => {
    console.error(`Service worker registration failed with ${error}`);
  });
};

if ("serviceWorker" in navigator) {
  window.addEventListener("load", () => {
    registerServiceWorker();
  });
}


getBundles().then(async (fluentBundles) => {
  const dataFactory = new DataFactory();
  const store = await initStore(dataFactory);
  const engine = new Engine(store);

  ElmPortal({ mountId: "elm-portal-target" });

  registerCustomElements();

  var flags = {
    sandbox: sandbox,
    useTripleStore: useTripleStore,
    useHelmholtzAai: useHelmholtzAai,
    hideUserPaswordLogin: hideUserPaswordLogin,
    useSparklis: useSparklis,
    allowUploads: allowUploads,
    imprintUrl: imprintUrl,
    dataProtectionUrl: dataProtectionUrl,
    accessibilityUrl: accessibilityUrl,
    csrfToken: csrfToken,
    fluentBundles: fluentBundles,
    rawLocale: navigator.language,
  };

  var app = Elm.Main.init({ flags: flags });

  ConcurrentTask.register({
    tasks: {
      "openTab": (options) => {
        window.open(options.url, "_blank");
      },
      "localstorage:getItem": (options) => {
        try {
          const item = localStorage.getItem(options.key);
          if (item === null) {
            return { error: "NO_VALUE" };
          } else {
            return item;
          }
        } catch (e) {
          return { error: "READ_BLOCKED" };
        }
      },
      "localstorage:setItem": (options) => {
        try {
          localStorage.setItem(options.key, options.value);
        } catch (e) {
          if (e.name === "QuotaExceededError") {
            return { error: "QUOTA_EXCEEDED" };
          } else {
            return { error: "WRITE_BLOCKED" };
          }
        }
      },
      "localstorage:removeItem": (options) => {
        localStorage.removeItem(options.key);
      },
      "quadstore:clear": async () => await store.clear(),
      "quadstore:parseTurtle": (args) => {
        performance.mark("quadstore:parseTurtle:started");

        const parser = new N3.Parser({ baseIRI: args.iri });
        const quads = parser.parse(args.raw);

        performance.mark("quadstore:parseTurtle:finished");
        performance.measure(`quadstore:parseTurtle(${args.iri})`, "quadstore:parseTurtle:started", "quadstore:parseTurtle:finished");

        return quads;
      },
      "quadstore:parseTrig": (args) => {
        performance.mark("quadstore:parseTrig:started");

        const parser = new N3.Parser({ baseIRI: args.iri });

        const graphs = {}
        graphs[args.iri] = []

        const quadsUnsorted = parser.parse(args.raw);
        quadsUnsorted.forEach((quad) => {
          if (quad.graph) {
            const graphId = quad.graph.value || args.iri;

            if (graphs[graphId]) {
              graphs[graphId].push(quad);
            } else {
              graphs[graphId] = [quad];
            }
          } else {
            console.log("quad without graph", quad);
          }
        })

        performance.mark("quadstore:parseTrig:finished");
        performance.measure(`quadstore:parseTrig(${args.iri})`, "quadstore:parseTrig:started", "quadstore:parseTrig:finished");

        return graphs;
      },
      "quadstore:storeGraph": async (args) => {
        performance.mark("quadstore:storeGraph:started");

        const quads = new Array();
        args.triples.forEach((quad) => {
          quads.push(dataFactory.quad(
            quad.subject,
            quad.predicate,
            quad.object,
            dataFactory.namedNode(args.iri),
          ))
        });
        store.deleteGraph(dataFactory.namedNode(args.iri));
        const scope = await store.initScope();
        await store.multiPut(quads, { scope });

        performance.mark("quadstore:storeGraph:finished");
        performance.measure(`quadstore:storeGraph(${args.iri})`, "quadstore:storeGraph:started", "quadstore:storeGraph:finished");
      },
      "quadstore:getGraph": async (args) => {
        performance.mark("quadstore:getGraph:started");

        const graph = dataFactory.namedNode(args.iri);
        const result = await store.get({ graph });

        performance.mark("quadstore:getGraph:finished");
        performance.measure(`quadstore:getGraph(${args.iri})`, "quadstore:getGraph:started", "quadstore:getGraph:finished");

        return result.items;
      },
      "quadstore:getGraphTurtle": async (args) => {
        performance.mark("quadstore:getGraphTurtle:started");

        const graph = dataFactory.namedNode(args.iri);
        const result = await store.get({ graph });
        const dataset = rdf.dataset(result.items);
        const stream = (new SerializerTurtle()).import(dataset.toStream());
        const text = await readStreamText(stream);

        performance.mark("quadstore:getGraphTurtle:finished");
        performance.measure(`quadstore:getGraphTurtle(${args.iri})`, "quadstore:getGraphTurtle:started", "quadstore:getGraphTurtle:finished");

        return text;
      },
      "quadstore:getGraphNTriples": async (args) => {
        performance.mark("quadstore:getGraphNTriples:started");

        const graph = dataFactory.namedNode(args.iri);
        const result = await store.get({ graph });
        const dataset = rdf.dataset(result.items);
        const stream = (new SerializerNTriples()).import(dataset.toStream());
        const text = await readStreamText(stream);

        performance.mark("quadstore:getGraphNTriples:finished");
        performance.measure(`quadstore:getGraphNTriples(${args.iri})`, "quadstore:getGraphNTriples:started", "quadstore:getGraphNTriples:finished");

        return text;
      },
      "quadstore:getGraphJsonLd": async (args) => {
        performance.mark("quadstore:getGraphJsonLd:started");

        const graph = dataFactory.namedNode(args.iri);
        const result = await store.get({ graph });
        const dataset = rdf.dataset(result.items);
        const stream = (new SerializerJsonLd()).import(dataset.toStream());
        const objects = await readStreamObjects(stream);
        const text = JSON.stringify(objects, null, 2);

        performance.mark("quadstore:getGraphJsonLd:finished");
        performance.measure(`quadstore:getGraphJsonLd(${args.iri})`, "quadstore:getGraphJsonLd:started", "quadstore:getGraphJsonLd:finished");

        return text;
      },
      "quadstore:deleteGraph": async (args) => {
        performance.mark("quadstore:deleteGraph:started");

        const graph = dataFactory.namedNode(args.iri);
        await store.deleteGraph(graph);

        performance.mark("quadstore:deleteGraph:finished");
        performance.measure(`quadstore:deleteGraph(${args.iri})`, "quadstore:deleteGraph:started", "quadstore:deleteGraph:finished");
      },
      "quadstore:query": async (args) => {
        performance.mark("quadstore:query:started");

        const query = await engine.query(args.queryString);
        if (query.resultType !== 'bindings') {
          throw new Error('Unexpected result type');
        }

        const stream = await query.execute();
        const readStream = async () => {
          return new Promise((resolve, reject) => {
            const rows = new Array();
            stream.on("error", (error) => {
              reject(error);
            });
            stream.on("data", (bindings) => {
              const row = {}
              bindings.forEach((value, key) => {
                row[key.value] = value
              });
              rows.push(row);
            });
            stream.on("end", () => {
              resolve(rows);
            });
          });
        };

        const result = await readStream();

        performance.mark("quadstore:query:finished");
        performance.measure(`quadstore:query(${args.iri})`, "quadstore:query:started", "quadstore:query:finished");

        return result;
      },
    },
    ports: {
      send: app.ports.send,
      receive: app.ports.receive,
    },
  });

  app.ports.copyToClipboard_.subscribe(async ({mimeType, parts}) => {
    const blob = new Blob(parts, { type: mimeType });
    // XXX Firefox <=127 does not support `ClipboardItem`
    if (typeof ClipboardItem !== "undefined") {
      const data = [new ClipboardItem({ [mimeType]: blob })];
      await navigator.clipboard.write(data);
    } else {
      await navigator.clipboard.writeText(parts[0]);
    }
  });

});


// quadstore

const initStore = async (dataFactory) => {
  const store = new Quadstore({
    dataFactory,
    backend: new BrowserLevel('quadstore'),
  });
  await store.open();

  return store;
};



const readStreamText = async (stream) => {
  return new Promise((resolve, reject) => {
    let text = "";
    stream.on("error", (error) => {
      reject(error);
    });
    stream.on("data", (textNext) => {
      text = text + textNext;
    });
    stream.on("end", () => {
      resolve(text);
    });
  });
};

const readStreamObjects = async (stream) => {
  return new Promise((resolve, reject) => {
    let objects = new Array();
    stream.on("error", (error) => {
      reject(error);
    });
    stream.on("data", (object) => {
      objects = objects.concat(object);
    });
    stream.on("end", () => {
      resolve(objects);
    });
  });
};

// CLIPBOARD
//
// To allow reading the clipboard from `paste` events in Elm, we extens `paste` events in the capturing phase with a property `event.clipboardData._elmCompat.data[format]` which proxies `event.clipboardData.getData(format)`. Thus, making it compatible with JSON decoders.
window.addEventListener("paste", e => {
  e.clipboardData._elmCompat = {
    data: new Proxy({}, {
      get: (_, format) => e.clipboardData.getData(format),
      has: (_, format) => true,
    }),
  };
}, { capture: true, passive: true });
