module Ontology.Class exposing
    ( Class, ClassRdfs
    , NodeShape, NodeShapeRdfs
    , fromGraph, decoder
    )

{-|

@docs Class, ClassRdfs
@docs NodeShape, NodeShapeRdfs

@docs fromGraph, decoder

-}

import List.Extra as List
import Rdf exposing (Iri, StringOrLangString)
import Rdf.Decode as Decode exposing (Decoder)
import Rdf.Decode.Extra as Decode
import Rdf.Graph exposing (Graph)
import Rdf.Namespaces.OWL as OWL
import Rdf.Namespaces.RDFS as RDFS
import Rdf.Namespaces.SH as SH


type alias Class =
    { this : Iri
    , nodeShapes : List NodeShape
    , rdfs : ClassRdfs
    }


type alias ClassRdfs =
    { subClassOf : List Iri
    , label : Maybe StringOrLangString
    , comment : Maybe StringOrLangString
    }


type alias NodeShape =
    { this : Iri
    , rdfs : NodeShapeRdfs
    }


type alias NodeShapeRdfs =
    { isDefinedBy : Iri
    , label : Maybe StringOrLangString
    , comment : Maybe StringOrLangString
    }


fromGraph : Graph -> List Class
fromGraph graph =
    graph
        |> Decode.decode
            (Decode.map2 List.append
                (Decode.fromInstancesOf RDFS.class decoder)
                (Decode.fromInstancesOf OWL.class decoder)
            )
        |> Result.withDefault []
        |> List.uniqueBy (.this >> Rdf.toUrl)


decoder : Decoder Class
decoder =
    Decode.succeed Class
        |> Decode.custom Decode.iri
        |> Decode.optionalAt SH.inverse.targetClass (Decode.many decoderNodeShape) []
        |> Decode.custom
            (Decode.succeed ClassRdfs
                |> Decode.optional RDFS.subClassOf (Decode.many Decode.iri) []
                |> Decode.optionalStringOrLangString RDFS.label
                |> Decode.optionalStringOrLangString RDFS.comment
            )


decoderNodeShape : Decoder NodeShape
decoderNodeShape =
    Decode.succeed NodeShape
        |> Decode.custom Decode.iri
        |> Decode.custom
            (Decode.succeed NodeShapeRdfs
                |> Decode.required RDFS.isDefinedBy Decode.iri
                |> Decode.optionalStringOrLangString RDFS.label
                |> Decode.optionalStringOrLangString RDFS.comment
            )
