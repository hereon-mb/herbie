module Ontology.Shacl exposing
    ( Class, NodeShape
    , fromGraph
    , Author, AuthorRdfs, ClassRdfs, DocumentVersion, DocumentVersionProv, DocumentVersionRdfs, NodeShapeRdfs
    )

{-|

@docs Class, NodeShape

@docs fromGraph

-}

import List.Extra as List
import Rdf exposing (Iri, StringOrLangString)
import Rdf.Decode as Decode exposing (Decoder)
import Rdf.Decode.Extra as Decode
import Rdf.Graph exposing (Graph)
import Rdf.Namespaces.OWL as OWL
import Rdf.Namespaces.PROV as PROV
import Rdf.Namespaces.RDFS as RDFS
import Rdf.Namespaces.SH as SH
import Time


type alias Class =
    { this : Iri
    , nodeShapes : List NodeShape
    , rdfs : ClassRdfs
    }


type alias ClassRdfs =
    { label : Maybe StringOrLangString
    , comment : Maybe StringOrLangString
    }


type alias NodeShape =
    { this : Iri
    , rdfs : NodeShapeRdfs
    }


type alias NodeShapeRdfs =
    { isDefinedBy : DocumentVersion
    }


type alias DocumentVersion =
    { this : Iri
    , rdfs : DocumentVersionRdfs
    , prov : DocumentVersionProv
    }


type alias DocumentVersionRdfs =
    { label : Maybe StringOrLangString
    , comment : Maybe StringOrLangString
    }


type alias DocumentVersionProv =
    { wasAttributedTo : Author
    , generatedAtTime : Time.Posix
    }


type alias Author =
    { rdfs : AuthorRdfs
    }


type alias AuthorRdfs =
    { label : StringOrLangString
    }


fromGraph : Graph -> List Class
fromGraph graph =
    [ Decode.decode (Decode.fromInstancesOf RDFS.class decoder) graph
    , Decode.decode (Decode.fromInstancesOf OWL.class decoder) graph
    ]
        |> List.concatMap (Result.withDefault [])
        |> List.uniqueBy (.this >> Rdf.toUrl)


decoder : Decoder Class
decoder =
    Decode.succeed Class
        |> Decode.custom Decode.iri
        |> Decode.optionalAt SH.inverse.targetClass (Decode.many decoderNodeShape) []
        |> Decode.custom
            (Decode.succeed ClassRdfs
                |> Decode.optionalStringOrLangString RDFS.label
                |> Decode.optionalStringOrLangString RDFS.comment
            )


decoderNodeShape : Decoder NodeShape
decoderNodeShape =
    Decode.succeed NodeShape
        |> Decode.custom Decode.iri
        |> Decode.custom
            (Decode.succeed NodeShapeRdfs
                |> Decode.required RDFS.isDefinedBy decoderDocumentVersion
            )


decoderDocumentVersion : Decoder DocumentVersion
decoderDocumentVersion =
    Decode.succeed DocumentVersion
        |> Decode.custom Decode.iri
        |> Decode.custom
            (Decode.succeed DocumentVersionRdfs
                |> Decode.optionalStringOrLangString RDFS.label
                |> Decode.optionalStringOrLangString RDFS.comment
            )
        |> Decode.custom
            (Decode.succeed DocumentVersionProv
                |> Decode.required PROV.wasAttributedTo decoderAuthor
                |> Decode.required PROV.generatedAtTime Decode.dateTime
            )


decoderAuthor : Decoder Author
decoderAuthor =
    Decode.succeed Author
        |> Decode.custom
            (Decode.succeed AuthorRdfs
                |> Decode.required RDFS.label Decode.stringOrLangString
            )
