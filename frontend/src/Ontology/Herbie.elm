{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Ontology.Herbie exposing
    ( Document, DocumentTerms, DocumentProv, DocumentHerbie
    , DocumentDraft, DocumentDraftRdfs, DocumentDraftTerms, DocumentDraftProv
    , DocumentVersion, DocumentVersionRdfs, DocumentVersionTerms, DocumentVersionSchema
    , DocumentVersionReference, DocumentVersionReferenceRdfs, populateDefaultValues
    , Author, AuthorRdfs
    , decoderDocument
    , versionAt, iriEntered, iriPersisted, iriGenerated, iriSupport
    , Access, accessFromGraph, accessToGraph
    , herbie
    )

{-|


# Document

@docs Document, DocumentTerms, DocumentProv, DocumentHerbie
@docs DocumentDraft, DocumentDraftRdfs, DocumentDraftTerms, DocumentDraftProv
@docs DocumentVersion, DocumentVersionRdfs, DocumentVersionTerms, DocumentVersionSchema
@docs DocumentVersionReference, DocumentVersionReferenceRdfs, populateDefaultValues
@docs Author, AuthorRdfs

@docs decoderDocument

@docs versionAt, iriEntered, iriPersisted, iriGenerated, iriSupport


# Access

@docs Access, accessFromGraph, accessToGraph


# Namespaces

@docs herbie

-}

import List.Extra as List
import Rdf exposing (BlankNodeOrIri, Iri, StringOrLangString)
import Rdf.Decode as Decode exposing (Decoder)
import Rdf.Decode.Extra as Decode
import Rdf.Graph as Rdf exposing (Graph)
import Rdf.Namespaces exposing (a, owl, schema, sh)
import Rdf.Namespaces.DCTERMS as DCTERMS
import Rdf.Namespaces.PROV as PROV
import Rdf.Namespaces.RDFS as RDFS
import Rdf.PropertyPath as Rdf exposing (PropertyPath)
import Time exposing (Posix)


herbie : String -> Iri
herbie name =
    Rdf.iri ("http://purls.helmholtz-metadaten.de/herbie/core/#" ++ name)


hac : String -> Iri
hac name =
    Rdf.iri ("http://purls.helmholtz-metadaten.de/herbie/hac/#" ++ name)


type alias Document =
    { this : Iri
    , draft : Maybe DocumentDraft
    , versions : List DocumentVersion
    , publishedAt : Maybe Posix
    , terms : DocumentTerms
    , prov : DocumentProv
    , herbie : DocumentHerbie
    }


type alias DocumentTerms =
    { created : Posix
    , modified : Posix
    }


type alias DocumentProv =
    { wasAttributedTo : Author
    }


type alias DocumentHerbie =
    { workspaceUuid : String
    }


type alias DocumentDraft =
    { this : Iri
    , rdfs : DocumentDraftRdfs
    , terms : DocumentDraftTerms
    , prov : DocumentDraftProv
    }


type alias DocumentDraftRdfs =
    { label : Maybe StringOrLangString
    , comment : Maybe StringOrLangString
    }


type alias DocumentDraftTerms =
    { modified : Posix
    , source : List Iri
    , conformsTo : Maybe DocumentVersionReference
    }


type alias DocumentDraftProv =
    { generatedAtTime : Posix
    , wasAttributedTo : Author
    }


type alias DocumentVersion =
    { this : Iri
    , index : Int
    , createdAt : Posix
    , publishedAt : Posix
    , createdBy : Author
    , ontologies : List Iri
    , nodeShapes : List Iri
    , rdfs : DocumentVersionRdfs
    , terms : DocumentVersionTerms
    , schema : DocumentVersionSchema
    }


type alias DocumentVersionRdfs =
    { label : Maybe StringOrLangString
    , comment : Maybe StringOrLangString
    }


type alias DocumentVersionTerms =
    { source : List Iri
    , conformsTo : Maybe DocumentVersionReference
    }


type alias DocumentVersionSchema =
    { hasPart : List DocumentVersionReference
    }


type alias Author =
    { this : Iri
    , rdfs : AuthorRdfs
    }


type alias AuthorRdfs =
    { label : StringOrLangString
    }


type alias DocumentVersionReference =
    { this : Iri
    , rdfs : DocumentVersionReferenceRdfs
    }


type alias DocumentVersionReferenceRdfs =
    { label : Maybe StringOrLangString
    }


{-| XXX We have no good way to detect when a document is first opened, which is required information for populating default values.

Default values cannot be populated when empty, because the user might have deleted a default value, and saved the document.

-}
populateDefaultValues : Document -> { populateDefaultValues : Bool }
populateDefaultValues document =
    { populateDefaultValues = document.terms.modified == document.terms.created }


iriEntered : Iri -> Iri
iriEntered =
    Rdf.dropFragment >> Rdf.appendPath "entered/"


iriPersisted : Iri -> Iri
iriPersisted =
    Rdf.dropFragment >> Rdf.appendPath "persisted/"


iriGenerated : Iri -> Iri
iriGenerated =
    Rdf.dropFragment >> Rdf.appendPath "generated/"


iriSupport : Iri -> Iri
iriSupport =
    Rdf.dropFragment >> Rdf.appendPath "support/"


versionAt : Int -> Document -> Maybe DocumentVersion
versionAt index document =
    List.getAt (List.length document.versions - index) document.versions


decoderDocument : Decoder Document
decoderDocument =
    let
        toUuid iriWorkspace =
            case List.reverse (String.split "/" (Rdf.toUrl iriWorkspace)) of
                "" :: uuid :: _ ->
                    Decode.succeed uuid

                _ ->
                    Decode.fail ("Could not extract uuid from workspace IRI <" ++ Rdf.toUrl iriWorkspace ++ ">.")
    in
    Decode.andThen
        (\versions ->
            let
                publishedAt =
                    versions
                        |> List.sortBy (.publishedAt >> Time.posixToMillis)
                        |> List.head
                        |> Maybe.map .publishedAt
            in
            Decode.succeed Document
                |> Decode.custom Decode.iri
                |> Decode.optionalAt (Rdf.InversePath (Rdf.PredicatePath PROV.specializationOf)) decoderDraft Nothing
                |> Decode.hardcoded versions
                |> Decode.hardcoded publishedAt
                |> Decode.custom
                    (Decode.succeed DocumentTerms
                        |> Decode.required DCTERMS.created Decode.dateTime
                        |> Decode.required DCTERMS.modified Decode.dateTime
                    )
                |> Decode.custom
                    (Decode.succeed DocumentProv
                        |> Decode.required PROV.wasAttributedTo decoderAuthor
                    )
                |> Decode.custom
                    (Decode.succeed DocumentHerbie
                        |> Decode.required (herbie "workspace") (Decode.andThen toUuid Decode.iri)
                    )
        )
        (Decode.property (Rdf.InversePath (Rdf.PredicatePath PROV.specializationOf)) decoderVersions)


decoderDraft : Decoder (Maybe DocumentDraft)
decoderDraft =
    Decode.predicate a (Decode.many Decode.iri)
        |> Decode.andThen
            (\classes ->
                if List.member (herbie "DocumentDraft") classes then
                    Decode.map Just decoderDraftHelp

                else
                    Decode.succeed Nothing
            )
        |> Decode.many
        |> Decode.map (List.filterMap identity >> List.head)


decoderDraftHelp : Decoder DocumentDraft
decoderDraftHelp =
    Decode.succeed DocumentDraft
        |> Decode.custom Decode.iri
        |> Decode.custom
            (Decode.succeed DocumentDraftRdfs
                |> Decode.optionalStringOrLangString RDFS.label
                |> Decode.optionalStringOrLangString RDFS.comment
            )
        |> Decode.custom
            (Decode.succeed DocumentDraftTerms
                |> Decode.required DCTERMS.modified Decode.dateTime
                |> Decode.optional DCTERMS.source (Decode.many Decode.iri) []
                |> Decode.optional DCTERMS.conformsTo (Decode.map Just decoderDocumentVersionReference) Nothing
            )
        |> Decode.custom
            (Decode.succeed DocumentDraftProv
                |> Decode.required PROV.generatedAtTime Decode.dateTime
                |> Decode.required PROV.wasAttributedTo decoderAuthor
            )


decoderVersions : Decoder (List DocumentVersion)
decoderVersions =
    Decode.predicate a (Decode.many Decode.iri)
        |> Decode.andThen
            (\classes ->
                if List.member (herbie "DocumentVersion") classes then
                    Decode.map Just decoderVersion

                else
                    Decode.succeed Nothing
            )
        |> Decode.many
        |> Decode.map
            (List.filterMap identity
                >> List.sortBy
                    (.publishedAt
                        >> Time.posixToMillis
                        >> (*) -1
                    )
            )


decoderVersion : Decoder DocumentVersion
decoderVersion =
    Decode.succeed DocumentVersion
        |> Decode.custom Decode.iri
        |> Decode.custom decoderVersionIndex
        |> Decode.requiredAt pathStartedAtTime Decode.dateTime
        |> Decode.requiredAt pathEndedAtTime Decode.dateTime
        |> Decode.requiredAt pathWasEndedBy decoderAuthor
        |> Decode.optionalAt (Rdf.InversePath (Rdf.PredicatePath RDFS.isDefinedBy)) decoderOntologies []
        |> Decode.optionalAt (Rdf.InversePath (Rdf.PredicatePath RDFS.isDefinedBy)) decoderNodeShapes []
        |> Decode.custom
            (Decode.succeed DocumentVersionRdfs
                |> Decode.optionalStringOrLangString RDFS.label
                |> Decode.optionalStringOrLangString RDFS.comment
            )
        |> Decode.custom
            (Decode.succeed DocumentVersionTerms
                |> Decode.optional DCTERMS.source (Decode.many Decode.iri) []
                |> Decode.optional DCTERMS.conformsTo (Decode.map Just decoderDocumentVersionReference) Nothing
            )
        |> Decode.custom
            (Decode.succeed DocumentVersionSchema
                |> Decode.optional (schema "hasPart") (Decode.many decoderDocumentVersionReference) []
            )


decoderVersionIndex : Decoder Int
decoderVersionIndex =
    let
        indexFromIri iri =
            case
                iri
                    |> Rdf.toUrl
                    |> String.split "/"
                    |> List.reverse
                    |> List.getAt 1
                    |> Maybe.andThen String.toInt
            of
                Nothing ->
                    Decode.fail ("Could not extract version index from IRI <" ++ Rdf.toUrl iri ++ ">.")

                Just index ->
                    Decode.succeed index
    in
    Decode.andThen indexFromIri Decode.iri


decoderDocumentVersionReference : Decoder DocumentVersionReference
decoderDocumentVersionReference =
    Decode.succeed DocumentVersionReference
        |> Decode.custom Decode.iri
        |> Decode.custom
            (Decode.succeed DocumentVersionReferenceRdfs
                |> Decode.optionalStringOrLangString RDFS.label
            )


decoderOntologies : Decoder (List Iri)
decoderOntologies =
    Decode.succeed Tuple.pair
        |> Decode.custom Decode.iri
        |> Decode.required a (Decode.many Decode.iri)
        |> Decode.andThen
            (\( iri, classes ) ->
                if List.member (owl "Ontology") classes then
                    Decode.succeed (Just iri)

                else
                    Decode.succeed Nothing
            )
        |> Decode.many
        |> Decode.map (List.filterMap identity)


decoderNodeShapes : Decoder (List Iri)
decoderNodeShapes =
    Decode.succeed Tuple.pair
        |> Decode.custom Decode.iri
        |> Decode.required a (Decode.many Decode.iri)
        |> Decode.andThen
            (\( iri, classes ) ->
                if List.member (sh "NodeShape") classes then
                    Decode.succeed (Just iri)

                else
                    Decode.succeed Nothing
            )
        |> Decode.many
        |> Decode.map (List.filterMap identity)


decoderAuthor : Decoder Author
decoderAuthor =
    Decode.succeed Author
        |> Decode.custom Decode.iri
        |> Decode.custom
            (Decode.succeed AuthorRdfs
                |> Decode.required RDFS.label Decode.stringOrLangString
            )


pathStartedAtTime : PropertyPath
pathStartedAtTime =
    Rdf.SequencePath
        (Rdf.InversePath (Rdf.PredicatePath PROV.generated))
        [ Rdf.PredicatePath PROV.startedAtTime ]


pathEndedAtTime : PropertyPath
pathEndedAtTime =
    Rdf.SequencePath
        (Rdf.InversePath (Rdf.PredicatePath PROV.generated))
        [ Rdf.PredicatePath PROV.endedAtTime ]


pathWasEndedBy : PropertyPath
pathWasEndedBy =
    Rdf.SequencePath
        (Rdf.InversePath (Rdf.PredicatePath PROV.generated))
        [ Rdf.PredicatePath PROV.wasEndedBy ]


type alias Access =
    { visibleToEveryone : Bool
    }


type alias Rule =
    { this : BlankNodeOrIri
    , hac : RuleHac
    }


type alias RuleHac =
    { role : Iri
    , target : Iri
    , action : Iri
    }


decoderRule : Decoder Rule
decoderRule =
    Decode.succeed Rule
        |> Decode.custom Decode.blankNodeOrIri
        |> Decode.custom
            (Decode.succeed RuleHac
                |> Decode.required (hac "role") Decode.iri
                |> Decode.required (hac "target") Decode.iri
                |> Decode.required (hac "action") Decode.iri
            )


accessFromGraph : Document -> Graph -> Access
accessFromGraph document graph =
    { visibleToEveryone =
        Decode.decode (Decode.fromInstancesOf (hac "Rule") decoderRule) graph
            |> Result.withDefault []
            |> List.any
                (\rule ->
                    (rule.hac.role == hac "AuthenticatedAuthor")
                        && (rule.hac.target == document.this)
                        && (rule.hac.action == hac "Visible")
                )
    }


accessToGraph : Document -> Access -> Graph
accessToGraph document access =
    if access.visibleToEveryone then
        Rdf.emptyGraph
            |> Rdf.insert (Rdf.blankNode "rule") a (hac "Rule")
            |> Rdf.insert (Rdf.blankNode "rule") (hac "role") (hac "AuthenticatedAuthor")
            |> Rdf.insert (Rdf.blankNode "rule") (hac "target") document.this
            |> Rdf.insert (Rdf.blankNode "rule") (hac "action") (hac "Visible")

    else
        Rdf.emptyGraph
