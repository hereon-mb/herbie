module Ontology.Instance exposing
    ( Instance, InstanceRdfs
    , IsDefinedBy, IsDefinedByRdfs, IsDefinedByTerms, IsDefinedByProv
    , ConformsTo, ConformsToRdfs
    , SeeAlso, SeeAlsoRdfs
    , decoder
    )

{-|

@docs Instance, InstanceRdfs
@docs IsDefinedBy, IsDefinedByRdfs, IsDefinedByTerms, IsDefinedByProv
@docs ConformsTo, ConformsToRdfs
@docs SeeAlso, SeeAlsoRdfs

@docs decoder, decoderDetails

-}

import List.NonEmpty exposing (NonEmpty)
import Ontology.Class as Class exposing (Class)
import Ontology.Herbie as Herbie
import Rdf exposing (Iri, StringOrLangString)
import Rdf.Decode as Decode exposing (Decoder)
import Rdf.Decode.Extra as Decode
import Rdf.Namespaces as Rdf
import Rdf.Namespaces.DCTERMS as DCTERMS
import Rdf.Namespaces.PROV as PROV
import Rdf.Namespaces.RDFS as RDFS


type alias Instance =
    { this : Iri
    , a : NonEmpty Class
    , rdfs : InstanceRdfs
    }


type alias InstanceRdfs =
    { label : Maybe StringOrLangString
    , comment : Maybe StringOrLangString
    , isDefinedBy : Maybe IsDefinedBy
    , seeAlso : List SeeAlso
    }


type alias IsDefinedBy =
    { this : Iri
    , rdfs : IsDefinedByRdfs
    , terms : IsDefinedByTerms
    , prov : IsDefinedByProv
    }


type alias IsDefinedByRdfs =
    { label : Maybe StringOrLangString
    , comment : Maybe StringOrLangString
    }


type alias IsDefinedByTerms =
    { conformsTo : Maybe ConformsTo
    }


type alias IsDefinedByProv =
    { specializationOf : Herbie.Document
    }


type alias ConformsTo =
    { this : Iri
    , rdfs : ConformsToRdfs
    }


type alias ConformsToRdfs =
    { label : Maybe StringOrLangString
    , comment : Maybe StringOrLangString
    }


type alias SeeAlso =
    { this : Iri
    , rdfs : SeeAlsoRdfs
    }


type alias SeeAlsoRdfs =
    { label : Maybe StringOrLangString
    }


decoder : Decoder Instance
decoder =
    Decode.succeed Instance
        |> Decode.custom Decode.iri
        |> Decode.required Rdf.a (Decode.oneOrMore Class.decoder)
        |> Decode.custom
            (Decode.succeed InstanceRdfs
                |> Decode.optionalStringOrLangString RDFS.label
                |> Decode.optionalStringOrLangString RDFS.comment
                |> Decode.custom (Decode.zeroOrOne RDFS.isDefinedBy decoderIsDefinedBy)
                |> Decode.custom (Decode.zeroOrMany RDFS.seeAlso decoderSeeAlso)
            )


decoderIsDefinedBy : Decoder IsDefinedBy
decoderIsDefinedBy =
    Decode.succeed IsDefinedBy
        |> Decode.custom Decode.iri
        |> Decode.custom
            (Decode.succeed IsDefinedByRdfs
                |> Decode.optionalStringOrLangString RDFS.label
                |> Decode.optionalStringOrLangString RDFS.comment
            )
        |> Decode.custom
            (Decode.succeed IsDefinedByTerms
                |> Decode.optional DCTERMS.conformsTo (Decode.map Just decoderConformsTo) Nothing
            )
        |> Decode.custom
            (Decode.succeed IsDefinedByProv
                |> Decode.required PROV.specializationOf Herbie.decoderDocument
            )


decoderConformsTo : Decoder ConformsTo
decoderConformsTo =
    Decode.succeed ConformsTo
        |> Decode.custom Decode.iri
        |> Decode.custom
            (Decode.succeed ConformsToRdfs
                |> Decode.optionalStringOrLangString RDFS.label
                |> Decode.optionalStringOrLangString RDFS.comment
            )


decoderSeeAlso : Decoder SeeAlso
decoderSeeAlso =
    Decode.succeed SeeAlso
        |> Decode.custom Decode.iri
        |> Decode.custom
            (Decode.succeed SeeAlsoRdfs
                |> Decode.optionalStringOrLangString RDFS.label
            )
