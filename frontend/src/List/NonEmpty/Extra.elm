module List.NonEmpty.Extra exposing (resultCombine)

import List.NonEmpty exposing (NonEmpty)
import Result.Extra as Result


resultCombine : NonEmpty (Result x a) -> Result x (NonEmpty a)
resultCombine ( first, rest ) =
    Result.map2 Tuple.pair first (Result.combine rest)
