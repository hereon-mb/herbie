{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Fluent exposing
    ( toId
    , text
    , textWith, Argument, int, posix
    , verbatim
    , Bundles, provider
    , Fluent, decoder, encode, toElement
    )

{-|


# Text

@docs FluentoElement, toId
@docs text


# Text with arguments

@docs textWith, Argument, int, posix
@docs verbatim


# Combining text


# Provider

@docs Bundles, provider

-}

import Element exposing (Element)
import Html exposing (Html)
import Html.Attributes
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Decode
import Json.Encode as Encode exposing (Value)
import String.Extra as String
import Time


{-| TODO
-}
type alias Bundles =
    Value


{-| TODO
-}
type Fluent
    = Fluent String (List Argument)
    | Verbatim String
    | Concat (List Fluent)
    | ConcatRow (List Fluent)


{-| TODO
-}
type Argument
    = ArgumentString String String
    | ArgumentPosix String Time.Posix
    | ArgumentInt String Int
    | ArgumentFloat String Float


{-| TODO
-}
posix : String -> Time.Posix -> Argument
posix =
    ArgumentPosix


{-| TODO
-}
int : String -> Int -> Argument
int =
    ArgumentInt


{-| TODO
-}
provider : Bundles -> List (Html msg) -> Html msg
provider bundles nodes =
    Html.node "fluent-provider"
        [ Html.Attributes.property "bundles" bundles ]
        nodes


{-| TODO
-}
text : String -> Fluent
text id =
    Fluent id []


{-| TODO
-}
textWith : List Argument -> String -> Fluent
textWith arguments id =
    Fluent id arguments


{-| TODO
-}
verbatim : String -> Fluent
verbatim =
    Verbatim


{-| TODO
-}
toElement : Fluent -> Element msg
toElement fluent =
    case fluent of
        Fluent id arguments ->
            Element.html
                (Html.node "fluent-text"
                    [ Html.Attributes.attribute "messageId" id
                    , Html.Attributes.property "args"
                        (Encode.object (List.map encodeArgument arguments))
                    ]
                    []
                )

        Verbatim s ->
            Element.text s

        Concat fluents ->
            Element.paragraph []
                (List.map toElement fluents)

        ConcatRow fluents ->
            Element.row [] (List.map toElement fluents)


encodeArgument : Argument -> ( String, Value )
encodeArgument argument =
    case argument of
        ArgumentString name stringValue ->
            ( name, Encode.string stringValue )

        ArgumentPosix name posixValue ->
            ( name, Encode.int (Time.posixToMillis posixValue) )

        ArgumentInt name intValue ->
            ( name, Encode.int intValue )

        ArgumentFloat name floatValue ->
            ( name, Encode.float floatValue )


toId : Fluent -> String
toId fluent =
    case fluent of
        Fluent id _ ->
            String.dasherize id

        Verbatim s ->
            String.dasherize s

        Concat fluents ->
            fluents
                |> List.map toId
                |> String.join "--"

        ConcatRow fluents ->
            fluents
                |> List.map toId
                |> String.join "--"


decoder : Decoder Fluent
decoder =
    let
        help kind =
            case kind of
                "fluent" ->
                    Decode.succeed Fluent
                        |> Decode.required "id" Decode.string
                        |> Decode.required "arguments" (Decode.list argumentDecoder)

                "verbatim" ->
                    Decode.succeed Verbatim
                        |> Decode.required "value" Decode.string

                "concat" ->
                    Decode.succeed Concat
                        |> Decode.required "fluents" (Decode.list (Decode.lazy (\_ -> decoder)))

                "concat-row" ->
                    Decode.succeed ConcatRow
                        |> Decode.required "fluents" (Decode.list (Decode.lazy (\_ -> decoder)))

                _ ->
                    Decode.fail ("unknown kind '" ++ kind ++ "'")
    in
    Decode.andThen help (Decode.field "kind" Decode.string)


argumentDecoder : Decoder Argument
argumentDecoder =
    let
        help kind =
            case kind of
                "argument-string" ->
                    Decode.succeed ArgumentString
                        |> Decode.required "name" Decode.string
                        |> Decode.required "value" Decode.string

                "argument-posix" ->
                    Decode.succeed ArgumentPosix
                        |> Decode.required "name" Decode.string
                        |> Decode.required "value" (Decode.map Time.millisToPosix Decode.int)

                "argument-int" ->
                    Decode.succeed ArgumentInt
                        |> Decode.required "name" Decode.string
                        |> Decode.required "value" Decode.int

                "argument-float" ->
                    Decode.succeed ArgumentFloat
                        |> Decode.required "name" Decode.string
                        |> Decode.required "value" Decode.float

                _ ->
                    Decode.fail ("unknown kind '" ++ kind ++ "'")
    in
    Decode.andThen help (Decode.field "kind" Decode.string)


encode : Fluent -> Value
encode fluent =
    case fluent of
        Fluent id arguments ->
            Encode.object
                [ ( "kind", Encode.string "fluent" )
                , ( "id", Encode.string id )
                , ( "arguments", Encode.list encodeArgumentHelp arguments )
                ]

        Verbatim s ->
            Encode.object
                [ ( "kind", Encode.string "verbatim" )
                , ( "value", Encode.string s )
                ]

        Concat fluents ->
            Encode.object
                [ ( "kind", Encode.string "concat" )
                , ( "fluents", Encode.list encode fluents )
                ]

        ConcatRow fluents ->
            Encode.object
                [ ( "kind", Encode.string "concat-row" )
                , ( "fluents", Encode.list encode fluents )
                ]


encodeArgumentHelp : Argument -> Value
encodeArgumentHelp argument =
    case argument of
        ArgumentString name stringValue ->
            Encode.object
                [ ( "kind", Encode.string "argument-string" )
                , ( "name", Encode.string name )
                , ( "value", Encode.string stringValue )
                ]

        ArgumentPosix name posixValue ->
            Encode.object
                [ ( "kind", Encode.string "argument-posix" )
                , ( "name", Encode.string name )
                , ( "value", Encode.int (Time.posixToMillis posixValue) )
                ]

        ArgumentInt name intValue ->
            Encode.object
                [ ( "kind", Encode.string "argument-int" )
                , ( "name", Encode.string name )
                , ( "value", Encode.int intValue )
                ]

        ArgumentFloat name floatValue ->
            Encode.object
                [ ( "kind", Encode.string "argument-float" )
                , ( "name", Encode.string name )
                , ( "value", Encode.float floatValue )
                ]
