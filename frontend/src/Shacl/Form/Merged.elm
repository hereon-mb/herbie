module Shacl.Form.Merged exposing
    ( DataEditorGrid
    , DataEditorSchedule
    , DataShapeNode
    , DataShapeProperty
    , Editor(..)
    , Error(..)
    , ShapeNode(..)
    , ShapeProperty(..)
    , errorToString
    , fromCollected
    )

{-| This modules merges node and property shapes.

The following document (A) should, after merging, be equivalent to document
(B). In fact, this modules transforms (A) into (B).

**Document (A)**

```turtle
sh:property [
    sh:path <merge> ;
    sh:node [
        sh:property [
            sh:path <leaf> ;
            sh:node [
                sh:property [
                    sh:path <nestedA> ;
                ] ;
            ] ;
        ] ;
    ] ;
    sh:node [
        sh:property [
            sh:path <leaf> ;
            sh:node [
                sh:property [
                    sh:path <nestedB> ;
                ] ;
            ] ;
        ] ;
    ] ;
] ;
```

**Document B**

```turtle
sh:property [
    sh:node [
        sh:property [
            sh:path <leaf> ;
            sh:node [
                sh:property [
                    sh:path <nestedA> ;
                ] ;
                sh:property [
                    sh:path <nestedB> ;
                ] ;
            ] ;
        ] ;
    ] ;
] ;
```

Merging is implemented as a single pass over the root node shape (A). The
algorithm proceeds top-to-bottom, and carries out the following steps.

1.  replaces the property shapes of the current node with the merged property
    shapes,
2.  recurses into each property shape,
3.  replaces the node shapes of the current property shape with the merged node
    shapes,
4.  recurses into each node shape.
5.  repeat

-}

import Maybe.Extra as Maybe
import Rdf exposing (AnyLiteral, BlankNodeOrIriOrAnyLiteral, Iri, StringOrLangString)
import Rdf.PropertyPath exposing (PropertyPath)
import Result.Extra as Result
import Result.Pipeline as Result
import Shacl
import Shacl.Form.Collected as Collected
import Shacl.OrderBy exposing (OrderBy)


type ShapeNode
    = ShapeNode DataShapeNode


type alias DataShapeNode =
    { nodeShapes : List Shacl.NodeShape
    , targetClasses : List Iri
    , classes : List Iri
    , hasValue : List Shacl.Value
    , ins : List Shacl.Value
    , properties : List ShapeProperty
    , ors : List (List ShapeNode)
    , nodes : List ShapeNode
    , names : List StringOrLangString
    , labels : List StringOrLangString
    , comments : List StringOrLangString
    }


type ShapeProperty
    = ShapeProperty DataShapeProperty


type alias DataShapeProperty =
    { propertyShapes : List Shacl.PropertyShape
    , path : PropertyPath
    , nodes : List ShapeNode
    , qualifiedNode : Maybe ShapeNode
    , editor : Maybe Editor
    , names : List StringOrLangString
    , datatype : Maybe Iri
    , singleLine : Maybe Bool
    , detailsEditorOrderPath : Maybe PropertyPath
    , viewer : Maybe Shacl.Viewer
    , hasValue : List Shacl.Value
    , ins : List Shacl.Value
    , group : Maybe Iri
    , order : Maybe Float
    , descriptions : List StringOrLangString
    , labelInCard : Maybe Shacl.ConfigLabel
    , minExclusive : Maybe AnyLiteral
    , minInclusive : Maybe AnyLiteral
    , maxExclusive : Maybe AnyLiteral
    , maxInclusive : Maybe AnyLiteral
    , minCount : List Int
    , maxCount : List Int
    , readonly : Maybe Bool
    , generated : Maybe Bool
    , persisted : Maybe Bool
    , nodeKind : Maybe Shacl.NodeKind
    , classes : List Iri
    , optionalViaCheckbox : Maybe Bool
    , defaultValue : List BlankNodeOrIriOrAnyLiteral
    , includes : List Iri
    , orderBy : Maybe OrderBy
    , qualifiedMinCount : Maybe Int
    , qualifiedMaxCount : Maybe Int
    }


type Editor
    = EditorDetails
    | EditorInstanceSelect
    | EditorRadioButtons
    | EditorGrid DataEditorGrid
    | EditorSchedule DataEditorSchedule


type alias DataEditorGrid =
    { availableColumnsPath : PropertyPath
    , availableRowsPath : PropertyPath
    , columnPath : Iri
    , rowPath : Iri
    , cellViewer : Maybe ShapeNode
    , cellEditor : Maybe ShapeNode
    }


type alias DataEditorSchedule =
    { hashScheduleEditorDayOffsetPath : PropertyPath
    , hashScheduleEditorOrderPath : PropertyPath
    , hashScheduleEditorSamples : List Iri
    , hashScheduleEditorTitlePath : PropertyPath
    , hashScheduleEditorDatePath : PropertyPath
    }


{-| -}
type Error
    = UnmergableShapeProperty (List Shacl.PropertyShape) (List Shacl.PropertyShape) String
    | UnmergableNodeShape


{-| -}
errorToString : Error -> String
errorToString error =
    case error of
        UnmergableShapeProperty shapePropertiesA shapePropertiesB unmergableProperty ->
            "I cannot merge the values of `"
                ++ unmergableProperty
                ++ "' in the to merge the shape property "
                ++ propertyShapeNodes shapePropertiesA
                ++ " with the shape property "
                ++ propertyShapeNodes shapePropertiesB
                ++ "."

        UnmergableNodeShape ->
            "I cannot merge some node shapes"


propertyShapeNodes : List Shacl.PropertyShape -> String
propertyShapeNodes propertyShapes =
    propertyShapes
        |> List.map (.node >> Rdf.serializeNode)
        |> String.join ", "


{-| -}
fromCollected : Collected.ShapeNode -> Result Error ShapeNode
fromCollected =
    fromShapeNode
        >> Result.andThen mergeRecShapeNode


fromShapeNode : Collected.ShapeNode -> Result Error ShapeNode
fromShapeNode (Collected.ShapeNode data) =
    Ok DataShapeNode
        |> Result.hardcoded [ data.nodeShape ]
        |> Result.hardcoded data.nodeShape.shTargetClass
        |> Result.hardcoded data.nodeShape.shClass
        |> Result.hardcoded data.nodeShape.shHasValue
        |> Result.hardcoded data.nodeShape.shIn
        |> Result.required
            (data.shapeProperties
                |> List.map fromShapeProperty
                |> Result.combine
            )
        |> Result.required
            (data.ors
                |> List.map (List.map fromShapeNode >> Result.combine)
                |> Result.combine
            )
        |> Result.required
            (data.nodes
                |> List.map fromShapeNode
                |> Result.combine
            )
        |> Result.hardcoded (Maybe.toList data.nodeShape.shName)
        |> Result.hardcoded (Maybe.toList data.nodeShape.rdfsLabel)
        |> Result.hardcoded (Maybe.toList data.nodeShape.rdfsComment)
        |> Result.map ShapeNode


fromShapeProperty : Collected.ShapeProperty -> Result Error ShapeProperty
fromShapeProperty (Collected.ShapeProperty data) =
    Ok DataShapeProperty
        |> Result.hardcoded [ data.propertyShape ]
        |> Result.hardcoded data.propertyShape.shPath
        |> Result.required
            (data.nodes
                |> List.map fromShapeNode
                |> Result.combine
            )
        |> Result.required
            (data.qualifiedNode
                |> Maybe.map (fromShapeNode >> Result.map Just)
                |> Maybe.withDefault (Ok Nothing)
            )
        |> Result.required
            (data.editor
                |> Maybe.map (fromEditor >> Result.map Just)
                |> Maybe.withDefault (Ok Nothing)
            )
        |> Result.hardcoded (Maybe.toList data.propertyShape.shName)
        |> Result.hardcoded data.propertyShape.shDatatype
        |> Result.hardcoded data.propertyShape.dashSingleLine
        |> Result.hardcoded data.propertyShape.hashDetailsEditorOrderPath
        |> Result.hardcoded data.propertyShape.dashViewer
        |> Result.hardcoded data.propertyShape.shHasValue
        |> Result.hardcoded data.propertyShape.shIn
        |> Result.hardcoded data.propertyShape.shGroup
        |> Result.hardcoded data.propertyShape.shOrder
        |> Result.hardcoded (Maybe.toList data.propertyShape.shDescription)
        |> Result.hardcoded data.propertyShape.hashLabelInCard
        |> Result.hardcoded data.propertyShape.shMinExclusive
        |> Result.hardcoded data.propertyShape.shMinInclusive
        |> Result.hardcoded data.propertyShape.shMaxExclusive
        |> Result.hardcoded data.propertyShape.shMaxInclusive
        |> Result.hardcoded data.propertyShape.shMinCount
        |> Result.hardcoded data.propertyShape.shMaxCount
        |> Result.hardcoded data.propertyShape.hashReadonly
        |> Result.hardcoded data.propertyShape.hashGenerated
        |> Result.hardcoded data.propertyShape.hashPersisted
        |> Result.hardcoded data.propertyShape.shNodeKind
        |> Result.hardcoded data.propertyShape.shClass
        |> Result.hardcoded data.propertyShape.hashOptionalViaCheckbox
        |> Result.hardcoded data.propertyShape.shDefaultValue
        |> Result.hardcoded data.propertyShape.hashInclude
        |> Result.hardcoded data.propertyShape.hashOrderBy
        |> Result.hardcoded data.propertyShape.shQualifiedMinCount
        |> Result.hardcoded data.propertyShape.shQualifiedMaxCount
        |> Result.map ShapeProperty


fromEditor : Collected.Editor -> Result Error Editor
fromEditor editor =
    case editor of
        Collected.EditorDetails ->
            Ok EditorDetails

        Collected.EditorInstanceSelect ->
            Ok EditorInstanceSelect

        Collected.EditorRadioButtons ->
            Ok EditorRadioButtons

        Collected.EditorGrid data ->
            Ok DataEditorGrid
                |> Result.hardcoded data.availableColumnsPath
                |> Result.hardcoded data.availableRowsPath
                |> Result.hardcoded data.columnPath
                |> Result.hardcoded data.rowPath
                |> Result.required
                    (data.cellViewer
                        |> Maybe.map (fromShapeNode >> Result.map Just)
                        |> Maybe.withDefault (Ok Nothing)
                    )
                |> Result.required
                    (data.cellEditor
                        |> Maybe.map (fromShapeNode >> Result.map Just)
                        |> Maybe.withDefault (Ok Nothing)
                    )
                |> Result.map EditorGrid

        Collected.EditorSchedule data ->
            Ok DataEditorSchedule
                |> Result.hardcoded data.hashScheduleEditorDayOffsetPath
                |> Result.hardcoded data.hashScheduleEditorOrderPath
                |> Result.hardcoded data.hashScheduleEditorSamples
                |> Result.hardcoded data.hashScheduleEditorTitlePath
                |> Result.hardcoded data.hashScheduleEditorDatePath
                |> Result.map EditorSchedule


mergeRecShapeNode : ShapeNode -> Result Error ShapeNode
mergeRecShapeNode =
    mergeRecShapeNodeAnds
        >> Result.andThen mergeRecShapeNodeProperties


mergeRecShapeNodeAnds : ShapeNode -> Result Error ShapeNode
mergeRecShapeNodeAnds ((ShapeNode { nodes }) as shapeNode) =
    -- FIXME This does not resolve nested sh:node's
    mergeBy mergeShapeNode (shapeNode :: nodes)
        |> Result.andThen (List.head >> Result.fromMaybe UnmergableNodeShape)


mergeRecShapeNodeProperties : ShapeNode -> Result Error ShapeNode
mergeRecShapeNodeProperties (ShapeNode shapeNode) =
    shapeNode.properties
        |> mergeShapeProperties
        |> Result.andThen (List.map mergeRecShapeProperty >> Result.combine)
        |> Result.map (\properties -> ShapeNode { shapeNode | properties = properties })


mergeRecShapeProperty : ShapeProperty -> Result Error ShapeProperty
mergeRecShapeProperty (ShapeProperty shapeProperty) =
    shapeProperty.nodes
        |> mergeShapeNodes
        |> Result.andThen (List.map mergeRecShapeNode >> Result.combine)
        |> Result.map (\nodes -> ShapeProperty { shapeProperty | nodes = nodes })


mergeShapeNodes : List ShapeNode -> Result Error (List ShapeNode)
mergeShapeNodes =
    mergeBy mergeShapeNode


mergeShapeNode : ShapeNode -> ShapeNode -> Result Error (Maybe ShapeNode)
mergeShapeNode (ShapeNode shapeNodeA) (ShapeNode shapeNodeB) =
    Ok DataShapeNode
        |> Result.hardcoded (shapeNodeA.nodeShapes ++ shapeNodeB.nodeShapes)
        |> Result.hardcoded (shapeNodeA.targetClasses ++ shapeNodeB.targetClasses)
        |> Result.hardcoded (shapeNodeA.classes ++ shapeNodeB.classes)
        |> Result.hardcoded (shapeNodeA.hasValue ++ shapeNodeB.hasValue)
        |> Result.hardcoded (shapeNodeA.ins ++ shapeNodeB.ins)
        |> Result.hardcoded (shapeNodeA.properties ++ shapeNodeB.properties)
        |> Result.hardcoded (shapeNodeA.ors ++ shapeNodeB.ors)
        |> Result.hardcoded (shapeNodeA.nodes ++ shapeNodeB.nodes)
        |> Result.hardcoded (shapeNodeA.names ++ shapeNodeB.names)
        |> Result.hardcoded (shapeNodeA.labels ++ shapeNodeB.labels)
        |> Result.hardcoded (shapeNodeA.comments ++ shapeNodeB.comments)
        |> Result.map (Just << ShapeNode)


mergeShapeProperties : List ShapeProperty -> Result Error (List ShapeProperty)
mergeShapeProperties =
    mergeBy mergeShapeProperty


mergeShapeProperty : ShapeProperty -> ShapeProperty -> Result Error (Maybe ShapeProperty)
mergeShapeProperty (ShapeProperty shapePropertyA) (ShapeProperty shapePropertyB) =
    let
        unmergable =
            Err << UnmergableShapeProperty shapePropertyA.propertyShapes shapePropertyB.propertyShapes
    in
    if shapePropertyA.path /= shapePropertyB.path then
        Ok Nothing

    else if Maybe.isJust shapePropertyA.qualifiedNode && Maybe.isJust shapePropertyB.qualifiedNode && (shapePropertyA.qualifiedNode /= shapePropertyB.qualifiedNode) then
        Ok Nothing

    else if Maybe.isJust shapePropertyA.editor && Maybe.isJust shapePropertyB.editor && (shapePropertyA.editor /= shapePropertyB.editor) then
        unmergable "editor"

    else if Maybe.isJust shapePropertyA.datatype && Maybe.isJust shapePropertyB.datatype && (shapePropertyA.datatype /= shapePropertyB.datatype) then
        unmergable "datatype"

    else if Maybe.isJust shapePropertyA.singleLine && Maybe.isJust shapePropertyB.singleLine && (shapePropertyA.singleLine /= shapePropertyB.singleLine) then
        unmergable "singleLine"

    else if Maybe.isJust shapePropertyA.detailsEditorOrderPath && Maybe.isJust shapePropertyB.detailsEditorOrderPath && (shapePropertyA.detailsEditorOrderPath /= shapePropertyB.detailsEditorOrderPath) then
        unmergable "detailsEditorOrderPath"

    else if Maybe.isJust shapePropertyA.viewer && Maybe.isJust shapePropertyB.viewer && (shapePropertyA.viewer /= shapePropertyB.viewer) then
        unmergable "viewer"

    else if Maybe.isJust shapePropertyA.group && Maybe.isJust shapePropertyB.group && (shapePropertyA.group /= shapePropertyB.group) then
        unmergable "group"

    else if Maybe.isJust shapePropertyA.order && Maybe.isJust shapePropertyB.order && (shapePropertyA.order /= shapePropertyB.order) then
        unmergable "order"

    else if Maybe.isJust shapePropertyA.labelInCard && Maybe.isJust shapePropertyB.labelInCard && (shapePropertyA.labelInCard /= shapePropertyB.labelInCard) then
        unmergable "labelInCard"

    else if Maybe.isJust shapePropertyA.minExclusive && Maybe.isJust shapePropertyB.minExclusive && (shapePropertyA.minExclusive /= shapePropertyB.minExclusive) then
        unmergable "minExclusive"

    else if Maybe.isJust shapePropertyA.minInclusive && Maybe.isJust shapePropertyB.minInclusive && (shapePropertyA.minInclusive /= shapePropertyB.minInclusive) then
        unmergable "minInclusive"

    else if Maybe.isJust shapePropertyA.maxExclusive && Maybe.isJust shapePropertyB.maxExclusive && (shapePropertyA.maxExclusive /= shapePropertyB.maxExclusive) then
        unmergable "maxExclusive"

    else if Maybe.isJust shapePropertyA.maxInclusive && Maybe.isJust shapePropertyB.maxInclusive && (shapePropertyA.maxInclusive /= shapePropertyB.maxInclusive) then
        unmergable "maxInclusive"

    else if Maybe.isJust shapePropertyA.readonly && Maybe.isJust shapePropertyB.readonly && (shapePropertyA.readonly /= shapePropertyB.readonly) then
        unmergable "readonly"

    else if Maybe.isJust shapePropertyA.generated && Maybe.isJust shapePropertyB.generated && (shapePropertyA.generated /= shapePropertyB.generated) then
        unmergable "generated"

    else if Maybe.isJust shapePropertyA.persisted && Maybe.isJust shapePropertyB.persisted && (shapePropertyA.persisted /= shapePropertyB.persisted) then
        unmergable "persisted"

    else if Maybe.isJust shapePropertyA.nodeKind && Maybe.isJust shapePropertyB.nodeKind && (shapePropertyA.nodeKind /= shapePropertyB.nodeKind) then
        unmergable "nodeKind"

    else if Maybe.isJust shapePropertyA.optionalViaCheckbox && Maybe.isJust shapePropertyB.optionalViaCheckbox && (shapePropertyA.optionalViaCheckbox /= shapePropertyB.optionalViaCheckbox) then
        unmergable "optionalViaCheckbox"

    else if Maybe.isJust shapePropertyA.orderBy && Maybe.isJust shapePropertyB.orderBy && (shapePropertyA.orderBy /= shapePropertyB.orderBy) then
        unmergable "orderBy"

    else if Maybe.isJust shapePropertyA.qualifiedMinCount && Maybe.isJust shapePropertyB.qualifiedMinCount && (shapePropertyA.qualifiedMinCount /= shapePropertyB.qualifiedMinCount) then
        unmergable "qualifiedMinCount"

    else if Maybe.isJust shapePropertyA.qualifiedMaxCount && Maybe.isJust shapePropertyB.qualifiedMaxCount && (shapePropertyA.qualifiedMaxCount /= shapePropertyB.qualifiedMaxCount) then
        unmergable "qualifiedMaxCount"

    else
        Ok DataShapeProperty
            |> Result.hardcoded (shapePropertyA.propertyShapes ++ shapePropertyB.propertyShapes)
            |> Result.hardcoded shapePropertyA.path
            |> Result.hardcoded (shapePropertyA.nodes ++ shapePropertyB.nodes)
            |> Result.hardcoded (Maybe.or shapePropertyA.qualifiedNode shapePropertyB.qualifiedNode)
            |> Result.hardcoded (Maybe.or shapePropertyA.editor shapePropertyB.editor)
            |> Result.hardcoded (shapePropertyA.names ++ shapePropertyB.names)
            |> Result.hardcoded (Maybe.or shapePropertyA.datatype shapePropertyB.datatype)
            |> Result.hardcoded (Maybe.or shapePropertyA.singleLine shapePropertyB.singleLine)
            |> Result.hardcoded (Maybe.or shapePropertyA.detailsEditorOrderPath shapePropertyB.detailsEditorOrderPath)
            |> Result.hardcoded (Maybe.or shapePropertyA.viewer shapePropertyB.viewer)
            |> Result.hardcoded (shapePropertyA.hasValue ++ shapePropertyB.hasValue)
            |> Result.hardcoded (shapePropertyA.ins ++ shapePropertyB.ins)
            |> Result.hardcoded (Maybe.or shapePropertyA.group shapePropertyB.group)
            |> Result.hardcoded (Maybe.or shapePropertyA.order shapePropertyB.order)
            |> Result.hardcoded (shapePropertyA.descriptions ++ shapePropertyB.descriptions)
            |> Result.hardcoded (Maybe.or shapePropertyA.labelInCard shapePropertyB.labelInCard)
            |> Result.hardcoded (Maybe.or shapePropertyA.minExclusive shapePropertyB.minExclusive)
            |> Result.hardcoded (Maybe.or shapePropertyA.minInclusive shapePropertyB.minInclusive)
            |> Result.hardcoded (Maybe.or shapePropertyA.maxExclusive shapePropertyB.maxExclusive)
            |> Result.hardcoded (Maybe.or shapePropertyA.maxInclusive shapePropertyB.maxInclusive)
            |> Result.hardcoded (shapePropertyA.minCount ++ shapePropertyB.minCount)
            |> Result.hardcoded (shapePropertyA.maxCount ++ shapePropertyB.maxCount)
            |> Result.hardcoded (Maybe.or shapePropertyA.readonly shapePropertyB.readonly)
            |> Result.hardcoded (Maybe.or shapePropertyA.generated shapePropertyB.generated)
            |> Result.hardcoded (Maybe.or shapePropertyA.persisted shapePropertyB.persisted)
            |> Result.hardcoded (Maybe.or shapePropertyA.nodeKind shapePropertyB.nodeKind)
            |> Result.hardcoded (shapePropertyA.classes ++ shapePropertyB.classes)
            |> Result.hardcoded (Maybe.or shapePropertyA.optionalViaCheckbox shapePropertyB.optionalViaCheckbox)
            |> Result.hardcoded (shapePropertyA.defaultValue ++ shapePropertyB.defaultValue)
            |> Result.hardcoded (shapePropertyA.includes ++ shapePropertyB.includes)
            |> Result.hardcoded (Maybe.or shapePropertyA.orderBy shapePropertyB.orderBy)
            |> Result.hardcoded (Maybe.or shapePropertyA.qualifiedMinCount shapePropertyB.qualifiedMinCount)
            |> Result.hardcoded (Maybe.or shapePropertyA.qualifiedMaxCount shapePropertyB.qualifiedMaxCount)
            |> Result.map ShapeProperty
            |> Result.map Just


{-| Takes as first argument a merge function with possible outcomes:

  - `Err ...`: The elements should be merged but this would cause conflicts or
    some other error occured.

  - `Ok Nothing`: The elements should not be merged.

  - `Ok (Just ...)`: The fields were merged.

-}
mergeBy : (a -> a -> Result err (Maybe a)) -> List a -> Result err (List a)
mergeBy merge xs =
    mergeByStep merge xs []


mergeByHelp :
    (a -> a -> Result err (Maybe a))
    -> ( a, List a )
    -> List a
    -> List a
    -> Result err (List a)
mergeByHelp merge ( first, rest ) unmerged merged =
    case unmerged of
        [] ->
            mergeByStep merge rest (first :: merged)

        firstUnmerged :: restUnmerged ->
            case merge first firstUnmerged of
                Err e ->
                    Err e

                Ok Nothing ->
                    mergeByHelp merge ( first, rest ) restUnmerged (firstUnmerged :: merged)

                Ok (Just y) ->
                    mergeByStep merge rest (y :: restUnmerged ++ merged)


mergeByStep :
    (a -> a -> Result err (Maybe a))
    -> List a
    -> List a
    -> Result err (List a)
mergeByStep merge xs ys =
    case xs of
        [] ->
            Ok ys

        first :: rest ->
            mergeByHelp merge ( first, rest ) ys []
