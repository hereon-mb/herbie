module Shacl.Form.Compile exposing
    ( run
    , Error, errorToString
    )

{-|

@docs run
@docs Error, errorToString

-}

import Shacl
import Shacl.Form.Canonical as Canonical
import Shacl.Form.Collected as Collected exposing (Spec)
import Shacl.Form.Grouped as Grouped
import Shacl.Form.Merged as Merged
import Shacl.Form.Translated as Translated


type Error
    = ErrorCollected Collected.Error
    | ErrorMerged Merged.Error
    | ErrorTranslated Translated.Error
    | ErrorGrouped Grouped.Error


{-| Compile a SHACL form. This goes in several stages

1.  **Collected:** Collect all node and property shapes starting from `nodeShape`.

2.  **Merged:** Try to merge property shapes for the same property path.

3.  **Translated:** Decide on which input element to generate for each property path.

4.  **Grouped:** Group all input elements.

-}
run : Spec -> Shacl.NodeShape -> Result Error Grouped.DataForm
run spec nodeShape =
    (Collected.fromShacl spec nodeShape |> Result.mapError ErrorCollected)
        |> Result.andThen (Merged.fromCollected >> Result.mapError ErrorMerged)
        |> Result.andThen Canonical.fromMerged
        |> Result.andThen (Translated.fromCanonical spec >> Result.mapError ErrorTranslated)
        |> Result.andThen (Grouped.fromTranslated spec >> Result.mapError ErrorGrouped)


errorToString : Error -> String
errorToString error =
    case error of
        ErrorCollected collected ->
            Collected.errorToString collected

        ErrorMerged merged ->
            Merged.errorToString merged

        ErrorTranslated translated ->
            Translated.errorToString translated

        ErrorGrouped grouped ->
            Grouped.errorToString grouped
