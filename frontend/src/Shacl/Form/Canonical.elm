module Shacl.Form.Canonical exposing
    ( ShapeNode(..), DataShapeNode
    , ShapeProperty(..), DataShapeProperty, DataLeaf, DataNested
    , Editor(..), DataEditorGrid, DataEditorSchedule
    , fromMerged
    , group, order
    )

{-|

@docs ShapeNode, DataShapeNode
@docs ShapeProperty, DataShapeProperty, DataLeaf, DataNested
@docs Editor, DataEditorGrid, DataEditorSchedule

@docs fromMerged

@docs group, order

-}

import Rdf exposing (AnyLiteral, BlankNodeOrIriOrAnyLiteral, Iri, StringOrLangString)
import Rdf.Hash as Hash
import Rdf.PropertyPath exposing (PropertyPath)
import Result.Extra as Result
import Result.Pipeline as Result
import Shacl
import Shacl.Form.Merged as Merged
import Shacl.OrderBy exposing (OrderBy)


type ShapeNode
    = ShapeNode DataShapeNode


type alias DataShapeNode =
    { nodeShapes : List Shacl.NodeShape
    , targetClasses : List Iri
    , classes : List Iri
    , values : List Shacl.Value
    , ins : List Shacl.Value
    , properties : List ShapeProperty
    , ors : List (List ShapeNode)
    , names : List StringOrLangString
    , labels : List StringOrLangString
    , comments : List StringOrLangString
    , hash : Int
    }


type ShapeProperty
    = Leaf DataShapeProperty DataLeaf
    | Nested DataShapeProperty DataNested


type alias DataShapeProperty =
    { propertyShapes : List Shacl.PropertyShape
    , path : PropertyPath
    , group : Maybe Iri
    , order : Float
    , names : List StringOrLangString
    , descriptions : List StringOrLangString
    , labelInCard : Shacl.ConfigLabel
    , minExclusive : Maybe AnyLiteral
    , minInclusive : Maybe AnyLiteral
    , maxExclusive : Maybe AnyLiteral
    , maxInclusive : Maybe AnyLiteral
    , minCount : Int
    , maxCount : Int
    , readonly : Bool
    , generated : Bool
    , persisted : Bool
    , nodeKind : Maybe Shacl.NodeKind
    , classes : List Iri
    , -- whether or not this shape property comes from a qualified value shape
      qualified : Bool
    , optionalViaCheckbox : Maybe Bool
    , defaultValue : List BlankNodeOrIriOrAnyLiteral
    , includes : List Iri
    , orderBy : Maybe OrderBy
    , hash : Int
    }


type alias DataLeaf =
    { datatype : Maybe Iri
    , editor : Maybe Editor
    , singleLine : Maybe Bool
    , values : List Shacl.Value
    , ins : List Shacl.Value
    }


type alias DataNested =
    { node : ShapeNode
    , editor : Maybe Editor
    , editorOrderPath : Maybe PropertyPath
    , viewer : Maybe Shacl.Viewer
    }


type Editor
    = EditorDetails
    | EditorInstanceSelect
    | EditorRadioButtons
    | EditorGrid DataEditorGrid
    | EditorSchedule DataEditorSchedule


type alias DataEditorGrid =
    { availableColumnsPath : PropertyPath
    , availableRowsPath : PropertyPath
    , columnPath : Iri
    , rowPath : Iri
    , cellViewer : Maybe ShapeNode
    , cellEditor : Maybe ShapeNode
    }


type alias DataEditorSchedule =
    { hashScheduleEditorDayOffsetPath : PropertyPath
    , hashScheduleEditorOrderPath : PropertyPath
    , hashScheduleEditorSamples : List Iri
    , hashScheduleEditorTitlePath : PropertyPath
    , hashScheduleEditorDatePath : PropertyPath
    }


group : ShapeProperty -> Maybe Iri
group property =
    case property of
        Leaf data _ ->
            data.group

        Nested data _ ->
            data.group


order : ShapeProperty -> Float
order property =
    case property of
        Leaf data _ ->
            data.order

        Nested data _ ->
            data.order


fromMerged : Merged.ShapeNode -> Result err ShapeNode
fromMerged =
    fromShapeNode


fromShapeNode : Merged.ShapeNode -> Result err ShapeNode
fromShapeNode (Merged.ShapeNode data) =
    Ok
        (\properties ors ->
            Ok DataShapeNode
                |> Result.hardcoded data.nodeShapes
                |> Result.hardcoded data.targetClasses
                |> Result.hardcoded data.classes
                |> Result.hardcoded data.hasValue
                |> Result.hardcoded data.ins
                |> Result.hardcoded properties
                |> Result.hardcoded ors
                |> Result.hardcoded data.names
                |> Result.hardcoded data.labels
                |> Result.hardcoded data.comments
                |> Result.hardcoded
                    (Hash.initial
                        |> Hash.iris "sh:targetClass" data.targetClasses
                        |> Hash.iris "sh:class" data.classes
                        |> Hash.combineMany (List.map toDataShapeProperty properties)
                        |> Hash.combineMany (List.concatMap (List.map toDataShapeNode) ors)
                    )
                |> Result.map ShapeNode
        )
        |> Result.required ((Result.combine << List.map fromShapeProperty) data.properties)
        |> Result.required ((Result.combine << List.map (Result.combine << List.map fromShapeNode)) data.ors)
        |> Result.join


fromShapeProperty : Merged.ShapeProperty -> Result err ShapeProperty
fromShapeProperty ((Merged.ShapeProperty ({ nodes, qualifiedNode, editor } as dataShapeProperty)) as shapeProperty) =
    case ( dataShapeProperty.datatype, nodes, qualifiedNode ) of
        ( Nothing, [ nodeCollected ], Nothing ) ->
            propertyFromCollectedHelp
                shapeProperty
                (mkDataShapeProperty shapeProperty)
                nodeCollected
                |> Result.map computeHashShapeProperty

        ( Nothing, [], Just nodeCollected ) ->
            propertyFromCollectedHelp
                shapeProperty
                (mkDataShapeProperty shapeProperty
                    |> modifyDataShapePropertyForQualifiedNode shapeProperty
                )
                nodeCollected
                |> Result.map computeHashShapeProperty

        _ ->
            let
                dataLeaf =
                    Ok DataLeaf
                        |> Result.hardcoded dataShapeProperty.datatype
                        |> Result.required editorMerged
                        |> Result.hardcoded dataShapeProperty.singleLine
                        |> Result.hardcoded dataShapeProperty.hasValue
                        |> Result.hardcoded dataShapeProperty.ins

                editorMerged =
                    case editor of
                        Nothing ->
                            Ok Nothing

                        Just editorCollected ->
                            fromEditor editorCollected
                                |> Result.map Just
            in
            Ok Leaf
                |> Result.hardcoded (mkDataShapeProperty shapeProperty)
                |> Result.required dataLeaf
                |> Result.map computeHashShapeProperty


propertyFromCollectedHelp :
    Merged.ShapeProperty
    -> DataShapeProperty
    -> Merged.ShapeNode
    -> Result err ShapeProperty
propertyFromCollectedHelp (Merged.ShapeProperty dataShapeProperty) data_ ((Merged.ShapeNode dataNodeCollected) as nodeCollected) =
    let
        shapeNodeCollected =
            fromMerged nodeCollected

        editorMerged =
            case dataShapeProperty.editor of
                Nothing ->
                    Ok Nothing

                Just editorCollected ->
                    fromEditor editorCollected
                        |> Result.map Just

        data =
            data_
                |> passClassFromNested nodeCollected

        isNested =
            hasNestedProperties || hasOrs

        hasNestedProperties =
            Result.unwrap True
                (\(ShapeNode { properties }) -> (not << List.isEmpty) properties)
                shapeNodeCollected

        hasOrs =
            case nodeCollected of
                Merged.ShapeNode dataShapeNode ->
                    (not << List.isEmpty) dataShapeNode.ors
    in
    if isNested then
        let
            dataNested =
                Ok DataNested
                    |> Result.required shapeNodeCollected
                    |> Result.required editorMerged
                    |> Result.hardcoded dataShapeProperty.detailsEditorOrderPath
                    |> Result.hardcoded dataShapeProperty.viewer
        in
        Ok Nested
            |> Result.hardcoded data
            |> Result.required dataNested

    else
        let
            dataLeaf =
                Ok DataLeaf
                    |> Result.hardcoded dataShapeProperty.datatype
                    |> Result.required editorMerged
                    |> Result.hardcoded dataShapeProperty.singleLine
                    |> Result.hardcoded (dataShapeProperty.hasValue ++ dataNodeCollected.hasValue)
                    |> Result.hardcoded (dataShapeProperty.ins ++ dataNodeCollected.ins)
        in
        Ok Leaf
            |> Result.hardcoded data
            |> Result.required dataLeaf


mkDataShapeProperty : Merged.ShapeProperty -> DataShapeProperty
mkDataShapeProperty (Merged.ShapeProperty data) =
    { propertyShapes = data.propertyShapes
    , path = data.path
    , group = data.group
    , order = Maybe.withDefault (toFloat infinity) data.order
    , names = data.names
    , descriptions = data.descriptions
    , labelInCard = Maybe.withDefault Shacl.LabelAbove data.labelInCard
    , minExclusive = data.minExclusive
    , minInclusive = data.minInclusive
    , maxExclusive = data.maxExclusive
    , maxInclusive = data.minInclusive
    , minCount = Maybe.withDefault 0 (List.maximum data.minCount)
    , maxCount = Maybe.withDefault infinity (List.minimum data.maxCount)
    , readonly = Maybe.withDefault False data.readonly
    , generated = Maybe.withDefault False data.generated
    , persisted = Maybe.withDefault False data.persisted
    , nodeKind = data.nodeKind
    , classes = data.classes
    , qualified = False
    , optionalViaCheckbox = data.optionalViaCheckbox
    , defaultValue = data.defaultValue
    , includes = data.includes
    , orderBy = data.orderBy
    , hash = -1
    }


computeHashShapeProperty : ShapeProperty -> ShapeProperty
computeHashShapeProperty shapeProperty =
    case shapeProperty of
        Leaf data dataLeaf ->
            let
                iriValues =
                    List.filterMap (.this >> Rdf.toIri) dataLeaf.values

                iriIns =
                    List.filterMap (.this >> Rdf.toIri) dataLeaf.ins
            in
            Leaf
                { data
                    | hash =
                        Hash.initial
                            |> Hash.propertyPath "sh:path" data.path
                            |> Hash.iris "sh:class" data.classes
                            |> Hash.maybeIri "sh:datatype" dataLeaf.datatype
                            |> Hash.iris "sh:hasValue" iriValues
                            |> Hash.iris "sh:in" iriIns
                }
                dataLeaf

        Nested data dataNested ->
            Nested
                { data
                    | hash =
                        Hash.initial
                            |> Hash.propertyPath "sh:path" data.path
                            |> Hash.iris "sh:class" data.classes
                            |> Hash.combine (toDataShapeNode dataNested.node)
                }
                dataNested


toDataShapeNode : ShapeNode -> DataShapeNode
toDataShapeNode (ShapeNode data) =
    data


toDataShapeProperty : ShapeProperty -> DataShapeProperty
toDataShapeProperty shapeProperty =
    case shapeProperty of
        Leaf data _ ->
            data

        Nested data _ ->
            data


{-| Modifies a `DataShapeProperty` for the presence of qualified value shapes inside the property shape.

The original `PropertyShape` has to be passed since we require `sh:Qualified{Max,Min}Count` from there.

-}
modifyDataShapePropertyForQualifiedNode :
    Merged.ShapeProperty
    -> DataShapeProperty
    -> DataShapeProperty
modifyDataShapePropertyForQualifiedNode (Merged.ShapeProperty shapeProperty) dataShapeProperty =
    { dataShapeProperty
        | minCount = Maybe.withDefault 0 shapeProperty.qualifiedMinCount
        , maxCount = Maybe.withDefault infinity shapeProperty.qualifiedMaxCount
        , qualified = True
    }


passClassFromNested : Merged.ShapeNode -> DataShapeProperty -> DataShapeProperty
passClassFromNested (Merged.ShapeNode dataShapeNode) dataShapeProperty =
    { dataShapeProperty
        | classes = dataShapeNode.classes ++ dataShapeProperty.classes
    }


fromEditor : Merged.Editor -> Result err Editor
fromEditor editor =
    case editor of
        Merged.EditorDetails ->
            Ok EditorDetails

        Merged.EditorInstanceSelect ->
            Ok EditorInstanceSelect

        Merged.EditorRadioButtons ->
            Ok EditorRadioButtons

        Merged.EditorGrid data ->
            Ok DataEditorGrid
                |> Result.hardcoded data.availableColumnsPath
                |> Result.hardcoded data.availableRowsPath
                |> Result.hardcoded data.columnPath
                |> Result.hardcoded data.rowPath
                |> Result.required
                    (case data.cellViewer of
                        Nothing ->
                            Ok Nothing

                        Just cellViewer ->
                            cellViewer
                                |> fromMerged
                                |> Result.map Just
                    )
                |> Result.required
                    (case data.cellEditor of
                        Nothing ->
                            Ok Nothing

                        Just cellEditor ->
                            cellEditor
                                |> fromMerged
                                |> Result.map Just
                    )
                |> Result.map EditorGrid

        Merged.EditorSchedule data ->
            Ok DataEditorSchedule
                |> Result.hardcoded data.hashScheduleEditorDayOffsetPath
                |> Result.hardcoded data.hashScheduleEditorOrderPath
                |> Result.hardcoded data.hashScheduleEditorSamples
                |> Result.hardcoded data.hashScheduleEditorTitlePath
                |> Result.hardcoded data.hashScheduleEditorDatePath
                |> Result.map EditorSchedule


infinity : Int
infinity =
    ceiling (1 / 0)
