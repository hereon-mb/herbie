module Shacl.Form.Ui exposing
    ( label, labelProvide
    , description
    , WithDescription
    )

{-|

@docs label, labelProvide
@docs description

-}

import Fluent exposing (Fluent, verbatim)
import Rdf exposing (StringOrLangString)
import Shacl.Form.Context exposing (C)
import Shacl.Form.Localize as Localize
import Shacl.Form.Translated as Translated


label : C -> { rest | names : List StringOrLangString } -> Fluent
label c data =
    data.names
        |> List.head
        |> Maybe.map (Localize.forLabel c)
        |> Maybe.withDefault "Missing label"
        |> verbatim


labelProvide : C -> { rest | names : List StringOrLangString } -> Fluent
labelProvide c data =
    [ "Provide "
    , data.names
        |> List.head
        |> Maybe.map (Localize.withinSentence c)
        |> Maybe.withDefault "a value"
    ]
        |> String.concat
        |> verbatim


type alias WithDescription rest =
    { rest
        | descriptions : List StringOrLangString
        , optional : Translated.Optional
        , readonly : Bool
        , generated : Bool
        , persisted : Bool
    }


description : C -> WithDescription rest -> Maybe Fluent
description c data =
    let
        info =
            case
                [ descriptionReadonlyGeneratedPersisted data
                , descriptionOptional data
                ]
                    |> List.filterMap identity
            of
                [] ->
                    Nothing

                infos ->
                    Just (String.join "; " infos)
    in
    case List.head data.descriptions of
        Nothing ->
            Maybe.map verbatim info

        Just text ->
            [ Just (Localize.verbatim c text)
            , Maybe.map (\textInfo -> " (" ++ textInfo ++ ")") info
            ]
                |> List.filterMap identity
                |> String.concat
                |> verbatim
                |> Just


descriptionReadonlyGeneratedPersisted : WithDescription rest -> Maybe String
descriptionReadonlyGeneratedPersisted data =
    case ( data.readonly, data.generated, data.persisted ) of
        ( True, True, True ) ->
            Just "Generated, will not change once submitted"

        ( True, True, False ) ->
            Just "Generated"

        ( True, False, True ) ->
            Nothing

        ( True, False, False ) ->
            Just "Read-only"

        ( False, True, True ) ->
            -- FIXME adjust description when this is an edit form
            Just "Generated, cannot be changed once submitted"

        ( False, True, False ) ->
            Just "Generated"

        ( False, False, True ) ->
            -- FIXME adjust description when this is an edit form
            Just "Cannot be changed once submitted"

        ( False, False, False ) ->
            Nothing


descriptionOptional : WithDescription rest -> Maybe String
descriptionOptional data =
    case data.optional of
        Translated.OptionalByItself ->
            Just "Optional"

        Translated.OptionalViaNesting ->
            Just "Optional"

        Translated.Required ->
            Nothing
