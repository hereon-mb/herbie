module Shacl.Form.Decoded exposing
    ( Field(..)
    , decode, decodeStrict, Strictness(..)
    , create, Mint(..)
    , duplicate
    , Error, errorToString
    , DataCheckbox, decoderCheckbox
    , DataCheckboxInstance, decoderCheckboxInstance
    , DataCollection, InstanceCollection, decoderInstanceCollection
    , DataCollectionSelect, InstanceCollectionSelect, SingleSelectOne, singleSelectOne
    , DataConstant
    , DataDateTime
    , DataFileUpload, NamedFile
    , DataForm
    , DataGrid, InstanceGrid, GridCells, GridColumn, GridRow, IndexGrid
    , DataGroup
    , DataIriReadOnly
    , DataNumber, decoderNumber
    , DataOptionalViaCheckbox
    , DataSchedule, InstanceSchedule, decoderInstanceSchedule
    , DataSelectMany
    , DataSelectOne
    , DataSingleNested
    , DataText, decoderText
    , DataVariants, VariantSelected
    , Injectable(..), DataConstantInjectable, DataSelectOneInjectable
    , LabelledIri
    )

{-|

@docs Field
@docs decode, decodeStrict, Strictness
@docs create, Mint
@docs duplicate
@docs Error, errorToString

@docs DataCheckbox, decoderCheckbox
@docs DataCheckboxInstance, decoderCheckboxInstance
@docs DataCollection, InstanceCollection, decoderInstanceCollection
@docs DataCollectionSelect, InstanceCollectionSelect, SingleSelectOne, singleSelectOne
@docs DataConstant
@docs DataDateTime
@docs DataFileUpload, NamedFile
@docs DataForm
@docs DataGrid, InstanceGrid, GridCells, GridColumn, GridRow, IndexGrid
@docs DataGroup
@docs DataIriReadOnly
@docs DataNumber, AffixedValue, Affix, decoderNumber
@docs DataOptionalViaCheckbox
@docs DataSchedule, InstanceSchedule, decoderInstanceSchedule
@docs DataSelectMany
@docs DataSelectOne
@docs DataSingleNested
@docs DataText, decoderText
@docs DataVariants, VariantSelected

@docs Injectable, DataConstantInjectable, DataSelectOneInjectable

@docs LabelledIri

-}

import Api.Decimal as Decimal
import Date exposing (Date)
import Dict exposing (Dict)
import List.Extra as List
import List.NonEmpty as NonEmpty
import Locale
import Ontology.Herbie exposing (herbie)
import Rdf exposing (BlankNodeOrIri, Iri, StringOrLangString)
import Rdf.Decode as Decode exposing (Decoder)
import Rdf.Decode.Extra as Decode
import Rdf.Graph exposing (Graph)
import Rdf.Mint as Mint exposing (Minter)
import Rdf.Namespaces.RDFS as RDFS
import Rdf.PropertyPath exposing (PropertyPath)
import Rdf.SetIri as SetIri
import Shacl.Form.Breadcrumbs as Breadcrumbs exposing (Path)
import Shacl.Form.Context exposing (C)
import Shacl.Form.Grouped as Grouped
import Shacl.Form.Translated as Translated
import String.Extra as String
import Time exposing (Posix)


type Field
    = FieldCheckbox DataCheckbox
    | FieldCheckboxInstance DataCheckboxInstance
    | FieldCollection DataCollection
    | FieldCollectionSelect DataCollectionSelect
    | FieldConstant DataConstant
    | FieldConstantInjectable DataConstantInjectable
    | FieldDateTime DataDateTime
    | FieldFileUpload DataFileUpload
    | FieldForm DataForm
    | FieldGrid DataGrid
    | FieldGroup DataGroup
    | FieldIriReadOnly DataIriReadOnly
    | FieldNumber DataNumber
    | FieldSchedule DataSchedule
    | FieldSelectMany DataSelectMany
    | FieldSelectOne DataSelectOne
    | FieldSelectOneInjectable DataSelectOneInjectable
    | FieldSingleNested DataSingleNested
    | FieldText DataText
    | FieldVariants DataVariants


type alias DataCheckbox =
    { path : Path
    , translated : Translated.DataCheckbox
    , nodeFocus : BlankNodeOrIri
    , checked : Maybe Bool
    }


type alias DataCheckboxInstance =
    { path : Path
    , translated : Translated.DataCheckboxInstance
    , nodeFocus : BlankNodeOrIri
    , checked : Maybe Bool
    }


type alias DataCollection =
    { path : Path
    , grouped : Grouped.DataCollection
    , nodeFocus : BlankNodeOrIri
    , instances : List InstanceCollection
    }


type alias InstanceCollection =
    { path : Path
    , nodeFocus : BlankNodeOrIri
    , field : Field
    , order : Maybe Int
    , label : Maybe StringOrLangString
    }


type alias DataCollectionSelect =
    { path : Path
    , grouped : Grouped.DataCollectionSelect
    , nodeFocus : BlankNodeOrIri
    , instances : List InstanceCollectionSelect
    }


type alias InstanceCollectionSelect =
    { path : Path
    , nodeFocus : BlankNodeOrIri
    , field : Field
    , selectOne : SingleSelectOne
    }


type alias SingleSelectOne =
    { path : Path
    , nodeFocus : BlankNodeOrIri
    , value : Maybe LabelledIri
    }


type alias DataConstant =
    { path : Path
    , translated : Translated.DataConstant
    , nodeFocus : BlankNodeOrIri
    }


type alias DataConstantInjectable =
    { path : Path
    , translated : Translated.DataConstantInjectable
    , nodeFocus : BlankNodeOrIri
    , label : Maybe StringOrLangString
    }


type alias DataDateTime =
    { path : Path
    , translated : Translated.DataDateTime
    , nodeFocus : BlankNodeOrIri
    , posix : Maybe Posix
    }


type alias DataFileUpload =
    { path : Path
    , translated : Translated.DataFileUpload
    , nodeFocus : BlankNodeOrIri
    , file : Maybe NamedFile
    }


type alias NamedFile =
    { iri : Iri
    , name : Maybe String
    }


type alias DataForm =
    { path : Path
    , grouped : Grouped.DataForm
    , nodeFocus : BlankNodeOrIri
    , fields : List Field
    }


type alias DataGrid =
    { path : Path
    , grouped : Grouped.DataGrid
    , nodeFocus : BlankNodeOrIri
    , gridCells : GridCells
    , instances : Dict IndexGrid InstanceGrid
    }


type alias GridCells =
    { gridRows : List GridRow
    , gridColumns : List GridColumn
    }


type alias GridColumn =
    { iri : Iri
    , label : StringOrLangString
    }


type alias GridRow =
    GridColumn


{-| Model cells are indexed row-first, ie. `( indexRow, indexColumn )`, except
in breadcrumbs, where they are indexed `[ indexColumn, indexRow ]`.
-}
type alias IndexGrid =
    ( Int, Int )


type alias InstanceGrid =
    -- FIXME We want to change field to fieldEditor and fieldViewer
    { path : Path
    , nodeFocus : BlankNodeOrIri
    , field : Field
    , label : Maybe StringOrLangString
    }


type alias DataGroup =
    { path : Path
    , grouped : Grouped.DataGroup
    , nodeFocus : BlankNodeOrIri
    , field : Field
    }


type alias DataIriReadOnly =
    { path : Path
    , translated : Translated.DataIriReadOnly
    , nodeFocus : BlankNodeOrIri
    , value : Maybe LabelledIri
    }


type alias DataNumber =
    { path : Path
    , translated : Translated.DataNumber
    , nodeFocus : BlankNodeOrIri
    , value : Maybe String
    , injected : Maybe Injectable
    }


type alias DataOptionalViaCheckbox =
    { path : Path
    , grouped : Grouped.DataOptionalViaCheckbox
    , nodeFocus : BlankNodeOrIri
    , field : Field
    }


type alias DataSchedule =
    { path : Path
    , grouped : Grouped.DataSchedule
    , nodeFocus : BlankNodeOrIri
    , date : Maybe Date
    , instances : List InstanceSchedule
    }


type alias InstanceSchedule =
    { path : Path
    , nodeFocus : BlankNodeOrIri
    , field : Field
    , dayOffset : Int
    , order : Int
    , label : Maybe StringOrLangString
    }


type alias DataSelectMany =
    { path : Path
    , translated : Translated.DataSelectMany
    , nodeFocus : BlankNodeOrIri
    , selected : List LabelledIri
    }


type alias DataSelectOne =
    { path : Path
    , translated : Translated.DataSelectOne
    , nodeFocus : BlankNodeOrIri
    , selected : Maybe LabelledIri
    }


type alias DataSelectOneInjectable =
    { path : Path
    , translated : Translated.DataSelectOneInjectable
    , nodeFocus : BlankNodeOrIri
    , selected : Maybe Translated.Value
    }


type alias DataSingleNested =
    { path : Path
    , grouped : Grouped.DataSingleNested
    , nodeFocus : BlankNodeOrIri
    , field : Maybe Field
    }


type alias DataText =
    { path : Path
    , translated : Translated.DataText
    , nodeFocus : BlankNodeOrIri
    , text : Maybe String
    }


type alias DataVariants =
    { path : Path
    , grouped : Grouped.DataVariants
    , nodeFocus : BlankNodeOrIri
    , selected : VariantSelected
    }


type alias VariantSelected =
    { path : Path
    , index : Int
    , grouped : Grouped.Variant
    , field : Field
    }


type alias LabelledIri =
    { iri : Iri
    , label : Maybe StringOrLangString
    }


type Injectable
    = InjectableConstant DataConstantInjectable
    | InjectableSelectOne DataSelectOneInjectable



-- ERROR


type Error
    = ErrorDecode Decode.Error


errorToString : Error -> String
errorToString error =
    case error of
        ErrorDecode errorDecode ->
            Decode.errorToString errorDecode



-- INJECT


inject : DataForm -> DataForm
inject data =
    if data.grouped.inject then
        { data
            | fields =
                data.fields
                    |> List.filterMap getInjectable
                    |> List.head
                    |> Maybe.map (\injectable -> List.map (injectInjectable injectable) data.fields)
                    |> Maybe.withDefault data.fields
        }

    else
        { data | fields = List.map injectField data.fields }


injectField : Field -> Field
injectField field =
    case field of
        FieldCheckbox _ ->
            field

        FieldCheckboxInstance _ ->
            field

        FieldCollection data ->
            FieldCollection
                { data
                    | instances =
                        List.map
                            (\instance ->
                                { instance | field = injectField instance.field }
                            )
                            data.instances
                }

        FieldCollectionSelect _ ->
            field

        FieldConstant _ ->
            field

        FieldConstantInjectable _ ->
            field

        FieldDateTime _ ->
            field

        FieldFileUpload _ ->
            field

        FieldForm data ->
            FieldForm (inject data)

        FieldGrid data ->
            FieldGrid
                { data
                    | instances =
                        Dict.map
                            (\_ instance ->
                                { instance | field = injectField instance.field }
                            )
                            data.instances
                }

        FieldGroup data ->
            FieldGroup { data | field = injectField data.field }

        FieldIriReadOnly _ ->
            field

        FieldNumber _ ->
            field

        FieldSchedule data ->
            FieldSchedule
                { data
                    | instances =
                        List.map
                            (\instance ->
                                { instance | field = injectField instance.field }
                            )
                            data.instances
                }

        FieldSelectMany _ ->
            field

        FieldSelectOne _ ->
            field

        FieldSelectOneInjectable _ ->
            field

        FieldSingleNested data ->
            FieldSingleNested
                { data
                    | field =
                        Maybe.map (\fieldNested -> injectField fieldNested) data.field
                }

        FieldText _ ->
            field

        FieldVariants ({ selected } as data) ->
            FieldVariants
                { data | selected = { selected | field = injectField selected.field } }


getInjectable : Field -> Maybe Injectable
getInjectable field =
    case field of
        FieldCheckbox _ ->
            Nothing

        FieldCheckboxInstance _ ->
            Nothing

        FieldCollection _ ->
            Nothing

        FieldCollectionSelect _ ->
            Nothing

        FieldConstant _ ->
            Nothing

        FieldConstantInjectable data ->
            Just (InjectableConstant data)

        FieldDateTime _ ->
            Nothing

        FieldFileUpload _ ->
            Nothing

        FieldForm data ->
            data.fields
                |> List.filterMap getInjectable
                |> List.head

        FieldGrid _ ->
            Nothing

        FieldGroup _ ->
            Nothing

        FieldIriReadOnly _ ->
            Nothing

        FieldNumber _ ->
            Nothing

        FieldSchedule _ ->
            Nothing

        FieldSelectMany _ ->
            Nothing

        FieldSelectOne _ ->
            Nothing

        FieldSelectOneInjectable data ->
            Just (InjectableSelectOne data)

        FieldSingleNested data ->
            Maybe.andThen getInjectable data.field

        FieldText _ ->
            Nothing

        FieldVariants _ ->
            Nothing


injectInjectable : Injectable -> Field -> Field
injectInjectable injectable field =
    case field of
        FieldCheckbox _ ->
            field

        FieldCheckboxInstance _ ->
            field

        FieldCollection _ ->
            field

        FieldCollectionSelect _ ->
            field

        FieldConstant _ ->
            field

        FieldConstantInjectable _ ->
            field

        FieldDateTime _ ->
            field

        FieldFileUpload _ ->
            field

        FieldForm data ->
            FieldForm
                { data
                    | fields = List.map (injectInjectable injectable) data.fields
                }

        FieldGrid _ ->
            field

        FieldGroup _ ->
            field

        FieldIriReadOnly _ ->
            field

        FieldNumber data ->
            FieldNumber { data | injected = Just injectable }

        FieldSchedule _ ->
            field

        FieldSelectMany _ ->
            field

        FieldSelectOne _ ->
            field

        FieldSelectOneInjectable _ ->
            field

        FieldSingleNested data ->
            FieldSingleNested
                { data
                    | field = Maybe.map (injectInjectable injectable) data.field
                }

        FieldText _ ->
            field

        FieldVariants _ ->
            field



-- CREATE


type Mint
    = NoMint { nodeFocus : BlankNodeOrIri }
    | MintBlankNode
    | MintIri { nodeFocus : BlankNodeOrIri, class : Iri }


{-| Create an empty `Decoded` form for the given SHACL document, which is
assumed to be inserted at the provided `Path`.
-}
create : Path -> Mint -> Grouped.Field -> Minter err Field
create pathParent mint field =
    createHelp pathParent mint field
        |> Mint.map injectField


createHelp : Path -> Mint -> Grouped.Field -> Minter err Field
createHelp pathParent mint field =
    case mint of
        NoMint { nodeFocus } ->
            createNested pathParent field nodeFocus

        MintBlankNode ->
            Mint.blankNode
                |> Mint.andThen
                    (\nodeFocusNew ->
                        createNested pathParent field (Rdf.asBlankNodeOrIri nodeFocusNew)
                    )

        MintIri { nodeFocus, class } ->
            Mint.iri nodeFocus class
                |> Mint.andThen
                    (\nodeFocusNew ->
                        createNested pathParent field (Rdf.asBlankNodeOrIri nodeFocusNew)
                    )


createNested : Path -> Grouped.Field -> BlankNodeOrIri -> Minter err Field
createNested pathParent field nodeFocus =
    case field of
        Grouped.FieldCheckbox translated ->
            Mint.static
                (FieldCheckbox
                    { path = Breadcrumbs.Hash translated.hash :: pathParent
                    , translated = translated
                    , nodeFocus = nodeFocus
                    , checked = translated.defaultValue
                    }
                )

        Grouped.FieldCheckboxInstance translated ->
            Mint.static
                (FieldCheckboxInstance
                    { path = Breadcrumbs.Hash translated.hash :: pathParent
                    , translated = translated
                    , nodeFocus = nodeFocus
                    , checked = translated.defaultValue
                    }
                )

        Grouped.FieldCollection grouped ->
            Mint.static
                (FieldCollection
                    { path = Breadcrumbs.Hash grouped.hash :: pathParent
                    , grouped = grouped
                    , nodeFocus = nodeFocus
                    , instances = []
                    }
                )

        Grouped.FieldCollectionSelect grouped ->
            Mint.static
                (FieldCollectionSelect
                    { path = Breadcrumbs.Hash grouped.hash :: pathParent
                    , grouped = grouped
                    , nodeFocus = nodeFocus
                    , instances = []
                    }
                )

        Grouped.FieldConstant translated ->
            Mint.static
                (FieldConstant
                    { path = Breadcrumbs.Hash translated.hash :: pathParent
                    , translated = translated
                    , nodeFocus = nodeFocus
                    }
                )

        Grouped.FieldConstantInjectable translated ->
            Mint.static
                (FieldConstantInjectable
                    { path = Breadcrumbs.Hash translated.hash :: pathParent
                    , translated = translated
                    , nodeFocus = nodeFocus
                    , label = Nothing
                    }
                )

        Grouped.FieldDateTime translated ->
            Mint.static
                (FieldDateTime
                    { path = Breadcrumbs.Hash translated.hash :: pathParent
                    , translated = translated
                    , nodeFocus = nodeFocus
                    , posix = translated.defaultValue
                    }
                )

        Grouped.FieldFileUpload translated ->
            Mint.static
                (FieldFileUpload
                    { path = Breadcrumbs.Hash translated.hash :: pathParent
                    , translated = translated
                    , nodeFocus = nodeFocus
                    , file =
                        Maybe.map
                            (\iri ->
                                { iri = iri
                                , name = Nothing
                                }
                            )
                            translated.defaultValue
                    }
                )

        Grouped.FieldForm grouped ->
            grouped.successes
                |> List.map
                    (.field
                        >> createHelp pathParent (NoMint { nodeFocus = nodeFocus })
                    )
                |> Mint.sequence
                |> Mint.map
                    (\fields ->
                        { path = pathParent
                        , grouped = grouped
                        , nodeFocus = nodeFocus
                        , fields = fields
                        }
                    )
                |> Mint.map FieldForm

        Grouped.FieldGrid grouped ->
            Mint.static
                (FieldGrid
                    { path = Breadcrumbs.Hash grouped.hash :: pathParent
                    , grouped = grouped
                    , nodeFocus = nodeFocus
                    , gridCells =
                        -- FIXME Retrieve from graph
                        { gridColumns = []
                        , gridRows = []
                        }
                    , instances = Dict.empty
                    }
                )

        Grouped.FieldGroup grouped ->
            grouped.field
                |> createHelp pathParent (NoMint { nodeFocus = nodeFocus })
                |> Mint.map
                    (\fieldNested ->
                        FieldGroup
                            { path = pathParent
                            , grouped = grouped
                            , nodeFocus = nodeFocus
                            , field = fieldNested
                            }
                    )

        Grouped.FieldIriReadOnly translated ->
            Mint.static
                (FieldIriReadOnly
                    { path = Breadcrumbs.Hash translated.hash :: pathParent
                    , translated = translated
                    , nodeFocus = nodeFocus
                    , value = translated.defaultValue
                    }
                )

        Grouped.FieldNumber translated ->
            Mint.static
                (FieldNumber
                    { path = Breadcrumbs.Hash translated.hash :: pathParent
                    , translated = translated
                    , nodeFocus = nodeFocus
                    , value = translated.defaultValue
                    , injected = Nothing
                    }
                )

        Grouped.FieldSchedule grouped ->
            Mint.static
                (FieldSchedule
                    { path = Breadcrumbs.Hash grouped.hash :: pathParent
                    , grouped = grouped
                    , nodeFocus = nodeFocus
                    , date = Nothing -- FIXME Retrieve from graph
                    , instances = []
                    }
                )

        Grouped.FieldSelectMany translated ->
            Mint.static
                (FieldSelectMany
                    { path = Breadcrumbs.Hash translated.hash :: pathParent
                    , translated = translated
                    , nodeFocus = nodeFocus
                    , selected =
                        List.map labelledIriFromValue
                            translated.defaultValue
                    }
                )

        Grouped.FieldSelectOne translated ->
            Mint.static
                (FieldSelectOne
                    { path = Breadcrumbs.Hash translated.hash :: pathParent
                    , translated = translated
                    , nodeFocus = nodeFocus
                    , selected =
                        Maybe.map labelledIriFromValue
                            translated.defaultValue
                    }
                )

        Grouped.FieldSelectOneInjectable translated ->
            Mint.static
                (FieldSelectOneInjectable
                    { path = Breadcrumbs.Hash translated.hash :: pathParent
                    , translated = translated
                    , nodeFocus = nodeFocus
                    , selected = translated.defaultValue
                    }
                )

        Grouped.FieldSingleNested grouped ->
            let
                mint =
                    if grouped.mintIri then
                        case List.head grouped.classes of
                            Nothing ->
                                MintBlankNode

                            Just class ->
                                -- FIXME Mint from all classes
                                MintIri
                                    { nodeFocus = nodeFocus
                                    , class = class
                                    }

                    else
                        MintBlankNode

                path =
                    Breadcrumbs.Hash grouped.hash :: pathParent
            in
            grouped.field
                |> createHelp path mint
                |> Mint.map
                    (\fieldNested ->
                        FieldSingleNested
                            { path = path
                            , grouped = grouped
                            , nodeFocus = nodeFocus
                            , field = Just fieldNested
                            }
                    )

        Grouped.FieldText translated ->
            Mint.static
                (FieldText
                    { path = Breadcrumbs.Hash translated.hash :: pathParent
                    , translated = translated
                    , nodeFocus = nodeFocus
                    , text = translated.defaultValue
                    }
                )

        Grouped.FieldVariants grouped ->
            let
                variant =
                    NonEmpty.head grouped.variants

                path =
                    Breadcrumbs.Hash grouped.hash :: pathParent

                pathNested =
                    Breadcrumbs.Hash variant.hash :: path
            in
            variant.field
                |> createHelp pathNested (NoMint { nodeFocus = nodeFocus })
                |> Mint.map
                    (\fieldNested ->
                        FieldVariants
                            { path = path
                            , grouped = grouped
                            , nodeFocus = nodeFocus
                            , selected =
                                { path = pathNested
                                , index = 0
                                , grouped = NonEmpty.head grouped.variants
                                , field = fieldNested
                                }
                            }
                    )



-- DUPLICATE


{-| Duplicate a `Decoded` value. This will recursively mint new IRIs and Blank
Nodes and copy all data. The duplicated field is assumed to be inserted at the
provided `Path`.
-}
duplicate : Path -> Mint -> Field -> Minter err Field
duplicate pathParent mint field =
    duplicateHelp pathParent mint field
        |> Mint.map injectField


duplicateHelp : Path -> Mint -> Field -> Minter err Field
duplicateHelp pathParent mint field =
    case mint of
        NoMint { nodeFocus } ->
            duplicateNested pathParent field nodeFocus

        MintBlankNode ->
            Mint.blankNode
                |> Mint.andThen
                    (Rdf.asBlankNodeOrIri
                        >> duplicateNested pathParent field
                    )

        MintIri { nodeFocus, class } ->
            Mint.iri nodeFocus class
                |> Mint.andThen
                    (Rdf.asBlankNodeOrIri
                        >> duplicateNested pathParent field
                    )


duplicateNested : Path -> Field -> BlankNodeOrIri -> Minter err Field
duplicateNested pathParent field nodeFocus =
    case field of
        FieldCheckbox data ->
            duplicateCheckbox pathParent data nodeFocus

        FieldCheckboxInstance data ->
            duplicateCheckboxInstance pathParent data nodeFocus

        FieldCollection data ->
            duplicateCollection pathParent data nodeFocus

        FieldCollectionSelect data ->
            duplicateCollectionSelect pathParent data nodeFocus

        FieldConstant data ->
            duplicateConstant pathParent data nodeFocus

        FieldConstantInjectable data ->
            duplicateConstantInjectable pathParent data nodeFocus

        FieldDateTime data ->
            duplicateDateTime pathParent data nodeFocus

        FieldFileUpload data ->
            duplicateFileUpload pathParent data nodeFocus

        FieldForm data ->
            duplicateForm pathParent data nodeFocus

        FieldGrid data ->
            duplicateGrid pathParent data nodeFocus

        FieldGroup data ->
            duplicateGroup pathParent data nodeFocus

        FieldIriReadOnly data ->
            duplicateIriReadOnly pathParent data nodeFocus

        FieldNumber data ->
            duplicateNumber pathParent data nodeFocus

        FieldSchedule data ->
            duplicateSchedule pathParent data nodeFocus

        FieldSelectMany data ->
            duplicateSelectMany pathParent data nodeFocus

        FieldSelectOne data ->
            duplicateSelectOne pathParent data nodeFocus

        FieldSelectOneInjectable data ->
            duplicateSelectOneInjectable pathParent data nodeFocus

        FieldSingleNested data ->
            duplicateSingleNested pathParent data nodeFocus

        FieldText data ->
            duplicateText pathParent data nodeFocus

        FieldVariants data ->
            duplicateVariants pathParent data nodeFocus


duplicateCheckbox : Path -> DataCheckbox -> BlankNodeOrIri -> Minter err Field
duplicateCheckbox pathParent data nodeFocus =
    Mint.static
        (FieldCheckbox
            { data
                | path = Breadcrumbs.Hash data.translated.hash :: pathParent
                , nodeFocus = nodeFocus
            }
        )


duplicateCheckboxInstance :
    Path
    -> DataCheckboxInstance
    -> BlankNodeOrIri
    -> Minter err Field
duplicateCheckboxInstance pathParent data nodeFocus =
    Mint.static
        (FieldCheckboxInstance
            { data
                | path = Breadcrumbs.Hash data.translated.hash :: pathParent
                , nodeFocus = nodeFocus
            }
        )


duplicateCollection :
    Path
    -> DataCollection
    -> BlankNodeOrIri
    -> Minter err Field
duplicateCollection pathParent data nodeFocus =
    let
        path =
            Breadcrumbs.Hash data.grouped.hash :: pathParent
    in
    data.instances
        |> List.indexedMap (duplicateInstanceCollection path data)
        |> Mint.sequence
        |> Mint.map
            (\instances ->
                FieldCollection
                    { data
                        | path = path
                        , nodeFocus = nodeFocus
                        , instances = instances
                    }
            )


duplicateInstanceCollection :
    Path
    -> DataCollection
    -> Int
    -> InstanceCollection
    -> Minter err InstanceCollection
duplicateInstanceCollection path data index instance =
    let
        pathNested =
            Breadcrumbs.Ordinal index :: path
    in
    instance.field
        |> duplicateHelp pathNested
            (MintIri
                { nodeFocus = data.nodeFocus
                , class = NonEmpty.head data.grouped.classes
                }
            )
        |> Mint.map
            (\field ->
                { instance
                    | path = pathNested

                    -- FIXME
                    -- , nodeFocus = ???
                    , field = field
                }
            )


duplicateCollectionSelect :
    Path
    -> DataCollectionSelect
    -> BlankNodeOrIri
    -> Minter err Field
duplicateCollectionSelect pathParent data nodeFocus =
    let
        path =
            Breadcrumbs.Hash data.grouped.hash :: pathParent
    in
    data.instances
        |> List.indexedMap (duplicateInstanceCollectionSelect path data)
        |> Mint.sequence
        |> Mint.map
            (\instances ->
                FieldCollectionSelect
                    { data
                        | path = path
                        , nodeFocus = nodeFocus
                        , instances = instances
                    }
            )


duplicateInstanceCollectionSelect :
    Path
    -> DataCollectionSelect
    -> Int
    -> InstanceCollectionSelect
    -> Minter err InstanceCollectionSelect
duplicateInstanceCollectionSelect path data index instance =
    let
        pathNested =
            Breadcrumbs.Ordinal index :: path
    in
    instance.field
        |> duplicateHelp pathNested
            (MintIri
                { nodeFocus = data.nodeFocus
                , class = NonEmpty.head data.grouped.classes
                }
            )
        |> Mint.map
            (\field ->
                case singleSelectOne field of
                    Nothing ->
                        -- This should never happen
                        instance

                    Just selectOne ->
                        { instance
                            | path = pathNested

                            -- FIXME
                            -- , nodeFocus = ???
                            , field = field
                            , selectOne = selectOne
                        }
            )


duplicateConstant :
    Path
    -> DataConstant
    -> BlankNodeOrIri
    -> Minter err Field
duplicateConstant pathParent data nodeFocus =
    Mint.static
        (FieldConstant
            { data
                | path = Breadcrumbs.Hash data.translated.hash :: pathParent
                , nodeFocus = nodeFocus
            }
        )


duplicateConstantInjectable :
    Path
    -> DataConstantInjectable
    -> BlankNodeOrIri
    -> Minter err Field
duplicateConstantInjectable pathParent data nodeFocus =
    Mint.static
        (FieldConstantInjectable
            { data
                | path = Breadcrumbs.Hash data.translated.hash :: pathParent
                , nodeFocus = nodeFocus
            }
        )


duplicateDateTime :
    Path
    -> DataDateTime
    -> BlankNodeOrIri
    -> Minter err Field
duplicateDateTime pathParent data nodeFocus =
    Mint.static
        (FieldDateTime
            { data
                | path = Breadcrumbs.Hash data.translated.hash :: pathParent
                , nodeFocus = nodeFocus
            }
        )


duplicateFileUpload :
    Path
    -> DataFileUpload
    -> BlankNodeOrIri
    -> Minter err Field
duplicateFileUpload pathParent data nodeFocus =
    Mint.static
        (FieldFileUpload
            { data
                | path = Breadcrumbs.Hash data.translated.hash :: pathParent
                , nodeFocus = nodeFocus
            }
        )


duplicateForm :
    Path
    -> DataForm
    -> BlankNodeOrIri
    -> Minter err Field
duplicateForm pathParent data nodeFocus =
    data.fields
        |> List.map (duplicateHelp pathParent (NoMint { nodeFocus = nodeFocus }))
        |> Mint.sequence
        |> Mint.map
            (\fields ->
                FieldForm
                    { data
                        | path = pathParent
                        , nodeFocus = nodeFocus
                        , fields = fields
                    }
            )


duplicateGrid :
    Path
    -> DataGrid
    -> BlankNodeOrIri
    -> Minter err Field
duplicateGrid pathParent data nodeFocus =
    let
        path =
            Breadcrumbs.Hash data.grouped.hash :: pathParent
    in
    data.instances
        |> Dict.toList
        |> List.map (duplicateInstanceGrid path data)
        |> Mint.sequence
        |> Mint.map
            (\instances ->
                FieldGrid
                    { data
                        | path = path
                        , nodeFocus = nodeFocus
                        , instances = Dict.fromList instances
                    }
            )


duplicateInstanceGrid :
    Path
    -> DataGrid
    -> ( ( Int, Int ), InstanceGrid )
    -> Minter err ( ( Int, Int ), InstanceGrid )
duplicateInstanceGrid path data ( ( row, column ), instance ) =
    let
        pathNested =
            Breadcrumbs.Ordinal column :: Breadcrumbs.Ordinal row :: path
    in
    instance.field
        |> duplicateHelp pathNested
            (MintIri
                { nodeFocus = data.nodeFocus
                , class = NonEmpty.head data.grouped.classes
                }
            )
        |> Mint.map
            (\field ->
                ( ( row, column )
                , { instance
                    | path = pathNested

                    -- FIXME
                    -- , nodeFocus = ???
                    , field = field
                  }
                )
            )


duplicateGroup :
    Path
    -> DataGroup
    -> BlankNodeOrIri
    -> Minter err Field
duplicateGroup pathParent data nodeFocus =
    data.field
        |> duplicateHelp pathParent (NoMint { nodeFocus = nodeFocus })
        |> Mint.map
            (\field ->
                FieldGroup
                    { data
                        | path = pathParent
                        , nodeFocus = nodeFocus
                        , field = field
                    }
            )


duplicateIriReadOnly :
    Path
    -> DataIriReadOnly
    -> BlankNodeOrIri
    -> Minter err Field
duplicateIriReadOnly pathParent data nodeFocus =
    Mint.static
        (FieldIriReadOnly
            { data
                | path = Breadcrumbs.Hash data.translated.hash :: pathParent
                , nodeFocus = nodeFocus
            }
        )


duplicateNumber :
    Path
    -> DataNumber
    -> BlankNodeOrIri
    -> Minter err Field
duplicateNumber pathParent data nodeFocus =
    Mint.static
        (FieldNumber
            { data
                | path = Breadcrumbs.Hash data.translated.hash :: pathParent
                , nodeFocus = nodeFocus
            }
        )


duplicateSchedule :
    Path
    -> DataSchedule
    -> BlankNodeOrIri
    -> Minter err Field
duplicateSchedule pathParent data nodeFocus =
    let
        path =
            Breadcrumbs.Hash data.grouped.hash :: pathParent
    in
    data.instances
        |> List.indexedMap (duplicateInstanceSchedule path data)
        |> Mint.sequence
        |> Mint.map
            (\instances ->
                FieldSchedule
                    { data
                        | path = path
                        , nodeFocus = nodeFocus
                        , instances = instances
                    }
            )


duplicateInstanceSchedule :
    Path
    -> DataSchedule
    -> Int
    -> InstanceSchedule
    -> Minter err InstanceSchedule
duplicateInstanceSchedule path data index instance =
    let
        pathNested =
            Breadcrumbs.Ordinal index :: path
    in
    instance.field
        |> duplicateHelp pathNested
            (MintIri
                { nodeFocus = data.nodeFocus
                , class = NonEmpty.head data.grouped.classes
                }
            )
        |> Mint.map
            (\field ->
                { instance
                    | path = pathNested

                    -- FIXME
                    -- , nodeFocus = ???
                    , field = field
                }
            )


duplicateSelectMany :
    Path
    -> DataSelectMany
    -> BlankNodeOrIri
    -> Minter err Field
duplicateSelectMany pathParent data nodeFocus =
    Mint.static
        (FieldSelectMany
            { data
                | path = Breadcrumbs.Hash data.translated.hash :: pathParent
                , nodeFocus = nodeFocus
            }
        )


duplicateSelectOne :
    Path
    -> DataSelectOne
    -> BlankNodeOrIri
    -> Minter err Field
duplicateSelectOne pathParent data nodeFocus =
    Mint.static
        (FieldSelectOne
            { data
                | path = Breadcrumbs.Hash data.translated.hash :: pathParent
                , nodeFocus = nodeFocus
            }
        )


duplicateSelectOneInjectable :
    Path
    -> DataSelectOneInjectable
    -> BlankNodeOrIri
    -> Minter err Field
duplicateSelectOneInjectable pathParent data nodeFocus =
    Mint.static
        (FieldSelectOneInjectable
            { data
                | path = Breadcrumbs.Hash data.translated.hash :: pathParent
                , nodeFocus = nodeFocus
            }
        )


duplicateSingleNested :
    Path
    -> DataSingleNested
    -> BlankNodeOrIri
    -> Minter err Field
duplicateSingleNested pathParent data nodeFocus =
    let
        mint =
            if data.grouped.mintIri then
                case List.head data.grouped.classes of
                    Nothing ->
                        MintBlankNode

                    Just class ->
                        -- FIXME Mint from all classes
                        MintIri
                            { nodeFocus = nodeFocus
                            , class = class
                            }

            else
                MintBlankNode

        path =
            Breadcrumbs.Hash data.grouped.hash :: pathParent
    in
    case data.field of
        Nothing ->
            Mint.static
                (FieldSingleNested
                    { data
                        | path = path
                        , nodeFocus = nodeFocus
                    }
                )

        Just fieldNested ->
            fieldNested
                |> duplicateHelp path mint
                |> Mint.map
                    (\fieldNestedDuplicated ->
                        FieldSingleNested
                            { data
                                | path = path
                                , nodeFocus = nodeFocus
                                , field = Just fieldNestedDuplicated
                            }
                    )


duplicateText :
    Path
    -> DataText
    -> BlankNodeOrIri
    -> Minter err Field
duplicateText pathParent data nodeFocus =
    Mint.static
        (FieldText
            { data
                | path = Breadcrumbs.Hash data.translated.hash :: pathParent
                , nodeFocus = nodeFocus
            }
        )


duplicateVariants :
    Path
    -> DataVariants
    -> BlankNodeOrIri
    -> Minter err Field
duplicateVariants pathParent data nodeFocus =
    data.selected
        |> duplicateVariantSelected pathParent data
        |> Mint.map
            (\selected ->
                FieldVariants
                    { data
                        | path = pathParent
                        , nodeFocus = nodeFocus
                        , selected = selected
                    }
            )


duplicateVariantSelected :
    Path
    -> DataVariants
    -> VariantSelected
    -> Minter err VariantSelected
duplicateVariantSelected pathParent data selected =
    let
        path =
            Breadcrumbs.Hash selected.grouped.hash :: pathParent
    in
    selected.field
        |> duplicateHelp path (NoMint { nodeFocus = data.nodeFocus })
        |> Mint.map
            (\field ->
                { selected
                    | path = path
                    , field = field
                }
            )



-- DECODE


{-| Decode an RDF graph for a SHACL document assuming that the graph is
a **draft**, i.e. contains `rdfs:isDefinedBy` triples for all nested blank
nodes.
-}
decode :
    C
    -> Grouped.DataForm
    -> Graph
    -> Iri
    -> BlankNodeOrIri
    -> Result Error DataForm
decode c data graph iriDocument nodeFocus =
    graph
        |> Decode.decode
            (Decode.from nodeFocus
                (decoderDataForm c WithIsDefinedBy iriDocument [] data)
            )
        |> Result.mapError ErrorDecode
        |> Result.map inject


{-| Decode an RDF graph for a SHACL document assuming that the graph is
valid. This does not need to check the `rdfs:isDefinedBy` triples.
-}
decodeStrict :
    C
    -> Grouped.DataForm
    -> Graph
    -> Iri
    -> BlankNodeOrIri
    -> Result Error DataForm
decodeStrict c data graph iriDocument nodeFocus =
    graph
        |> Decode.decode
            (Decode.from nodeFocus
                (decoderDataForm c Strict iriDocument [] data)
            )
        |> Result.mapError ErrorDecode
        |> Result.map inject


{-| Specify how to decode:

  - **Strict**: Assume that the graph is valid w.r.t. the SHACL document. This
    is used to decode **published** graphs.

  - **WithIsDefinedBy**: Do not assume that the graph is valid w.r.t. the SHACL
    document but require `rdfs:isDefinedBy` annotations for each node, which are
    used to distribute nodes onto different qualified value shapes for the same
    property path. This is used to decode **draft** graphs.

  - **Relaxed**: Do not assume that the graph is valid w.r.t. the SHACL
    document and also do not require `rdfs:isDefinedBy` annotations. Ambiguous
    nodes are skipped. This is used to decode pasted graphs.

Note: We might add a **RelaxedSafe** mode which collects all possible
variations when encountering ambiguous graphs.

-}
type Strictness
    = Strict
    | WithIsDefinedBy
    | Relaxed


decoder : C -> Strictness -> Iri -> Path -> Grouped.Field -> Decoder Field
decoder c strictness iriDocument pathParent field =
    case field of
        Grouped.FieldCheckbox translated ->
            decoderFieldCheckbox strictness pathParent translated

        Grouped.FieldCheckboxInstance translated ->
            decoderFieldCheckboxInstance strictness pathParent translated

        Grouped.FieldCollection grouped ->
            decoderFieldCollection c strictness iriDocument pathParent grouped

        Grouped.FieldCollectionSelect grouped ->
            decoderFieldCollectionSelect c strictness iriDocument pathParent grouped

        Grouped.FieldConstant translated ->
            decoderFieldConstant strictness pathParent translated

        Grouped.FieldConstantInjectable translated ->
            decoderFieldConstantInjectable strictness pathParent translated

        Grouped.FieldDateTime translated ->
            decoderFieldDateTime strictness pathParent translated

        Grouped.FieldFileUpload translated ->
            decoderFieldFileUpload strictness pathParent translated

        Grouped.FieldForm grouped ->
            decoderFieldForm c strictness iriDocument pathParent grouped

        Grouped.FieldGrid grouped ->
            decoderFieldGrid c strictness iriDocument pathParent grouped

        Grouped.FieldGroup grouped ->
            decoderFieldGroup c strictness iriDocument pathParent grouped

        Grouped.FieldIriReadOnly translated ->
            decoderFieldIriReadOnly strictness pathParent translated

        Grouped.FieldNumber translated ->
            decoderFieldNumber c strictness pathParent translated

        Grouped.FieldSchedule grouped ->
            decoderFieldSchedule c strictness iriDocument pathParent grouped

        Grouped.FieldSelectMany translated ->
            decoderFieldSelectMany pathParent translated

        Grouped.FieldSelectOne translated ->
            decoderFieldSelectOne strictness pathParent translated

        Grouped.FieldSelectOneInjectable translated ->
            decoderFieldSelectOneInjectable strictness pathParent translated

        Grouped.FieldSingleNested grouped ->
            decoderFieldSingleNested c strictness iriDocument pathParent grouped

        Grouped.FieldText translated ->
            decoderFieldText strictness pathParent translated

        Grouped.FieldVariants grouped ->
            decoderFieldVariants c strictness iriDocument pathParent grouped


decoderFieldCheckbox :
    Strictness
    -> Path
    -> Translated.DataCheckbox
    -> Decoder Field
decoderFieldCheckbox strictness pathParent translated =
    Decode.succeed DataCheckbox
        |> Decode.hardcoded (Breadcrumbs.Hash translated.hash :: pathParent)
        |> Decode.hardcoded translated
        |> Decode.custom Decode.blankNodeOrIri
        |> Decode.custom (decodeRequired strictness translated decoderCheckbox)
        |> Decode.map FieldCheckbox


decoderCheckbox : Decoder Bool
decoderCheckbox =
    Decode.bool


decoderFieldCheckboxInstance :
    Strictness
    -> Path
    -> Translated.DataCheckboxInstance
    -> Decoder Field
decoderFieldCheckboxInstance strictness pathParent translated =
    Decode.succeed DataCheckboxInstance
        |> Decode.hardcoded (Breadcrumbs.Hash translated.hash :: pathParent)
        |> Decode.hardcoded translated
        |> Decode.custom Decode.blankNodeOrIri
        |> Decode.custom
            (decodeRequired strictness
                translated
                (decoderCheckboxInstance translated)
            )
        |> Decode.map FieldCheckboxInstance


decoderCheckboxInstance : Translated.DataCheckboxInstance -> Decoder Bool
decoderCheckboxInstance translated =
    Decode.map (\_ -> True) (Decode.exact translated.iri)


decoderFieldCollection :
    C
    -> Strictness
    -> Iri
    -> Path
    -> Grouped.DataCollection
    -> Decoder Field
decoderFieldCollection c strictness iriDocument pathParent grouped =
    let
        path =
            Breadcrumbs.Hash grouped.hash :: pathParent
    in
    Decode.succeed DataCollection
        |> Decode.hardcoded path
        |> Decode.hardcoded grouped
        |> Decode.custom Decode.blankNodeOrIri
        |> Decode.custom
            (Decode.zeroOrManyAt grouped.path
                (decoderInstanceCollection c strictness iriDocument path grouped)
                |> Decode.map (adjustPaths strictness)
            )
        |> Decode.map FieldCollection


decoderInstanceCollection :
    C
    -> Strictness
    -> Iri
    -> Path
    -> Grouped.DataCollection
    -> Decoder InstanceCollection
decoderInstanceCollection c strictness iriDocument path grouped =
    let
        withPathNested pathNested =
            Decode.succeed (\_ -> InstanceCollection)
                |> Decode.custom decodeIsInstanceOf
                |> Decode.hardcoded pathNested
                |> Decode.custom Decode.blankNodeOrIri
                |> Decode.custom (decodeField pathNested)
                |> decodeOrderPath
                |> Decode.optionalStringOrLangString RDFS.label

        decodeIsInstanceOf =
            Decode.isInstanceOf (SetIri.fromList (NonEmpty.toList grouped.classes))

        decodeField pathNested =
            Decode.lazy
                (\_ -> decoder c strictness iriDocument pathNested grouped.field)

        decodeOrderPath =
            case grouped.orderPath of
                Nothing ->
                    Decode.hardcoded Nothing

                Just orderPath ->
                    Decode.optionalAt orderPath
                        (Decode.map Just Decode.int)
                        Nothing
    in
    case strictness of
        Strict ->
            withPathNested path

        WithIsDefinedBy ->
            decoderPathNestedInstance iriDocument path
                |> Decode.andThen withPathNested

        Relaxed ->
            withPathNested path


decoderFieldCollectionSelect :
    C
    -> Strictness
    -> Iri
    -> Path
    -> Grouped.DataCollectionSelect
    -> Decoder Field
decoderFieldCollectionSelect c strictness iriDocument pathParent grouped =
    let
        path =
            Breadcrumbs.Hash grouped.hash :: pathParent
    in
    Decode.succeed DataCollectionSelect
        |> Decode.hardcoded path
        |> Decode.hardcoded grouped
        |> Decode.custom Decode.blankNodeOrIri
        |> Decode.custom
            (Decode.zeroOrManyAt grouped.path
                (decoderInstanceCollectionSelect c strictness iriDocument path grouped)
                |> Decode.map (adjustPaths strictness)
            )
        |> Decode.map FieldCollectionSelect


decoderInstanceCollectionSelect :
    C
    -> Strictness
    -> Iri
    -> Path
    -> Grouped.DataCollectionSelect
    -> Decoder InstanceCollectionSelect
decoderInstanceCollectionSelect c strictness iriDocument path grouped =
    let
        withPathNested pathNested =
            decodeField pathNested
                |> Decode.andThen
                    (\field ->
                        case singleSelectOne field of
                            Nothing ->
                                Decode.fail "could not get single select one"

                            Just selectOne ->
                                Decode.succeed (\_ -> InstanceCollectionSelect)
                                    |> Decode.custom decodeIsInstanceOf
                                    |> Decode.hardcoded pathNested
                                    |> Decode.custom Decode.blankNodeOrIri
                                    |> Decode.hardcoded field
                                    |> Decode.hardcoded selectOne
                    )

        decodeField pathNested =
            Decode.lazy
                (\_ -> decoder c strictness iriDocument pathNested grouped.field)

        decodeIsInstanceOf =
            Decode.isInstanceOf (SetIri.fromList (NonEmpty.toList grouped.classes))
    in
    case strictness of
        Strict ->
            withPathNested path

        WithIsDefinedBy ->
            decoderPathNestedInstance iriDocument path
                |> Decode.andThen withPathNested

        Relaxed ->
            withPathNested path


singleSelectOne : Field -> Maybe SingleSelectOne
singleSelectOne field =
    case field of
        FieldCheckbox _ ->
            Nothing

        FieldCheckboxInstance _ ->
            Nothing

        FieldCollection _ ->
            Nothing

        FieldCollectionSelect _ ->
            Nothing

        FieldConstant _ ->
            Nothing

        FieldConstantInjectable _ ->
            Nothing

        FieldDateTime _ ->
            Nothing

        FieldFileUpload _ ->
            Nothing

        FieldForm data ->
            data.fields
                |> List.filterMap singleSelectOne
                |> List.head

        FieldGrid _ ->
            Nothing

        FieldGroup _ ->
            Nothing

        FieldIriReadOnly _ ->
            Nothing

        FieldNumber _ ->
            Nothing

        FieldSchedule _ ->
            Nothing

        FieldSelectMany _ ->
            Nothing

        FieldSelectOne data ->
            Just
                { path = data.path
                , nodeFocus = data.nodeFocus
                , value = data.selected
                }

        FieldSelectOneInjectable _ ->
            Nothing

        FieldSingleNested data ->
            Maybe.andThen singleSelectOne data.field

        FieldText _ ->
            Nothing

        FieldVariants _ ->
            Nothing


decoderFieldConstant :
    Strictness
    -> Path
    -> Translated.DataConstant
    -> Decoder Field
decoderFieldConstant strictness pathParent translated =
    let
        decodeValue =
            case strictness of
                Strict ->
                    Decode.oneAt translated.path (Decode.exact translated.value)

                WithIsDefinedBy ->
                    Decode.zeroOrOneStrictAt translated.path
                        (Decode.exact translated.value)

                Relaxed ->
                    Decode.zeroOrOneStrictAt translated.path
                        (Decode.exact translated.value)
    in
    Decode.succeed (\_ -> DataConstant)
        |> Decode.custom decodeValue
        |> Decode.hardcoded (Breadcrumbs.Hash translated.hash :: pathParent)
        |> Decode.hardcoded translated
        |> Decode.custom Decode.blankNodeOrIri
        |> Decode.map FieldConstant


decoderFieldConstantInjectable :
    Strictness
    -> Path
    -> Translated.DataConstantInjectable
    -> Decoder Field
decoderFieldConstantInjectable strictness pathParent translated =
    let
        decodeValue =
            decodeRequired strictness translated (Decode.exact translated.value.this)

        decodeLabel =
            Decode.from translated.value.this
                (Decode.oneOf
                    [ Decode.map Just
                        (Decode.predicate RDFS.label Decode.stringOrLangString)
                    , Decode.succeed Nothing
                    ]
                )
    in
    Decode.succeed (\_ -> DataConstantInjectable)
        |> Decode.custom decodeValue
        |> Decode.hardcoded (Breadcrumbs.Hash translated.hash :: pathParent)
        |> Decode.hardcoded translated
        |> Decode.custom Decode.blankNodeOrIri
        |> Decode.custom decodeLabel
        |> Decode.map FieldConstantInjectable


decoderFieldDateTime : Strictness -> Path -> Translated.DataDateTime -> Decoder Field
decoderFieldDateTime strictness pathParent translated =
    Decode.succeed DataDateTime
        |> Decode.hardcoded (Breadcrumbs.Hash translated.hash :: pathParent)
        |> Decode.hardcoded translated
        |> Decode.custom Decode.blankNodeOrIri
        |> Decode.custom
            (decodeOptional strictness translated (decoderDateTime translated))
        |> Decode.map FieldDateTime


decoderFieldFileUpload : Strictness -> Path -> Translated.DataFileUpload -> Decoder Field
decoderFieldFileUpload strictness pathParent translated =
    Decode.succeed DataFileUpload
        |> Decode.hardcoded (Breadcrumbs.Hash translated.hash :: pathParent)
        |> Decode.hardcoded translated
        |> Decode.custom Decode.blankNodeOrIri
        |> Decode.custom (decodeOptional strictness translated decoderFileUpload)
        |> Decode.map FieldFileUpload


decoderFieldForm :
    C
    -> Strictness
    -> Iri
    -> Path
    -> Grouped.DataForm
    -> Decoder Field
decoderFieldForm c strictness iriDocument pathParent grouped =
    decoderDataForm c strictness iriDocument pathParent grouped
        |> Decode.map FieldForm


decoderDataForm :
    C
    -> Strictness
    -> Iri
    -> Path
    -> Grouped.DataForm
    -> Decoder DataForm
decoderDataForm c strictness iriDocument pathParent grouped =
    let
        decodeSuccesses =
            grouped.successes
                |> List.map (.field >> decodeField)
                |> Decode.combine

        decodeField field =
            Decode.lazy (\_ -> decoder c strictness iriDocument pathParent field)
    in
    Decode.succeed DataForm
        |> Decode.hardcoded pathParent
        |> Decode.hardcoded grouped
        |> Decode.custom Decode.blankNodeOrIri
        |> Decode.custom decodeSuccesses


decoderFieldGrid : C -> Strictness -> Iri -> Path -> Grouped.DataGrid -> Decoder Field
decoderFieldGrid c strictness iriDocument pathParent grouped =
    -- FIXME Handle strictness == Bool case
    let
        path =
            Breadcrumbs.Hash grouped.hash :: pathParent
    in
    Decode.andThen
        (\gridCells ->
            Decode.succeed
                (\nodeFocus instances ->
                    FieldGrid
                        { path = path
                        , grouped = grouped
                        , nodeFocus = nodeFocus
                        , gridCells = gridCells
                        , instances = Dict.fromList instances
                        }
                )
                |> Decode.custom Decode.blankNodeOrIri
                |> Decode.custom
                    (Decode.zeroOrManyAt grouped.path
                        (decoderInstanceGrid c strictness iriDocument path grouped gridCells)
                    )
        )
        (decoderGridCells grouped)


decoderGridCells : Grouped.DataGrid -> Decoder GridCells
decoderGridCells grouped =
    let
        manySorted =
            Decode.many decoderGridColumn
                |> Decode.map (List.sortBy Tuple.first >> List.map Tuple.second)
    in
    Decode.succeed GridCells
        |> Decode.requiredAt grouped.dataEditorGrid.availableColumnsPath manySorted
        |> Decode.requiredAt grouped.dataEditorGrid.availableRowsPath manySorted


decoderGridColumn : Decoder ( Int, GridColumn )
decoderGridColumn =
    Decode.succeed Tuple.pair
        |> Decode.required (herbie "order") Decode.int
        |> Decode.custom
            (Decode.succeed GridColumn
                |> Decode.custom Decode.iri
                |> Decode.required RDFS.label Decode.stringOrLangString
            )


type alias Cell =
    { iri : BlankNodeOrIri
    , column : Iri
    , row : Iri
    }


decoderInstanceGrid :
    C
    -> Strictness
    -> Iri
    -> Path
    -> Grouped.DataGrid
    -> GridCells
    -> Decoder ( IndexGrid, InstanceGrid )
decoderInstanceGrid c strictness iriDocument path grouped { gridColumns, gridRows } =
    let
        cellToIndexGrid cell =
            Maybe.map2
                (\row column ->
                    ( row, column )
                )
                (List.findIndex (\row -> row.iri == cell.row) gridRows)
                (List.findIndex (\column -> column.iri == cell.column) gridColumns)
                |> Maybe.map Decode.succeed
                |> Maybe.withDefault (Decode.fail "could not determine cell position")

        decodeCell =
            Decode.succeed Cell
                |> Decode.custom Decode.blankNodeOrIri
                |> Decode.required grouped.dataEditorGrid.columnPath Decode.iri
                |> Decode.required grouped.dataEditorGrid.rowPath Decode.iri
                |> Decode.andThen cellToIndexGrid

        decodeInstanceGrid pathNested =
            Decode.succeed (\_ -> InstanceGrid)
                |> Decode.custom decodeInstanceOf
                |> Decode.hardcoded pathNested
                |> Decode.custom Decode.blankNodeOrIri
                |> Decode.custom (decodeField pathNested)
                |> Decode.optionalStringOrLangString RDFS.label

        decodeInstanceOf =
            Decode.isInstanceOf (SetIri.fromList (NonEmpty.toList grouped.classes))

        decodeField pathNested =
            Decode.lazy
                (\_ -> decoder c strictness iriDocument pathNested grouped.field)
    in
    decoderPathNestedInstanceGrid iriDocument path
        |> Decode.andThen
            (\pathNested ->
                Decode.succeed Tuple.pair
                    |> Decode.custom decodeCell
                    |> Decode.custom (decodeInstanceGrid pathNested)
            )


decoderPathNestedInstanceGrid : Iri -> Path -> Decoder Path
decoderPathNestedInstanceGrid iriDocument path =
    let
        withIsDefinedBy isDefinedBys =
            let
                urlParent =
                    Rdf.setFragment (Breadcrumbs.id path) iriDocument
            in
            isDefinedBys
                |> List.map
                    (\isDefinedBy ->
                        if
                            String.startsWith
                                (Rdf.toUrl urlParent)
                                (Rdf.toUrl isDefinedBy)
                        then
                            case gridIndexFromUrl isDefinedBy urlParent of
                                Just { row, column } ->
                                    Decode.succeed
                                        (Breadcrumbs.Ordinal column
                                            :: Breadcrumbs.Ordinal row
                                            :: path
                                        )

                                Nothing ->
                                    Decode.fail "Could not determine index of grid instance"

                        else
                            Decode.fail "Wrong rdfs:isDefinedBy of grid instance"
                    )
                |> Decode.oneOf

        gridIndexFromUrl isDefinedBy urlParent =
            case
                String.rightOf
                    (Rdf.toUrl urlParent)
                    (Rdf.toUrl isDefinedBy)
                    |> String.dropLeft 1
                    |> String.split "-"
                    |> List.filterMap String.toInt
            of
                [ row, column ] ->
                    Just { row = row, column = column }

                _ ->
                    Nothing
    in
    Decode.predicate RDFS.isDefinedBy (Decode.many Decode.iri)
        |> Decode.andThen withIsDefinedBy


decoderFieldGroup :
    C
    -> Strictness
    -> Iri
    -> Path
    -> Grouped.DataGroup
    -> Decoder Field
decoderFieldGroup c strictness iriDocument pathParent grouped =
    let
        decodeField =
            Decode.lazy
                (\_ -> decoder c strictness iriDocument pathParent grouped.field)
    in
    Decode.succeed DataGroup
        |> Decode.hardcoded pathParent
        |> Decode.hardcoded grouped
        |> Decode.custom Decode.blankNodeOrIri
        |> Decode.custom decodeField
        |> Decode.map FieldGroup


decoderFieldIriReadOnly :
    Strictness
    -> Path
    -> Translated.DataIriReadOnly
    -> Decoder Field
decoderFieldIriReadOnly strictness pathParent translated =
    Decode.succeed DataIriReadOnly
        |> Decode.hardcoded (Breadcrumbs.Hash translated.hash :: pathParent)
        |> Decode.hardcoded translated
        |> Decode.custom Decode.blankNodeOrIri
        |> Decode.custom
            (decodeRequired strictness translated decoderIriReadOnly)
        |> Decode.map FieldIriReadOnly


decoderFieldNumber : C -> Strictness -> Path -> Translated.DataNumber -> Decoder Field
decoderFieldNumber c strictness pathParent translated =
    Decode.succeed DataNumber
        |> Decode.hardcoded (Breadcrumbs.Hash translated.hash :: pathParent)
        |> Decode.hardcoded translated
        |> Decode.custom Decode.blankNodeOrIri
        |> Decode.custom
            (decodeOptional strictness translated (decoderNumber c translated))
        |> Decode.hardcoded Nothing
        |> Decode.map FieldNumber


decoderFieldSchedule :
    C
    -> Strictness
    -> Iri
    -> Path
    -> Grouped.DataSchedule
    -> Decoder Field
decoderFieldSchedule c strictness iriDocument pathParent grouped =
    let
        path =
            Breadcrumbs.Hash grouped.hash :: pathParent

        decodeEditorDatePath =
            Decode.zeroOrOneAt grouped.editorDatePath
                (Decode.map (Date.fromPosix Time.utc) Decode.date)
    in
    Decode.succeed DataSchedule
        |> Decode.hardcoded path
        |> Decode.hardcoded grouped
        |> Decode.custom Decode.blankNodeOrIri
        |> Decode.custom decodeEditorDatePath
        |> Decode.custom
            (Decode.zeroOrManyAt grouped.path
                (decoderInstanceSchedule c strictness iriDocument path grouped)
                |> Decode.map (adjustPaths strictness)
            )
        |> Decode.map FieldSchedule


decoderInstanceSchedule :
    C
    -> Strictness
    -> Iri
    -> Path
    -> Grouped.DataSchedule
    -> Decoder InstanceSchedule
decoderInstanceSchedule c strictness iriDocument path grouped =
    let
        withPathNested pathNested =
            Decode.succeed (\_ -> InstanceSchedule)
                |> Decode.custom decodeIsInstanceOf
                |> Decode.hardcoded pathNested
                |> Decode.custom Decode.blankNodeOrIri
                |> Decode.custom (decodeField pathNested)
                |> Decode.requiredAt grouped.editorDayOffsetPath Decode.int
                |> Decode.requiredAt grouped.editorOrderPath Decode.int
                |> Decode.optionalStringOrLangString RDFS.label

        decodeIsInstanceOf =
            Decode.isInstanceOf (SetIri.fromList (NonEmpty.toList grouped.classes))

        decodeField pathNested =
            Decode.lazy
                (\_ -> decoder c strictness iriDocument pathNested grouped.field)
    in
    case strictness of
        Strict ->
            withPathNested path

        WithIsDefinedBy ->
            decoderPathNestedInstance iriDocument path
                |> Decode.andThen withPathNested

        Relaxed ->
            withPathNested path


decoderFieldSelectMany : Path -> Translated.DataSelectMany -> Decoder Field
decoderFieldSelectMany pathParent translated =
    Decode.succeed DataSelectMany
        |> Decode.hardcoded (Breadcrumbs.Hash translated.hash :: pathParent)
        |> Decode.hardcoded translated
        |> Decode.custom Decode.blankNodeOrIri
        |> Decode.custom
            (Decode.zeroOrManyAt translated.path
                (decoderSelectMany translated)
            )
        |> Decode.map FieldSelectMany


decoderFieldSelectOne :
    Strictness
    -> Path
    -> Translated.DataSelectOne
    -> Decoder Field
decoderFieldSelectOne strictness pathParent translated =
    Decode.succeed DataSelectOne
        |> Decode.hardcoded (Breadcrumbs.Hash translated.hash :: pathParent)
        |> Decode.hardcoded translated
        |> Decode.custom Decode.blankNodeOrIri
        |> Decode.custom
            (decodeOptional strictness translated (decoderSelectOne translated))
        |> Decode.map FieldSelectOne


decoderFieldSelectOneInjectable :
    Strictness
    -> Path
    -> Translated.DataSelectOneInjectable
    -> Decoder Field
decoderFieldSelectOneInjectable strictness pathParent translated =
    Decode.succeed DataSelectOneInjectable
        |> Decode.hardcoded (Breadcrumbs.Hash translated.hash :: pathParent)
        |> Decode.hardcoded translated
        |> Decode.custom Decode.blankNodeOrIri
        |> Decode.custom
            (decodeRequired strictness
                translated
                (decoderSelectOneInjectable translated)
            )
        |> Decode.map FieldSelectOneInjectable


decoderFieldSingleNested :
    C
    -> Strictness
    -> Iri
    -> Path
    -> Grouped.DataSingleNested
    -> Decoder Field
decoderFieldSingleNested c strictness iriDocument pathParent grouped =
    Decode.succeed DataSingleNested
        |> Decode.hardcoded (Breadcrumbs.Hash grouped.hash :: pathParent)
        |> Decode.hardcoded grouped
        |> Decode.custom Decode.blankNodeOrIri
        |> Decode.custom
            (Decode.zeroOrOneAt grouped.path
                (decoderSingleNested c strictness iriDocument pathParent grouped)
            )
        |> Decode.map FieldSingleNested


decoderSingleNested :
    C
    -> Strictness
    -> Iri
    -> Path
    -> Grouped.DataSingleNested
    -> Decoder Field
decoderSingleNested c strictness iriDocument pathParent grouped =
    let
        path =
            Breadcrumbs.Hash grouped.hash :: pathParent

        decodeIsInstanceOf =
            Decode.isInstanceOf (SetIri.fromList grouped.classes)

        decodeField =
            Decode.lazy
                (\_ -> decoder c strictness iriDocument path grouped.field)
    in
    case strictness of
        Strict ->
            Decode.succeed (\_ -> identity)
                |> Decode.custom decodeIsInstanceOf
                |> Decode.custom decodeField

        WithIsDefinedBy ->
            Decode.succeed (\_ _ -> identity)
                |> Decode.custom decodeIsInstanceOf
                |> Decode.custom (decodeIsDefinedBy iriDocument path)
                |> Decode.custom decodeField

        Relaxed ->
            Decode.succeed (\_ -> identity)
                |> Decode.custom decodeIsInstanceOf
                |> Decode.custom decodeField


decoderFieldText : Strictness -> Path -> Translated.DataText -> Decoder Field
decoderFieldText strictness pathParent translated =
    Decode.succeed DataText
        |> Decode.hardcoded (Breadcrumbs.Hash translated.hash :: pathParent)
        |> Decode.hardcoded translated
        |> Decode.custom Decode.blankNodeOrIri
        |> Decode.custom (decodeOptional strictness translated decoderText)
        |> Decode.map FieldText


decoderText : Decoder String
decoderText =
    Decode.string


decoderFieldVariants :
    C
    -> Strictness
    -> Iri
    -> Path
    -> Grouped.DataVariants
    -> Decoder Field
decoderFieldVariants c strictness iriDocument pathParent grouped =
    let
        path =
            Breadcrumbs.Hash grouped.hash :: pathParent
    in
    Decode.succeed DataVariants
        |> Decode.hardcoded path
        |> Decode.hardcoded grouped
        |> Decode.custom Decode.blankNodeOrIri
        |> Decode.custom
            (decoderVariantSelected c strictness iriDocument path grouped)
        |> Decode.map FieldVariants


decoderVariantSelected :
    C
    -> Strictness
    -> Iri
    -> Path
    -> Grouped.DataVariants
    -> Decoder VariantSelected
decoderVariantSelected c strictness iriDocument path grouped =
    let
        decoderAllVariants =
            Decode.oneOf
                (List.indexedMap
                    (decoderVariant c strictness iriDocument path)
                    (NonEmpty.toList grouped.variants)
                )
    in
    case strictness of
        Strict ->
            decoderAllVariants

        WithIsDefinedBy ->
            Decode.oneOf
                [ decoderAllVariants
                , decoderVariant c Relaxed iriDocument path 0 (NonEmpty.head grouped.variants)
                ]

        Relaxed ->
            decoderAllVariants


decoderVariant :
    C
    -> Strictness
    -> Iri
    -> Path
    -> Int
    -> Grouped.Variant
    -> Decoder VariantSelected
decoderVariant c strictness iriDocument path index variant =
    let
        pathNested =
            Breadcrumbs.Hash variant.hash :: path
    in
    case strictness of
        Strict ->
            decoderVariantHelp c strictness iriDocument index variant pathNested

        WithIsDefinedBy ->
            decodeIsDefinedBy iriDocument pathNested
                |> Decode.andThen
                    (\_ ->
                        decoderVariantHelp c strictness iriDocument index variant pathNested
                    )

        Relaxed ->
            decoderVariantHelp c strictness iriDocument index variant pathNested


decoderVariantHelp :
    C
    -> Strictness
    -> Iri
    -> Int
    -> Grouped.Variant
    -> Path
    -> Decoder VariantSelected
decoderVariantHelp c strictness iriDocument index variant pathNested =
    let
        decodeIsInstanceOf =
            Decode.isInstanceOf
                (SetIri.fromList
                    (variant.classes ++ variant.targetClasses)
                )

        decodeField =
            Decode.lazy
                (\_ -> decoder c strictness iriDocument pathNested variant.field)
    in
    Decode.succeed (\_ -> VariantSelected)
        |> Decode.custom decodeIsInstanceOf
        |> Decode.hardcoded pathNested
        |> Decode.hardcoded index
        |> Decode.hardcoded variant
        |> Decode.custom decodeField


decoderDateTime : Translated.DataDateTime -> Decoder Posix
decoderDateTime translated =
    if translated.withTime then
        Decode.dateTime

    else
        Decode.date


decoderFileUpload : Decoder NamedFile
decoderFileUpload =
    Decode.succeed NamedFile
        |> Decode.custom Decode.iri
        |> Decode.optional RDFS.label (Decode.map Just Decode.string) Nothing


decoderIriReadOnly : Decoder LabelledIri
decoderIriReadOnly =
    Decode.succeed LabelledIri
        |> Decode.custom Decode.iri
        |> Decode.optionalStringOrLangString RDFS.label


decoderNumber : C -> Translated.DataNumber -> Decoder String
decoderNumber c translated =
    Decode.anyLiteral
        |> Decode.andThen (Rdf.toValue >> localizeValue c translated.numberType)


localizeValue : C -> Translated.NumberType -> String -> Decoder String
localizeValue c numberType value =
    case numberType of
        Translated.NumberInteger ->
            value
                |> String.toInt
                |> Maybe.map (String.fromInt >> Decode.succeed)
                |> Maybe.withDefault
                    (Decode.fail ("expected an integer but got " ++ value))

        Translated.NumberFloat ->
            let
                fixDecimal =
                    case c.locale of
                        Locale.De ->
                            String.replace "." ","

                        Locale.EnUS ->
                            identity
            in
            value
                |> String.toFloat
                |> Maybe.map (String.fromFloat >> fixDecimal >> Decode.succeed)
                |> Maybe.withDefault
                    (Decode.fail ("expected a float but got " ++ value))

        Translated.NumberDecimal ->
            value
                |> Decimal.fromString Locale.EnUS
                |> Maybe.map (Decimal.toString c.locale >> Decode.succeed)
                |> Maybe.withDefault
                    (Decode.fail ("expected a decimal but got " ++ value))


decoderSelectMany : Translated.DataSelectMany -> Decoder LabelledIri
decoderSelectMany translated =
    Decode.isInstanceOf (SetIri.singleton translated.class)
        |> Decode.andThen
            (\_ ->
                Decode.succeed LabelledIri
                    |> Decode.custom Decode.iri
                    |> Decode.optionalStringOrLangString RDFS.label
            )


decoderSelectOne : Translated.DataSelectOne -> Decoder LabelledIri
decoderSelectOne translated =
    Decode.isInstanceOf (SetIri.singleton translated.class)
        |> Decode.andThen
            (\_ ->
                Decode.succeed LabelledIri
                    |> Decode.custom Decode.iri
                    |> Decode.optionalStringOrLangString RDFS.label
            )


decoderSelectOneInjectable :
    Translated.DataSelectOneInjectable
    -> Decoder Translated.Value
decoderSelectOneInjectable _ =
    -- FIXME We should check that the iri matches one of the options in
    -- translated and get the label from there
    Decode.succeed Translated.Value
        |> Decode.custom Decode.iri
        |> Decode.required RDFS.label Decode.stringOrLangString


decoderPathNestedInstance : Iri -> Path -> Decoder Path
decoderPathNestedInstance iriDocument path =
    let
        withIsDefinedBy isDefinedBys =
            isDefinedBys
                |> List.map
                    (\isDefinedBy ->
                        let
                            urlParent =
                                Rdf.setFragment (Breadcrumbs.id path) iriDocument
                        in
                        if
                            String.startsWith
                                (Rdf.toUrl urlParent)
                                (Rdf.toUrl isDefinedBy)
                        then
                            case indexFromUrl isDefinedBy urlParent of
                                Nothing ->
                                    Decode.fail "Could not determine index of collection instance"

                                Just index ->
                                    Decode.succeed (Breadcrumbs.Ordinal index :: path)

                        else
                            Decode.fail "Wrong rdfs:isDefinedBy of collection instance"
                    )
                |> Decode.oneOf

        indexFromUrl isDefinedBy urlParent =
            String.rightOf
                (Rdf.toUrl urlParent)
                (Rdf.toUrl isDefinedBy)
                |> String.dropLeft 1
                |> String.toInt
    in
    Decode.predicate RDFS.isDefinedBy (Decode.many Decode.iri)
        |> Decode.andThen withIsDefinedBy


decodeRequired :
    Strictness
    -> { r | path : PropertyPath }
    -> Decoder a
    -> Decoder (Maybe a)
decodeRequired strictness translated decoderA =
    case strictness of
        Strict ->
            Decode.oneAt translated.path decoderA

        WithIsDefinedBy ->
            Decode.zeroOrOneAt translated.path decoderA

        Relaxed ->
            Decode.zeroOrOneAt translated.path decoderA


decodeOptional :
    Strictness
    -> { r | optional : Translated.Optional, path : PropertyPath }
    -> Decoder a
    -> Decoder (Maybe a)
decodeOptional strictness translated decoderA =
    case strictness of
        Strict ->
            case translated.optional of
                Translated.OptionalByItself ->
                    Decode.zeroOrOneAt translated.path decoderA

                Translated.OptionalViaNesting ->
                    Decode.oneAt translated.path decoderA

                Translated.Required ->
                    Decode.oneAt translated.path decoderA

        WithIsDefinedBy ->
            Decode.zeroOrOneAt translated.path decoderA

        Relaxed ->
            Decode.zeroOrOneAt translated.path decoderA


adjustPaths : Strictness -> List { r | path : Path } -> List { r | path : Path }
adjustPaths strictness =
    let
        adjustPath index instance =
            { instance
                | path =
                    Breadcrumbs.Ordinal index :: instance.path
            }
    in
    case strictness of
        Strict ->
            List.indexedMap adjustPath

        WithIsDefinedBy ->
            identity

        Relaxed ->
            List.indexedMap adjustPath


labelledIriFromValue : { this : Iri, label : StringOrLangString } -> LabelledIri
labelledIriFromValue { this, label } =
    { iri = this
    , label = Just label
    }


decodeIsDefinedBy : Iri -> Path -> Decoder ()
decodeIsDefinedBy iriDocument path =
    let
        iriExpected =
            Rdf.setFragment (Breadcrumbs.id path) iriDocument
    in
    Decode.andThen
        (\iris ->
            if SetIri.member iriExpected (SetIri.fromList iris) then
                Decode.succeed ()

            else
                Decode.fail "rdfs:isDefinedBy missing"
        )
        (Decode.zeroOrMany RDFS.isDefinedBy Decode.iri)
