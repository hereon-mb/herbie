module Shacl.Form.Effect exposing
    ( Effect(..), Option
    , none, batch, map, fromCmd
    , createField, duplicateField, userIntentsToCreateDatasetFor
    , getResource, getSubclassesWithShaclShapes, createFile, getGraph
    , focus, copyToClipboard, addToastError, tooltipMsg
    , ConfigGetGraph, fromMsg
    )

{-|

@docs Effect, Option
@docs none, batch, map, fromCmd
@docs createField, duplicateField, userIntentsToCreateDatasetFor
@docs getResource, getSubclassesWithShaclShapes, createFile, getGraph
@docs focus, copyToClipboard, addToastError, tooltipMsg

-}

import Accessibility.Blob exposing (Blob)
import Api
import File exposing (File)
import Ontology.Shacl exposing (Class)
import Rdf exposing (Iri)
import Rdf.Graph exposing (Graph)
import Shacl.Form.Breadcrumbs exposing (Path)
import Shacl.Form.Decoded as Decoded
import Shacl.Form.Grouped as Grouped
import Toast
import Ui.Atom.Tooltip as Tooltip


type Effect msg
    = None
    | Batch (List (Effect msg))
    | Cmd (Cmd msg)
    | Msg msg
      -- FORM
    | CreateField (Api.Response Decoded.Field -> msg) Bool Path Decoded.Mint Grouped.Field
    | DuplicateField (Api.Response Decoded.Field -> msg) Bool Path Decoded.Mint Decoded.Field
    | UserIntentsToCreateDatasetFor (List Class)
      -- RESOURCES
    | GetResource (Maybe (List Option) -> msg) Iri
    | GetSubclassesWithShaclShapes (Api.Response (List Class) -> msg) Iri
    | CreateFile (Api.Response Graph -> msg) File
    | GetGraph ConfigGetGraph (Api.Response Graph -> msg) Iri
      -- UI
    | Focus String
    | CopyToClipboard Blob
    | AddToast Toast.Content
    | AddToastError Api.Error
    | TooltipMsg Tooltip.Msg


type alias Option =
    { iri : Iri
    , value : String
    }


type alias ConfigGetGraph =
    { store : Bool
    , reload : Bool
    , closure : Bool
    }


none : Effect msg
none =
    None


batch : List (Effect msg) -> Effect msg
batch =
    Batch


map : (a -> b) -> Effect a -> Effect b
map f effect =
    case effect of
        None ->
            None

        Batch effects ->
            Batch (List.map (map f) effects)

        Cmd cmd ->
            Cmd (Cmd.map f cmd)

        Msg msg ->
            Msg (f msg)

        -- FORM
        CreateField onField preview path mint field ->
            CreateField (onField >> f) preview path mint field

        DuplicateField onField preview path mint field ->
            DuplicateField (onField >> f) preview path mint field

        UserIntentsToCreateDatasetFor classes ->
            UserIntentsToCreateDatasetFor classes

        -- RESOURCES
        GetResource onOptions class ->
            GetResource (onOptions >> f) class

        GetSubclassesWithShaclShapes onClasses class ->
            GetSubclassesWithShaclShapes (onClasses >> f) class

        CreateFile onFile file ->
            CreateFile (onFile >> f) file

        GetGraph config onGraph iri ->
            GetGraph config (onGraph >> f) iri

        -- UI
        Focus id ->
            Focus id

        CopyToClipboard blob ->
            CopyToClipboard blob

        AddToast toast ->
            AddToast toast

        AddToastError error ->
            AddToastError error

        TooltipMsg msg ->
            TooltipMsg msg


fromCmd : Cmd msg -> Effect msg
fromCmd =
    Cmd


fromMsg : msg -> Effect msg
fromMsg =
    Msg


createField :
    (Api.Response Decoded.Field -> msg)
    -> Bool
    -> Path
    -> Decoded.Mint
    -> Grouped.Field
    -> Effect msg
createField =
    CreateField


duplicateField :
    (Api.Response Decoded.Field -> msg)
    -> Bool
    -> Path
    -> Decoded.Mint
    -> Decoded.Field
    -> Effect msg
duplicateField =
    DuplicateField


userIntentsToCreateDatasetFor : List Class -> Effect msg
userIntentsToCreateDatasetFor =
    UserIntentsToCreateDatasetFor


getResource : (Maybe (List Option) -> msg) -> Iri -> Effect msg
getResource =
    GetResource


getSubclassesWithShaclShapes : (Api.Response (List Class) -> msg) -> Iri -> Effect msg
getSubclassesWithShaclShapes =
    GetSubclassesWithShaclShapes


createFile : (Api.Response Graph -> msg) -> File -> Effect msg
createFile =
    CreateFile


getGraph : ConfigGetGraph -> (Api.Response Graph -> msg) -> Iri -> Effect msg
getGraph =
    GetGraph


focus : String -> Effect msg
focus =
    Focus


copyToClipboard : Blob -> Effect msg
copyToClipboard =
    CopyToClipboard


addToastError : Api.Error -> Effect msg
addToastError =
    AddToastError


tooltipMsg : Tooltip.Msg -> Effect msg
tooltipMsg =
    TooltipMsg
