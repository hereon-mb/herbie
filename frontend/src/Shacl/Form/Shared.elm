module Shacl.Form.Shared exposing
    ( init, initWith
    , Shared
    , Config
    , withInfo
    , Option
    , sortOptionsByScore
    , optionFromOptionCombobox, optionToOptionCombobox
    , optionFromString, optionToString
    , Static
    )

{-|

@docs init, initWith

@docs Shared

@docs Config

@docs withInfo

@docs Option
@docs sortOptionsByScore
@docs optionFromOptionCombobox, optionToOptionCombobox
@docs optionFromString, optionToString

@docs helpFromValidationResult

-}

import Element exposing (Element)
import Fluent exposing (Fluent, verbatim)
import List.Extra as List
import Ontology exposing (Ontology)
import Pretty
import Query
import Rdf exposing (BlankNodeOrIri, Iri)
import Rdf.Decode as Decode exposing (Decoder)
import Rdf.DictIri exposing (DictIri)
import Rdf.Encode exposing (PropertyEncoder)
import Rdf.Graph exposing (Graph, Seed)
import Rdf.PropertyPath exposing (PropertyPath)
import Shacl exposing (Shapes)
import Shacl.Form.Breadcrumbs exposing (Breadcrumbs, Path)
import Shacl.Form.Collected as Collected
import Shacl.Form.Context exposing (C)
import Shacl.Form.Decoded as Decoded
import Shacl.Form.Grouped as Grouped
import Shacl.Form.Translated as Translated
import Shacl.Form.Updated exposing (Updated)
import Shacl.Report exposing (Report)
import UUID
import Ui.Atom.OptionCombobox exposing (OptionCombobox)
import Ui.Atom.Tooltip as Tooltip
import Ui.Molecule.Table as Table
import Ui.Molecule.TableCell as TableCell exposing (TableCell)


init :
    { value : C -> Shared -> decoded -> Maybe a
    , default : C -> Shared -> decoded -> Maybe a
    , path : decoded -> PropertyPath
    , withValue :
        Config e f m field msg
        -> C
        -> Shared
        -> Seed
        -> Breadcrumbs
        -> decoded
        -> a
        -> Updated field m
    , empty :
        Config e f m field msg
        -> C
        -> Shared
        -> Seed
        -> Breadcrumbs
        -> decoded
        -> Updated field m
    }
    -> Config e f m field msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Breadcrumbs
    -> decoded
    -> Updated field m
init f config { populateDefaultValues } c shared seed breadcrumbs decoded =
    case f.value c shared decoded of
        Nothing ->
            case f.default c shared decoded of
                Nothing ->
                    f.empty config c shared seed breadcrumbs decoded

                Just value ->
                    if populateDefaultValues then
                        f.withValue config c shared seed breadcrumbs decoded value

                    else
                        f.empty config c shared seed breadcrumbs decoded

        Just value ->
            f.withValue config c shared seed breadcrumbs decoded value


initWith :
    { decoder : C -> Shared -> field -> Decoder a
    , default : C -> Shared -> field -> Maybe a
    , nodeFocus : field -> BlankNodeOrIri
    , path : field -> PropertyPath
    , updateValue :
        Config error form msg field msgField
        -> C
        -> Shared
        -> Seed
        -> a
        -> field
        -> Maybe (Updated field msg)
    , removeValue :
        Config error form msg field msgField
        -> C
        -> Shared
        -> Seed
        -> field
        -> Maybe (Updated field msg)
    , tagField : Translated.TagField
    }
    -> Config error form msg field msgField
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> field
    -> Result error (Maybe (Updated field msg))
initWith cfg config { populateDefaultValues } c shared seed field =
    case
        Decode.decode (Decode.from (cfg.nodeFocus field) (cfg.decoder c shared field))
            shared.graphCombinedBackend
    of
        Ok value ->
            Ok (cfg.updateValue config c shared seed value field)

        Err errorDecode ->
            let
                initFromDefault =
                    Result.mapError (config.toError cfg.tagField (cfg.nodeFocus field) (cfg.path field))
                        (case cfg.default c shared field of
                            Nothing ->
                                Ok (cfg.removeValue config c shared seed field)

                            Just value ->
                                if populateDefaultValues then
                                    Ok (cfg.updateValue config c shared seed value field)

                                else
                                    Ok (cfg.removeValue config c shared seed field)
                        )
            in
            case errorDecode of
                Decode.UnknownProperty _ _ ->
                    initFromDefault

                Decode.NodeDoesNotExist _ ->
                    initFromDefault

                Decode.CustomError _ ->
                    -- FIXME We have to catch this to recover from no instance found
                    initFromDefault

                _ ->
                    Err (config.toError cfg.tagField (cfg.nodeFocus field) (cfg.path field) errorDecode)


type alias Shared =
    { preview : Bool
    , graphOntology : Graph
    , ontology : Ontology
    , shapes : Shapes
    , resources : DictIri (List Option)
    , iri : Iri
    , graph : Graph
    , graphEntered : Graph
    , graphPersisted : Graph
    , graphGenerated : Graph
    , graphSupport : Graph
    , graphFrontend : Graph
    , graphCombinedBackend : Graph
    , graphCombinedFrontend : Graph
    , editForm : Bool
    , spec : Collected.Spec
    , workspaceUuid : String
    , seeds : UUID.Seeds
    }


type alias Config error form msg field msgField =
    { -- INIT
      init :
        Static
        -> C
        -> Shared
        -> Seed
        -> Breadcrumbs
        -> Decoded.Field
        -> Updated form msg
    , initWith :
        Static
        -> C
        -> Shared
        -> Seed
        -> form
        -> Result error (Maybe (Updated form msg))

    -- UPDATE
    , update : C -> Shared -> Seed -> msg -> form -> Updated form msg
    , expandAll : form -> form

    -- SUBSCRIPTIONS
    , subscriptions : form -> Sub msg

    -- VIEW
    , view : C -> Report -> Bool -> form -> Maybe (Element msg)
    , fullscreen : C -> Report -> Bool -> form -> Maybe (Element msg)
    , card : C -> form -> Maybe (Element msg)
    , columns : C -> List String -> Grouped.Field -> List (Table.Column form msg)
    , help : C -> Report -> form -> List Fluent
    , summary : C -> form -> List Fluent

    -- OUTPUT
    , encoders : C -> form -> List PropertyEncoder

    -- INFO
    , isEmpty : form -> Bool
    , nodeFocus : form -> BlankNodeOrIri
    , path : form -> Path
    , toField : form -> Maybe field
    , dragging : form -> Bool
    , log : form -> Pretty.Doc ()

    -- MSG
    , msgTooltip : Tooltip.Msg -> msg
    , userPressedLink : BlankNodeOrIri -> msg
    , toMsg : msgField -> msg
    , toError :
        Translated.TagField
        -> BlankNodeOrIri
        -> PropertyPath
        -> Decode.Error
        -> error
    }


type alias Static =
    { populateDefaultValues : Bool
    }



-- TABLES


withInfo : List String -> TableCell msg -> TableCell msg
withInfo scope =
    case scope of
        [] ->
            identity

        _ ->
            scope
                |> List.reverse
                |> String.join " - "
                |> verbatim
                |> TableCell.withInfo



-- COMBOBOX OPTIONS


type alias Option =
    { iri : Iri
    , value : String
    }


sortOptionsByScore : String -> List Option -> List Option
sortOptionsByScore =
    Query.sortByScore
        [ .value >> Just
        , .iri >> Rdf.serializeNode >> Just
        ]


optionToOptionCombobox : Option -> OptionCombobox
optionToOptionCombobox option =
    { id = Rdf.serializeNode option.iri
    , value = option.value
    , label = verbatim option.value
    , subLabel = verbatim (Rdf.serializeNode option.iri)
    }


optionFromOptionCombobox : List Option -> OptionCombobox -> Maybe Option
optionFromOptionCombobox options optionCombobox =
    List.find (\option -> Rdf.serializeNode option.iri == optionCombobox.id) options


optionToString : Option -> String
optionToString =
    .iri >> Rdf.serializeNode


optionFromString : List Option -> String -> Maybe Option
optionFromString options str =
    List.find (\option -> Rdf.serializeNode option.iri == str) options
