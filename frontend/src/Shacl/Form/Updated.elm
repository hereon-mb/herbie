module Shacl.Form.Updated exposing
    ( Updated, from
    , withEffect, needsPersisting
    , map, mapEffect
    )

{-|

@docs Updated, from
@docs withEffect, needsPersisting
@docs map, mapEffect

-}

import Rdf.Graph exposing (Seed)
import Shacl.Form.Effect as Effect exposing (Effect)


{-| TODO
-}
type alias Updated model msg =
    { model : model
    , effect : Effect msg
    , seed : Seed
    , needsPersisting : Bool
    }


{-| TODO
-}
from : Seed -> model -> Updated model msg
from seed model =
    { model = model
    , effect = Effect.none
    , seed = seed
    , needsPersisting = False
    }


{-| TODO
-}
withEffect : Effect msg -> Updated model msg -> Updated model msg
withEffect effect updated =
    { updated | effect = Effect.batch [ updated.effect, effect ] }


{-| TODO
-}
needsPersisting : Updated model msg -> Updated model msg
needsPersisting updated =
    { updated | needsPersisting = True }


{-| TODO
-}
map : (modelA -> modelB) -> Updated modelA msg -> Updated modelB msg
map toModelB updatedA =
    { model = toModelB updatedA.model
    , effect = updatedA.effect
    , seed = updatedA.seed
    , needsPersisting = updatedA.needsPersisting
    }


{-| TODO
-}
mapEffect : (msgA -> msgB) -> Updated model msgA -> Updated model msgB
mapEffect toMsgB updatedA =
    { model = updatedA.model
    , effect = Effect.map toMsgB updatedA.effect
    , seed = updatedA.seed
    , needsPersisting = updatedA.needsPersisting
    }
