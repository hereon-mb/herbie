module Shacl.Form.Localize exposing
    ( verbatim, verbatims
    , forLabel, forLabelPlural
    , withinSentence, withinSentencePlural, withinSentenceWithArticleIndefinite
    , C
    )

{-|

@docs verbatim, verbatims
@docs forLabel, forLabelPlural
@docs withinSentence, withinSentencePlural, withinSentenceWithArticleIndefinite

-}

import Inflect
import Locale exposing (Locale)
import Rdf exposing (StringOrLangString)
import String.Extra as String


type alias C rest =
    { rest | locale : Locale }


verbatim : C r -> StringOrLangString -> String
verbatim c stringOrLangString =
    case Rdf.nonLocalized stringOrLangString of
        Nothing ->
            localize c stringOrLangString

        Just nonLocalized ->
            nonLocalized


verbatims : C r -> List StringOrLangString -> String
verbatims c stringOrLangStrings =
    stringOrLangStrings
        |> List.map (verbatim c)
        |> String.join " "


forLabel : C r -> StringOrLangString -> String
forLabel c stringOrLangString =
    case Rdf.nonLocalized stringOrLangString of
        Nothing ->
            stringOrLangString
                |> localize c
                |> String.toSentenceCase

        Just nonLocalized ->
            nonLocalized


forLabelPlural : C r -> StringOrLangString -> String
forLabelPlural c stringOrLangString =
    case Rdf.nonLocalized stringOrLangString of
        Nothing ->
            stringOrLangString
                |> localizePlural c
                |> String.toSentenceCase

        Just nonLocalized ->
            nonLocalized


withinSentence : C r -> StringOrLangString -> String
withinSentence c stringOrLangString =
    case Rdf.nonLocalized stringOrLangString of
        Nothing ->
            stringOrLangString
                |> localize c

        Just nonLocalized ->
            nonLocalized


withinSentencePlural : C r -> StringOrLangString -> String
withinSentencePlural c stringOrLangString =
    case Rdf.nonLocalized stringOrLangString of
        Nothing ->
            stringOrLangString
                |> localizePlural c

        Just nonLocalized ->
            nonLocalized


withinSentenceWithArticleIndefinite : C r -> StringOrLangString -> String
withinSentenceWithArticleIndefinite c stringOrLangString =
    if
        "aeiou"
            |> String.toList
            |> List.map String.fromChar
            |> List.any (\vowel -> String.startsWith vowel (verbatim c stringOrLangString))
    then
        "an " ++ withinSentence c stringOrLangString

    else
        "a " ++ withinSentence c stringOrLangString



-- SUPPORT


localize : C r -> StringOrLangString -> String
localize c stringOrLangString =
    case c.locale of
        Locale.EnUS ->
            Rdf.localize "en" stringOrLangString
                |> Maybe.withDefault "Text missing"

        Locale.De ->
            Rdf.localize "de" stringOrLangString
                |> Maybe.withDefault "Text fehlt"


localizePlural : C r -> StringOrLangString -> String
localizePlural c stringOrLangString =
    case c.locale of
        Locale.EnUS ->
            Rdf.localize "en" stringOrLangString
                |> Maybe.map Inflect.toPlural
                |> Maybe.withDefault "Text missing"

        Locale.De ->
            Rdf.localize "de" stringOrLangString
                -- FIXME add german pluralization
                |> Maybe.withDefault "Text fehlt"
