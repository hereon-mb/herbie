module Shacl.Form.Instance exposing (Instance)

import Rdf exposing (StringOrLangString)
import Shacl.Form.Breadcrumbs exposing (Breadcrumbs)


type alias Instance form =
    { breadcrumbs : Breadcrumbs
    , expanded : Bool
    , form : form
    , label : Maybe StringOrLangString
    }
