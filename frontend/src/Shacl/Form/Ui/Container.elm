module Shacl.Form.Ui.Container exposing
    ( Container
    , expand, wrappable
    , wrap
    , ConfigWrap, configField, configCard
    , getElement
    )

{-|

@docs Container
@docs expand, wrappable
@docs wrap
@docs ConfigWrap, configField, configCard

-}

import Element
    exposing
        ( Element
        , alignTop
        , column
        , el
        , fill
        , minimum
        , spacing
        , spacingXY
        , width
        , wrappedRow
        )
import Ui.Theme.Dimensions as Dimensions


type Container msg
    = Container (Data msg)


type alias Data msg =
    { wrappable : Bool
    , element : Element msg
    }


{-| FIXME remove this
-}
getElement : Container msg -> Element msg
getElement (Container data) =
    data.element


expand : Element msg -> Container msg
expand element =
    Container
        { wrappable = False
        , element = element
        }


wrappable : Element msg -> Container msg
wrappable element =
    Container
        { wrappable = True
        , element = element
        }


type alias ConfigWrap =
    { widthFieldMinimum : Int
    , spacingFieldX : Int
    , spacingFieldY : Int
    }


configField : ConfigWrap
configField =
    let
        spacingFieldX =
            16
    in
    { widthFieldMinimum = (widthFormMaximum - spacingFieldX - 2 * paddingCardX) // 2
    , spacingFieldX = spacingFieldX
    , spacingFieldY = 8
    }


configCard : ConfigWrap
configCard =
    let
        spacingFieldX =
            16
    in
    { widthFieldMinimum = (widthFormMaximum - 3 * spacingFieldX - 2 * paddingCardX) // 4
    , spacingFieldX = spacingFieldX
    , spacingFieldY = 8
    }


widthFormMaximum : Int
widthFormMaximum =
    Dimensions.paneWidthMax


paddingCardX : Int
paddingCardX =
    16


wrap : ConfigWrap -> List (Container msg) -> Element msg
wrap config =
    let
        collectWrappedRows (Container data) acc =
            if data.wrappable then
                { acc | fields = data.element :: acc.fields }

            else
                { fields = []
                , rows =
                    if List.isEmpty acc.fields then
                        data.element :: acc.rows

                    else
                        [ data.element
                        , doTheWrap (List.reverse acc.fields)
                        ]
                            ++ acc.rows
                }

        wrapLastRow acc =
            if List.isEmpty acc.fields then
                acc.rows

            else
                doTheWrap (List.reverse acc.fields) :: acc.rows

        doTheWrap =
            List.map
                (el
                    [ width (minimum config.widthFieldMinimum fill)
                    , alignTop
                    ]
                )
                >> wrappedRow
                    [ width fill
                    , spacingXY config.spacingFieldX config.spacingFieldY
                    ]
    in
    List.foldl collectWrappedRows
        { fields = []
        , rows = []
        }
        >> wrapLastRow
        >> List.reverse
        >> column
            [ width fill
            , spacing config.spacingFieldY
            ]
