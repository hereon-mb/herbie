module Shacl.Form.Grouped exposing
    ( Field(..)
    , fromTranslated, Error(..), errorToString
    , resourcesRequired, resourcesRequiredForm
    , DataCollection
    , DataCollectionSelect
    , DataForm, Success, Alternative
    , DataGrid, DataEditorGrid
    , DataGroup
    , DataOptionalViaCheckbox
    , DataSchedule
    , DataSingleNested
    , Variant
    , DataVariants
    )

{-|

@docs Field
@docs fromTranslated, Error, errorToString
@docs resourcesRequired, resourcesRequiredForm

@docs DataCollection
@docs DataCollectionSelect
@docs DataForm, Success, Alternative
@docs DataGrid, DataEditorGrid
@docs DataGroup
@docs DataOptionalViaCheckbox
@docs DataSchedule
@docs DataSingleNested
@docs DataVariant, Variant
@docs DataVariants

-}

import Dict exposing (Dict)
import Graph exposing (Graph)
import IntDict
import List.NonEmpty as NonEmpty exposing (NonEmpty)
import List.NonEmpty.Extra as NonEmpty
import Maybe.Extra as Maybe
import Rdf exposing (BlankNodeOrIri, Iri, StringOrLangString)
import Rdf.PropertyPath exposing (PropertyPath)
import Result.Extra as Result
import Result.Pipeline as Result
import Shacl
import Shacl.Form.Collected as Collected
import Shacl.Form.Translated as Translated


type Field
    = FieldCheckbox Translated.DataCheckbox
    | FieldCheckboxInstance Translated.DataCheckboxInstance
    | FieldCollection DataCollection
    | FieldCollectionSelect DataCollectionSelect
    | FieldConstant Translated.DataConstant
    | FieldConstantInjectable Translated.DataConstantInjectable
    | FieldDateTime Translated.DataDateTime
    | FieldFileUpload Translated.DataFileUpload
    | FieldForm DataForm
    | FieldGrid DataGrid
    | FieldGroup DataGroup
    | FieldIriReadOnly Translated.DataIriReadOnly
    | FieldNumber Translated.DataNumber
    | FieldSchedule DataSchedule
    | FieldSelectMany Translated.DataSelectMany
    | FieldSelectOne Translated.DataSelectOne
    | FieldSelectOneInjectable Translated.DataSelectOneInjectable
    | FieldSingleNested DataSingleNested
    | FieldText Translated.DataText
    | FieldVariants DataVariants


type alias DataForm =
    { counts : Translated.Counts
    , successes : List Success
    , failures : List Translated.Failure
    , inject : Bool
    }


type alias Success =
    { field : Field
    , alternatives : List Alternative
    , errors : List ( Translated.TagField, Translated.Error )
    }


type alias Alternative =
    { score : Int
    , field : Field
    }


type alias DataGroup =
    { names : List StringOrLangString
    , field : Field
    }


type alias DataOptionalViaCheckbox =
    { group : Maybe Iri
    , order : Float
    , names : List StringOrLangString
    , field : Field
    }


type alias DataVariants =
    { hash : Int
    , group : Maybe Iri
    , order : Float
    , variants : NonEmpty Variant
    }


type alias Variant =
    { hash : Int
    , names : List StringOrLangString
    , descriptions : List StringOrLangString
    , targetClasses : List Iri
    , classes : List Iri
    , field : Field
    }


type alias DataCollection =
    { hash : Int
    , path : PropertyPath
    , group : Maybe Iri
    , order : Float
    , names : List StringOrLangString
    , descriptions : List StringOrLangString
    , labelInCard : Shacl.ConfigLabel
    , minCount : Int
    , maxCount : Int
    , field : Field
    , classes : NonEmpty Iri
    , orderPath : Maybe PropertyPath
    , viewer : Shacl.Viewer
    }


type alias DataCollectionSelect =
    { hash : Int
    , path : PropertyPath
    , group : Maybe Iri
    , order : Float
    , names : List StringOrLangString
    , descriptions : List StringOrLangString
    , minCount : Int
    , maxCount : Int
    , field : Field
    , classes : NonEmpty Iri
    , selectOne : Translated.DataSelectOne
    }


type alias DataSingleNested =
    { hash : Int
    , counts : Translated.Counts
    , path : PropertyPath
    , group : Maybe Iri
    , order : Float
    , field : Field
    , classes : List Iri
    , names : List StringOrLangString
    , qualified : Bool
    , optional : Translated.OptionalNested
    , node : BlankNodeOrIri
    , mintIri : Bool
    }


type alias DataSchedule =
    { hash : Int
    , path : PropertyPath
    , group : Maybe Iri
    , order : Float
    , names : List StringOrLangString
    , descriptions : List StringOrLangString
    , labelInCard : Shacl.ConfigLabel
    , minCount : Int
    , maxCount : Int
    , field : Field
    , classes : NonEmpty Iri
    , viewer : Shacl.Viewer
    , editorDayOffsetPath : PropertyPath
    , editorOrderPath : PropertyPath
    , editorTitlePath : PropertyPath
    , editorDatePath : PropertyPath

    -- FIXME
    -- instancesSample :
    --   List
    --       { label : StringOrLangString
    --       , fields : List field
    --       }
    }


type alias DataGrid =
    { hash : Int
    , path : PropertyPath
    , group : Maybe Iri
    , order : Float
    , names : List StringOrLangString
    , descriptions : List StringOrLangString
    , labelInCard : Shacl.ConfigLabel
    , minCount : Int
    , maxCount : Int
    , field : Field
    , classes : NonEmpty Iri
    , className : StringOrLangString
    , dataEditorGrid : DataEditorGrid
    }


type alias DataEditorGrid =
    { availableColumnsPath : PropertyPath
    , availableRowsPath : PropertyPath
    , -- FIXME this should be a `PropertyPath`, but encoding it is non-canonical
      columnPath : Iri
    , -- FIXME this should be a `PropertyPath`, but encoding it is non-canonical
      rowPath : Iri
    , cellViewer : Maybe Field
    , cellEditor : Maybe Field
    }


resourcesRequiredForm : DataForm -> List Iri
resourcesRequiredForm form =
    List.concatMap (.field >> resourcesRequired) form.successes


resourcesRequired : Field -> List Iri
resourcesRequired field =
    case field of
        FieldForm form ->
            resourcesRequiredForm form

        FieldGroup data ->
            resourcesRequired data.field

        FieldText _ ->
            []

        FieldCheckbox _ ->
            []

        FieldCheckboxInstance _ ->
            []

        FieldConstant _ ->
            []

        FieldConstantInjectable _ ->
            []

        FieldDateTime _ ->
            []

        FieldNumber _ ->
            []

        FieldSelectOne data ->
            [ data.class ]

        FieldSelectOneInjectable _ ->
            []

        FieldSelectMany data ->
            [ data.class ]

        FieldFileUpload _ ->
            []

        FieldIriReadOnly data ->
            NonEmpty.toList data.classes

        FieldVariants data ->
            data.variants
                |> NonEmpty.toList
                |> List.concatMap (.field >> resourcesRequired)

        FieldCollection data ->
            resourcesRequired data.field

        FieldCollectionSelect data ->
            resourcesRequired data.field

        FieldSingleNested data ->
            resourcesRequired data.field

        FieldSchedule data ->
            resourcesRequired data.field

        FieldGrid data ->
            resourcesRequired data.field


type Error
    = MissingPropertyGroup
        { propertyGroup : Iri
        }
    | NotImplemented


errorToString : Error -> String
errorToString error =
    case error of
        MissingPropertyGroup { propertyGroup } ->
            "I could not find the property group " ++ Rdf.serializeNode propertyGroup ++ "."

        NotImplemented ->
            "I ran into an unimplemented part during the grouping stage."


fromTranslated : Collected.Spec -> Translated.DataForm -> Result Error DataForm
fromTranslated spec { counts, successes, failures, inject } =
    Ok DataForm
        |> Result.hardcoded counts
        |> Result.required (groupSuccesses spec successes)
        |> Result.hardcoded failures
        |> Result.hardcoded inject


fieldFromTranslated : Collected.Spec -> Translated.Field -> Result Error Field
fieldFromTranslated spec field =
    case field of
        Translated.FieldForm { counts, successes, failures, inject } ->
            Ok DataForm
                |> Result.hardcoded counts
                |> Result.required (groupSuccesses spec successes)
                |> Result.hardcoded failures
                |> Result.hardcoded inject
                |> Result.map FieldForm

        Translated.FieldText data ->
            Ok (FieldText data)

        Translated.FieldCheckbox data ->
            Ok (FieldCheckbox data)

        Translated.FieldCheckboxInstance data ->
            Ok (FieldCheckboxInstance data)

        Translated.FieldConstant data ->
            Ok (FieldConstant data)

        Translated.FieldConstantInjectable data ->
            Ok (FieldConstantInjectable data)

        Translated.FieldDateTime data ->
            Ok (FieldDateTime data)

        Translated.FieldNumber data ->
            Ok (FieldNumber data)

        Translated.FieldSelectOne data ->
            Ok (FieldSelectOne data)

        Translated.FieldSelectOneInjectable data ->
            Ok (FieldSelectOneInjectable data)

        Translated.FieldSelectMany data ->
            Ok (FieldSelectMany data)

        Translated.FieldFileUpload data ->
            Ok (FieldFileUpload data)

        Translated.FieldIriReadOnly data ->
            Ok (FieldIriReadOnly data)

        Translated.FieldVariants data ->
            let
                variants =
                    data.variants
                        |> NonEmpty.map
                            (\variant ->
                                Ok Variant
                                    |> Result.hardcoded variant.hash
                                    |> Result.hardcoded variant.names
                                    |> Result.hardcoded variant.descriptions
                                    |> Result.hardcoded variant.targetClasses
                                    |> Result.hardcoded variant.classes
                                    |> Result.required (fieldFromTranslated spec variant.field)
                            )
                        |> NonEmpty.resultCombine
            in
            Ok DataVariants
                |> Result.hardcoded data.hash
                |> Result.hardcoded data.group
                |> Result.hardcoded data.order
                |> Result.required variants
                |> Result.map FieldVariants

        Translated.FieldCollection data ->
            Ok DataCollection
                |> Result.hardcoded data.hash
                |> Result.hardcoded data.path
                |> Result.hardcoded data.group
                |> Result.hardcoded data.order
                |> Result.hardcoded data.names
                |> Result.hardcoded data.descriptions
                |> Result.hardcoded data.labelInCard
                |> Result.hardcoded data.minCount
                |> Result.hardcoded data.maxCount
                |> Result.required (fieldFromTranslated spec data.field)
                |> Result.hardcoded data.classes
                |> Result.hardcoded data.orderPath
                |> Result.hardcoded data.viewer
                |> Result.map FieldCollection

        Translated.FieldCollectionSelect data ->
            Ok DataCollectionSelect
                |> Result.hardcoded data.hash
                |> Result.hardcoded data.path
                |> Result.hardcoded data.group
                |> Result.hardcoded data.order
                |> Result.hardcoded data.names
                |> Result.hardcoded data.descriptions
                |> Result.hardcoded data.minCount
                |> Result.hardcoded data.maxCount
                |> Result.required (fieldFromTranslated spec data.field)
                |> Result.hardcoded data.classes
                |> Result.hardcoded data.selectOne
                |> Result.map FieldCollectionSelect

        Translated.FieldSingleNested data ->
            Ok DataSingleNested
                |> Result.hardcoded data.hash
                |> Result.hardcoded data.counts
                |> Result.hardcoded data.path
                |> Result.hardcoded data.group
                |> Result.hardcoded data.order
                |> Result.required (fieldFromTranslated spec data.field)
                |> Result.hardcoded data.classes
                |> Result.hardcoded data.names
                |> Result.hardcoded data.qualified
                |> Result.hardcoded data.optional
                |> Result.hardcoded data.node
                |> Result.hardcoded data.mintIri
                |> Result.map FieldSingleNested

        Translated.FieldSchedule data ->
            Ok DataSchedule
                |> Result.hardcoded data.hash
                |> Result.hardcoded data.path
                |> Result.hardcoded data.group
                |> Result.hardcoded data.order
                |> Result.hardcoded data.names
                |> Result.hardcoded data.descriptions
                |> Result.hardcoded data.labelInCard
                |> Result.hardcoded data.minCount
                |> Result.hardcoded data.maxCount
                |> Result.required (fieldFromTranslated spec data.field)
                |> Result.hardcoded data.classes
                |> Result.hardcoded data.viewer
                |> Result.hardcoded data.editorDayOffsetPath
                |> Result.hardcoded data.editorOrderPath
                |> Result.hardcoded data.editorTitlePath
                |> Result.hardcoded data.editorDatePath
                |> Result.map FieldSchedule

        Translated.FieldGrid data ->
            let
                dataEditorGrid =
                    Ok DataEditorGrid
                        |> Result.hardcoded data.dataEditorGrid.availableColumnsPath
                        |> Result.hardcoded data.dataEditorGrid.availableRowsPath
                        |> Result.hardcoded data.dataEditorGrid.columnPath
                        |> Result.hardcoded data.dataEditorGrid.rowPath
                        |> Result.required
                            (case data.dataEditorGrid.cellViewer of
                                Nothing ->
                                    Ok Nothing

                                Just cellViewer ->
                                    cellViewer
                                        |> fieldFromTranslated spec
                                        |> Result.map Just
                            )
                        |> Result.required
                            (case data.dataEditorGrid.cellEditor of
                                Nothing ->
                                    Ok Nothing

                                Just cellEditor ->
                                    cellEditor
                                        |> fieldFromTranslated spec
                                        |> Result.map Just
                            )
            in
            Ok DataGrid
                |> Result.hardcoded data.hash
                |> Result.hardcoded data.path
                |> Result.hardcoded data.group
                |> Result.hardcoded data.order
                |> Result.hardcoded data.names
                |> Result.hardcoded data.descriptions
                |> Result.hardcoded data.labelInCard
                |> Result.hardcoded data.minCount
                |> Result.hardcoded data.maxCount
                |> Result.required (fieldFromTranslated spec data.field)
                |> Result.hardcoded data.classes
                |> Result.hardcoded data.className
                |> Result.required dataEditorGrid
                |> Result.map FieldGrid


type Group
    = Leaf
        { success : Translated.Success
        , order : Float
        }
    | Group
        { propertyGroup : Shacl.PropertyGroup
        , iri : Iri
        , order : Float
        }


groupSuccesses : Collected.Spec -> List Translated.Success -> Result Error (List Success)
groupSuccesses ({ shapes } as spec) successes =
    successes
        |> sortIntoGroups shapes
        |> Result.andThen
            (\{ seeds, graph } ->
                let
                    fieldFromIdNode idNode =
                        case Graph.get idNode graph of
                            Nothing ->
                                Err NotImplemented

                            Just { node, outgoing } ->
                                case node.label of
                                    Leaf { success, order } ->
                                        success
                                            |> successFromTranslated spec
                                            |> Result.map
                                                (\successGrouped ->
                                                    { success = successGrouped
                                                    , group = Nothing
                                                    , order = order
                                                    }
                                                )

                                    Group { propertyGroup, order } ->
                                        outgoing
                                            |> IntDict.keys
                                            |> List.map fieldFromIdNode
                                            |> Result.combine
                                            |> Result.map (List.sortBy .order >> List.map .success)
                                            |> Result.map
                                                (\successesGrouped ->
                                                    { success =
                                                        { field =
                                                            FieldGroup
                                                                { names = Maybe.toList propertyGroup.name
                                                                , field =
                                                                    FieldForm
                                                                        { counts = Translated.zeros
                                                                        , successes = successesGrouped
                                                                        , failures = []
                                                                        , inject = False
                                                                        }
                                                                }
                                                        , alternatives = []
                                                        , errors = []
                                                        }
                                                    , group = Nothing
                                                    , order = order
                                                    }
                                                )
                in
                seeds
                    |> List.map fieldFromIdNode
                    |> Result.combine
                    |> Result.map (List.sortBy .order >> List.map .success)
            )


successFromTranslated : Collected.Spec -> Translated.Success -> Result Error Success
successFromTranslated spec success =
    Ok Success
        |> Result.required (fieldFromTranslated spec success.field)
        |> Result.required
            (success.alternatives
                |> List.map (alternativeFromTranslated spec)
                |> Result.combine
            )
        |> Result.hardcoded success.errors


alternativeFromTranslated : Collected.Spec -> Translated.Alternative -> Result Error Alternative
alternativeFromTranslated spec alternative =
    Ok Alternative
        |> Result.hardcoded alternative.score
        |> Result.required (fieldFromTranslated spec alternative.field)


type alias Final =
    { seeds : List Int
    , graph : Graph Group ()
    }


type alias Step =
    { idNode : Int
    , nodes : List (Graph.Node Group)
    , edges : List (Graph.Edge ())
    , groups : Dict String Int
    , seeds : List Int
    }


stepInitial : Step
stepInitial =
    { idNode = 0
    , nodes = []
    , edges = []
    , groups = Dict.empty
    , seeds = []
    }


stepToFinal : Step -> Final
stepToFinal step =
    { seeds = step.seeds
    , graph = Graph.fromNodesAndEdges step.nodes step.edges
    }


sortIntoGroups : Shacl.Shapes -> List Translated.Success -> Result Error Final
sortIntoGroups shapes fields =
    fields
        |> List.foldl (\field step -> Result.andThen (nextStep shapes field) step) (Ok stepInitial)
        |> Result.map stepToFinal


nextStep : Shacl.Shapes -> Translated.Success -> Step -> Result Error Step
nextStep shapes success step =
    { step
        | idNode = step.idNode + 1
        , nodes =
            { id = step.idNode
            , label =
                Leaf
                    { success = success
                    , order = Translated.orderFromField success.field
                    }
            }
                :: step.nodes
    }
        |> addGroup shapes step.idNode (Translated.groupFromField success.field)


addGroup : Shacl.Shapes -> Int -> Maybe Iri -> Step -> Result Error Step
addGroup shapes idNodeField group step =
    case group of
        Nothing ->
            Ok { step | seeds = idNodeField :: step.seeds }

        Just iriGroup ->
            case Dict.get (Rdf.serializeNode iriGroup) step.groups of
                Nothing ->
                    case Shacl.getPropertyGroup shapes iriGroup of
                        Nothing ->
                            Err (MissingPropertyGroup { propertyGroup = iriGroup })

                        Just propertyGroup ->
                            { step
                                | idNode = step.idNode + 1
                                , nodes =
                                    { id = step.idNode
                                    , label =
                                        Group
                                            { propertyGroup = propertyGroup
                                            , iri = iriGroup
                                            , order = Maybe.withDefault (toFloat infinity) propertyGroup.order
                                            }
                                    }
                                        :: step.nodes
                                , edges =
                                    { from = step.idNode
                                    , to = idNodeField
                                    , label = ()
                                    }
                                        :: step.edges
                                , groups = Dict.insert (Rdf.serializeNode iriGroup) step.idNode step.groups
                            }
                                |> addGroup shapes step.idNode propertyGroup.group

                Just idNodeGroup ->
                    Ok
                        { step
                            | edges =
                                { from = idNodeGroup
                                , to = idNodeField
                                , label = ()
                                }
                                    :: step.edges
                        }


infinity : Int
infinity =
    ceiling (1 / 0)
