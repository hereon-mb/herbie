module Shacl.Form.Breadcrumbs exposing
    ( Breadcrumbs, Path, Segment(..)
    , init
    , pointer, id
    , lastOrdinal
    )

{-|

@docs Breadcrumbs, Path, Segment
@docs init
@docs pointer, path, id
@docs lastOrdinal

-}

import Hex
import Pointer exposing (Pointer)


type alias Breadcrumbs =
    { depthGroup : Int
    , depthCollection : Int
    }


type alias Path =
    List Segment


type Segment
    = Ordinal Int
    | Hash Int


init : Breadcrumbs
init =
    { depthGroup = 0
    , depthCollection = 0
    }


id : Path -> String
id =
    List.reverse
        >> List.map segmentToString
        >> String.join "-"


lastOrdinal : Path -> Maybe Int
lastOrdinal path =
    case path of
        (Ordinal ordinal) :: _ ->
            Just ordinal

        _ ->
            Nothing


segmentToString : Segment -> String
segmentToString segment =
    case segment of
        Ordinal ord ->
            String.fromInt ord

        Hash hash ->
            Hex.toString hash


pointer : Path -> Pointer
pointer path =
    path
        |> id
        |> Pointer.verbatim
