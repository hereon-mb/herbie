module Shacl.Form.Loader exposing
    ( Loader, loadDraft, loadVersion, loadVersionIri
    , update, Msg, Step(..), Error(..), errorToString
    , Latest(..), DataLatestDraft, DataLatestVersion
    , toLoading
    , effectToEffect
    )

{-|

@docs Loader, loadDraft, loadVersion, loadVersionIri
@docs update, Msg, Step, Error, errorToString
@docs Latest, DataLatestDraft, DataLatestVersion
@docs toLoading
@docs effectToEffect

-}

import Action
import Api
import Context exposing (C)
import Dict
import Effect exposing (Effect)
import File exposing (File)
import Http
import List.Extra as List
import Locale
import Maybe.Extra as Maybe
import Maybe.Pipeline as Maybe
import Ontology.Herbie as Herbie
import Ontology.Instance exposing (Instance)
import Ontology.Shacl exposing (Class)
import Random
import Rdf exposing (BlankNodeOrIriOrAnyLiteral, Iri)
import Rdf.Decode as Decode
import Rdf.DictIri as DictIri exposing (DictIri)
import Rdf.Extra as Rdf
import Rdf.Graph as Rdf exposing (Graph, Seed)
import Rdf.Namespaces exposing (a)
import Rdf.Namespaces.RDFS as RDFS
import SPARQL
import SPARQL.Node as Node
import Shacl.Form.Effect exposing (Option)
import Shacl.Form.Localize as Localize
import Shacl.Form.Viewed as Form exposing (DataInit, Form, OutMsg, initStatic)
import Shared.Loading as Loading exposing (Loading)
import String.Extra as String
import Triple
import Url.Builder as Url


type Loader
    = -- DRAFT
      LoadDraft Rdf.Iri
    | LoadingDraft DataLoadingDraft
      -- VERSION
    | LoadVersion Rdf.Iri Int
    | LoadingVersion DataLoadingVersion


type alias DataLoadingDraft =
    { iri : Rdf.Iri
    , graph : Rdf.Graph
    , seed : Maybe Seed
    , document : Herbie.Document
    , draft : Herbie.DocumentDraft
    , conformsTo : Maybe Rdf.Iri
    , loading : Loading
    , loadingClosure : Loading
    , graphs : DictIri Graph
    , graphClosures : DictIri Graph
    }


type alias DataLoadingVersion =
    { iri : Rdf.Iri
    , graph : Rdf.Graph
    , seed : Maybe Seed
    , document : Herbie.Document
    , version : Herbie.DocumentVersion
    , conformsTo : Maybe Rdf.Iri
    , loading : Loading
    , loadingClosure : Loading
    , graphs : DictIri Graph
    , graphClosures : DictIri Graph
    }


toLoading : Loader -> Loading
toLoading loader =
    case loader of
        -- DRAFT
        LoadDraft iri ->
            Loading.empty
                |> Loading.addMany [ iri ]
                |> Loading.asked

        LoadingDraft data ->
            data.loading

        -- VERSION
        LoadVersion iri _ ->
            Loading.empty
                |> Loading.addMany [ iri ]
                |> Loading.asked

        LoadingVersion data ->
            data.loading


type Msg
    = CacheReturnedDocument (Api.Response Rdf.Graph)
    | CacheReturnedGraph Rdf.Iri (Api.Response Rdf.Graph)
    | CacheReturnedGraphClosure Rdf.Iri (Api.Response Rdf.Graph)
    | BrowserGeneratedSeed Seed


type Error
    = CouldNotLoadDocument Api.Error
    | NotAHerbieDocument Decode.Error
    | NoDraft
    | NoVersion
    | CouldNotLoadGraph Rdf.Iri Api.Error
    | GraphsMissing
    | CouldNotInitializeForm Form.Error


errorToString : C -> Error -> String
errorToString c error =
    case error of
        CouldNotLoadDocument errorApi ->
            "Could not load document: " ++ apiErrorToString c errorApi ++ "."

        NotAHerbieDocument errorDecode ->
            "This is not a valid herbie document: " ++ Decode.errorToString errorDecode

        NoDraft ->
            "There is not draft for this document."

        NoVersion ->
            "There is no such version for this document."

        CouldNotLoadGraph iri errorApi ->
            "Could not load the graph " ++ Rdf.serializeNode iri ++ ": " ++ apiErrorToString c errorApi ++ "."

        GraphsMissing ->
            "Some graphs could not be loaded."

        CouldNotInitializeForm errorForm ->
            "Could not initialize form: " ++ Form.errorToString errorForm ++ "."


apiErrorToString : C -> Api.Error -> String
apiErrorToString c error =
    error
        |> Api.errorToMessages
        |> Localize.verbatims c


loadDraft : Rdf.Iri -> ( Loader, Effect Msg )
loadDraft iri =
    ( LoadDraft iri
    , getDocument iri
    )


loadVersion : Rdf.Iri -> Int -> ( Loader, Effect Msg )
loadVersion iri index =
    ( LoadVersion iri index
    , getDocument iri
    )


loadVersionIri : Rdf.Iri -> ( Loader, Effect Msg )
loadVersionIri iri =
    let
        ( index, iriDocument ) =
            iri
                |> Rdf.toUrl
                |> String.split "/"
                |> List.reverse
                |> List.splitAt 3
                |> Tuple.mapFirst (List.getAt 1 >> Maybe.andThen String.toInt >> Maybe.withDefault -1)
                |> Tuple.mapSecond (List.reverse >> String.join "/" >> prepend "/" >> Rdf.iri)

        prepend a b =
            b ++ a
    in
    ( LoadVersion iriDocument index
    , getDocument iriDocument
    )


getDocument : Rdf.Iri -> Effect Msg
getDocument iri =
    Effect.fromAction
        (Action.GetGraph
            { store = True
            , reload = False
            , closure = False
            }
            CacheReturnedDocument
            iri
        )


type Step
    = Running Loader (Effect Msg)
    | SuccessDraft
        { document : Herbie.Document
        , draft : Herbie.DocumentDraft
        , graphDraft : Graph
        , graphAccess : Graph
        , graphEntered : Graph
        , graphGenerated : Graph
        }
    | SuccessDraftWithForm
        { document : Herbie.Document
        , draft : Herbie.DocumentDraft
        , conformsTo : Rdf.Iri
        , form : Form
        , effect : Effect Form.Msg
        , outMsg : Maybe OutMsg
        , graphDraft : Graph
        , graphAccess : Graph
        , graphEntered : Graph
        , graphGenerated : Graph
        }
    | SuccessVersion
        { document : Herbie.Document
        , version : Herbie.DocumentVersion
        , latest : Latest
        , graphVersion : Graph
        , graphAccess : Graph
        , graphEntered : Graph
        , graphGenerated : Graph
        }
    | SuccessVersionWithForm
        { document : Herbie.Document
        , version : Herbie.DocumentVersion
        , conformsTo : Rdf.Iri
        , form : Form
        , latest : Latest
        , graphVersion : Graph
        , graphAccess : Graph
        , graphEntered : Graph
        , graphGenerated : Graph
        }
    | Failed Error


type Latest
    = LatestVersion DataLatestVersion
    | LatestDraft DataLatestDraft


type alias DataLatestVersion =
    { version : Herbie.DocumentVersion
    , graph : Rdf.Graph
    , conformsTo : Maybe Rdf.Iri
    , form : Maybe Form
    }


type alias DataLatestDraft =
    { draft : Herbie.DocumentDraft
    , graph : Rdf.Graph
    , conformsTo : Maybe Rdf.Iri
    , form : Maybe Form
    }


update : C -> Msg -> Loader -> Step
update c msg loader =
    case loader of
        -- DRAFT
        LoadDraft iri ->
            updateLoadDraft msg iri

        LoadingDraft data ->
            updateLoadingDraft c msg data

        -- VERSION
        LoadVersion iri index ->
            updateLoadVersion msg iri index

        LoadingVersion data ->
            updateLoadingVersion c msg data



-- DRAFT


updateLoadDraft : Msg -> Rdf.Iri -> Step
updateLoadDraft msg iri =
    case msg of
        CacheReturnedDocument (Err error) ->
            Failed (CouldNotLoadDocument error)

        CacheReturnedDocument (Ok graph) ->
            case Decode.decode (Decode.from iri Herbie.decoderDocument) graph of
                Err error ->
                    Failed (NotAHerbieDocument error)

                Ok document ->
                    case document.draft of
                        Nothing ->
                            Failed NoDraft

                        Just draft ->
                            loadGraphsDraft iri graph document draft

        CacheReturnedGraph _ _ ->
            Running (LoadDraft iri) Effect.none

        CacheReturnedGraphClosure _ _ ->
            Running (LoadDraft iri) Effect.none

        BrowserGeneratedSeed _ ->
            Running (LoadDraft iri) Effect.none


loadGraphsDraft :
    Rdf.Iri
    -> Rdf.Graph
    -> Herbie.Document
    -> Herbie.DocumentDraft
    -> Step
loadGraphsDraft iri graph document draft =
    let
        loading =
            Loading.empty
                |> Loading.addMany irisMeta
                |> Loading.addMany irisDraft
                |> Loading.addMany irisVersionLatest
                |> Loading.loaded document.this

        loadingClosure =
            Loading.empty
                |> Loading.addMany irisConformsTo

        irisMeta =
            [ document.this
            , iriAccess document.this
            ]

        irisDraft =
            [ draft.this
            , Herbie.iriEntered draft.this
            , Herbie.iriPersisted draft.this
            , Herbie.iriGenerated draft.this
            , Herbie.iriSupport draft.this
            ]

        irisConformsTo =
            case draft.terms.conformsTo of
                Nothing ->
                    []

                Just conformsTo ->
                    [ conformsTo.this ]

        irisVersionLatest =
            case List.head document.versions of
                Nothing ->
                    []

                Just versionLatest ->
                    [ versionLatest.this ]
    in
    Running
        (LoadingDraft
            { iri = iri
            , graph = graph
            , seed = Nothing
            , document = document
            , draft = draft
            , conformsTo = Maybe.map .this draft.terms.conformsTo
            , loading = Loading.asked loading
            , loadingClosure = Loading.asked loadingClosure
            , graphs = DictIri.empty
            , graphClosures = DictIri.empty
            }
        )
        (Effect.batch
            [ loading
                |> Loading.notAsked
                |> List.map getGraph
                |> Effect.batch
            , loadingClosure
                |> Loading.notAsked
                |> List.map getGraphClosure
                |> Effect.batch
            , Random.generate BrowserGeneratedSeed Rdf.seedGenerator
                |> Effect.fromCmd
            ]
        )


updateLoadingDraft : C -> Msg -> DataLoadingDraft -> Step
updateLoadingDraft c msg data =
    case msg of
        CacheReturnedDocument _ ->
            Running (LoadingDraft data) Effect.none

        CacheReturnedGraph iri (Err error) ->
            Failed (CouldNotLoadGraph iri error)

        CacheReturnedGraph iri (Ok graph) ->
            { data
                | loading = Loading.loaded iri data.loading
                , graphs = DictIri.insert iri graph data.graphs
            }
                |> finalizeDraft c

        CacheReturnedGraphClosure iri (Err error) ->
            Failed (CouldNotLoadGraph iri error)

        CacheReturnedGraphClosure iri (Ok graph) ->
            { data
                | loadingClosure = Loading.loaded iri data.loadingClosure
                , graphClosures = DictIri.insert iri graph data.graphClosures
            }
                |> finalizeDraft c

        BrowserGeneratedSeed seed ->
            { data | seed = Just seed }
                |> finalizeDraft c


finalizeDraft : C -> DataLoadingDraft -> Step
finalizeDraft c data =
    if
        Loading.allLoaded data.loading
            && Loading.allLoaded data.loadingClosure
            && Maybe.isJust data.seed
    then
        case data.conformsTo of
            Nothing ->
                Just
                    (\graphDraft graphAccess graphEntered graphGenerated ->
                        SuccessDraft
                            { document = data.document
                            , draft = data.draft
                            , graphDraft = graphDraft
                            , graphAccess = graphAccess
                            , graphEntered = graphEntered
                            , graphGenerated = graphGenerated
                            }
                    )
                    |> Maybe.required (DictIri.get data.draft.this data.graphs)
                    |> Maybe.required (DictIri.get (iriAccess data.document.this) data.graphs)
                    |> Maybe.required (DictIri.get (Herbie.iriEntered data.draft.this) data.graphs)
                    |> Maybe.required (DictIri.get (Herbie.iriGenerated data.draft.this) data.graphs)
                    |> Maybe.withDefault (Failed GraphsMissing)

            Just conformsTo ->
                let
                    populateDefaultValues =
                        Herbie.populateDefaultValues data.document
                in
                case
                    initForm c
                        populateDefaultValues
                        False
                        data.seed
                        data.document.herbie.workspaceUuid
                        data.draft.this
                        data.graphs
                        data.graphClosures
                        conformsTo
                of
                    Err error ->
                        Failed error

                    Ok ( form, effect, outMsg ) ->
                        Just
                            (\graphDraft graphAccess graphEntered graphGenerated ->
                                SuccessDraftWithForm
                                    { document = data.document
                                    , draft = data.draft
                                    , conformsTo = conformsTo
                                    , form = form
                                    , effect = effect
                                    , outMsg = outMsg
                                    , graphDraft = graphDraft
                                    , graphAccess = graphAccess
                                    , graphEntered = graphEntered
                                    , graphGenerated = graphGenerated
                                    }
                            )
                            |> Maybe.required (DictIri.get data.draft.this data.graphs)
                            |> Maybe.required (DictIri.get (iriAccess data.document.this) data.graphs)
                            |> Maybe.required (DictIri.get (Herbie.iriEntered data.draft.this) data.graphs)
                            |> Maybe.required (DictIri.get (Herbie.iriGenerated data.draft.this) data.graphs)
                            |> Maybe.withDefault (Failed GraphsMissing)

    else
        Running (LoadingDraft data) Effect.none



-- VERSION


updateLoadVersion : Msg -> Rdf.Iri -> Int -> Step
updateLoadVersion msg iri index =
    case msg of
        CacheReturnedDocument (Err error) ->
            Failed (CouldNotLoadDocument error)

        CacheReturnedDocument (Ok graph) ->
            case Decode.decode (Decode.from iri Herbie.decoderDocument) graph of
                Err error ->
                    Failed (NotAHerbieDocument error)

                Ok document ->
                    case Herbie.versionAt index document of
                        Nothing ->
                            Failed NoVersion

                        Just version ->
                            loadGraphsVersion iri graph document version

        CacheReturnedGraph _ _ ->
            Running (LoadVersion iri index) Effect.none

        CacheReturnedGraphClosure _ _ ->
            Running (LoadVersion iri index) Effect.none

        BrowserGeneratedSeed _ ->
            Running (LoadVersion iri index) Effect.none


loadGraphsVersion : Rdf.Iri -> Rdf.Graph -> Herbie.Document -> Herbie.DocumentVersion -> Step
loadGraphsVersion iri graph document version =
    let
        loading =
            Loading.empty
                |> Loading.addMany irisMeta
                |> Loading.addMany irisVersion
                |> Loading.addMany irisVersionLatest
                |> Loading.addMany irisLatest
                |> Loading.loaded document.this

        loadingClosure =
            Loading.empty
                |> Loading.addMany irisConformsTo
                |> Loading.addMany irisConformsToLatest

        irisMeta =
            [ document.this
            , iriAccess document.this
            ]

        irisVersion =
            [ version.this
            , Herbie.iriEntered version.this
            , Herbie.iriPersisted version.this
            , Herbie.iriGenerated version.this
            , Herbie.iriSupport version.this
            ]

        irisConformsTo =
            case version.terms.conformsTo of
                Nothing ->
                    []

                Just conformsTo ->
                    [ conformsTo.this ]

        irisVersionLatest =
            case List.head document.versions of
                Nothing ->
                    []

                Just versionLatest ->
                    [ versionLatest.this ]

        irisLatest =
            case maybeIriLatest of
                Nothing ->
                    []

                Just iriLatest ->
                    [ iriLatest
                    , Herbie.iriEntered iriLatest
                    , Herbie.iriPersisted iriLatest
                    , Herbie.iriGenerated iriLatest
                    , Herbie.iriSupport iriLatest
                    ]

        irisConformsToLatest =
            case document.draft of
                Nothing ->
                    case List.head document.versions of
                        Nothing ->
                            []

                        Just versionLatest ->
                            case versionLatest.terms.conformsTo of
                                Nothing ->
                                    []

                                Just conformsTo ->
                                    [ conformsTo.this ]

                Just draft ->
                    case draft.terms.conformsTo of
                        Nothing ->
                            []

                        Just conformsTo ->
                            [ conformsTo.this ]

        maybeIriLatest =
            case document.draft of
                Nothing ->
                    Maybe.map .this (List.head document.versions)

                Just draft ->
                    Just draft.this
    in
    Running
        (LoadingVersion
            { iri = iri
            , graph = graph
            , seed = Nothing
            , document = document
            , version = version
            , conformsTo = Maybe.map .this version.terms.conformsTo
            , loading = Loading.asked loading
            , loadingClosure = Loading.asked loadingClosure
            , graphs = DictIri.empty
            , graphClosures = DictIri.empty
            }
        )
        (Effect.batch
            [ loading
                |> Loading.notAsked
                |> List.map getGraph
                |> Effect.batch
            , loadingClosure
                |> Loading.notAsked
                |> List.map getGraphClosure
                |> Effect.batch
            , Random.generate BrowserGeneratedSeed Rdf.seedGenerator
                |> Effect.fromCmd
            ]
        )


updateLoadingVersion : C -> Msg -> DataLoadingVersion -> Step
updateLoadingVersion c msg data =
    case msg of
        CacheReturnedDocument _ ->
            Running (LoadingVersion data) Effect.none

        CacheReturnedGraph iri (Err error) ->
            Failed (CouldNotLoadGraph iri error)

        CacheReturnedGraph iri (Ok graph) ->
            { data
                | loading = Loading.loaded iri data.loading
                , graphs = DictIri.insert iri graph data.graphs
            }
                |> finalizeVersion c

        CacheReturnedGraphClosure iri (Err error) ->
            Failed (CouldNotLoadGraph iri error)

        CacheReturnedGraphClosure iri (Ok graph) ->
            { data
                | loadingClosure = Loading.loaded iri data.loadingClosure
                , graphClosures = DictIri.insert iri graph data.graphClosures
            }
                |> finalizeVersion c

        BrowserGeneratedSeed seed ->
            { data | seed = Just seed }
                |> finalizeVersion c


finalizeVersion : C -> DataLoadingVersion -> Step
finalizeVersion c data =
    if
        Loading.allLoaded data.loading
            && Loading.allLoaded data.loadingClosure
            && Maybe.isJust data.seed
    then
        case data.conformsTo of
            Nothing ->
                case initLatest c data of
                    Nothing ->
                        Running (LoadingVersion data) Effect.none

                    Just latest ->
                        Just
                            (\graphVersion graphAccess graphEntered graphGenerated ->
                                SuccessVersion
                                    { document = data.document
                                    , version = data.version
                                    , latest = latest
                                    , graphVersion = graphVersion
                                    , graphAccess = graphAccess
                                    , graphEntered = graphEntered
                                    , graphGenerated = graphGenerated
                                    }
                            )
                            |> Maybe.required (DictIri.get data.version.this data.graphs)
                            |> Maybe.required (DictIri.get (iriAccess data.document.this) data.graphs)
                            |> Maybe.required (DictIri.get (Herbie.iriEntered data.version.this) data.graphs)
                            |> Maybe.required (DictIri.get (Herbie.iriGenerated data.version.this) data.graphs)
                            |> Maybe.withDefault (Failed GraphsMissing)

            Just conformsTo ->
                let
                    populateDefaultValues =
                        { populateDefaultValues = False }
                in
                case
                    initForm c
                        populateDefaultValues
                        True
                        data.seed
                        data.document.herbie.workspaceUuid
                        data.version.this
                        data.graphs
                        data.graphClosures
                        conformsTo
                of
                    Err error ->
                        Failed error

                    Ok ( form, _, _ ) ->
                        case initLatest c data of
                            Nothing ->
                                Running (LoadingVersion data) Effect.none

                            Just latest ->
                                Just
                                    (\graphVersion graphAccess graphEntered graphGenerated ->
                                        SuccessVersionWithForm
                                            { document = data.document
                                            , version = data.version
                                            , conformsTo = conformsTo
                                            , form = form
                                            , latest = latest
                                            , graphVersion = graphVersion
                                            , graphAccess = graphAccess
                                            , graphEntered = graphEntered
                                            , graphGenerated = graphGenerated
                                            }
                                    )
                                    |> Maybe.required (DictIri.get data.version.this data.graphs)
                                    |> Maybe.required (DictIri.get (iriAccess data.document.this) data.graphs)
                                    |> Maybe.required (DictIri.get (Herbie.iriEntered data.version.this) data.graphs)
                                    |> Maybe.required (DictIri.get (Herbie.iriGenerated data.version.this) data.graphs)
                                    |> Maybe.withDefault (Failed GraphsMissing)

    else
        Running (LoadingVersion data) Effect.none


initLatest : C -> DataLoadingVersion -> Maybe Latest
initLatest c data =
    case List.head data.document.versions of
        Nothing ->
            case data.document.draft of
                Nothing ->
                    Nothing

                Just draft ->
                    case DictIri.get draft.this data.graphs of
                        Nothing ->
                            Nothing

                        Just graphDraft ->
                            let
                                populateDefaultValues =
                                    { populateDefaultValues = False }
                            in
                            { draft = draft
                            , graph = graphDraft
                            , conformsTo = Maybe.map .this draft.terms.conformsTo
                            , form =
                                draft.terms.conformsTo
                                    |> Maybe.map .this
                                    |> Maybe.andThen
                                        (initForm c
                                            populateDefaultValues
                                            False
                                            data.seed
                                            data.document.herbie.workspaceUuid
                                            draft.this
                                            data.graphs
                                            data.graphClosures
                                            >> Result.toMaybe
                                        )
                                    |> Maybe.map (\( f, _, _ ) -> f)
                            }
                                |> LatestDraft
                                |> Just

        Just versionLatest ->
            case DictIri.get versionLatest.this data.graphs of
                Nothing ->
                    Nothing

                Just graphVersionLatest ->
                    let
                        populateDefaultValues =
                            { populateDefaultValues = False }
                    in
                    { version = versionLatest
                    , graph = graphVersionLatest
                    , conformsTo = Maybe.map .this versionLatest.terms.conformsTo
                    , form =
                        versionLatest.terms.conformsTo
                            |> Maybe.map .this
                            |> Maybe.andThen
                                (initForm c
                                    populateDefaultValues
                                    True
                                    data.seed
                                    data.document.herbie.workspaceUuid
                                    versionLatest.this
                                    data.graphs
                                    data.graphClosures
                                    >> Result.toMaybe
                                )
                            |> Maybe.map (\( f, _, _ ) -> f)
                    }
                        |> LatestVersion
                        |> Just



-- SHARED


getGraph : Rdf.Iri -> Effect Msg
getGraph iri =
    Effect.fromAction
        (Action.GetGraph
            { store = True
            , reload = False
            , closure = False
            }
            (CacheReturnedGraph iri)
            iri
        )


getGraphClosure : Rdf.Iri -> Effect Msg
getGraphClosure iri =
    Effect.fromAction
        (Action.GetGraph
            { store = False
            , reload = False
            , closure = True
            }
            (CacheReturnedGraphClosure iri)
            iri
        )


iriAccess : Rdf.Iri -> Rdf.Iri
iriAccess iri =
    Rdf.iri (Rdf.toUrl iri ++ "access/")


initForm :
    C
    -> { populateDefaultValues : Bool }
    -> Bool
    -> Maybe Seed
    -> String
    -> Rdf.Iri
    -> DictIri Graph
    -> DictIri Graph
    -> Rdf.Iri
    -> Result Error ( Form, Effect Form.Msg, Maybe OutMsg )
initForm c populateDefaultValues strict seed workspaceUuid iri graphs graphClosures conformsTo =
    Just DataInit
        |> Maybe.hardcoded iri
        |> Maybe.hardcoded False
        |> Maybe.hardcoded strict
        |> Maybe.hardcoded workspaceUuid
        |> Maybe.required (DictIri.get iri graphs)
        |> Maybe.required (DictIri.get (Herbie.iriEntered iri) graphs)
        |> Maybe.required (DictIri.get (Herbie.iriPersisted iri) graphs)
        |> Maybe.required (DictIri.get (Herbie.iriGenerated iri) graphs)
        |> Maybe.required (DictIri.get (Herbie.iriSupport iri) graphs)
        |> Maybe.required (DictIri.get conformsTo graphClosures)
        |> Maybe.hardcoded populateDefaultValues
        |> Maybe.required seed
        |> Result.fromMaybe GraphsMissing
        |> Result.andThen (initStatic c.cShacl >> Result.mapError CouldNotInitializeForm)
        |> Result.map (Triple.mapSecond (effectToEffect c workspaceUuid))


effectToEffect : C -> String -> Shacl.Form.Effect.Effect msg -> Effect msg
effectToEffect c workspaceUuid effect =
    case effect of
        Shacl.Form.Effect.None ->
            Effect.none

        Shacl.Form.Effect.Batch effects ->
            Effect.batch (List.map (effectToEffect c workspaceUuid) effects)

        Shacl.Form.Effect.Cmd cmd ->
            Effect.fromCmd cmd

        Shacl.Form.Effect.Msg msg ->
            Effect.fromMsg msg

        -- FORM
        Shacl.Form.Effect.CreateField onField preview path mint field ->
            Effect.fromAction (Action.CreateField onField preview path mint field)

        Shacl.Form.Effect.DuplicateField onField preview path mint field ->
            Effect.fromAction (Action.DuplicateField onField preview path mint field)

        Shacl.Form.Effect.UserIntentsToCreateDatasetFor classes ->
            Effect.fromAction (Action.UserIntentsToCreateDatasetFor workspaceUuid classes)

        -- RESOURCES
        Shacl.Form.Effect.GetResource onOptions class ->
            getResource c workspaceUuid onOptions class

        Shacl.Form.Effect.GetSubclassesWithShaclShapes onClasses class ->
            getSubclassesWithShaclShapes c workspaceUuid onClasses class

        Shacl.Form.Effect.CreateFile onFile file ->
            createFile c onFile file

        Shacl.Form.Effect.GetGraph config onGraph iri ->
            Effect.fromAction (Action.GetGraph config onGraph iri)

        -- UI
        Shacl.Form.Effect.Focus id ->
            Effect.fromAction (Action.Focus id)

        Shacl.Form.Effect.CopyToClipboard blob ->
            Effect.fromAction (Action.CopyToClipboard blob)

        Shacl.Form.Effect.AddToast toast ->
            Effect.fromAction (Action.AddToast toast)

        Shacl.Form.Effect.AddToastError error ->
            Effect.fromAction (Action.toastFromError c error)

        Shacl.Form.Effect.TooltipMsg tooltipMsg ->
            Effect.fromAction (Action.TooltipMsg tooltipMsg)


getResource : C -> String -> (Maybe (List Option) -> msg) -> Iri -> Effect msg
getResource c workspaceUuid onOptions iri =
    if c.useTripleStore then
        getResourceFromBackend c workspaceUuid onOptions iri

    else
        getResourceFromQuadstore c onOptions iri


getResourceFromBackend :
    C
    -> String
    -> (Maybe (List Option) -> msg)
    -> Iri
    -> Effect msg
getResourceFromBackend c workspaceUuid onOptions iri =
    let
        onInstances response =
            case response of
                Err _ ->
                    Nothing

                Ok instances ->
                    instances
                        |> List.filterMap (optionFromInstance c)
                        |> Just
    in
    Effect.fromAction
        (Action.GetInstances (onInstances >> onOptions)
            { workspaceUuid = workspaceUuid
            , class = iri
            , includes = []
            }
        )


optionFromInstance : C -> Instance -> Maybe Option
optionFromInstance c instance =
    Just Option
        |> Maybe.hardcoded instance.this
        |> Maybe.required
            (instance.rdfs.label
                |> Maybe.map (Localize.forLabel c)
                |> Maybe.orElse (Rdf.labelFallback instance.this)
            )


getResourceFromQuadstore :
    C
    -> (Maybe (List Option) -> msg)
    -> Iri
    -> Effect msg
getResourceFromQuadstore c onOptions iri =
    let
        onRows rows =
            rows
                |> List.filterMap toTuple
                |> optionsFromPendingResource c
                |> Just

        toTuple terms =
            Just Tuple.pair
                |> Maybe.required
                    (terms
                        |> Dict.get "iri"
                        |> Maybe.andThen Rdf.toIri
                    )
                |> Maybe.required (Dict.get "label" terms)
    in
    [ SPARQL.graph (Node.var "graph")
        [ ( Node.var "iri", Node.iri a, Node.iri iri )
        , ( Node.var "iri", Node.iri RDFS.label, Node.var "label" )
        ]

    -- FIXME exclude draft and previous document version graphs
    ]
        |> SPARQL.select [ "iri", "label" ]
        |> SPARQL.toString
        |> Action.Query (onRows >> onOptions)
        |> Effect.fromAction


optionsFromPendingResource : C -> List ( Iri, BlankNodeOrIriOrAnyLiteral ) -> List Option
optionsFromPendingResource c allOptions =
    let
        addValuesForCurrentLocale collected =
            List.foldl collectValueForCurrentLocale collected allOptions

        addValuesWithAnyLocale collected =
            List.foldl collectValueWithAnyLocale collected allOptions

        addValuesWithoutLocale collected =
            List.foldl collectValueWithoutLocale collected allOptions

        collectValueForCurrentLocale ( iriOption, value ) collected =
            if DictIri.member iriOption collected then
                collected

            else
                case Rdf.toLangString value of
                    Just ( languageTag, valueInLocale ) ->
                        case ( String.toLower languageTag, c.locale ) of
                            ( "en", Locale.EnUS ) ->
                                DictIri.insert iriOption
                                    (String.toSentenceCase valueInLocale)
                                    collected

                            ( "de", Locale.De ) ->
                                DictIri.insert iriOption
                                    (String.toSentenceCase valueInLocale)
                                    collected

                            _ ->
                                collected

                    Nothing ->
                        collected

        collectValueWithAnyLocale ( iriOption, value ) collected =
            if DictIri.member iriOption collected then
                collected

            else
                case Rdf.toLangString value of
                    Just ( _, valueInLocale ) ->
                        DictIri.insert iriOption (String.toSentenceCase valueInLocale) collected

                    Nothing ->
                        collected

        collectValueWithoutLocale ( iriOption, value ) collected =
            if DictIri.member iriOption collected then
                collected

            else
                case Rdf.toString value of
                    Just valueString ->
                        DictIri.insert iriOption valueString collected

                    Nothing ->
                        collected
    in
    DictIri.empty
        |> addValuesForCurrentLocale
        |> addValuesWithAnyLocale
        |> addValuesWithoutLocale
        |> DictIri.map
            (\iriOption value ->
                { iri = iriOption
                , value = value
                }
            )
        |> DictIri.values


getSubclassesWithShaclShapes :
    C
    -> String
    -> (Api.Response (List Class) -> msg)
    -> Iri
    -> Effect msg
getSubclassesWithShaclShapes c workspaceUuid onResponse class =
    let
        iri =
            Api.iriHttpWith c
                [ "construct", "subclasses-with-shacl-shapes" ]
                [ Url.string "workspace" workspaceUuid
                , Url.string "class" (Rdf.toUrl class)
                ]
    in
    iri
        |> Action.GetGraph
            { store = False
            , reload = True
            , closure = False
            }
            (Result.map Ontology.Shacl.fromGraph >> onResponse)
        |> Effect.fromAction


createFile : C -> (Api.Response Graph -> msg) -> File -> Effect msg
createFile c onFile file =
    Api.request c
        { method = "POST"
        , headers = [ Http.header "Accept" "application/n-triples" ]
        , pathSegments = [ "raw" ]
        , queryParameters = []
        , body = Http.multipartBody [ Http.filePart "file" file ]
        , expect = Api.expectGraph onFile
        , tracker = Nothing
        }
