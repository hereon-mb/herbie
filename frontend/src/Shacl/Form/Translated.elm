module Shacl.Form.Translated exposing
    ( Field(..), TagField(..)
    , fromCanonical, Error(..), errorToString
    , groupFromField, orderFromField
    , DataCheckbox, DataCheckboxInstance
    , DataCollection
    , DataConstant
    , DataConstantInjectable
    , DataDateTime
    , DataFileUpload
    , DataForm, Success, Alternative, Failure
    , DataGrid, DataEditorGrid
    , DataIriReadOnly
    , DataNumber, NumberType(..)
    , DataSchedule
    , DataSelectMany
    , DataSelectOne, VariantSelectOne(..)
    , DataSelectOneInjectable
    , DataSingleNested
    , DataText
    , DataVariants, Variant
    , Value
    , Counts, zeros
    , Inject(..)
    , Optional(..), OptionalNested(..)
    , DataCollectionSelect
    )

{-|

@docs Field, TagField
@docs fromCanonical, Error, errorToString
@docs groupFromField, orderFromField

@docs DataCheckbox, DataCheckboxInstance
@docs DataCollection
@docs DataConstant
@docs DataConstantInjectable
@docs DataDateTime
@docs DataFileUpload
@docs DataForm, Success, Alternative, Failure
@docs DataGrid, DataEditorGrid
@docs DataIriReadOnly
@docs DataNumber, NumberType
@docs DataSchedule
@docs DataSelectMany
@docs DataSelectOne, VariantSelectOne
@docs DataSelectOneInjectable
@docs DataSingleNested
@docs DataText
@docs DataVariants, Variant

@docs Value
@docs Counts, zeros
@docs Inject
@docs Optional, OptionalNested

-}

import Decimal
import List.Extra as List
import List.NonEmpty as NonEmpty exposing (NonEmpty)
import Maybe.Extra as Maybe
import Maybe.Pipeline as Maybe
import Ontology
import Rdf exposing (AnyLiteral, BlankNodeOrIri, BlankNodeOrIriOrAnyLiteral, Iri, StringOrLangString)
import Rdf.Decode as Decode
import Rdf.Decode.Extra as Decode
import Rdf.Namespaces exposing (xsd)
import Rdf.Namespaces.RDFS as RDFS
import Rdf.Namespaces.SH as SH
import Rdf.PropertyPath as Rdf exposing (PropertyPath(..))
import Rdf.SetIri as SetIri
import Result.Extra as Result
import Result.Pipeline as Result
import Shacl
import Shacl.Form.Canonical as Canonical exposing (Editor(..))
import Shacl.Form.Collected exposing (Spec)
import Shacl.Namespaces exposing (hash)
import Shacl.OrderBy as Shacl
import Time


type TagField
    = TagFieldCheckbox
    | TagFieldCheckboxInstance
    | TagFieldCollection
    | TagFieldCollectionSelect
    | TagFieldConstant
    | TagFieldConstantInjectable
    | TagFieldDateTime
    | TagFieldFileUpload
    | TagFieldGrid
    | TagFieldIriReadOnly
    | TagFieldNumber
    | TagFieldSchedule
    | TagFieldSelectMany
    | TagFieldSelectOne
    | TagFieldSelectOneInjectable
    | TagFieldSingleNested
    | TagFieldText
    | TagFieldVariants


type Field
    = FieldCheckbox DataCheckbox
    | FieldCheckboxInstance DataCheckboxInstance
    | FieldCollection DataCollection
    | FieldCollectionSelect DataCollectionSelect
    | FieldConstant DataConstant
    | FieldConstantInjectable DataConstantInjectable
    | FieldDateTime DataDateTime
    | FieldFileUpload DataFileUpload
    | FieldForm DataForm
    | FieldGrid DataGrid
    | FieldIriReadOnly DataIriReadOnly
    | FieldNumber DataNumber
    | FieldSchedule DataSchedule
    | FieldSelectMany DataSelectMany
    | FieldSelectOne DataSelectOne
    | FieldSelectOneInjectable DataSelectOneInjectable
    | FieldSingleNested DataSingleNested
    | FieldText DataText
    | FieldVariants DataVariants


type alias DataCheckbox =
    { hash : Int
    , counts : Counts
    , path : PropertyPath
    , group : Maybe Iri
    , order : Float
    , names : List StringOrLangString
    , descriptions : List StringOrLangString
    , labelInCard : Shacl.ConfigLabel
    , readonly : Bool
    , generated : Bool
    , persisted : Bool
    , defaultValue : Maybe Bool
    }


type alias DataCheckboxInstance =
    { hash : Int
    , counts : Counts
    , path : PropertyPath
    , group : Maybe Iri
    , order : Float
    , names : List StringOrLangString
    , descriptions : List StringOrLangString
    , labelInCard : Shacl.ConfigLabel
    , readonly : Bool
    , generated : Bool
    , persisted : Bool
    , iri : Iri
    , classes : List Iri
    , defaultValue : Maybe Bool
    }


type alias DataCollection =
    { hash : Int
    , path : PropertyPath
    , group : Maybe Iri
    , order : Float
    , names : List StringOrLangString
    , descriptions : List StringOrLangString
    , labelInCard : Shacl.ConfigLabel
    , minCount : Int
    , maxCount : Int
    , field : Field
    , classes : NonEmpty Iri
    , orderPath : Maybe PropertyPath
    , viewer : Shacl.Viewer
    }


type alias DataCollectionSelect =
    { hash : Int
    , counts : Counts
    , path : PropertyPath
    , group : Maybe Iri
    , order : Float
    , names : List StringOrLangString
    , descriptions : List StringOrLangString
    , minCount : Int
    , maxCount : Int
    , field : Field
    , classes : NonEmpty Iri
    , selectOne : DataSelectOne
    }


type alias DataConstant =
    { hash : Int
    , counts : Counts
    , path : PropertyPath
    , value : BlankNodeOrIriOrAnyLiteral
    , group : Maybe Iri
    , order : Float
    , qualified : Bool
    }


type alias DataConstantInjectable =
    { hash : Int
    , counts : Counts
    , path : PropertyPath
    , value : Value
    , inject : Inject
    }


type alias DataDateTime =
    { hash : Int
    , counts : Counts
    , path : PropertyPath
    , group : Maybe Iri
    , order : Float
    , names : List StringOrLangString
    , descriptions : List StringOrLangString
    , labelInCard : Shacl.ConfigLabel
    , optional : Optional
    , readonly : Bool
    , generated : Bool
    , persisted : Bool
    , withTime : Bool
    , defaultValue : Maybe Time.Posix
    }


type alias DataFileUpload =
    { hash : Int
    , counts : Counts
    , path : PropertyPath
    , group : Maybe Iri
    , order : Float
    , names : List StringOrLangString
    , descriptions : List StringOrLangString
    , labelInCard : Shacl.ConfigLabel
    , optional : Optional
    , defaultValue : Maybe Iri
    }


type alias DataForm =
    { hash : Int
    , counts : Counts
    , successes : List Success
    , failures : List Failure
    , inject : Bool
    }


type alias Success =
    { field : Field
    , alternatives : List Alternative
    , errors : List ( TagField, Error )
    }


type alias Failure =
    { propertyShapes : List Shacl.PropertyShape
    , errors : List ( TagField, Error )
    }


type alias Alternative =
    { score : Int
    , field : Field
    }


type alias DataGrid =
    { hash : Int
    , path : PropertyPath
    , group : Maybe Iri
    , order : Float
    , names : List StringOrLangString
    , descriptions : List StringOrLangString
    , labelInCard : Shacl.ConfigLabel
    , minCount : Int
    , maxCount : Int
    , field : Field
    , classes : NonEmpty Iri
    , className : StringOrLangString
    , dataEditorGrid : DataEditorGrid
    }


type alias DataEditorGrid =
    { availableColumnsPath : PropertyPath
    , availableRowsPath : PropertyPath
    , columnPath : Iri
    , rowPath : Iri
    , cellViewer : Maybe Field
    , cellEditor : Maybe Field
    }


type alias DataIriReadOnly =
    { hash : Int
    , counts : Counts
    , path : PropertyPath
    , group : Maybe Iri
    , order : Float
    , names : List StringOrLangString
    , descriptions : List StringOrLangString
    , labelInCard : Shacl.ConfigLabel
    , classes : NonEmpty Iri
    , defaultValue : Maybe { iri : Iri, label : Maybe StringOrLangString }
    }


type alias DataNumber =
    { hash : Int
    , counts : Counts
    , path : PropertyPath
    , group : Maybe Iri
    , order : Float
    , names : List StringOrLangString
    , descriptions : List StringOrLangString
    , labelInCard : Shacl.ConfigLabel
    , numberType : NumberType
    , minExclusive : Maybe AnyLiteral
    , minInclusive : Maybe AnyLiteral
    , maxExclusive : Maybe AnyLiteral
    , maxInclusive : Maybe AnyLiteral
    , optional : Optional
    , readonly : Bool
    , generated : Bool
    , persisted : Bool
    , defaultValue : Maybe String
    }


type alias DataSchedule =
    { hash : Int
    , path : PropertyPath
    , group : Maybe Iri
    , order : Float
    , names : List StringOrLangString
    , descriptions : List StringOrLangString
    , labelInCard : Shacl.ConfigLabel
    , minCount : Int
    , maxCount : Int
    , field : Field
    , classes : NonEmpty Iri
    , viewer : Shacl.Viewer
    , editorDayOffsetPath : PropertyPath
    , editorOrderPath : PropertyPath
    , editorTitlePath : PropertyPath
    , editorDatePath : PropertyPath

    -- FIXME
    -- instancesSample :
    --   List
    --       { label : StringOrLangString
    --       , fields : List field
    --       }
    }


type alias DataSelectMany =
    { hash : Int
    , counts : Counts
    , path : PropertyPath
    , group : Maybe Iri
    , order : Float
    , names : List StringOrLangString
    , descriptions : List StringOrLangString
    , labelInCard : Shacl.ConfigLabel
    , minCount : Int
    , maxCount : Int
    , readonly : Bool
    , generated : Bool
    , persisted : Bool
    , class : Iri
    , node : BlankNodeOrIri
    , defaultValue : List Value
    , orderBy : Maybe Shacl.OrderBy
    , options : List Value
    }


type alias DataSelectOne =
    { hash : Int
    , counts : Counts
    , path : PropertyPath
    , group : Maybe Iri
    , order : Float
    , names : List StringOrLangString
    , descriptions : List StringOrLangString
    , labelInCard : Shacl.ConfigLabel
    , optional : Optional
    , readonly : Bool
    , generated : Bool
    , persisted : Bool
    , class : Iri
    , node : BlankNodeOrIri
    , variant : VariantSelectOne
    , defaultValue : Maybe Value
    , orderBy : Maybe Shacl.OrderBy
    , options : List Value
    }


type alias DataSelectOneInjectable =
    { hash : Int
    , counts : Counts
    , path : PropertyPath
    , readonly : Bool
    , generated : Bool
    , persisted : Bool
    , defaultValue : Maybe Value
    , orderBy : Maybe Shacl.OrderBy
    , options : List Value
    , inject : Inject
    }


type alias DataSingleNested =
    { hash : Int
    , counts : Counts
    , path : PropertyPath
    , group : Maybe Iri
    , order : Float
    , field : Field
    , classes : List Iri
    , names : List StringOrLangString
    , qualified : Bool
    , optional : OptionalNested
    , node : BlankNodeOrIri
    , mintIri : Bool
    }


type alias DataText =
    { hash : Int
    , counts : Counts
    , path : PropertyPath
    , group : Maybe Iri
    , order : Float
    , names : List StringOrLangString
    , descriptions : List StringOrLangString
    , labelInCard : Shacl.ConfigLabel
    , optional : Optional
    , readonly : Bool
    , generated : Bool
    , persisted : Bool
    , singleLine : Bool
    , defaultValue : Maybe String
    }


type alias DataVariants =
    { hash : Int
    , group : Maybe Iri
    , order : Float
    , variants : NonEmpty Variant
    }


type alias Variant =
    { hash : Int
    , names : List StringOrLangString
    , descriptions : List StringOrLangString
    , targetClasses : List Iri
    , classes : List Iri
    , field : Field
    }


type alias Value =
    { this : Iri
    , label : StringOrLangString
    }


valueFromShaclValue : Shacl.Value -> Maybe Value
valueFromShaclValue value =
    Just Value
        |> Maybe.required (Rdf.toIri value.this)
        |> Maybe.required value.label


type alias Counts =
    { numberOfNonInjectableRequiredAtomicFields : Int
    , numberOfNonInjectableOptionalAtomicFields : Int
    , numberOfInjectableAtomicFields : Int
    , numberOfInjectingAtomicFields : Int
    }


zeros : Counts
zeros =
    { numberOfNonInjectableRequiredAtomicFields = 0
    , numberOfNonInjectableOptionalAtomicFields = 0
    , numberOfInjectableAtomicFields = 0
    , numberOfInjectingAtomicFields = 0
    }


sumCounts : Counts -> Counts -> Counts
sumCounts countsA countsB =
    { numberOfNonInjectableRequiredAtomicFields =
        countsA.numberOfNonInjectableRequiredAtomicFields
            + countsB.numberOfNonInjectableRequiredAtomicFields
    , numberOfNonInjectableOptionalAtomicFields =
        countsA.numberOfNonInjectableOptionalAtomicFields
            + countsB.numberOfNonInjectableOptionalAtomicFields
    , numberOfInjectableAtomicFields =
        countsA.numberOfInjectableAtomicFields
            + countsB.numberOfInjectableAtomicFields
    , numberOfInjectingAtomicFields =
        countsA.numberOfInjectingAtomicFields
            + countsB.numberOfInjectingAtomicFields
    }


getCounts : Field -> Maybe Counts
getCounts field =
    case field of
        FieldCheckbox data ->
            Just data.counts

        FieldCheckboxInstance data ->
            Just data.counts

        FieldCollection _ ->
            Nothing

        FieldCollectionSelect data ->
            Just data.counts

        FieldConstant data ->
            Just data.counts

        FieldConstantInjectable data ->
            Just data.counts

        FieldDateTime data ->
            Just data.counts

        FieldFileUpload data ->
            Just data.counts

        FieldForm data ->
            Just data.counts

        FieldGrid _ ->
            Nothing

        FieldIriReadOnly data ->
            Just data.counts

        FieldNumber data ->
            Just data.counts

        FieldSchedule _ ->
            Nothing

        FieldSelectMany data ->
            Just data.counts

        FieldSelectOne data ->
            Just data.counts

        FieldSelectOneInjectable data ->
            Just data.counts

        FieldSingleNested data ->
            Just data.counts

        FieldText data ->
            Just data.counts

        FieldVariants _ ->
            Nothing


{-| A atomic field can be optional via several ways:

  - `OptionalByItself`: The original property shape had `sh:minCount 0`
  - `OptionalViaNesting`: This happens in the following situation: The property
    shape has `sh:minCount 1`, but it is nested inside (a chain of) node shapes
    with `sh:maxCount 1` and the outer most having `sh:minCount 0`, and there is
    no other atomic field which requires user input or cannot be injected in this
    field, inside the sub tree starting at the node shape with `sh:minCount 0`.
  - `Required`: The field is neither optional by itself or induced by some
    nesting.

-}
type Optional
    = OptionalByItself
    | OptionalViaNesting
    | Required


optionalFromBool : Bool -> Optional
optionalFromBool bool =
    if bool then
        OptionalByItself

    else
        Required


type NumberType
    = NumberInteger
    | NumberFloat
    | NumberDecimal


numberTypeFromIri : Iri -> Maybe NumberType
numberTypeFromIri iri =
    if iri == xsd "int" then
        Just NumberInteger

    else if iri == xsd "integer" then
        Just NumberInteger

    else if iri == xsd "double" then
        Just NumberFloat

    else if iri == xsd "decimal" then
        Just NumberDecimal

    else
        Nothing


type VariantSelectOne
    = Dropdown
    | RadioButtons


type Inject
    = InjectPrefix
    | InjectSuffix


{-| The optionality of a node shape with `sh:maxCount 1` can be displayed in
several ways:

  - `OptionalViaCheckbox`: The node shape has `sh:minCount 0` and there is no
    unique nested field which can have `OptionalViaNesting`.
  - `OptionalViaNesting`: The node shape has `sh:minCount 0` and there exists
    a uniquely determinable nested field which is by itself required but can be
    set to `OptionalViaNesting`.
  - `RequiredNested`: The node shape has `sh:minCount 1`.

-}
type OptionalNested
    = OptionalViaCheckbox
    | OptionalViaNested
    | RequiredNested


groupFromField : Field -> Maybe Iri
groupFromField field =
    case field of
        FieldForm _ ->
            Nothing

        FieldText { group } ->
            group

        FieldCheckbox { group } ->
            group

        FieldCheckboxInstance { group } ->
            group

        FieldConstant { group } ->
            group

        FieldConstantInjectable _ ->
            Nothing

        FieldDateTime { group } ->
            group

        FieldNumber { group } ->
            group

        FieldSelectOne { group } ->
            group

        FieldSelectOneInjectable _ ->
            Nothing

        FieldSelectMany { group } ->
            group

        FieldFileUpload { group } ->
            group

        FieldIriReadOnly { group } ->
            group

        FieldVariants { group } ->
            group

        FieldCollection { group } ->
            group

        FieldCollectionSelect { group } ->
            group

        FieldSingleNested { group } ->
            group

        FieldSchedule { group } ->
            group

        FieldGrid { group } ->
            group


orderFromField : Field -> Float
orderFromField field =
    case field of
        FieldForm _ ->
            infinity

        FieldText { order } ->
            order

        FieldCheckbox { order } ->
            order

        FieldCheckboxInstance { order } ->
            order

        FieldConstant { order } ->
            order

        FieldConstantInjectable _ ->
            infinity

        FieldDateTime { order } ->
            order

        FieldNumber { order } ->
            order

        FieldSelectOne { order } ->
            order

        FieldSelectOneInjectable _ ->
            infinity

        FieldSelectMany { order } ->
            order

        FieldFileUpload { order } ->
            order

        FieldIriReadOnly { order } ->
            order

        FieldVariants { order } ->
            order

        FieldCollection { order } ->
            order

        FieldCollectionSelect { order } ->
            order

        FieldSingleNested { order } ->
            order

        FieldSchedule { order } ->
            order

        FieldGrid { order } ->
            order


type Error
    = -- sh:minCount and sh:maxCount
      MinCountTooHigh
        { propertyShapes : List Shacl.PropertyShape
        , tagField : TagField
        , minCountMaximum : Int
        , minCountGiven : Int
        }
    | MinCountTooLow
        { propertyShapes : List Shacl.PropertyShape
        , tagField : TagField
        , minCountMinimum : Int
        , minCountGiven : Int
        }
    | MaxCountTooHigh
        { propertyShapes : List Shacl.PropertyShape
        , tagField : TagField
        , maxCountMaximum : Int
        , maxCountGiven : Int
        }
    | MaxCountTooLow
        { propertyShapes : List Shacl.PropertyShape
        , tagField : TagField
        , maxCountMinimum : Int
        , maxCountGiven : Int
        }
    | MinCountAboveMaxCount
        { propertyShapes : List Shacl.PropertyShape
        , tagField : TagField
        , minCountGiven : Int
        , maxCountGiven : Int
        }
    | WrongMinCount
        { propertyShapes : List Shacl.PropertyShape
        , tagField : TagField
        , minCountExpected : Int
        , minCountGiven : Int
        }
    | WrongMaxCount
        { propertyShapes : List Shacl.PropertyShape
        , tagField : TagField
        , maxCountExpected : Int
        , maxCountGiven : Int
        }
      -- sh:datatype
    | MissingDatatype
        { propertyShapes : List Shacl.PropertyShape
        , tagField : TagField
        , datatypeExpected : NonEmpty Iri
        }
    | WrongDatatype
        { propertyShapes : List Shacl.PropertyShape
        , tagField : TagField
        , datatypeExpected : NonEmpty Iri
        , datatypeGiven : Iri
        }
      -- sh:nodeKind
    | MissingNodeKind
        { propertyShapes : List Shacl.PropertyShape
        , tagField : TagField
        , nodeKindExpected : NonEmpty Shacl.NodeKind
        }
    | WrongNodeKind
        { propertyShapes : List Shacl.PropertyShape
        , tagField : TagField
        , nodeKindExpected : NonEmpty Shacl.NodeKind
        , nodeKindGiven : Shacl.NodeKind
        }
      -- sh:class
    | MissingClass
        { propertyShapes : List Shacl.PropertyShape
        , tagField : TagField
        }
    | TooManyClasses
        { propertyShapes : List Shacl.PropertyShape
        , tagField : TagField
        , countMax : Int
        , countGiven : Int
        }
    | MissingLabelForClass
        { propertyShapes : List Shacl.PropertyShape
        , tagField : TagField
        , class : Iri
        }
      -- sh:value
    | MissingValue
        { propertyShapes : List Shacl.PropertyShape
        , tagField : TagField
        }
    | TooManyValues
        { propertyShapes : List Shacl.PropertyShape
        , tagField : TagField
        , countMax : Int
        , countGiven : Int
        }
    | ValueNotIri
        { propertyShapes : List Shacl.PropertyShape
        , tagField : TagField
        , value : BlankNodeOrIriOrAnyLiteral
        }
    | MissingLabelForValue
        { propertyShapes : List Shacl.PropertyShape
        , tagField : TagField
        , value : BlankNodeOrIriOrAnyLiteral
        }
      -- dash:editor
    | MissingEditor
        { propertyShapes : List Shacl.PropertyShape
        , tagField : TagField
        }
    | WrongEditor
        { propertyShapes : List Shacl.PropertyShape
        , tagField : TagField
        , noEditorAllowed : Bool
        , editorExpected : NonEmpty Editor
        , editorGiven : Editor
        }
    | MissingScheduleEditorDayOffsetPath (List Shacl.PropertyShape)
    | MissingScheduleEditorOrderPath (List Shacl.PropertyShape)
    | MissingScheduleEditorTitlePath (List Shacl.PropertyShape)
    | MissingScheduleEditorDatePath (List Shacl.PropertyShape)
    | MissingVariant
        { nodeShapes : List Shacl.NodeShape
        }
    | IncompatibleGroups
    | IncompatibleDefaultValue BlankNodeOrIriOrAnyLiteral
    | TooManyDefaultValues (List BlankNodeOrIriOrAnyLiteral)
    | NotImplemented
      -- INJECTION
    | TooManyInjectables
    | TooManySlots
    | NoSlotForInjectable
      -- COLLECTION SELECT
    | FieldNotAllowedWithinCollectionSelect
        { tagField : TagField
        }
    | OptionalSingleNestedNotAllowedWithinCollectionSelect
    | SelectOneMustBeRequiredWithinCollectionSelect
    | NoSelectOneFound
    | TooManySelectOnes


errorToString : Error -> String
errorToString error =
    let
        nodeShapeNodes nodeShapes =
            nodeShapes
                |> List.map (.node >> Rdf.serializeNode)
                |> String.join ", "
    in
    case error of
        -- sh:minCount and sh:maxCount
        MinCountTooHigh data ->
            "For a "
                ++ input data.tagField
                ++ " the sh:minCount must be lower than "
                ++ String.fromInt data.minCountMaximum
                ++ ", but it was "
                ++ String.fromInt data.minCountGiven
                ++ "."

        MinCountTooLow data ->
            "For a "
                ++ input data.tagField
                ++ " the sh:minCount must be higher than "
                ++ String.fromInt data.minCountMinimum
                ++ ", but it was "
                ++ String.fromInt data.minCountGiven
                ++ "."

        MaxCountTooHigh data ->
            "For a "
                ++ input data.tagField
                ++ " the sh:maxCount must be lower than "
                ++ String.fromInt data.maxCountMaximum
                ++ ", but it was "
                ++ String.fromInt data.maxCountGiven
                ++ "."

        MaxCountTooLow data ->
            "For a "
                ++ input data.tagField
                ++ " the sh:maxCount must be higher than "
                ++ String.fromInt data.maxCountMinimum
                ++ ", but it was "
                ++ String.fromInt data.maxCountGiven
                ++ "."

        MinCountAboveMaxCount data ->
            "The sh:minCount is above the sh:maxCount: "
                ++ greaterThan data.minCountGiven data.maxCountGiven
                ++ "."

        WrongMinCount data ->
            "The "
                ++ input data.tagField
                ++ " expects a sh:minCount of "
                ++ String.fromInt data.minCountExpected
                ++ ", but I found "
                ++ String.fromInt data.minCountGiven
                ++ "."

        WrongMaxCount data ->
            "The "
                ++ input data.tagField
                ++ " expects a sh:max of "
                ++ String.fromInt data.maxCountExpected
                ++ ", but I found "
                ++ String.fromInt data.maxCountGiven
                ++ "."

        -- sh:datatype
        MissingDatatype data ->
            "The "
                ++ input data.tagField
                ++ " expects the sh:datatype to be "
                ++ oneOf Rdf.serializeNode data.datatypeExpected
                ++ "."

        WrongDatatype data ->
            "The "
                ++ input data.tagField
                ++ " expects the sh:datatype to be "
                ++ oneOf Rdf.serializeNode data.datatypeExpected
                ++ ", but got "
                ++ Rdf.serializeNode data.datatypeGiven
                ++ "."

        -- sh:nodeKind
        MissingNodeKind data ->
            "The "
                ++ input data.tagField
                ++ " expects the sh:nodeKind to be "
                ++ oneOf nodeKindToString data.nodeKindExpected
                ++ "."

        WrongNodeKind data ->
            "The "
                ++ input data.tagField
                ++ " expects the sh:nodeKind to be "
                ++ oneOf nodeKindToString data.nodeKindExpected
                ++ ", but got "
                ++ nodeKindToString data.nodeKindGiven
                ++ "."

        -- sh:class
        MissingClass data ->
            "You have to give an sh:class for the " ++ input data.tagField ++ "."

        TooManyClasses data ->
            "You have provided "
                ++ String.fromInt data.countGiven
                ++ " sh:class's for the "
                ++ input data.tagField
                ++ ", but there should be at most "
                ++ String.fromInt data.countMax
                ++ "."

        MissingLabelForClass data ->
            "I could not find an rdfs:label for the class "
                ++ Rdf.serializeNode data.class
                ++ " which is required by the "
                ++ input data.tagField
                ++ "."

        -- sh:value
        MissingValue data ->
            "You have to give an sh:value for the " ++ input data.tagField ++ "."

        TooManyValues data ->
            "You have provided "
                ++ String.fromInt data.countGiven
                ++ " sh:value's for the "
                ++ input data.tagField
                ++ ", but there should be at most "
                ++ String.fromInt data.countMax
                ++ "."

        ValueNotIri data ->
            "The sh:value should be an IRI but I found " ++ Rdf.serializeNode data.value ++ "."

        MissingLabelForValue data ->
            "I could not find an rdfs:label for the value "
                ++ Rdf.serializeNode data.value
                ++ " which is required by the "
                ++ input data.tagField
                ++ "."

        -- dash:editor
        MissingEditor data ->
            "You have to give a dash:editor for the " ++ input data.tagField ++ "."

        WrongEditor data ->
            "The "
                ++ input data.tagField
                ++ " expects the dash:editor to be "
                ++ oneOf editorToString data.editorExpected
                ++ (if data.noEditorAllowed then
                        ", or no dash:editor at all"

                    else
                        ""
                   )
                ++ ", but I found "
                ++ editorToString data.editorGiven

        MissingScheduleEditorDayOffsetPath propertyShapes ->
            "The property shape " ++ propertyShapeNodes propertyShapes ++ " defines no day offset path for the schedule editor."

        MissingScheduleEditorOrderPath propertyShapes ->
            "The property shape " ++ propertyShapeNodes propertyShapes ++ " defines no order path for the schedule editor."

        MissingScheduleEditorTitlePath propertyShapes ->
            "The property shape " ++ propertyShapeNodes propertyShapes ++ " defines no title path for the schedule editor."

        MissingScheduleEditorDatePath propertyShapes ->
            "The property shape " ++ propertyShapeNodes propertyShapes ++ " defines no date path for the schedule editor."

        MissingVariant { nodeShapes } ->
            "I could not find a variant for the node shape " ++ nodeShapeNodes nodeShapes ++ " during translation."

        IncompatibleGroups ->
            "I found fields inside one variant which belong to different groups during translation."

        IncompatibleDefaultValue node ->
            "The default value encountered is incompatible: " ++ Rdf.serializeNode node ++ "."

        TooManyDefaultValues nodes ->
            "There are too many default values specified: " ++ String.join ", " (List.map Rdf.serializeNode nodes) ++ "."

        NotImplemented ->
            "I ran into an unimplemented part during the translation stage."

        -- INJECTION
        TooManyInjectables ->
            "I encountered more then one injectable at a level."

        TooManySlots ->
            "I found an injectable field but more then one slot for injection."

        NoSlotForInjectable ->
            "I could not find a slot for an injectable field."

        -- COLLECTION SELECT
        FieldNotAllowedWithinCollectionSelect data ->
            "A "
                ++ input data.tagField
                ++ " is not possible within a select many with nesting component."

        OptionalSingleNestedNotAllowedWithinCollectionSelect ->
            "An optional single nested field is not possible within a select many with nesting component."

        SelectOneMustBeRequiredWithinCollectionSelect ->
            "The select one field within a select many with nesting component cannot be optional."

        NoSelectOneFound ->
            "I did not find any suitable select one component."

        TooManySelectOnes ->
            "I found more then one suitable select one component but a select many with nesting needs exactly one."


tagToString : TagField -> String
tagToString tag =
    case tag of
        TagFieldText ->
            "text"

        TagFieldCheckbox ->
            "checkbox"

        TagFieldCheckboxInstance ->
            "checkbox-via-instance"

        TagFieldConstant ->
            "constant"

        TagFieldConstantInjectable ->
            "constant-injectable"

        TagFieldDateTime ->
            "date-time"

        TagFieldNumber ->
            "number"

        TagFieldSelectOne ->
            "select-one"

        TagFieldSelectOneInjectable ->
            "select-one-injectable"

        TagFieldSelectMany ->
            "select-many"

        TagFieldFileUpload ->
            "file-upload"

        TagFieldIriReadOnly ->
            "IRI-readonly"

        TagFieldCollection ->
            "collection"

        TagFieldCollectionSelect ->
            "collection-select"

        TagFieldSingleNested ->
            "single-nested"

        TagFieldSchedule ->
            "schedule"

        TagFieldGrid ->
            "grid"

        TagFieldVariants ->
            "variants"


propertyShapeNodes : List Shacl.PropertyShape -> String
propertyShapeNodes propertyShapes =
    propertyShapes
        |> List.map (.node >> Rdf.serializeNode)
        |> String.join ", "


input : TagField -> String
input tag =
    tagToString tag ++ " input"


nodeKindToString : Shacl.NodeKind -> String
nodeKindToString n =
    Rdf.serializeNode
        (case n of
            Shacl.NodeKindBlankNode ->
                SH.blankNode

            Shacl.NodeKindIri ->
                SH.iri

            Shacl.NodeKindLiteral ->
                SH.literal

            Shacl.NodeKindBlankNodeOrIri ->
                SH.blankNodeOrIri

            Shacl.NodeKindBlankNodeOrLiteral ->
                SH.blankNodeOrLiteral

            Shacl.NodeKindIriOrLiteral ->
                SH.iriOrLiteral
        )


oneOf : (a -> String) -> NonEmpty a -> String
oneOf aToString nonEmpty =
    if NonEmpty.length nonEmpty == 1 then
        aToString (NonEmpty.head nonEmpty)

    else
        "one of "
            ++ String.join ", " (List.map aToString (NonEmpty.toList nonEmpty))


greaterThan : Int -> Int -> String
greaterThan a b =
    String.fromInt a ++ " > " ++ String.fromInt b


editorToString : Canonical.Editor -> String
editorToString e =
    case e of
        Canonical.EditorDetails ->
            "details editor"

        Canonical.EditorInstanceSelect ->
            "instance select"

        Canonical.EditorRadioButtons ->
            "radio buttons"

        Canonical.EditorGrid _ ->
            "grid"

        Canonical.EditorSchedule _ ->
            "schedule"


fromCanonical : Spec -> Canonical.ShapeNode -> Result Error DataForm
fromCanonical spec node =
    fromCanonicalHelp spec node
        |> Result.map computeCounts
        |> Result.andThen computeInjections
        |> Result.andThen adjustOptionality
        |> Result.map (propagateNamesAndDescriptions [] [])


type Injection
    = SlotPresent
    | InjectablePresent


{-| Compute at which level injections should be performed.


# Examples


## Working

    * Form
      * SingleNested
        * Form                        Inject
          * Number                      SlotPresent
          * ConstantInjectable          InjectablePresent

    * Form
      * SingleNested (Optional)
        * Form                        Inject
          * Number                      SlotPresent
          * ConstantInjectable          InjectablePresent

    * Form
      * SingleNested
        * Form                        Inject
          * SingleNested                SlotPresent
            * Form                        SlotPresent
              * Number                      SlotPresent
          * SingleNested                InjectablePresent
            * Form                        InjectablePresent
              * ConstantInjectable          InjectablePresent

    * Form
      * SingleNested
        * Form                        Inject
          * Number                      SlotPresent
          * SelectMany
          * ConstantInjectable          InjectablePresent


## Not working

    * Form                        InjectablePresent
      * SingleNested                InjectablePresent
        * Form                        InjectablePresent
          * Number (Optional)
          * ConstantInjectable          InjectablePresent

    => Error: no Slot for Injectable

    * Form
      * Collection
        * Form                        SlotPresent
          * Number                      SlotPresent
      * Collection
        * Form                        InjectablePresent
          * ConstantInjectable          InjectablePresent

    => Error: no Slot for Injectable

    * Form
      * SingleNested
        * Form
          * Number                      SlotPresent
          * SingleNested                SlotPresent
            * Form                        SlotPresent
              * Number                      SlotPresent
          * ConstantInjectable                 InjectablePresent

    => Error: too many Slots for Injectable

    * Form
      * SingleNested
        * Form
          * Number                      SlotPresent
          * ConstantInjectable          InjectablePresent
          * SingleNested                InjectablePresent
            * Form                        InjectablePresent
              * ConstantInjectable          InjectablePresent

    => Error: too many Injectables

-}
computeInjections : DataForm -> Result Error DataForm
computeInjections data =
    computeInjectionsForm data
        |> Result.andThen
            (\( dataUpdated, injection ) ->
                if Maybe.isJust injection then
                    Err NoSlotForInjectable

                else
                    Ok dataUpdated
            )


computeInjectionsForm : DataForm -> Result Error ( DataForm, Maybe Injection )
computeInjectionsForm data =
    data.successes
        |> List.map computeInjectionsSuccess
        |> Result.combine
        |> Result.andThen
            (\computed ->
                let
                    ( successes, injections ) =
                        computed
                            |> List.unzip
                            |> Tuple.mapSecond (List.filterMap identity)

                    countSlots =
                        List.count ((==) SlotPresent) injections

                    countInjectables =
                        List.count ((==) InjectablePresent) injections
                in
                if countInjectables > 1 then
                    Err TooManyInjectables

                else if countInjectables == 1 then
                    if countSlots > 1 then
                        Err TooManySlots

                    else if countSlots == 1 then
                        Ok
                            ( { data
                                | successes = successes
                                , inject = True
                              }
                            , Nothing
                            )

                    else
                        Ok
                            ( { data | successes = successes }
                            , Just InjectablePresent
                            )

                else
                    Ok
                        ( { data | successes = successes }
                        , Nothing
                        )
            )


computeInjectionsSuccess : Success -> Result Error ( Success, Maybe Injection )
computeInjectionsSuccess success =
    computeInjectionsField success.field
        |> Result.map (Tuple.mapFirst (\fieldUpdated -> { success | field = fieldUpdated }))


computeInjectionsField : Field -> Result Error ( Field, Maybe Injection )
computeInjectionsField field =
    case field of
        FieldCheckbox _ ->
            Ok ( field, Nothing )

        FieldCheckboxInstance _ ->
            Ok ( field, Nothing )

        FieldCollection data ->
            data.field
                |> computeInjectionsField
                |> Result.andThen
                    (\( fieldUpdated, injection ) ->
                        if injection == Just InjectablePresent then
                            Err NoSlotForInjectable

                        else
                            Ok
                                ( FieldCollection { data | field = fieldUpdated }
                                , Nothing
                                )
                    )

        FieldCollectionSelect _ ->
            Ok ( field, Nothing )

        FieldConstant _ ->
            Ok ( field, Nothing )

        FieldConstantInjectable _ ->
            Ok ( field, Just InjectablePresent )

        FieldDateTime _ ->
            Ok ( field, Nothing )

        FieldFileUpload _ ->
            Ok ( field, Nothing )

        FieldForm data ->
            computeInjectionsForm data
                |> Result.map (Tuple.mapFirst FieldForm)

        FieldGrid data ->
            data.field
                |> computeInjectionsField
                |> Result.andThen
                    (\( fieldUpdated, injection ) ->
                        if injection == Just InjectablePresent then
                            Err NoSlotForInjectable

                        else
                            Ok
                                ( FieldGrid { data | field = fieldUpdated }
                                , Nothing
                                )
                    )

        FieldIriReadOnly _ ->
            Ok ( field, Nothing )

        FieldNumber _ ->
            Ok ( field, Just SlotPresent )

        FieldSchedule data ->
            data.field
                |> computeInjectionsField
                |> Result.andThen
                    (\( fieldUpdated, injection ) ->
                        if injection == Just InjectablePresent then
                            Err NoSlotForInjectable

                        else
                            Ok
                                ( FieldSchedule { data | field = fieldUpdated }
                                , Nothing
                                )
                    )

        FieldSelectMany _ ->
            Ok ( field, Nothing )

        FieldSelectOne _ ->
            Ok ( field, Nothing )

        FieldSelectOneInjectable _ ->
            Ok ( field, Just InjectablePresent )

        FieldSingleNested data ->
            data.field
                |> computeInjectionsField
                |> Result.andThen
                    (\( fieldUpdated, injection ) ->
                        case data.optional of
                            OptionalViaCheckbox ->
                                if injection == Just InjectablePresent then
                                    Err NoSlotForInjectable

                                else
                                    Ok
                                        ( FieldSingleNested { data | field = fieldUpdated }
                                        , Nothing
                                        )

                            OptionalViaNested ->
                                Ok
                                    ( FieldSingleNested { data | field = fieldUpdated }
                                    , injection
                                    )

                            RequiredNested ->
                                Ok
                                    ( FieldSingleNested { data | field = fieldUpdated }
                                    , injection
                                    )
                    )

        FieldText _ ->
            Ok ( field, Nothing )

        FieldVariants data ->
            let
                updateVariants variantsUpdated =
                    ( FieldVariants { data | variants = variantsUpdated }
                    , Nothing
                    )
            in
            data.variants
                |> NonEmpty.map computeInjectsVariant
                |> Tuple.mapSecond Result.combine
                |> Result.combineBoth
                |> Result.map updateVariants


computeInjectsVariant : Variant -> Result Error Variant
computeInjectsVariant variant =
    variant.field
        |> computeInjectionsField
        |> Result.andThen
            (\( fieldUpdated, injection ) ->
                if injection == Just InjectablePresent then
                    Err NoSlotForInjectable

                else
                    Ok { variant | field = fieldUpdated }
            )


computeCounts : DataForm -> DataForm
computeCounts data =
    let
        successes =
            List.map computeCountsSuccess data.successes
    in
    { data
        | counts =
            successes
                |> List.filterMap (.field >> getCounts)
                |> List.foldl sumCounts zeros
        , successes = successes
    }


computeCountsSuccess : Success -> Success
computeCountsSuccess success =
    { success | field = computeCountsField success.field }


computeCountsField : Field -> Field
computeCountsField field =
    case field of
        FieldCheckbox _ ->
            field

        FieldCheckboxInstance _ ->
            field

        FieldCollection data ->
            FieldCollection { data | field = computeCountsField data.field }

        FieldCollectionSelect _ ->
            field

        FieldConstant _ ->
            field

        FieldConstantInjectable _ ->
            field

        FieldDateTime _ ->
            field

        FieldFileUpload _ ->
            field

        FieldForm data ->
            FieldForm (computeCounts data)

        FieldGrid data ->
            FieldGrid { data | field = computeCountsField data.field }

        FieldIriReadOnly _ ->
            field

        FieldNumber _ ->
            field

        FieldSchedule data ->
            FieldSchedule { data | field = computeCountsField data.field }

        FieldSelectMany _ ->
            field

        FieldSelectOne _ ->
            field

        FieldSelectOneInjectable _ ->
            field

        FieldSingleNested data ->
            let
                fieldUpdated =
                    computeCountsField data.field
            in
            case data.optional of
                OptionalViaCheckbox ->
                    FieldSingleNested
                        { data
                            | counts = zeros
                            , field = fieldUpdated
                        }

                OptionalViaNested ->
                    FieldSingleNested
                        { data
                            | counts = Maybe.withDefault zeros (getCounts fieldUpdated)
                            , field = fieldUpdated
                        }

                RequiredNested ->
                    FieldSingleNested
                        { data
                            | counts = Maybe.withDefault zeros (getCounts fieldUpdated)
                            , field = fieldUpdated
                        }

        FieldText _ ->
            field

        FieldVariants data ->
            let
                variantsUpdated =
                    NonEmpty.map
                        (\variant ->
                            { variant | field = computeCountsField variant.field }
                        )
                        data.variants
            in
            FieldVariants { data | variants = variantsUpdated }


adjustOptionality : DataForm -> Result Error DataForm
adjustOptionality data =
    data.successes
        |> List.map adjustOptionalitySuccess
        |> Result.combine
        |> Result.map (\successes -> { data | successes = successes })


adjustOptionalitySuccess : Success -> Result Error Success
adjustOptionalitySuccess success =
    adjustOptionalityField success.field
        |> Result.map (\field -> { success | field = field })


adjustOptionalityField : Field -> Result Error Field
adjustOptionalityField field =
    case field of
        FieldCheckbox _ ->
            Ok field

        FieldCheckboxInstance _ ->
            Ok field

        FieldCollection data ->
            data.field
                |> adjustOptionalityField
                |> Result.map (\fieldUpdated -> FieldCollection { data | field = fieldUpdated })

        FieldCollectionSelect _ ->
            Ok field

        FieldConstant _ ->
            Ok field

        FieldConstantInjectable _ ->
            Ok field

        FieldDateTime _ ->
            Ok field

        FieldFileUpload _ ->
            Ok field

        FieldForm data ->
            data
                |> adjustOptionality
                |> Result.map FieldForm

        FieldGrid data ->
            data.field
                |> adjustOptionalityField
                |> Result.map (\fieldUpdated -> FieldGrid { data | field = fieldUpdated })

        FieldIriReadOnly _ ->
            Ok field

        FieldNumber _ ->
            Ok field

        FieldSchedule data ->
            data.field
                |> adjustOptionalityField
                |> Result.map (\fieldUpdated -> FieldSchedule { data | field = fieldUpdated })

        FieldSelectMany _ ->
            Ok field

        FieldSelectOne _ ->
            Ok field

        FieldSelectOneInjectable _ ->
            Ok field

        FieldSingleNested data ->
            case data.optional of
                OptionalViaCheckbox ->
                    case getCounts data.field of
                        Nothing ->
                            Ok field

                        Just counts ->
                            if counts.numberOfNonInjectableRequiredAtomicFields == 1 then
                                data.field
                                    |> makeNonInjectableAtomicFieldOptional
                                    |> Result.map
                                        (\fieldUpdated ->
                                            FieldSingleNested
                                                { data
                                                    | field = fieldUpdated
                                                    , optional = OptionalViaNested
                                                }
                                        )

                            else
                                data.field
                                    |> adjustOptionalityField
                                    |> Result.map
                                        (\fieldUpdated ->
                                            FieldSingleNested { data | field = fieldUpdated }
                                        )

                OptionalViaNested ->
                    data.field
                        |> adjustOptionalityField
                        |> Result.map
                            (\fieldUpdated ->
                                FieldSingleNested { data | field = fieldUpdated }
                            )

                RequiredNested ->
                    data.field
                        |> adjustOptionalityField
                        |> Result.map
                            (\fieldUpdated ->
                                FieldSingleNested { data | field = fieldUpdated }
                            )

        FieldText _ ->
            Ok field

        FieldVariants _ ->
            Ok field


makeNonInjectableAtomicFieldOptional : Field -> Result Error Field
makeNonInjectableAtomicFieldOptional field =
    case field of
        FieldCheckbox _ ->
            Ok field

        FieldCheckboxInstance _ ->
            Ok field

        FieldCollection _ ->
            Ok field

        FieldCollectionSelect _ ->
            Ok field

        FieldConstant _ ->
            Ok field

        FieldConstantInjectable _ ->
            Ok field

        FieldDateTime data ->
            Ok (FieldDateTime { data | optional = OptionalViaNesting })

        FieldFileUpload data ->
            Ok (FieldFileUpload { data | optional = OptionalViaNesting })

        FieldForm data ->
            data.successes
                |> List.map
                    (\success ->
                        success.field
                            |> makeNonInjectableAtomicFieldOptional
                            |> Result.map (\fieldUpdated -> { success | field = fieldUpdated })
                    )
                |> Result.combine
                |> Result.map (\successes -> FieldForm { data | successes = successes })

        FieldGrid _ ->
            Ok field

        FieldIriReadOnly _ ->
            Ok field

        FieldNumber data ->
            Ok (FieldNumber { data | optional = OptionalViaNesting })

        FieldSchedule _ ->
            Ok field

        FieldSelectMany _ ->
            Ok field

        FieldSelectOne data ->
            Ok (FieldSelectOne { data | optional = OptionalViaNesting })

        FieldSelectOneInjectable _ ->
            Ok field

        FieldSingleNested data ->
            data.field
                |> makeNonInjectableAtomicFieldOptional
                |> Result.map (\fieldUpdated -> FieldSingleNested { data | field = fieldUpdated })

        FieldText data ->
            Ok (FieldText { data | optional = OptionalViaNesting })

        FieldVariants _ ->
            Ok field


propagateNamesAndDescriptions :
    List StringOrLangString
    -> List StringOrLangString
    -> DataForm
    -> DataForm
propagateNamesAndDescriptions names descriptions data =
    { data
        | successes =
            List.map (propagateNamesAndDescriptionsSuccess names descriptions)
                data.successes
    }


propagateNamesAndDescriptionsSuccess :
    List StringOrLangString
    -> List StringOrLangString
    -> Success
    -> Success
propagateNamesAndDescriptionsSuccess names descriptions success =
    { success
        | field =
            propagateNamesAndDescriptionsField
                names
                descriptions
                success.field
    }


propagateNamesAndDescriptionsField :
    List StringOrLangString
    -> List StringOrLangString
    -> Field
    -> Field
propagateNamesAndDescriptionsField names descriptions field =
    case field of
        FieldCheckbox data ->
            FieldCheckbox
                { data
                    | names = names ++ data.names
                    , descriptions = descriptions ++ data.descriptions
                }

        FieldCheckboxInstance data ->
            FieldCheckboxInstance
                { data
                    | names = names ++ data.names
                    , descriptions = descriptions ++ data.descriptions
                }

        FieldCollection data ->
            FieldCollection { data | field = propagateNamesAndDescriptionsField [] [] data.field }

        FieldCollectionSelect data ->
            FieldCollectionSelect
                { data
                    | names = names ++ data.names
                    , descriptions = descriptions ++ data.descriptions
                }

        FieldConstant _ ->
            field

        FieldConstantInjectable _ ->
            field

        FieldDateTime data ->
            FieldDateTime
                { data
                    | names = names ++ data.names
                    , descriptions = descriptions ++ data.descriptions
                }

        FieldFileUpload data ->
            FieldFileUpload
                { data
                    | names = names ++ data.names
                    , descriptions = descriptions ++ data.descriptions
                }

        FieldForm data ->
            FieldForm (propagateNamesAndDescriptions names descriptions data)

        FieldGrid _ ->
            field

        FieldIriReadOnly data ->
            FieldIriReadOnly
                { data
                    | names = names ++ data.names
                    , descriptions = descriptions ++ data.descriptions
                }

        FieldNumber data ->
            FieldNumber
                { data
                    | names = names ++ data.names
                    , descriptions = descriptions ++ data.descriptions
                }

        FieldSchedule data ->
            FieldSchedule { data | field = propagateNamesAndDescriptionsField [] [] data.field }

        FieldSelectMany data ->
            FieldSelectMany
                { data
                    | names = names ++ data.names
                    , descriptions = descriptions ++ data.descriptions
                }

        FieldSelectOne data ->
            FieldSelectOne
                { data
                    | names = names ++ data.names
                    , descriptions = descriptions ++ data.descriptions
                }

        FieldSelectOneInjectable _ ->
            field

        FieldSingleNested data ->
            let
                ( namesPropagated, descriptionsPropagated ) =
                    case getCounts data.field of
                        Nothing ->
                            ( []
                            , []
                            )

                        Just counts ->
                            if
                                (counts.numberOfNonInjectableRequiredAtomicFields == 1)
                                    && (counts.numberOfNonInjectableOptionalAtomicFields == 0)
                            then
                                ( names ++ data.names
                                , descriptions
                                )

                            else
                                ( []
                                , []
                                )
            in
            FieldSingleNested
                { data
                    | field =
                        propagateNamesAndDescriptionsField
                            namesPropagated
                            descriptionsPropagated
                            data.field
                }

        FieldText data ->
            FieldText
                { data
                    | names = names ++ data.names
                    , descriptions = descriptions ++ data.descriptions
                }

        FieldVariants data ->
            FieldVariants
                { data
                    | variants =
                        NonEmpty.map
                            (\variant ->
                                { variant | field = propagateNamesAndDescriptionsField [] [] variant.field }
                            )
                            data.variants
                }


fromCanonicalHelp : Spec -> Canonical.ShapeNode -> Result Error DataForm
fromCanonicalHelp spec ((Canonical.ShapeNode ({ nodeShapes, ors, properties } as data)) as node) =
    let
        ( successes, failures ) =
            properties
                |> List.map (successFromMerged spec nodeShapes)
                |> Result.partition

        variants =
            ors
                |> List.map (fieldVariants spec node)
                |> Result.combine
    in
    Ok DataForm
        |> Result.hardcoded data.hash
        |> Result.hardcoded zeros
        |> Result.required (Result.map (List.append successes) variants)
        |> Result.hardcoded failures
        |> Result.hardcoded False


fieldForm : Spec -> Canonical.ShapeNode -> Result Error Field
fieldForm spec node =
    fromCanonicalHelp spec node
        |> Result.map FieldForm


successFromMerged : Spec -> List Shacl.NodeShape -> Canonical.ShapeProperty -> Result Failure Success
successFromMerged spec nodeShapes property =
    case property of
        Canonical.Leaf data dataLeaf ->
            let
                ( successes, errors ) =
                    [ ( TagFieldText, Result.map (Alternative 10) (fieldText spec data dataLeaf) )
                    , ( TagFieldCheckbox, Result.map (Alternative 10) (fieldCheckbox spec data dataLeaf) )
                    , ( TagFieldCheckboxInstance
                      , Result.map (Alternative 20) (fieldCheckboxInstance spec nodeShapes data dataLeaf)
                      )
                    , ( TagFieldConstant, Result.map (Alternative 10) (fieldConstant spec nodeShapes data dataLeaf) )
                    , ( TagFieldDateTime, Result.map (Alternative 10) (fieldDateTime spec data dataLeaf) )
                    , ( TagFieldNumber, Result.map (Alternative 10) (fieldNumber spec data dataLeaf) )
                    , ( TagFieldSelectOne, Result.map (Alternative 10) (fieldSelectOne spec data dataLeaf) )
                    , ( TagFieldSelectOne, Result.map (Alternative 10) (fieldSelectOneInjectable spec data dataLeaf) )
                    , ( TagFieldSelectMany, Result.map (Alternative 10) (fieldSelectMany spec data dataLeaf) )
                    , ( TagFieldFileUpload, Result.map (Alternative 10) (fieldFileUpload spec data) )
                    , ( TagFieldIriReadOnly, Result.map (Alternative 5) (fieldIriReadOnly spec data dataLeaf) )
                    ]
                        |> List.map (\( tag, alternative ) -> Result.mapError (Tuple.pair tag) alternative)
                        |> Result.partition

                sorted =
                    successes
                        |> List.sortBy .score
                        |> List.reverse
            in
            case sorted of
                [] ->
                    Err
                        { propertyShapes = data.propertyShapes
                        , errors = errors
                        }

                { field } :: alternatives ->
                    Ok
                        { field = field
                        , alternatives = alternatives
                        , errors = errors
                        }

        Canonical.Nested data dataNested ->
            let
                ( successes, errors ) =
                    [ ( TagFieldGrid, fieldGrid spec data dataNested )
                    , ( TagFieldSchedule, fieldSchedule spec data dataNested )
                    , ( TagFieldCollectionSelect, fieldCollectionSelect spec data dataNested )
                    , ( TagFieldCollection, fieldCollection spec data dataNested )
                    , ( TagFieldSingleNested
                      , -- takes precedence over `FieldCollection`
                        Result.map (Alternative 15) (fieldSingleNested spec data dataNested)
                      )
                    ]
                        |> List.map (\( tag, alternative ) -> Result.mapError (Tuple.pair tag) alternative)
                        |> Result.partition

                sorted =
                    successes
                        |> List.sortBy .score
                        |> List.reverse
            in
            case sorted of
                [] ->
                    Err
                        { propertyShapes = data.propertyShapes
                        , errors = errors
                        }

                { field } :: alternatives ->
                    Ok
                        { field = field
                        , alternatives = alternatives
                        , errors = errors
                        }


fieldText : Spec -> Canonical.DataShapeProperty -> Canonical.DataLeaf -> Result Error Field
fieldText spec data dataLeaf =
    case dataLeaf.datatype of
        Nothing ->
            Err
                (MissingDatatype
                    { propertyShapes = data.propertyShapes
                    , tagField = TagFieldText
                    , datatypeExpected = NonEmpty.singleton (xsd "string")
                    }
                )

        Just datatype ->
            if datatype /= xsd "string" then
                Err
                    (WrongDatatype
                        { propertyShapes = data.propertyShapes
                        , tagField = TagFieldText
                        , datatypeExpected = NonEmpty.singleton (xsd "string")
                        , datatypeGiven = datatype
                        }
                    )

            else
                getOptionalForLeaf TagFieldText data
                    |> Result.andThen
                        (\optional ->
                            let
                                counts =
                                    if optional then
                                        { zeros | numberOfNonInjectableOptionalAtomicFields = 1 }

                                    else
                                        { zeros | numberOfNonInjectableRequiredAtomicFields = 1 }
                            in
                            Ok DataText
                                |> Result.hardcoded data.hash
                                |> Result.hardcoded counts
                                |> Result.hardcoded data.path
                                |> Result.hardcoded data.group
                                |> Result.hardcoded data.order
                                |> Result.hardcoded (getNames spec data)
                                |> Result.hardcoded (getDescriptions spec data)
                                |> Result.hardcoded data.labelInCard
                                |> Result.hardcoded (optionalFromBool optional)
                                |> Result.hardcoded data.readonly
                                |> Result.hardcoded data.generated
                                |> Result.hardcoded data.persisted
                                |> Result.hardcoded (Maybe.withDefault True dataLeaf.singleLine)
                                |> Result.required (getDefaultValue Rdf.toString data.defaultValue)
                                |> Result.map FieldText
                        )


fieldCheckbox : Spec -> Canonical.DataShapeProperty -> Canonical.DataLeaf -> Result Error Field
fieldCheckbox spec data dataLeaf =
    case dataLeaf.datatype of
        Nothing ->
            Err
                (MissingDatatype
                    { propertyShapes = data.propertyShapes
                    , tagField = TagFieldCheckbox
                    , datatypeExpected = NonEmpty.singleton (xsd "boolean")
                    }
                )

        Just datatype ->
            if datatype /= xsd "boolean" then
                Err
                    (WrongDatatype
                        { propertyShapes = data.propertyShapes
                        , tagField = TagFieldCheckbox
                        , datatypeExpected = NonEmpty.singleton (xsd "boolean")
                        , datatypeGiven = datatype
                        }
                    )

            else if data.minCount < 1 then
                Err
                    (MinCountTooLow
                        { propertyShapes = data.propertyShapes
                        , tagField = TagFieldCheckbox
                        , minCountMinimum = 1
                        , minCountGiven = data.minCount
                        }
                    )

            else
                Ok DataCheckbox
                    |> Result.hardcoded data.hash
                    |> Result.hardcoded { zeros | numberOfNonInjectableRequiredAtomicFields = 1 }
                    |> Result.hardcoded data.path
                    |> Result.hardcoded data.group
                    |> Result.hardcoded data.order
                    |> Result.hardcoded (getNames spec data)
                    |> Result.hardcoded (getDescriptions spec data)
                    |> Result.hardcoded data.labelInCard
                    |> Result.hardcoded data.readonly
                    |> Result.hardcoded data.generated
                    |> Result.hardcoded data.persisted
                    |> Result.required (getDefaultValue Rdf.toBool data.defaultValue)
                    |> Result.map FieldCheckbox


fieldCheckboxInstance :
    Spec
    -> List Shacl.NodeShape
    -> Canonical.DataShapeProperty
    -> Canonical.DataLeaf
    -> Result Error Field
fieldCheckboxInstance spec _ data dataLeaf =
    case dataLeaf.values of
        [] ->
            Err
                (MissingValue
                    { propertyShapes = data.propertyShapes
                    , tagField = TagFieldCheckbox
                    }
                )

        [ value ] ->
            case Rdf.toIri value.this of
                Nothing ->
                    Err
                        (ValueNotIri
                            { propertyShapes = data.propertyShapes
                            , tagField = TagFieldCheckbox
                            , value = value.this
                            }
                        )

                Just iri ->
                    if data.minCount /= 0 then
                        Err
                            (WrongMaxCount
                                { propertyShapes = data.propertyShapes
                                , tagField = TagFieldCheckbox
                                , maxCountExpected = 1
                                , maxCountGiven = data.maxCount
                                }
                            )

                    else if data.maxCount /= 1 then
                        Err
                            (WrongMinCount
                                { propertyShapes = data.propertyShapes
                                , tagField = TagFieldCheckbox
                                , minCountExpected = 1
                                , minCountGiven = data.minCount
                                }
                            )

                    else
                        Ok DataCheckboxInstance
                            |> Result.hardcoded data.hash
                            |> Result.hardcoded { zeros | numberOfNonInjectableRequiredAtomicFields = 1 }
                            |> Result.hardcoded data.path
                            |> Result.hardcoded data.group
                            |> Result.hardcoded data.order
                            |> Result.hardcoded (getNames spec data)
                            |> Result.hardcoded (getDescriptions spec data)
                            |> Result.hardcoded data.labelInCard
                            |> Result.hardcoded data.readonly
                            |> Result.hardcoded data.generated
                            |> Result.hardcoded data.persisted
                            |> Result.hardcoded iri
                            |> Result.hardcoded data.classes
                            |> Result.required (getDefaultValue (toCheckboxInstanceDefaultValue iri) data.defaultValue)
                            |> Result.map FieldCheckboxInstance

        _ ->
            Err
                (TooManyValues
                    { propertyShapes = data.propertyShapes
                    , tagField = TagFieldCheckbox
                    , countGiven = List.length dataLeaf.values
                    , countMax = 1
                    }
                )


toCheckboxInstanceDefaultValue : Iri -> Rdf.Node compatible -> Maybe Bool
toCheckboxInstanceDefaultValue value node =
    case Rdf.toIri node of
        Nothing ->
            Nothing

        Just iriOther ->
            if iriOther == value then
                Nothing

            else
                Just True


fieldConstant :
    Spec
    -> List Shacl.NodeShape
    -> Canonical.DataShapeProperty
    -> Canonical.DataLeaf
    -> Result Error Field
fieldConstant _ _ data dataLeaf =
    if data.minCount < 1 then
        Err
            (MinCountTooLow
                { propertyShapes = data.propertyShapes
                , tagField = TagFieldConstant
                , minCountMinimum = 1
                , minCountGiven = data.minCount
                }
            )

    else if data.maxCount > 1 then
        Err
            (MaxCountTooHigh
                { propertyShapes = data.propertyShapes
                , tagField = TagFieldConstant
                , maxCountMaximum = 1
                , maxCountGiven = data.maxCount
                }
            )

    else
        case dataLeaf.values of
            [] ->
                Err
                    (MissingValue
                        { propertyShapes = data.propertyShapes
                        , tagField = TagFieldConstant
                        }
                    )

            [ value ] ->
                case data.includes of
                    [ include ] ->
                        let
                            iri =
                                value.this
                                    |> Rdf.toIri
                                    |> Result.fromMaybe
                                        (ValueNotIri
                                            { propertyShapes = data.propertyShapes
                                            , tagField = TagFieldConstantInjectable
                                            , value = value.this
                                            }
                                        )

                            label =
                                value.label
                                    |> Result.fromMaybe
                                        (MissingLabelForValue
                                            { propertyShapes = data.propertyShapes
                                            , tagField = TagFieldConstantInjectable
                                            , value = value.this
                                            }
                                        )
                        in
                        if include == hash "Prefix" then
                            let
                                counts =
                                    { zeros | numberOfInjectableAtomicFields = 1 }
                            in
                            Ok DataConstantInjectable
                                |> Result.hardcoded data.hash
                                |> Result.hardcoded counts
                                |> Result.hardcoded data.path
                                |> Result.required
                                    (Ok Value
                                        |> Result.required iri
                                        |> Result.required label
                                    )
                                |> Result.hardcoded InjectPrefix
                                |> Result.map FieldConstantInjectable

                        else if include == hash "Suffix" then
                            let
                                counts =
                                    { zeros | numberOfInjectableAtomicFields = 1 }
                            in
                            Ok DataConstantInjectable
                                |> Result.hardcoded data.hash
                                |> Result.hardcoded counts
                                |> Result.hardcoded data.path
                                |> Result.required
                                    (Ok Value
                                        |> Result.required iri
                                        |> Result.required label
                                    )
                                |> Result.hardcoded InjectSuffix
                                |> Result.map FieldConstantInjectable

                        else
                            Ok DataConstant
                                |> Result.hardcoded data.hash
                                |> Result.hardcoded zeros
                                |> Result.hardcoded data.path
                                |> Result.hardcoded value.this
                                |> Result.hardcoded data.group
                                |> Result.hardcoded data.order
                                |> Result.hardcoded data.qualified
                                |> Result.map FieldConstant

                    _ ->
                        Ok DataConstant
                            |> Result.hardcoded data.hash
                            |> Result.hardcoded zeros
                            |> Result.hardcoded data.path
                            |> Result.hardcoded value.this
                            |> Result.hardcoded data.group
                            |> Result.hardcoded data.order
                            |> Result.hardcoded data.qualified
                            |> Result.map FieldConstant

            _ ->
                Err
                    (TooManyValues
                        { propertyShapes = data.propertyShapes
                        , tagField = TagFieldConstant
                        , countMax = 1
                        , countGiven = List.length dataLeaf.values
                        }
                    )


fieldDateTime : Spec -> Canonical.DataShapeProperty -> Canonical.DataLeaf -> Result Error Field
fieldDateTime spec data dataLeaf =
    case dataLeaf.datatype of
        Nothing ->
            Err
                (MissingDatatype
                    { propertyShapes = data.propertyShapes
                    , tagField = TagFieldDateTime
                    , datatypeExpected = NonEmpty.fromCons (xsd "date") [ xsd "dateTime" ]
                    }
                )

        Just datatype ->
            getOptionalForLeaf TagFieldDateTime data
                |> Result.andThen
                    (\optional ->
                        let
                            counts =
                                if optional then
                                    { zeros | numberOfNonInjectableOptionalAtomicFields = 1 }

                                else
                                    { zeros | numberOfNonInjectableRequiredAtomicFields = 1 }
                        in
                        if datatype == xsd "date" then
                            Ok DataDateTime
                                |> Result.hardcoded data.hash
                                |> Result.hardcoded counts
                                |> Result.hardcoded data.path
                                |> Result.hardcoded data.group
                                |> Result.hardcoded data.order
                                |> Result.hardcoded (getNames spec data)
                                |> Result.hardcoded (getDescriptions spec data)
                                |> Result.hardcoded data.labelInCard
                                |> Result.hardcoded (optionalFromBool optional)
                                |> Result.hardcoded data.readonly
                                |> Result.hardcoded data.generated
                                |> Result.hardcoded data.persisted
                                |> Result.hardcoded False
                                |> Result.required (getDefaultValue Rdf.toDate data.defaultValue)
                                |> Result.map FieldDateTime

                        else if datatype == xsd "dateTime" then
                            Ok DataDateTime
                                |> Result.hardcoded data.hash
                                |> Result.hardcoded counts
                                |> Result.hardcoded data.path
                                |> Result.hardcoded data.group
                                |> Result.hardcoded data.order
                                |> Result.hardcoded (getNames spec data)
                                |> Result.hardcoded (getDescriptions spec data)
                                |> Result.hardcoded data.labelInCard
                                |> Result.hardcoded (optionalFromBool optional)
                                |> Result.hardcoded data.readonly
                                |> Result.hardcoded data.generated
                                |> Result.hardcoded data.persisted
                                |> Result.hardcoded True
                                |> Result.required (getDefaultValue Rdf.toDateTime data.defaultValue)
                                |> Result.map FieldDateTime

                        else
                            Err
                                (WrongDatatype
                                    { propertyShapes = data.propertyShapes
                                    , tagField = TagFieldDateTime
                                    , datatypeExpected = NonEmpty.fromCons (xsd "date") [ xsd "dateTime" ]
                                    , datatypeGiven = datatype
                                    }
                                )
                    )


fieldNumber : Spec -> Canonical.DataShapeProperty -> Canonical.DataLeaf -> Result Error Field
fieldNumber spec data dataLeaf =
    case dataLeaf.datatype of
        Nothing ->
            Err
                (MissingDatatype
                    { propertyShapes = data.propertyShapes
                    , tagField = TagFieldNumber
                    , datatypeExpected =
                        NonEmpty.fromCons
                            (xsd "int")
                            [ xsd "integer"
                            , xsd "double"
                            , xsd "decimal"
                            ]
                    }
                )

        Just datatype ->
            case numberTypeFromIri datatype of
                Nothing ->
                    Err
                        (WrongDatatype
                            { propertyShapes = data.propertyShapes
                            , tagField = TagFieldNumber
                            , datatypeExpected =
                                NonEmpty.fromCons
                                    (xsd "int")
                                    [ xsd "integer"
                                    , xsd "double"
                                    , xsd "decimal"
                                    ]
                            , datatypeGiven = datatype
                            }
                        )

                Just numberType ->
                    getOptionalForLeaf TagFieldNumber data
                        |> Result.andThen
                            (\optional ->
                                let
                                    counts =
                                        if optional then
                                            { zeros
                                                | numberOfNonInjectableOptionalAtomicFields = 1
                                                , numberOfInjectingAtomicFields = 1
                                            }

                                        else
                                            { zeros
                                                | numberOfNonInjectableRequiredAtomicFields = 1
                                                , numberOfInjectingAtomicFields = 1
                                            }
                                in
                                Ok DataNumber
                                    |> Result.hardcoded data.hash
                                    |> Result.hardcoded counts
                                    |> Result.hardcoded data.path
                                    |> Result.hardcoded data.group
                                    |> Result.hardcoded data.order
                                    |> Result.hardcoded (getNames spec data)
                                    |> Result.hardcoded (getDescriptions spec data)
                                    |> Result.hardcoded data.labelInCard
                                    |> Result.hardcoded numberType
                                    |> Result.hardcoded data.minExclusive
                                    |> Result.hardcoded data.minInclusive
                                    |> Result.hardcoded data.maxExclusive
                                    |> Result.hardcoded data.maxInclusive
                                    |> Result.hardcoded (optionalFromBool optional)
                                    |> Result.hardcoded data.readonly
                                    |> Result.hardcoded data.generated
                                    |> Result.hardcoded data.persisted
                                    |> Result.required (getDefaultValue (toNumberDefaultValue numberType) data.defaultValue)
                                    |> Result.map FieldNumber
                            )


toNumberDefaultValue : NumberType -> Rdf.Node compatible -> Maybe String
toNumberDefaultValue numberType node =
    case numberType of
        NumberInteger ->
            Rdf.toInt node
                |> Maybe.map (\int -> String.fromInt int)

        NumberFloat ->
            Rdf.toFloat node
                |> Maybe.map (\float -> String.fromFloat float)

        NumberDecimal ->
            Rdf.toDecimal node
                |> Maybe.map (\decimal -> Decimal.toString decimal)


fieldSelectOne : Spec -> Canonical.DataShapeProperty -> Canonical.DataLeaf -> Result Error Field
fieldSelectOne spec data dataLeaf =
    case ( dataLeaf.editor, dataLeaf.ins ) of
        ( Nothing, [] ) ->
            -- FIXME If we succeed here we break optional via checkbox logic
            Err NotImplemented

        ( Nothing, _ :: _ ) ->
            fieldSelectOneHelp spec data dataLeaf RadioButtons

        ( Just EditorRadioButtons, _ ) ->
            fieldSelectOneHelp spec data dataLeaf RadioButtons

        ( Just EditorInstanceSelect, _ ) ->
            fieldSelectOneHelp spec data dataLeaf Dropdown

        ( Just editor, _ ) ->
            Err
                (WrongEditor
                    { propertyShapes = data.propertyShapes
                    , tagField = TagFieldSelectOne
                    , noEditorAllowed = True
                    , editorExpected =
                        NonEmpty.fromCons
                            EditorRadioButtons
                            [ EditorInstanceSelect ]
                    , editorGiven = editor
                    }
                )


fieldSelectOneHelp :
    Spec
    -> Canonical.DataShapeProperty
    -> Canonical.DataLeaf
    -> VariantSelectOne
    -> Result Error Field
fieldSelectOneHelp spec data dataLeaf variant =
    if data.minCount < 0 then
        Err
            (MinCountTooLow
                { propertyShapes = data.propertyShapes
                , tagField = TagFieldSelectOne
                , minCountMinimum = 0
                , minCountGiven = data.minCount
                }
            )

    else if data.minCount > data.maxCount then
        Err
            (MinCountAboveMaxCount
                { propertyShapes = data.propertyShapes
                , tagField = TagFieldSelectOne
                , minCountGiven = data.minCount
                , maxCountGiven = data.maxCount
                }
            )

    else if data.maxCount /= 1 then
        Err
            (WrongMaxCount
                { propertyShapes = data.propertyShapes
                , tagField = TagFieldSelectOne
                , maxCountExpected = 1
                , maxCountGiven = data.maxCount
                }
            )

    else
        case data.classes of
            [] ->
                Err
                    (MissingClass
                        { propertyShapes = data.propertyShapes
                        , tagField = TagFieldSelectOne
                        }
                    )

            [ class ] ->
                case data.propertyShapes of
                    [ propertyShape ] ->
                        case data.includes of
                            [ _ ] ->
                                -- FIXME Return proper error
                                Err NotImplemented

                            _ ->
                                let
                                    optional =
                                        data.minCount == 0

                                    options =
                                        if List.isEmpty dataLeaf.ins then
                                            spec.data
                                                |> Decode.decode
                                                    (Decode.fromInstancesOf class
                                                        (Decode.succeed Value
                                                            |> Decode.custom Decode.iri
                                                            |> Decode.required RDFS.label Decode.stringOrLangString
                                                        )
                                                    )
                                                |> Result.withDefault []

                                        else
                                            List.filterMap valueFromShaclValue dataLeaf.ins

                                    counts =
                                        if optional then
                                            { zeros | numberOfNonInjectableOptionalAtomicFields = 1 }

                                        else
                                            case data.includes of
                                                [ include ] ->
                                                    if include == hash "Prefix" then
                                                        { zeros | numberOfInjectableAtomicFields = 1 }

                                                    else if include == hash "Suffix" then
                                                        { zeros | numberOfInjectableAtomicFields = 1 }

                                                    else
                                                        { zeros | numberOfNonInjectableRequiredAtomicFields = 1 }

                                                _ ->
                                                    { zeros | numberOfNonInjectableRequiredAtomicFields = 1 }
                                in
                                Ok DataSelectOne
                                    |> Result.hardcoded data.hash
                                    |> Result.hardcoded counts
                                    |> Result.hardcoded data.path
                                    |> Result.hardcoded data.group
                                    |> Result.hardcoded data.order
                                    |> Result.hardcoded (getNames spec data)
                                    |> Result.hardcoded (getDescriptions spec data)
                                    |> Result.hardcoded data.labelInCard
                                    |> Result.hardcoded (optionalFromBool optional)
                                    |> Result.hardcoded data.readonly
                                    |> Result.hardcoded data.generated
                                    |> Result.hardcoded data.persisted
                                    |> Result.hardcoded class
                                    |> Result.hardcoded propertyShape.node
                                    |> Result.hardcoded variant
                                    |> Result.required (getDefaultValue (toSelectDefaultValue spec) data.defaultValue)
                                    |> Result.hardcoded data.orderBy
                                    |> Result.hardcoded options
                                    |> Result.map FieldSelectOne

                    _ ->
                        -- FIXME Make multiple property shapes possible
                        Err NotImplemented

            _ ->
                Err
                    (TooManyClasses
                        { propertyShapes = data.propertyShapes
                        , tagField = TagFieldSelectOne
                        , countMax = 1
                        , countGiven = List.length data.classes
                        }
                    )


fieldSelectOneInjectable : Spec -> Canonical.DataShapeProperty -> Canonical.DataLeaf -> Result Error Field
fieldSelectOneInjectable spec data dataLeaf =
    case dataLeaf.ins of
        [] ->
            -- FIXME If we succeed here we break optional via checkbox logic
            Err NotImplemented

        _ ->
            if data.minCount < 1 then
                Err
                    (MinCountTooLow
                        { propertyShapes = data.propertyShapes
                        , tagField = TagFieldSelectOne
                        , minCountMinimum = 1
                        , minCountGiven = data.minCount
                        }
                    )

            else if data.minCount > data.maxCount then
                Err
                    (MinCountAboveMaxCount
                        { propertyShapes = data.propertyShapes
                        , tagField = TagFieldSelectOne
                        , minCountGiven = data.minCount
                        , maxCountGiven = data.maxCount
                        }
                    )

            else if data.maxCount /= 1 then
                Err
                    (WrongMaxCount
                        { propertyShapes = data.propertyShapes
                        , tagField = TagFieldSelectOne
                        , maxCountExpected = 1
                        , maxCountGiven = data.maxCount
                        }
                    )

            else
                case data.propertyShapes of
                    [ _ ] ->
                        case data.includes of
                            [ include ] ->
                                let
                                    counts =
                                        { zeros | numberOfInjectableAtomicFields = 1 }

                                    options =
                                        dataLeaf.ins
                                            |> List.filterMap valueFromShaclValue

                                    optional =
                                        data.minCount == 0
                                in
                                if optional then
                                    Err
                                        (MinCountTooLow
                                            { propertyShapes = data.propertyShapes
                                            , tagField = TagFieldSelectOneInjectable
                                            , minCountMinimum = 1
                                            , minCountGiven = data.minCount
                                            }
                                        )

                                else if include == hash "Prefix" then
                                    Ok DataSelectOneInjectable
                                        |> Result.hardcoded data.hash
                                        |> Result.hardcoded counts
                                        |> Result.hardcoded data.path
                                        |> Result.hardcoded data.readonly
                                        |> Result.hardcoded data.generated
                                        |> Result.hardcoded data.persisted
                                        |> Result.required (getDefaultValue (toSelectDefaultValue spec) data.defaultValue)
                                        |> Result.hardcoded data.orderBy
                                        |> Result.hardcoded options
                                        |> Result.hardcoded InjectPrefix
                                        |> Result.map FieldSelectOneInjectable

                                else if include == hash "Suffix" then
                                    Ok DataSelectOneInjectable
                                        |> Result.hardcoded data.hash
                                        |> Result.hardcoded counts
                                        |> Result.hardcoded data.path
                                        |> Result.hardcoded data.readonly
                                        |> Result.hardcoded data.generated
                                        |> Result.hardcoded data.persisted
                                        |> Result.required (getDefaultValue (toSelectDefaultValue spec) data.defaultValue)
                                        |> Result.hardcoded data.orderBy
                                        |> Result.hardcoded options
                                        |> Result.hardcoded InjectSuffix
                                        |> Result.map FieldSelectOneInjectable

                                else
                                    -- FIXME Return proper error
                                    Err NotImplemented

                            _ ->
                                -- FIXME Return proper error
                                Err NotImplemented

                    _ ->
                        -- FIXME Make multiple property shapes possible
                        Err NotImplemented


fieldSelectMany : Spec -> Canonical.DataShapeProperty -> Canonical.DataLeaf -> Result Error Field
fieldSelectMany spec data dataLeaf =
    case dataLeaf.editor of
        Nothing ->
            Err
                (MissingEditor
                    { propertyShapes = data.propertyShapes
                    , tagField = TagFieldSelectMany
                    }
                )

        Just editor ->
            case editor of
                EditorInstanceSelect ->
                    if data.minCount < 0 then
                        Err
                            (MinCountTooLow
                                { propertyShapes = data.propertyShapes
                                , tagField = TagFieldSelectMany
                                , minCountMinimum = 0
                                , minCountGiven = data.minCount
                                }
                            )

                    else if data.minCount > data.maxCount then
                        Err
                            (MinCountAboveMaxCount
                                { propertyShapes = data.propertyShapes
                                , tagField = TagFieldSelectMany
                                , minCountGiven = data.minCount
                                , maxCountGiven = data.maxCount
                                }
                            )

                    else if data.maxCount < 2 then
                        Err
                            (MaxCountTooLow
                                { propertyShapes = data.propertyShapes
                                , tagField = TagFieldSelectMany
                                , maxCountMinimum = 2
                                , maxCountGiven = data.maxCount
                                }
                            )

                    else
                        case data.classes of
                            [] ->
                                Err
                                    (MissingClass
                                        { propertyShapes = data.propertyShapes
                                        , tagField = TagFieldSelectMany
                                        }
                                    )

                            [ class ] ->
                                case data.propertyShapes of
                                    [ propertyShape ] ->
                                        let
                                            options =
                                                if List.isEmpty dataLeaf.ins then
                                                    spec.data
                                                        |> Decode.decode
                                                            (Decode.fromInstancesOf class
                                                                (Decode.succeed Value
                                                                    |> Decode.custom Decode.iri
                                                                    |> Decode.required RDFS.label Decode.stringOrLangString
                                                                )
                                                            )
                                                        |> Result.withDefault []

                                                else
                                                    List.filterMap valueFromShaclValue dataLeaf.ins

                                            counts =
                                                { zeros | numberOfNonInjectableRequiredAtomicFields = 1 }
                                        in
                                        Ok DataSelectMany
                                            |> Result.hardcoded data.hash
                                            |> Result.hardcoded counts
                                            |> Result.hardcoded data.path
                                            |> Result.hardcoded data.group
                                            |> Result.hardcoded data.order
                                            |> Result.hardcoded (getNames spec data)
                                            |> Result.hardcoded (getDescriptions spec data)
                                            |> Result.hardcoded data.labelInCard
                                            |> Result.hardcoded data.minCount
                                            |> Result.hardcoded data.maxCount
                                            |> Result.hardcoded data.readonly
                                            |> Result.hardcoded data.generated
                                            |> Result.hardcoded data.persisted
                                            |> Result.hardcoded class
                                            |> Result.hardcoded propertyShape.node
                                            |> Result.required (getDefaultValues (toSelectDefaultValue spec) data.defaultValue)
                                            |> Result.hardcoded data.orderBy
                                            |> Result.hardcoded options
                                            |> Result.map FieldSelectMany

                                    _ ->
                                        -- FIXME Make multiple property shapes possible
                                        Err NotImplemented

                            _ ->
                                Err
                                    (TooManyClasses
                                        { propertyShapes = data.propertyShapes
                                        , tagField = TagFieldSelectMany
                                        , countMax = 1
                                        , countGiven = List.length data.classes
                                        }
                                    )

                _ ->
                    Err
                        (WrongEditor
                            { propertyShapes = data.propertyShapes
                            , tagField = TagFieldSelectMany
                            , noEditorAllowed = False
                            , editorExpected = NonEmpty.singleton EditorInstanceSelect
                            , editorGiven = editor
                            }
                        )


toSelectDefaultValue : Spec -> Rdf.Node compatible -> Maybe Value
toSelectDefaultValue spec node =
    case Rdf.toIri node of
        Nothing ->
            Nothing

        Just iri ->
            spec.data
                |> Decode.decode
                    (Decode.from iri
                        (Decode.map (\label -> { this = iri, label = label })
                            (Decode.predicate RDFS.label Decode.stringOrLangString)
                        )
                    )
                |> Result.toMaybe


fieldFileUpload : Spec -> Canonical.DataShapeProperty -> Result Error Field
fieldFileUpload spec data =
    case data.nodeKind of
        Nothing ->
            Err
                (MissingNodeKind
                    { propertyShapes = data.propertyShapes
                    , tagField = TagFieldFileUpload
                    , nodeKindExpected = NonEmpty.singleton Shacl.NodeKindIri
                    }
                )

        Just nodeKind ->
            if nodeKind /= Shacl.NodeKindIri then
                Err
                    (WrongNodeKind
                        { propertyShapes = data.propertyShapes
                        , tagField = TagFieldFileUpload
                        , nodeKindExpected = NonEmpty.singleton Shacl.NodeKindIri
                        , nodeKindGiven = nodeKind
                        }
                    )

            else
                getOptionalForLeaf TagFieldFileUpload data
                    |> Result.andThen
                        (\optional ->
                            let
                                counts =
                                    if optional then
                                        { zeros | numberOfNonInjectableOptionalAtomicFields = 1 }

                                    else
                                        { zeros | numberOfNonInjectableRequiredAtomicFields = 1 }
                            in
                            Ok DataFileUpload
                                |> Result.hardcoded data.hash
                                |> Result.hardcoded counts
                                |> Result.hardcoded data.path
                                |> Result.hardcoded data.group
                                |> Result.hardcoded data.order
                                |> Result.hardcoded (getNames spec data)
                                |> Result.hardcoded (getDescriptions spec data)
                                |> Result.hardcoded data.labelInCard
                                |> Result.hardcoded (optionalFromBool optional)
                                |> Result.required (getDefaultValue Rdf.toIri data.defaultValue)
                                |> Result.map FieldFileUpload
                        )


fieldIriReadOnly : Spec -> Canonical.DataShapeProperty -> Canonical.DataLeaf -> Result Error Field
fieldIriReadOnly spec data _ =
    let
        counts =
            { zeros | numberOfNonInjectableRequiredAtomicFields = 1 }
    in
    Ok DataIriReadOnly
        |> Result.hardcoded data.hash
        |> Result.hardcoded counts
        |> Result.hardcoded data.path
        |> Result.hardcoded data.group
        |> Result.hardcoded data.order
        |> Result.hardcoded (getNames spec data)
        |> Result.hardcoded (getDescriptions spec data)
        |> Result.hardcoded data.labelInCard
        |> Result.required (getClasses TagFieldIriReadOnly data)
        |> Result.required
            (getDefaultValue
                (\node ->
                    case Rdf.toIri node of
                        Nothing ->
                            Nothing

                        Just iri ->
                            Decode.decode
                                (Decode.from iri
                                    (Decode.succeed
                                        (\label ->
                                            { iri = iri
                                            , label = label
                                            }
                                        )
                                        |> Decode.optionalStringOrLangString RDFS.label
                                    )
                                )
                                spec.data
                                |> Result.toMaybe
                )
                data.defaultValue
            )
        |> Result.map FieldIriReadOnly


fieldGrid : Spec -> Canonical.DataShapeProperty -> Canonical.DataNested -> Result Error Alternative
fieldGrid spec data dataNested =
    let
        form =
            dataNested
                |> getGridEditorData spec data
                |> Result.andThen
                    (\{ cellEditor } ->
                        case cellEditor of
                            Nothing ->
                                fieldForm spec dataNested.node

                            Just formCellEditor ->
                                Ok formCellEditor
                    )
    in
    Ok DataGrid
        |> Result.hardcoded data.hash
        |> Result.hardcoded data.path
        |> Result.hardcoded data.group
        |> Result.hardcoded data.order
        |> Result.hardcoded (getNames spec data)
        |> Result.hardcoded (getDescriptions spec data)
        |> Result.hardcoded data.labelInCard
        |> Result.hardcoded data.minCount
        |> Result.hardcoded data.maxCount
        |> Result.required form
        |> Result.required (getClasses TagFieldGrid data)
        |> Result.required (getClassName TagFieldGrid spec data)
        |> Result.required (getGridEditorData spec data dataNested)
        |> Result.map FieldGrid
        |> Result.map (Alternative 15)


fieldVariants : Spec -> Canonical.ShapeNode -> List Canonical.ShapeNode -> Result Error Success
fieldVariants spec ((Canonical.ShapeNode data) as node) nodes =
    let
        group =
            case
                nodes
                    |> List.concatMap (\(Canonical.ShapeNode { properties }) -> List.map Canonical.group properties)
                    |> List.filterMap identity
                    |> SetIri.fromList
                    |> SetIri.toList
            of
                [] ->
                    Ok Nothing

                [ single ] ->
                    Ok (Just single)

                _ ->
                    Err IncompatibleGroups

        order =
            nodes
                |> List.concatMap (\(Canonical.ShapeNode { properties }) -> List.map Canonical.order properties)
                |> List.maximum
                |> Maybe.withDefault infinity
                |> Ok
    in
    Ok DataVariants
        |> Result.hardcoded data.hash
        |> Result.required group
        |> Result.required order
        |> Result.required (getVariants spec node nodes)
        |> Result.map FieldVariants
        |> Result.map
            (\field ->
                { field = field
                , alternatives = []
                , errors = []
                }
            )


getVariants : Spec -> Canonical.ShapeNode -> List Canonical.ShapeNode -> Result Error (NonEmpty Variant)
getVariants spec (Canonical.ShapeNode data) nodes =
    nodes
        |> List.map
            (\((Canonical.ShapeNode dataVariant) as nodeVariant) ->
                Ok Variant
                    |> Result.hardcoded dataVariant.hash
                    |> Result.hardcoded (getNamesNode spec nodeVariant)
                    |> Result.hardcoded (getDescriptionsNode spec nodeVariant)
                    |> Result.hardcoded dataVariant.targetClasses
                    |> Result.hardcoded dataVariant.classes
                    |> Result.required (fieldForm spec nodeVariant)
            )
        |> Result.combine
        |> Result.andThen
            (NonEmpty.fromList
                >> Result.fromMaybe
                    (MissingVariant
                        { nodeShapes = data.nodeShapes
                        }
                    )
            )


fieldSchedule : Spec -> Canonical.DataShapeProperty -> Canonical.DataNested -> Result Error Alternative
fieldSchedule spec data dataNested =
    let
        editorDayOffsetPath =
            case dataNested.editor of
                Just (EditorSchedule { hashScheduleEditorDayOffsetPath }) ->
                    Ok hashScheduleEditorDayOffsetPath

                _ ->
                    Err (MissingScheduleEditorDayOffsetPath data.propertyShapes)

        editorOrderPath =
            case dataNested.editor of
                Just (EditorSchedule { hashScheduleEditorOrderPath }) ->
                    Ok hashScheduleEditorOrderPath

                _ ->
                    Err (MissingScheduleEditorOrderPath data.propertyShapes)

        editorTitlePath =
            case dataNested.editor of
                Just (EditorSchedule { hashScheduleEditorTitlePath }) ->
                    Ok hashScheduleEditorTitlePath

                _ ->
                    Err (MissingScheduleEditorTitlePath data.propertyShapes)

        editorDatePath =
            case dataNested.editor of
                Just (EditorSchedule { hashScheduleEditorDatePath }) ->
                    Ok hashScheduleEditorDatePath

                _ ->
                    Err (MissingScheduleEditorDatePath data.propertyShapes)
    in
    Ok DataSchedule
        |> Result.hardcoded data.hash
        |> Result.hardcoded data.path
        |> Result.hardcoded data.group
        |> Result.hardcoded data.order
        |> Result.hardcoded (getNames spec data)
        |> Result.hardcoded (getDescriptions spec data)
        |> Result.hardcoded data.labelInCard
        |> Result.hardcoded data.minCount
        |> Result.hardcoded data.maxCount
        |> Result.required (fieldForm spec dataNested.node)
        |> Result.required (getClasses TagFieldSchedule data)
        |> Result.hardcoded (Maybe.withDefault Shacl.ValueCardViewer dataNested.viewer)
        |> Result.required editorDayOffsetPath
        |> Result.required editorOrderPath
        |> Result.required editorTitlePath
        |> Result.required editorDatePath
        |> Result.map FieldSchedule
        -- takes precedence over list
        |> Result.map (Alternative 15)


fieldCollectionSelect : Spec -> Canonical.DataShapeProperty -> Canonical.DataNested -> Result Error Alternative
fieldCollectionSelect spec data dataNested =
    Ok
        (\optional form ->
            let
                counts =
                    if optional then
                        { zeros | numberOfNonInjectableOptionalAtomicFields = 1 }

                    else
                        { zeros | numberOfNonInjectableRequiredAtomicFields = 1 }
            in
            case singleSelectOne form of
                Err error ->
                    Err error

                Ok Nothing ->
                    Err NoSelectOneFound

                Ok (Just selectOne) ->
                    Ok DataCollectionSelect
                        |> Result.hardcoded data.hash
                        |> Result.hardcoded counts
                        |> Result.hardcoded data.path
                        |> Result.hardcoded data.group
                        |> Result.hardcoded data.order
                        |> Result.hardcoded (getNames spec data)
                        |> Result.hardcoded (getDescriptions spec data)
                        |> Result.hardcoded data.minCount
                        |> Result.hardcoded data.maxCount
                        |> Result.hardcoded form
                        |> Result.required (getClasses TagFieldCollection data)
                        |> Result.hardcoded selectOne
                        |> Result.map FieldCollectionSelect
                        |> Result.map (Alternative 15)
        )
        |> Result.required (getOptionalForLeaf TagFieldCollectionSelect data)
        |> Result.required (fieldForm spec dataNested.node)
        |> Result.join


singleSelectOne : Field -> Result Error (Maybe DataSelectOne)
singleSelectOne field =
    case field of
        FieldCheckbox _ ->
            Err
                (FieldNotAllowedWithinCollectionSelect
                    { tagField = TagFieldCheckbox
                    }
                )

        FieldCheckboxInstance _ ->
            Err
                (FieldNotAllowedWithinCollectionSelect
                    { tagField = TagFieldCheckboxInstance
                    }
                )

        FieldCollection _ ->
            Err
                (FieldNotAllowedWithinCollectionSelect
                    { tagField = TagFieldCollection
                    }
                )

        FieldCollectionSelect _ ->
            Err
                (FieldNotAllowedWithinCollectionSelect
                    { tagField = TagFieldCollectionSelect
                    }
                )

        FieldConstant _ ->
            Ok Nothing

        FieldConstantInjectable _ ->
            Err
                (FieldNotAllowedWithinCollectionSelect
                    { tagField = TagFieldConstantInjectable
                    }
                )

        FieldDateTime _ ->
            Err
                (FieldNotAllowedWithinCollectionSelect
                    { tagField = TagFieldDateTime
                    }
                )

        FieldFileUpload _ ->
            Err
                (FieldNotAllowedWithinCollectionSelect
                    { tagField = TagFieldFileUpload
                    }
                )

        FieldForm data ->
            data.successes
                |> List.map (.field >> singleSelectOne)
                |> Result.combine
                |> Result.map (List.filterMap identity)
                |> Result.andThen
                    (\selectOnes ->
                        case selectOnes of
                            [ selectOne ] ->
                                Ok (Just selectOne)

                            _ ->
                                Err TooManySelectOnes
                    )

        FieldGrid _ ->
            Err
                (FieldNotAllowedWithinCollectionSelect
                    { tagField = TagFieldGrid
                    }
                )

        FieldIriReadOnly _ ->
            Err
                (FieldNotAllowedWithinCollectionSelect
                    { tagField = TagFieldIriReadOnly
                    }
                )

        FieldNumber _ ->
            Err
                (FieldNotAllowedWithinCollectionSelect
                    { tagField = TagFieldNumber
                    }
                )

        FieldSchedule _ ->
            Err
                (FieldNotAllowedWithinCollectionSelect
                    { tagField = TagFieldSchedule
                    }
                )

        FieldSelectMany _ ->
            Err
                (FieldNotAllowedWithinCollectionSelect
                    { tagField = TagFieldSelectMany
                    }
                )

        FieldSelectOne data ->
            case data.optional of
                OptionalByItself ->
                    Err SelectOneMustBeRequiredWithinCollectionSelect

                OptionalViaNesting ->
                    Err SelectOneMustBeRequiredWithinCollectionSelect

                Required ->
                    Ok (Just data)

        FieldSelectOneInjectable _ ->
            Err
                (FieldNotAllowedWithinCollectionSelect
                    { tagField = TagFieldSelectOne
                    }
                )

        FieldSingleNested data ->
            case data.optional of
                OptionalViaCheckbox ->
                    Err OptionalSingleNestedNotAllowedWithinCollectionSelect

                OptionalViaNested ->
                    Err OptionalSingleNestedNotAllowedWithinCollectionSelect

                RequiredNested ->
                    singleSelectOne data.field

        FieldText _ ->
            Err
                (FieldNotAllowedWithinCollectionSelect
                    { tagField = TagFieldText
                    }
                )

        FieldVariants _ ->
            Err
                (FieldNotAllowedWithinCollectionSelect
                    { tagField = TagFieldVariants
                    }
                )


fieldCollection : Spec -> Canonical.DataShapeProperty -> Canonical.DataNested -> Result Error Alternative
fieldCollection spec data dataNested =
    Ok DataCollection
        |> Result.hardcoded data.hash
        |> Result.hardcoded data.path
        |> Result.hardcoded data.group
        |> Result.hardcoded data.order
        |> Result.hardcoded (getNames spec data)
        |> Result.hardcoded (getDescriptions spec data)
        |> Result.hardcoded data.labelInCard
        |> Result.hardcoded data.minCount
        |> Result.hardcoded data.maxCount
        |> Result.required (fieldForm spec dataNested.node)
        |> Result.required (getClasses TagFieldCollection data)
        |> Result.hardcoded dataNested.editorOrderPath
        |> Result.hardcoded (Maybe.withDefault Shacl.ValueCardViewer dataNested.viewer)
        |> Result.map FieldCollection
        |> Result.map (Alternative 10)


{-| Single nested fields flatten a property shape with `0 <= minCount <= 1 ==
maxCount` into the form of the contained property shapes.

Visually, such a single nested property consists only of the contained form.
Implementationally, the single nested field takes care of messaging the
document serialization/ deserialization and error reporting in such a way that
the nesting is transparent to the contained form.

Note that single nested fields nest forms, not just single fields. In case of
optional forms, the form gets wrapped in an "optional via checkbox" field,
iff

  - it contains more than one form field, or
  - it specifies `dash:optionalViaCheckbox true`.

Note also that single nested fields do not require their forms to succeed on
all property shapes.

-}
fieldSingleNested :
    Spec
    -> Canonical.DataShapeProperty
    -> Canonical.DataNested
    -> Result Error Field
fieldSingleNested spec data dataNested =
    if data.maxCount > 1 then
        Err
            (MaxCountTooHigh
                { propertyShapes = data.propertyShapes
                , tagField = TagFieldSingleNested
                , maxCountMaximum = 1
                , maxCountGiven = data.maxCount
                }
            )

    else if data.minCount < 0 then
        Err
            (MinCountTooLow
                { propertyShapes = data.propertyShapes
                , tagField = TagFieldSingleNested
                , minCountMinimum = 0
                , minCountGiven = data.minCount
                }
            )

    else if data.minCount > data.maxCount then
        Err
            (MinCountAboveMaxCount
                { propertyShapes = data.propertyShapes
                , tagField = TagFieldSingleNested
                , minCountGiven = data.minCount
                , maxCountGiven = data.maxCount
                }
            )

    else
        case data.propertyShapes of
            [ propertyShape ] ->
                Ok DataSingleNested
                    |> Result.hardcoded data.hash
                    |> Result.hardcoded zeros
                    |> Result.hardcoded data.path
                    |> Result.hardcoded data.group
                    |> Result.hardcoded data.order
                    |> Result.required (fieldForm spec dataNested.node)
                    |> Result.hardcoded data.classes
                    |> Result.hardcoded (getNames spec data)
                    |> Result.hardcoded data.qualified
                    |> Result.required
                        (Result.map
                            (\optional ->
                                if optional then
                                    OptionalViaCheckbox

                                else
                                    RequiredNested
                            )
                            (getOptionalForLeaf TagFieldSingleNested data)
                        )
                    |> Result.hardcoded propertyShape.node
                    |> Result.hardcoded (propertyShape.shNodeKind == Just Shacl.NodeKindIri)
                    |> Result.map FieldSingleNested

            _ ->
                -- FIXME Make multiple property shapes possible
                Err NotImplemented


getOptionalForLeaf : TagField -> Canonical.DataShapeProperty -> Result Error Bool
getOptionalForLeaf tagField data =
    if data.minCount > 1 then
        Err
            (MinCountTooHigh
                { propertyShapes = data.propertyShapes
                , tagField = tagField
                , minCountMaximum = 1
                , minCountGiven = data.minCount
                }
            )

    else if data.minCount < 0 then
        Err
            (MinCountTooLow
                { propertyShapes = data.propertyShapes
                , tagField = tagField
                , minCountMinimum = 0
                , minCountGiven = data.minCount
                }
            )

    else if data.maxCount < 1 then
        Err
            (MaxCountTooLow
                { propertyShapes = data.propertyShapes
                , tagField = tagField
                , maxCountMinimum = 1
                , maxCountGiven = data.maxCount
                }
            )

    else if data.minCount > data.maxCount then
        Err
            (MinCountAboveMaxCount
                { propertyShapes = data.propertyShapes
                , tagField = tagField
                , minCountGiven = data.minCount
                , maxCountGiven = data.maxCount
                }
            )

    else
        Ok (data.minCount == 0)


getNames : Spec -> Canonical.DataShapeProperty -> List StringOrLangString
getNames { ontology } data =
    [ data.names
    , List.filterMap (Maybe.andThen .label << Ontology.getClass ontology) data.classes
    , data.path
        |> lastPredicatePath
        |> Maybe.andThen (Ontology.getProperty ontology)
        |> Maybe.andThen .label
        |> Maybe.unwrap [] List.singleton
    ]
        |> List.concat


getNamesNode : Spec -> Canonical.ShapeNode -> List StringOrLangString
getNamesNode { ontology } (Canonical.ShapeNode dataShapeNode) =
    [ dataShapeNode.names
    , dataShapeNode.labels
    , (dataShapeNode.classes ++ dataShapeNode.targetClasses)
        |> List.filterMap (Ontology.getClass ontology >> Maybe.andThen .label)
    ]
        |> List.concat


getDescriptions : Spec -> Canonical.DataShapeProperty -> List StringOrLangString
getDescriptions { ontology } data =
    [ data.descriptions
    , List.filterMap (Maybe.andThen .comment << Ontology.getClass ontology) data.classes
    , data.path
        |> lastPredicatePath
        |> Maybe.andThen (Ontology.getProperty ontology)
        |> Maybe.andThen .comment
        |> Maybe.unwrap [] List.singleton
    ]
        |> List.concat


getDescriptionsNode : Spec -> Canonical.ShapeNode -> List StringOrLangString
getDescriptionsNode { ontology } (Canonical.ShapeNode data) =
    [ data.comments
    , (data.classes ++ data.targetClasses)
        |> List.filterMap (Ontology.getClass ontology >> Maybe.andThen .comment)
    ]
        |> List.concat


getClasses : TagField -> Canonical.DataShapeProperty -> Result Error (NonEmpty Iri)
getClasses tagField data =
    data.classes
        |> NonEmpty.fromList
        |> Result.fromMaybe
            (MissingClass
                { propertyShapes = data.propertyShapes
                , tagField = tagField
                }
            )


getClassName : TagField -> Spec -> Canonical.DataShapeProperty -> Result Error StringOrLangString
getClassName tagField { ontology } data =
    getClasses tagField data
        |> Result.map NonEmpty.head
        |> Result.andThen
            (\class ->
                class
                    |> Ontology.getClass ontology
                    |> Maybe.andThen .label
                    |> Result.fromMaybe
                        (MissingLabelForClass
                            { propertyShapes = data.propertyShapes
                            , tagField = tagField
                            , class = class
                            }
                        )
            )


getGridEditorData : Spec -> Canonical.DataShapeProperty -> Canonical.DataNested -> Result Error DataEditorGrid
getGridEditorData spec data dataNested =
    case dataNested.editor of
        Nothing ->
            Err
                (MissingEditor
                    { propertyShapes = data.propertyShapes
                    , tagField = TagFieldGrid
                    }
                )

        Just (Canonical.EditorGrid dataEditorGrid) ->
            Ok DataEditorGrid
                |> Result.hardcoded dataEditorGrid.availableColumnsPath
                |> Result.hardcoded dataEditorGrid.availableRowsPath
                |> Result.hardcoded dataEditorGrid.columnPath
                |> Result.hardcoded dataEditorGrid.rowPath
                |> Result.required
                    (case dataEditorGrid.cellViewer of
                        Nothing ->
                            Ok Nothing

                        Just cellViewer ->
                            cellViewer
                                |> fieldForm spec
                                |> Result.map Just
                    )
                |> Result.required
                    (case dataEditorGrid.cellEditor of
                        Nothing ->
                            Ok Nothing

                        Just cellEditor ->
                            cellEditor
                                |> fieldForm spec
                                |> Result.map Just
                    )

        Just editor ->
            Err
                (WrongEditor
                    { propertyShapes = data.propertyShapes
                    , tagField = TagFieldGrid
                    , noEditorAllowed = False
                    , editorExpected =
                        NonEmpty.singleton
                            (Canonical.EditorGrid
                                { availableColumnsPath = Rdf.PredicatePath (Rdf.iri "http://example.org/")
                                , availableRowsPath = Rdf.PredicatePath (Rdf.iri "http://example.org/")
                                , columnPath = Rdf.iri "http://example.org/"
                                , rowPath = Rdf.iri "http://example.org/"
                                , cellViewer = Nothing
                                , cellEditor = Nothing
                                }
                            )
                    , editorGiven = editor
                    }
                )


getDefaultValue : (Rdf.Node compatible -> Maybe a) -> List (Rdf.Node compatible) -> Result Error (Maybe a)
getDefaultValue toValue nodes =
    case nodes of
        [] ->
            Ok Nothing

        [ node ] ->
            case toValue node of
                Nothing ->
                    Err (IncompatibleDefaultValue (Rdf.asBlankNodeOrIriOrAnyLiteral node))

                Just value ->
                    Ok (Just value)

        _ ->
            Err (TooManyDefaultValues (List.map Rdf.asBlankNodeOrIriOrAnyLiteral nodes))


getDefaultValues : (Rdf.Node compatible -> Maybe a) -> List (Rdf.Node compatible) -> Result Error (List a)
getDefaultValues toValue nodes =
    case nodes of
        [] ->
            Ok []

        _ ->
            List.foldl
                (\node result ->
                    case toValue node of
                        Nothing ->
                            Err (IncompatibleDefaultValue (Rdf.asBlankNodeOrIriOrAnyLiteral node))

                        Just value ->
                            Result.map (\values -> value :: values) result
                )
                (Ok [])
                nodes


lastPredicatePath : PropertyPath -> Maybe Iri
lastPredicatePath path =
    case path of
        PredicatePath iri ->
            Just iri

        SequencePath first rest ->
            case rest of
                [] ->
                    lastPredicatePath first

                _ ->
                    rest
                        |> List.reverse
                        |> List.head
                        |> Maybe.andThen lastPredicatePath

        _ ->
            Nothing


infinity : Float
infinity =
    1 / 0
