module Shacl.Form.Collected exposing
    ( ShapeNode(..), DataShapeNode
    , ShapeProperty(..), DataShapeProperty
    , fromShacl, Spec, Error(..), errorToString
    , Editor(..), DataEditorGrid, DataEditorSchedule
    )

{-|

@docs ShapeNode, DataShapeNode
@docs ShapeProperty, DataShapeProperty

@docs fromShacl, Spec, Error, errorToString

@docs Editor, DataEditorGrid, DataEditorSchedule

-}

import Ontology exposing (Ontology)
import Rdf exposing (BlankNodeOrIri, Iri)
import Rdf.Graph exposing (Graph)
import Rdf.PropertyPath exposing (PropertyPath)
import Result.Extra as Result
import Result.Pipeline as Result
import Shacl


type ShapeNode
    = ShapeNode DataShapeNode


type alias DataShapeNode =
    { nodeShape : Shacl.NodeShape
    , ors : List (List ShapeNode)
    , nodes : List ShapeNode
    , shapeProperties : List ShapeProperty
    }


type ShapeProperty
    = ShapeProperty DataShapeProperty


type alias DataShapeProperty =
    { propertyShape : Shacl.PropertyShape
    , nodes : List ShapeNode
    , qualifiedNode : Maybe ShapeNode
    , editor : Maybe Editor -- FIXME This should be done at a later stage
    }


type Editor
    = EditorDetails
    | EditorInstanceSelect
    | EditorRadioButtons
    | EditorGrid DataEditorGrid
    | EditorSchedule DataEditorSchedule


type alias DataEditorGrid =
    { availableColumnsPath : PropertyPath
    , availableRowsPath : PropertyPath
    , columnPath : Iri
    , rowPath : Iri
    , cellViewer : Maybe ShapeNode
    , cellEditor : Maybe ShapeNode
    }


type alias DataEditorSchedule =
    { hashScheduleEditorDayOffsetPath : PropertyPath
    , hashScheduleEditorOrderPath : PropertyPath
    , hashScheduleEditorSamples : List Iri
    , hashScheduleEditorTitlePath : PropertyPath
    , hashScheduleEditorDatePath : PropertyPath
    }


type Error
    = MissingPropertyShape
        { node : BlankNodeOrIri
        }
    | MissingNodeShape
        { node : BlankNodeOrIri
        }


errorToString : Error -> String
errorToString error =
    case error of
        MissingPropertyShape { node } ->
            "I could not find the property shape " ++ Rdf.serializeNode node ++ " during the collecting stage."

        MissingNodeShape { node } ->
            "I could not find the node shape " ++ Rdf.serializeNode node ++ " during the collecting stage."


type alias Spec =
    { data : Graph
    , shapes : Shacl.Shapes
    , ontology : Ontology
    , strict : Bool
    }


fromShacl : Spec -> Shacl.NodeShape -> Result Error ShapeNode
fromShacl spec nodeShape =
    -- TODO Check that nodeShape exists in shapes
    Ok DataShapeNode
        |> Result.hardcoded nodeShape
        |> Result.required (ors spec nodeShape)
        |> Result.required (nodesForNodeShape spec nodeShape)
        |> Result.required (shapeProperties spec nodeShape)
        |> Result.map ShapeNode


ors : Spec -> Shacl.NodeShape -> Result Error (List (List ShapeNode))
ors spec nodeShape =
    nodeShape.shOr
        |> List.map (List.map (getShapeNode spec) >> Result.combine)
        |> Result.combine
        |> Result.andThen (List.map (List.map (fromShacl spec) >> Result.combine) >> Result.combine)


nodesForNodeShape : Spec -> Shacl.NodeShape -> Result Error (List ShapeNode)
nodesForNodeShape spec nodeShape =
    nodeShape.shNode
        |> List.map (getShapeNode spec)
        |> Result.combine
        |> Result.andThen (List.map (fromShacl spec) >> Result.combine)


shapeProperties : Spec -> Shacl.NodeShape -> Result Error (List ShapeProperty)
shapeProperties spec nodeShape =
    nodeShape.shProperty
        |> List.map (getShapeProperty spec)
        |> Result.combine
        |> Result.andThen (List.map (fromShapeProperty spec) >> Result.combine)


fromShapeProperty : Spec -> Shacl.PropertyShape -> Result Error ShapeProperty
fromShapeProperty spec propertyShape =
    Ok DataShapeProperty
        |> Result.hardcoded propertyShape
        |> Result.required (nodesForPropertyShape spec propertyShape)
        |> Result.required (qualifiedNode spec propertyShape)
        |> Result.required (editor spec propertyShape)
        |> Result.map ShapeProperty


nodesForPropertyShape : Spec -> Shacl.PropertyShape -> Result Error (List ShapeNode)
nodesForPropertyShape spec propertyShape =
    propertyShape.shNode
        |> List.map (getShapeNode spec >> Result.andThen (fromShacl spec))
        |> Result.combine


qualifiedNode : Spec -> Shacl.PropertyShape -> Result Error (Maybe ShapeNode)
qualifiedNode spec propertyShape =
    case propertyShape.shQualifiedValueShape of
        Nothing ->
            Ok Nothing

        Just shQualifiedValueShape ->
            shQualifiedValueShape
                |> getShapeNode spec
                |> Result.andThen (fromShacl spec)
                |> Result.map Just


editor : Spec -> Shacl.PropertyShape -> Result Error (Maybe Editor)
editor spec propertyShape =
    case propertyShape.dashEditor of
        Nothing ->
            Ok Nothing

        Just Shacl.DetailsEditor ->
            Ok (Just EditorDetails)

        Just Shacl.InstancesSelectEditor ->
            Ok (Just EditorInstanceSelect)

        Just Shacl.RadioButtonsEditor ->
            Ok (Just EditorRadioButtons)

        Just (Shacl.GridEditor data) ->
            Ok DataEditorGrid
                |> Result.hardcoded data.availableColumnsPath
                |> Result.hardcoded data.availableRowsPath
                |> Result.hardcoded data.columnPath
                |> Result.hardcoded data.rowPath
                |> Result.required (cellViewer spec data)
                |> Result.required (cellEditor spec data)
                |> Result.map (EditorGrid >> Just)

        Just (Shacl.ScheduleEditor data) ->
            Ok DataEditorSchedule
                |> Result.hardcoded data.hashScheduleEditorDayOffsetPath
                |> Result.hardcoded data.hashScheduleEditorOrderPath
                |> Result.hardcoded data.hashScheduleEditorSamples
                |> Result.hardcoded data.hashScheduleEditorTitlePath
                |> Result.hardcoded data.hashScheduleEditorDatePath
                |> Result.map (EditorSchedule >> Just)


cellViewer : Spec -> Shacl.GridEditorData -> Result Error (Maybe ShapeNode)
cellViewer spec data =
    data.cellViewer
        |> Maybe.map
            (getShapeNode spec
                >> Result.andThen (fromShacl spec)
                >> Result.map Just
            )
        |> Maybe.withDefault (Ok Nothing)


cellEditor : Spec -> Shacl.GridEditorData -> Result Error (Maybe ShapeNode)
cellEditor spec data =
    data.cellEditor
        |> Maybe.map
            (getShapeNode spec
                >> Result.andThen (fromShacl spec)
                >> Result.map Just
            )
        |> Maybe.withDefault (Ok Nothing)


getShapeNode : Spec -> BlankNodeOrIri -> Result Error Shacl.NodeShape
getShapeNode { shapes } node =
    node
        |> Shacl.getNodeShape shapes
        |> Result.fromMaybe
            (MissingNodeShape
                { node = node
                }
            )


getShapeProperty : Spec -> BlankNodeOrIri -> Result Error Shacl.PropertyShape
getShapeProperty { shapes } node =
    node
        |> Shacl.getPropertyShape shapes
        |> Result.fromMaybe
            (MissingPropertyShape
                { node = node
                }
            )
