module Shacl.Form.Viewed.DateTime exposing
    ( Model
    , init, initWith
    , Msg, update, expandAll
    , subscriptions
    , view, viewFullscreen, card, columns, help, summary
    , encoders
    , toNodeFocus, toPath, isEmpty, dragging, log
    )

{-|


# Model

@docs Model


# Init

@docs init, initWith


# Update

@docs Msg, update, expandAll


# Subscriptions

@docs subscriptions


# View

@docs view, viewFullscreen, card, columns, help, summary


# Output

@docs encoders


# Info

@docs toNodeFocus, toPath, isEmpty, dragging, log

-}

import Element as Ui exposing (Element)
import Element.Lazy as Ui
import Fluent exposing (Fluent, verbatim)
import Iso8601
import Maybe.Apply as Maybe
import Maybe.Extra as Maybe
import Pretty
import Rdf exposing (BlankNodeOrIri)
import Rdf.Decode as Decode exposing (Decoder)
import Rdf.Encode as Encode
import Rdf.Graph exposing (Seed)
import Rdf.Namespaces exposing (xsd)
import Shacl.Clipboard as Clipboard
import Shacl.Form.Breadcrumbs as Breadcrumbs exposing (Breadcrumbs, Path)
import Shacl.Form.Context exposing (C)
import Shacl.Form.Decoded as Decoded
import Shacl.Form.Effect as Effect
import Shacl.Form.Localize as Localize
import Shacl.Form.Shared as Shared exposing (Config, Shared, withInfo)
import Shacl.Form.Translated as Translated
import Shacl.Form.Ui as Ui
import Shacl.Form.Ui.Container as Container exposing (Container)
import Shacl.Form.Updated as Updated exposing (Updated)
import Shacl.Report as Report exposing (ConstraintComponent(..), Report, ValidationResult(..))
import String.Extra as String
import Time exposing (Posix)
import Ui.Atom.ErrorWrapper exposing (errorWrapper)
import Ui.Atom.Labelled as Labelled
import Ui.Molecule.DatePicker exposing (datePicker)
import Ui.Molecule.Table as Table
import Ui.Molecule.TableCell as TableCell exposing (TableCell)



-- MODEL


{-| -}
type Model
    = Model Data


type alias Data =
    { path : Path
    , breadcrumbs : Breadcrumbs
    , translated : Translated.DataDateTime
    , nodeFocus : BlankNodeOrIri
    , value : Maybe Posix
    }



-- INIT


{-| -}
init :
    Config e f m Model Msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Breadcrumbs
    -> Decoded.DataDateTime
    -> Updated Model m
init =
    Shared.init
        { value = \_ _ decoded -> decoded.posix
        , default = \_ _ { translated } -> translated.defaultValue
        , withValue =
            \_ _ _ seed breadcrumbs { path, translated, nodeFocus } value ->
                { model =
                    Model
                        { path = path
                        , breadcrumbs = breadcrumbs
                        , translated = translated
                        , nodeFocus = nodeFocus
                        , value = Just value
                        }
                , effect = Effect.none
                , seed = seed
                , needsPersisting = False
                }
        , path = .translated >> .path
        , empty =
            \_ _ _ seed breadcrumbs { path, translated, nodeFocus } ->
                { model =
                    Model
                        { path = path
                        , breadcrumbs = breadcrumbs
                        , translated = translated
                        , nodeFocus = nodeFocus
                        , value = Nothing
                        }
                , effect = Effect.none
                , seed = seed
                , needsPersisting = False
                }
        }


{-| -}
initWith :
    Config e f m Model Msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Model
    -> Result e (Maybe (Updated Model m))
initWith =
    Shared.initWith
        { decoder = \c shared (Model data) -> decoder c shared data.translated
        , default = \_ _ (Model data) -> data.translated.defaultValue
        , nodeFocus = \(Model data) -> data.nodeFocus
        , path = \(Model data) -> data.translated.path
        , updateValue = updateValue
        , removeValue = removeValue
        , tagField = Translated.TagFieldDateTime
        }


updateValue :
    Config e f m Model Msg
    -> C
    -> Shared
    -> Seed
    -> Posix
    -> Model
    -> Maybe (Updated Model m)
updateValue _ _ _ seed value (Model data) =
    if Just value == data.value then
        Nothing

    else
        { data | value = Just value }
            |> Model
            |> Updated.from seed
            |> Just


removeValue :
    Config e f m Model Msg
    -> C
    -> Shared
    -> Seed
    -> Model
    -> Maybe (Updated Model m)
removeValue _ _ _ seed (Model data) =
    if data.value == Nothing then
        Nothing

    else
        { data | value = Nothing }
            |> Model
            |> Updated.from seed
            |> Just



-- UPDATE


{-| -}
type Msg
    = UserChangedDateTime (Maybe Posix)
    | UserCopied
    | UserPasted (Maybe Posix)
    | Ignored


{-| -}
update :
    Config e f m Model Msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Msg
    -> Model
    -> Updated Model m
update config _ c _ seed msg ((Model data) as model) =
    case msg of
        UserChangedDateTime value ->
            { data | value = value }
                |> Model
                |> Updated.from seed
                |> Updated.needsPersisting

        UserCopied ->
            model
                |> Updated.from seed
                |> Updated.withEffect
                    (encoders config c model
                        |> Clipboard.serialize data.nodeFocus seed
                        |> Effect.copyToClipboard
                    )

        UserPasted valuePasted ->
            { data | value = valuePasted }
                |> Model
                |> Updated.from seed
                |> Updated.needsPersisting

        Ignored ->
            Updated.from seed model


{-| -}
expandAll : Config e f m Model Msg -> Model -> Model
expandAll =
    \_ -> identity



-- SUBSCRIPTIONS


{-| -}
subscriptions : Config e f m Model Msg -> Model -> Sub m
subscriptions =
    \_ _ -> Sub.none



-- VIEW


{-| -}
view :
    Config e f m Model Msg
    -> C
    -> Report
    -> Bool
    -> Model
    -> Maybe (Container m)
view config c report _ model =
    viewHelp config c report model
        |> Container.wrappable
        |> Just


viewHelp :
    Config e f m Model Msg
    -> C
    -> Report
    -> Model
    -> Element m
viewHelp =
    Ui.lazy4
        (\config c report ((Model data) as model) ->
            { id = Breadcrumbs.pointer data.path
            , label = Ui.label c data.translated
            , help = []
            , initialDate = data.value
            , locale = c.locale
            , onChange = UserChangedDateTime
            , onCopy = Just UserCopied
            , onPaste =
                Just
                    ( "text/plain"
                    , Ignored
                    , Clipboard.allObjects UserPasted (Decode.map Just (decoderValue data.translated))
                    )
            , withTime = data.translated.withTime
            , supportingText = Ui.description c data.translated
            }
                |> datePicker
                |> errorWrapper (help config c report model)
                |> Ui.map config.toMsg
        )


{-| -}
viewFullscreen :
    Config e f m Model Msg
    -> C
    -> Report
    -> Bool
    -> Model
    -> Maybe (Element m)
viewFullscreen _ _ _ _ _ =
    Nothing


{-| -}
card : Config e f m Model Msg -> C -> Model -> Maybe (Container msg)
card _ c (Model data) =
    data.value
        |> Maybe.map
            (if data.translated.withTime then
                Labelled.posix (Ui.label c data.translated)

             else
                Labelled.date (Ui.label c data.translated)
            )
        |> Maybe.map Container.wrappable


{-| -}
columns :
    Config e f m Model Msg
    -> C
    -> List String
    -> Translated.DataDateTime
    -> List (Table.Column f m)
columns config c scope translated =
    [ { header = viewHeader c scope translated
      , width = Ui.shrink
      , view = viewCell config
      }
    ]


viewHeader : C -> List String -> Translated.DataDateTime -> TableCell m
viewHeader c scope translated =
    translated
        |> Ui.label c
        |> TableCell.textBold
        |> withInfo scope


viewCell : Config e f m Model Msg -> f -> TableCell m
viewCell config form =
    case config.toField form of
        Nothing ->
            TableCell.empty

        Just (Model data) ->
            data.value
                |> Maybe.andThen
                    (\value ->
                        if data.translated.withTime then
                            value
                                |> TableCell.dateTime
                                |> Just

                        else
                            value
                                |> TableCell.date
                                |> Just
                    )
                |> Maybe.withDefault TableCell.empty


{-| -}
help : Config e f m Model Msg -> C -> Report -> Model -> List Fluent
help _ c report (Model data) =
    report.results
        |> Report.validationResultsFor
            (Report.emptySelector
                |> Maybe.apply Report.selectFocusNode (Rdf.toIri data.nodeFocus)
                |> Report.selectPath data.translated.path
            )
        |> List.filterMap (helpFromValidationResult c data)


helpFromValidationResult : C -> Data -> ValidationResult -> Maybe Fluent
helpFromValidationResult c data (ValidationResult { constraintComponent, message }) =
    let
        name =
            data.translated.names
                |> List.head
                |> Maybe.map (Localize.withinSentenceWithArticleIndefinite c)
                |> Maybe.withDefault "a value"
    in
    case constraintComponent of
        MinCountConstraintComponent ->
            Just (verbatim ("Select " ++ name))

        _ ->
            Just (verbatim (Localize.verbatim c message ++ "."))


{-| -}
summary : Config e f m Model Msg -> C -> Model -> List Fluent
summary _ _ (Model data) =
    case data.value of
        Nothing ->
            []

        Just value ->
            if data.translated.withTime then
                [ tr [ Fluent.posix "posix" value ] "posix" ]

            else
                [ tr [ Fluent.posix "date" value ] "date" ]


tr : List Fluent.Argument -> String -> Fluent
tr args key =
    Fluent.textWith args ("shacl-form-viewed-date-time--" ++ key)



-- OUTPUT


{-| -}
encoders : Config e f m Model Msg -> C -> Model -> List Encode.PropertyEncoder
encoders _ _ (Model data) =
    case data.value of
        Nothing ->
            []

        Just value ->
            if data.translated.withTime then
                [ Encode.property data.translated.path
                    (Encode.literal
                        (Rdf.literal (xsd "dateTime")
                            (Iso8601.fromTime value)
                        )
                    )
                ]

            else
                [ Encode.property data.translated.path
                    (Encode.literal
                        (Rdf.literal (xsd "date")
                            (String.leftOf "T" (Iso8601.fromTime value))
                        )
                    )
                ]



-- INFO


{-| -}
toNodeFocus : Model -> BlankNodeOrIri
toNodeFocus (Model data) =
    data.nodeFocus


{-| -}
toPath : Model -> Path
toPath (Model data) =
    data.path


{-| -}
isEmpty : Config e f m Model Msg -> Model -> Bool
isEmpty _ (Model data) =
    Maybe.isNothing data.value


{-| -}
dragging : Config e f m Model Msg -> Model -> Bool
dragging =
    \_ _ -> False


{-| -}
log : Config e f m Model Msg -> Model -> Pretty.Doc t
log _ (Model _) =
    Pretty.string "Date Time"



-- DECODE


decoder : C -> Shared -> Translated.DataDateTime -> Decoder Time.Posix
decoder _ _ translated =
    Decode.property translated.path (decoderValue translated)


decoderValue : Translated.DataDateTime -> Decoder Time.Posix
decoderValue translated =
    if translated.withTime then
        Decode.dateTime

    else
        Decode.date
