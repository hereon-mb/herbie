module Shacl.Form.Viewed.SingleNested exposing
    ( Model
    , init, initWith
    , Msg, update, expandAll
    , subscriptions
    , view, viewFullscreen, card, columns, help, summary
    , encoders
    , toNodeFocus, toPath, isEmpty, dragging, log
    )

{-|


# Model

@docs Model


# Init

@docs init, initWith


# Update

@docs Msg, update, expandAll


# Subscriptions

@docs subscriptions


# View

@docs view, viewFullscreen, card, columns, help, summary


# Output

@docs encoders


# Info

@docs toNodeFocus, toPath, isEmpty, dragging, log

-}

import Api
import Element exposing (Element, el, fill, none, paddingEach, row, width)
import Element.Extra as Element
import Fluent exposing (Fluent, verbatim)
import Html
import Html.Attributes
import Maybe.Apply as Maybe
import Maybe.Extra as Maybe
import Pretty
import Rdf exposing (BlankNodeOrIri, Iri)
import Rdf.Encode as Encode exposing (PropertyEncoder)
import Rdf.Graph exposing (Seed)
import Rdf.Namespaces exposing (a)
import Rdf.Namespaces.RDFS as RDFS
import Shacl.Form.Breadcrumbs as Breadcrumbs exposing (Breadcrumbs, Path)
import Shacl.Form.Context exposing (C)
import Shacl.Form.Decoded as Decoded
import Shacl.Form.Effect as Effect
import Shacl.Form.Grouped as Grouped
import Shacl.Form.Localize as Localize
import Shacl.Form.Shared exposing (Config, Shared)
import Shacl.Form.Translated as Translated
import Shacl.Form.Ui as Ui
import Shacl.Form.Ui.Container as Container exposing (Container)
import Shacl.Form.Updated as Updated exposing (Updated)
import Shacl.Report as Report exposing (Report, ValidationResult(..))
import Ui.Atom.Checkbox as Checkbox
import Ui.Atom.ErrorWrapper exposing (errorWrapper)
import Ui.Molecule.Table as Table
import Ui.Molecule.TableCell as TableCell
import Ui.Theme.Layout as Layout



-- MODEL


{-| -}
type Model f
    = Model (Data f)


type alias Data f =
    { path : Path
    , breadcrumbs : Breadcrumbs
    , grouped : Grouped.DataSingleNested
    , nodeFocus : BlankNodeOrIri
    , iriDocument : Iri
    , form : Maybe f
    , checked : Bool
    }



-- INIT


{-| -}
init :
    Config e f m (Model f) (Msg m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Breadcrumbs
    -> Decoded.DataSingleNested
    -> Updated (Model f) m
init config populateDefaultValues c shared seed breadcrumbs decoded =
    case decoded.field of
        Nothing ->
            { model = toModel config shared decoded.path breadcrumbs decoded.nodeFocus decoded.grouped Nothing
            , effect =
                decoded.grouped.field
                    |> Effect.CreateField ApiReturnedDecoded
                        shared.preview
                        decoded.path
                        (if decoded.grouped.mintIri then
                            case List.head decoded.grouped.classes of
                                Nothing ->
                                    Decoded.MintBlankNode

                                Just class ->
                                    -- FIXME Mint from all classes
                                    Decoded.MintIri
                                        { nodeFocus = decoded.nodeFocus
                                        , class = class
                                        }

                         else
                            Decoded.MintBlankNode
                        )
                    |> Effect.map config.toMsg
            , seed = seed
            , needsPersisting = False
            }

        Just field ->
            let
                breadcrumbsNested =
                    increaseDepthCollection breadcrumbs
            in
            field
                |> config.init populateDefaultValues c shared seed breadcrumbsNested
                |> Updated.map (toModel config shared decoded.path breadcrumbs decoded.nodeFocus decoded.grouped << Just)
                |> Updated.mapEffect (MsgForm >> config.toMsg)


{-| -}
initWith :
    Config e f m (Model f) (Msg m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Model f
    -> Result e (Maybe (Updated (Model f) m))
initWith _ _ _ _ _ _ =
    -- FIXME
    -- if data.grouped.mintIri then
    --     getInstance shared data.breadcrumbs data.grouped
    --         |> Maybe.map
    --             (\instance ->
    --                 if Just instance == data.nodeInstance then
    --                     initFormWith config populateDefaultValues c shared seed data instance
    --                 else
    --                     initForm config populateDefaultValues c shared seed data.breadcrumbs data.grouped instance
    --                         |> Result.map Just
    --             )
    --         |> Maybe.withDefault
    --             (mintInstance config c shared seed data.breadcrumbs data.grouped
    --                 |> Ok
    --                 |> Result.map Just
    --             )
    -- else
    --     let
    --         ( nodeFocus, _ ) =
    --             case getInstance shared data.breadcrumbs data.grouped of
    --                 Nothing ->
    --                     Rdf.generateBlankNode seed
    --                         |> Tuple.mapFirst Rdf.asBlankNodeOrIri
    --                 Just instance ->
    --                     ( instance, seed )
    --     in
    --     if Just nodeFocus == data.nodeInstance then
    --         initFormWith config populateDefaultValues c shared seed data nodeFocus
    --     else
    --         initForm config populateDefaultValues c shared seed data.breadcrumbs data.grouped nodeFocus
    --             |> Result.map Just
    Ok Nothing


toModel :
    Config e f m (Model f) (Msg m)
    -> Shared
    -> Path
    -> Breadcrumbs
    -> BlankNodeOrIri
    -> Grouped.DataSingleNested
    -> Maybe f
    -> Model f
toModel config shared path breadcrumbs nodeFocus grouped form =
    Model
        { path = path
        , breadcrumbs = breadcrumbs
        , grouped = grouped
        , nodeFocus = nodeFocus
        , iriDocument = shared.iri
        , form = form
        , checked = Maybe.unwrap False (not << config.isEmpty) form
        }



-- UPDATE


{-| -}
type Msg m
    = ApiReturnedDecoded (Api.Response Decoded.Field)
    | UserToggledVisibility
    | MsgForm m


{-| -}
update :
    Config e f m (Model f) (Msg m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Msg m
    -> Model f
    -> Updated (Model f) m
update config populateDefaultValues c shared seed msg ((Model ({ breadcrumbs } as data)) as singleNested) =
    case msg of
        ApiReturnedDecoded (Err error) ->
            { model = singleNested
            , effect = Effect.addToastError error
            , seed = seed
            , needsPersisting = False
            }

        ApiReturnedDecoded (Ok field) ->
            let
                breadcrumbsNested =
                    increaseDepthCollection breadcrumbs

                setFormAndNodeInstance form =
                    Model { data | form = Just form }
            in
            field
                |> config.init populateDefaultValues c shared seed breadcrumbsNested
                |> Updated.mapEffect (MsgForm >> config.toMsg)
                |> Updated.map setFormAndNodeInstance
                |> Updated.needsPersisting

        UserToggledVisibility ->
            { data | checked = not data.checked }
                |> Model
                |> Updated.from seed
                |> Updated.needsPersisting

        MsgForm msgForm ->
            case data.form of
                Nothing ->
                    { model = singleNested
                    , effect = Effect.none
                    , seed = seed
                    , needsPersisting = False
                    }

                Just form ->
                    config.update c shared seed msgForm form
                        |> Updated.map (\formUpdated -> Model { data | form = Just formUpdated })
                        |> Updated.mapEffect (MsgForm >> config.toMsg)


{-| -}
expandAll : Config e f m (Model f) (Msg m) -> Model f -> Model f
expandAll config (Model data) =
    Model { data | form = Maybe.map config.expandAll data.form }



-- SUBSCRIPTIONS


{-| -}
subscriptions : Config e f m (Model f) (Msg m) -> Model f -> Sub m
subscriptions config (Model data) =
    Maybe.unwrap Sub.none config.subscriptions data.form



-- VIEW


{-| -}
view :
    Config e f m (Model f) (Msg m)
    -> C
    -> Report
    -> Bool
    -> Model f
    -> Maybe (Container m)
view config c report isDragging ((Model data) as model) =
    case data.form of
        Just form ->
            case data.grouped.optional of
                Translated.OptionalViaCheckbox ->
                    [ viewCheckbox config c model
                    , if data.checked then
                        row
                            [ width fill
                            , paddingEach
                                { top = 0
                                , right = 0
                                , bottom = 0
                                , left = 8 + 36 + 6 + 20 - 16
                                }
                            ]
                            [ viewForm config c report isDragging model form
                            ]

                      else
                        none
                    ]
                        |> Element.column
                            [ width fill

                            --, Clipboard.onCopy (config.toMsg UserCopied)
                            ]
                        |> Container.expand
                        |> Just

                Translated.OptionalViaNested ->
                    viewForm config c report isDragging model form
                        |> Container.expand
                        |> Just

                Translated.RequiredNested ->
                    viewForm config c report isDragging model form
                        |> Container.expand
                        |> Just

        _ ->
            Nothing


viewCheckbox : Config e f m (Model f) (Msg m) -> C -> Model f -> Element m
viewCheckbox config c (Model data) =
    [ data.checked
        |> Checkbox.checkbox
            (\_ -> config.toMsg UserToggledVisibility)
            (Ui.labelProvide c data.grouped)
        |> Checkbox.toElement

    -- , Icon.ContentCopy
    --     |> ButtonIcon.buttonIcon (config.toMsg UserCopied)
    --     |> ButtonIcon.withAttributes
    --         [ Element.class "shacl-form-input-optional-via-checkbox__button-copy"
    --         , Element.alignRight
    --         , Element.paddingXY 8 0
    --         ]
    --     |> ButtonIcon.toElement
    ]
        |> Element.row
            [ width fill
            , Element.class "shacl-form-input-single-nested"
            ]


viewForm :
    Config e f m (Model f) (Msg m)
    -> C
    -> Report
    -> Bool
    -> Model f
    -> f
    -> Element m
viewForm config c report isDragging ((Model data) as model) form =
    (config.view c (reportInstance data.nodeFocus model report) isDragging form
        |> Maybe.map
            (Element.map (MsgForm >> config.toMsg)
                >> errorWrapper (help config c report model)
            )
        |> Maybe.withDefault none
    )
        -- FIXME This is a hack to transfer focus from the nesting
        -- element to the nested element when clicking on a problem
        -- raised on the nesting element. We cannot simply use the same
        -- breadcrumb for the nesting and the nested element, because
        -- the nesting in apparent to `Shacl.Form` which determines and
        -- relies on the breadcrumb for each element.
        |> Layout.defaultEmbedded
        |> (Html.label
                [ Html.Attributes.id (Breadcrumbs.id data.path)
                , Html.Attributes.for (Breadcrumbs.id (config.path form))
                , Html.Attributes.style "width" "100%"
                ]
                << List.singleton
           )
        |> Element.html
        |> el [ width fill ]


{-| -}
viewFullscreen :
    Config e f m (Model f) (Msg m)
    -> C
    -> Report
    -> Bool
    -> Model f
    -> Maybe (Element m)
viewFullscreen config c report isDragging ((Model data) as model) =
    data.form
        |> Maybe.andThen (config.fullscreen c (reportInstance data.nodeFocus model report) isDragging)
        |> Maybe.map (Element.map (MsgForm >> config.toMsg))


reportInstance : BlankNodeOrIri -> Model f -> Report -> Report
reportInstance nodeFocus (Model { grouped }) report =
    Report.detailsFor
        (Report.emptySelector
            |> Maybe.apply Report.selectFocusNode (Rdf.toIri nodeFocus)
            |> Report.selectPath grouped.path
        )
        report


increaseDepthCollection : Breadcrumbs -> Breadcrumbs
increaseDepthCollection breadcrumbs =
    { breadcrumbs | depthCollection = breadcrumbs.depthCollection + 1 }


{-| -}
card : Config e f m (Model f) (Msg m) -> C -> Model f -> Maybe (Container m)
card config c (Model data) =
    data.form
        |> Maybe.andThen (config.card c)
        |> Maybe.map
            (Element.map (MsgForm >> config.toMsg)
                >> Container.expand
            )


{-| -}
columns :
    Config e f m (Model f) (Msg m)
    -> C
    -> List String
    -> Grouped.DataSingleNested
    -> List (Table.Column f m)
columns config c scope grouped =
    grouped.field
        |> config.columns c scope
        |> List.map
            (\column ->
                { header = column.header
                , width = column.width
                , view =
                    config.toField
                        >> Maybe.andThen (\(Model data) -> data.form)
                        >> Maybe.map column.view
                        >> Maybe.withDefault TableCell.empty
                }
            )


{-| -}
help : Config e f m (Model f) (Msg m) -> C -> Report -> Model f -> List Fluent
help config c report ((Model data) as model) =
    case data.form of
        Nothing ->
            []

        Just form ->
            let
                fluents =
                    report.results
                        |> Report.validationResultsFor
                            (Report.emptySelector
                                |> Maybe.apply Report.selectFocusNode (Rdf.toIri data.nodeFocus)
                                |> Maybe.apply Report.selectSourceShape (Rdf.toIri data.grouped.node)
                                |> Report.selectPath data.grouped.path
                            )
                        |> List.map (helpFromValidationResult c model >> verbatim)

                fluentsInstance =
                    config.help c (reportInstance data.nodeFocus model report) form
            in
            if not (List.isEmpty fluents) && List.isEmpty fluentsInstance then
                fluents

            else
                fluentsInstance


helpFromValidationResult : C -> Model f -> ValidationResult -> String
helpFromValidationResult c (Model { grouped }) (ValidationResult { constraintComponent, message }) =
    let
        name =
            grouped.names
                |> List.head
                |> Maybe.map (Localize.withinSentenceWithArticleIndefinite c)
                |> Maybe.withDefault "a value"
    in
    case constraintComponent of
        Report.MinCountConstraintComponent ->
            "Enter " ++ name ++ "."

        Report.QualifiedMinCountConstraintComponent ->
            "Enter " ++ name ++ "."

        _ ->
            Localize.verbatim c message ++ "."


{-| -}
summary : Config e f m (Model f) (Msg m) -> C -> Model f -> List Fluent
summary config c (Model data) =
    Maybe.unwrap [] (config.summary c) data.form



-- OUTPUT


{-| -}
encoders :
    Config e f m (Model f) (Msg m)
    -> C
    -> Model f
    -> List Encode.PropertyEncoder
encoders config c (Model data) =
    case data.form of
        Nothing ->
            []

        Just form ->
            case data.grouped.optional of
                Translated.OptionalViaCheckbox ->
                    if data.checked then
                        [ encoderInstance config c data form ]

                    else
                        []

                Translated.OptionalViaNested ->
                    if config.isEmpty form then
                        []

                    else
                        [ encoderInstance config c data form ]

                Translated.RequiredNested ->
                    [ encoderInstance config c data form ]


encoderInstance :
    Config e f m (Model f) (Msg m)
    -> C
    -> Data f
    -> f
    -> PropertyEncoder
encoderInstance config c data form =
    Encode.property data.grouped.path
        (Encode.node (config.nodeFocus form)
            (List.concat
                [ config.encoders c form
                , encodersClasses data
                , encodersIsDefinedBy data
                ]
            )
        )


encodersClasses : Data f -> List PropertyEncoder
encodersClasses data =
    List.map (Encode.predicate a << Encode.iri) data.grouped.classes


encodersIsDefinedBy : Data f -> List PropertyEncoder
encodersIsDefinedBy data =
    let
        iriDocumentWithFragment =
            Rdf.setFragment (Breadcrumbs.id data.path) data.iriDocument
    in
    [ Encode.predicate RDFS.isDefinedBy
        (Encode.object iriDocumentWithFragment)
    ]



-- INFO


{-| -}
toNodeFocus : Model f -> BlankNodeOrIri
toNodeFocus (Model data) =
    data.nodeFocus


{-| -}
toPath : Model f -> Path
toPath (Model data) =
    data.path


{-| -}
isEmpty : Config e f m (Model f) (Msg m) -> Model f -> Bool
isEmpty config (Model data) =
    Maybe.unwrap True config.isEmpty data.form


{-| -}
dragging : Config e f m (Model f) (Msg m) -> Model f -> Bool
dragging config (Model data) =
    Maybe.unwrap False config.dragging data.form


{-| -}
log : Config e f m (Model f) (Msg m) -> Model f -> Pretty.Doc ()
log config (Model data) =
    Pretty.lines
        [ Pretty.string "Single Nested"
        , case data.form of
            Nothing ->
                Pretty.empty

            Just form ->
                Pretty.indent 2 (config.log form)
        ]
