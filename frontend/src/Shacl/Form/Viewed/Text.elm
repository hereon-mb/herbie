module Shacl.Form.Viewed.Text exposing
    ( Model
    , init, initWith
    , Msg, update, expandAll
    , subscriptions
    , view, viewFullscreen, card, columns, help, summary
    , encoders
    , toNodeFocus, toPath, isEmpty, dragging, log
    )

{-|


# Model

@docs Model


# Init

@docs init, initWith


# Update

@docs Msg, update, expandAll


# Subscriptions

@docs subscriptions


# View

@docs view, viewFullscreen, card, columns, help, summary


# Output

@docs encoders


# Info

@docs toNodeFocus, toPath, isEmpty, dragging, log

-}

import Element as Ui exposing (Element)
import Element.Lazy exposing (lazy4)
import Fluent exposing (Fluent)
import Maybe.Apply as Maybe
import Maybe.Extra as Maybe
import Pretty
import Rdf exposing (BlankNodeOrIri)
import Rdf.Decode as Decode exposing (Decoder)
import Rdf.Decode.Extra as Decode
import Rdf.Encode as Encode exposing (PropertyEncoder)
import Rdf.Graph exposing (Graph, Seed)
import Shacl exposing (ConfigLabel(..))
import Shacl.Clipboard as Clipboard
import Shacl.Form.Breadcrumbs as Breadcrumbs exposing (Breadcrumbs, Path)
import Shacl.Form.Context exposing (C)
import Shacl.Form.Decoded as Decoded
import Shacl.Form.Effect as Effect
import Shacl.Form.Localize as Localize
import Shacl.Form.Shared as Shared exposing (Config, Shared, withInfo)
import Shacl.Form.Translated as Translated
import Shacl.Form.Ui as Ui
import Shacl.Form.Ui.Container as Container exposing (Container)
import Shacl.Form.Updated as Updated exposing (Updated)
import Shacl.Report as Report exposing (ConstraintComponent(..), Report, ValidationResult(..))
import String.Extra.Extra as String
import Ui.Atom.Button as Button exposing (button)
import Ui.Atom.ErrorWrapper exposing (errorWrapper)
import Ui.Atom.Icon as Icon
import Ui.Atom.Labelled as Labelled
import Ui.Atom.TextArea as TextArea exposing (textArea)
import Ui.Atom.TextInput as TextInput exposing (textInput)
import Ui.Molecule.Table as Table
import Ui.Molecule.TableCell as TableCell exposing (TableCell)
import Ui.Theme.Markdown as Markdown
import Ui.Theme.Typography as Typography



-- MODEL


{-| -}
type Model
    = Model Data


type alias Data =
    { path : Path
    , breadcrumbs : Breadcrumbs
    , translated : Translated.DataText
    , nodeFocus : BlankNodeOrIri
    , editForm : Bool
    , valueGenerated : Maybe String
    , value : Maybe String
    }



-- INIT


{-| -}
init :
    Config e f m Model Msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Breadcrumbs
    -> Decoded.DataText
    -> Updated Model m
init config populateDefaultValues c shared seed breadcrumbs decoded =
    case decoded.text of
        Nothing ->
            empty config populateDefaultValues c shared seed decoded.path breadcrumbs decoded.nodeFocus decoded.translated

        Just text ->
            withValue config c shared seed decoded.path breadcrumbs decoded.nodeFocus decoded.translated text


{-| -}
empty :
    Config e f m Model Msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Path
    -> Breadcrumbs
    -> BlankNodeOrIri
    -> Translated.DataText
    -> Updated Model msg
empty config { populateDefaultValues } c shared seed path breadcrumbs nodeFocus translated =
    if populateDefaultValues then
        case translated.defaultValue of
            Nothing ->
                withValue config c shared seed path breadcrumbs nodeFocus translated ""

            Just text ->
                withValue config c shared seed path breadcrumbs nodeFocus translated text

    else
        withValue config c shared seed path breadcrumbs nodeFocus translated ""


withValue :
    Config e f m Model Msg
    -> C
    -> Shared
    -> Seed
    -> Path
    -> Breadcrumbs
    -> BlankNodeOrIri
    -> Translated.DataText
    -> String
    -> Updated Model msg
withValue _ c shared seed path breadcrumbs nodeFocus translated value =
    { path = path
    , breadcrumbs = breadcrumbs
    , translated = translated
    , nodeFocus = nodeFocus
    , editForm = shared.editForm
    , valueGenerated = valueGenerated c shared nodeFocus translated
    , value = Just value
    }
        |> Model
        |> Updated.from seed


valueGenerated : C -> Shared -> BlankNodeOrIri -> Translated.DataText -> Maybe String
valueGenerated c shared nodeFocus translated =
    shared.graphGenerated
        |> decode c shared nodeFocus translated
        |> Result.toMaybe


{-| -}
initWith :
    Config e f m Model Msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Model
    -> Result e (Maybe (Updated Model m))
initWith =
    Shared.initWith
        { decoder = \c shared (Model data) -> decoder c shared data.translated
        , default = \_ _ (Model data) -> data.translated.defaultValue
        , nodeFocus = \(Model data) -> data.nodeFocus
        , path = \(Model data) -> data.translated.path
        , updateValue = updateValue
        , removeValue = removeValue
        , tagField = Translated.TagFieldText
        }


updateValue :
    Config e f m Model Msg
    -> C
    -> Shared
    -> Seed
    -> String
    -> Model
    -> Maybe (Updated Model msg)
updateValue _ _ _ seed value (Model data) =
    if Just value == Maybe.map cleanup data.value then
        Nothing

    else
        { data | value = Just value }
            |> Model
            |> Updated.from seed
            |> Just


removeValue :
    Config e f m Model Msg
    -> C
    -> Shared
    -> Seed
    -> Model
    -> Maybe (Updated Model msg)
removeValue _ _ _ seed (Model data) =
    if data.value == Nothing then
        Nothing

    else
        { data | value = Nothing }
            |> Model
            |> Updated.from seed
            |> Just



-- UPDATE


{-| -}
type Msg
    = UserChangedText String
    | UserPressedReset
    | UserCopied
    | UserPasted String
    | Ignored


{-| -}
update :
    Config e f m Model Msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Msg
    -> Model
    -> Updated Model msg
update config _ c _ seed msg ((Model data) as model) =
    case msg of
        UserChangedText valueUpdated ->
            { data | value = Just valueUpdated }
                |> Model
                |> Updated.from seed
                |> Updated.needsPersisting

        UserPressedReset ->
            { data | value = Nothing }
                |> Model
                |> Updated.from seed
                |> Updated.needsPersisting

        UserCopied ->
            model
                |> Updated.from seed
                |> Updated.withEffect
                    (encoders config c model
                        |> Clipboard.serialize data.nodeFocus seed
                        |> Effect.copyToClipboard
                    )

        UserPasted valuePasted ->
            { data
                | value =
                    Just
                        (if data.translated.singleLine then
                            String.forTextInput valuePasted

                         else
                            valuePasted
                        )
            }
                |> Model
                |> Updated.from seed
                |> Updated.needsPersisting

        Ignored ->
            Updated.from seed model


{-| -}
expandAll : Config e f m Model Msg -> Model -> Model
expandAll _ model =
    model



-- SUBSCRIPTIONS


{-| -}
subscriptions : Config e f m Model Msg -> Model -> Sub m
subscriptions _ _ =
    Sub.none



-- VIEW


{-| -}
view :
    Config e f m Model Msg
    -> C
    -> Report
    -> Bool
    -> Model
    -> Maybe (Container m)
view config c report _ model =
    viewHelp config c report model
        |> Container.expand
        |> Just


viewHelp :
    Config e f m Model Msg
    -> C
    -> Report
    -> Model
    -> Element m
viewHelp =
    lazy4
        (\config c report ((Model data) as model) ->
            data
                |> valueActual
                |> Maybe.withDefault ""
                |> viewInput c data
                |> addResetButton data
                |> errorWrapper (help config c report model)
                |> Ui.map config.toMsg
        )


viewInput : C -> Data -> String -> Element Msg
viewInput c data =
    if data.translated.singleLine then
        viewTextInput c data

    else
        viewTextArea c data


viewTextInput : C -> Data -> String -> Element Msg
viewTextInput c data value =
    value
        |> textInput UserChangedText (TextInput.labelAbove (Ui.label c data.translated))
        |> TextInput.withId (Breadcrumbs.id data.path)
        |> TextInput.withReadonly (readonly data)
        |> TextInput.withHoverableActionIcon
            { icon = Icon.ContentCopy
            , onClick = UserCopied
            }
        |> TextInput.onPasteGraph Ignored onPaste
        |> TextInput.onCopy UserCopied
        |> Maybe.apply TextInput.withSupportingText (Ui.description c data.translated)
        |> TextInput.view


viewTextArea : C -> Data -> String -> Element Msg
viewTextArea c data value =
    Ui.el
        [ Ui.width Ui.fill
        , Ui.height (heightTextArea value)
        ]
        (value
            |> textArea UserChangedText
            |> TextArea.withLabel (Ui.label c data.translated)
            |> TextArea.withId (Breadcrumbs.id data.path)
            |> TextArea.withReadonly (readonly data)
            |> TextArea.withHoverableActionIcon
                { icon = Icon.ContentCopy
                , onClick = UserCopied
                }
            |> Maybe.apply TextArea.withSupportingText
                (Ui.description c data.translated)
            |> TextArea.onPasteGraph Ignored onPaste
            |> TextArea.onCopy UserCopied
            |> TextArea.toElement
        )


onPaste : Decoder Msg
onPaste =
    Decode.map UserPasted (Decode.oneAtAnyPredicate Decoded.decoderText)


heightTextArea : String -> Ui.Length
heightTextArea =
    String.lines
        >> List.length
        >> max 3
        >> (*) 23
        >> (+) 57
        >> Ui.px


addResetButton : Data -> Element Msg -> Element Msg
addResetButton data element =
    if
        not data.translated.readonly
            && data.translated.generated
            && (not data.translated.persisted
                    || (data.translated.persisted && not data.editForm)
               )
    then
        Ui.row
            [ Ui.width Ui.fill
            , Ui.spacing 16
            ]
            [ element
            , viewResetButton data
            ]

    else
        element


viewResetButton : Data -> Element Msg
viewResetButton data =
    Ui.el
        [ Ui.alignTop
        , Ui.paddingXY 0 16
        ]
        ("Reset"
            |> Fluent.verbatim
            |> button UserPressedReset
            |> Button.withFormat Button.Outlined
            |> Button.withEnabled (valueGeneratedOverwritten data)
            |> Button.view
        )


{-| -}
viewFullscreen :
    Config e f m Model Msg
    -> C
    -> Report
    -> Bool
    -> Model
    -> Maybe (Element m)
viewFullscreen _ _ _ _ _ =
    Nothing


{-| -}
card : Config e f m Model Msg -> C -> Model -> Maybe (Container msg)
card _ c (Model data) =
    data
        |> valueActual
        |> Maybe.map (viewValue c data)
        |> Maybe.map Container.expand


viewValue : C -> Data -> String -> Element msg
viewValue c data =
    case data.translated.labelInCard of
        LabelAbove ->
            if data.translated.singleLine then
                Labelled.string (Ui.label c data.translated)

            else
                Labelled.markdown (Ui.label c data.translated)

        LabelHidden ->
            if data.translated.singleLine then
                Fluent.verbatim >> Typography.body1

            else
                Markdown.view


{-| -}
columns :
    Config e f m Model Msg
    -> C
    -> List String
    -> Translated.DataText
    -> List (Table.Column f m)
columns config c scope translated =
    [ { header = viewHeader c scope translated
      , width = Ui.shrink
      , view = viewCell config
      }
    ]


viewHeader : C -> List String -> Translated.DataText -> TableCell msg
viewHeader c scope translated =
    translated
        |> Ui.label c
        |> TableCell.textBold
        |> withInfo scope


viewCell : Config e f m Model Msg -> f -> TableCell m
viewCell config form =
    case config.toField form of
        Nothing ->
            TableCell.empty

        Just (Model data) ->
            data
                |> valueActual
                |> Maybe.map cleanup
                |> Maybe.andThen
                    (\value ->
                        if value == "" then
                            Nothing

                        else
                            value
                                |> Fluent.verbatim
                                |> TableCell.text
                                |> Just
                    )
                |> Maybe.withDefault TableCell.empty


{-| -}
help : Config e f m Model Msg -> C -> Report -> Model -> List Fluent
help _ c report (Model data) =
    report.results
        |> Report.validationResultsFor
            (Report.emptySelector
                |> Maybe.apply Report.selectFocusNode (Rdf.toIri data.nodeFocus)
                |> Report.selectPath data.translated.path
            )
        |> List.map (helpFromValidationResult c data >> Fluent.verbatim)


helpFromValidationResult : C -> Data -> ValidationResult -> String
helpFromValidationResult c data (ValidationResult { constraintComponent, message }) =
    let
        name =
            data.translated.names
                |> List.head
                |> Maybe.map (Localize.withinSentenceWithArticleIndefinite c)
                |> Maybe.withDefault "a value"
    in
    case constraintComponent of
        MinCountConstraintComponent ->
            "Enter " ++ name ++ "."

        _ ->
            Localize.verbatim c message ++ "."


{-| -}
summary : Config e f m Model Msg -> C -> Model -> List Fluent
summary _ _ (Model data) =
    case valueActual data of
        Nothing ->
            []

        Just value ->
            if cleanup value == "" then
                []

            else
                [ Fluent.verbatim value ]



-- OUTPUT


{-| -}
encoders : Config e f m Model Msg -> C -> Model -> List Encode.PropertyEncoder
encoders _ _ (Model data) =
    if data.translated.generated then
        if data.translated.readonly then
            []

        else if valueGeneratedOverwritten data then
            encodersValue data

        else
            []

    else
        encodersValue data


encodersValue : Data -> List PropertyEncoder
encodersValue data =
    case Maybe.map cleanup data.value of
        Nothing ->
            []

        Just "" ->
            []

        Just value ->
            [ Encode.property data.translated.path
                (Encode.literal (Rdf.string value))
            ]



-- INFO


{-| -}
toNodeFocus : Model -> BlankNodeOrIri
toNodeFocus (Model data) =
    data.nodeFocus


{-| -}
toPath : Model -> Path
toPath (Model data) =
    data.path


{-| -}
isEmpty : Config e f m Model Msg -> Model -> Bool
isEmpty _ (Model data) =
    data.value
        |> Maybe.map cleanup
        |> Maybe.map String.isEmpty
        |> Maybe.withDefault True


{-| -}
dragging : Config e f m Model Msg -> Model -> Bool
dragging _ _ =
    False


{-| -}
log : Config e f m Model Msg -> Model -> Pretty.Doc t
log _ _ =
    Pretty.string "Text"



-- DECODE
-- FIXME This should be moved to Decoded


decode :
    C
    -> Shared
    -> BlankNodeOrIri
    -> Translated.DataText
    -> Graph
    -> Result Decode.Error String
decode c shared nodeFocus translated =
    Decode.decode (Decode.from nodeFocus (decoder c shared translated))


decoder : C -> Shared -> Translated.DataText -> Decoder String
decoder _ _ translated =
    Decode.property translated.path Decoded.decoderText



-- SUPPORT


valueActual : Data -> Maybe String
valueActual data =
    if data.translated.generated then
        if data.translated.readonly then
            data.valueGenerated

        else
            data.value
                |> Maybe.orElse data.valueGenerated

    else
        data.value


valueGeneratedOverwritten : Data -> Bool
valueGeneratedOverwritten data =
    data.valueGenerated /= valueActual data


readonly : Data -> Bool
readonly data =
    data.translated.readonly || (data.translated.persisted && data.editForm)


cleanup : String -> String
cleanup =
    String.trim
