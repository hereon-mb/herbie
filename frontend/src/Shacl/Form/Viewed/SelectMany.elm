module Shacl.Form.Viewed.SelectMany exposing
    ( Model
    , init, initWith
    , Msg, update, expandAll
    , subscriptions
    , view, viewFullscreen, card, columns, help, summary
    , encoders
    , toNodeFocus, toPath, isEmpty, dragging, log
    )

{-|


# Model

@docs Model


# Init

@docs init, initWith


# Update

@docs Msg, update, expandAll


# Subscriptions

@docs subscriptions


# View

@docs view, viewFullscreen, card, columns, help, summary


# Output

@docs encoders


# Info

@docs toNodeFocus, toPath, isEmpty, dragging, log

-}

import Api
import Element as Ui exposing (Element)
import Element.Extra exposing (preventDefaultOnClick)
import Element.Lazy exposing (lazy4)
import Fluent exposing (Fluent, verbatim)
import List.Extra as List
import List.NonEmpty as NonEmpty
import Maybe.Apply as Maybe
import Maybe.Extra as Maybe
import Ontology.Shacl exposing (Class)
import Pretty
import Rdf exposing (BlankNodeOrIri)
import Rdf.Decode as Decode exposing (Decoder)
import Rdf.Decode.Extra as Decode
import Rdf.Encode as Encode
import Rdf.Graph exposing (Seed)
import Rdf.Namespaces exposing (a)
import Rdf.Namespaces.RDFS as RDFS
import Rdf.SetIri as SetIri
import Shacl.Clipboard as Clipboard
import Shacl.Form.Breadcrumbs as Breadcrumbs exposing (Breadcrumbs, Path)
import Shacl.Form.Context exposing (C)
import Shacl.Form.Decoded as Decoded
import Shacl.Form.Effect as Effect
import Shacl.Form.Localize as Localize
import Shacl.Form.Shared as Shared
    exposing
        ( Config
        , Option
        , Shared
        , optionFromOptionCombobox
        , optionToOptionCombobox
        , sortOptionsByScore
        , withInfo
        )
import Shacl.Form.Translated as Translated
import Shacl.Form.Ui as Ui
import Shacl.Form.Ui.Container as Container exposing (Container)
import Shacl.Form.Updated as Updated exposing (Updated)
import Shacl.OrderBy as OrderBy
import Shacl.Report as Report exposing (ConstraintComponent(..), Report, ValidationResult(..))
import Ui.Atom.Button as Button exposing (button)
import Ui.Atom.ErrorWrapper exposing (errorWrapper)
import Ui.Atom.Labelled as Labelled
import Ui.Molecule.ComboboxMultiselect as ComboboxMultiselect exposing (comboboxMultiselect)
import Ui.Molecule.Table as Table
import Ui.Molecule.TableCell as TableCell exposing (TableCell)



-- MODEL


{-| -}
type Model
    = Model Data


type alias Data =
    { path : Path
    , breadcrumbs : Breadcrumbs
    , translated : Translated.DataSelectMany
    , nodeFocus : BlankNodeOrIri
    , workspaceUuid : String
    , options : List Option
    , query : String
    , values : List Option
    , classesWithShaclShapes : List Class
    }



-- INIT


{-| -}
init :
    Config e f m Model Msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Breadcrumbs
    -> Decoded.DataSelectMany
    -> Updated Model m
init =
    Shared.init
        { value =
            \_ _ { selected } ->
                if List.isEmpty selected then
                    Nothing

                else
                    Just selected
        , default =
            \_ _ { translated } ->
                translated.defaultValue
                    |> List.map
                        (\value ->
                            { iri = value.this
                            , label = Just value.label
                            }
                        )
                    |> Just
        , path = .translated >> .path
        , withValue =
            \config c shared seed breadcrumbs decoded selected ->
                { path = decoded.path
                , breadcrumbs = breadcrumbs
                , translated = decoded.translated
                , nodeFocus = decoded.nodeFocus
                , workspaceUuid = shared.workspaceUuid
                , options = optionsFromDecoded c shared decoded
                , query = ""
                , values =
                    List.map
                        (\{ iri, label } ->
                            { iri = iri
                            , value = Maybe.unwrap "" (Localize.forLabel c) label
                            }
                        )
                        selected
                , classesWithShaclShapes = []
                }
                    |> Model
                    |> Updated.from seed
                    |> Updated.withEffect
                        (Effect.map config.toMsg
                            (Effect.batch
                                [ Effect.getResource
                                    ApiReturnedResource
                                    decoded.translated.class
                                , Effect.getSubclassesWithShaclShapes
                                    ApiReturnedSubclassesWithShaclShapes
                                    decoded.translated.class
                                ]
                            )
                        )
        , empty =
            \config c shared seed breadcrumbs decoded ->
                { path = decoded.path
                , breadcrumbs = breadcrumbs
                , translated = decoded.translated
                , nodeFocus = decoded.nodeFocus
                , workspaceUuid = shared.workspaceUuid
                , options = optionsFromDecoded c shared decoded
                , query = ""
                , values = []
                , classesWithShaclShapes = []
                }
                    |> Model
                    |> Updated.from seed
                    |> Updated.withEffect
                        (Effect.map config.toMsg
                            (Effect.batch
                                [ Effect.getResource
                                    ApiReturnedResource
                                    decoded.translated.class
                                , Effect.getSubclassesWithShaclShapes
                                    ApiReturnedSubclassesWithShaclShapes
                                    decoded.translated.class
                                ]
                            )
                        )
        }


optionsFromDecoded : C -> Shared -> Decoded.DataSelectMany -> List Option
optionsFromDecoded c shared decoded =
    decoded.translated.options
        |> List.map (valueToOption c)
        |> List.uniqueBy .iri
        |> Maybe.apply
            (OrderBy.sortBy .iri c.locale shared.graphCombinedFrontend)
            decoded.translated.orderBy


valueToOption : C -> Translated.Value -> Option
valueToOption c value =
    { iri = value.this
    , value = Localize.forLabel c value.label
    }


{-| -}
initWith :
    Config e f m Model Msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Model
    -> Result e (Maybe (Updated Model m))
initWith =
    Shared.initWith
        { decoder = \c shared (Model data) -> decoder c shared data.translated
        , default =
            \c _ (Model data) ->
                data.translated.defaultValue
                    |> List.map
                        (\value ->
                            { iri = value.this
                            , value = Localize.forLabel c value.label
                            }
                        )
                    |> Just
        , nodeFocus = \(Model data) -> data.nodeFocus
        , path = \(Model data) -> data.translated.path
        , updateValue = updateValue
        , removeValue = removeValue
        , tagField = Translated.TagFieldSelectMany
        }


updateValue :
    Config e f m Model Msg
    -> C
    -> Shared
    -> Seed
    -> List Option
    -> Model
    -> Maybe (Updated Model m)
updateValue _ _ _ seed options (Model data) =
    if SetIri.fromList (List.map .iri options) == SetIri.fromList (List.map .iri data.values) then
        Nothing

    else
        { data | values = options }
            |> Model
            |> Updated.from seed
            |> Just


removeValue :
    Config e f m Model Msg
    -> C
    -> Shared
    -> Seed
    -> Model
    -> Maybe (Updated Model m)
removeValue _ _ _ seed (Model data) =
    if List.isEmpty data.values then
        Nothing

    else
        { data | values = [] }
            |> Model
            |> Updated.from seed
            |> Just



-- INIT


{-| -}
type Msg
    = NoOp
    | ApiReturnedResource (Maybe (List Option))
    | ApiReturnedSubclassesWithShaclShapes (Api.Response (List Class))
    | UserPressedCreateNew (List Class)
    | UserChangedDropdownMultiselectQuery String
    | UserSelectedValueInDropdownMultiselect Option
    | UserUnselectedValueInDropdownMultiselect Option
    | UserCopied
    | UserPasted (List Option)


{-| -}
update :
    Config e f m Model Msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Msg
    -> Model
    -> Updated Model m
update config _ c shared seed msg ((Model data) as model) =
    case msg of
        NoOp ->
            Updated.from seed model

        ApiReturnedResource Nothing ->
            -- FIXME Report this to user
            Updated.from seed model

        ApiReturnedResource (Just options) ->
            { data
                | options =
                    [ data.options
                    , options
                    ]
                        |> List.concat
                        |> List.uniqueBy .iri
                        |> Maybe.apply
                            (OrderBy.sortBy .iri c.locale shared.graphCombinedFrontend)
                            data.translated.orderBy
            }
                |> Model
                |> Updated.from seed

        ApiReturnedSubclassesWithShaclShapes (Err _) ->
            -- FIXME Do we want to report the error?
            Updated.from seed model

        ApiReturnedSubclassesWithShaclShapes (Ok classesWithShaclShapes) ->
            { data | classesWithShaclShapes = classesWithShaclShapes }
                |> Model
                |> Updated.from seed

        UserPressedCreateNew classes ->
            model
                |> Updated.from seed
                |> Updated.withEffect (Effect.UserIntentsToCreateDatasetFor classes)

        UserChangedDropdownMultiselectQuery query ->
            { data | query = query }
                |> Model
                |> Updated.from seed

        UserSelectedValueInDropdownMultiselect value ->
            { data | values = List.unique (data.values ++ [ value ]) }
                |> Model
                |> Updated.from seed
                |> Updated.needsPersisting

        UserUnselectedValueInDropdownMultiselect value ->
            { data | values = List.filter ((/=) value) data.values }
                |> Model
                |> Updated.from seed
                |> Updated.needsPersisting

        UserCopied ->
            model
                |> Updated.from seed
                |> Updated.withEffect
                    (encoders config c model
                        |> Clipboard.serialize data.nodeFocus seed
                        |> Effect.copyToClipboard
                    )

        UserPasted valuesPasted ->
            { data | values = valuesPasted }
                |> Model
                |> Updated.from seed
                |> Updated.needsPersisting


{-| -}
expandAll : Config e f m Model Msg -> Model -> Model
expandAll _ =
    identity



-- SUBSCRIPTIONS


{-| -}
subscriptions : Config e f m Model Msg -> Model -> Sub m
subscriptions _ _ =
    Sub.none



-- VIEW


{-| -}
view :
    Config e f m Model Msg
    -> C
    -> Report
    -> Bool
    -> Model
    -> Maybe (Container m)
view config c report _ model =
    viewHelp config c report model
        |> Container.expand
        |> Just


viewHelp :
    Config e f m Model Msg
    -> C
    -> Report
    -> Model
    -> Element m
viewHelp =
    lazy4
        (\config c report ((Model data) as model) ->
            let
                notSelected option =
                    not (List.member option data.values)
            in
            Ui.column
                [ Ui.width Ui.fill
                , Ui.spacing 8
                ]
                [ { id = Breadcrumbs.pointer data.path
                  , label = ComboboxMultiselect.labelAbove (Ui.label c data.translated)
                  , initialQuery = data.query
                  , supportingText = Nothing
                  , help = []
                  , options =
                        data.options
                            |> List.filter notSelected
                            |> sortOptionsByScore data.query
                  , selected = data.values
                  , toOption = optionToOptionCombobox
                  , fromOption = optionFromOptionCombobox data.options
                  , onChange = UserChangedDropdownMultiselectQuery
                  , onSelect = UserSelectedValueInDropdownMultiselect
                  , onUnselect = UserUnselectedValueInDropdownMultiselect
                  , onCopy = Just UserCopied
                  , onPaste = Just ( "text/plain", NoOp, Clipboard.allObjects UserPasted (decoderValue c data.translated) )
                  , noOp = NoOp
                  , renderSubLabels = c.preferences.renderIrisInDropdownMenus
                  }
                    |> comboboxMultiselect
                    |> errorWrapper (help config c report model)
                    |> Ui.map config.toMsg
                , Ui.el
                    [ Ui.width Ui.fill
                    , preventDefaultOnClick (config.toMsg NoOp)
                    ]
                    (viewCreateNew config data)
                ]
        )


viewCreateNew : Config e f m Model Msg -> Data -> Ui.Element m
viewCreateNew config data =
    if List.isEmpty data.classesWithShaclShapes then
        Ui.none

    else
        Ui.el [ Ui.alignRight ]
            ("Create new"
                |> verbatim
                |> button (config.toMsg (UserPressedCreateNew data.classesWithShaclShapes))
                |> Button.withFormat Button.Text
                |> Button.view
            )


{-| -}
viewFullscreen :
    Config e f m Model Msg
    -> C
    -> Report
    -> Bool
    -> Model
    -> Maybe (Element m)
viewFullscreen _ _ _ _ _ =
    Nothing


{-| -}
card : Config e f m Model Msg -> C -> Model -> Maybe (Container msg)
card _ c (Model data) =
    data.values
        |> List.map
            (\option ->
                { url = c.toUrl data.translated.class option.iri
                , label = verbatim option.value
                }
            )
        |> Labelled.linkList (Ui.label c data.translated)
        |> Container.expand
        |> Just


{-| -}
columns :
    Config e f m Model Msg
    -> C
    -> List String
    -> Translated.DataSelectMany
    -> List (Table.Column f m)
columns config c scope translated =
    [ { header = viewHeader c scope translated
      , width = Ui.shrink
      , view = viewCell config
      }
    ]


viewHeader : C -> List String -> Translated.DataSelectMany -> TableCell m
viewHeader c scope translated =
    translated
        |> Ui.label c
        |> TableCell.textBold
        |> withInfo scope


viewCell : Config e f m Model Msg -> f -> TableCell mb
viewCell config form =
    case config.toField form of
        Nothing ->
            TableCell.empty

        Just (Model data) ->
            if List.isEmpty data.values then
                TableCell.empty

            else
                data.values
                    |> List.map
                        (\option ->
                            { url = Rdf.toUrl option.iri
                            , label = verbatim option.value
                            }
                        )
                    |> TableCell.linkList


{-| -}
help : Config e f m Model Msg -> C -> Report -> Model -> List Fluent
help _ c report (Model data) =
    report.results
        |> Report.validationResultsFor
            (Report.emptySelector
                |> Maybe.apply Report.selectFocusNode (Rdf.toIri data.nodeFocus)
                |> Report.selectPath data.translated.path
                |> Maybe.apply Report.selectSourceShape (Rdf.toIri data.translated.node)
            )
        |> List.filterMap (helpFromValidationResult c data)


helpFromValidationResult : C -> Data -> ValidationResult -> Maybe Fluent
helpFromValidationResult c data (ValidationResult { constraintComponent, message }) =
    let
        minCountConstraint =
            Just (verbatim ("Select " ++ name))

        maxCountConstraint =
            Just (verbatim ("You have selected too many " ++ namePlural))

        name =
            data.translated.names
                |> List.head
                |> Maybe.map (Localize.withinSentenceWithArticleIndefinite c)
                |> Maybe.withDefault "a value"

        namePlural =
            data.translated.names
                |> List.head
                |> Maybe.map (Localize.withinSentencePlural c)
                |> Maybe.withDefault "values"
    in
    case constraintComponent of
        MinCountConstraintComponent ->
            minCountConstraint

        QualifiedMinCountConstraintComponent ->
            minCountConstraint

        MaxCountConstraintComponent ->
            maxCountConstraint

        QualifiedMaxCountConstraintComponent ->
            maxCountConstraint

        _ ->
            Just (verbatim (Localize.verbatim c message ++ "."))


{-| -}
summary : Config e f m Model Msg -> C -> Model -> List Fluent
summary _ c (Model data) =
    case data.values of
        [] ->
            []

        [ value ] ->
            [ Fluent.verbatim value.value ]

        _ ->
            case List.head data.translated.names of
                Nothing ->
                    []

                Just name ->
                    [ Fluent.verbatim
                        (String.concat
                            [ String.fromInt (List.length data.values)
                            , " "
                            , Localize.withinSentencePlural c name
                            ]
                        )
                    ]



-- OUTPUT


{-| -}
encoders : Config e f m Model Msg -> C -> Model -> List Encode.PropertyEncoder
encoders _ _ (Model data) =
    case Maybe.map NonEmpty.toList (NonEmpty.fromList data.values) of
        Nothing ->
            []

        Just values ->
            -- FIXME `class` is not serialized into clipboard
            List.map
                (\{ iri, value } ->
                    Encode.property data.translated.path
                        (Encode.node iri
                            [ Encode.predicate a
                                (Encode.object data.translated.class)
                            , Encode.predicate RDFS.label
                                (Encode.literal (Rdf.string value))
                            ]
                        )
                )
                values



-- INFO


{-| -}
toNodeFocus : Model -> BlankNodeOrIri
toNodeFocus (Model data) =
    data.nodeFocus


{-| -}
toPath : Model -> Path
toPath (Model data) =
    data.path


{-| -}
isEmpty : Config e f m Model Msg -> Model -> Bool
isEmpty _ (Model data) =
    List.isEmpty data.values


{-| -}
dragging : Config e f m Model Msg -> Model -> Bool
dragging =
    \_ _ -> False


{-| -}
log : Config e f m Model Msg -> Model -> Pretty.Doc t
log _ (Model _) =
    Pretty.string "Select Many"



-- DECODE


decoder : C -> Shared -> Translated.DataSelectMany -> Decoder (List Option)
decoder c _ translated =
    Decode.property translated.path (decoderValue c translated)


decoderValue : C -> Translated.DataSelectMany -> Decoder (List Option)
decoderValue c translated =
    Decode.instancesOfStrict translated.class
        (Decode.succeed Option
            |> Decode.custom Decode.iri
            |> Decode.required RDFS.label (Decode.map (Localize.forLabel c) Decode.stringOrLangString)
        )
