module Shacl.Form.Viewed.Constant exposing
    ( Model
    , init, initWith
    , expandAll
    , subscriptions
    , view, viewFullscreen, card, columns, help, summary
    , encoders
    , toNodeFocus, toPath, isEmpty, dragging, log
    )

{-|


# Model

@docs Model


# Init

@docs init, initWith


# Update

@docs Msg, expandAll


# Subscriptions

@docs subscriptions


# View

@docs view, viewFullscreen, card, columns, help, summary


# Output

@docs encoders


# Info

@docs toNodeFocus, toPath, isEmpty, dragging, log

-}

import Element exposing (Element)
import Fluent exposing (Fluent)
import Pretty
import Rdf exposing (BlankNodeOrIri)
import Rdf.Encode as Encode
import Rdf.Graph as Rdf
import Shacl.Form.Breadcrumbs exposing (Breadcrumbs, Path)
import Shacl.Form.Context exposing (C)
import Shacl.Form.Decoded as Decoded
import Shacl.Form.Shared exposing (Config, Shared)
import Shacl.Form.Translated as Translated
import Shacl.Form.Ui.Container exposing (Container)
import Shacl.Form.Updated as Updated exposing (Updated)
import Shacl.Report exposing (Report)
import Ui.Molecule.Table as Table



-- MODEL


{-| -}
type Model
    = Model Data


type alias Data =
    { path : Path
    , breadcrumbs : Breadcrumbs
    , translated : Translated.DataConstant
    , nodeFocus : BlankNodeOrIri
    }



-- INIT


{-| -}
init :
    Config e f m Model msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Rdf.Seed
    -> Breadcrumbs
    -> Decoded.DataConstant
    -> Updated Model m
init _ _ _ _ seed breadcrumbs decoded =
    { path = decoded.path
    , breadcrumbs = breadcrumbs
    , translated = decoded.translated
    , nodeFocus = decoded.nodeFocus
    }
        |> Model
        |> Updated.from seed


{-| -}
initWith :
    Config e f m Model msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Rdf.Seed
    -> Model
    -> Result e (Maybe (Updated Model m))
initWith _ _ _ _ _ _ =
    Ok Nothing



-- UPDATE


{-| -}
expandAll : Config e f m Model msg -> Model -> Model
expandAll _ model =
    model



-- SUBSCRIPTIONS


{-| -}
subscriptions : Config e f m Model msg -> Model -> Sub m
subscriptions _ _ =
    Sub.none



-- VIEW


{-| -}
view :
    Config e f m Model msg
    -> C
    -> Report
    -> Bool
    -> Model
    -> Maybe (Container m)
view _ _ _ _ _ =
    Nothing


{-| -}
viewFullscreen :
    Config e f m Model msg
    -> C
    -> Report
    -> Bool
    -> Model
    -> Maybe (Element m)
viewFullscreen _ _ _ _ _ =
    Nothing


{-| -}
card : Config e f m Model msg -> C -> Model -> Maybe (Container m)
card _ _ _ =
    Nothing


{-| -}
columns :
    Config e f m Model msg
    -> C
    -> List String
    -> Translated.DataConstant
    -> List (Table.Column f m)
columns _ _ _ _ =
    []


{-| -}
help : Config e f m Model msg -> C -> Report -> Model -> List Fluent
help _ _ _ _ =
    []


{-| -}
summary : Config e f m Model msg -> C -> Model -> List Fluent
summary _ _ _ =
    []



-- OUTPUT


{-| -}
encoders : Config e f m Model msg -> C -> Model -> List Encode.PropertyEncoder
encoders _ _ (Model data) =
    [ Encode.property data.translated.path
        (Encode.object data.translated.value)
    ]



-- INFO


{-| -}
toNodeFocus : Model -> BlankNodeOrIri
toNodeFocus (Model data) =
    data.nodeFocus


{-| -}
toPath : Model -> Path
toPath (Model data) =
    data.path


{-| -}
isEmpty : Config e f m Model msg -> Model -> Bool
isEmpty _ _ =
    False


{-| -}
dragging : Config e f m Model msg -> Model -> Bool
dragging _ _ =
    False


{-| -}
log : Config e f m Model msg -> Model -> Pretty.Doc t
log _ (Model _) =
    Pretty.string "Constant"
