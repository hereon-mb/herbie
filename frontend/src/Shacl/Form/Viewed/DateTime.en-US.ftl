shacl-form-viewed-date-time--date = { DATETIME($date) }
shacl-form-viewed-date-time--posix = { DATETIME($posix, year: "numeric", month: "numeric", day: "numeric", hour: "numeric", minute: "numeric") }
