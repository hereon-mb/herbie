module Shacl.Form.Viewed.FileUpload exposing
    ( Model
    , init, initWith
    , Msg, update, expandAll
    , subscriptions
    , view, viewFullscreen, card, columns, help, summary
    , encoders
    , toNodeFocus, toPath, isEmpty, dragging, log
    )

{-|


# Model

@docs Model


# Init

@docs init, initWith


# Update

@docs Msg, update, expandAll


# Subscriptions

@docs subscriptions


# View

@docs view, viewFullscreen, card, columns, help, summary


# Output

@docs encoders


# Info

@docs toNodeFocus, toPath, isEmpty, dragging, log

-}

import Api
import Element
    exposing
        ( Element
        , el
        , shrink
        )
import Element.Extra exposing (stopPropagationOnClick)
import Element.Lazy exposing (lazy4)
import Fluent exposing (Fluent, verbatim)
import Maybe.Apply as Maybe
import Maybe.Extra as Maybe
import Pretty
import Rdf exposing (BlankNodeOrIri, Iri)
import Rdf.Decode as Decode exposing (Decoder)
import Rdf.Encode as Encode
import Rdf.Graph exposing (Graph, Seed)
import Rdf.Namespaces exposing (rdfs)
import Rdf.Namespaces.RDFS as RDFS
import Rdf.Query as Rdf exposing (emptyQuery)
import Shacl.Clipboard as Clipboard
import Shacl.Form.Breadcrumbs exposing (Breadcrumbs, Path)
import Shacl.Form.Context exposing (C)
import Shacl.Form.Decoded as Decoded
import Shacl.Form.Effect as Effect
import Shacl.Form.Localize as Localize
import Shacl.Form.Shared as Shared exposing (Config, Shared, withInfo)
import Shacl.Form.Translated as Translated
import Shacl.Form.Ui as Ui
import Shacl.Form.Ui.Container as Container exposing (Container)
import Shacl.Form.Updated as Updated exposing (Updated)
import Shacl.Report as Report exposing (ConstraintComponent(..), Report, ValidationResult(..))
import Ui.Atom.ErrorWrapper exposing (errorWrapper)
import Ui.Atom.Labelled as Labelled
import Ui.Molecule.FileUploadSingle as FileUploadSingle
import Ui.Molecule.Table as Table
import Ui.Molecule.TableCell as TableCell exposing (TableCell)



-- MODEL


{-| -}
type Model
    = Model Data


type alias Data =
    { path : Path
    , breadcrumbs : Breadcrumbs
    , translated : Translated.DataFileUpload
    , nodeFocus : BlankNodeOrIri
    , file : Maybe Decoded.NamedFile
    , dropzoneHovered : Bool
    }



-- INIT


{-| -}
init :
    Config e f m Model Msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Breadcrumbs
    -> Decoded.DataFileUpload
    -> Updated Model m
init =
    Shared.init
        { value = \_ _ decoded -> decoded.file
        , default =
            \_ _ { translated } ->
                -- FIXME Also retrieve file name here
                translated.defaultValue
                    |> Maybe.map
                        (\iri ->
                            { iri = iri
                            , name = Nothing
                            }
                        )
        , path = .translated >> .path
        , withValue =
            \_ _ _ seed breadcrumbs { path, translated, nodeFocus } file ->
                { path = path
                , breadcrumbs = breadcrumbs
                , translated = translated
                , nodeFocus = nodeFocus
                , file = Just file
                , dropzoneHovered = False
                }
                    |> Model
                    |> Updated.from seed
        , empty =
            \_ _ _ seed breadcrumbs { path, translated, nodeFocus } ->
                { path = path
                , breadcrumbs = breadcrumbs
                , translated = translated
                , nodeFocus = nodeFocus
                , file = Nothing
                , dropzoneHovered = False
                }
                    |> Model
                    |> Updated.from seed
        }


{-| -}
initWith :
    Config e f m Model Msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Model
    -> Result e (Maybe (Updated Model m))
initWith =
    Shared.initWith
        { decoder = \c shared (Model data) -> decoder c shared data.translated
        , default =
            \_ _ (Model data) ->
                Maybe.map
                    (\iri ->
                        { iri = iri
                        , name = Nothing
                        }
                    )
                    data.translated.defaultValue
        , nodeFocus = \(Model data) -> data.nodeFocus
        , path = \(Model data) -> data.translated.path
        , updateValue = updateValue
        , removeValue = removeValue
        , tagField = Translated.TagFieldFileUpload
        }


updateValue :
    Config e f m Model Msg
    -> C
    -> Shared
    -> Seed
    -> Decoded.NamedFile
    -> Model
    -> Maybe (Updated Model msg)
updateValue _ _ _ seed file (Model data) =
    if Just file == data.file then
        Nothing

    else
        { data | file = Just file }
            |> Model
            |> Updated.from seed
            |> Just


removeValue :
    Config e f m Model Msg
    -> C
    -> Shared
    -> Seed
    -> Model
    -> Maybe (Updated Model msg)
removeValue _ _ _ seed (Model data) =
    if data.file == Nothing then
        Nothing

    else
        { data | file = Nothing }
            |> Model
            |> Updated.from seed
            |> Just



-- UPDATE


{-| -}
type Msg
    = NoOp
    | MsgFileUploadSingle FileUploadSingle.Msg
    | ApiCreatedFile (Api.Response Graph)
    | UserPressedDeleteFile
    | UserCopied
    | UserPasted
        (Maybe
            { iri : Iri
            , name : Maybe String
            }
        )


{-| -}
update :
    Config e f m Model Msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Msg
    -> Model
    -> Updated Model m
update config _ c _ seed msg ((Model data) as model) =
    case msg of
        NoOp ->
            Updated.from seed model

        MsgFileUploadSingle msgFileUploadSingle ->
            let
                ( dropzoneHoveredNew, effectFileUploadSingle ) =
                    FileUploadSingle.update
                        { url = Maybe.map (.iri >> Rdf.toUrl) data.file
                        , createFile =
                            if c.allowUploads && c.permissions.canUploadRawFiles then
                                Effect.createFile ApiCreatedFile

                            else
                                \_ ->
                                    { message = Fluent.verbatim "Uploading raw files is not possible on this Herbie instance. Please contact the administrators."
                                    }
                                        |> Effect.AddToast
                        , deleteFile =
                            -- FIXME move this to view
                            Effect.fromMsg UserPressedDeleteFile
                        , liftMsg = MsgFileUploadSingle
                        }
                        msgFileUploadSingle
            in
            { data | dropzoneHovered = dropzoneHoveredNew }
                |> Model
                |> Updated.from seed
                |> Updated.withEffect
                    (Effect.map config.toMsg effectFileUploadSingle)

        ApiCreatedFile (Err error) ->
            model
                |> Updated.from seed
                |> Updated.withEffect (Effect.addToastError error)

        ApiCreatedFile (Ok graphFile) ->
            case
                emptyQuery
                    |> Rdf.getIriSubject graphFile
                    |> Maybe.map
                        (\iriFile ->
                            { iri = iriFile
                            , name =
                                emptyQuery
                                    |> Rdf.withSubject iriFile
                                    |> Rdf.withPredicate (rdfs "label")
                                    |> Rdf.getString graphFile
                            }
                        )
            of
                Nothing ->
                    -- FIXME store and show error
                    Updated.from seed model

                Just file ->
                    { data | file = Just file }
                        |> Model
                        |> Updated.from seed
                        |> Updated.needsPersisting

        UserPressedDeleteFile ->
            -- FIXME delete actual file, when it was never published
            { data | file = Nothing }
                |> Model
                |> Updated.from seed
                |> Updated.needsPersisting

        UserCopied ->
            model
                |> Updated.from seed
                |> Updated.withEffect
                    (encoders config c model
                        |> Clipboard.serialize data.nodeFocus seed
                        |> Effect.copyToClipboard
                    )

        UserPasted filePasted ->
            { data | file = filePasted }
                |> Model
                |> Updated.from seed
                |> Updated.needsPersisting


{-| -}
expandAll : Config e f m Model Msg -> Model -> Model
expandAll _ model =
    model



-- SUBSCRIPTIONS


{-| -}
subscriptions : Config e f m Model Msg -> Model -> Sub m
subscriptions _ _ =
    Sub.none



-- VIEW


{-| -}
view :
    Config e f m Model Msg
    -> C
    -> Report
    -> Bool
    -> Model
    -> Maybe (Container m)
view config c report _ model =
    viewHelp config c report model
        |> Container.expand
        |> Just


viewHelp :
    Config e f m Model Msg
    -> C
    -> Report
    -> Model
    -> Element m
viewHelp =
    lazy4
        (\config c report ((Model data) as model) ->
            let
                name =
                    data.translated.names
                        |> List.head
                        |> Maybe.map (Localize.withinSentenceWithArticleIndefinite c)
                        |> Maybe.withDefault "a value"
            in
            FileUploadSingle.view
                { label = Ui.label c data.translated
                , labelButton =
                    [ "Upload "
                    , name
                    ]
                        |> String.concat
                        |> verbatim
                , filename = Maybe.andThen .name data.file
                , dropzoneHovered = data.dropzoneHovered
                , help = []
                , liftMsg = MsgFileUploadSingle
                , onCopy = Just UserCopied
                , onPaste =
                    Just
                        ( "text/plain"
                        , NoOp
                        , Clipboard.allObjects UserPasted (Decode.map Just decoderValue)
                        )
                }
                |> Element.map config.toMsg
                |> errorWrapper (help config c report model)
        )


{-| -}
viewFullscreen :
    Config e f m Model Msg
    -> C
    -> Report
    -> Bool
    -> Model
    -> Maybe (Element m)
viewFullscreen _ _ _ _ _ =
    Nothing


{-| -}
card : Config e f m Model Msg -> C -> Model -> Maybe (Container m)
card config c (Model data) =
    data.file
        |> Maybe.map
            (\file ->
                { url = Rdf.toUrl file.iri
                , label =
                    file.name
                        |> Maybe.withDefault "Unknown filename"
                        |> verbatim
                }
                    |> Labelled.download (Ui.label c data.translated)
                    |> el [ stopPropagationOnClick (config.toMsg NoOp) ]
            )
        |> Maybe.map Container.wrappable


{-| -}
columns :
    Config e f m Model Msg
    -> C
    -> List String
    -> Translated.DataFileUpload
    -> List (Table.Column f m)
columns config c scope translated =
    [ { header = viewHeader c scope translated
      , width = shrink
      , view = viewCell config
      }
    ]


viewHeader : C -> List String -> Translated.DataFileUpload -> TableCell m
viewHeader c scope translated =
    translated
        |> Ui.label c
        |> TableCell.textBold
        |> withInfo scope


viewCell : Config e f m Model Msg -> f -> TableCell m
viewCell config form =
    case config.toField form of
        Nothing ->
            TableCell.empty

        Just (Model data) ->
            data.file
                |> Maybe.map
                    (\file ->
                        { url = Rdf.toUrl file.iri
                        , label =
                            file.name
                                |> Maybe.withDefault "Filename missing"
                                |> verbatim
                        }
                            |> TableCell.download
                    )
                |> Maybe.withDefault TableCell.empty


{-| -}
help : Config e f m Model Msg -> C -> Report -> Model -> List Fluent
help _ c report (Model data) =
    report.results
        |> Report.validationResultsFor
            (Report.emptySelector
                |> Maybe.apply Report.selectFocusNode (Rdf.toIri data.nodeFocus)
                |> Report.selectPath data.translated.path
            )
        |> List.filterMap (helpFromValidationResult c data)


helpFromValidationResult : C -> Data -> ValidationResult -> Maybe Fluent
helpFromValidationResult c data (ValidationResult { constraintComponent, message }) =
    let
        name =
            data.translated.names
                |> List.head
                |> Maybe.map (Localize.withinSentenceWithArticleIndefinite c)
                |> Maybe.withDefault "a value"
    in
    case constraintComponent of
        MinCountConstraintComponent ->
            Just (verbatim ("Enter " ++ name))

        _ ->
            Just (verbatim (Localize.verbatim c message ++ "."))


{-| -}
summary : Config e f m Model Msg -> C -> Model -> List Fluent
summary _ _ (Model data) =
    case Maybe.andThen .name data.file of
        Nothing ->
            []

        Just name ->
            [ Fluent.verbatim name ]



-- OUTPUT


{-| -}
encoders : Config e f m Model Msg -> C -> Model -> List Encode.PropertyEncoder
encoders _ _ (Model data) =
    case data.file of
        Nothing ->
            []

        Just file ->
            [ Encode.property data.translated.path
                (Encode.node file.iri
                    (case file.name of
                        Nothing ->
                            []

                        Just fileName ->
                            -- FIXME Is it ok to also serialize `file.name`
                            -- into clipboard?
                            [ Encode.predicate RDFS.label
                                (Encode.literal (Rdf.string fileName))
                            ]
                    )
                )
            ]



-- INFO


{-| -}
toNodeFocus : Model -> BlankNodeOrIri
toNodeFocus (Model data) =
    data.nodeFocus


{-| -}
toPath : Model -> Path
toPath (Model data) =
    data.path


{-| -}
isEmpty : Config e f m Model Msg -> Model -> Bool
isEmpty _ (Model data) =
    Maybe.isNothing data.file


{-| -}
dragging : Config e f m Model Msg -> Model -> Bool
dragging =
    \_ _ -> False


{-| -}
log : Config e f m Model Msg -> Model -> Pretty.Doc t
log _ _ =
    Pretty.string "File Upload"



-- DECODE


decoder : C -> Shared -> Translated.DataFileUpload -> Decoder Decoded.NamedFile
decoder _ _ translated =
    Decode.property translated.path decoderValue


decoderValue : Decoder Decoded.NamedFile
decoderValue =
    Decode.succeed Decoded.NamedFile
        |> Decode.custom Decode.iri
        |> Decode.optional (rdfs "label") (Decode.map Just Decode.string) Nothing
