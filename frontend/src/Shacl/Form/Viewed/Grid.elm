module Shacl.Form.Viewed.Grid exposing
    ( Model
    , init, initWith
    , Msg, update, expandAll
    , subscriptions
    , view, viewFullscreen, card, columns, help, summary
    , encoders
    , toNodeFocus, toPath, isEmpty, dragging, log
    )

{-|


# Model

@docs Model


# Init

@docs init, initWith


# Update

@docs Msg, update, expandAll


# Subscriptions

@docs subscriptions


# View

@docs view, viewFullscreen, card, columns, help, summary


# Output

@docs encoders


# Info

@docs toNodeFocus, toPath, isEmpty, dragging, log

-}

import Accessibility.Keyboard as Keyboard
import Api
import Browser.Dom
import Browser.Events
import Dict exposing (Dict)
import Element
    exposing
        ( Color
        , Element
        , alignBottom
        , alignRight
        , alignTop
        , centerX
        , centerY
        , clip
        , clipX
        , column
        , el
        , fill
        , height
        , htmlAttribute
        , inFront
        , maximum
        , mouseDown
        , mouseOver
        , moveDown
        , none
        , padding
        , paddingEach
        , paddingXY
        , paragraph
        , px
        , row
        , scrollbarX
        , scrollbarY
        , spacing
        , text
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Extra as Element
    exposing
        ( colorToRgba
        , disabled
        , pointerEvents
        , stopPropagationOnClick
        , style
        , tabindex
        )
import Element.Font as Font
import Element.Input as Input
import Fluent exposing (Fluent, verbatim)
import Html.Events
import Json.Decode as JDecode
import List
import List.Extra as List
import List.NonEmpty as NonEmpty
import Maybe.Apply as Maybe
import Maybe.Extra as Maybe
import Pretty
import Process
import Rdf exposing (BlankNodeOrIri, Iri, StringOrLangString)
import Rdf.Encode as Encode exposing (PropertyEncoder)
import Rdf.Graph exposing (Seed)
import Rdf.Namespaces exposing (a)
import Rdf.Namespaces.RDFS as RDFS
import Set exposing (Set)
import Shacl.Clipboard as Clipboard
import Shacl.Form.Breadcrumbs as Breadcrumbs exposing (Breadcrumbs, Path)
import Shacl.Form.Context exposing (C)
import Shacl.Form.Decoded as Decoded
import Shacl.Form.Effect as Effect exposing (Effect)
import Shacl.Form.Grouped as Grouped
import Shacl.Form.Localize as Localize
import Shacl.Form.Shared exposing (Config, Shared)
import Shacl.Form.Ui.Container as Container exposing (Container)
import Shacl.Form.Updated as Updated exposing (Updated)
import Shacl.Report as Report exposing (ConstraintComponent(..), Report, ValidationResult(..))
import Task
import Tuple.Extra as Tuple
import Ui.Atom.Button as Button exposing (button)
import Ui.Atom.ButtonIcon as ButtonIcon exposing (buttonIcon)
import Ui.Atom.Icon as Icon
import Ui.Atom.Tooltip as Tooltip
import Ui.Molecule.Table as Table
import Ui.Theme.Color as Color
    exposing
        ( error
        , outlineVariant
        , surface
        , surfaceContainer
        , surfaceContainerHigh
        , surfaceContainerHighest
        , surfaceContainerLow
        )
import Ui.Theme.Dimensions as Dimensions
import Ui.Theme.Elevation as Elevation
import Ui.Theme.Input as Input
import Ui.Theme.Typography
    exposing
        ( body1
        , h5
        , h6
        )



-- MODEL


{-| -}
type Model f
    = Model (Data f)


type alias Data f =
    { path : Path
    , breadcrumbs : Breadcrumbs
    , grouped : Grouped.DataGrid
    , nodeFocus : BlankNodeOrIri
    , iriDocument : Iri

    --
    , gridCells : GridCells
    , instances : Dict IndexGrid (Instance f)
    , editing : Maybe IndexGrid
    , cellFormHeight : Maybe Float
    , rowHeadersWidth : Maybe Float
    , hoveredRow : Maybe Int
    , hoveredColumn : Maybe Int
    , hoveredCell : Maybe IndexGrid
    , focusedCell : Maybe IndexGrid
    , tabIndexZeroCell : IndexGrid
    , selectedCells : Set IndexGrid
    , formCellViewer : Grouped.Field
    , formCellEditor : Grouped.Field
    , fullscreen : Bool
    , mouseSelection : MouseSelection
    }


type alias GridCells =
    { gridColumns : List GridColumn
    , gridRows : List GridRow
    }


type alias GridRow =
    { iri : Iri
    , label : StringOrLangString
    }


type alias GridColumn =
    GridRow


{-| Model cells are indexed row-first, ie. `( indexRow, indexColumn )`, except
in breadcrumbs, where they are indexed `[ indexColumn, indexRow ]`.
-}
type alias IndexGrid =
    ( Int, Int )


type alias Instance form =
    { formEditor : form
    , formViewer : form
    }


{-| `MouseSelection` maintains a `count` so that events can be ordered due to
the asynchronous nature of `UserDraggedGrid` (uses `Cmd`s)
-}
type MouseSelection
    = MouseSelection Int MouseSelectionState


type MouseSelectionState
    = MouseSelectionStateInactive
    | MouseSelectionStateActive
        { start :
            { x : Float
            , y : Float
            }
        , end :
            { x : Float
            , y : Float
            }
        }



-- INIT


{-| -}
init :
    Config e f m (Model f) (Msg f m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Breadcrumbs
    -> Decoded.DataGrid
    -> Updated (Model f) m
init config populateDefaultValues c shared seed breadcrumbs decoded =
    let
        formCellViewer =
            Maybe.withDefault decoded.grouped.field decoded.grouped.dataEditorGrid.cellViewer

        formCellEditor =
            Maybe.withDefault decoded.grouped.field decoded.grouped.dataEditorGrid.cellEditor

        modelFromInstances instances =
            Model
                { path = decoded.path
                , breadcrumbs = breadcrumbs
                , grouped = decoded.grouped
                , nodeFocus = decoded.nodeFocus
                , iriDocument = shared.iri
                , gridCells = decoded.gridCells
                , instances = instances
                , editing = Nothing
                , cellFormHeight = Nothing
                , rowHeadersWidth = Nothing
                , hoveredRow = Nothing
                , hoveredColumn = Nothing
                , hoveredCell = Nothing
                , focusedCell = Nothing
                , tabIndexZeroCell = ( floor 0, floor 0 )
                , selectedCells = Set.empty
                , formCellViewer = formCellViewer
                , formCellEditor = formCellEditor
                , fullscreen = False
                , mouseSelection = MouseSelection 0 MouseSelectionStateInactive
                }
    in
    decoded.instances
        |> Dict.toList
        |> List.foldl (initInstance config populateDefaultValues c shared breadcrumbs)
            { model = []
            , effect = Effect.none
            , seed = seed
            , needsPersisting = False
            }
        |> Updated.map (Dict.fromList >> modelFromInstances)


initInstance :
    Config e f m (Model f) (Msg f m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Breadcrumbs
    -> ( ( Int, Int ), Decoded.InstanceGrid )
    -> Updated (List ( ( Int, Int ), Instance f )) m
    -> Updated (List ( ( Int, Int ), Instance f )) m
initInstance config populateDefaultValues c shared breadcrumbs ( ( row, column ), instance ) updated =
    let
        breadcrumbsNested =
            breadcrumbs
                |> increaseDepthCollection

        updatedViewer =
            -- FIXME We want to have a fieldViewer here
            instance.field
                |> config.init populateDefaultValues c shared updated.seed breadcrumbsNested

        updatedEditor =
            -- FIXME We want to have a fieldEditor here
            instance.field
                |> config.init populateDefaultValues c shared updated.seed breadcrumbsNested
    in
    -- FIXME Adjust config.form, to turn of iri minting
    -- and blank node seeding
    { model =
        ( ( row, column )
        , { formViewer = updatedViewer.model
          , formEditor = updatedEditor.model
          }
        )
            :: updated.model
    , effect = Effect.batch [ updatedEditor.effect, updated.effect ]
    , seed = updatedEditor.seed
    , needsPersisting = updatedEditor.needsPersisting || updated.needsPersisting
    }


{-| -}
initWith :
    Config e f m (Model f) (Msg f m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Model f
    -> Result e (Maybe (Updated (Model f) msg))
initWith _ _ _ _ _ _ =
    -- FIXME Since we allow blank nodes as focus nodes for each grid item, it
    -- is not possible to partially init the components from a new graph, as in
    -- the new graph the blank nodes might have changed and cannot be mapped
    -- any more.  We therefore do not update the grid items, which could lead
    -- to stale UI.  We could add some indicator in the UI at some point.
    Ok Nothing


increaseDepthCollection : Breadcrumbs -> Breadcrumbs
increaseDepthCollection breadcrumbs =
    { breadcrumbs | depthCollection = breadcrumbs.depthCollection + 1 }



-- UPDATE


{-| -}
type Msg f m
    = NoOp
    | UserPressedEditSelectedCells
    | UserPressedClearSelection
    | UserPressedEditGridCell IndexGrid
    | ApiReturnedDecoded IndexGrid (Api.Response Decoded.Field)
    | UserHoveredGridRow (Maybe Int)
    | UserHoveredGridColumn (Maybe Int)
    | UserDismissedGridForm
    | UserHoveredGridCell (Maybe IndexGrid)
    | UserFocusedGridCell (Maybe IndexGrid)
    | UserInteractedFocusGridCell Keyboard.IntentFocus
    | UserClickedGridCell IndexGrid
    | UserCtrlClickedGridCell IndexGrid
    | UserShiftClickedGridCell IndexGrid
    | UserClickedGridRow Int
    | UserClickedGridColumn Int
    | UserInteractedSelectionGridCell Keyboard.IntentSelection
    | UserPressedOpenFullscreen
    | UserPressedCloseFullscreen
    | UserCopiedGridCell (Set IndexGrid)
    | UserDraggedGrid
        Path
        (Maybe
            (Result
                Browser.Dom.Error
                { x : Float
                , y : Float
                , width : Float
                , height : Float
                }
            )
        )
        Int
        { x : Float
        , y : Float
        }
    | UserReleasedGrid Int
    | Scrolled
    | MsgForm Int Int m


{-| -}
update :
    Config e f m (Model f) (Msg f m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Msg f m
    -> Model f
    -> Updated (Model f) m
update config populateDefaultValues c shared seed msg ((Model data) as model) =
    let
        noResult =
            { model = model
            , effect = Effect.none
            , seed = seed
            , needsPersisting = False
            }
    in
    case msg of
        NoOp ->
            noResult

        UserPressedEditSelectedCells ->
            if Set.isEmpty data.selectedCells then
                noResult

            else
                let
                    indexGrid =
                        data.selectedCells
                            |> Set.toList
                            |> List.maximum
                            |> Maybe.withDefault ( 0, 0 )
                in
                { data
                    | editing = Just indexGrid -- FIXME Delay this until all instances have been created
                    , focusedCell = Just indexGrid
                }
                    |> Model
                    |> Updated.from seed
                    |> Updated.withEffect
                        (data.selectedCells
                            |> Set.toList
                            |> List.map
                                (\selectedCell ->
                                    data.grouped.field
                                        |> Effect.CreateField
                                            (ApiReturnedDecoded selectedCell)
                                            shared.preview
                                            data.path
                                            (Decoded.MintIri
                                                { nodeFocus = data.nodeFocus
                                                , class = NonEmpty.head data.grouped.classes
                                                }
                                            )
                                        |> Effect.map config.toMsg
                                )
                            |> Effect.batch
                        )

        UserPressedClearSelection ->
            { model =
                Model
                    { data
                        | selectedCells = Set.empty
                        , editing = Nothing
                    }
            , effect = Effect.none
            , seed = seed
            , needsPersisting = False
            }

        UserPressedEditGridCell indexGrid ->
            { data
                | editing = Just indexGrid -- FIXME Delay this until all instances have been created
                , focusedCell = Just indexGrid
            }
                |> Model
                |> Updated.from seed
                |> Updated.withEffect
                    (data.grouped.field
                        |> Effect.CreateField
                            (ApiReturnedDecoded indexGrid)
                            shared.preview
                            -- FIXME Mint from all classes
                            data.path
                            (Decoded.MintIri
                                { nodeFocus = data.nodeFocus
                                , class = NonEmpty.head data.grouped.classes
                                }
                            )
                        |> Effect.map config.toMsg
                    )

        ApiReturnedDecoded _ (Err error) ->
            model
                |> Updated.from seed
                |> Updated.withEffect (Effect.addToastError error)

        ApiReturnedDecoded (( gridRow, gridColumn ) as indexGrid) (Ok field) ->
            let
                breadcrumbsNested =
                    data.breadcrumbs
                        |> increaseDepthCollection

                insertInstance form =
                    Model
                        { data
                            | instances =
                                Dict.insert indexGrid
                                    { formEditor = form
                                    , formViewer = form
                                    }
                                    data.instances
                        }
            in
            config.init populateDefaultValues c shared seed breadcrumbsNested field
                |> Updated.map insertInstance
                |> Updated.needsPersisting
                |> Updated.mapEffect (MsgForm gridRow gridColumn >> config.toMsg)

        UserCopiedGridCell selectedCells ->
            { model = model
            , effect =
                selectedCells
                    |> Set.toList
                    |> List.filterMap (encoderInstance config c data)
                    |> Clipboard.serialize data.nodeFocus seed
                    |> Effect.copyToClipboard
            , seed = seed
            , needsPersisting = False
            }

        UserHoveredGridRow hoveredRow ->
            { model = Model { data | hoveredRow = hoveredRow }
            , effect = Effect.none
            , seed = seed
            , needsPersisting = False
            }

        UserHoveredGridColumn hoveredColumn ->
            { model = Model { data | hoveredColumn = hoveredColumn }
            , effect = Effect.none
            , seed = seed
            , needsPersisting = False
            }

        UserDismissedGridForm ->
            { model = Model { data | editing = Nothing }
            , effect = Effect.none
            , seed = seed
            , needsPersisting = False
            }

        UserHoveredGridCell hoveredCell ->
            { model = Model { data | hoveredCell = hoveredCell }
            , effect = Effect.none
            , seed = seed
            , needsPersisting = False
            }

        UserFocusedGridCell focusedCell ->
            let
                dataUpdated =
                    { data
                        | focusedCell = focusedCell
                        , tabIndexZeroCell = Maybe.withDefault data.tabIndexZeroCell focusedCell
                    }
            in
            { model = Model dataUpdated
            , effect = synchronizeGridDomState config data.nodeFocus data dataUpdated
            , seed = seed
            , needsPersisting = False
            }

        UserInteractedFocusGridCell intentFocus ->
            let
                indexGridMax =
                    ( List.length data.gridCells.gridRows - 1
                    , List.length data.gridCells.gridColumns - 1
                    )

                focusedCellUpdated =
                    Maybe.map
                        (Keyboard.applyIntentFocus
                            indexGridMax
                            intentFocus
                        )
                        data.focusedCell

                dataUpdated =
                    { data
                        | focusedCell = focusedCellUpdated
                        , tabIndexZeroCell = Maybe.withDefault data.tabIndexZeroCell focusedCellUpdated
                    }
            in
            { model = Model dataUpdated
            , effect = synchronizeGridDomState config data.nodeFocus data dataUpdated
            , seed = seed
            , needsPersisting = False
            }

        UserClickedGridCell indexGrid ->
            let
                dataUpdated =
                    { data
                        | editing =
                            if data.editing == Just indexGrid then
                                data.editing

                            else
                                Nothing
                        , focusedCell = Just indexGrid
                        , selectedCells = Set.singleton indexGrid
                    }
            in
            { model = Model dataUpdated
            , effect = synchronizeGridDomState config data.nodeFocus data dataUpdated
            , seed = seed
            , needsPersisting = False
            }

        UserCtrlClickedGridCell indexGrid ->
            if not (Set.member indexGrid data.selectedCells) then
                let
                    dataUpdated =
                        { data
                            | editing = Nothing
                            , focusedCell = Just indexGrid
                            , selectedCells = Set.insert indexGrid data.selectedCells
                        }
                in
                { model = Model dataUpdated
                , effect = synchronizeGridDomState config data.nodeFocus data dataUpdated
                , seed = seed
                , needsPersisting = False
                }

            else
                let
                    dataUpdated =
                        { data
                            | editing = Nothing
                            , focusedCell = Just indexGrid
                            , selectedCells = Set.remove indexGrid data.selectedCells
                        }
                in
                { model = Model dataUpdated
                , effect = synchronizeGridDomState config data.nodeFocus data dataUpdated
                , seed = seed
                , needsPersisting = False
                }

        UserShiftClickedGridCell (( indexRow, indexColumn ) as indexGrid) ->
            let
                indicesGrid =
                    Maybe.unwrap Set.empty
                        (\( indexRowFocused, indexColumnFocused ) ->
                            let
                                indexRowMin =
                                    min indexRow indexRowFocused

                                indexRowMax =
                                    max indexRow indexRowFocused

                                indexColumnMin =
                                    min indexColumn indexColumnFocused

                                indexColumnMax =
                                    max indexColumn indexColumnFocused
                            in
                            Set.fromList
                                (List.concatMap
                                    (\indexRowSelected ->
                                        List.map
                                            (\indexColumnSelected ->
                                                ( indexRowSelected, indexColumnSelected )
                                            )
                                            (List.range indexColumnMin indexColumnMax)
                                    )
                                    (List.range indexRowMin indexRowMax)
                                )
                        )
                        data.focusedCell

                dataUpdated =
                    { data
                        | editing = Nothing
                        , selectedCells = indicesGrid
                        , focusedCell = Just indexGrid
                    }
            in
            { model = Model dataUpdated
            , effect = synchronizeGridDomState config data.nodeFocus data dataUpdated
            , seed = seed
            , needsPersisting = False
            }

        UserClickedGridRow indexRow ->
            let
                indexColumnMax =
                    List.length data.gridCells.gridColumns - 1

                focusedCell =
                    ( indexRow, 0 )

                selectedCells =
                    Set.fromList
                        (List.map
                            (\indexColumn ->
                                ( indexRow, indexColumn )
                            )
                            (List.range 0 indexColumnMax)
                        )

                dataUpdated =
                    { data
                        | selectedCells = selectedCells
                        , focusedCell = Just focusedCell
                    }
            in
            { model = Model dataUpdated
            , effect = synchronizeGridDomState config data.nodeFocus data dataUpdated
            , seed = seed
            , needsPersisting = False
            }

        UserClickedGridColumn indexColumn ->
            let
                indexRowMax =
                    List.length data.gridCells.gridRows - 1

                focusedCell =
                    ( 0, indexColumn )

                selectedCells =
                    Set.fromList
                        (List.map
                            (\indexRow ->
                                ( indexRow, indexColumn )
                            )
                            (List.range 0 indexRowMax)
                        )

                dataUpdated =
                    { data
                        | selectedCells = selectedCells
                        , focusedCell = Just focusedCell
                    }
            in
            { model = Model dataUpdated
            , effect = synchronizeGridDomState config data.nodeFocus data dataUpdated
            , seed = seed
            , needsPersisting = False
            }

        UserInteractedSelectionGridCell intentSelection ->
            let
                indexGridMax =
                    ( List.length data.gridCells.gridRows - 1
                    , List.length data.gridCells.gridColumns - 1
                    )

                ( focusedCellUpdated, selectedCellsUpdated ) =
                    Maybe.unwrap ( Nothing, data.selectedCells )
                        (\focusedCell ->
                            Keyboard.applyIntentSelection
                                indexGridMax
                                intentSelection
                                focusedCell
                                data.selectedCells
                        )
                        data.focusedCell

                dataUpdated =
                    { data
                        | focusedCell = Maybe.orElse data.focusedCell focusedCellUpdated
                        , selectedCells = selectedCellsUpdated
                    }
            in
            { model = Model dataUpdated
            , effect = synchronizeGridDomState config data.nodeFocus data dataUpdated
            , seed = seed
            , needsPersisting = False
            }

        UserPressedOpenFullscreen ->
            { model =
                Model
                    { data
                        | fullscreen = True
                        , editing = Nothing
                    }
            , effect = Effect.none
            , seed = seed
            , needsPersisting = False
            }

        UserPressedCloseFullscreen ->
            { model =
                Model
                    { data
                        | fullscreen = False
                        , editing = Nothing
                    }
            , effect = Effect.none
            , seed = seed
            , needsPersisting = False
            }

        UserDraggedGrid pathFull Nothing count point ->
            { model = model
            , effect =
                Effect.fromCmd
                    (Task.attempt
                        (\element ->
                            UserDraggedGrid pathFull (Just element) count point
                        )
                        -- sleep 100ms to initiate mouse selection
                        (Process.sleep 100
                            |> Task.andThen
                                (\_ ->
                                    Task.map2
                                        (\{ viewport } { element } ->
                                            { x = element.x - viewport.x
                                            , y = element.y - viewport.y
                                            , width = element.width
                                            , height = element.height
                                            }
                                        )
                                        (Browser.Dom.getViewportOf (idGridScrollContainer data.nodeFocus))
                                        (Browser.Dom.getElement (idGridScrollContainer data.nodeFocus))
                                )
                        )
                    )
                    |> Effect.map config.toMsg
            , seed = seed
            , needsPersisting = False
            }

        UserDraggedGrid _ (Just (Err _)) _ _ ->
            { model = model
            , effect = Effect.none
            , seed = seed
            , needsPersisting = False
            }

        UserDraggedGrid _ (Just (Ok viewport)) count pointRaw ->
            case data.mouseSelection of
                MouseSelection countCurrent mouseSelectionState ->
                    if count /= countCurrent then
                        { model = model
                        , effect = Effect.none
                        , seed = seed
                        , needsPersisting = False
                        }

                    else
                        let
                            point =
                                { x = clamp 0 viewport.width (pointRaw.x - viewport.x)
                                , y = clamp 0 viewport.height (pointRaw.y - viewport.y)
                                }
                        in
                        case mouseSelectionState of
                            MouseSelectionStateInactive ->
                                { model =
                                    Model
                                        { data
                                            | mouseSelection =
                                                MouseSelection countCurrent
                                                    (MouseSelectionStateActive
                                                        { start = point
                                                        , end = point
                                                        }
                                                    )
                                        }
                                , effect = Effect.none
                                , seed = seed
                                , needsPersisting = False
                                }

                            MouseSelectionStateActive { start } ->
                                { model =
                                    Model
                                        { data
                                            | mouseSelection =
                                                MouseSelection countCurrent
                                                    (MouseSelectionStateActive
                                                        { start = start
                                                        , end = point
                                                        }
                                                    )
                                        }
                                , effect = Effect.none
                                , seed = seed
                                , needsPersisting = False
                                }

        UserReleasedGrid count ->
            case data.mouseSelection of
                MouseSelection countCurrent _ ->
                    if count /= countCurrent then
                        { model = model
                        , effect = Effect.none
                        , seed = seed
                        , needsPersisting = False
                        }

                    else
                        let
                            indexRowMax =
                                List.length data.gridCells.gridRows - 1

                            indexColumnMax =
                                List.length data.gridCells.gridColumns - 1

                            selectedCells =
                                List.filter (selectedByMouseSelection data.mouseSelection)
                                    (List.sort
                                        (List.concatMap
                                            (\indexRow ->
                                                List.map
                                                    (\indexColumn ->
                                                        ( indexRow, indexColumn )
                                                    )
                                                    (List.range 0 indexColumnMax)
                                            )
                                            (List.range 0 indexRowMax)
                                        )
                                    )

                            focusedCell =
                                List.head selectedCells
                        in
                        case focusedCell of
                            Nothing ->
                                -- clear the mouse selection without updating the selection, as the trivial (0x0px) mouse selection indicates a click
                                { model = Model { data | mouseSelection = MouseSelection (countCurrent + 1) MouseSelectionStateInactive }
                                , effect = Effect.none
                                , seed = seed
                                , needsPersisting = False
                                }

                            Just _ ->
                                let
                                    dataUpdated =
                                        { data
                                            | mouseSelection =
                                                MouseSelection (countCurrent + 1)
                                                    MouseSelectionStateInactive
                                            , focusedCell = List.head selectedCells
                                            , selectedCells = Set.fromList selectedCells
                                        }
                                in
                                { model = Model dataUpdated
                                , effect = synchronizeGridDomState config data.nodeFocus data dataUpdated
                                , seed = seed
                                , needsPersisting = False
                                }

        Scrolled ->
            noResult

        MsgForm gridRow gridColumn msgForm ->
            case Dict.get ( gridRow, gridColumn ) data.instances of
                Nothing ->
                    noResult

                Just instance ->
                    config.update c shared seed msgForm instance.formEditor
                        |> Updated.map
                            (\formUpdated ->
                                Model
                                    { data
                                        | instances =
                                            Dict.insert ( gridRow, gridColumn )
                                                { instance | formEditor = formUpdated }
                                                data.instances
                                    }
                            )
                        |> Updated.mapEffect (MsgForm gridRow gridColumn >> config.toMsg)


synchronizeGridDomState :
    Config e f m (Model f) (Msg f m)
    -> BlankNodeOrIri
    -> Data f
    -> Data f
    -> Effect m
synchronizeGridDomState config nodeFocus data dataUpdated =
    if dataUpdated.focusedCell /= data.focusedCell then
        Effect.batch
            (Maybe.values
                [ Maybe.map
                    (Effect.focus << idGridCell nodeFocus)
                    dataUpdated.focusedCell
                , Maybe.map
                    (Effect.fromCmd << scrollGridCellIntoView nodeFocus)
                    dataUpdated.focusedCell
                ]
            )
            |> Effect.map config.toMsg

    else
        Effect.none


scrollGridCellIntoView : BlankNodeOrIri -> IndexGrid -> Cmd (Msg f m)
scrollGridCellIntoView focusNode focused =
    Task.andThen
        (\{ viewportContainer, elementContainer, elementFocused, rowHeaders } ->
            let
                rowHeadersWidth =
                    rowHeaders.element.width

                viewportLeft =
                    viewportContainer.viewport.x

                viewportRight =
                    viewportLeft + viewportContainer.viewport.width

                -- FIXME computes the x position of the element within viewport, independent of scroll. this is arcane. don't try to understand this. don't.
                elementLeft =
                    elementFocused.element.x
                        - elementContainer.element.x
                        + viewportContainer.viewport.x

                elementRight =
                    elementLeft + elementWidth

                elementWidth =
                    elementFocused.element.width
            in
            -- ignoring fractional parts for viewport testing improves robustness
            if (floor (viewportLeft + rowHeadersWidth) <= floor elementLeft) && (floor elementRight <= floor viewportRight) then
                Task.succeed ()

            else
                let
                    -- final scroll position if element touches the left (right) edge of the viewport
                    ( scrollXMin, scrollXMax ) =
                        ( elementLeft - rowHeadersWidth, elementRight - viewportContainer.viewport.width )

                    scrollDistanceMin =
                        abs (viewportContainer.viewport.x + rowHeadersWidth - scrollXMin)

                    scrollDistanceMax =
                        abs (viewportContainer.viewport.x - scrollXMax)
                in
                Browser.Dom.setViewportOf (idGridScrollContainer focusNode)
                    -- scroll the element in view by moving the viewport minimally
                    (if scrollDistanceMin < scrollDistanceMax then
                        scrollXMin

                     else
                        scrollXMax
                    )
                    0
        )
        (Task.map4
            (\viewportContainer elementContainer elementFocused rowHeaders ->
                { viewportContainer = viewportContainer
                , elementContainer = elementContainer
                , elementFocused = elementFocused
                , rowHeaders = rowHeaders
                }
            )
            (Browser.Dom.getViewportOf (idGridScrollContainer focusNode))
            (Browser.Dom.getElement (idGridScrollContainer focusNode))
            (Browser.Dom.getElement (idGridCell focusNode focused))
            (Browser.Dom.getElement (idGridRowHeaders focusNode))
        )
        |> Task.attempt (\_ -> Scrolled)


{-| -}
expandAll : Config e f m (Model f) (Msg f m) -> Model f -> Model f
expandAll config (Model data) =
    Model
        { data
            | instances =
                Dict.map
                    (\_ instance ->
                        { instance
                            | formEditor = config.expandAll instance.formEditor
                            , formViewer = config.expandAll instance.formViewer
                        }
                    )
                    data.instances
        }



-- SUBSCRIPTIONS


{-| -}
subscriptions : Config e f m (Model f) (Msg f m) -> Model f -> Sub m
subscriptions config (Model data) =
    Sub.batch
        [ data.instances
            |> Dict.values
            |> List.map (.formEditor >> config.subscriptions)
            |> Sub.batch
        , case data.mouseSelection of
            MouseSelection count _ ->
                -- we register this handler unconditionally, as mouseup can be
                -- raised in the same tick as mousedown
                Browser.Events.onMouseUp (JDecode.succeed (UserReleasedGrid count))
                    |> Sub.map config.toMsg
        ]


{-| -}
view :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Model f
    -> Maybe (Container m)
view config =
    viewGridHelp config False


{-| -}
viewFullscreen :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Model f
    -> Maybe (Element m)
viewFullscreen config c report isDragging ((Model data) as model) =
    if data.fullscreen then
        viewGridHelp config True c report isDragging model
            |> Maybe.map Container.getElement
            |> Maybe.or
                (data.editing
                    |> Maybe.andThen (\indexGrid -> Maybe.map (Tuple.pair indexGrid) (Dict.get indexGrid data.instances))
                    |> Maybe.andThen
                        (\( indexGrid, instance ) ->
                            instance.formEditor
                                |> config.fullscreen c report isDragging
                                |> Maybe.map
                                    (Element.map
                                        (MsgForm (Tuple.first indexGrid) (Tuple.second indexGrid)
                                            >> config.toMsg
                                        )
                                    )
                        )
                )

    else
        Nothing


viewGridHelp :
    Config e f m (Model f) (Msg f m)
    -> Bool
    -> C
    -> Report
    -> Bool
    -> Model f
    -> Maybe (Container m)
viewGridHelp config fullscreen c report isDragging (Model ({ breadcrumbs } as data)) =
    let
        toHeading =
            if breadcrumbs.depthGroup == 0 then
                h5

            else
                h6

        viewHeader attrs element =
            element
                |> el
                    [ width fill
                    , Font.center
                    , centerY
                    , paddingXY 8 0
                    ]
                |> el
                    ([ Background.color (backgroundColor breadcrumbs)
                     , Border.solid
                     , Border.width 1
                     , Border.color outlineVariant
                     , Border.rounded 12
                     , Element.pointer
                     ]
                        ++ attrs
                    )
                |> gridFocusBorder False

        viewCornerHeader =
            none
                |> el
                    [ height (px configGrid.heightColumnHeader)
                    , width fill
                    ]
                |> gridFocusBorder False

        viewColumnHeader indexColumn column =
            let
                hovered =
                    Just indexColumn == data.hoveredColumn
            in
            text (Localize.forLabel c column.label)
                |> viewHeader
                    (List.concat
                        [ [ width (px configGrid.widthColumn)
                          , height (px configGrid.heightColumnHeader)
                          , Events.onMouseEnter (config.toMsg (UserHoveredGridColumn (Just indexColumn)))
                          , Events.onMouseLeave (config.toMsg (UserHoveredGridColumn Nothing))
                          , Events.onClick (config.toMsg (UserClickedGridColumn indexColumn))
                          , inFront (viewGridSelectionIndicator data hovered (IndicatorColumn indexColumn))
                          ]
                        , if hovered then
                            [ Elevation.z1 ]

                          else
                            []
                        ]
                    )

        viewRowHeader indexRow gridRow =
            let
                hovered =
                    Just indexRow == data.hoveredRow
            in
            [ viewGridSelectionIndicator data hovered (IndicatorRow indexRow)
            , text (Localize.forLabel c gridRow.label)
                |> el
                    [ width fill
                    , Font.center
                    , centerY
                    , paddingXY 4 0
                    ]
            ]
                |> row
                    ([ Background.color (backgroundColor breadcrumbs)
                     , Border.solid
                     , Border.width 1
                     , Border.color outlineVariant
                     , Border.rounded 12
                     , Element.pointer
                     , height (px configGrid.heightRow)
                     , width fill
                     , Events.onMouseEnter (config.toMsg (UserHoveredGridRow (Just indexRow)))
                     , Events.onMouseLeave (config.toMsg (UserHoveredGridRow Nothing))
                     , Events.onClick (config.toMsg (UserClickedGridRow indexRow))
                     ]
                        ++ (if hovered then
                                [ Elevation.z1 ]

                            else
                                []
                           )
                    )
                |> gridFocusBorder False

        tabIndexZeroCell =
            let
                indexTabLast =
                    ( List.length data.gridCells.gridRows - 1
                    , List.length data.gridCells.gridColumns - 1
                    )
            in
            if
                (( 0, 0 ) <= data.tabIndexZeroCell)
                    && (data.tabIndexZeroCell <= indexTabLast)
            then
                data.tabIndexZeroCell

            else
                ( 0, 0 )

        viewIconFullscreenToggle =
            [ case data.editing of
                Nothing ->
                    case Set.size data.selectedCells of
                        0 ->
                            []

                        1 ->
                            [ "Copy cell"
                                |> verbatim
                                |> button (config.toMsg (UserCopiedGridCell data.selectedCells))
                                |> Button.withFormat Button.Outlined
                                |> Button.withLeadingIcon Icon.ContentCopy
                                |> Button.view
                            , "Edit cell"
                                |> verbatim
                                |> button (config.toMsg UserPressedEditSelectedCells)
                                |> Button.withFormat Button.Outlined
                                |> Button.withLeadingIcon Icon.Edit
                                |> Button.view
                            , "Clear selection"
                                |> verbatim
                                |> button (config.toMsg UserPressedClearSelection)
                                |> Button.withFormat Button.Text
                                |> Button.view
                            ]

                        countSelectedCells ->
                            [ [ "Copy "
                              , String.fromInt countSelectedCells
                              , " cells"
                              ]
                                |> String.concat
                                |> verbatim
                                |> button (config.toMsg (UserCopiedGridCell data.selectedCells))
                                |> Button.withFormat Button.Outlined
                                |> Button.withLeadingIcon Icon.ContentCopy
                                |> Button.view
                            , [ "Edit "
                              , String.fromInt countSelectedCells
                              , " cells"
                              ]
                                |> String.concat
                                |> verbatim
                                |> button (config.toMsg UserPressedEditSelectedCells)
                                |> Button.withFormat Button.Outlined
                                |> Button.withLeadingIcon Icon.Edit
                                |> Button.view
                            , "Clear selection"
                                |> verbatim
                                |> button (config.toMsg UserPressedClearSelection)
                                |> Button.withFormat Button.Text
                                |> Button.view
                            ]

                Just _ ->
                    []
            , [ if fullscreen then
                    verbatim "Close"
                        |> button (config.toMsg UserPressedCloseFullscreen)
                        |> Button.withFormat Button.Text
                        |> Button.withLeadingIcon Icon.FullscreenExit
                        |> Button.view

                else
                    verbatim "Open fullscreen"
                        |> button (config.toMsg UserPressedOpenFullscreen)
                        |> Button.withFormat Button.Text
                        |> Button.withLeadingIcon Icon.Fullscreen
                        |> Button.view
              ]
            ]
                |> List.concat
                |> Element.wrappedRow
                    [ alignRight
                    , spacing 8
                    , width fill
                    ]

        viewHeading =
            [ data.grouped.names
                |> List.head
                |> Maybe.map (Localize.forLabelPlural c)
                |> Maybe.withDefault "Missing label"
                |> verbatim
                |> toHeading
                |> List.singleton
                |> paragraph
                    [ width fill
                    , alignTop
                    , paddingXY 0 8
                    ]
            , viewIconFullscreenToggle
            ]
                |> row
                    [ width fill
                    , paddingEach
                        { top = 16
                        , bottom = 0
                        , left = 16
                        , right = 0
                        }
                    , spacing 8
                    ]

        viewValidationResult =
            Input.help (help config c report (Model data))

        viewCellsPlaceholder =
            [ "Please select a well plate."
                |> verbatim
                |> body1
            ]
                |> paragraph
                    [ centerX
                    , centerY
                    , Font.center
                    ]
                |> el
                    [ width fill
                    , height fill
                    ]

        viewCells insertSpace =
            [ [ viewCornerHeader
              , data.gridCells.gridRows
                    |> List.indexedMap viewRowHeader
                    |> insertSpaceForForm insertSpace
              ]
                |> column
                    [ Element.id (idGridRowHeaders data.nodeFocus)
                    , style "position" "sticky"
                    , style "left" "0"
                    , style "z-index" "1"
                    , if fullscreen then
                        Background.color surface

                      else
                        -- FIXME Find a way to not adjust the breadcrumbs here
                        Background.color (backgroundColor { breadcrumbs | depthCollection = breadcrumbs.depthCollection - 1 })
                    ]
            , data.gridCells.gridColumns
                |> List.indexedMap
                    (\indexColumn gridColumn ->
                        [ viewColumnHeader indexColumn gridColumn
                        , data.gridCells.gridRows
                            |> List.indexedMap
                                (\indexRow _ ->
                                    data.instances
                                        |> Dict.get ( indexRow, indexColumn )
                                        |> viewGridCell config fullscreen c report data tabIndexZeroCell ( indexRow, indexColumn )
                                )
                            |> insertSpaceForForm insertSpace
                        ]
                            |> column []
                    )
                |> row []
            ]
                -- FIXME `scrollbarX` does not work, unless wrapped in `el`, and `width` set on both elements, cf. https://github.com/mdgriffith/elm-ui/issues/321
                |> row
                    ([ scrollbarX
                     , width fill
                     , paddingEach
                        { top = 0
                        , bottom = 16
                        , left = 0
                        , right = 0
                        }
                     , alignTop
                     ]
                        ++ (-- FIXME ids are duplicate on scroll overlay
                            if
                                (data.fullscreen && fullscreen)
                                    || (not data.fullscreen && not fullscreen)
                            then
                                Element.id (idGridScrollContainer data.nodeFocus)
                                    :: viewMouseSelection

                            else
                                []
                           )
                    )
                |> el
                    [ width fill
                    , height fill
                    , clip
                    ]

        insertSpaceForForm insertSpace =
            if insertSpace then
                case data.editing of
                    Nothing ->
                        column []

                    Just ( indexRowEditing, _ ) ->
                        List.splitAt (indexRowEditing + 1)
                            >> (\( before, after ) ->
                                    [ column [ width fill ] before
                                    , el
                                        [ ((data.cellFormHeight
                                                |> Maybe.withDefault 0
                                                |> floor
                                           )
                                            + 10
                                          )
                                            |> px
                                            |> height
                                        ]
                                        none
                                    , column [ width fill ] after
                                    ]
                                        |> column []
                               )

            else
                column []

        viewCellForm showBackground =
            data.editing
                |> Maybe.andThen (\indexGrid -> Maybe.map (Tuple.pair indexGrid) (Dict.get indexGrid data.instances))
                |> Maybe.map
                    (Tuple.apply
                        (viewGridForm config
                            showBackground
                            c
                            report
                            isDragging
                            data
                            [ height fill, width fill ]
                        )
                    )

        viewCellFormPlaceholder =
            [ [ "Please select a "
              , Localize.forLabel c data.grouped.className
              , " to view and edit all of its properties."
              , "."
              ]
                |> String.join ""
                |> verbatim
                |> body1
            ]
                |> paragraph
                    [ centerX
                    , centerY
                    , Font.center
                    , paddingXY 32 16
                    ]
                |> el
                    [ width fill
                    , height fill
                    ]

        viewMouseSelection =
            let
                (MouseSelection count _) =
                    data.mouseSelection

                pointDecoder =
                    JDecode.map2
                        (\x y ->
                            { x = x
                            , y = y
                            }
                        )
                        (JDecode.field "clientX" JDecode.float)
                        (JDecode.field "clientY" JDecode.float)
            in
            List.concat
                [ [ htmlAttribute
                        (Html.Events.on "mousedown"
                            (JDecode.map (config.toMsg << UserDraggedGrid data.path Nothing count) pointDecoder)
                        )
                  ]
                , case mouseSelectionRect data.mouseSelection of
                    Nothing ->
                        []

                    Just { top, left, bottom, right } ->
                        [ htmlAttribute
                            (Html.Events.on "mousemove"
                                (JDecode.map (config.toMsg << UserDraggedGrid data.path Nothing count) pointDecoder)
                            )
                        , inFront
                            (el
                                [ paddingEach
                                    { top = round top
                                    , left = round left
                                    , bottom = 0
                                    , right = 0
                                    }
                                , pointerEvents False
                                ]
                                (el
                                    [ width (px (round (right - left)))
                                    , height (px (round (bottom - top)))
                                    , Border.dashed
                                    , Border.color Color.primary
                                    , Border.width 2
                                    ]
                                    none
                                )
                            )
                        ]
                ]
    in
    if fullscreen then
        [ [ Icon.ArrowBack
                |> buttonIcon (config.toMsg UserPressedCloseFullscreen)
                |> ButtonIcon.toElement
          , data.grouped.names
                |> List.head
                |> Maybe.map (Localize.forLabelPlural c)
                |> Maybe.withDefault "Missing label"
                |> verbatim
                |> h5
          ]
            |> row
                [ width fill
                , height (px 48)
                , spacing 8
                , paddingXY 8 0
                ]
        , viewValidationResult
        , if
            List.isEmpty data.gridCells.gridColumns
                || List.isEmpty data.gridCells.gridRows
          then
            viewCellsPlaceholder

          else
            [ viewCells False
                |> el
                    [ width fill
                    , height fill
                    , clip
                    , padding 12
                    ]
            , viewCellForm True
                |> Maybe.withDefault viewCellFormPlaceholder
                |> el
                    [ width fill
                    , paddingEach
                        { top = 16
                        , bottom = 16
                        , left = 0
                        , right = 16
                        }
                    ]
                |> el
                    [ width (maximum Dimensions.paneWidthMax fill)
                    , alignTop
                    ]
            ]
                |> row
                    [ width fill
                    , spacing 16
                    ]
        ]
            |> column
                [ width fill
                , height fill
                ]
            |> Container.expand
            |> Just

    else
        [ viewHeading
        , viewValidationResult
        , if
            List.isEmpty data.gridCells.gridColumns
                || List.isEmpty data.gridCells.gridRows
          then
            viewCellsPlaceholder

          else
            viewCells True
                |> el
                    [ width fill
                    , inFront
                        (viewCellForm True
                            |> Maybe.map
                                (\cellForm ->
                                    [ el
                                        [ data.rowHeadersWidth
                                            |> Maybe.withDefault 32
                                            |> floor
                                            |> px
                                            |> width
                                        ]
                                        none
                                    , cellForm
                                    ]
                                        |> row
                                            [ width fill
                                            , moveDown
                                                (44
                                                    + ((data.editing
                                                            |> Maybe.map Tuple.first
                                                            |> Maybe.withDefault 0
                                                            |> toFloat
                                                       )
                                                        + 1
                                                      )
                                                    * 140
                                                )
                                            , padding 5
                                            ]
                                )
                            |> Maybe.withDefault none
                        )
                    ]
        ]
            |> column
                [ width fill
                , spacing 16
                , paddingEach
                    { top = 0
                    , bottom = 16
                    , left = 0
                    , right = 0
                    }
                ]
            |> Container.expand
            |> Just


viewGridCellEmpty : Element msg
viewGridCellEmpty =
    [ ""
        |> verbatim
        |> body1
    ]
        |> paragraph
            [ centerX
            , centerY
            , Font.center
            ]
        |> el
            [ width fill
            , height fill
            , padding 16
            ]


type GridSelectionIndicator
    = IndicatorRow Int
    | IndicatorColumn Int


viewGridSelectionIndicator : Data f -> Bool -> GridSelectionIndicator -> Element msg
viewGridSelectionIndicator data hovered gridSelectionIndicator =
    let
        ( indexRowMax, indexColumnMax ) =
            ( List.length data.gridCells.gridRows - 1
            , List.length data.gridCells.gridColumns - 1
            )

        ( selected, background ) =
            case gridSelectionIndicator of
                IndicatorRow indexRow ->
                    let
                        indicesColumn =
                            List.map
                                (\indexColumn ->
                                    ( indexRow, indexColumn )
                                )
                                (List.range 0 indexColumnMax)
                    in
                    ( List.all (\indexGrid -> Set.member indexGrid data.selectedCells) indicesColumn
                    , [ backgroundSelectedPartially
                            hovered
                            "to right"
                            (List.map (\indexGrid -> Set.member indexGrid data.selectedCells) indicesColumn)
                      ]
                    )

                IndicatorColumn indexColumn ->
                    let
                        indicesRow =
                            List.map
                                (\indexRow ->
                                    ( indexRow, indexColumn )
                                )
                                (List.range 0 indexRowMax)
                    in
                    ( List.all (\indexGrid -> Set.member indexGrid data.selectedCells) indicesRow
                    , [ backgroundSelectedPartially
                            hovered
                            "to bottom"
                            (List.map (\indexGrid -> Set.member indexGrid data.selectedCells) indicesRow)
                      ]
                    )

        borderRadius =
            case gridSelectionIndicator of
                IndicatorRow _ ->
                    Border.roundEach
                        { topLeft = 12
                        , topRight = 0
                        , bottomLeft = 12
                        , bottomRight = 0
                        }

                IndicatorColumn _ ->
                    Border.roundEach
                        { topLeft = 12
                        , topRight = 0
                        , bottomLeft = 12
                        , bottomRight = 0
                        }

        pxWidth =
            px
                (case gridSelectionIndicator of
                    IndicatorRow _ ->
                        -- adjust height to not overlay parent's 1 px border
                        configGrid.widthRowHeader - 2

                    IndicatorColumn _ ->
                        32
                )

        theHeight =
            case gridSelectionIndicator of
                IndicatorRow _ ->
                    fill

                IndicatorColumn _ ->
                    -- adjust width to not overlay parent's 1 px border
                    px (configGrid.heightColumnHeader - 2)
    in
    el
        ([ width fill
         , height fill
         ]
            ++ background
        )
        (if selected then
            Icon.viewSmall Icon.Done
                |> el
                    [ centerX
                    , centerY
                    ]
                |> el
                    [ width (px 31)
                    , height (px 30)
                    , alignTop
                    ]

         else
            none
        )
        |> el
            [ width pxWidth
            , height theHeight
            , borderRadius
            , clip
            ]


backgroundSelectedPartially : Bool -> String -> List Bool -> Element.Attribute msg
backgroundSelectedPartially hovered directionGradient selecteds =
    let
        n =
            toFloat (List.length selecteds)
    in
    style "background"
        (String.concat
            [ "linear-gradient("
            , directionGradient
            , ","
            , String.join ","
                (List.indexedMap
                    (\k selected ->
                        let
                            m =
                                toFloat k
                        in
                        (if selected then
                            colorToRgba
                                ((if hovered then
                                    Color.hoveredWith Color.onSecondaryContainer

                                  else
                                    identity
                                 )
                                    Color.secondaryContainer
                                )

                         else
                            "transparent"
                        )
                            ++ " "
                            ++ (String.fromFloat (100 * m / n) ++ "%")
                            ++ " "
                            ++ (String.fromFloat (100 * (m + 1) / n) ++ "%")
                    )
                    selecteds
                )
            , ")"
            ]
        )


viewGridCell :
    Config e f m (Model f) (Msg f m)
    -> Bool
    -> C
    -> Report
    -> Data f
    -> IndexGrid
    -> IndexGrid
    -> Maybe (Instance f)
    -> Element m
viewGridCell config fullscreen c report data tabIndexZeroCell (( indexRow, indexColumn ) as indexGrid) maybeInstance =
    let
        hovered =
            Just indexGrid == data.hoveredCell

        hoveredByHeader =
            (Just indexRow == data.hoveredRow)
                || (Just indexColumn == data.hoveredColumn)

        focused =
            (Just indexGrid == data.focusedCell)
                || selectedByMouseSelection data.mouseSelection indexGrid

        selected =
            Set.member indexGrid data.selectedCells

        viewGridCellSelectionIndicator =
            [ if selected then
                Icon.viewSmall Icon.Done
                    |> el
                        [ centerX
                        , centerY
                        ]
                    |> el
                        [ width (px 32)
                        , height (px 32)
                        ]

              else
                none
            , if hovered || focused then
                Input.button
                    [ stopPropagationOnClick (config.toMsg (UserCopiedGridCell (Set.singleton indexGrid)))
                    , -- prevent initiating a mouse selection
                      htmlAttribute (Html.Events.stopPropagationOn "mousedown" (JDecode.succeed ( config.toMsg NoOp, True )))
                    , disabled False
                    , padding 4
                    , Border.rounded 13
                    , mouseDown [ Background.color (Color.pressedWith Color.onSurface Color.transparent) ]
                    , mouseOver [ Background.color (Color.hoveredWith Color.onSurface Color.transparent) ]
                    , tabindex -1
                    ]
                    { onPress = Nothing
                    , label = Icon.viewSmall Icon.ContentCopy
                    }
                    |> Tooltip.add
                        { label = verbatim "Copy cell"
                        , placement = Tooltip.Above
                        , tooltip = c.tooltip
                        , tooltipMsg = config.msgTooltip
                        , id = idGridCell data.nodeFocus indexGrid ++ "--copy"
                        }
                    |> el
                        [ centerX
                        , centerY
                        ]
                    |> el
                        [ width (px 32)
                        , height (px 32)
                        , alignBottom
                        ]

              else
                none
            , if hovered || focused then
                Input.button
                    [ stopPropagationOnClick (config.toMsg (UserPressedEditGridCell indexGrid))
                    , -- prevent initiating a mouse selection
                      htmlAttribute (Html.Events.stopPropagationOn "mousedown" (JDecode.succeed ( config.toMsg NoOp, True )))
                    , disabled False
                    , padding 4
                    , Border.rounded 13
                    , mouseDown [ Background.color (Color.pressedWith Color.onSurface Color.transparent) ]
                    , mouseOver [ Background.color (Color.hoveredWith Color.onSurface Color.transparent) ]
                    , tabindex -1
                    ]
                    { onPress = Nothing
                    , label = Icon.viewSmall Icon.Edit
                    }
                    |> Tooltip.add
                        { label = verbatim "Edit cell"
                        , placement = Tooltip.Above
                        , tooltip = c.tooltip
                        , tooltipMsg = config.msgTooltip
                        , id = idGridCell data.nodeFocus indexGrid ++ "--edit"
                        }
                    |> el
                        [ centerX
                        , centerY
                        ]
                    |> el
                        [ width (px 32)
                        , height (px 32)
                        , alignBottom
                        ]

              else
                none
            ]
                |> column
                    [ width (px 32)
                    , height fill
                    , Background.color
                        (if selected then
                            if hovered then
                                Color.hoveredWith Color.onSecondaryContainer Color.secondaryContainer

                            else
                                Color.secondaryContainer

                         else
                            Color.transparent
                        )
                    ]

        viewGridCellErrorIndicator =
            el
                [ alignBottom
                , width (px 32)
                , height (px 32)
                , Font.color error
                , Font.center
                ]
                (el
                    [ centerX
                    , centerY
                    ]
                    (Icon.viewNormal Icon.Error)
                )

        validationErrors =
            Maybe.unwrap []
                (\instance ->
                    List.filter (\(ValidationResult { focusNode }) -> config.nodeFocus instance.formEditor == focusNode)
                        report.results
                )
                maybeInstance
    in
    el
        (List.concat
            [ [ Events.onMouseEnter (config.toMsg (UserHoveredGridCell (Just indexGrid)))
              , Events.onMouseLeave (config.toMsg (UserHoveredGridCell Nothing))
              , Events.onFocus (config.toMsg (UserFocusedGridCell (Just indexGrid)))
              , Events.onLoseFocus (config.toMsg (UserFocusedGridCell Nothing))
              , Keyboard.onKeyDown
                    (List.concat
                        [ Keyboard.mapEvents (config.toMsg << UserInteractedFocusGridCell) Keyboard.eventsIntentFocus
                        , Keyboard.mapEvents (config.toMsg << UserInteractedSelectionGridCell) Keyboard.eventsIntentSelection
                        , [ { key = "c"
                            , ctrlKey = True
                            , shiftKey = False
                            , msg = config.toMsg (UserCopiedGridCell data.selectedCells)
                            }
                          ]
                        ]
                    )
              , -- prevent default focus behavior
                htmlAttribute
                    (Html.Events.preventDefaultOn "mousedown"
                        (JDecode.succeed ( config.toMsg NoOp, True ))
                    )
              , htmlAttribute
                    (Html.Events.on "click"
                        (JDecode.map2 Tuple.pair
                            (JDecode.field "ctrlKey" JDecode.bool)
                            (JDecode.field "shiftKey" JDecode.bool)
                            |> JDecode.map
                                (\( ctrl, shift ) ->
                                    config.toMsg
                                        ((if shift then
                                            UserShiftClickedGridCell

                                          else if ctrl then
                                            UserCtrlClickedGridCell

                                          else
                                            UserClickedGridCell
                                         )
                                            indexGrid
                                        )
                                )
                        )
                    )
              , tabindex
                    (if indexGrid == tabIndexZeroCell then
                        0

                     else
                        -1
                    )
              ]
            , (-- FIXME ids are duplicate on scroll overlay
               if
                (data.fullscreen && fullscreen)
                    || (not data.fullscreen && not fullscreen)
               then
                [ Element.id (idGridCell data.nodeFocus indexGrid)
                ]

               else
                []
              )
            ]
        )
        (Maybe.unwrap
            ([ viewGridCellSelectionIndicator
             , viewGridCellEmpty
                |> el
                    [ width fill
                    , Font.center
                    , centerY
                    ]
             ]
                |> row
                    [ width fill
                    , height fill
                    ]
            )
            (\instance ->
                [ viewGridCellSelectionIndicator
                , instance.formViewer
                    |> config.card c
                    |> Maybe.map (Element.map (MsgForm indexRow indexColumn >> config.toMsg))
                    |> Maybe.withDefault none
                    |> el
                        [ width fill
                        , centerY
                        , clipX
                        , paddingEach
                            { top = 8
                            , bottom = 8
                            , left = 4
                            , right = 8
                            }
                        ]
                ]
                    |> row
                        [ width fill
                        , height fill
                        ]
            )
            maybeInstance
            |> el
                (List.concat
                    [ [ width (px configGrid.widthColumn)
                      , height (px configGrid.heightRow)
                      , Background.color (backgroundColor data.breadcrumbs)
                      , Border.rounded 12
                      , scrollbarY
                      , Element.pointer
                      , -- XXX make tooltips show
                        Element.style "overflow" "visible"
                      ]
                    , if hovered || hoveredByHeader then
                        [ Elevation.z1 ]

                      else
                        []
                    , if not (List.isEmpty validationErrors) then
                        [ inFront viewGridCellErrorIndicator ]

                      else
                        []
                    ]
                )
            |> gridFocusBorder focused
        )


viewGridForm :
    Config e f m (Model f) (Msg f m)
    -> Bool
    -> C
    -> Report
    -> Bool
    -> Data f
    -> List (Element.Attribute m)
    -> IndexGrid
    -> Instance f
    -> Element m
viewGridForm config showBackground c report isDragging data attrs indexGrid instance =
    let
        ( labelGridRow, labelGridColumn ) =
            let
                placeholder n =
                    "[" ++ String.fromInt n ++ "]"

                unwrap xs n =
                    Maybe.unwrap
                        (placeholder n)
                        (Localize.forLabel c << .label)
                        (List.getAt n xs)
            in
            Tuple.mapBoth
                (unwrap data.gridCells.gridRows)
                (unwrap data.gridCells.gridColumns)
                indexGrid

        toHeading =
            if data.breadcrumbs.depthGroup == 0 then
                h5

            else
                h6

        numSelectedCells =
            Set.size data.selectedCells
    in
    [ [ ("Edit cell "
            ++ labelGridRow
            ++ ", "
            ++ labelGridColumn
            ++ (if numSelectedCells > 1 then
                    " (and " ++ String.fromInt (numSelectedCells - 1) ++ " other(s))"

                else
                    ""
               )
        )
            |> verbatim
            |> toHeading
      , [ Icon.Link
            |> buttonIcon (config.userPressedLink (config.nodeFocus instance.formEditor))
            |> ButtonIcon.toElement
            |> Tooltip.add
                { label = verbatim "Copy link"
                , placement = Tooltip.Above
                , tooltip = c.tooltip
                , tooltipMsg = config.msgTooltip
                , id = Rdf.serializeNode (config.nodeFocus instance.formEditor) ++ "--copy-link"
                }
        , Icon.Close
            |> buttonIcon (config.toMsg UserDismissedGridForm)
            |> ButtonIcon.toElement
            |> Tooltip.add
                { label = verbatim "Close"
                , placement = Tooltip.Above
                , tooltip = c.tooltip
                , tooltipMsg = config.msgTooltip
                , id = Rdf.serializeNode (config.nodeFocus instance.formEditor) ++ "--close"
                }
        ]
            |> row
                [ spacing 4
                , alignRight
                ]
      ]
        |> row [ width fill ]

    -- FIXME
    -- , viewGridFormFields config c report breadcrumbs grid indexGrid instance
    , instance.formEditor
        |> config.view c report isDragging
        |> Maybe.map (Element.map (MsgForm (Tuple.first indexGrid) (Tuple.second indexGrid) >> config.toMsg))
        |> Maybe.withDefault none
    ]
        |> column
            [ width fill
            , spacing Container.configField.spacingFieldY
            ]
        -- FIXME `scrollbarX` does not work, unless wrapped in `el`, and `width` set on both elements, cf. https://github.com/mdgriffith/elm-ui/issues/321
        |> (if showBackground then
                el
                    [ width fill
                    , padding 16
                    , Border.rounded 12
                    , Border.width 1
                    , Border.color outlineVariant
                    , Background.color (backgroundColor data.breadcrumbs)
                    ]
                    >> el
                        [ width fill
                        , Element.id (idGridCell data.nodeFocus indexGrid ++ "--container")
                        ]

            else
                el
                    (List.concat
                        [ [ scrollbarX
                          , width fill
                          ]
                        , if showBackground then
                            [ padding 16
                            ]

                          else
                            []
                        ]
                    )
                    >> el
                        (List.concat
                            [ [ width fill
                              , alignTop
                              , clipX
                              ]
                            , attrs
                            ]
                        )
           )



-- FIXME
--- viewGridFormFields : Config field msg (Msg field) -> C -> Report -> Breadcrumbs -> Model field -> IndexGrid -> Instance field -> Element msg
--- viewGridFormFields config c report breadcrumbs data ( indexRow, indexColumn ) instance =
---     let
---         viewNested userPressedClearSelectedValues indexInner fieldNested =
---             let
---                 instancesSelected =
---                     data.instances
---                         |> Dict.filter (\index _ -> Set.member index data.selectedCells)
---                         |> Dict.map
---                             (\_ instanceProto ->
---                                 { nodeFocus = instanceProto.nodeFocus
---                                 , fields = instanceProto.fieldsEditor
---                                 }
---                             )
---                         |> Dict.values
---
---                 fieldsSelected =
---                     instancesSelected
---                         |> List.filterMap
---                             (\instanceSelected ->
---                                 List.getAt indexInner instanceSelected.fields
---                             )
---
---                 valuesSelected =
---                     fieldsSelected
---                         |> List.map config.fieldToString
---                         |> List.sort
---                         |> List.unique
---
---                 numValuesSelected =
---                     List.length valuesSelected
---             in
---             Maybe.map
---                 (\container ->
---                     if numValuesSelected > 1 then
---                         column
---                             [ width fill
---                             , spacing 4
---                             ]
---                             [ row [ width fill ]
---                                 [ Container.getElement container
---                                 , el [ alignRight ]
---                                     (button
---                                         (config.toMsg breadcrumbs (userPressedClearSelectedValues indexInner))
---                                         (verbatim "Clear selected values")
---                                         |> Button.withFormat Button.Outlined
---                                         |> Button.withLeadingIcon Icon.Backspace
---                                         |> Button.view
---                                     )
---                                 ]
---                             , paragraph []
---                                 [ ("and " ++ String.fromInt (numValuesSelected - 1) ++ " other value(s)")
---                                     |> verbatim
---                                     |> body2
---                                 ]
---                             ]
---                             |> (if Container.getWrappable container then
---                                     Container.wrappable
---
---                                 else
---                                     Container.expand
---                                )
---
---                     else
---                         container
---                 )
---                 (if numValuesSelected > 1 then
---                     config.viewCardField
---                         { breadcrumbs
---                             | nodeFocus = (config.nodeFocus instance.formEditor)
---                             , path = indexInner :: indexColumn :: indexRow :: breadcrumbs.path
---                             , depthCollection = breadcrumbs.depthCollection + 1
---                         }
---                         fieldNested
---
---                  else
---                     config.viewField
---                         { breadcrumbs
---                             | nodeFocus = (config.nodeFocus instance.formEditor)
---                             , path = indexInner :: indexColumn :: indexRow :: breadcrumbs.path
---                             , depthCollection = breadcrumbs.depthCollection + 1
---                         }
---                         fieldNested
---                 )
---     in
---     [ Input.help
---         (List.filterMap
---             (helpFromValidationResult c InputTypeEnter (config.nodeFocus instance.formEditor) Nothing (Rdf.stringOrLangStringFromList []))
---             report.results
---         )
---     , instance.fieldsEditor
---         |> List.indexedMap (viewNested UserPressedClearSelectedValues)
---         |> List.filterMap identity
---         |> Container.wrap Container.configField
---     ]
---         |> column
---             [ width (maximum Dimensions.paneWidthMax fill)
---             , spacing Container.configField.spacingFieldY
---             ]


gridFocusBorder : Bool -> Element msg -> Element msg
gridFocusBorder focused =
    el
        [ width fill
        , height fill
        , padding 4
        , Border.dashed
        , Border.width 2
        , Border.color
            (if focused then
                Color.primary

             else
                Color.transparent
            )
        , Border.rounded 12
        ]


selectedByMouseSelection : MouseSelection -> IndexGrid -> Bool
selectedByMouseSelection mouseSelection ( indexRow, indexColumn ) =
    let
        rectCell =
            let
                offsetTop =
                    configGrid.heightColumnHeader + configGrid.gap

                offsetLeft =
                    configGrid.widthRowHeader + configGrid.gap

                cellWidth =
                    configGrid.widthColumn + configGrid.gap

                cellHeight =
                    configGrid.heightRow + configGrid.gap

                top =
                    offsetTop + (indexRow * cellHeight)

                left =
                    offsetLeft + (indexColumn * cellWidth)
            in
            { top = toFloat top
            , left = toFloat left
            , bottom = toFloat (top + cellHeight)
            , right = toFloat (left + cellWidth)
            }
    in
    Maybe.unwrap False
        (intersectsRect rectCell)
        (mouseSelectionRect mouseSelection)


type alias Rect a =
    { top : a
    , left : a
    , bottom : a
    , right : a
    }


mouseSelectionRect : MouseSelection -> Maybe (Rect Float)
mouseSelectionRect mouseSelection =
    case mouseSelection of
        MouseSelection _ MouseSelectionStateInactive ->
            Nothing

        MouseSelection _ (MouseSelectionStateActive { start, end }) ->
            Just
                { top = min start.y end.y
                , left = min start.x end.x
                , bottom = max start.y end.y
                , right = max start.x end.x
                }


{-| Two rectangles intersect iff their bounding rectangle's dimension is smaller than the sum of their dimensions.
-}
intersectsRect : Rect number -> Rect number -> Bool
intersectsRect rectA rectB =
    let
        rect =
            { top = min rectA.top rectB.top
            , left = min rectA.left rectB.left
            , bottom = max rectA.bottom rectB.bottom
            , right = max rectA.right rectB.right
            }

        width { left, right } =
            right - left

        height { top, bottom } =
            bottom - top

        result =
            (width rect < width rectA + width rectB)
                && (height rect < height rectA + height rectB)
    in
    result


type alias ConfigViewGrid =
    { widthColumn : Int
    , heightRow : Int
    , heightColumnHeader : Int
    , widthRowHeader : Int
    , gap : Int
    }


configGrid : ConfigViewGrid
configGrid =
    let
        base =
            32
    in
    { widthColumn = 4 * base
    , heightRow = 4 * base
    , heightColumnHeader = base
    , widthRowHeader = base
    , gap = 12
    }


{-| -}
card : Config e f m (Model f) (Msg f m) -> C -> Model f -> Maybe (Container m)
card config c (Model ({ breadcrumbs } as data)) =
    let
        toHeading =
            if breadcrumbs.depthGroup == 0 then
                h5

            else
                h6

        viewHeader attrs element =
            element
                |> el
                    [ width fill
                    , Font.center
                    , centerY
                    ]
                |> el
                    ([ Background.color (backgroundColor breadcrumbs)
                     , Border.solid
                     , Border.width 1
                     , Border.color outlineVariant
                     , Border.rounded 12
                     , Elevation.z1Hover
                     ]
                        ++ attrs
                    )

        viewCornerHeader =
            none
                |> el
                    [ height (px configGrid.heightColumnHeader)
                    , width fill
                    ]

        viewColumnHeader column =
            text (Localize.forLabel c column.label)
                |> viewHeader
                    [ width (px configGrid.widthColumn)
                    , height (px configGrid.heightColumnHeader)
                    ]

        viewRowHeader row =
            text (Localize.forLabel c row.label)
                |> viewHeader
                    [ height (px configGrid.heightRow)
                    , width fill
                    , padding 8
                    ]

        namePlural =
            data.grouped.names
                |> List.head
                |> Maybe.map (Localize.withinSentencePlural c)
                |> Maybe.withDefault "values"
    in
    [ data.grouped.names
        |> List.head
        |> Maybe.map (Localize.forLabelPlural c)
        |> Maybe.withDefault "Missing label"
        |> verbatim
        |> toHeading
        |> el
            [ paddingEach
                { top = 16
                , bottom = 0
                , left = 0
                , right = 0
                }
            ]
    , if
        List.isEmpty data.gridCells.gridColumns
            || List.isEmpty data.gridCells.gridRows
      then
        [ "There are no "
        , namePlural
        , "."
        ]
            |> String.concat
            |> verbatim
            |> body1
            |> el [ centerX ]
            |> el
                [ width fill
                , padding 32
                , spacing 32
                , Border.rounded 12
                , Background.color (backgroundColor breadcrumbs)
                ]

      else
        [ [ viewCornerHeader
          , data.gridCells.gridRows
                |> List.map viewRowHeader
                |> column [ spacing configGrid.gap ]
          ]
            |> column
                [ spacing configGrid.gap
                , style "position" "sticky"
                , style "left" "0"
                , style "z-index" "1"
                , Background.color (backgroundColor { breadcrumbs | depthCollection = breadcrumbs.depthCollection - 1 })
                ]
        , data.gridCells.gridColumns
            |> List.indexedMap
                (\indexColumn gridColumn ->
                    [ viewColumnHeader gridColumn
                    , data.gridCells.gridRows
                        |> List.indexedMap
                            (\indexRow _ ->
                                data.instances
                                    |> Dict.get ( indexRow, indexColumn )
                                    |> viewCardGridCell config c breadcrumbs indexRow indexColumn
                            )
                        |> column [ spacing configGrid.gap ]
                    ]
                        |> column [ spacing configGrid.gap ]
                )
            -- FIXME `scrollbarX` does not work, unless wrapped in `el`, and `width` set on both elements, cf. https://github.com/mdgriffith/elm-ui/issues/321
            |> row
                [ width fill
                , spacing configGrid.gap
                ]
        ]
            |> row
                [ scrollbarX
                , width fill
                , spacing configGrid.gap
                , paddingEach
                    { top = 0
                    , bottom = 16
                    , left = 0
                    , right = 0
                    }
                ]
            |> el
                [ width fill
                , height fill
                ]
    ]
        |> column
            [ width fill
            , spacing 4
            ]
        |> Container.expand
        |> Just


viewCardGridCellEmpty : Element msg
viewCardGridCellEmpty =
    [ "(empty)"
        |> verbatim
        |> body1
    ]
        |> paragraph
            [ centerX
            , centerY
            , Font.center
            ]
        |> el
            [ width fill
            , height fill
            , padding 16
            ]


viewCardGridCell :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Breadcrumbs
    -> Int
    -> Int
    -> Maybe (Instance f)
    -> Element m
viewCardGridCell config c breadcrumbs indexRow indexColumn maybeInstance =
    maybeInstance
        |> Maybe.unwrap
            (viewCardGridCellEmpty
                |> el
                    [ width fill
                    , Font.center
                    , centerY
                    ]
            )
            (.formViewer
                >> config.card c
                >> Maybe.map (Element.map (MsgForm indexRow indexColumn >> config.toMsg))
                >> Maybe.withDefault none
                >> el
                    [ width fill
                    , centerY
                    ]
            )
        |> el
            [ width (px configGrid.widthColumn)
            , height (px configGrid.heightRow)
            , padding 8
            , Background.color (backgroundColor breadcrumbs)
            , Border.solid
            , Border.width 1
            , Border.color outlineVariant
            , Border.rounded 12
            , Elevation.z1Hover
            ]


backgroundColor : Breadcrumbs -> Color
backgroundColor breadcrumbs =
    if breadcrumbs.depthCollection == -1 then
        surfaceContainerLow

    else
        case breadcrumbs.depthCollection of
            0 ->
                surfaceContainer

            1 ->
                surfaceContainerHigh

            2 ->
                surfaceContainerHighest

            _ ->
                surfaceContainerHighest


{-| -}
columns :
    Config e f m (Model f) (Msg f m)
    -> C
    -> List String
    -> Grouped.DataGrid
    -> List (Table.Column f m)
columns _ _ _ _ =
    []


{-| -}
help : Config e f m (Model f) (Msg f m) -> C -> Report -> Model f -> List Fluent
help _ c report (Model data) =
    report.results
        |> List.filter
            (Report.validationResultFor
                (Report.emptySelector
                    |> Maybe.apply Report.selectFocusNode (Rdf.toIri data.nodeFocus)
                    |> Report.selectPath data.grouped.path
                )
            )
        |> List.filterMap (helpFromValidationResult c data)


helpFromValidationResult : C -> Data f -> ValidationResult -> Maybe Fluent
helpFromValidationResult c data (ValidationResult { constraintComponent, message }) =
    let
        name =
            data.grouped.names
                |> List.head
                |> Maybe.map (Localize.withinSentenceWithArticleIndefinite c)
                |> Maybe.withDefault "a value"
    in
    case constraintComponent of
        MinCountConstraintComponent ->
            Just (verbatim ("Enter " ++ name))

        _ ->
            Just (verbatim (Localize.verbatim c message ++ "."))


{-| -}
summary : Config e f m (Model f) (Msg f m) -> C -> Model f -> List Fluent
summary config c (Model data) =
    case Dict.values data.instances of
        [] ->
            []

        [ instance ] ->
            config.summary c instance.formEditor

        _ ->
            case List.head data.grouped.names of
                Nothing ->
                    []

                Just name ->
                    [ Fluent.verbatim
                        (String.concat
                            [ String.fromInt (Dict.size data.instances)
                            , " "
                            , Localize.withinSentencePlural c name
                            ]
                        )
                    ]



-- INFO


{-| -}
toNodeFocus : Model f -> BlankNodeOrIri
toNodeFocus (Model data) =
    data.nodeFocus


{-| -}
toPath : Model f -> Path
toPath (Model data) =
    data.path


{-| -}
isEmpty : Config e f m (Model f) (Msg f m) -> Model f -> Bool
isEmpty _ _ =
    False


{-| -}
dragging : Config e f m (Model f) (Msg f m) -> Model f -> Bool
dragging _ (Model data) =
    case data.mouseSelection of
        MouseSelection _ (MouseSelectionStateActive _) ->
            True

        MouseSelection _ MouseSelectionStateInactive ->
            False


{-| -}
log : Config e f m (Model f) (Msg f m) -> Model f -> Pretty.Doc ()
log config (Model data) =
    Pretty.lines
        [ Pretty.string "Grid"
        , Pretty.indent 2 (Pretty.lines (List.map (.formEditor >> config.log) (Dict.values data.instances)))
        ]



-- OUTPUT


{-| -}
encoders :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Model f
    -> List Encode.PropertyEncoder
encoders config c (Model data) =
    data.instances
        |> Dict.keys
        |> List.filterMap (encoderInstance config c data)


encoderInstance :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Data f
    -> ( Int, Int )
    -> Maybe Encode.PropertyEncoder
encoderInstance config c data (( indexRow, indexColumn ) as indexInstance) =
    Maybe.map2 Tuple.pair
        (Dict.get indexInstance data.instances)
        (Maybe.map2 Tuple.pair
            (List.getAt indexRow data.gridCells.gridRows)
            (List.getAt indexColumn data.gridCells.gridColumns)
        )
        |> Maybe.map (encoderInstanceHelp config c data)


encoderInstanceHelp :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Data f
    -> ( Instance f, ( GridRow, GridColumn ) )
    -> PropertyEncoder
encoderInstanceHelp config c data ( instance, ( row, column ) ) =
    Encode.property data.grouped.path
        (Encode.node (config.nodeFocus instance.formEditor)
            (List.concat
                [ encodersClasses data
                , encodersIsDefinedBy config data instance
                , encodersRowAndColumn data row column
                , config.encoders c instance.formEditor
                ]
            )
        )


encodersClasses : Data f -> List PropertyEncoder
encodersClasses data =
    List.map (Encode.predicate a << Encode.iri)
        (NonEmpty.toList data.grouped.classes)


encodersIsDefinedBy :
    Config e f m (Model f) (Msg f m)
    -> Data f
    -> Instance f
    -> List PropertyEncoder
encodersIsDefinedBy config data instance =
    let
        iriDocumentWithFragment =
            Rdf.setFragment (Breadcrumbs.id (config.path instance.formEditor))
                data.iriDocument
    in
    [ Encode.predicate RDFS.isDefinedBy
        (Encode.object iriDocumentWithFragment)
    ]


encodersRowAndColumn : Data f -> GridRow -> GridColumn -> List PropertyEncoder
encodersRowAndColumn data row column =
    [ Encode.predicate data.grouped.dataEditorGrid.rowPath
        (Encode.iri row.iri)
    , Encode.predicate data.grouped.dataEditorGrid.columnPath
        (Encode.iri column.iri)
    ]



-- SUPPORT


idGridScrollContainer : BlankNodeOrIri -> String
idGridScrollContainer focusNode =
    Rdf.serializeNode focusNode ++ "--scroll"


idGridCell : BlankNodeOrIri -> IndexGrid -> String
idGridCell focusNode ( indexRow, indexColumn ) =
    String.concat
        [ Rdf.serializeNode focusNode
        , "--"
        , String.fromInt indexRow
        , "-"
        , String.fromInt indexColumn
        ]


idGridRowHeaders : BlankNodeOrIri -> String
idGridRowHeaders focusNode =
    Rdf.serializeNode focusNode ++ "--row-headers"
