module Shacl.Form.Viewed.Checkbox exposing
    ( Model
    , init, initWith
    , Msg, update, expandAll
    , subscriptions
    , view, viewFullscreen, card, columns, help, summary
    , encoders
    , toNodeFocus, toPath, isEmpty, dragging, log
    )

{-|


# Model

@docs Model


# Init

@docs init, initWith


# Update

@docs Msg, update, expandAll


# Subscriptions

@docs subscriptions


# View

@docs view, viewFullscreen, card, columns, help, summary


# Output

@docs encoders


# Info

@docs toNodeFocus, toPath, isEmpty, dragging, log

-}

import Element as Ui exposing (Element)
import Element.Lazy as Ui
import Fluent exposing (Fluent, verbatim)
import Maybe.Apply as Maybe
import Pretty
import Rdf exposing (BlankNodeOrIri)
import Rdf.Decode as Decode exposing (Decoder)
import Rdf.Decode.Extra as Decode
import Rdf.Encode as Encode
import Rdf.Graph exposing (Seed)
import Shacl.Clipboard as Clipboard
import Shacl.Form.Breadcrumbs as Breadcrumbs exposing (Breadcrumbs, Path)
import Shacl.Form.Context exposing (C)
import Shacl.Form.Decoded as Decoded
import Shacl.Form.Effect as Effect
import Shacl.Form.Localize as Localize
import Shacl.Form.Shared as Shared exposing (Config, Shared, withInfo)
import Shacl.Form.Translated as Translated
import Shacl.Form.Ui as Ui
import Shacl.Form.Ui.Container as Container exposing (Container)
import Shacl.Form.Updated as Updated exposing (Updated)
import Shacl.Report as Report exposing (Report, ValidationResult(..))
import Ui.Atom.Checkbox as Checkbox
import Ui.Atom.ErrorWrapper exposing (errorWrapper)
import Ui.Atom.Labelled as Labelled
import Ui.Molecule.Table as Table
import Ui.Molecule.TableCell as TableCell exposing (TableCell)



-- MODEL


{-| -}
type Model
    = Model Data


type alias Data =
    { path : Path
    , breadcrumbs : Breadcrumbs
    , translated : Translated.DataCheckbox
    , nodeFocus : BlankNodeOrIri
    , value : Bool
    }



-- INIT


{-| -}
init :
    Config e f m Model Msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Breadcrumbs
    -> Decoded.DataCheckbox
    -> Updated Model m
init =
    Shared.init
        { value = \_ _ decoded -> decoded.checked
        , default = \_ _ { translated } -> translated.defaultValue
        , path = .translated >> .path
        , withValue =
            \_ _ _ seed breadcrumbs { path, translated, nodeFocus } value ->
                { model =
                    Model
                        { path = path
                        , breadcrumbs = breadcrumbs
                        , translated = translated
                        , nodeFocus = nodeFocus
                        , value = value
                        }
                , effect = Effect.none
                , seed = seed
                , needsPersisting = False
                }
        , empty =
            \_ _ _ seed breadcrumbs { path, translated, nodeFocus } ->
                { model =
                    Model
                        { path = path
                        , breadcrumbs = breadcrumbs
                        , translated = translated
                        , nodeFocus = nodeFocus
                        , value = False
                        }
                , effect = Effect.none
                , seed = seed
                , needsPersisting = False
                }
        }


{-| -}
initWith :
    Config e f m Model Msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Model
    -> Result e (Maybe (Updated Model m))
initWith =
    Shared.initWith
        { decoder = \c shared (Model data) -> decoder c shared data.translated
        , default = \_ _ (Model data) -> data.translated.defaultValue
        , nodeFocus = \(Model data) -> data.nodeFocus
        , path = \(Model data) -> data.translated.path
        , updateValue = updateValue
        , removeValue = removeValue
        , tagField = Translated.TagFieldCheckbox
        }


updateValue :
    Config e f m Model Msg
    -> C
    -> Shared
    -> Seed
    -> Bool
    -> Model
    -> Maybe (Updated Model m)
updateValue _ _ _ seed value (Model data) =
    if value == data.value then
        Nothing

    else
        { data | value = value }
            |> Model
            |> Updated.from seed
            |> Just


removeValue :
    Config e f m Model Msg
    -> C
    -> Shared
    -> Seed
    -> Model
    -> Maybe (Updated Model m)
removeValue _ _ _ seed (Model data) =
    if data.value == False then
        Nothing

    else
        { data | value = False }
            |> Model
            |> Updated.from seed
            |> Just



-- UPDATE


{-| -}
type Msg
    = UserChangedCheckboxValue Bool
    | UserCopied
    | UserPasted Bool
    | Ignored


{-| -}
update :
    Config e f m Model Msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Msg
    -> Model
    -> Updated Model m
update config _ c _ seed msg ((Model data) as model) =
    case msg of
        UserChangedCheckboxValue value ->
            { data | value = value }
                |> Model
                |> Updated.from seed
                |> Updated.needsPersisting

        UserCopied ->
            model
                |> Updated.from seed
                |> Updated.withEffect
                    (encoders config c model
                        |> Clipboard.serialize data.nodeFocus seed
                        |> Effect.copyToClipboard
                    )

        UserPasted valuePasted ->
            { data | value = valuePasted }
                |> Model
                |> Updated.from seed
                |> Updated.needsPersisting

        Ignored ->
            Updated.from seed model


{-| -}
expandAll : Config e f m Model Msg -> Model -> Model
expandAll _ model =
    model



-- SUBSCRIPTIONS


{-| -}
subscriptions : Config e f m Model Msg -> Model -> Sub m
subscriptions _ _ =
    Sub.none



-- VIEW


{-| -}
view :
    Config e f m Model Msg
    -> C
    -> Report
    -> Bool
    -> Model
    -> Maybe (Container m)
view config c report _ model =
    viewHelp config c report model
        |> Container.wrappable
        |> Just


viewHelp :
    Config e f m Model Msg
    -> C
    -> Report
    -> Model
    -> Element m
viewHelp =
    Ui.lazy4
        (\config c report ((Model data) as model) ->
            data.value
                |> Checkbox.checkbox UserChangedCheckboxValue
                    (Ui.label c data.translated)
                |> Checkbox.withId (Breadcrumbs.pointer data.path)
                |> Checkbox.onPasteGraph Ignored onPaste
                |> Checkbox.onCopy UserCopied
                |> Maybe.apply Checkbox.withSupportingText (description c data)
                |> Checkbox.toElement
                |> errorWrapper (help config c report model)
                |> Ui.map config.toMsg
        )


onPaste : Decoder Msg
onPaste =
    Decode.map UserPasted (Decode.oneAtAnyPredicate Decoded.decoderCheckbox)


description : C -> Data -> Maybe Fluent
description c data =
    data.translated.descriptions
        |> List.head
        |> Maybe.map
            (Localize.verbatim c
                >> verbatim
            )


{-| -}
viewFullscreen :
    Config e f m Model Msg
    -> C
    -> Report
    -> Bool
    -> Model
    -> Maybe (Element m)
viewFullscreen _ _ _ _ _ =
    Nothing


{-| -}
card : Config e f m Model Msg -> C -> Model -> Maybe (Container m)
card _ c (Model data) =
    data.value
        |> Labelled.bool
            { label = Ui.label c data.translated
            , ifTrue = verbatim "Yes"
            , ifFalse = verbatim "No"
            }
        |> Container.expand
        |> Just


{-| -}
columns :
    Config e f m Model Msg
    -> C
    -> List String
    -> Translated.DataCheckbox
    -> List (Table.Column f m)
columns config c scope translated =
    [ { header = viewHeader c scope translated
      , width = Ui.shrink
      , view = viewCell config
      }
    ]


viewHeader : C -> List String -> Translated.DataCheckbox -> TableCell msg
viewHeader c scope translated =
    translated
        |> Ui.label c
        |> TableCell.textBold
        |> withInfo scope


viewCell : Config e f m Model Msg -> f -> TableCell m
viewCell config form =
    case config.toField form of
        Nothing ->
            TableCell.empty

        Just (Model data) ->
            if data.value then
                "Yes"
                    |> verbatim
                    |> TableCell.text

            else
                "No"
                    |> verbatim
                    |> TableCell.text


{-| -}
help : Config e f m Model Msg -> C -> Report -> Model -> List Fluent
help _ c report (Model data) =
    report.results
        |> Report.validationResultsFor
            (Report.emptySelector
                |> Maybe.apply Report.selectFocusNode (Rdf.toIri data.nodeFocus)
                |> Report.selectPath data.translated.path
            )
        |> List.filterMap (helpFromValidationResult c)


helpFromValidationResult : C -> ValidationResult -> Maybe Fluent
helpFromValidationResult c (ValidationResult { constraintComponent, message }) =
    case constraintComponent of
        _ ->
            Just (verbatim (Localize.verbatim c message ++ "."))


{-| -}
summary : Config e f m Model Msg -> C -> Model -> List Fluent
summary _ c (Model data) =
    if data.value then
        [ Ui.label c data.translated ]

    else
        []



-- OUTPUT


{-| -}
encoders : Config e f m Model Msg -> C -> Model -> List Encode.PropertyEncoder
encoders _ _ (Model data) =
    [ Encode.property data.translated.path (Encode.literal (Rdf.bool data.value))
    ]



-- INFO


{-| -}
toNodeFocus : Model -> BlankNodeOrIri
toNodeFocus (Model data) =
    data.nodeFocus


{-| -}
toPath : Model -> Path
toPath (Model data) =
    data.path


{-| -}
isEmpty : Config e f m Model Msg -> Model -> Bool
isEmpty _ (Model data) =
    not data.value


{-| -}
dragging : Config e f m Model Msg -> Model -> Bool
dragging _ _ =
    False


{-| -}
log : Config e f m Model Msg -> Model -> Pretty.Doc t
log _ (Model _) =
    Pretty.string "Checkbox"



-- DECODE
-- FIXME This should be moved to Decoded


decoder : C -> Shared -> Translated.DataCheckbox -> Decoder Bool
decoder _ _ translated =
    Decode.property translated.path Decoded.decoderCheckbox
