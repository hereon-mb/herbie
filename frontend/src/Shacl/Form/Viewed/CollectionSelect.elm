module Shacl.Form.Viewed.CollectionSelect exposing
    ( Model
    , init, initWith
    , Msg, update, expandAll
    , subscriptions
    , view, viewFullscreen, card, columns, help, summary
    , encoders
    , toNodeFocus, toPath, isEmpty, dragging, log
    )

{-|


# Model

@docs Model


# Init

@docs init, initWith


# Update

@docs Msg, update, expandAll


# Subscriptions

@docs subscriptions


# View

@docs view, viewFullscreen, card, columns, help, summary


# Output

@docs encoders


# Info

@docs toNodeFocus, toPath, isEmpty, dragging, log

-}

import Api
import Element exposing (Element)
import Element.Lazy exposing (lazy4)
import Fluent exposing (Fluent, verbatim)
import List.Extra as List
import List.NonEmpty as NonEmpty
import Maybe.Apply as Maybe
import Maybe.Extra as Maybe
import Pretty
import Rdf exposing (BlankNodeOrIri, Iri)
import Rdf.Encode as Encode exposing (PropertyEncoder)
import Rdf.Extra as Rdf
import Rdf.Graph exposing (Seed)
import Rdf.Namespaces exposing (a)
import Rdf.Namespaces.RDFS as RDFS
import Shacl.Form.Breadcrumbs as Breadcrumbs exposing (Breadcrumbs, Path)
import Shacl.Form.Context exposing (C)
import Shacl.Form.Decoded as Decoded
import Shacl.Form.Effect as Effect
import Shacl.Form.Grouped as Grouped
import Shacl.Form.Localize as Localize
import Shacl.Form.Shared exposing (Config, Option, Shared, optionFromOptionCombobox, optionToOptionCombobox, sortOptionsByScore, withInfo)
import Shacl.Form.Translated as Translated
import Shacl.Form.Ui as Ui
import Shacl.Form.Ui.Container as Container exposing (Container)
import Shacl.Form.Updated as Updated exposing (Updated)
import Shacl.OrderBy as OrderBy
import Shacl.Report as Report exposing (ConstraintComponent(..), Report, ValidationResult(..))
import Ui.Atom.ErrorWrapper exposing (errorWrapper)
import Ui.Atom.Labelled as Labelled
import Ui.Molecule.ComboboxMultiselect as ComboboxMultiselect exposing (comboboxMultiselect)
import Ui.Molecule.Table as Table
import Ui.Molecule.TableCell as TableCell exposing (TableCell)



-- MODEL


{-| -}
type Model f
    = Model (Data f)


type alias Data f =
    { path : Path
    , breadcrumbs : Breadcrumbs
    , grouped : Grouped.DataCollectionSelect
    , nodeFocus : BlankNodeOrIri
    , workspaceUuid : String
    , iriDocument : Iri
    , options : List Option
    , query : String
    , instances : List (Instance f)
    }


type alias Instance f =
    { breadcrumbs : Breadcrumbs
    , form : f
    , nodeFocus : BlankNodeOrIri
    , value : Option
    }



-- INIT


{-| -}
init :
    Config e f m (Model f) (Msg f m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Breadcrumbs
    -> Decoded.DataCollectionSelect
    -> Updated (Model f) m
init config populateDefaultValues c shared seed breadcrumbs decoded =
    let
        modelFromInstances instances =
            Model
                { path = decoded.path
                , breadcrumbs = breadcrumbs
                , grouped = decoded.grouped
                , nodeFocus = decoded.nodeFocus
                , workspaceUuid = shared.workspaceUuid
                , iriDocument = shared.iri
                , options = optionsFromDecoded c shared decoded.grouped.selectOne
                , query = ""
                , instances = instances
                }
    in
    decoded.instances
        |> List.foldl (initInstance config populateDefaultValues c shared seed breadcrumbs)
            ( { model = []
              , effect =
                    Effect.map config.toMsg
                        (Effect.getResource
                            ApiReturnedResource
                            decoded.grouped.selectOne.class
                        )
              , seed = seed
              , needsPersisting = False
              }
            , 0
            )
        |> Tuple.first
        |> Updated.map
            (List.reverse
                >> modelFromInstances
            )


optionsFromDecoded : C -> Shared -> Translated.DataSelectOne -> List Option
optionsFromDecoded c shared translated =
    translated.options
        |> List.map (valueToOption c)
        |> List.uniqueBy .iri
        |> Maybe.apply
            (OrderBy.sortBy .iri c.locale shared.graphCombinedFrontend)
            translated.orderBy


valueToOption : C -> Translated.Value -> Option
valueToOption c value =
    { iri = value.this
    , value = Localize.forLabel c value.label
    }


initInstance :
    Config e f m (Model f) (Msg f m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Breadcrumbs
    -> Decoded.InstanceCollectionSelect
    -> ( Updated (List (Instance f)) m, Int )
    -> ( Updated (List (Instance f)) m, Int )
initInstance config populateDefaultValues c shared seed breadcrumbs instance ( updated, index ) =
    let
        breadcrumbsNested =
            breadcrumbs
                |> increaseDepthCollection

        updatedForm =
            config.init populateDefaultValues c shared seed breadcrumbsNested instance.field
    in
    case instance.selectOne.value of
        Nothing ->
            ( updated, index )

        Just value ->
            ( { model =
                    { breadcrumbs = breadcrumbsNested
                    , form = updatedForm.model
                    , nodeFocus = instance.selectOne.nodeFocus
                    , value =
                        { iri = value.iri
                        , value = Maybe.unwrap "" (Localize.forLabel c) value.label
                        }
                    }
                        :: updated.model
              , effect =
                    Effect.batch
                        [ Effect.map (MsgForm index >> config.toMsg) updatedForm.effect
                        , updated.effect
                        ]
              , seed = updatedForm.seed
              , needsPersisting = updatedForm.needsPersisting || updated.needsPersisting
              }
            , index + 1
            )


increaseDepthCollection : Breadcrumbs -> Breadcrumbs
increaseDepthCollection breadcrumbs =
    { breadcrumbs | depthCollection = breadcrumbs.depthCollection + 1 }


{-| -}
initWith :
    Config e f m (Model f) (Msg f m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Model f
    -> Result e (Maybe (Updated (Model f) msg))
initWith _ _ _ _ _ _ =
    -- FIXME Since we allow blank nodes as focus nodes for each collection
    -- item, it is not possible to partially init the components from a new
    -- graph, as in the new graph the blank nodes might have changed and cannot
    -- be mapped any more.  We therefore do not update the collection items,
    -- which could lead to stale UI.  We could add some indicator in the UI at
    -- some point.
    --
    -- This also means, that the instance's label will not be updated in the
    -- UI.
    Ok Nothing



-- UPDATE


{-| -}
type Msg f m
    = NoOp
    | ApiReturnedResource (Maybe (List Option))
    | ApiReturnedDecoded Option (Api.Response Decoded.Field)
    | UserChangedDropdownMultiselectQuery String
    | UserSelectedValueInDropdownMultiselect Option
    | UserUnselectedValueInDropdownMultiselect Option
    | MsgForm Int m


{-| -}
update :
    Config e f m (Model f) (Msg f m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Msg f m
    -> Model f
    -> Updated (Model f) m
update config populateDefaultValues c shared seed msg ((Model ({ breadcrumbs } as data)) as collection) =
    case msg of
        NoOp ->
            Updated.from seed collection

        ApiReturnedResource Nothing ->
            -- FIXME Report this to user
            Updated.from seed collection

        ApiReturnedResource (Just options) ->
            { data
                | options =
                    [ data.options
                    , options
                    ]
                        |> List.concat
                        |> List.uniqueBy .iri
                        |> Maybe.apply
                            (OrderBy.sortBy .iri c.locale shared.graphCombinedFrontend)
                            data.grouped.selectOne.orderBy
            }
                |> Model
                |> Updated.from seed

        ApiReturnedDecoded _ (Err error) ->
            collection
                |> Updated.from seed
                |> Updated.withEffect (Effect.addToastError error)

        ApiReturnedDecoded value (Ok field) ->
            case Decoded.singleSelectOne field of
                Nothing ->
                    Updated.from seed collection

                Just { nodeFocus } ->
                    let
                        breadcrumbsNested =
                            breadcrumbs
                                |> increaseDepthCollection

                        appendInstance form =
                            Model
                                { data
                                    | instances =
                                        data.instances
                                            ++ [ { breadcrumbs = breadcrumbsNested
                                                 , form = form
                                                 , nodeFocus = nodeFocus
                                                 , value = value
                                                 }
                                               ]
                                }
                    in
                    config.init populateDefaultValues c shared seed breadcrumbsNested field
                        |> Updated.map appendInstance
                        |> Updated.needsPersisting
                        |> Updated.mapEffect (MsgForm (List.length data.instances) >> config.toMsg)

        UserChangedDropdownMultiselectQuery query ->
            { data | query = query }
                |> Model
                |> Updated.from seed

        UserSelectedValueInDropdownMultiselect value ->
            collection
                |> Updated.from seed
                |> Updated.withEffect
                    (data.grouped.field
                        |> Effect.CreateField
                            (ApiReturnedDecoded value)
                            shared.preview
                            (Breadcrumbs.Ordinal (List.length data.instances) :: data.path)
                            -- FIXME Mint from all classes
                            Decoded.MintBlankNode
                        |> Effect.map config.toMsg
                    )

        UserUnselectedValueInDropdownMultiselect value ->
            { data | instances = List.filter (\instance -> instance.value /= value) data.instances }
                |> Model
                |> Updated.from seed
                |> Updated.needsPersisting

        MsgForm index msgForm ->
            case List.getAt index data.instances of
                Nothing ->
                    Updated.from seed collection

                Just instance ->
                    config.update c shared seed msgForm instance.form
                        |> Updated.map
                            (\formUpdated ->
                                Model
                                    { data
                                        | instances =
                                            List.setAt index
                                                { instance | form = formUpdated }
                                                data.instances
                                    }
                            )
                        |> Updated.mapEffect (MsgForm index >> config.toMsg)


{-| -}
expandAll : Config e f m (Model f) (Msg f m) -> Model f -> Model f
expandAll _ model =
    model



-- SUBSCRIPTIONS


{-| -}
subscriptions : Config e f m (Model f) (Msg f m) -> Model f -> Sub m
subscriptions config (Model data) =
    data.instances
        |> List.map (.form >> config.subscriptions)
        |> Sub.batch



-- VIEW


{-| -}
view :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Model f
    -> Maybe (Container m)
view config c report _ model =
    viewHelp config c report model
        |> Container.expand
        |> Just


viewHelp :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Model f
    -> Element m
viewHelp =
    lazy4
        (\config c report ((Model data) as model) ->
            let
                notSelected option =
                    not (List.member option selected)

                selected =
                    List.map .value data.instances
            in
            { id = Breadcrumbs.pointer data.path
            , label = ComboboxMultiselect.labelAbove (Ui.label c data.grouped)
            , initialQuery = data.query
            , supportingText = Nothing
            , help = []
            , options =
                data.options
                    |> List.filter notSelected
                    |> sortOptionsByScore data.query
            , selected = selected
            , toOption = optionToOptionCombobox
            , fromOption = optionFromOptionCombobox data.options
            , onChange = UserChangedDropdownMultiselectQuery
            , onSelect = UserSelectedValueInDropdownMultiselect
            , onUnselect = UserUnselectedValueInDropdownMultiselect
            , onCopy = Nothing
            , onPaste = Nothing
            , noOp = NoOp
            , renderSubLabels = c.preferences.renderIrisInDropdownMenus
            }
                |> comboboxMultiselect
                |> errorWrapper (help config c report model)
                |> Element.map config.toMsg
        )


{-| -}
viewFullscreen :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Model f
    -> Maybe (Element m)
viewFullscreen config c report isDragging (Model data) =
    data.instances
        |> List.indexedMap Tuple.pair
        |> List.filterMap
            (\( index, instance ) ->
                instance.form
                    |> config.fullscreen c report isDragging
                    |> Maybe.map (Element.map (MsgForm index >> config.toMsg))
            )
        |> List.head


{-| -}
card :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Model f
    -> Maybe (Container m)
card _ c (Model data) =
    data.instances
        |> List.map
            (\{ value } ->
                { url = c.toUrl data.grouped.selectOne.class value.iri
                , label = verbatim value.value
                }
            )
        |> Labelled.linkList (Ui.label c data.grouped)
        |> Container.expand
        |> Just


{-| -}
columns :
    Config e f m (Model f) (Msg f m)
    -> C
    -> List String
    -> Grouped.DataCollectionSelect
    -> List (Table.Column f m)
columns config c scope grouped =
    [ { header = viewHeader c scope grouped
      , width = Element.shrink
      , view = viewCell config
      }
    ]


viewHeader : C -> List String -> Grouped.DataCollectionSelect -> TableCell m
viewHeader c scope grouped =
    grouped
        |> Ui.label c
        |> TableCell.textBold
        |> withInfo scope


viewCell : Config e f m (Model f) (Msg f m) -> f -> TableCell mb
viewCell config form =
    case config.toField form of
        Nothing ->
            TableCell.empty

        Just (Model data) ->
            if List.isEmpty data.instances then
                TableCell.empty

            else
                data.instances
                    |> List.map
                        (\{ value } ->
                            { url = Rdf.toUrl value.iri
                            , label = verbatim value.value
                            }
                        )
                    |> TableCell.linkList


{-| -}
help : Config e f m (Model f) (Msg f m) -> C -> Report -> Model f -> List Fluent
help config c report (Model data) =
    helpCollection c report data
        ++ List.concatMap
            (.form
                >> config.nodeFocus
                >> helpInstanceForSummary c report data.nodeFocus data
            )
            data.instances


helpCollection : C -> Report -> Data form -> List Fluent
helpCollection c report data =
    report.results
        |> List.filter
            (\((ValidationResult { value }) as validationResult) ->
                Report.validationResultFor
                    (Report.emptySelector
                        |> Maybe.apply Report.selectFocusNode (Rdf.toIri data.nodeFocus)
                        |> Report.selectPath data.grouped.path
                    )
                    validationResult
                    && (value == Nothing)
            )
        |> List.filterMap (helpCollectionFromValidationResult c data)


helpCollectionFromValidationResult : C -> Data form -> ValidationResult -> Maybe Fluent
helpCollectionFromValidationResult c data (ValidationResult { constraintComponent, message }) =
    let
        name =
            data.grouped.names
                |> List.head
                |> Maybe.map (Localize.withinSentenceWithArticleIndefinite c)
                |> Maybe.withDefault "a value"

        namePlural =
            data.grouped.names
                |> List.head
                |> Maybe.map (Localize.withinSentencePlural c)
                |> Maybe.withDefault "values"
    in
    case constraintComponent of
        MinCountConstraintComponent ->
            if data.grouped.maxCount - data.grouped.minCount > 0 then
                Just
                    (verbatim
                        ("Enter "
                            ++ String.fromInt data.grouped.minCount
                            ++ " or more "
                            ++ namePlural
                            ++ "."
                        )
                    )

            else if data.grouped.minCount == 1 then
                Just (verbatim ("Enter " ++ name ++ "."))

            else
                Just
                    (verbatim
                        ("Enter "
                            ++ String.fromInt data.grouped.minCount
                            ++ " "
                            ++ namePlural
                            ++ "."
                        )
                    )

        MaxCountConstraintComponent ->
            Just
                (verbatim
                    ("Enter at most "
                        ++ String.fromInt data.grouped.maxCount
                        ++ " "
                        ++ namePlural
                        ++ "."
                    )
                )

        _ ->
            Just (verbatim (Localize.verbatim c message ++ "."))


helpInstanceForSummary : C -> Report -> BlankNodeOrIri -> Data form -> BlankNodeOrIri -> List Fluent
helpInstanceForSummary c report nodeFocus data nodeInstance =
    report.results
        |> List.filter
            (\((ValidationResult _) as validationResult) ->
                Report.validationResultFor
                    (Report.emptySelector
                        |> Maybe.apply Report.selectFocusNode (Rdf.toIri nodeFocus)
                        |> Report.selectPath data.grouped.path
                        |> Maybe.apply Report.selectValue (Rdf.toAnyLiteralOrIri nodeInstance)
                    )
                    validationResult
            )
        |> List.filterMap (helpInstanceForSummaryFromValidationResult c data)


helpInstanceForSummaryFromValidationResult : C -> Data form -> ValidationResult -> Maybe Fluent
helpInstanceForSummaryFromValidationResult c data (ValidationResult { constraintComponent, message }) =
    let
        name =
            data.grouped.names
                |> List.head
                |> Maybe.map (Localize.withinSentenceWithArticleIndefinite c)
                |> Maybe.withDefault "a value"
    in
    case constraintComponent of
        NodeConstraintComponent ->
            Just (verbatim ("There is a problem with " ++ name ++ "."))

        _ ->
            Just (verbatim (Localize.verbatim c message ++ "."))


{-| -}
summary : Config e f m (Model f) (Msg f m) -> C -> Model f -> List Fluent
summary _ c (Model data) =
    case data.options of
        [] ->
            []

        [ option ] ->
            [ Fluent.verbatim option.value ]

        _ ->
            case List.head data.grouped.names of
                Nothing ->
                    []

                Just name ->
                    [ Fluent.verbatim
                        (String.concat
                            [ String.fromInt (List.length data.options)
                            , " "
                            , Localize.withinSentencePlural c name
                            ]
                        )
                    ]



-- OUTPUT


{-| -}
encoders :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Model f
    -> List Encode.PropertyEncoder
encoders config c (Model data) =
    data.instances
        |> List.map (encoderInstance config c data)


encoderInstance :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Data f
    -> Instance f
    -> PropertyEncoder
encoderInstance config c data instance =
    Encode.property data.grouped.path
        (Encode.node (config.nodeFocus instance.form)
            (List.concat
                [ encodersClasses data
                , encodersIsDefinedBy config data instance
                , config.encoders c instance.form
                , [ Encode.from instance.nodeFocus
                        [ Encode.property data.grouped.selectOne.path
                            (Encode.node instance.value.iri
                                [ Encode.predicate a
                                    (Encode.iri data.grouped.selectOne.class)
                                ]
                            )
                        ]
                  ]
                ]
            )
        )


encodersClasses : Data f -> List PropertyEncoder
encodersClasses data =
    List.map
        (Encode.predicate a << Encode.object)
        (NonEmpty.toList data.grouped.classes)


encodersIsDefinedBy :
    Config e f m (Model f) (Msg f m)
    -> Data f
    -> Instance f
    -> List PropertyEncoder
encodersIsDefinedBy config data instance =
    let
        iriDocumentWithFragment =
            Rdf.setFragment (Breadcrumbs.id (config.path instance.form))
                data.iriDocument
    in
    [ Encode.predicate RDFS.isDefinedBy
        (Encode.object iriDocumentWithFragment)
    ]



-- INFO


{-| -}
toNodeFocus : Model f -> BlankNodeOrIri
toNodeFocus (Model data) =
    data.nodeFocus


{-| -}
toPath : Model f -> Path
toPath (Model data) =
    data.path


{-| -}
isEmpty : Config e f m (Model f) (Msg f m) -> Model f -> Bool
isEmpty _ _ =
    False


{-| -}
dragging : Config e f m (Model f) (Msg f m) -> Model f -> Bool
dragging config (Model data) =
    List.any (.form >> config.dragging) data.instances


{-| -}
log : Config e f m (Model f) (Msg f m) -> Model f -> Pretty.Doc ()
log config (Model data) =
    Pretty.lines
        [ Pretty.string "Collection"
        , Pretty.indent 2 (Pretty.lines (List.map (.form >> config.log) data.instances))
        ]
