module Shacl.Form.Viewed.IriReadOnly exposing
    ( Model
    , init, initWith
    , Msg, update, expandAll
    , subscriptions
    , view, viewFullscreen, card, columns, help, summary
    , encoders
    , toNodeFocus, toPath, isEmpty, dragging, log
    )

{-|


# Model

@docs Model


# Init

@docs init, initWith


# Update

@docs Msg, update, expandAll


# Subscriptions

@docs subscriptions


# View

@docs view, viewFullscreen, card, columns, help, summary


# Output

@docs encoders


# Info

@docs toNodeFocus, toPath, isEmpty, dragging, log

-}

import Accessibility.Clipboard as Clipboard
import Element
    exposing
        ( Element
        , el
        , none
        , paddingXY
        , shrink
        )
import Element.Extra as Element
import Element.Lazy exposing (lazy3)
import Fluent exposing (Fluent, verbatim)
import Pretty
import Rdf exposing (BlankNodeOrIri, Iri, StringOrLangString)
import Rdf.Decode as Decode exposing (Decoder)
import Rdf.Decode.Extra as Decode
import Rdf.Encode as Encode
import Rdf.Graph exposing (Seed)
import Rdf.Namespaces.RDFS as RDFS
import Shacl.Clipboard as Clipboard
import Shacl.Form.Breadcrumbs exposing (Breadcrumbs, Path)
import Shacl.Form.Context exposing (C)
import Shacl.Form.Decoded as Decoded
import Shacl.Form.Effect as Effect
import Shacl.Form.Localize as Localize
import Shacl.Form.Shared as Shared exposing (Config, Shared, withInfo)
import Shacl.Form.Translated as Translated
import Shacl.Form.Ui as Ui
import Shacl.Form.Ui.Container as Container exposing (Container)
import Shacl.Form.Updated as Updated exposing (Updated)
import Shacl.Report exposing (Report)
import Ui.Atom.ButtonIcon as ButtonIcon
import Ui.Atom.Icon as Icon
import Ui.Atom.Labelled as Labelled
import Ui.Molecule.Table as Table
import Ui.Molecule.TableCell as TableCell exposing (TableCell)



-- MODEL


{-| -}
type Model
    = Model Data


type alias Data =
    { path : Path
    , breadcrumbs : Breadcrumbs
    , translated : Translated.DataIriReadOnly
    , nodeFocus : BlankNodeOrIri
    , value : Maybe LabelledIri
    }


type alias LabelledIri =
    { iri : Iri
    , label : Maybe StringOrLangString
    }



-- INIT


{-| -}
init :
    Config e f m Model Msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Breadcrumbs
    -> Decoded.DataIriReadOnly
    -> Updated Model m
init =
    Shared.init
        { value = \_ _ decoded -> decoded.value
        , default = \_ _ { translated } -> translated.defaultValue
        , path = .translated >> .path
        , withValue =
            \_ _ _ seed breadcrumbs { path, translated, nodeFocus } value ->
                { path = path
                , breadcrumbs = breadcrumbs
                , translated = translated
                , nodeFocus = nodeFocus
                , value = Just value
                }
                    |> Model
                    |> Updated.from seed
        , empty =
            \_ _ _ seed breadcrumbs { path, translated, nodeFocus } ->
                { path = path
                , breadcrumbs = breadcrumbs
                , translated = translated
                , nodeFocus = nodeFocus
                , value = Nothing
                }
                    |> Model
                    |> Updated.from seed
        }


{-| -}
initWith :
    Config e f m Model Msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Model
    -> Result e (Maybe (Updated Model m))
initWith =
    Shared.initWith
        { decoder = \c shared (Model data) -> decoder c shared data.translated
        , default = \_ _ (Model data) -> data.translated.defaultValue
        , nodeFocus = \(Model data) -> data.nodeFocus
        , path = \(Model data) -> data.translated.path
        , updateValue = updateValue
        , removeValue = removeValue
        , tagField = Translated.TagFieldIriReadOnly
        }


updateValue :
    Config e f m Model Msg
    -> C
    -> Shared
    -> Seed
    -> LabelledIri
    -> Model
    -> Maybe (Updated Model m)
updateValue _ _ _ seed iri (Model data) =
    if Just iri == data.value then
        Nothing

    else
        { data | value = Just iri }
            |> Model
            |> Updated.from seed
            |> Just


removeValue :
    Config e f m Model Msg
    -> C
    -> Shared
    -> Seed
    -> Model
    -> Maybe (Updated Model m)
removeValue _ _ _ seed (Model data) =
    if data.value == Nothing then
        Nothing

    else
        { data | value = Nothing }
            |> Model
            |> Updated.from seed
            |> Just



-- UPDATE


{-| -}
type Msg
    = UserCopied


{-| -}
update :
    Config e f m Model Msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Msg
    -> Model
    -> Updated Model m
update config _ c _ seed msg ((Model data) as model) =
    case msg of
        UserCopied ->
            model
                |> Updated.from seed
                |> Updated.withEffect
                    (encoders config c model
                        |> Clipboard.serialize data.nodeFocus seed
                        |> Effect.copyToClipboard
                    )


{-| -}
expandAll : Config e f m Model Msg -> Model -> Model
expandAll =
    \_ -> identity



-- SUBSCRIPTIONS


{-| -}
subscriptions : Config e f m Model Msg -> Model -> Sub m
subscriptions =
    \_ _ -> Sub.none



-- VIEW


{-| -}
view :
    Config e f m Model Msg
    -> C
    -> Report
    -> Bool
    -> Model
    -> Maybe (Container m)
view config c _ _ model =
    viewHelp config c model
        |> Container.wrappable
        |> Just


viewHelp :
    Config e f m Model Msg
    -> C
    -> Model
    -> Element m
viewHelp =
    lazy3
        (\config c (Model data) ->
            data.value
                |> Maybe.map (viewValue c data >> Element.map config.toMsg)
                |> Maybe.withDefault none
        )


viewValue : C -> Data -> LabelledIri -> Element Msg
viewValue c data value =
    Element.row
        [ paddingXY 16 4
        , Clipboard.onCopy UserCopied
        , Element.class "shacl-form-input-iri-read-only"
        ]
        [ viewLink c data value
        , viewButtonCopy
        ]


viewButtonCopy : Element Msg
viewButtonCopy =
    Icon.ContentCopy
        |> ButtonIcon.buttonIcon UserCopied
        |> ButtonIcon.withAttributes
            [ Element.class "shacl-form-input-iri-read-only__button-copy" ]
        |> ButtonIcon.toElement


{-| -}
viewFullscreen :
    Config e f m Model Msg
    -> C
    -> Report
    -> Bool
    -> Model
    -> Maybe (Element m)
viewFullscreen _ _ _ _ _ =
    Nothing


{-| -}
card : Config e f m Model Msg -> C -> Model -> Maybe (Container m)
card _ c (Model data) =
    data.value
        |> Maybe.map (cardHelp c data)
        |> Maybe.map Container.wrappable


cardHelp : C -> Data -> LabelledIri -> Element msg
cardHelp c data value =
    el [ paddingXY 16 4 ]
        (viewLink c data value)


viewLink : C -> Data -> LabelledIri -> Element msg
viewLink c data { iri, label } =
    Labelled.link (Ui.label c data.translated)
        { url = Rdf.toUrl iri
        , label =
            label
                |> Maybe.map (Localize.forLabel c)
                |> Maybe.withDefault "Label missing"
                |> verbatim
        }


{-| -}
columns :
    Config e f m Model Msg
    -> C
    -> List String
    -> Translated.DataIriReadOnly
    -> List (Table.Column f m)
columns config c scope translated =
    [ { header = viewHeader c scope translated
      , width = shrink
      , view = viewCell config c
      }
    ]


viewHeader : C -> List String -> Translated.DataIriReadOnly -> TableCell m
viewHeader c scope translated =
    translated
        |> Ui.label c
        |> TableCell.textBold
        |> withInfo scope


viewCell : Config e f m Model Msg -> C -> f -> TableCell m
viewCell config c form =
    case config.toField form of
        Nothing ->
            TableCell.empty

        Just (Model data) ->
            data.value
                |> Maybe.map
                    (\{ iri, label } ->
                        { url = Rdf.toUrl iri
                        , label =
                            label
                                |> Maybe.map (Localize.forLabel c)
                                |> Maybe.withDefault "Label missing"
                                |> verbatim
                        }
                            |> TableCell.link
                    )
                |> Maybe.withDefault TableCell.empty


{-| -}
help : Config e f m Model Msg -> C -> Report -> Model -> List Fluent
help _ _ _ _ =
    []


{-| -}
summary : Config e f m Model Msg -> C -> Model -> List Fluent
summary _ _ _ =
    []



-- OUTPUT


{-| -}
encoders : Config e f m Model Msg -> C -> Model -> List Encode.PropertyEncoder
encoders _ _ _ =
    -- FIXME Do we want to serialize this?
    []



-- INFO


{-| -}
toNodeFocus : Model -> BlankNodeOrIri
toNodeFocus (Model data) =
    data.nodeFocus


{-| -}
toPath : Model -> Path
toPath (Model data) =
    data.path


{-| -}
isEmpty : Config e f m Model Msg -> Model -> Bool
isEmpty =
    \_ _ -> False


{-| -}
dragging : Config e f m Model Msg -> Model -> Bool
dragging =
    \_ _ -> False


{-| -}
log : Config e f m Model Msg -> Model -> Pretty.Doc t
log _ _ =
    Pretty.string "IRI Read Only"



-- DECODE


decoder : C -> Shared -> Translated.DataIriReadOnly -> Decoder LabelledIri
decoder _ _ translated =
    Decode.property translated.path decoderValue


decoderValue : Decoder LabelledIri
decoderValue =
    Decode.succeed LabelledIri
        |> Decode.custom Decode.iri
        |> Decode.optionalStringOrLangString RDFS.label
