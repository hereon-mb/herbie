module Shacl.Form.Viewed.SelectOne exposing
    ( Model
    , init, initWith
    , Msg, update, expandAll
    , subscriptions
    , view, viewFullscreen, card, columns, help, summary
    , encoders
    , toNodeFocus, toPath, isEmpty, dragging, log
    )

{-|


# Model

@docs Model


# Init

@docs init, initWith


# Update

@docs Msg, update, expandAll


# Subscriptions

@docs subscriptions


# View

@docs view, viewFullscreen, card, columns, help, summary


# Output

@docs encoders


# Info

@docs toNodeFocus, toPath, isEmpty, dragging, log

-}

import Api
import Element as Ui exposing (Element)
import Element.Extra exposing (preventDefaultOnClick)
import Element.Lazy exposing (lazy4)
import Fluent exposing (Fluent, verbatim)
import List.Extra as List
import Maybe.Apply as Maybe
import Maybe.Extra as Maybe
import Ontology.Shacl exposing (Class)
import Pretty
import Rdf exposing (BlankNodeOrIri)
import Rdf.Decode as Decode exposing (Decoder)
import Rdf.Decode.Extra as Decode
import Rdf.Encode as Encode
import Rdf.Extra as Rdf
import Rdf.Graph exposing (Seed)
import Rdf.Namespaces exposing (a)
import Rdf.Namespaces.RDFS as RDFS
import Shacl.Clipboard as Clipboard
import Shacl.Form.Breadcrumbs as Breadcrumbs exposing (Breadcrumbs, Path)
import Shacl.Form.Context exposing (C)
import Shacl.Form.Decoded as Decoded
import Shacl.Form.Effect as Effect
import Shacl.Form.Localize as Localize
import Shacl.Form.Shared as Shared
    exposing
        ( Config
        , Option
        , Shared
        , optionFromOptionCombobox
        , optionFromString
        , optionToOptionCombobox
        , optionToString
        , sortOptionsByScore
        , withInfo
        )
import Shacl.Form.Translated as Translated
import Shacl.Form.Ui as Ui
import Shacl.Form.Ui.Container as Container exposing (Container)
import Shacl.Form.Updated as Updated exposing (Updated)
import Shacl.OrderBy as OrderBy
import Shacl.Report as Report exposing (ConstraintComponent(..), Report, ValidationResult(..))
import Ui.Atom.Button as Button exposing (button)
import Ui.Atom.ChoiceChips as ChoiceChips exposing (choiceChips)
import Ui.Atom.ErrorWrapper exposing (errorWrapper)
import Ui.Atom.Labelled as Labelled
import Ui.Atom.Radio as Radio
import Ui.Molecule.ComboboxSingleselect as ComboboxSingleselect exposing (comboboxSingleselect)
import Ui.Molecule.Table as Table
import Ui.Molecule.TableCell as TableCell exposing (TableCell)



-- MODEL


{-| -}
type Model
    = Model Data


type alias Data =
    { path : Path
    , breadcrumbs : Breadcrumbs
    , translated : Translated.DataSelectOne
    , nodeFocus : BlankNodeOrIri
    , workspaceUuid : String
    , options : List Option
    , query : String
    , value : Maybe Option
    , classesWithShaclShapes : List Class
    }



-- INIT


{-| -}
init :
    Config e f m Model Msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Breadcrumbs
    -> Decoded.DataSelectOne
    -> Updated Model m
init =
    Shared.init
        { value = \_ _ { selected } -> selected
        , default =
            \_ _ { translated } ->
                translated.defaultValue
                    |> Maybe.map
                        (\value ->
                            { iri = value.this
                            , label = Just value.label
                            }
                        )
        , path = .translated >> .path
        , withValue =
            \config c shared seed breadcrumbs decoded selected ->
                { path = decoded.path
                , breadcrumbs = breadcrumbs
                , translated = decoded.translated
                , nodeFocus = decoded.nodeFocus
                , workspaceUuid = shared.workspaceUuid
                , options = optionsFromDecoded c shared decoded
                , query = ""
                , value =
                    Just
                        { iri = selected.iri
                        , value = Maybe.unwrap "" (Localize.forLabel c) selected.label
                        }
                , classesWithShaclShapes = []
                }
                    |> Model
                    |> Updated.from seed
                    |> Updated.withEffect
                        (Effect.map config.toMsg
                            (Effect.batch
                                [ Effect.getResource
                                    ApiReturnedResource
                                    decoded.translated.class
                                , Effect.getSubclassesWithShaclShapes
                                    ApiReturnedSubclassesWithShaclShapes
                                    decoded.translated.class
                                ]
                            )
                        )
        , empty =
            \config c shared seed breadcrumbs decoded ->
                { path = decoded.path
                , breadcrumbs = breadcrumbs
                , translated = decoded.translated
                , nodeFocus = decoded.nodeFocus
                , workspaceUuid = shared.workspaceUuid
                , options = optionsFromDecoded c shared decoded
                , query = ""
                , value = Nothing
                , classesWithShaclShapes = []
                }
                    |> Model
                    |> Updated.from seed
                    |> Updated.withEffect
                        (Effect.map config.toMsg
                            (Effect.batch
                                [ Effect.getResource
                                    ApiReturnedResource
                                    decoded.translated.class
                                , Effect.getSubclassesWithShaclShapes
                                    ApiReturnedSubclassesWithShaclShapes
                                    decoded.translated.class
                                ]
                            )
                        )
        }


optionsFromDecoded : C -> Shared -> Decoded.DataSelectOne -> List Option
optionsFromDecoded c shared decoded =
    decoded.translated.options
        |> List.map (valueToOption c)
        |> List.uniqueBy .iri
        |> Maybe.apply
            (OrderBy.sortBy .iri c.locale shared.graphCombinedFrontend)
            decoded.translated.orderBy


valueToOption : C -> Translated.Value -> Option
valueToOption c value =
    { iri = value.this
    , value = Localize.forLabel c value.label
    }


{-| -}
initWith :
    Config e f m Model Msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Model
    -> Result e (Maybe (Updated Model m))
initWith =
    Shared.initWith
        { decoder = \c shared (Model data) -> decoder c shared data.translated
        , default =
            \c _ (Model data) ->
                data.translated.defaultValue
                    |> Maybe.map
                        (\value ->
                            { iri = value.this
                            , value = Localize.forLabel c value.label
                            }
                        )
        , nodeFocus = \(Model data) -> data.nodeFocus
        , path = \(Model data) -> data.translated.path
        , updateValue = updateValue
        , removeValue = removeValue
        , tagField = Translated.TagFieldSelectOne
        }


updateValue :
    Config e f m Model Msg
    -> C
    -> Shared
    -> Seed
    -> Option
    -> Model
    -> Maybe (Updated Model m)
updateValue _ _ _ seed option (Model data) =
    if Just option.iri == Maybe.map .iri data.value then
        Nothing

    else
        { data | value = Just option }
            |> Model
            |> Updated.from seed
            |> Just


removeValue :
    Config e f m Model Msg
    -> C
    -> Shared
    -> Seed
    -> Model
    -> Maybe (Updated Model m)
removeValue _ _ _ seed (Model data) =
    if data.value == Nothing then
        Nothing

    else
        { data | value = Nothing }
            |> Model
            |> Updated.from seed
            |> Just



-- UPDATE


{-| -}
type Msg
    = NoOp
    | ApiReturnedResource (Maybe (List Option))
    | ApiReturnedSubclassesWithShaclShapes (Api.Response (List Class))
    | UserPressedCreateNew (List Class)
    | UserChangedDropdownQuery String
    | UserChangedDropdownSelection (Maybe Option)
    | UserCopied
    | UserPasted (Maybe Option)


{-| -}
update :
    Config e f m Model Msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Msg
    -> Model
    -> Updated Model m
update config _ c shared seed msg ((Model data) as model) =
    case msg of
        NoOp ->
            Updated.from seed model

        ApiReturnedResource Nothing ->
            -- FIXME Report this to user
            Updated.from seed model

        ApiReturnedResource (Just options) ->
            { data
                | options =
                    [ data.options
                    , options
                    ]
                        |> List.concat
                        |> List.uniqueBy .iri
                        |> Maybe.apply
                            (OrderBy.sortBy .iri c.locale shared.graphCombinedFrontend)
                            data.translated.orderBy
            }
                |> Model
                |> Updated.from seed

        ApiReturnedSubclassesWithShaclShapes (Err _) ->
            -- FIXME Do we want to report the error?
            Updated.from seed model

        ApiReturnedSubclassesWithShaclShapes (Ok classesWithShaclShapes) ->
            { data | classesWithShaclShapes = classesWithShaclShapes }
                |> Model
                |> Updated.from seed

        UserPressedCreateNew classes ->
            model
                |> Updated.from seed
                |> Updated.withEffect (Effect.userIntentsToCreateDatasetFor classes)

        UserChangedDropdownQuery query ->
            { data | query = query }
                |> Model
                |> Updated.from seed

        UserChangedDropdownSelection value ->
            { data | value = value }
                |> Model
                |> Updated.from seed
                |> Updated.needsPersisting

        UserCopied ->
            model
                |> Updated.from seed
                |> Updated.withEffect
                    (encoders config c model
                        |> Clipboard.serialize data.nodeFocus seed
                        |> Effect.copyToClipboard
                    )

        UserPasted valuePasted ->
            { data | value = valuePasted }
                |> Model
                |> Updated.from seed
                |> Updated.needsPersisting


{-| -}
expandAll : Config e f m Model Msg -> Model -> Model
expandAll _ model =
    model



-- SUBSCRIPTIONS


{-| -}
subscriptions : Config e f m Model Msg -> Model -> Sub m
subscriptions _ _ =
    Sub.none



-- VIEW


{-| -}
view :
    Config e f m Model Msg
    -> C
    -> Report
    -> Bool
    -> Model
    -> Maybe (Container m)
view config c report _ model =
    viewHelp config c report model
        |> Container.wrappable
        |> Just


viewHelp :
    Config e f m Model Msg
    -> C
    -> Report
    -> Model
    -> Element m
viewHelp =
    lazy4
        (\config c report ((Model data) as model) ->
            Ui.column
                [ Ui.width Ui.fill
                , Ui.spacing 8
                ]
                [ errorWrapper (help config c report model) (viewInput c data)
                , Ui.el
                    [ Ui.width Ui.fill
                    , preventDefaultOnClick NoOp
                    ]
                    (viewCreateNew data)
                ]
                |> Ui.map config.toMsg
        )


viewInput : C -> Data -> Element Msg
viewInput c data =
    case data.translated.variant of
        Translated.Dropdown ->
            if List.length data.options > 5 then
                viewDropdown c data

            else
                viewChoiceChips c data

        Translated.RadioButtons ->
            viewRadioButtons c data


viewDropdown : C -> Data -> Element Msg
viewDropdown c data =
    comboboxSingleselect
        { id = Breadcrumbs.pointer data.path
        , label = ComboboxSingleselect.labelAbove (Ui.label c data.translated)
        , supportingText = Ui.description c data.translated
        , help = []
        , options = sortOptionsByScore data.query data.options
        , selected = data.value
        , toOption = optionToOptionCombobox
        , fromOption = optionFromOptionCombobox data.options
        , onChange = UserChangedDropdownQuery
        , onSelect = UserChangedDropdownSelection << Just
        , onUnselect = UserChangedDropdownSelection Nothing
        , onCopy = Just UserCopied
        , onPaste = Just ( "text/plain", NoOp, onPaste c data )
        , noOp = NoOp
        , renderSubLabels = c.preferences.renderIrisInDropdownMenus
        }


viewChoiceChips : C -> Data -> Element Msg
viewChoiceChips c data =
    choiceChips
        { id = Breadcrumbs.pointer data.path
        , label = ChoiceChips.labelAbove (Ui.label c data.translated)
        , help = []
        , chips = chips data.options
        , selected = data.value
        , onChange = UserChangedDropdownSelection << Just
        , onCopy = Just UserCopied
        , onPaste = Just ( "text/plain", NoOp, onPaste c data )
        , toString = optionToString
        , fromString = optionFromString data.options
        }


chips : List Option -> List (ChoiceChips.Chip Option)
chips =
    List.map chip


chip : Option -> ChoiceChips.Chip Option
chip option =
    { option = option
    , label = verbatim option.value
    }


viewRadioButtons : C -> Data -> Element Msg
viewRadioButtons c data =
    optionsRadioButtons data.options
        |> Radio.radio (UserChangedDropdownSelection << Just) (Ui.label c data.translated) data.value
        |> Radio.withId (Breadcrumbs.id data.path)
        |> Radio.onCopy UserCopied
        |> Radio.onPaste "text/plain" NoOp (onPaste c data)
        |> Radio.view


optionsRadioButtons : List Option -> List (Radio.Option Option)
optionsRadioButtons =
    List.map (\option -> Radio.option option (verbatim option.value))


onPaste : C -> Data -> String -> Maybe Msg
onPaste c data =
    Clipboard.allObjectsOne UserPasted (Decode.map Just (decoderValue c data.translated))


viewCreateNew : Data -> Element Msg
viewCreateNew data =
    if List.isEmpty data.classesWithShaclShapes then
        Ui.none

    else
        Ui.el [ Ui.alignRight ]
            ("Create new"
                |> verbatim
                |> button (UserPressedCreateNew data.classesWithShaclShapes)
                |> Button.withFormat Button.Text
                |> Button.view
            )


{-| -}
viewFullscreen :
    Config e f m Model Msg
    -> C
    -> Report
    -> Bool
    -> Model
    -> Maybe (Element m)
viewFullscreen _ _ _ _ _ =
    Nothing


{-| -}
card : Config e f m Model Msg -> C -> Model -> Maybe (Container msg)
card _ c (Model data) =
    data.value
        |> Maybe.map (linkConfigFromOption c data >> Labelled.link (Ui.label c data.translated))
        |> Maybe.map Container.expand


{-| -}
columns :
    Config e f m Model Msg
    -> C
    -> List String
    -> Translated.DataSelectOne
    -> List (Table.Column f m)
columns config c scope translated =
    [ { header = viewHeader c scope translated
      , width = Ui.shrink
      , view = viewCell config c
      }
    ]


viewHeader : C -> List String -> Translated.DataSelectOne -> TableCell m
viewHeader c scope translated =
    translated
        |> Ui.label c
        |> TableCell.textBold
        |> withInfo scope


viewCell : Config e f m Model Msg -> C -> f -> TableCell m
viewCell config c form =
    case config.toField form of
        Nothing ->
            TableCell.empty

        Just (Model data) ->
            data.value
                |> Maybe.map (linkConfigFromOption c data >> TableCell.link)
                |> Maybe.withDefault TableCell.empty


linkConfigFromOption : C -> Data -> Option -> { url : String, label : Fluent }
linkConfigFromOption c data option =
    { url = c.toUrl data.translated.class option.iri
    , label = verbatim option.value
    }


{-| -}
help : Config e f m Model Msg -> C -> Report -> Model -> List Fluent
help _ c report (Model data) =
    report.results
        |> Report.validationResultsFor
            (Report.emptySelector
                |> Maybe.apply Report.selectFocusNode (Rdf.toIri data.nodeFocus)
                |> Report.selectPath data.translated.path
                |> Maybe.apply Report.selectSourceShape (Rdf.toIri data.translated.node)
            )
        |> List.filterMap (helpFromValidationResult c data)


helpFromValidationResult : C -> Data -> ValidationResult -> Maybe Fluent
helpFromValidationResult c data (ValidationResult { constraintComponent, message }) =
    let
        minCountConstraint =
            Just (verbatim ("Select " ++ name))

        maxCountConstraint =
            Just (verbatim ("You have selected too many " ++ namePlural))

        name =
            data.translated.names
                |> List.head
                |> Maybe.map (Localize.withinSentenceWithArticleIndefinite c)
                |> Maybe.withDefault "a value"

        namePlural =
            data.translated.names
                |> List.head
                |> Maybe.map (Localize.withinSentencePlural c)
                |> Maybe.withDefault "values"
    in
    case constraintComponent of
        MinCountConstraintComponent ->
            minCountConstraint

        QualifiedMinCountConstraintComponent ->
            minCountConstraint

        MaxCountConstraintComponent ->
            maxCountConstraint

        QualifiedMaxCountConstraintComponent ->
            maxCountConstraint

        _ ->
            Just (verbatim (Localize.verbatim c message ++ "."))


{-| -}
summary : Config e f m Model Msg -> C -> Model -> List Fluent
summary _ _ (Model data) =
    case data.value of
        Nothing ->
            []

        Just value ->
            [ Fluent.verbatim value.value ]



-- OUTPUT


{-| -}
encoders : Config e f m Model Msg -> C -> Model -> List Encode.PropertyEncoder
encoders _ _ (Model data) =
    case data.value of
        Nothing ->
            []

        Just { iri, value } ->
            [ Encode.property data.translated.path
                (Encode.node iri
                    [ Encode.predicate a (Encode.object data.translated.class)
                    , Encode.predicate RDFS.label
                        (Encode.literal (Rdf.string value))
                    ]
                )
            ]



-- INFO


{-| -}
toNodeFocus : Model -> BlankNodeOrIri
toNodeFocus (Model data) =
    data.nodeFocus


{-| -}
toPath : Model -> Path
toPath (Model data) =
    data.path


{-| -}
isEmpty : Config e f m Model Msg -> Model -> Bool
isEmpty _ (Model data) =
    Maybe.isNothing data.value


{-| -}
dragging : Config e f m Model Msg -> Model -> Bool
dragging =
    \_ _ -> False


{-| -}
log : Config e f m Model Msg -> Model -> Pretty.Doc t
log _ _ =
    Pretty.string "Select One"



-- DECODE


decoder : C -> Shared -> Translated.DataSelectOne -> Decoder Option
decoder c _ translated =
    Decode.property translated.path (decoderValue c translated)


decoderValue : C -> Translated.DataSelectOne -> Decoder Option
decoderValue c translated =
    Decode.instanceOf translated.class
        (Decode.succeed Option
            |> Decode.custom Decode.iri
            |> Decode.custom (decoderName c)
        )


decoderName : C -> Decoder String
decoderName c =
    Decode.oneOf
        [ Decode.predicate RDFS.label (Decode.map (Localize.forLabel c) Decode.stringOrLangString)
        , Decode.andThen
            (\iri ->
                iri
                    |> Rdf.defaultLabel
                    |> Maybe.map Decode.succeed
                    |> Maybe.withDefault
                        (Decode.fail
                            ("could not pick a good name for instance "
                                ++ Rdf.serializeNode iri
                            )
                        )
            )
            Decode.iri
        ]
