module Shacl.Form.Viewed.Variants exposing
    ( Model
    , init, initWith
    , Msg, update, expandAll
    , subscriptions
    , view, viewFullscreen, card, columns, help, summary
    , encoders
    , toNodeFocus, toPath, isEmpty, dragging, log
    )

{-|


# Model

@docs Model


# Init

@docs init, initWith


# Update

@docs Msg, update, expandAll


# Subscriptions

@docs subscriptions


# View

@docs view, viewFullscreen, card, columns, help, summary


# Output

@docs encoders


# Info

@docs toNodeFocus, toPath, isEmpty, dragging, log

-}

import Accessibility.Clipboard as Clipboard
import Api
import Element
    exposing
        ( Element
        , column
        , el
        , fill
        , none
        , paddingEach
        , paddingXY
        , paragraph
        , spacing
        , width
        )
import Element.Extra as Element
import Element.Font as Font
import Element.Lazy exposing (lazy5)
import Fluent exposing (Fluent, verbatim)
import List.Extra as List
import List.NonEmpty as NonEmpty
import Pretty
import Rdf exposing (BlankNodeOrIri, Iri, StringOrLangString)
import Rdf.Decode as Decode
import Rdf.Encode as Encode exposing (PropertyEncoder)
import Rdf.Graph exposing (Seed)
import Rdf.Namespaces exposing (a)
import Rdf.Namespaces.RDFS as RDFS
import Shacl.Clipboard as Clipboard
import Shacl.Form.Breadcrumbs as Breadcrumbs exposing (Breadcrumbs, Path)
import Shacl.Form.Context exposing (C)
import Shacl.Form.Decoded as Decoded
import Shacl.Form.Effect as Effect
import Shacl.Form.Grouped as Grouped
import Shacl.Form.Localize as Localize
import Shacl.Form.Shared exposing (Config, Shared)
import Shacl.Form.Ui as Ui
import Shacl.Form.Ui.Container as Container exposing (Container)
import Shacl.Form.Updated as Updated exposing (Updated)
import Shacl.Report exposing (Report)
import Ui.Atom.ButtonIcon as ButtonIcon
import Ui.Atom.ErrorWrapper exposing (errorWrapper)
import Ui.Atom.Icon as Icon
import Ui.Atom.SegmentedButtonsSingleSelect as SegmentedButtonsSingleSelect exposing (segmentedButtonsSingleSelect)
import Ui.Molecule.Table as Table
import Ui.Theme.Color exposing (onSurfaceVariant)
import Ui.Theme.Typography exposing (body2, caption)



-- MODEL


{-| -}
type Model f
    = Model (Data f)


type alias Data f =
    { path : Path
    , breadcrumbs : Breadcrumbs
    , grouped : Grouped.DataVariants
    , nodeFocus : BlankNodeOrIri
    , iriDocument : Iri
    , selected : Int
    , form : f
    }



-- INIT


{-| -}
init :
    Config e f m (Model f) (Msg f m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Breadcrumbs
    -> Decoded.DataVariants
    -> Updated (Model f) m
init config populateDefaultValues c shared seed breadcrumbs decoded =
    let
        toModel index form =
            Model
                { path = decoded.path
                , breadcrumbs = breadcrumbs
                , grouped = decoded.grouped
                , nodeFocus = decoded.nodeFocus
                , iriDocument = shared.iri
                , selected = index
                , form = form
                }

        breadcrumbsNested =
            increaseDepthCollection breadcrumbs
    in
    decoded.selected.field
        |> config.init populateDefaultValues c shared seed breadcrumbsNested
        |> Updated.mapEffect (MsgForm >> config.toMsg)
        |> Updated.map (toModel decoded.selected.index)


{-| -}
initWith :
    Config e f m (Model f) (Msg f m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Model f
    -> Result e (Maybe (Updated (Model f) m))
initWith _ _ _ _ _ _ =
    -- FIXME
    -- let
    --     ( selected, _ ) =
    --         getSelected shared data.breadcrumbs data.grouped
    -- in
    -- if selected == data.selected then
    --     data.form
    --         |> config.initWith populateDefaultValues c shared seed
    --         |> Result.map
    --             (Maybe.map
    --                 (Updated.map
    --                     (\form ->
    --                         Model { data | form = form }
    --                     )
    --                 )
    --             )
    -- else
    --     init config populateDefaultValues c shared seed data.breadcrumbs data.grouped
    --         |> Result.map Just
    Ok Nothing



-- UPDATE


{-| -}
type Msg f m
    = UserSelectedVariant Int
    | ApiReturnedDecoded Int (Api.Response Decoded.Field)
    | UserCopied
    | UserPasted Int f
    | Ignored
    | MsgForm m


{-| -}
update :
    Config e f m (Model f) (Msg f m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Msg f m
    -> Model f
    -> Updated (Model f) m
update config populateDefaultValues c shared seed msg ((Model data) as model) =
    case msg of
        UserSelectedVariant index ->
            case
                data.grouped.variants
                    |> NonEmpty.toList
                    |> List.getAt index
            of
                Nothing ->
                    Updated.from seed model

                Just variant ->
                    model
                        |> Updated.from seed
                        |> Updated.withEffect
                            -- FIXME We dont want to mint a new node when
                            -- switching variants
                            (variant.field
                                |> Effect.CreateField
                                    (ApiReturnedDecoded index)
                                    shared.preview
                                    (Breadcrumbs.Hash variant.hash :: data.path)
                                    (Decoded.NoMint { nodeFocus = data.nodeFocus })
                                |> Effect.map config.toMsg
                            )

        ApiReturnedDecoded _ (Err error) ->
            model
                |> Updated.from seed
                |> Updated.withEffect (Effect.addToastError error)

        ApiReturnedDecoded index (Ok field) ->
            let
                { breadcrumbs } =
                    data

                breadcrumbsNested =
                    increaseDepthCollection breadcrumbs

                setFormAndSelected form =
                    Model
                        { data
                            | selected = index
                            , form = form
                        }
            in
            field
                |> config.init populateDefaultValues c shared seed breadcrumbsNested
                |> Updated.mapEffect (MsgForm >> config.toMsg)
                |> Updated.map setFormAndSelected
                |> Updated.needsPersisting

        UserCopied ->
            model
                |> Updated.from seed
                |> Updated.withEffect
                    (encoders config c model
                        |> Clipboard.serialize data.nodeFocus seed
                        |> Effect.copyToClipboard
                    )

        UserPasted indexPasted formPasted ->
            { data
                | selected = indexPasted
                , form = formPasted
            }
                |> Model
                |> Updated.from seed

        Ignored ->
            Updated.from seed model

        MsgForm msgForm ->
            config.update c shared seed msgForm data.form
                |> Updated.map (\formUpdated -> Model { data | form = formUpdated })
                |> Updated.mapEffect (MsgForm >> config.toMsg)


{-| -}
expandAll : Config e f m (Model f) (Msg f m) -> Model f -> Model f
expandAll config (Model data) =
    Model { data | form = config.expandAll data.form }



-- SUBSCRIPTIONS


{-| -}
subscriptions : Config e f m (Model f) (Msg f m) -> Model f -> Sub m
subscriptions config (Model data) =
    config.subscriptions data.form



-- VIEW


{-| -}
view :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Model f
    -> Maybe (Container m)
view config c report isDragging model =
    viewHelp config c report isDragging model
        |> Container.expand
        |> Just


viewHelp :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Model f
    -> Element m
viewHelp =
    lazy5
        (\config c report isDragging ((Model data) as model) ->
            errorWrapper (help config c report model)
                (column
                    [ width fill
                    , spacing 8
                    , Clipboard.onCopy (config.toMsg UserCopied)
                    , Clipboard.onPaste "text/plain"
                        (config.toMsg Ignored)
                        (Clipboard.allObjectsWithClasses
                            (List.concatMap .classes
                                (NonEmpty.toList data.grouped.variants)
                            )
                            (\( n, form ) -> config.toMsg (UserPasted n form))
                            (Decode.fail "TODO")
                        )
                    ]
                    [ Element.row
                        [ width fill
                        , spacing 2
                        , Element.class "shacl-form-input-variants__heading"
                        ]
                        [ viewSegmentedButtons config c data
                        , viewActionCopy config
                        ]
                    , viewSelectedVariant config c report isDragging data
                    ]
                )
        )


viewSegmentedButtons : Config e f m (Model f) (Msg f m) -> C -> Data f -> Element m
viewSegmentedButtons config c data =
    segmentedButtonsSingleSelect
        { id = Breadcrumbs.pointer data.path
        , help = []
        , buttons =
            data.grouped.variants
                |> NonEmpty.toList
                |> List.indexedMap (variantToButton c)
        , selected = data.selected
        , onChange = config.toMsg << UserSelectedVariant
        , toString = String.fromInt
        , fromString = String.toInt
        }


viewActionCopy : Config e f m (Model f) (Msg f m) -> Element m
viewActionCopy config =
    Element.el
        [ Element.paddingXY 8 0 ]
        (Icon.ContentCopy
            |> ButtonIcon.buttonIcon (config.toMsg UserCopied)
            |> ButtonIcon.withAttributes
                [ Element.class "shacl-form-input-variants__button-copy" ]
            |> ButtonIcon.excludeFromTabSequence
            |> ButtonIcon.toElement
        )


viewSelectedVariant :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Data f
    -> Element m
viewSelectedVariant config c report isDragging data =
    data.grouped.variants
        |> NonEmpty.toList
        |> List.getAt data.selected
        |> Maybe.map (viewVariant config c report isDragging data.form)
        |> Maybe.withDefault none


{-| -}
viewFullscreen :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Model f
    -> Maybe (Element m)
viewFullscreen config c report isDragging (Model data) =
    data.form
        |> config.fullscreen c report isDragging
        |> Maybe.map (Element.map (MsgForm >> config.toMsg))


variantToButton : C -> Int -> Grouped.Variant -> SegmentedButtonsSingleSelect.Button Int
variantToButton c index variant =
    { option = index
    , label = Ui.label c variant
    }


viewVariant :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> f
    -> Grouped.Variant
    -> Element m
viewVariant config c report isDragging form variant =
    column
        [ width fill
        , spacing 16
        ]
        [ variant.descriptions
            |> List.head
            |> Maybe.map (viewComment c)
            |> Maybe.withDefault none
        , config.view c report isDragging form
            |> Maybe.map (Element.map (MsgForm >> config.toMsg))
            |> Maybe.withDefault none
        ]


viewComment : C -> StringOrLangString -> Element m
viewComment c comment =
    comment
        |> Localize.verbatim c
        |> verbatim
        |> body2
        |> List.singleton
        |> paragraph
            [ width fill
            , paddingXY 16 0
            , Font.color onSurfaceVariant
            ]


{-| -}
card : Config e f m (Model f) (Msg f m) -> C -> Model f -> Maybe (Container m)
card config c (Model data) =
    data.grouped.variants
        |> NonEmpty.toList
        |> List.getAt data.selected
        |> Maybe.andThen (cardVariant config c data.form)


cardVariant :
    Config e f m (Model f) (Msg f m)
    -> C
    -> f
    -> Grouped.Variant
    -> Maybe (Container m)
cardVariant config c form variant =
    case config.card c form of
        Nothing ->
            Nothing

        Just elementForm ->
            let
                toHeading =
                    caption
            in
            [ variant.names
                |> List.head
                |> Maybe.map (Localize.forLabel c)
                |> Maybe.withDefault "Missing label"
                |> verbatim
                |> toHeading
                |> el
                    [ paddingEach
                        { top = 16
                        , bottom = 0
                        , left = 0
                        , right = 0
                        }
                    ]
            , Element.map (MsgForm >> config.toMsg) elementForm
            ]
                |> column
                    [ width fill
                    , spacing 8
                    ]
                |> Container.expand
                |> Just


{-| -}
columns :
    Config e f m (Model f) (Msg f m)
    -> C
    -> List String
    -> Grouped.DataVariants
    -> List (Table.Column f m)
columns _ _ _ _ =
    []


{-| -}
help : Config e f m (Model f) (Msg f m) -> C -> Report -> Model f -> List Fluent
help _ _ _ _ =
    []


{-| -}
summary : Config e f m (Model f) (Msg f m) -> C -> Model f -> List Fluent
summary _ c (Model data) =
    case List.getAt data.selected (NonEmpty.toList data.grouped.variants) of
        Nothing ->
            []

        Just variant ->
            [ Ui.label c variant ]



-- OUTPUT


{-| -}
encoders :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Model f
    -> List Encode.PropertyEncoder
encoders config c (Model data) =
    case List.getAt data.selected (NonEmpty.toList data.grouped.variants) of
        Nothing ->
            []

        Just variant ->
            List.concat
                [ List.map (Encode.predicate a << Encode.iri) variant.classes
                , List.map (Encode.predicate a << Encode.iri) variant.targetClasses
                , encodersIsDefinedBy config data
                , config.encoders c data.form
                ]


encodersIsDefinedBy :
    Config e f m (Model f) (Msg f m)
    -> Data f
    -> List PropertyEncoder
encodersIsDefinedBy config data =
    let
        iriDocumentWithFragment =
            Rdf.setFragment (Breadcrumbs.id (config.path data.form))
                data.iriDocument
    in
    [ Encode.predicate RDFS.isDefinedBy
        (Encode.object iriDocumentWithFragment)
    ]



-- INFO


{-| -}
toNodeFocus : Model f -> BlankNodeOrIri
toNodeFocus (Model data) =
    data.nodeFocus


{-| -}
toPath : Model f -> Path
toPath (Model data) =
    data.path


{-| -}
isEmpty : Config e f m (Model f) (Msg f m) -> Model f -> Bool
isEmpty _ _ =
    False


{-| -}
dragging : Config e f m (Model f) (Msg f m) -> Model f -> Bool
dragging config (Model data) =
    config.dragging data.form


{-| -}
log : Config e f m (Model f) (Msg f m) -> Model f -> Pretty.Doc ()
log config (Model data) =
    Pretty.lines
        [ Pretty.string "Variants"
        , Pretty.indent 2 (config.log data.form)
        ]



-- SUPPORT


increaseDepthCollection : Breadcrumbs -> Breadcrumbs
increaseDepthCollection breadcrumbs =
    { breadcrumbs | depthCollection = breadcrumbs.depthCollection + 1 }
