module Shacl.Form.Viewed.Group exposing
    ( Model
    , init, initWith
    , Msg, update, expandAll
    , subscriptions
    , view, viewFullscreen, card, columns, help, summary
    , encoders
    , toNodeFocus, toPath, isEmpty, dragging, log
    )

{-|


# Model

@docs Model


# Init

@docs init, initWith


# Update

@docs Msg, update, expandAll


# Subscriptions

@docs subscriptions


# View

@docs view, viewFullscreen, card, columns, help, summary


# Output

@docs encoders


# Info

@docs toNodeFocus, toPath, isEmpty, dragging, log

-}

import Accessibility.Clipboard as Clipboard
import Element
    exposing
        ( Element
        , column
        , el
        , fill
        , none
        , paddingEach
        , spacing
        , width
        )
import Element.Extra as Element
import Fluent exposing (Fluent, verbatim)
import Pretty
import Rdf exposing (BlankNodeOrIri)
import Rdf.Decode as Decode
import Rdf.Encode as Encode
import Rdf.Graph exposing (Seed)
import Shacl.Clipboard as Clipboard
import Shacl.Form.Breadcrumbs exposing (Breadcrumbs, Path)
import Shacl.Form.Context exposing (C)
import Shacl.Form.Decoded as Decoded
import Shacl.Form.Effect as Effect
import Shacl.Form.Grouped as Grouped
import Shacl.Form.Localize as Localize
import Shacl.Form.Shared exposing (Config, Shared)
import Shacl.Form.Ui.Container as Container exposing (Container)
import Shacl.Form.Updated as Updated exposing (Updated)
import Shacl.Report exposing (Report)
import Ui.Atom.ButtonIcon as ButtonIcon
import Ui.Atom.Icon as Icon
import Ui.Molecule.Table as Table
import Ui.Molecule.TableCell as TableCell
import Ui.Theme.Typography exposing (caption, h5, h6)



-- MODEL


{-| -}
type Model f
    = Model (Data f)


type alias Data f =
    { path : Path
    , breadcrumbs : Breadcrumbs
    , grouped : Grouped.DataGroup
    , nodeFocus : BlankNodeOrIri
    , form : f
    }



-- INIT


{-| -}
init :
    Config e f m (Model f) (Msg f m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Breadcrumbs
    -> Decoded.DataGroup
    -> Updated (Model f) m
init config populateDefaultValues c shared seed breadcrumbs decoded =
    let
        breadcrumbsNested =
            { breadcrumbs | depthGroup = 1 + breadcrumbs.depthGroup }
    in
    decoded.field
        |> config.init populateDefaultValues c shared seed breadcrumbsNested
        |> Updated.map
            (\form ->
                Model
                    { path = decoded.path
                    , breadcrumbs = breadcrumbs
                    , grouped = decoded.grouped
                    , nodeFocus = decoded.nodeFocus
                    , form = form
                    }
            )
        |> Updated.mapEffect (MsgForm >> config.toMsg)


{-| -}
initWith :
    Config e f m (Model f) (Msg f m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Model f
    -> Result e (Maybe (Updated (Model f) m))
initWith config populateDefaultValues c shared seed (Model data) =
    data.form
        |> config.initWith populateDefaultValues c shared seed
        |> Result.map
            (Maybe.map
                (Updated.map (\form -> Model { data | form = form })
                    >> Updated.mapEffect (MsgForm >> config.toMsg)
                )
            )



-- UPDATE


{-| -}
type Msg f m
    = UserCopied
    | UserPasted f
    | Ignored
    | MsgForm m


{-| -}
update :
    Config e f m (Model f) (Msg f m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Msg f m
    -> Model f
    -> Updated (Model f) m
update config _ c shared seed msg ((Model data) as model) =
    case msg of
        UserCopied ->
            model
                |> Updated.from seed
                |> Updated.withEffect
                    (encoders config c model
                        |> Clipboard.serialize data.nodeFocus seed
                        |> Effect.copyToClipboard
                    )

        UserPasted formPasted ->
            { data | form = formPasted }
                |> Model
                |> Updated.from seed
                |> Updated.needsPersisting

        Ignored ->
            Updated.from seed model

        MsgForm msgForm ->
            config.update c shared seed msgForm data.form
                |> Updated.map (\formUpdated -> Model { data | form = formUpdated })
                |> Updated.mapEffect (MsgForm >> config.toMsg)


{-| -}
expandAll : Config e f m (Model f) (Msg f m) -> Model f -> Model f
expandAll config (Model data) =
    Model { data | form = config.expandAll data.form }



-- SUBSCRIPTIONS


{-| -}
subscriptions : Config e f m (Model f) (Msg f m) -> Model f -> Sub m
subscriptions config (Model data) =
    config.subscriptions data.form



-- VIEW


{-| -}
view : Config e f m (Model f) (Msg f m) -> C -> Report -> Bool -> Model f -> Maybe (Container m)
view config c report isDragging (Model data) =
    data.form
        |> config.view c report isDragging
        |> Maybe.map
            (Element.map (MsgForm >> config.toMsg)
                >> viewGroupHelp config c data
                >> Container.expand
            )


viewGroupHelp : Config e f m (Model f) (Msg f m) -> C -> Data f -> Element m -> Element m
viewGroupHelp config c data containers =
    column
        [ width fill
        , spacing Container.configField.spacingFieldY
        , Clipboard.onCopy (config.toMsg UserCopied)
        , Clipboard.onPaste "text/plain"
            (config.toMsg Ignored)
            (Maybe.map config.toMsg << Clipboard.allSubjectsOne UserPasted (Decode.fail "TODO"))
        ]
        [ viewName config c data
        , containers
        ]


viewName : Config e f m (Model f) (Msg f m) -> C -> Data f -> Element m
viewName config c data =
    case List.head data.grouped.names of
        Nothing ->
            none

        Just theName ->
            let
                toHeading =
                    case data.breadcrumbs.depthGroup of
                        0 ->
                            h5

                        _ ->
                            h6
            in
            Element.row
                [ paddingEach
                    { top = 16
                    , bottom = 0
                    , left = 16
                    , right = 8
                    }
                , width fill
                , Element.class "shacl-form-input-group__heading"
                ]
                [ theName
                    |> Localize.forLabel c
                    |> verbatim
                    |> toHeading
                , Icon.ContentCopy
                    |> ButtonIcon.buttonIcon (config.toMsg UserCopied)
                    |> ButtonIcon.withAttributes
                        [ Element.class "shacl-form-input-group__button-copy"
                        , Element.alignRight
                        ]
                    |> ButtonIcon.excludeFromTabSequence
                    |> ButtonIcon.toElement
                ]


{-| -}
viewFullscreen :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Model f
    -> Maybe (Element m)
viewFullscreen config c report isDragging (Model data) =
    data.form
        |> config.fullscreen c report isDragging
        |> Maybe.map (Element.map (MsgForm >> config.toMsg))


{-| -}
card : Config e f m (Model f) (Msg f m) -> C -> Model f -> Maybe (Container m)
card config c (Model data) =
    data.form
        |> config.card c
        |> Maybe.map
            (\elementForm ->
                let
                    toHeading =
                        caption
                in
                [ case List.head data.grouped.names of
                    Nothing ->
                        none

                    Just theName ->
                        theName
                            |> Localize.forLabel c
                            |> verbatim
                            |> toHeading
                            |> el
                                [ paddingEach
                                    { top = 16
                                    , bottom = 0
                                    , left = 0
                                    , right = 0
                                    }
                                ]
                , Element.map (MsgForm >> config.toMsg) elementForm
                ]
                    |> column
                        [ width fill
                        , spacing Container.configField.spacingFieldY
                        ]
                    |> Container.expand
            )


{-| -}
columns :
    Config e f m (Model f) (Msg f m)
    -> C
    -> List String
    -> Grouped.DataGroup
    -> List (Table.Column f m)
columns config c scope grouped =
    config.columns c scope grouped.field
        |> List.map
            (\column ->
                { header = column.header
                , width = column.width
                , view =
                    config.toField
                        >> Maybe.map (\(Model data) -> column.view data.form)
                        >> Maybe.withDefault TableCell.empty
                }
            )


{-| -}
help : Config e f m (Model f) (Msg f m) -> C -> Report -> Model f -> List Fluent
help _ _ _ _ =
    []


{-| -}
summary : Config e f m (Model f) (Msg f m) -> C -> Model f -> List Fluent
summary config c (Model data) =
    config.summary c data.form



-- OUTPUT


{-| -}
encoders :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Model f
    -> List Encode.PropertyEncoder
encoders config c (Model data) =
    config.encoders c data.form



-- INFO


{-| -}
toNodeFocus : Model f -> BlankNodeOrIri
toNodeFocus (Model data) =
    data.nodeFocus


{-| -}
toPath : Model f -> Path
toPath (Model data) =
    data.path


{-| -}
isEmpty : Config e f m (Model f) (Msg f m) -> Model f -> Bool
isEmpty _ _ =
    False


{-| -}
dragging : Config e f m (Model f) (Msg f m) -> Model f -> Bool
dragging config (Model data) =
    config.dragging data.form


{-| -}
log : Config e f m (Model f) (Msg f m) -> Model f -> Pretty.Doc ()
log config (Model data) =
    Pretty.lines
        [ Pretty.string "Group"
        , Pretty.indent 2 (config.log data.form)
        ]
