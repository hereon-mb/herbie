module Shacl.Form.Viewed.Collection.Instance exposing
    ( Config
    , Global, Action, init
    , Callbacks, Msg, update, focusAction
    , Local, view, viewGrouped
    )

{-|

@docs Config
@docs Global, Action, init
@docs Callbacks, Msg, update, focusAction
@docs Local, view, viewGrouped

-}

import Element as Ui exposing (Attribute, Color, Element)
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Extra as Element
import Element.Font as Font
import Fluent exposing (Fluent)
import Json.Decode as Decode
import Rdf exposing (StringOrLangString)
import Shacl.Form.Context exposing (C)
import Shacl.Form.Effect as Effect exposing (Effect)
import Shacl.Form.Localize as Localize
import Shacl.Form.Ui.Container as Container
import Shacl.Form.Updated exposing (Updated)
import Ui.Atom.ButtonIcon as ButtonIcon exposing (ButtonIcon, buttonIcon)
import Ui.Atom.Divider as Divider
import Ui.Atom.ErrorWrapper exposing (errorWrapper)
import Ui.Atom.Icon as Icon
import Ui.Atom.Tooltip as Tooltip
import Ui.Theme.Color as Color
import Ui.Theme.Elevation as Elevation
import Ui.Theme.Typography as Typography


type alias Config m =
    { id : String
    , depthCollection : Int
    , withOrder : Bool
    , unbounded : Bool
    , toMsg : Msg -> m
    , msgTooltip : Tooltip.Msg -> m
    }


type alias Global =
    { count : Int
    , actionActive : Action
    }


type Action
    = MoveUp
    | MoveDown
    | Copy
    | Remove
    | CopyLink
    | CollapseOrExpand


init : Action
init =
    CollapseOrExpand



-- UPDATE


type Msg
    = -- BUTTONS
      UserPressedMoveUp
    | UserPressedMoveDown
    | UserPressedCopy
    | UserPressedRemove
    | UserPressedCopyLink
    | UserPressedCollapseOrExpand
      -- KEYBOARD
    | UserPressedKeyArrowLeft
    | UserPressedKeyArrowRight
    | UserPressedKeyArrowUp
    | UserPressedKeyArrowDown
    | UserPressedKeyHome
    | UserPressedKeyEnd


type alias Callbacks model m =
    { noOp : Updated model m
    , perform : Effect Msg -> Updated model m
    , update : Action -> Effect Msg -> Updated model m
    , moveInstanceUp : Action -> (Int -> Effect Msg) -> Updated model m
    , moveInstanceDown : Action -> (Int -> Effect Msg) -> Updated model m
    , copyInstance : Action -> Updated model m
    , removeInstance : Action -> Effect Msg -> Updated model m
    , copyNodeFocus : Action -> Updated model m
    , toggleExpansion : Action -> Effect Msg -> Updated model m
    }


update :
    Callbacks model m
    -> Config m
    -> Global
    -> Int
    -> Msg
    -> Updated model m
update callbacks config global index msg =
    case msg of
        -- BUTTONS
        UserPressedMoveUp ->
            callbacks.moveInstanceUp MoveUp
                (\indexNew -> focusAction config indexNew MoveUp)

        UserPressedMoveDown ->
            callbacks.moveInstanceDown MoveDown
                (\indexNew -> focusAction config indexNew MoveDown)

        UserPressedCopy ->
            callbacks.copyInstance Copy

        UserPressedRemove ->
            let
                indexNew =
                    if index == global.count - 1 && index > 0 then
                        index - 1

                    else
                        index
            in
            callbacks.removeInstance Remove
                (focusAction config indexNew Remove)

        UserPressedCopyLink ->
            callbacks.copyNodeFocus CopyLink

        UserPressedCollapseOrExpand ->
            callbacks.toggleExpansion CollapseOrExpand
                (focusAction config index CollapseOrExpand)

        -- KEYBOARD
        UserPressedKeyArrowLeft ->
            (case global.actionActive of
                MoveUp ->
                    CollapseOrExpand

                MoveDown ->
                    MoveUp

                Copy ->
                    if config.withOrder then
                        MoveDown

                    else
                        CollapseOrExpand

                Remove ->
                    Copy

                CopyLink ->
                    Remove

                CollapseOrExpand ->
                    CopyLink
            )
                |> changeActionActive callbacks config index

        UserPressedKeyArrowRight ->
            (case global.actionActive of
                MoveUp ->
                    MoveDown

                MoveDown ->
                    Copy

                Copy ->
                    Remove

                Remove ->
                    CopyLink

                CopyLink ->
                    CollapseOrExpand

                CollapseOrExpand ->
                    if config.withOrder then
                        MoveUp

                    else
                        Copy
            )
                |> changeActionActive callbacks config index

        UserPressedKeyArrowUp ->
            callbacks.perform
                (focusAction config
                    (if index == 0 && global.count > 0 then
                        global.count - 1

                     else
                        index - 1
                    )
                    global.actionActive
                )

        UserPressedKeyArrowDown ->
            callbacks.perform
                (focusAction config
                    (if index < global.count - 1 then
                        index + 1

                     else
                        0
                    )
                    global.actionActive
                )

        UserPressedKeyHome ->
            callbacks.perform
                (focusAction config 0 global.actionActive)

        UserPressedKeyEnd ->
            callbacks.perform
                (focusAction config (global.count - 1) global.actionActive)


changeActionActive :
    Callbacks model m
    -> Config m
    -> Int
    -> Action
    -> Updated model m
changeActionActive callbacks config index actionActiveUpdated =
    callbacks.update actionActiveUpdated
        (focusAction config index actionActiveUpdated)


focusAction : Config m -> Int -> Action -> Effect Msg
focusAction config index action =
    Effect.focus (idAction config index action)



-- VIEW


type alias Local m =
    { ordinal : Fluent
    , label : Maybe StringOrLangString
    , summary : List Fluent
    , help : List Fluent
    , expanded : Bool
    , form : Element m
    }


view : Config m -> C -> Int -> Global -> Local m -> Element m
view config c index global local =
    let
        handleUserPressedToggleExpansion =
            Ui.el
                [ Ui.width Ui.fill
                , Ui.pointer
                , Events.onClick (config.toMsg UserPressedCollapseOrExpand)
                ]
    in
    errorWrapper local.help
        (Ui.column
            [ Ui.width Ui.fill
            , Ui.padding 4
            , Ui.spacing Container.configField.spacingFieldY
            , Border.rounded 12
            , Border.width 2
            , Border.color (backgroundColor config)
            , Ui.focused
                [ Border.color Color.onSurface ]
            , Background.color (backgroundColor config)
            , Elevation.z2Hover
            ]
            [ viewHeader config c index global local
                |> handleUserPressedToggleExpansion
            , if local.expanded then
                Ui.el
                    [ Ui.width Ui.fill
                    , Ui.paddingEach
                        { top = 4
                        , bottom = 12
                        , left = 12
                        , right = 12
                        }
                    ]
                    local.form

              else
                Ui.none
            ]
        )


viewGrouped : C -> Global -> List ( Config m, Int, Local m ) -> Element m
viewGrouped c global indexedLocals =
    let
        count =
            List.length indexedLocals
    in
    Ui.column
        [ Ui.width Ui.fill ]
        (indexedLocals
            |> List.indexedMap (viewGroupedInstance c global count)
            |> List.intersperse Divider.horizontal
        )


viewGroupedInstance :
    C
    -> Global
    -> Int
    -> Int
    -> ( Config m, Int, Local m )
    -> Element m
viewGroupedInstance c global count row ( config, index, local ) =
    let
        handleUserPressedToggleExpansion =
            Ui.el
                [ Ui.width Ui.fill
                , Ui.pointer
                , Events.onClick (config.toMsg UserPressedCollapseOrExpand)
                ]
    in
    errorWrapper local.help
        (Ui.column
            [ Ui.width Ui.fill
            , Ui.padding 4
            , Ui.spacing Container.configField.spacingFieldY
            , if count == 1 then
                Border.rounded 12

              else if row == 0 then
                Border.roundEach
                    { topLeft = 12
                    , topRight = 12
                    , bottomRight = 0
                    , bottomLeft = 0
                    }

              else if row == count - 1 then
                Border.roundEach
                    { topLeft = 0
                    , topRight = 0
                    , bottomRight = 12
                    , bottomLeft = 12
                    }

              else
                Border.rounded 0
            , Border.width 2
            , Border.color (backgroundColor config)
            , Ui.focused
                [ Border.color Color.onSurface ]
            , Background.color (backgroundColor config)
            , Elevation.z2Hover
            ]
            [ viewHeader config c index global local
                |> handleUserPressedToggleExpansion
            , if local.expanded then
                Ui.el
                    [ Ui.width Ui.fill
                    , Ui.paddingEach
                        { top = 4
                        , bottom = 12
                        , left = 12
                        , right = 12
                        }
                    ]
                    local.form

              else
                Ui.none
            ]
        )


backgroundColor : Config m -> Color
backgroundColor config =
    case config.depthCollection of
        0 ->
            Color.surfaceContainer

        1 ->
            Color.surfaceContainerHigh

        2 ->
            Color.surfaceContainerHighest

        _ ->
            Color.surfaceContainerHighest


viewHeader : Config m -> C -> Int -> Global -> Local m -> Element m
viewHeader config c index global local =
    Ui.row
        [ Ui.width Ui.fill
        , Ui.spacing 16
        , handleKeyDown config
        ]
        (if config.withOrder then
            [ viewActionsLeft config c index global local
            , viewHeadingInstance c local
            , viewActionsRight config c index global local
            ]

         else
            [ viewHeadingInstance c local
            , viewActionsRight config c index global local
            ]
        )


handleKeyDown : Config m -> Attribute m
handleKeyDown config =
    Element.preventDefaultOnKeyDown
        [ Decode.andThen
            (\code ->
                case code of
                    "ArrowLeft" ->
                        Decode.succeed
                            ( config.toMsg UserPressedKeyArrowLeft
                            , True
                            )

                    "ArrowRight" ->
                        Decode.succeed
                            ( config.toMsg UserPressedKeyArrowRight
                            , True
                            )

                    "ArrowUp" ->
                        Decode.succeed
                            ( config.toMsg UserPressedKeyArrowUp
                            , True
                            )

                    "ArrowDown" ->
                        Decode.succeed
                            ( config.toMsg UserPressedKeyArrowDown
                            , True
                            )

                    "Home" ->
                        Decode.succeed
                            ( config.toMsg UserPressedKeyHome
                            , True
                            )

                    "End" ->
                        Decode.succeed
                            ( config.toMsg UserPressedKeyEnd
                            , True
                            )

                    _ ->
                        Decode.fail "not handling that key"
            )
            (Decode.field "code" Decode.string)
        ]


viewHeadingInstance : C -> Local m -> Element m
viewHeadingInstance c local =
    Ui.paragraph
        [ Ui.alignTop
        , Ui.width Ui.fill
        , Ui.paddingXY 16 9
        ]
        (case viewLabelInstance c local of
            Nothing ->
                viewSummaryInstance local

            Just labelInstance ->
                let
                    summaryInstance =
                        viewSummaryInstance local
                in
                if List.isEmpty summaryInstance then
                    [ labelInstance ]

                else
                    labelInstance
                        :: Ui.el [ Ui.width (Ui.px 16) ] Ui.none
                        :: summaryInstance
        )


viewLabelInstance : C -> Local m -> Maybe (Element m)
viewLabelInstance c local =
    local.label
        |> Maybe.map (Localize.forLabel c)
        |> Maybe.map (Fluent.verbatim >> Typography.h6)


viewSummaryInstance : Local m -> List (Element m)
viewSummaryInstance local =
    if List.isEmpty local.summary then
        []

    else
        local.summary
            |> List.take 3
            |> List.map viewSummaryItem
            |> List.intersperse separator


separator : Element msg
separator =
    Ui.el
        [ Font.size 18
        , Font.italic
        ]
        (Ui.text " · ")


viewSummaryItem : Fluent -> Element m
viewSummaryItem item =
    Ui.el
        [ Font.size 18
        , Font.italic
        ]
        (Fluent.toElement item)


viewActionsLeft : Config m -> C -> Int -> Global -> Local m -> Element m
viewActionsLeft config c index global local =
    Ui.row
        [ Ui.alignTop
        , Ui.spacing 4
        ]
        [ viewActionMoveUp config c index global
        , Typography.body1 local.ordinal
        , viewActionMoveDown config c index global
        ]


viewActionsRight : Config m -> C -> Int -> Global -> Local m -> Element m
viewActionsRight config c index global local =
    Ui.row
        [ Ui.alignTop
        , Ui.spacing 4
        , Ui.alignRight
        ]
        [ viewActionCopy config c index global
        , viewActionRemove config c index global
        , viewActionCopyLink config c index global
        , viewActionCollapseOrExpand config c index global local
        ]


viewActionMoveUp : Config m -> C -> Int -> Global -> Element m
viewActionMoveUp config c index global =
    Icon.ArrowUpward
        |> buttonIcon (config.toMsg UserPressedMoveUp)
        |> ButtonIcon.withEnabled (config.unbounded || index > 0)
        |> ButtonIcon.withId (idAction config index MoveUp)
        |> excludeFromTabSequenceUnless global MoveUp
        |> ButtonIcon.toElement
        |> Tooltip.add
            { label = Fluent.verbatim "Move up"
            , placement = Tooltip.Above
            , tooltip = c.tooltip
            , tooltipMsg = config.msgTooltip
            , id = idActionTooltip config index MoveUp
            }


viewActionMoveDown : Config m -> C -> Int -> Global -> Element m
viewActionMoveDown config c index global =
    Icon.ArrowDownward
        |> buttonIcon (config.toMsg UserPressedMoveDown)
        |> ButtonIcon.withEnabled (config.unbounded || index < global.count - 1)
        |> ButtonIcon.withId (idAction config index MoveDown)
        |> excludeFromTabSequenceUnless global MoveDown
        |> ButtonIcon.toElement
        |> Tooltip.add
            { label = Fluent.verbatim "Move down"
            , placement = Tooltip.Above
            , tooltip = c.tooltip
            , tooltipMsg = config.msgTooltip
            , id = idActionTooltip config index MoveDown
            }


viewActionCopy : Config m -> C -> Int -> Global -> Element m
viewActionCopy config c index global =
    Icon.ContentCopy
        |> buttonIcon (config.toMsg UserPressedCopy)
        |> ButtonIcon.withId (idAction config index Copy)
        |> excludeFromTabSequenceUnless global Copy
        |> ButtonIcon.toElement
        |> Tooltip.add
            { label = Fluent.verbatim "Copy"
            , placement = Tooltip.Above
            , tooltip = c.tooltip
            , tooltipMsg = config.msgTooltip
            , id = idActionTooltip config index Copy
            }


viewActionRemove : Config m -> C -> Int -> Global -> Element m
viewActionRemove config c index global =
    Icon.Delete
        |> buttonIcon (config.toMsg UserPressedRemove)
        |> ButtonIcon.withId (idAction config index Remove)
        |> excludeFromTabSequenceUnless global Remove
        |> ButtonIcon.toElement
        |> Tooltip.add
            { label = Fluent.verbatim "Remove"
            , placement = Tooltip.Above
            , tooltip = c.tooltip
            , tooltipMsg = config.msgTooltip
            , id = idActionTooltip config index Remove
            }


viewActionCopyLink : Config m -> C -> Int -> Global -> Element m
viewActionCopyLink config c index global =
    Icon.Link
        |> buttonIcon (config.toMsg UserPressedCopyLink)
        |> ButtonIcon.withId (idAction config index CopyLink)
        |> excludeFromTabSequenceUnless global CopyLink
        |> ButtonIcon.toElement
        |> Tooltip.add
            { label = Fluent.verbatim "Copy link"
            , placement = Tooltip.Above
            , tooltip = c.tooltip
            , tooltipMsg = config.msgTooltip
            , id = idActionTooltip config index CopyLink
            }


viewActionCollapseOrExpand : Config m -> C -> Int -> Global -> Local m -> Element m
viewActionCollapseOrExpand config c index global local =
    (if local.expanded then
        Icon.ExpandLess

     else
        Icon.ExpandMore
    )
        |> buttonIcon (config.toMsg UserPressedCollapseOrExpand)
        |> ButtonIcon.withId (idAction config index CollapseOrExpand)
        |> excludeFromTabSequenceUnless global CollapseOrExpand
        |> ButtonIcon.toElement
        |> Tooltip.add
            { label =
                if local.expanded then
                    Fluent.verbatim "Collapse"

                else
                    Fluent.verbatim "Expand"
            , placement = Tooltip.Above
            , tooltip = c.tooltip
            , tooltipMsg = config.msgTooltip
            , id = idActionTooltip config index CollapseOrExpand
            }


excludeFromTabSequenceUnless : Global -> Action -> ButtonIcon m -> ButtonIcon m
excludeFromTabSequenceUnless global action =
    if global.actionActive == action then
        identity

    else
        ButtonIcon.excludeFromTabSequence



-- IDS


idAction : Config m -> Int -> Action -> String
idAction config index action =
    config.id ++ "--" ++ String.fromInt index ++ "--" ++ actionToString action


idActionTooltip : Config m -> Int -> Action -> String
idActionTooltip config index action =
    idAction config index action ++ "--tooltip"


actionToString : Action -> String
actionToString action =
    case action of
        MoveUp ->
            "move-up"

        MoveDown ->
            "move-down"

        Copy ->
            "copy"

        Remove ->
            "remove"

        CopyLink ->
            "copy-link"

        CollapseOrExpand ->
            "collapse-or-expand"
