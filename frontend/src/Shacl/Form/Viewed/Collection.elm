module Shacl.Form.Viewed.Collection exposing
    ( Model
    , init, initWith
    , Msg, update, expandAll
    , subscriptions
    , view, viewFullscreen, card, columns, help, summary
    , encoders
    , toNodeFocus, toPath, isEmpty, dragging, log
    )

{-|


# Model

@docs Model


# Init

@docs init, initWith


# Update

@docs Msg, update, expandAll


# Subscriptions

@docs subscriptions


# View

@docs view, viewFullscreen, card, columns, help, summary


# Output

@docs encoders


# Info

@docs toNodeFocus, toPath, isEmpty, dragging, log

-}

import Api
import Element as Ui exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Element.Lazy as Ui
import Fluent exposing (Fluent)
import List.Extra as List
import List.NonEmpty as NonEmpty
import Maybe.Apply as Maybe
import Maybe.Extra as Maybe
import Pretty
import Rdf exposing (BlankNodeOrIri, Iri)
import Rdf.Decode as Decode
import Rdf.Decode.Extra as Decode
import Rdf.Encode as Encode exposing (PropertyEncoder)
import Rdf.Extra as Rdf
import Rdf.Graph exposing (Seed)
import Rdf.Namespaces exposing (a)
import Rdf.Namespaces.RDFS as RDFS
import Rdf.PropertyPath exposing (PropertyPath(..))
import Shacl exposing (Viewer(..))
import Shacl.Clipboard as Clipboard
import Shacl.Form.Breadcrumbs as Breadcrumbs exposing (Breadcrumbs, Path)
import Shacl.Form.Context exposing (C)
import Shacl.Form.Decoded as Decoded
import Shacl.Form.Effect as Effect
import Shacl.Form.Grouped as Grouped
import Shacl.Form.Instance exposing (Instance)
import Shacl.Form.Localize as Localize
import Shacl.Form.Shared exposing (Config, Shared)
import Shacl.Form.Ui.Container as Container exposing (Container)
import Shacl.Form.Updated as Updated exposing (Updated)
import Shacl.Form.Viewed.Collection.Instance as Instance
import Shacl.Report as Report exposing (ConstraintComponent(..), Report, ValidationResult(..))
import Ui.Atom.Button as Button exposing (button)
import Ui.Atom.ErrorWrapper exposing (errorWrapper)
import Ui.Molecule.Table as Table exposing (table)
import Ui.Theme.Color as Color
import Ui.Theme.Elevation as Elevation
import Ui.Theme.Typography as Typography



-- MODEL


{-| -}
type Model f
    = Model (Data f)


type alias Data f =
    { path : Path
    , breadcrumbs : Breadcrumbs
    , grouped : Grouped.DataCollection
    , nodeFocus : BlankNodeOrIri
    , iriDocument : Iri
    , instances : List (Instance f)
    , actionActive : Instance.Action
    , indexMax : Int
    }



-- INIT


{-| -}
init :
    Config e f m (Model f) (Msg f m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Breadcrumbs
    -> Decoded.DataCollection
    -> Updated (Model f) m
init config populateDefaultValues c shared seed breadcrumbs decoded =
    let
        modelFromInstances instances =
            Model
                { path = decoded.path
                , breadcrumbs = breadcrumbs
                , grouped = decoded.grouped
                , nodeFocus = decoded.nodeFocus
                , iriDocument = shared.iri
                , instances = instances
                , actionActive = Instance.init
                , indexMax = indexMaxFromInstances config instances
                }
    in
    decoded.instances
        |> List.foldl
            (initInstance config populateDefaultValues c shared seed breadcrumbs)
            ( { model = []
              , effect = Effect.none
              , seed = seed
              , needsPersisting = False
              }
            , 0
            )
        |> Tuple.first
        |> Updated.map
            (List.reverse
                >> sortInstances config shared decoded.grouped
                >> modelFromInstances
            )


initInstance :
    Config e f m (Model f) (Msg f m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Breadcrumbs
    -> Decoded.InstanceCollection
    -> ( Updated (List (Instance f)) m, Int )
    -> ( Updated (List (Instance f)) m, Int )
initInstance config populateDefaultValues c shared seed breadcrumbs instance ( updated, index ) =
    let
        breadcrumbsNested =
            breadcrumbs
                |> increaseDepthCollection

        updatedForm =
            config.init populateDefaultValues
                c
                shared
                seed
                breadcrumbsNested
                instance.field
    in
    ( { model =
            { breadcrumbs = breadcrumbsNested
            , expanded = False
            , form = updatedForm.model
            , label = instance.label
            }
                :: updated.model
      , effect =
            Effect.batch
                [ Effect.map (MsgForm index >> config.toMsg) updatedForm.effect
                , updated.effect
                ]
      , seed = updatedForm.seed
      , needsPersisting = updatedForm.needsPersisting || updated.needsPersisting
      }
    , index + 1
    )


sortInstances :
    Config e f m (Model f) (Msg f m)
    -> Shared
    -> Grouped.DataCollection
    -> List (Instance f)
    -> List (Instance f)
sortInstances config shared grouped =
    case grouped.orderPath of
        Nothing ->
            identity

        Just orderPath ->
            List.sortBy (getOrder config shared orderPath)


getOrder :
    Config e f m (Model f) (Msg f m)
    -> Shared
    -> PropertyPath
    -> Instance f
    -> Int
getOrder config shared orderPath instance =
    shared.graphCombinedBackend
        |> Decode.decode
            (Decode.from (config.nodeFocus instance.form)
                (Decode.property orderPath Decode.int)
            )
        |> Result.withDefault infinity


increaseDepthCollection : Breadcrumbs -> Breadcrumbs
increaseDepthCollection breadcrumbs =
    { breadcrumbs | depthCollection = breadcrumbs.depthCollection + 1 }


{-| -}
initWith :
    Config e f m (Model f) (Msg f m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Model f
    -> Result e (Maybe (Updated (Model f) msg))
initWith _ _ _ _ _ _ =
    -- FIXME Since we allow blank nodes as focus nodes for each collection
    -- item, it is not possible to partially init the components from a new
    -- graph, as in the new graph the blank nodes might have changed and cannot
    -- be mapped any more.  We therefore do not update the collection items,
    -- which could lead to stale UI.  We could add some indicator in the UI at
    -- some point.
    --
    -- This also means, that the instance's label will not be updated in the
    -- UI.
    Ok Nothing



-- UPDATE


{-| -}
type Msg f m
    = UserPressedAddCollection
    | ApiReturnedDecoded (Api.Response Decoded.Field)
    | UserPasted (List Decoded.InstanceCollection)
    | ApiReturnedDuplicated (List Decoded.InstanceCollection) (Api.Response Decoded.Field)
    | Ignored
    | MsgInstance Int Instance.Msg
    | MsgForm Int m


{-| -}
update :
    Config e f m (Model f) (Msg f m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Msg f m
    -> Model f
    -> Updated (Model f) m
update config populateDefaultValues c shared seed msg ((Model ({ breadcrumbs } as data)) as model) =
    case msg of
        UserPressedAddCollection ->
            let
                indexNew =
                    data.indexMax + 1
            in
            { data | indexMax = indexNew }
                |> Model
                |> Updated.from seed
                |> Updated.withEffect
                    (data.grouped.field
                        |> Effect.createField
                            ApiReturnedDecoded
                            shared.preview
                            (Breadcrumbs.Ordinal indexNew :: data.path)
                            -- FIXME Mint from all classes
                            (Decoded.MintIri
                                { nodeFocus = data.nodeFocus
                                , class = NonEmpty.head data.grouped.classes
                                }
                            )
                        |> Effect.map config.toMsg
                    )

        ApiReturnedDecoded (Err error) ->
            model
                |> Updated.from seed
                |> Updated.withEffect (Effect.addToastError error)

        ApiReturnedDecoded (Ok field) ->
            let
                breadcrumbsNested =
                    breadcrumbs
                        |> increaseDepthCollection

                updatedInstance =
                    config.init populateDefaultValues
                        c
                        shared
                        seed
                        breadcrumbsNested
                        field

                instance =
                    { breadcrumbs = breadcrumbsNested
                    , expanded = True
                    , form = updatedInstance.model
                    , label = Nothing
                    }

                indexNew =
                    List.length data.instances
            in
            { data | instances = data.instances ++ [ instance ] }
                |> Model
                |> Updated.from updatedInstance.seed
                |> Updated.needsPersisting
                |> Updated.withEffect
                    (Effect.map config.toMsg
                        (Effect.batch
                            [ Effect.map (MsgForm indexNew)
                                updatedInstance.effect
                            , Effect.map (MsgInstance indexNew)
                                (Instance.focusAction
                                    (configInstance config data indexNew)
                                    indexNew
                                    data.actionActive
                                )
                            ]
                        )
                    )

        UserPasted instances ->
            Updated.from seed model
                |> duplicateNextInstance config shared instances

        ApiReturnedDuplicated instances (Err error) ->
            model
                |> Updated.from seed
                |> Updated.withEffect (Effect.addToastError error)
                |> duplicateNextInstance config shared instances

        ApiReturnedDuplicated instances (Ok field) ->
            let
                breadcrumbsNested =
                    breadcrumbs
                        |> increaseDepthCollection

                updatedInstance =
                    config.init populateDefaultValues
                        c
                        shared
                        seed
                        breadcrumbsNested
                        field

                instance =
                    { breadcrumbs = breadcrumbsNested
                    , expanded = True
                    , form = updatedInstance.model
                    , label = Nothing
                    }

                indexNew =
                    List.length data.instances
            in
            { data | instances = data.instances ++ [ instance ] }
                |> Model
                |> Updated.from updatedInstance.seed
                |> Updated.needsPersisting
                |> Updated.withEffect
                    (Effect.map config.toMsg
                        (Effect.batch
                            [ Effect.map
                                (MsgForm indexNew)
                                updatedInstance.effect
                            , Effect.map (MsgInstance indexNew)
                                (Instance.focusAction
                                    (configInstance config data indexNew)
                                    indexNew
                                    data.actionActive
                                )
                            ]
                        )
                    )
                |> duplicateNextInstance config shared instances

        Ignored ->
            Updated.from seed model

        MsgInstance index msgInstance ->
            case List.getAt index data.instances of
                Nothing ->
                    Updated.from seed model

                Just instance ->
                    Instance.update
                        (callbacksInstance config c seed model index instance)
                        (configInstance config data index)
                        (globalInstance data)
                        index
                        msgInstance

        MsgForm index msgForm ->
            case List.getAt index data.instances of
                Nothing ->
                    Updated.from seed model

                Just instance ->
                    config.update c shared seed msgForm instance.form
                        |> Updated.map
                            (\formUpdated ->
                                Model
                                    { data
                                        | instances =
                                            List.setAt index
                                                { instance | form = formUpdated }
                                                data.instances
                                    }
                            )
                        |> Updated.mapEffect (MsgForm index >> config.toMsg)


duplicateNextInstance :
    Config e f m (Model f) (Msg f m)
    -> Shared
    -> List Decoded.InstanceCollection
    -> Updated (Model f) m
    -> Updated (Model f) m
duplicateNextInstance config shared instances updated =
    case instances of
        [] ->
            updated

        instance :: rest ->
            let
                indexNew =
                    data.indexMax + 1

                (Model data) =
                    updated.model
            in
            updated
                |> Updated.map
                    (\_ ->
                        Model { data | indexMax = indexNew }
                    )
                |> Updated.withEffect
                    (instance.field
                        |> Effect.DuplicateField
                            (ApiReturnedDuplicated rest)
                            shared.preview
                            (Breadcrumbs.Ordinal indexNew :: data.path)
                            -- FIXME Mint from all classes
                            (Decoded.MintIri
                                { nodeFocus = data.nodeFocus
                                , class = NonEmpty.head data.grouped.classes
                                }
                            )
                        |> Effect.map config.toMsg
                    )



-- INSTANCE


configInstance :
    Config e f m (Model f) (Msg f m)
    -> Data f
    -> Int
    -> Instance.Config m
configInstance config data index =
    { id = Breadcrumbs.id data.path
    , depthCollection = data.breadcrumbs.depthCollection
    , withOrder = Maybe.isJust data.grouped.orderPath
    , unbounded = False
    , toMsg = config.toMsg << MsgInstance index
    , msgTooltip = config.msgTooltip
    }


callbacksInstance :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Seed
    -> Model f
    -> Int
    -> Instance f
    -> Instance.Callbacks (Model f) m
callbacksInstance config c seed ((Model data) as model) index instance =
    { noOp = Updated.from seed model
    , perform =
        \effect ->
            model
                |> Updated.from seed
                |> Updated.withEffect effect
                |> Updated.mapEffect (config.toMsg << MsgInstance index)
    , update =
        \actionActive effect ->
            { data | actionActive = actionActive }
                |> Model
                |> Updated.from seed
                |> Updated.withEffect effect
                |> Updated.mapEffect (config.toMsg << MsgInstance index)
    , moveInstanceUp =
        \actionActive indexNewToEffect ->
            let
                indexNew =
                    if index == 0 then
                        index

                    else
                        index - 1
            in
            { data
                | instances = List.swapAt index indexNew data.instances
                , actionActive = actionActive
            }
                |> Model
                |> Updated.from seed
                |> Updated.withEffect (indexNewToEffect indexNew)
                |> Updated.mapEffect (config.toMsg << MsgInstance indexNew)
    , moveInstanceDown =
        \actionActive indexNewToEffect ->
            let
                indexNew =
                    if index == List.length data.instances - 1 then
                        index

                    else
                        index + 1
            in
            { data
                | instances = List.swapAt index indexNew data.instances
                , actionActive = actionActive
            }
                |> Model
                |> Updated.from seed
                |> Updated.withEffect (indexNewToEffect indexNew)
                |> Updated.mapEffect (config.toMsg << MsgInstance indexNew)
    , copyInstance =
        \actionActive ->
            { data | actionActive = actionActive }
                |> Model
                |> Updated.from seed
                |> Updated.withEffect
                    (encoderInstance config c data ( index, instance )
                        |> List.singleton
                        |> Clipboard.serialize data.nodeFocus seed
                        |> Effect.copyToClipboard
                    )
    , removeInstance =
        \actionActive effect ->
            { data
                | instances = List.removeAt index data.instances
                , actionActive = actionActive
            }
                |> Model
                |> Updated.from seed
                |> Updated.withEffect effect
                |> Updated.mapEffect (config.toMsg << MsgInstance index)
    , copyNodeFocus =
        \actionActive ->
            { data | actionActive = actionActive }
                |> Model
                |> Updated.from seed
                |> Updated.withEffect
                    (config.userPressedLink (config.nodeFocus instance.form)
                        |> Effect.fromMsg
                    )
    , toggleExpansion =
        \actionActive effect ->
            { data
                | instances =
                    List.setAt index
                        { instance | expanded = not instance.expanded }
                        data.instances
                , actionActive = actionActive
            }
                |> Model
                |> Updated.from seed
                |> Updated.withEffect effect
                |> Updated.mapEffect (config.toMsg << MsgInstance index)
    }


globalInstance : Data f -> Instance.Global
globalInstance data =
    { count = List.length data.instances
    , actionActive = data.actionActive
    }


localInstance :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Data f
    -> Int
    -> Instance f
    -> Instance.Local m
localInstance config c report isDragging data index instance =
    let
        ordinal =
            String.fromInt position ++ ordinalSuffix

        ordinalSuffix =
            if
                (remainderBy 100 position == 11)
                    || (remainderBy 100 position == 12)
                    || (remainderBy 100 position == 13)
            then
                "th"

            else if remainderBy 10 position == 1 then
                "st"

            else if remainderBy 10 position == 2 then
                "nd"

            else if remainderBy 10 position == 3 then
                "rd"

            else
                "th"

        position =
            index + 1
    in
    { ordinal = Fluent.verbatim ordinal
    , label = instance.label
    , summary = config.summary c instance.form
    , help =
        helpInstance c
            report
            data.nodeFocus
            data
            (config.nodeFocus instance.form)
    , expanded = instance.expanded
    , form =
        config.view c report isDragging instance.form
            |> Maybe.map (Ui.map (MsgForm index >> config.toMsg))
            |> Maybe.withDefault Ui.none
    }


{-| -}
expandAll : Config e f m (Model f) (Msg f m) -> Model f -> Model f
expandAll config (Model data) =
    Model
        { data
            | instances =
                List.map
                    (\instance ->
                        { instance
                            | expanded = True
                            , form = config.expandAll instance.form
                        }
                    )
                    data.instances
        }



-- SUBSCRIPTIONS


{-| -}
subscriptions : Config e f m (Model f) (Msg f m) -> Model f -> Sub m
subscriptions config (Model data) =
    data.instances
        |> List.map (.form >> config.subscriptions)
        |> Sub.batch



-- VIEW


{-| -}
view :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Model f
    -> Maybe (Container m)
view config c report isDragging model =
    viewHelp config c report isDragging model
        |> Container.expand
        |> Just


viewHelp :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Model f
    -> Element m
viewHelp =
    Ui.lazy5
        (\config c report isDragging (Model data) ->
            Ui.el
                [ Ui.width Ui.fill
                , Ui.paddingXY 0 16
                ]
                (errorWrapper
                    (helpCollection c report data)
                    (Ui.column
                        [ Ui.width Ui.fill
                        , Ui.spacing 16
                        ]
                        [ viewHeading c data
                        , if List.length data.instances == 0 then
                            viewInstancesEmpty config c data

                          else
                            viewInstances config c report isDragging data
                        ]
                    )
                )
        )


viewHeading : C -> Data f -> Element m
viewHeading c data =
    Ui.paragraph
        [ Ui.paddingXY 16 0 ]
        [ if data.breadcrumbs.depthGroup == 0 then
            Typography.h5 (textHeading c data)

          else
            Typography.h6 (textHeading c data)
        ]


viewInstancesEmpty :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Data f
    -> Element m
viewInstancesEmpty config c data =
    Ui.column
        [ Ui.width Ui.fill
        , Ui.padding 32
        , Ui.spacing 32
        , Border.rounded 12
        , Background.color (backgroundColor data.breadcrumbs)
        , Elevation.z2Hover
        ]
        [ Ui.el [ Ui.centerX ] (viewInstancesEmptyInfo c data)
        , Ui.el [ Ui.centerX ] (viewActionAddFirst config c data)
        ]


viewInstancesEmptyInfo : C -> Data f -> Element m
viewInstancesEmptyInfo c data =
    let
        namePlural =
            data.grouped.names
                |> List.head
                |> Maybe.map (Localize.withinSentencePlural c)
                |> Maybe.withDefault "values"
    in
    [ "There are no "
    , namePlural
    , ", yet"
    ]
        |> String.concat
        |> Fluent.verbatim
        |> Typography.body1


viewActionAddFirst :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Data f
    -> Element m
viewActionAddFirst config c data =
    let
        name =
            data.grouped.names
                |> List.head
                |> Maybe.map (Localize.forLabel c)
                |> Maybe.withDefault "value"
    in
    [ "Add first "
    , name
    ]
        |> String.concat
        |> Fluent.verbatim
        |> button (config.toMsg UserPressedAddCollection)
        |> Button.withId (Breadcrumbs.id data.path ++ "--add")
        |> Button.withFormat Button.Outlined
        |> Button.view


viewInstances :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Data f
    -> Element m
viewInstances config c report isDragging data =
    Ui.column
        [ Ui.width Ui.fill
        , Ui.spacing 16
        , onPaste config c data
        ]
        [ viewInstancesList config c report isDragging data
        , viewActionAdd config c data
        ]


onPaste : Config e f m (Model f) (Msg f m) -> C -> Data f -> Ui.Attribute m
onPaste config c data =
    let
        decoder =
            Decode.map
                (config.toMsg << UserPasted)
                (Decode.fromSubject
                    (Decode.manySafe
                        (Decoded.decoderInstanceCollection c
                            Decoded.Relaxed
                            data.iriDocument
                            data.path
                            data.grouped
                        )
                    )
                )
    in
    Clipboard.onPasteGraph (config.toMsg Ignored) decoder


viewInstancesList :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Data f
    -> Element m
viewInstancesList config c report isDragging data =
    data.instances
        |> List.indexedMap (viewCollectionFields config c report isDragging data)
        |> Ui.column
            [ Ui.width Ui.fill
            , Ui.spacing Container.configField.spacingFieldY
            ]


viewActionAdd :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Data f
    -> Element m
viewActionAdd config c data =
    let
        name =
            data.grouped.names
                |> List.head
                |> Maybe.map (Localize.forLabel c)
                |> Maybe.withDefault "value"
    in
    [ "Add "
    , name
    ]
        |> String.concat
        |> Fluent.verbatim
        |> button (config.toMsg UserPressedAddCollection)
        |> Button.withId (Breadcrumbs.id data.path ++ "--add")
        |> Button.withFormat Button.Outlined
        |> Button.view


viewCollectionFields :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Data f
    -> Int
    -> Instance f
    -> Element m
viewCollectionFields config c report isDragging data index instance =
    Instance.view
        (configInstance config data index)
        c
        index
        (globalInstance data)
        (localInstance config c report isDragging data index instance)


{-| -}
viewFullscreen :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Model f
    -> Maybe (Element m)
viewFullscreen config c report isDragging (Model data) =
    data.instances
        |> List.indexedMap Tuple.pair
        |> List.filterMap
            (\( index, instance ) ->
                instance.form
                    |> config.fullscreen c report isDragging
                    |> Maybe.map (Ui.map (MsgForm index >> config.toMsg))
            )
        |> List.head


{-| -}
card :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Model f
    -> Maybe (Container m)
card config c (Model data) =
    if List.length data.instances == 0 then
        Nothing

    else
        Ui.column
            [ Ui.width Ui.fill
            , Ui.spacing 4
            ]
            [ cardHeading c data
            , cardInstances config c data
            ]
            |> Container.expand
            |> Just


cardHeading : C -> Data f -> Element m
cardHeading c data =
    Ui.el
        [ Ui.paddingEach
            { top = 16
            , bottom = 0
            , left = 0
            , right = 0
            }
        ]
        (Typography.caption
            (textHeading c data)
        )


cardInstances :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Data f
    -> Element m
cardInstances config c data =
    case data.grouped.viewer of
        ValueCardViewer ->
            data.instances
                |> List.indexedMap (cardCollectionFields config c data.breadcrumbs)
                |> Ui.column
                    [ Ui.width Ui.fill
                    , Ui.spacing Container.configCard.spacingFieldY
                    ]

        ValueTableViewer ->
            { data = List.map .form data.instances
            , columns = config.columns c [] data.grouped.field
            }
                |> table
                |> Table.view c


cardCollectionFields :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Breadcrumbs
    -> Int
    -> Instance f
    -> Element m
cardCollectionFields config c breadcrumbs index instance =
    Ui.el
        [ Ui.width Ui.fill
        , Ui.padding 16
        , Border.rounded 12
        , Border.width 1
        , Border.color Color.outlineVariant
        , Ui.mouseOver [ Background.color (backgroundColor breadcrumbs) ]
        , Elevation.z2Hover
        ]
        (instance.form
            |> config.card c
            |> Maybe.map (Ui.map (MsgForm index >> config.toMsg))
            |> Maybe.withDefault Ui.none
        )


{-| -}
columns :
    Config e f m (Model f) (Msg f m)
    -> C
    -> List String
    -> Grouped.DataCollection
    -> List (Table.Column f m)
columns _ _ _ _ =
    []


{-| -}
help : Config e f m (Model f) (Msg f m) -> C -> Report -> Model f -> List Fluent
help config c report (Model data) =
    helpCollection c report data
        ++ List.concatMap
            (.form
                >> config.nodeFocus
                >> helpInstanceForSummary c report data.nodeFocus data
            )
            data.instances


helpCollection : C -> Report -> Data form -> List Fluent
helpCollection c report data =
    report.results
        |> List.filter
            (\((ValidationResult { value }) as validationResult) ->
                Report.validationResultFor
                    (Report.emptySelector
                        |> Maybe.apply Report.selectFocusNode (Rdf.toIri data.nodeFocus)
                        |> Report.selectPath data.grouped.path
                    )
                    validationResult
                    && (value == Nothing)
            )
        |> List.filterMap (helpCollectionFromValidationResult c data)


helpCollectionFromValidationResult :
    C
    -> Data form
    -> ValidationResult
    -> Maybe Fluent
helpCollectionFromValidationResult c data (ValidationResult { constraintComponent, message }) =
    let
        name =
            data.grouped.names
                |> List.head
                |> Maybe.map (Localize.withinSentenceWithArticleIndefinite c)
                |> Maybe.withDefault "a value"

        namePlural =
            data.grouped.names
                |> List.head
                |> Maybe.map (Localize.withinSentencePlural c)
                |> Maybe.withDefault "values"
    in
    case constraintComponent of
        MinCountConstraintComponent ->
            if data.grouped.maxCount - data.grouped.minCount > 0 then
                Just
                    (Fluent.verbatim
                        ("Enter "
                            ++ String.fromInt data.grouped.minCount
                            ++ " or more "
                            ++ namePlural
                            ++ "."
                        )
                    )

            else if data.grouped.minCount == 1 then
                Just (Fluent.verbatim ("Enter " ++ name ++ "."))

            else
                Just
                    (Fluent.verbatim
                        ("Enter "
                            ++ String.fromInt data.grouped.minCount
                            ++ " "
                            ++ namePlural
                            ++ "."
                        )
                    )

        MaxCountConstraintComponent ->
            Just
                (Fluent.verbatim
                    ("Enter at most "
                        ++ String.fromInt data.grouped.maxCount
                        ++ " "
                        ++ namePlural
                        ++ "."
                    )
                )

        _ ->
            Just (Fluent.verbatim (Localize.verbatim c message ++ "."))


helpInstanceForSummary :
    C
    -> Report
    -> BlankNodeOrIri
    -> Data form
    -> BlankNodeOrIri
    -> List Fluent
helpInstanceForSummary c report nodeFocus data nodeInstance =
    report.results
        |> List.filter
            (\((ValidationResult _) as validationResult) ->
                Report.validationResultFor
                    (Report.emptySelector
                        |> Maybe.apply Report.selectFocusNode (Rdf.toIri nodeFocus)
                        |> Report.selectPath data.grouped.path
                        |> Maybe.apply Report.selectValue (Rdf.toAnyLiteralOrIri nodeInstance)
                    )
                    validationResult
            )
        |> List.filterMap (helpInstanceForSummaryFromValidationResult c data)


helpInstanceForSummaryFromValidationResult :
    C
    -> Data form
    -> ValidationResult
    -> Maybe Fluent
helpInstanceForSummaryFromValidationResult c data (ValidationResult { constraintComponent, message }) =
    let
        name =
            data.grouped.names
                |> List.head
                |> Maybe.map (Localize.withinSentenceWithArticleIndefinite c)
                |> Maybe.withDefault "a value"
    in
    case constraintComponent of
        NodeConstraintComponent ->
            Just (Fluent.verbatim ("There is a problem with " ++ name ++ "."))

        _ ->
            Just (Fluent.verbatim (Localize.verbatim c message ++ "."))


helpInstance :
    C
    -> Report
    -> BlankNodeOrIri
    -> Data form
    -> BlankNodeOrIri
    -> List Fluent
helpInstance c report nodeFocus data nodeInstance =
    report.results
        |> List.filter
            (\((ValidationResult _) as validationResult) ->
                Report.validationResultFor
                    (Report.emptySelector
                        |> Maybe.apply Report.selectFocusNode (Rdf.toIri nodeFocus)
                        |> Report.selectPath data.grouped.path
                        |> Maybe.apply Report.selectValue (Rdf.toAnyLiteralOrIri nodeInstance)
                    )
                    validationResult
            )
        |> List.filterMap (helpInstanceFromValidationResult c data)


helpInstanceFromValidationResult :
    C
    -> Data form
    -> ValidationResult
    -> Maybe Fluent
helpInstanceFromValidationResult c data (ValidationResult { constraintComponent, message }) =
    let
        name =
            data.grouped.names
                |> List.head
                |> Maybe.map (Localize.withinSentence c)
                |> Maybe.withDefault "value"
    in
    case constraintComponent of
        NodeConstraintComponent ->
            Just (Fluent.verbatim ("There is a problem with this " ++ name ++ "."))

        _ ->
            Just (Fluent.verbatim (Localize.verbatim c message ++ "."))


backgroundColor : Breadcrumbs -> Ui.Color
backgroundColor breadcrumbs =
    case breadcrumbs.depthCollection of
        0 ->
            Color.surfaceContainer

        1 ->
            Color.surfaceContainerHigh

        2 ->
            Color.surfaceContainerHighest

        _ ->
            Color.surfaceContainerHighest


{-| -}
summary : Config e f m (Model f) (Msg f m) -> C -> Model f -> List Fluent
summary config c (Model data) =
    case data.instances of
        [] ->
            []

        [ instance ] ->
            config.summary c instance.form

        _ ->
            case List.head data.grouped.names of
                Nothing ->
                    []

                Just name ->
                    [ Fluent.verbatim
                        (String.concat
                            [ String.fromInt (List.length data.instances)
                            , " "
                            , Localize.withinSentencePlural c name
                            ]
                        )
                    ]


textHeading : C -> Data f -> Fluent
textHeading c data =
    let
        instancesCount =
            data.instances |> List.length |> String.fromInt
    in
    Fluent.verbatim
        (String.concat
            [ data.grouped.names
                |> List.head
                |> Maybe.map (Localize.forLabelPlural c)
                |> Maybe.withDefault "Missing label"
            , " (#"
            , instancesCount
            , ")"
            ]
        )



-- OUTPUT


{-| -}
encoders :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Model f
    -> List Encode.PropertyEncoder
encoders config c (Model data) =
    data.instances
        |> List.indexedMap Tuple.pair
        |> List.map (encoderInstance config c data)


encoderInstance :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Data f
    -> ( Int, Instance f )
    -> PropertyEncoder
encoderInstance config c data ( index, instance ) =
    Encode.property data.grouped.path
        (Encode.node (config.nodeFocus instance.form)
            (List.concat
                [ encodersClasses data
                , encodersIsDefinedBy config data instance
                , encodersOrderPath data index
                , config.encoders c instance.form
                ]
            )
        )


encodersClasses : Data f -> List PropertyEncoder
encodersClasses data =
    List.map
        (Encode.predicate a << Encode.object)
        (NonEmpty.toList data.grouped.classes)


encodersIsDefinedBy :
    Config e f m (Model f) (Msg f m)
    -> Data f
    -> Instance f
    -> List PropertyEncoder
encodersIsDefinedBy config data instance =
    let
        iriDocumentWithFragment =
            Rdf.setFragment (Breadcrumbs.id (config.path instance.form))
                data.iriDocument
    in
    [ Encode.predicate RDFS.isDefinedBy
        (Encode.object iriDocumentWithFragment)
    ]


encodersOrderPath : Data f -> Int -> List PropertyEncoder
encodersOrderPath data index =
    case data.grouped.orderPath of
        Just (PredicatePath hashDetailsEditorOrderPath) ->
            [ Encode.predicate hashDetailsEditorOrderPath
                (Encode.literal (Rdf.int index))
            ]

        _ ->
            []


infinity : Int
infinity =
    ceiling (1 / 0)



-- INFO


{-| -}
toNodeFocus : Model f -> BlankNodeOrIri
toNodeFocus (Model data) =
    data.nodeFocus


{-| -}
toPath : Model f -> Path
toPath (Model data) =
    data.path


{-| -}
isEmpty : Config e f m (Model f) (Msg f m) -> Model f -> Bool
isEmpty _ _ =
    False


{-| -}
dragging : Config e f m (Model f) (Msg f m) -> Model f -> Bool
dragging config (Model data) =
    List.any (.form >> config.dragging) data.instances


{-| -}
log : Config e f m (Model f) (Msg f m) -> Model f -> Pretty.Doc ()
log config (Model data) =
    Pretty.lines
        [ Pretty.string "Collection"
        , Pretty.indent 2 (Pretty.lines (List.map (.form >> config.log) data.instances))
        ]



-- SUPPORT


indexMaxFromInstances :
    Config e f m (Model f) (Msg f m)
    -> List (Instance f)
    -> Int
indexMaxFromInstances config instances =
    instances
        |> List.filterMap (indexFromInstance config)
        |> List.maximum
        |> Maybe.withDefault 0


indexFromInstance : Config e f m (Model f) (Msg f m) -> Instance f -> Maybe Int
indexFromInstance config instance =
    instance.form
        |> config.path
        |> Breadcrumbs.lastOrdinal
