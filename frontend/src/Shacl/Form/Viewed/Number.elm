module Shacl.Form.Viewed.Number exposing
    ( Model
    , init, initWith
    , Msg, update, expandAll
    , subscriptions
    , view, viewFullscreen, card, columns, help, summary
    , encoders
    , toNodeFocus, toPath, isEmpty, dragging, log
    )

{-|


# Model

@docs Model


# Init

@docs init, initWith


# Update

@docs Msg, update, expandAll


# Subscriptions

@docs subscriptions


# View

@docs view, viewFullscreen, card, columns, help, summary


# Output

@docs encoders


# Info

@docs toNodeFocus, toPath, isEmpty, dragging, log

-}

import Api.Decimal as Decimal exposing (Decimal)
import Bool.Apply as Bool
import Element exposing (Element, shrink)
import Element.Lazy exposing (lazy4)
import Fluent exposing (Fluent, verbatim)
import List.Extra as List
import Locale
import Maybe.Apply as Maybe
import Maybe.Extra as Maybe
import Pretty
import Rdf exposing (BlankNodeOrIri, Literal, StringOrLangString)
import Rdf.Decode as Decode exposing (Decoder)
import Rdf.Decode.Extra as Decode
import Rdf.Encode as Encode exposing (PropertyEncoder)
import Rdf.Graph exposing (Seed)
import Rdf.Namespaces exposing (xsd)
import Shacl.Clipboard as Clipboard
import Shacl.Form.Breadcrumbs as Breadcrumbs exposing (Breadcrumbs, Path)
import Shacl.Form.Context exposing (C)
import Shacl.Form.Decoded as Decoded
import Shacl.Form.Effect as Effect
import Shacl.Form.Localize as Localize
import Shacl.Form.Shared exposing (Config, Shared, withInfo)
import Shacl.Form.Translated as Translated exposing (NumberType(..))
import Shacl.Form.Ui as Ui
import Shacl.Form.Ui.Container as Container exposing (Container)
import Shacl.Form.Updated as Updated exposing (Updated)
import Shacl.Report as Report exposing (ConstraintComponent(..), Report, ValidationResult(..))
import String.Extra as String
import Ui.Atom.ErrorWrapper exposing (errorWrapper)
import Ui.Atom.Icon as Icon
import Ui.Atom.Labelled as Labelled
import Ui.Atom.TextInput as TextInput exposing (Affix, textInput)
import Ui.Molecule.Table as Table
import Ui.Molecule.TableCell as TableCell exposing (TableCell)



-- MODEL


{-| -}
type Model
    = Model Data


type alias Data =
    { path : Path
    , breadcrumbs : Breadcrumbs
    , translated : Translated.DataNumber
    , nodeFocus : BlankNodeOrIri
    , editForm : Bool
    , valueGenerated : Maybe String
    , value : String
    , injected : Maybe Injected
    }


type Injected
    = InjectedConstant DataConstant
    | InjectedSelectOne DataSelectOne


type alias DataConstant =
    { translated : Translated.DataConstantInjectable
    , nodeFocus : BlankNodeOrIri
    }


type alias DataSelectOne =
    { translated : Translated.DataSelectOneInjectable
    , nodeFocus : BlankNodeOrIri
    , selected : Maybe Translated.Value
    }



-- INIT


{-| -}
init :
    Config e f m Model Msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Breadcrumbs
    -> Decoded.DataNumber
    -> Updated Model m
init _ { populateDefaultValues } c shared seed breadcrumbs decoded =
    { model =
        let
            generated =
                valueGenerated c shared decoded.nodeFocus decoded.translated
        in
        Model
            { path = decoded.path
            , breadcrumbs = breadcrumbs
            , translated = decoded.translated
            , nodeFocus = decoded.nodeFocus
            , editForm = shared.editForm
            , valueGenerated = generated
            , value =
                case decoded.value of
                    Nothing ->
                        if populateDefaultValues then
                            Maybe.withDefault "" decoded.translated.defaultValue

                        else
                            ""

                    Just value ->
                        value
            , injected =
                case decoded.injected of
                    Nothing ->
                        Nothing

                    Just (Decoded.InjectableConstant data) ->
                        Just
                            (InjectedConstant
                                { translated = data.translated
                                , nodeFocus = data.nodeFocus
                                }
                            )

                    Just (Decoded.InjectableSelectOne data) ->
                        Just
                            (InjectedSelectOne
                                { translated = data.translated
                                , nodeFocus = data.nodeFocus
                                , selected =
                                    data.selected
                                        |> Maybe.orElse (List.head data.translated.options)
                                }
                            )
            }
    , effect = Effect.none
    , seed = seed
    , needsPersisting = False
    }


valueGenerated :
    C
    -> Shared
    -> BlankNodeOrIri
    -> Translated.DataNumber
    -> Maybe String
valueGenerated c shared nodeFocus translated =
    -- FIXME Move this to Decoded
    shared.graphGenerated
        |> Decode.decode
            (Decode.from nodeFocus
                (Decode.zeroOrOneAt translated.path
                    (Decoded.decoderNumber c translated)
                )
            )
        |> Result.toMaybe
        |> Maybe.join


{-| -}
initWith :
    Config e f m Model Msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Model
    -> Result e (Maybe (Updated Model m))
initWith _ _ _ _ _ _ =
    Ok Nothing



-- UPDATE


{-| -}
type Msg
    = UserChangedNumber String
    | UserChangedAffix Affix
    | UserCopied
    | UserPasted String
    | Ignored


{-| -}
update :
    Config e f m Model Msg
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Msg
    -> Model
    -> Updated Model m
update config _ c _ seed msg ((Model data) as model) =
    case msg of
        UserChangedNumber valueUpdated ->
            { data | value = valueUpdated }
                |> Model
                |> Updated.from seed
                |> Updated.needsPersisting

        UserChangedAffix affix ->
            case data.injected of
                Nothing ->
                    Updated.from seed model

                Just (InjectedConstant _) ->
                    Updated.from seed model

                Just (InjectedSelectOne dataSelectOne) ->
                    let
                        withIri option =
                            option.this == affix.value
                    in
                    case List.find withIri dataSelectOne.translated.options of
                        Nothing ->
                            Updated.from seed model

                        Just option ->
                            { data
                                | injected =
                                    Just
                                        (InjectedSelectOne
                                            { dataSelectOne
                                                | selected = Just option
                                            }
                                        )
                            }
                                |> Model
                                |> Updated.from seed
                                |> Updated.needsPersisting

        UserCopied ->
            model
                |> Updated.from seed
                |> Updated.withEffect
                    (encoders config c model
                        |> Clipboard.serialize data.nodeFocus seed
                        |> Effect.copyToClipboard
                    )

        UserPasted valuePasted ->
            { data | value = valuePasted }
                |> Model
                |> Updated.from seed
                |> Updated.needsPersisting

        Ignored ->
            Updated.from seed model


{-| -}
expandAll : Config e f m Model Msg -> Model -> Model
expandAll _ model =
    model



-- SUBSCRIPTIONS


{-| -}
subscriptions : Config e f m Model Msg -> Model -> Sub m
subscriptions _ _ =
    Sub.none



-- VIEW


{-| -}
view :
    Config e f m Model Msg
    -> C
    -> Report
    -> Bool
    -> Model
    -> Maybe (Container m)
view config c report _ model =
    viewHelp config c report model
        |> Container.expand
        |> Just


viewHelp : Config e f m Model Msg -> C -> Report -> Model -> Element m
viewHelp =
    lazy4
        (\config c report ((Model data) as model) ->
            let
                withAffix =
                    case data.injected of
                        Nothing ->
                            identity

                        Just (InjectedConstant dataConstant) ->
                            TextInput.withAffix (affixFromValue dataConstant.translated.value)
                                >> Bool.apply TextInput.withSuffix
                                    (dataConstant.translated.inject == Translated.InjectSuffix)

                        Just (InjectedSelectOne dataSelectOne) ->
                            Maybe.apply (TextInput.withAffix << affixFromValue) dataSelectOne.selected
                                >> TextInput.withAffixes UserChangedAffix
                                    (List.map affixFromValue dataSelectOne.translated.options)
                                >> Bool.apply TextInput.withSuffix
                                    (dataSelectOne.translated.inject == Translated.InjectSuffix)

                affixFromValue value =
                    { label = Localize.forLabel c value.label
                    , value = value.this
                    }
            in
            data
                |> valueActual
                |> Maybe.withDefault ""
                |> textInput UserChangedNumber (TextInput.labelAbove (Ui.label c data.translated))
                |> TextInput.withId (Breadcrumbs.id data.path)
                |> Maybe.apply TextInput.withSupportingText (Ui.description c data.translated)
                |> withAffix
                |> TextInput.withReadonly (readonly data)
                |> TextInput.withHoverableActionIcon
                    { icon = Icon.ContentCopy
                    , onClick = UserCopied
                    }
                |> TextInput.onPasteGraph Ignored (onPaste c data)
                |> TextInput.onCopy UserCopied
                |> TextInput.view
                |> errorWrapper (help config c report model)
                |> Element.map config.toMsg
        )


readonly : Data -> Bool
readonly data =
    data.translated.readonly || (data.translated.persisted && data.editForm)


onPaste : C -> Data -> Decoder Msg
onPaste c data =
    Decode.map UserPasted
        (Decode.oneAtAnyPredicate (Decoded.decoderNumber c data.translated))


{-| -}
viewFullscreen :
    Config e f m Model Msg
    -> C
    -> Report
    -> Bool
    -> Model
    -> Maybe (Element m)
viewFullscreen _ _ _ _ _ =
    Nothing


{-| -}
card : Config e f m Model Msg -> C -> Model -> Maybe (Container msg)
card _ c (Model data) =
    data
        |> valueActual
        |> Maybe.andThen (viewValue c data)
        |> Maybe.map Container.wrappable


viewValue : C -> Data -> String -> Maybe (Element msg)
viewValue c data value =
    case data.translated.numberType of
        NumberInteger ->
            value
                |> String.toInt
                |> Maybe.map (viewInt c data)

        NumberFloat ->
            value
                |> fixDecimalSeparator
                |> String.toFloat
                |> Maybe.map (viewFloat c data)

        NumberDecimal ->
            value
                |> fixDecimalSeparator
                |> Decimal.fromString Locale.EnUS
                |> Maybe.map (viewDecimal c data)


viewInt : C -> Data -> Int -> Element msg
viewInt c data =
    case data.injected of
        Nothing ->
            Labelled.int
                { label = Ui.label c data.translated }

        Just (InjectedConstant dataConstant) ->
            Labelled.intWithAffix (Ui.label c data.translated)
                { label = Localize.forLabel c dataConstant.translated.value.label }

        Just (InjectedSelectOne dataSelectOne) ->
            case dataSelectOne.selected of
                Nothing ->
                    Labelled.int
                        { label = Ui.label c data.translated }

                Just selected ->
                    Labelled.intWithAffix (Ui.label c data.translated)
                        { label = Localize.forLabel c selected.label }


viewFloat : C -> Data -> Float -> Element msg
viewFloat c data =
    case data.injected of
        Nothing ->
            Labelled.float
                { label = Ui.label c data.translated }

        Just (InjectedConstant dataConstant) ->
            Labelled.floatWithAffix (Ui.label c data.translated)
                { label = Localize.forLabel c dataConstant.translated.value.label }

        Just (InjectedSelectOne dataSelectOne) ->
            case dataSelectOne.selected of
                Nothing ->
                    Labelled.float
                        { label = Ui.label c data.translated }

                Just selected ->
                    Labelled.floatWithAffix (Ui.label c data.translated)
                        { label = Localize.forLabel c selected.label }


viewDecimal : C -> Data -> Decimal -> Element msg
viewDecimal c data =
    case data.injected of
        Nothing ->
            Labelled.decimal
                { locale = c.locale
                , label = Ui.label c data.translated
                }

        Just (InjectedConstant dataConstant) ->
            Labelled.decimalWithAffix c.locale
                (Ui.label c data.translated)
                { label = Localize.forLabel c dataConstant.translated.value.label }

        Just (InjectedSelectOne dataSelectOne) ->
            case dataSelectOne.selected of
                Nothing ->
                    Labelled.decimal
                        { locale = c.locale
                        , label = Ui.label c data.translated
                        }

                Just selected ->
                    Labelled.decimalWithAffix c.locale
                        (Ui.label c data.translated)
                        { label = Localize.forLabel c selected.label }


{-| -}
columns :
    Config e f m Model Msg
    -> C
    -> List String
    -> Translated.DataNumber
    -> List (Table.Column f m)
columns config c scope translated =
    [ { header = viewHeader c scope translated
      , width = shrink
      , view = viewCell config c
      }
    ]


viewHeader : C -> List String -> Translated.DataNumber -> TableCell msg
viewHeader c scope translated =
    translated
        |> Ui.label c
        |> TableCell.textBold
        |> withInfo scope


viewCell : Config e f m Model Msg -> C -> f -> TableCell m
viewCell config c form =
    case config.toField form of
        Nothing ->
            TableCell.empty

        Just (Model data) ->
            toCell c data (valueActual data)


toCell : C -> Data -> Maybe String -> TableCell m
toCell c data value =
    let
        withAffix =
            case data.injected of
                Nothing ->
                    identity

                Just (InjectedConstant dataConstant) ->
                    TableCell.withAffix
                        { label = Localize.forLabel c dataConstant.translated.value.label
                        , value = dataConstant.translated.value.this
                        }

                Just (InjectedSelectOne dataSelectOne) ->
                    case dataSelectOne.selected of
                        Nothing ->
                            identity

                        Just selected ->
                            TableCell.withAffix
                                { label = Localize.forLabel c selected.label
                                , value = selected.this
                                }
    in
    case data.translated.numberType of
        NumberInteger ->
            { value =
                value
                    |> Maybe.andThen String.toInt
                    |> Maybe.map Decimal.fromInt
            , valueDecimalPlaces = 0
            , prefix = Nothing
            , minimalPreDecimalPositions = 0
            }
                |> TableCell.decimalStatic
                |> withAffix

        NumberFloat ->
            { value =
                value
                    |> Maybe.andThen String.toFloat
                    |> Maybe.andThen Decimal.fromFloat
            , valueDecimalPlaces = 0
            , prefix = Nothing
            , minimalPreDecimalPositions = 0
            }
                |> TableCell.decimalStatic
                |> withAffix

        NumberDecimal ->
            case Maybe.andThen (Decimal.fromString Locale.EnUS) value of
                Nothing ->
                    TableCell.empty

                Just decimal ->
                    { value = Just decimal
                    , valueDecimalPlaces = Decimal.decimalPlacesCount decimal
                    , prefix = Nothing
                    , minimalPreDecimalPositions = 0
                    }
                        |> TableCell.decimalStatic
                        |> withAffix


{-| -}
help : Config e f m Model Msg -> C -> Report -> Model -> List Fluent
help _ c report (Model data) =
    report.results
        |> Report.validationResultsFor
            (Report.emptySelector
                |> Maybe.apply Report.selectFocusNode (Rdf.toIri data.nodeFocus)
                |> Report.selectPath data.translated.path
            )
        |> List.concatMap (helpFromValidationResult c data)
        |> List.map verbatim


helpFromValidationResult : C -> Data -> ValidationResult -> List String
helpFromValidationResult c data (ValidationResult { constraintComponent, message, detail }) =
    let
        nameForLabel =
            Maybe.unwrap "The input" (Localize.forLabel c) (List.head data.translated.names)

        nameWithinSentence =
            Maybe.unwrap "the input" (Localize.withinSentence c) (List.head data.translated.names)
    in
    case constraintComponent of
        DatatypeConstraintComponent ->
            let
                min =
                    data.translated.minInclusive
                        |> Maybe.orElse data.translated.minExclusive
                        |> Maybe.map Rdf.toValue

                max =
                    data.translated.maxInclusive
                        |> Maybe.orElse data.translated.maxExclusive
                        |> Maybe.map Rdf.toValue
            in
            case data.translated.numberType of
                NumberInteger ->
                    let
                        example =
                            case ( Maybe.andThen String.toInt min, Maybe.andThen String.toInt max ) of
                                ( Nothing, Nothing ) ->
                                    42

                                ( Just minInt, Nothing ) ->
                                    minInt + 1

                                ( Nothing, Just maxInt ) ->
                                    maxInt - 1

                                ( Just minInt, Just maxInt ) ->
                                    (maxInt - minInt) // 2
                    in
                    [ nameForLabel
                    , " must be a whole number, like "
                    , String.fromInt example
                    , "."
                    ]
                        |> String.concat
                        |> List.singleton

                NumberFloat ->
                    let
                        example =
                            case ( Maybe.andThen String.toFloat min, Maybe.andThen String.toFloat max ) of
                                ( Nothing, Nothing ) ->
                                    3.14

                                ( Just minFloat, Nothing ) ->
                                    minFloat + 1

                                ( Nothing, Just maxFloat ) ->
                                    maxFloat - 1

                                ( Just minFloat, Just maxFloat ) ->
                                    (maxFloat - minFloat) / 2
                    in
                    [ nameForLabel
                    , " must be a decimal number, like "
                    , localizeValue c data.translated.numberType (String.fromFloat example)
                    , "."
                    ]
                        |> String.concat
                        |> List.singleton

                NumberDecimal ->
                    let
                        example =
                            case ( Maybe.andThen (Decimal.fromString Locale.EnUS) min, Maybe.andThen (Decimal.fromString Locale.EnUS) max ) of
                                ( Nothing, Nothing ) ->
                                    3.14

                                ( Just minDecimal, Nothing ) ->
                                    Decimal.toFloat minDecimal + 1

                                ( Nothing, Just maxDecimal ) ->
                                    Decimal.toFloat maxDecimal - 1

                                ( Just minDecimal, Just maxDecimal ) ->
                                    (Decimal.toFloat maxDecimal - Decimal.toFloat minDecimal) / 2
                    in
                    [ nameForLabel
                    , " must be a decimal number, like "
                    , localizeValue c data.translated.numberType (String.fromFloat example)
                    , "."
                    ]
                        |> String.concat
                        |> List.singleton

        NodeConstraintComponent ->
            List.concatMap (helpFromValidationResult c data) detail

        MinCountConstraintComponent ->
            [ "Enter the "
            , nameWithinSentence
            , "."
            ]
                |> String.concat
                |> List.singleton

        MinExclusiveConstraintComponent ->
            case data.translated.minExclusive of
                Nothing ->
                    [ Localize.verbatim c message
                    , "."
                    ]
                        |> String.concat
                        |> List.singleton

                Just minExclusive ->
                    [ "The "
                    , nameWithinSentence
                    , " must be higher than "
                    , Rdf.toValue minExclusive
                    , "."
                    ]
                        |> String.concat
                        |> List.singleton

        MinInclusiveConstraintComponent ->
            case data.translated.minInclusive of
                Nothing ->
                    [ Localize.verbatim c message
                    , "."
                    ]
                        |> String.concat
                        |> List.singleton

                Just minInclusive ->
                    [ "The "
                    , nameWithinSentence
                    , " must be "
                    , Rdf.toValue minInclusive
                    , " or higher."
                    ]
                        |> String.concat
                        |> List.singleton

        MaxExclusiveConstraintComponent ->
            case data.translated.maxExclusive of
                Nothing ->
                    [ Localize.verbatim c message
                    , "."
                    ]
                        |> String.concat
                        |> List.singleton

                Just maxExclusive ->
                    [ "The "
                    , nameWithinSentence
                    , " must be lower than "
                    , Rdf.toValue maxExclusive
                    , "."
                    ]
                        |> String.concat
                        |> List.singleton

        MaxInclusiveConstraintComponent ->
            case data.translated.maxInclusive of
                Nothing ->
                    [ Localize.verbatim c message
                    , "."
                    ]
                        |> String.concat
                        |> List.singleton

                Just maxInclusive ->
                    [ "The "
                    , nameWithinSentence
                    , " must be "
                    , Rdf.toValue maxInclusive
                    , " or lower."
                    ]
                        |> String.concat
                        |> List.singleton

        _ ->
            [ Localize.verbatim c message
            , "."
            ]
                |> String.concat
                |> List.singleton


{-| -}
summary : Config e f m Model Msg -> C -> Model -> List Fluent
summary _ c (Model data) =
    case valueActual data of
        Nothing ->
            []

        Just value ->
            if cleanup value == "" then
                []

            else
                case data.injected of
                    Nothing ->
                        [ Fluent.verbatim value ]

                    Just (InjectedConstant dataConstant) ->
                        summaryWithAffix c
                            dataConstant.translated.inject
                            dataConstant.translated.value.label
                            value

                    Just (InjectedSelectOne dataSelectOne) ->
                        case dataSelectOne.selected of
                            Nothing ->
                                [ Fluent.verbatim value ]

                            Just selected ->
                                summaryWithAffix c
                                    dataSelectOne.translated.inject
                                    selected.label
                                    value


summaryWithAffix : C -> Translated.Inject -> StringOrLangString -> String -> List Fluent
summaryWithAffix c inject label value =
    case inject of
        Translated.InjectSuffix ->
            [ Fluent.verbatim (value ++ " " ++ Localize.forLabel c label) ]

        Translated.InjectPrefix ->
            [ Fluent.verbatim (Localize.forLabel c label ++ " " ++ value) ]



-- OUTPUT


{-| -}
encoders : Config e f m Model Msg -> C -> Model -> List PropertyEncoder
encoders _ _ (Model data) =
    if data.translated.generated then
        if data.translated.readonly then
            []

        else if valueGeneratedOverwritten data then
            encodersValue data

        else
            []

    else
        encodersValue data


encodersValue : Data -> List PropertyEncoder
encodersValue data =
    case valueActual data of
        Nothing ->
            []

        Just "" ->
            []

        Just value ->
            Encode.property data.translated.path
                (Encode.literal (literalValue data value))
                :: encodersInjected data


encodersInjected : Data -> List PropertyEncoder
encodersInjected { injected } =
    case injected of
        Nothing ->
            []

        Just (InjectedConstant data) ->
            [ Encode.from data.nodeFocus
                [ Encode.property data.translated.path
                    (Encode.iri data.translated.value.this)
                ]
            ]

        Just (InjectedSelectOne data) ->
            case data.selected of
                Nothing ->
                    []

                Just selected ->
                    [ Encode.from data.nodeFocus
                        [ Encode.property data.translated.path
                            (Encode.iri selected.this)
                        ]
                    ]


literalValue : Data -> String -> Literal a
literalValue data value =
    let
        datatype =
            case data.translated.numberType of
                NumberInteger ->
                    xsd "integer"

                NumberFloat ->
                    xsd "double"

                NumberDecimal ->
                    xsd "decimal"
    in
    unlocalizeValue data.translated.numberType value
        |> Maybe.map (Rdf.literal datatype)
        |> Maybe.withDefault (Rdf.literal (xsd "string") value)


unlocalizeValue : NumberType -> String -> Maybe String
unlocalizeValue numberType value =
    case numberType of
        NumberInteger ->
            value
                |> String.toInt
                |> Maybe.map String.fromInt

        NumberFloat ->
            value
                |> fixDecimalSeparator
                |> String.toFloat
                |> Maybe.map String.fromFloat

        NumberDecimal ->
            value
                |> fixDecimalSeparator
                |> Decimal.fromString Locale.EnUS
                |> Maybe.map (Decimal.toString Locale.EnUS)


{-| Try to (gracefully) fix the decimal separator. The resulting string should
only contain at most one `.` and no other `.`'s or `,`'s. This also removes
optional 1000-separators by assuming that the right most `.` or `,` is the
decimal separtor.

    fixDecimalSeparator "42"
    --> "42"

    fixDecimalSeparator "3.14"
    --> "3.14"

    fixDecimalSeparator "3,14"
    --> "3.14"

    fixDecimalSeparator "1.234,56"
    --> "1234.56"

    fixDecimalSeparator "1,234.56"
    --> "1234.56"

    fixDecimalSeparator "1,234,567"
    --> "1234567"

    fixDecimalSeparator "1.234.567"
    --> "1234567"

-}
fixDecimalSeparator : String -> String
fixDecimalSeparator raw =
    let
        countPeriods =
            String.countOccurrences "." raw

        countCommas =
            String.countOccurrences "," raw
    in
    if countPeriods > 1 && countCommas == 0 then
        String.replace "." "" raw

    else if countPeriods == 0 && countCommas > 1 then
        String.replace "," "" raw

    else if countPeriods == 0 && countCommas == 0 then
        raw

    else
        raw
            |> String.toList
            |> List.foldr
                (\char ( foundDecimalSeparator, collected ) ->
                    if foundDecimalSeparator then
                        if char == '.' || char == ',' then
                            ( True, collected )

                        else
                            ( True, char :: collected )

                    else if char == '.' then
                        ( True, char :: collected )

                    else if char == ',' then
                        ( True, '.' :: collected )

                    else
                        ( False, char :: collected )
                )
                ( False, [] )
            |> Tuple.second
            |> String.fromList



-- INFO


{-| -}
toNodeFocus : Model -> BlankNodeOrIri
toNodeFocus (Model data) =
    data.nodeFocus


{-| -}
toPath : Model -> Path
toPath (Model data) =
    data.path


{-| -}
isEmpty : Config e f m Model Msg -> Model -> Bool
isEmpty _ (Model data) =
    cleanup data.value == ""


{-| -}
dragging : Config e f m Model Msg -> Model -> Bool
dragging _ _ =
    False


{-| -}
log : Config e f m Model Msg -> Model -> Pretty.Doc t
log _ (Model _) =
    Pretty.string "Number"



-- SUPPORT


valueActual : Data -> Maybe String
valueActual data =
    if data.translated.generated then
        if data.translated.readonly then
            data.valueGenerated

        else if cleanup data.value == "" then
            data.valueGenerated

        else
            Just (cleanup data.value)

    else
        Just (cleanup data.value)


valueGeneratedOverwritten : Data -> Bool
valueGeneratedOverwritten data =
    data.valueGenerated /= valueActual data


cleanup : String -> String
cleanup =
    String.trim


localizeValue : C -> NumberType -> String -> String
localizeValue c numberType value =
    case numberType of
        NumberInteger ->
            value
                |> String.toInt
                |> Maybe.map String.fromInt
                |> Maybe.withDefault value

        NumberFloat ->
            value
                |> String.toFloat
                |> Maybe.map
                    (String.fromFloat
                        >> (case c.locale of
                                Locale.De ->
                                    String.replace "." ","

                                Locale.EnUS ->
                                    identity
                           )
                    )
                |> Maybe.withDefault value

        NumberDecimal ->
            value
                |> Decimal.fromString Locale.EnUS
                |> Maybe.map (Decimal.toString c.locale)
                |> Maybe.withDefault value
