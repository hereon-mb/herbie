module Shacl.Form.Viewed.Schedule exposing
    ( Model
    , init, initWith
    , Msg, update, expandAll
    , subscriptions
    , view, viewFullscreen, card, columns, help, summary
    , encoders
    , toNodeFocus, toPath, isEmpty, dragging, log
    )

{-|


# Model

@docs Model


# Init

@docs init, initWith


# Update

@docs Msg, update, expandAll


# Subscriptions

@docs subscriptions


# View

@docs view, viewFullscreen, card, columns, help, summary


# Output

@docs encoders


# Info

@docs toNodeFocus, toPath, isEmpty, dragging, log

-}

import Api
import Basics.Extra exposing (uncurry)
import Date exposing (Date)
import Date.Format
import Dict exposing (Dict)
import Dict.Extra as Dict
import Element as Ui exposing (Element)
import Element.Lazy as Ui
import Fluent exposing (Fluent)
import List.Extra as List
import List.NonEmpty as NonEmpty
import Maybe.Apply as Maybe
import Maybe.Extra as Maybe
import Pretty
import Rdf exposing (BlankNodeOrIri, Iri)
import Rdf.Decode as Decode
import Rdf.Decode.Extra as Decode
import Rdf.Encode as Encode exposing (PropertyEncoder)
import Rdf.Extra as Rdf
import Rdf.Graph exposing (Seed)
import Rdf.Namespaces exposing (a)
import Rdf.Namespaces.RDFS as RDFS
import Rdf.PropertyPath exposing (PropertyPath(..))
import Shacl exposing (Viewer(..))
import Shacl.Clipboard as Clipboard
import Shacl.Form.Breadcrumbs as Breadcrumbs exposing (Breadcrumbs, Path)
import Shacl.Form.Context exposing (C)
import Shacl.Form.Decoded as Decoded
import Shacl.Form.Effect as Effect
import Shacl.Form.Grouped as Grouped
import Shacl.Form.Instance exposing (Instance)
import Shacl.Form.Localize as Localize
import Shacl.Form.Shared exposing (Config, Shared)
import Shacl.Form.Ui.Container as Container exposing (Container)
import Shacl.Form.Updated as Updated exposing (Updated)
import Shacl.Form.Viewed.Collection.Instance as Instance
import Shacl.Report as Report exposing (ConstraintComponent(..), Report, ValidationResult(..))
import Time.Format
import Ui.Atom.Button as Button exposing (button)
import Ui.Atom.Divider as Divider
import Ui.Molecule.Table as Table exposing (table)
import Ui.Theme.Card as Card
import Ui.Theme.Input as Input
import Ui.Theme.Typography as Typography



-- MODEL


{-| -}
type Model f
    = Model (Data f)


type alias Data f =
    { path : Path
    , breadcrumbs : Breadcrumbs
    , grouped : Grouped.DataSchedule
    , nodeFocus : BlankNodeOrIri
    , iriDocument : Iri
    , date : Maybe Date
    , instances : List (InstanceSchedule f)
    , actionActive : Instance.Action
    , indexMax : Int

    -- FIXME
    -- , instancesSample :
    --     List
    --         { label : StringOrLangString
    --         , fields : List field
    --         }
    }


type alias InstanceSchedule f =
    { dayOffset : Int
    , order : Int
    , instance : Instance f
    }



-- INIT


{-| -}
init :
    Config e f m (Model f) (Msg f m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Breadcrumbs
    -> Decoded.DataSchedule
    -> Updated (Model f) m
init config populateDefaultValues c shared seed breadcrumbs decoded =
    let
        modelFromInstances instances =
            Model
                { path = decoded.path
                , breadcrumbs = breadcrumbs
                , grouped = decoded.grouped
                , nodeFocus = decoded.nodeFocus
                , date = decoded.date
                , iriDocument = shared.iri
                , instances = instances
                , actionActive = Instance.init
                , indexMax = indexMaxFromInstances config instances
                }

        -- FIXME
        --     instancesSample dayOffsetPath orderPath idSamples =
        --         idSamples
        --             |> List.filterMap
        --                 (\idScheduleEntry ->
        --                     Maybe.map
        --                         (\labelSampleStep ->
        --                             { label = labelSampleStep
        --                             , fields =
        --                                 shNodeShape
        --                                     |> fieldsGroupedFromNodeShape idScheduleEntry
        --                                     |> removeInternalFields dayOffsetPath orderPath
        --                             }
        --                         )
        --                         (Rdf.emptyQuery
        --                             |> Rdf.withSubject idScheduleEntry
        --                             |> Rdf.withPropertyPath (Rdf.PredicatePath (rdfs "label"))
        --                             |> Rdf.getStringOrLangString shared.graphCombinedBackend
        --                         )
        --                 )
    in
    decoded.instances
        |> List.foldl (initInstance config populateDefaultValues c shared seed breadcrumbs)
            ( { model = []
              , effect = Effect.none
              , seed = seed
              , needsPersisting = False
              }
            , 0
            )
        |> Tuple.first
        |> Updated.map
            (List.reverse
                >> sortAndNormalizeInstanceSchedules
                >> modelFromInstances
            )


initInstance :
    Config e f m (Model f) (Msg f m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Breadcrumbs
    -> Decoded.InstanceSchedule
    -> ( Updated (List (InstanceSchedule f)) m, Int )
    -> ( Updated (List (InstanceSchedule f)) m, Int )
initInstance config populateDefaultValues c shared seed breadcrumbs instance ( updated, index ) =
    let
        breadcrumbsNested =
            breadcrumbs
                |> increaseDepthCollection

        updatedForm =
            instance.field
                |> config.init populateDefaultValues c shared seed breadcrumbsNested
    in
    ( { model =
            { dayOffset = instance.dayOffset
            , order = instance.order
            , instance =
                { breadcrumbs = breadcrumbsNested
                , expanded = False
                , form = updatedForm.model
                , label = instance.label
                }
            }
                :: updated.model
      , effect =
            Effect.batch
                [ Effect.map (MsgForm index >> config.toMsg) updatedForm.effect
                , updated.effect
                ]
      , seed = updatedForm.seed
      , needsPersisting = updatedForm.needsPersisting || updated.needsPersisting
      }
    , index + 1
    )


increaseDepthCollection : Breadcrumbs -> Breadcrumbs
increaseDepthCollection breadcrumbs =
    { breadcrumbs | depthCollection = breadcrumbs.depthCollection + 1 }


{-| -}
initWith :
    Config e f m (Model f) (Msg f m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Model f
    -> Result e (Maybe (Updated (Model f) m))
initWith _ _ _ _ _ _ =
    -- FIXME Since we allow blank nodes as focus nodes for each schedule item,
    -- it is not possible to partially init the components from a new graph, as
    -- in the new graph the blank nodes might have changed and cannot be mapped
    -- any more.  We therefore do not update the schedule items, which could
    -- lead to stale UI.  We could add some indicator in the UI at some point.
    Ok Nothing



-- UPDATE


{-| -}
type Msg f m
    = Ignored
    | UserPressedAddSchedule
    | ApiReturnedDecoded (Api.Response Decoded.Field)
    | UserPasted (List Decoded.InstanceSchedule)
    | ApiReturnedDuplicated (List Decoded.InstanceSchedule) (Api.Response Decoded.Field)
    | MsgInstance Int Instance.Msg
    | MsgForm Int m


{-| -}
update :
    Config e f m (Model f) (Msg f m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Msg f m
    -> Model f
    -> Updated (Model f) m
update config populateDefaultValues c shared seed msg ((Model ({ breadcrumbs } as data)) as model) =
    case msg of
        Ignored ->
            Updated.from seed model

        UserPressedAddSchedule ->
            let
                indexNew =
                    data.indexMax + 1
            in
            { data | indexMax = indexNew }
                |> Model
                |> Updated.from seed
                |> Updated.withEffect
                    (data.grouped.field
                        |> Effect.createField
                            ApiReturnedDecoded
                            shared.preview
                            (Breadcrumbs.Ordinal indexNew :: data.path)
                            -- FIXME Mint from all classes
                            (Decoded.MintIri
                                { nodeFocus = data.nodeFocus
                                , class = NonEmpty.head data.grouped.classes
                                }
                            )
                        |> Effect.map config.toMsg
                    )

        ApiReturnedDecoded (Err error) ->
            model
                |> Updated.from seed
                |> Updated.withEffect (Effect.addToastError error)

        ApiReturnedDecoded (Ok field) ->
            let
                breadcrumbsNested =
                    breadcrumbs
                        |> increaseDepthCollection

                indexNew =
                    List.length data.instances
            in
            config.init populateDefaultValues c shared seed breadcrumbsNested field
                |> Updated.map (\form -> Model (addSchedule breadcrumbsNested form data))
                |> Updated.needsPersisting
                |> Updated.mapEffect (MsgForm indexNew)
                |> Updated.withEffect
                    (Effect.map (MsgInstance indexNew)
                        (Instance.focusAction
                            (configInstance config data indexNew)
                            indexNew
                            data.actionActive
                        )
                    )
                |> Updated.mapEffect config.toMsg

        UserPasted instances ->
            Updated.from seed model
                |> duplicateNextInstance config shared instances

        ApiReturnedDuplicated instances (Err error) ->
            model
                |> Updated.from seed
                |> Updated.withEffect (Effect.addToastError error)
                |> duplicateNextInstance config shared instances

        ApiReturnedDuplicated instances (Ok field) ->
            let
                breadcrumbsNested =
                    breadcrumbs
                        |> increaseDepthCollection

                indexNew =
                    List.length data.instances
            in
            config.init populateDefaultValues c shared seed breadcrumbsNested field
                |> Updated.map (\form -> Model (addSchedule breadcrumbsNested form data))
                |> Updated.needsPersisting
                |> Updated.mapEffect (MsgForm indexNew)
                |> Updated.withEffect
                    (Effect.map (MsgInstance indexNew)
                        (Instance.focusAction
                            (configInstance config data indexNew)
                            indexNew
                            data.actionActive
                        )
                    )
                |> Updated.mapEffect config.toMsg
                |> duplicateNextInstance config shared instances

        MsgInstance index msgInstance ->
            case List.getAt index data.instances of
                Nothing ->
                    Updated.from seed model

                Just instanceSchedule ->
                    Instance.update
                        (callbacksInstance config c seed model index instanceSchedule)
                        (configInstance config data index)
                        (globalInstance data)
                        index
                        msgInstance

        MsgForm index msgForm ->
            case List.getAt index data.instances of
                Nothing ->
                    Updated.from seed model

                Just ({ instance } as instanceSchedule) ->
                    config.update c shared seed msgForm instanceSchedule.instance.form
                        |> Updated.map
                            (\formUpdated ->
                                Model
                                    { data
                                        | instances =
                                            List.setAt index
                                                { instanceSchedule
                                                    | instance =
                                                        { instance | form = formUpdated }
                                                }
                                                data.instances
                                    }
                            )
                        |> Updated.mapEffect (MsgForm index >> config.toMsg)


duplicateNextInstance :
    Config e f m (Model f) (Msg f m)
    -> Shared
    -> List Decoded.InstanceSchedule
    -> Updated (Model f) m
    -> Updated (Model f) m
duplicateNextInstance config shared instances updated =
    case instances of
        [] ->
            updated

        instance :: rest ->
            let
                indexNew =
                    data.indexMax + 1

                (Model data) =
                    updated.model
            in
            updated
                |> Updated.map
                    (\_ ->
                        Model { data | indexMax = indexNew }
                    )
                |> Updated.withEffect
                    (instance.field
                        |> Effect.duplicateField
                            (ApiReturnedDuplicated rest)
                            shared.preview
                            (Breadcrumbs.Ordinal indexNew :: data.path)
                            -- FIXME Mint from all classes
                            (Decoded.MintIri
                                { nodeFocus = data.nodeFocus
                                , class = NonEmpty.head data.grouped.classes
                                }
                            )
                        |> Effect.map config.toMsg
                    )



-- INSTANCE


configInstance :
    Config e f m (Model f) (Msg f m)
    -> Data f
    -> Int
    -> Instance.Config m
configInstance config data index =
    { id = Breadcrumbs.id data.path
    , depthCollection = data.breadcrumbs.depthCollection
    , withOrder = True
    , unbounded = True
    , toMsg = config.toMsg << MsgInstance index
    , msgTooltip = config.msgTooltip
    }


callbacksInstance :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Seed
    -> Model f
    -> Int
    -> InstanceSchedule f
    -> Instance.Callbacks (Model f) m
callbacksInstance config c seed ((Model data) as model) index ({ instance } as instanceSchedule) =
    { noOp = Updated.from seed model
    , perform =
        \effect ->
            model
                |> Updated.from seed
                |> Updated.withEffect effect
                |> Updated.mapEffect (config.toMsg << MsgInstance index)
    , update =
        \actionActive effect ->
            { data | actionActive = actionActive }
                |> Model
                |> Updated.from seed
                |> Updated.withEffect effect
                |> Updated.mapEffect (config.toMsg << MsgInstance index)
    , moveInstanceUp =
        \actionActive indexNewToEffect ->
            let
                ( indexNew, dataUpdated ) =
                    { data | actionActive = actionActive }
                        |> moveUpwardSchedule index
            in
            dataUpdated
                |> Model
                |> Updated.from seed
                |> Updated.withEffect (indexNewToEffect indexNew)
                |> Updated.mapEffect (config.toMsg << MsgInstance indexNew)
    , moveInstanceDown =
        \actionActive indexNewToEffect ->
            let
                ( indexNew, dataUpdated ) =
                    { data | actionActive = actionActive }
                        |> moveDownwardSchedule index
            in
            dataUpdated
                |> Model
                |> Updated.from seed
                |> Updated.withEffect (indexNewToEffect indexNew)
                |> Updated.mapEffect (config.toMsg << MsgInstance indexNew)
    , copyInstance =
        \actionActive ->
            { data | actionActive = actionActive }
                |> Model
                |> Updated.from seed
                |> Updated.withEffect
                    (encoderInstance config c data ( index, instanceSchedule )
                        |> List.singleton
                        |> Clipboard.serialize data.nodeFocus seed
                        |> Effect.copyToClipboard
                    )
    , removeInstance =
        \actionActive effect ->
            { data
                | instances = List.removeAt index data.instances
                , actionActive = actionActive
            }
                |> Model
                |> Updated.from seed
                |> Updated.withEffect effect
                |> Updated.mapEffect (config.toMsg << MsgInstance index)
    , copyNodeFocus =
        \actionActive ->
            { data | actionActive = actionActive }
                |> Model
                |> Updated.from seed
                |> Updated.withEffect
                    (config.userPressedLink
                        (config.nodeFocus instanceSchedule.instance.form)
                        |> Effect.fromMsg
                    )
    , toggleExpansion =
        \actionActive effect ->
            { data
                | instances =
                    List.setAt index
                        { instanceSchedule
                            | instance =
                                { instance | expanded = not instance.expanded }
                        }
                        data.instances
                , actionActive = actionActive
            }
                |> Model
                |> Updated.from seed
                |> Updated.withEffect effect
                |> Updated.mapEffect (config.toMsg << MsgInstance index)
    }


globalInstance : Data f -> Instance.Global
globalInstance data =
    { count = List.length data.instances
    , actionActive = data.actionActive
    }


localInstance :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Data f
    -> Int
    -> InstanceSchedule f
    -> Instance.Local m
localInstance config c report isDragging data index instanceSchedule =
    let
        ordinal =
            data.date
                |> Maybe.map
                    (Date.add Date.Days instanceSchedule.dayOffset
                        >> Date.Format.shortWithWeekday Time.Format.Iso8601
                    )
                |> Maybe.withDefault
                    ("Day " ++ String.fromInt instanceSchedule.dayOffset)
                |> Fluent.verbatim
    in
    { ordinal = ordinal
    , label = instanceSchedule.instance.label
    , summary = config.summary c instanceSchedule.instance.form
    , help =
        helpInstance c
            report
            data.nodeFocus
            data
            (config.nodeFocus instanceSchedule.instance.form)
    , expanded = instanceSchedule.instance.expanded
    , form =
        config.view c report isDragging instanceSchedule.instance.form
            |> Maybe.map (Ui.map (MsgForm index >> config.toMsg))
            |> Maybe.withDefault Ui.none
    }


{-| -}
expandAll : Config e f m (Model f) (Msg f m) -> Model f -> Model f
expandAll config (Model data) =
    Model
        { data
            | instances =
                List.map
                    (\({ instance } as instanceSchedule) ->
                        { instanceSchedule
                            | instance =
                                { instance
                                    | expanded = True
                                    , form = config.expandAll instance.form
                                }
                        }
                    )
                    data.instances
        }



-- SUBSCRIPTIONS


{-| -}
subscriptions : Config e f m (Model f) (Msg f m) -> Model f -> Sub m
subscriptions config (Model data) =
    data.instances
        |> List.map (.instance >> .form >> config.subscriptions)
        |> Sub.batch



-- VIEW


{-| -}
view :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Model f
    -> Maybe (Container m)
view config c report isDragging model =
    viewHelp config c report isDragging model
        |> Container.expand
        |> Just


viewHelp :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Model f
    -> Element m
viewHelp =
    Ui.lazy5
        (\config c report isDragging (Model data) ->
            Ui.column
                [ Ui.width Ui.fill
                , Ui.spacing 16
                , Ui.paddingEach
                    { top = 0
                    , bottom = 16
                    , left = 0
                    , right = 0
                    }
                ]
                [ viewHeading c data
                , Input.help (help config c report (Model data))
                , if List.length data.instances == 0 then
                    viewInstancesEmpty config c data

                  else
                    viewInstances config c report isDragging data
                ]
        )


viewHeading : C -> Data f -> Element m
viewHeading c data =
    let
        toHeading =
            if data.breadcrumbs.depthGroup == 0 then
                Typography.h5

            else
                Typography.h6
    in
    Ui.el
        [ Ui.paddingEach
            { top = 16
            , bottom = 0
            , left = 16
            , right = 16
            }
        ]
        (data.grouped.names
            |> List.head
            |> Maybe.map (Localize.forLabelPlural c)
            |> Maybe.withDefault "Missing label"
            |> Fluent.verbatim
            |> toHeading
        )


viewInstancesEmpty :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Data f
    -> Element m
viewInstancesEmpty config c data =
    Ui.column
        [ Ui.width Ui.fill
        , Ui.padding 16
        , Ui.spacing 32
        ]
        [ Ui.el [ Ui.centerX ] (viewInstancesEmptyInfo c data)
        , Ui.el [ Ui.centerX ] (viewActionAddFirst config c data)
        ]
        |> Card.card
        |> Card.withType Card.Outlined
        |> Card.toElement


viewInstancesEmptyInfo : C -> Data f -> Element m
viewInstancesEmptyInfo c data =
    let
        namePlural =
            data.grouped.names
                |> List.head
                |> Maybe.map (Localize.withinSentencePlural c)
                |> Maybe.withDefault "values"
    in
    [ "There are no "
    , namePlural
    , ", yet"
    ]
        |> String.concat
        |> Fluent.verbatim
        |> Typography.body1


viewActionAddFirst : Config e f m (Model f) (Msg f m) -> C -> Data f -> Element m
viewActionAddFirst config c data =
    let
        name =
            data.grouped.names
                |> List.head
                |> Maybe.map (Localize.forLabel c)
                |> Maybe.withDefault "value"
    in
    [ "Add first "
    , name
    ]
        |> String.concat
        |> Fluent.verbatim
        |> button (config.toMsg UserPressedAddSchedule)
        |> Button.withId (Breadcrumbs.id data.path ++ "--add")
        |> Button.withFormat Button.Outlined
        |> Button.view


viewInstances :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Data f
    -> Element m
viewInstances config c report isDragging data =
    Ui.column
        [ Ui.width Ui.fill
        , Ui.spacing 16
        , onPaste config c data
        ]
        [ viewInstancesList config c report isDragging data
        , viewActionAdd config c data
        ]


onPaste : Config e f m (Model f) (Msg f m) -> C -> Data f -> Ui.Attribute m
onPaste config c data =
    let
        decoder =
            Decode.map
                (config.toMsg << UserPasted)
                (Decode.fromSubject
                    (Decode.manySafe
                        (Decoded.decoderInstanceSchedule c
                            Decoded.Relaxed
                            data.iriDocument
                            data.path
                            data.grouped
                        )
                    )
                )
    in
    Clipboard.onPasteGraph (config.toMsg Ignored) decoder


viewInstancesList :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Data f
    -> Element m
viewInstancesList config c report isDragging data =
    Ui.column
        [ Ui.width Ui.fill
        , Ui.spacing Container.configField.spacingFieldY
        ]
        (case data.date of
            Nothing ->
                data
                    |> instancesIndexedPerDay
                    |> viewScheduleDays config c report isDragging data

            Just date ->
                data
                    |> instancesIndexedPerWeek date
                    |> viewScheduleWeeks config c report isDragging data
        )


instancesIndexedPerDay : Data f -> Dict Int (List ( Int, InstanceSchedule f ))
instancesIndexedPerDay data =
    let
        groupByDay =
            Dict.groupBy (Tuple.second >> .dayOffset)
    in
    data.instances
        |> List.indexedMap Tuple.pair
        |> groupByDay


instancesIndexedPerWeek :
    Date
    -> Data f
    -> Dict Int (Dict Int (List ( Int, InstanceSchedule f )))
instancesIndexedPerWeek date data =
    let
        dayToWeek dayOffset =
            Date.weekNumber (Date.add Date.Days dayOffset date)

        groupByWeek =
            Dict.groupBy (Tuple.second >> .dayOffset >> dayToWeek)

        groupByDay =
            Dict.groupBy (Tuple.second >> .dayOffset)
    in
    data.instances
        |> List.indexedMap Tuple.pair
        |> groupByWeek
        |> Dict.map (\_ -> groupByDay)


viewActionAdd :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Data f
    -> Element m
viewActionAdd config c data =
    let
        name =
            data.grouped.names
                |> List.head
                |> Maybe.map (Localize.forLabel c)
                |> Maybe.withDefault "value"
    in
    [ "Add "
    , name
    ]
        |> String.concat
        |> Fluent.verbatim
        |> button (config.toMsg UserPressedAddSchedule)
        |> Button.withId (Breadcrumbs.id data.path ++ "--add")
        |> Button.withFormat Button.Outlined
        |> Button.view


{-| -}
viewFullscreen :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Model f
    -> Maybe (Element m)
viewFullscreen config c report isDragging (Model data) =
    data.instances
        |> List.indexedMap Tuple.pair
        |> List.filterMap
            (\( index, { instance } ) ->
                instance.form
                    |> config.fullscreen c report isDragging
                    |> Maybe.map (Ui.map (MsgForm index >> config.toMsg))
            )
        |> List.head


viewScheduleWeeks :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Data f
    -> Dict Int (Dict Int (List ( Int, InstanceSchedule f )))
    -> List (Element m)
viewScheduleWeeks config c report isDragging data weeks =
    Dict.toList weeks
        |> List.map (viewScheduleWeek config c report isDragging data)
        |> List.intercalate [ Divider.horizontal ]


viewScheduleWeek :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Data f
    -> ( Int, Dict Int (List ( Int, InstanceSchedule f )) )
    -> List (Element m)
viewScheduleWeek config c report isDragging data ( week, days ) =
    Typography.overline (Fluent.verbatim ("Week " ++ String.fromInt week))
        :: viewScheduleDays config c report isDragging data days


viewScheduleDays :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Data f
    -> Dict Int (List ( Int, InstanceSchedule f ))
    -> List (Element m)
viewScheduleDays config c report isDragging data days =
    days
        |> toDayOrOpenSlot
        |> Dict.toList
        |> List.map (viewScheduleDay config c report isDragging data)


toDayOrOpenSlot :
    Dict Int (List ( Int, InstanceSchedule f ))
    -> Dict Int (Maybe (List ( Int, InstanceSchedule f )))
toDayOrOpenSlot days =
    let
        dayOffsets =
            Dict.keys days
    in
    Dict.union
        (Dict.map (\_ -> Just) days)
        (Maybe.withDefault Dict.empty
            (Maybe.map2
                (\min max ->
                    Dict.fromList
                        (List.map
                            (\dayOffset -> ( dayOffset, Nothing ))
                            (List.range min max)
                        )
                )
                (List.minimum dayOffsets)
                (List.maximum dayOffsets)
            )
        )


viewScheduleDay :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Data f
    -> ( Int, Maybe (List ( Int, InstanceSchedule f )) )
    -> Element m
viewScheduleDay config c report isDragging data ( day, instanceSchedulesMaybe ) =
    Maybe.unwrap
        (viewScheduleOpenSlot data day)
        (viewInstanceSchedules config c report isDragging data)
        instanceSchedulesMaybe


viewScheduleOpenSlot : Data f -> Int -> Element m
viewScheduleOpenSlot data dayOffset =
    let
        dateText =
            data.date
                |> Maybe.map
                    (Date.add Date.Days dayOffset
                        >> Date.Format.shortWithWeekday Time.Format.Iso8601
                    )
                |> Maybe.withDefault ("Day " ++ String.fromInt dayOffset)
    in
    Ui.column
        [ Ui.width Ui.fill
        , Ui.spacing Container.configField.spacingFieldY
        ]
        [ Ui.text dateText
        ]
        |> Card.card
        |> Card.withType Card.Outlined
        |> Card.toElement


viewInstanceSchedules :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Report
    -> Bool
    -> Data f
    -> List ( Int, InstanceSchedule f )
    -> Element m
viewInstanceSchedules config c report isDragging data instanceSchedules =
    Instance.viewGrouped
        c
        (globalInstance data)
        (List.map
            (\( index, instanceSchedule ) ->
                ( configInstance config data index
                , index
                , localInstance config c report isDragging data index instanceSchedule
                )
            )
            instanceSchedules
        )


{-| -}
card : Config e f m (Model f) (Msg f m) -> C -> Model f -> Maybe (Container m)
card config c (Model data) =
    let
        toHeading =
            Typography.caption

        namePlural =
            data.grouped.names
                |> List.head
                |> Maybe.map (Localize.withinSentencePlural c)
                |> Maybe.withDefault "values"
    in
    [ Ui.el
        [ Ui.paddingEach
            { top = 16
            , bottom = 0
            , left = 0
            , right = 0
            }
        ]
        (data.grouped.names
            |> List.head
            |> Maybe.map (Localize.forLabelPlural c)
            |> Maybe.withDefault "Missing label"
            |> Fluent.verbatim
            |> toHeading
        )
    , if List.length data.instances == 0 then
        Ui.el
            [ Ui.width Ui.fill
            , Ui.padding 16
            , Ui.spacing 32
            ]
            (Ui.el
                [ Ui.centerX ]
                ([ "There are no "
                 , namePlural
                 , "."
                 ]
                    |> String.concat
                    |> Fluent.verbatim
                    |> Typography.body1
                )
            )
            |> Card.card
            |> Card.withType Card.Outlined
            |> Card.toElement

      else
        case data.grouped.viewer of
            ValueCardViewer ->
                Ui.column
                    [ Ui.width Ui.fill
                    , Ui.spacing Container.configCard.spacingFieldY
                    ]
                    (List.indexedMap (viewCardScheduleFields config c)
                        data.instances
                    )

            ValueTableViewer ->
                { data = List.map (.instance >> .form) data.instances
                , columns =
                    data.grouped.field
                        |> config.columns c []
                }
                    |> table
                    |> Table.view c
    ]
        |> Ui.column
            [ Ui.width Ui.fill
            , Ui.spacing 4
            ]
        |> Container.expand
        |> Just


viewCardScheduleFields :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Int
    -> InstanceSchedule f
    -> Element m
viewCardScheduleFields config c index instanceSchedule =
    instanceSchedule.instance.form
        |> config.card c
        |> Maybe.map (Ui.map (MsgForm index >> config.toMsg))
        |> Maybe.withDefault Ui.none
        |> Card.card
        |> Card.withType Card.Outlined
        |> Card.toElement


{-| -}
columns :
    Config e f m (Model f) (Msg f m)
    -> C
    -> List String
    -> Grouped.DataSchedule
    -> List (Table.Column f m)
columns _ _ _ _ =
    []


{-| -}
help : Config e f m (Model f) (Msg f m) -> C -> Report -> Model f -> List Fluent
help _ c report (Model data) =
    report.results
        |> List.filter
            (Report.validationResultFor
                (Report.emptySelector
                    |> Maybe.apply Report.selectFocusNode (Rdf.toIri data.nodeFocus)
                    |> Report.selectPath data.grouped.path
                )
            )
        |> List.filterMap (helpFromValidationResult c data)


helpFromValidationResult : C -> Data f -> ValidationResult -> Maybe Fluent
helpFromValidationResult c data (ValidationResult { constraintComponent, message }) =
    let
        name =
            data.grouped.names
                |> List.head
                |> Maybe.map (Localize.withinSentenceWithArticleIndefinite c)
                |> Maybe.withDefault "a value"
    in
    case constraintComponent of
        MinCountConstraintComponent ->
            Just (Fluent.verbatim ("Enter " ++ name))

        _ ->
            Just (Fluent.verbatim (Localize.verbatim c message ++ "."))


helpInstance :
    C
    -> Report
    -> BlankNodeOrIri
    -> Data form
    -> BlankNodeOrIri
    -> List Fluent
helpInstance c report nodeFocus data nodeInstance =
    report.results
        |> List.filter
            (\((ValidationResult _) as validationResult) ->
                Report.validationResultFor
                    (Report.emptySelector
                        |> Maybe.apply Report.selectFocusNode (Rdf.toIri nodeFocus)
                        |> Report.selectPath data.grouped.path
                        |> Maybe.apply Report.selectValue
                            (Rdf.toAnyLiteralOrIri nodeInstance)
                    )
                    validationResult
            )
        |> List.filterMap (helpInstanceFromValidationResult c data)


helpInstanceFromValidationResult :
    C
    -> Data form
    -> ValidationResult
    -> Maybe Fluent
helpInstanceFromValidationResult c data (ValidationResult { constraintComponent, message }) =
    let
        name =
            data.grouped.names
                |> List.head
                |> Maybe.map (Localize.withinSentence c)
                |> Maybe.withDefault "value"
    in
    case constraintComponent of
        NodeConstraintComponent ->
            Just (Fluent.verbatim ("There is a problem with this " ++ name ++ "."))

        _ ->
            Just (Fluent.verbatim (Localize.verbatim c message ++ "."))


{-| -}
summary : Config e f m (Model f) (Msg f m) -> C -> Model f -> List Fluent
summary config c (Model data) =
    case data.instances of
        [] ->
            []

        [ { instance } ] ->
            config.summary c instance.form

        _ ->
            case List.head data.grouped.names of
                Nothing ->
                    []

                Just name ->
                    [ Fluent.verbatim
                        (String.concat
                            [ String.fromInt (List.length data.instances)
                            , " "
                            , Localize.withinSentencePlural c name
                            ]
                        )
                    ]



-- OUTPUT


{-| -}
encoders :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Model f
    -> List Encode.PropertyEncoder
encoders config c (Model data) =
    data.instances
        |> List.indexedMap Tuple.pair
        |> List.map (encoderInstance config c data)


encoderInstance :
    Config e f m (Model f) (Msg f m)
    -> C
    -> Data f
    -> ( Int, InstanceSchedule f )
    -> PropertyEncoder
encoderInstance config c data ( index, instanceSchedule ) =
    Encode.property data.grouped.path
        (Encode.node (config.nodeFocus instanceSchedule.instance.form)
            (List.concat
                [ encodersClasses data
                , encodersIsDefinedBy config data instanceSchedule.instance
                , encodersOrderPath data index
                , encodersDayOffset data instanceSchedule
                , config.encoders c instanceSchedule.instance.form
                ]
            )
        )


encodersClasses : Data f -> List PropertyEncoder
encodersClasses data =
    List.map
        (Encode.predicate a << Encode.object)
        (NonEmpty.toList data.grouped.classes)


encodersIsDefinedBy :
    Config e f m (Model f) (Msg f m)
    -> Data f
    -> Instance f
    -> List PropertyEncoder
encodersIsDefinedBy config data instance =
    let
        iriDocumentWithFragment =
            Rdf.setFragment (Breadcrumbs.id (config.path instance.form))
                data.iriDocument
    in
    [ Encode.predicate RDFS.isDefinedBy
        (Encode.object iriDocumentWithFragment)
    ]


encodersOrderPath : Data f -> Int -> List PropertyEncoder
encodersOrderPath data index =
    case data.grouped.editorOrderPath of
        PredicatePath hashDetailsEditorOrderPath ->
            [ Encode.predicate hashDetailsEditorOrderPath
                (Encode.literal (Rdf.int index))
            ]

        _ ->
            []


encodersDayOffset : Data f -> InstanceSchedule f -> List PropertyEncoder
encodersDayOffset data instanceSchedule =
    [ Encode.property data.grouped.editorDayOffsetPath
        (Encode.literal (Rdf.int instanceSchedule.dayOffset))
    ]


infinity : Int
infinity =
    ceiling (1 / 0)



-- INFO


{-| -}
toNodeFocus : Model f -> BlankNodeOrIri
toNodeFocus (Model data) =
    data.nodeFocus


{-| -}
toPath : Model f -> Path
toPath (Model data) =
    data.path


{-| -}
isEmpty : Config e f m (Model f) (Msg f m) -> Model f -> Bool
isEmpty _ _ =
    False


{-| -}
dragging : Config e f m (Model f) (Msg f m) -> Model f -> Bool
dragging config (Model data) =
    List.any (.instance >> .form >> config.dragging) data.instances


{-| -}
log : Config e f m (Model f) (Msg f m) -> Model f -> Pretty.Doc ()
log config (Model data) =
    Pretty.lines
        [ Pretty.string "Schedule"
        , Pretty.indent 2 (Pretty.lines (List.map (.instance >> .form >> config.log) data.instances))
        ]



-- SUPPORT


{-| `InstanceSchedule`s are sorted by day offsets, and within groups of equal
days, by order. `order` can be assumed be in the range `[0..]`.

This function is stable, ie. preserving the relative order of the input list on
elements with the same sort-key.

-}
sortAndNormalizeInstanceSchedules :
    List (InstanceSchedule f)
    -> List (InstanceSchedule f)
sortAndNormalizeInstanceSchedules instanceSchedules =
    let
        sortKey instanceSchedule =
            [ instanceSchedule.dayOffset, instanceSchedule.order ]

        on get eq a b =
            eq (get a) (get b)
    in
    instanceSchedules
        |> List.sortBy sortKey
        |> (List.groupWhile (on .dayOffset (==))
                >> List.map (uncurry (::))
           )
        |> List.map
            (List.indexedMap
                (\orderNormalized instanceSchedule ->
                    { instanceSchedule | order = orderNormalized }
                )
            )
        |> List.concat


addSchedule : Breadcrumbs -> f -> Data f -> Data f
addSchedule breadcrumbsNested form data =
    let
        dayOffsetMaximum =
            Maybe.withDefault 0 (List.maximum (List.map .dayOffset data.instances))
    in
    { data
        | instances =
            data.instances
                ++ [ { dayOffset = 1 + dayOffsetMaximum
                     , order = 0
                     , instance =
                        { breadcrumbs = breadcrumbsNested
                        , expanded = True
                        , form = form
                        , label = Nothing
                        }
                     }
                   ]
    }


moveUpwardSchedule : Int -> Data f -> ( Int, Data f )
moveUpwardSchedule index data =
    case List.getAt index data.instances of
        Nothing ->
            ( index, data )

        Just instanceSchedule ->
            let
                ( indexNew, instanceScheduleUpdated ) =
                    if instanceSchedule.order <= 0 then
                        ( index
                        , { instanceSchedule
                            | dayOffset = instanceSchedule.dayOffset - 1
                            , order = infinity
                          }
                        )

                    else
                        ( index - 1
                        , { instanceSchedule
                            | order = instanceSchedule.order - 2
                          }
                        )
            in
            ( indexNew
            , { data
                | instances =
                    data.instances
                        |> List.setAt index instanceScheduleUpdated
                        |> sortAndNormalizeInstanceSchedules
              }
            )


moveDownwardSchedule : Int -> Data f -> ( Int, Data f )
moveDownwardSchedule index data =
    case List.getAt index data.instances of
        Nothing ->
            ( index, data )

        Just instanceSchedule ->
            let
                ( indexNew, instanceScheduleUpdated ) =
                    let
                        scheduleInstanceIsLastInDayGroup =
                            Maybe.unwrap True
                                (\scheduleInstanceNext ->
                                    scheduleInstanceNext.dayOffset
                                        /= instanceSchedule.dayOffset
                                )
                                (List.getAt (index + 1) data.instances)
                    in
                    if scheduleInstanceIsLastInDayGroup then
                        ( index
                        , { instanceSchedule
                            | dayOffset = instanceSchedule.dayOffset + 1
                            , order = 0
                          }
                        )

                    else
                        ( index + 1
                        , { instanceSchedule
                            | order = instanceSchedule.order + 2
                          }
                        )
            in
            ( indexNew
            , { data
                | instances =
                    data.instances
                        |> List.setAt index instanceScheduleUpdated
                        |> sortAndNormalizeInstanceSchedules
              }
            )



-- SUPPORT


indexMaxFromInstances :
    Config e f m (Model f) (Msg f m)
    -> List (InstanceSchedule f)
    -> Int
indexMaxFromInstances config instances =
    instances
        |> List.filterMap (.instance >> indexFromInstance config)
        |> List.maximum
        |> Maybe.withDefault 0


indexFromInstance : Config e f m (Model f) (Msg f m) -> Instance f -> Maybe Int
indexFromInstance config instance =
    instance.form
        |> config.path
        |> Breadcrumbs.lastOrdinal
