module Shacl.Form.Viewed.Form exposing
    ( Model
    , init, initWith
    , Msg, update, expandAll
    , subscriptions
    , view, viewFullscreen, card, columns, help, summary
    , encoders
    , toNodeFocus, toPath, isEmpty, dragging, log
    )

{-|


# Model

@docs Model


# Init

@docs init, initWith


# Update

@docs Msg, update, expandAll


# Subscriptions

@docs subscriptions


# View

@docs view, viewFullscreen, card, columns, help, summary


# Output

@docs encoders


# Info

@docs toNodeFocus, toPath, isEmpty, dragging, log

-}

import Element
    exposing
        ( Element
        , alignTop
        , column
        , el
        , fill
        , height
        , none
        , padding
        , paddingXY
        , paragraph
        , row
        , spacing
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Extra exposing (userSelect)
import Element.Font as Font
import Element.Lazy exposing (lazy)
import Fluent exposing (Fluent, verbatim)
import List.Extra as List
import Maybe.Apply as Maybe
import Pretty
import Rdf exposing (BlankNodeOrIri)
import Rdf.Encode as Encode
import Rdf.Graph exposing (Seed)
import Shacl.Form.Breadcrumbs exposing (Breadcrumbs, Path)
import Shacl.Form.Context exposing (C)
import Shacl.Form.Decoded as Decoded
import Shacl.Form.Effect as Effect
import Shacl.Form.Grouped as Grouped
import Shacl.Form.Localize as Localize
import Shacl.Form.Shared exposing (Config, Shared)
import Shacl.Form.Translated as Translated
import Shacl.Form.Ui.Container as Container exposing (Container)
import Shacl.Form.Updated as Updated exposing (Updated)
import Shacl.Report as Report exposing (Report, ValidationResult(..))
import Ui.Atom.Icon as Icon
import Ui.Molecule.Table as Table
import Ui.Molecule.TableCell as TableCell
import Ui.Theme.Color as Color exposing (error)
import Ui.Theme.Typography exposing (body1, body1Strong, h5)



-- MODEL


{-| -}
type Model f
    = Model (Data f)


type alias Data f =
    { path : Path
    , breadcrumbs : Breadcrumbs
    , grouped : Grouped.DataForm
    , nodeFocus : BlankNodeOrIri
    , fields : List f
    , failures : List Translated.Failure
    }



-- INIT


{-| -}
init :
    Config e f m (Model f) (Msg m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Breadcrumbs
    -> Decoded.DataForm
    -> Updated (Model f) m
init config populateDefaultValues c shared seed breadcrumbs decoded =
    let
        initHelp ( index, field ) updated =
            let
                breadcrumbsNested =
                    breadcrumbs

                updatedSuccess =
                    config.init populateDefaultValues c shared updated.seed breadcrumbsNested field
            in
            { model = updatedSuccess.model :: updated.model
            , effect =
                Effect.batch
                    [ Effect.map (MsgForm index >> config.toMsg) updatedSuccess.effect
                    , updated.effect
                    ]
            , seed = updatedSuccess.seed
            , needsPersisting = updatedSuccess.needsPersisting || updated.needsPersisting
            }
    in
    decoded.fields
        |> List.indexedMap Tuple.pair
        |> List.foldl initHelp
            { model = []
            , effect = Effect.none
            , seed = seed
            , needsPersisting = False
            }
        |> Updated.map
            (\successes ->
                Model
                    { path = decoded.path
                    , breadcrumbs = breadcrumbs
                    , nodeFocus = decoded.nodeFocus
                    , grouped = decoded.grouped
                    , fields = List.reverse successes
                    , failures = decoded.grouped.failures
                    }
            )


{-| -}
initWith :
    Config e f m (Model f) (Msg m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Model f
    -> Result e (Maybe (Updated (Model f) m))
initWith _ _ _ _ _ _ =
    -- FIXME
    -- let
    --     initHelp ( index, success ) updatedResult =
    --         case updatedResult of
    --             Err _ ->
    --                 updatedResult
    --             Ok updated ->
    --                 case
    --                     initSuccessWith config
    --                         populateDefaultValues
    --                         c
    --                         shared
    --                         updated.seed
    --                         index
    --                         success
    --                 of
    --                     Err error ->
    --                         Err error
    --                     Ok Nothing ->
    --                         Ok { updated | model = ( success, Nothing ) :: updated.model }
    --                     Ok (Just updatedSuccess) ->
    --                         Ok
    --                             { model = ( success, Just updatedSuccess.model ) :: updated.model
    --                             , effect = Effect.batch [ updatedSuccess.effect, updated.effect ]
    --                             , seed = updatedSuccess.seed
    --                             , needsPersisting = updatedSuccess.needsPersisting || updated.needsPersisting
    --                             }
    -- in
    -- data.successes
    --     |> List.indexedMap Tuple.pair
    --     |> List.foldl initHelp
    --         (Ok
    --             { model = []
    --             , effect = Effect.none
    --             , seed = seed
    --             , needsPersisting = False
    --             }
    --         )
    --     |> Result.map
    --         (\updated ->
    --             if List.all (Tuple.second >> Maybe.isNothing) updated.model then
    --                 Nothing
    --             else
    --                 { model =
    --                     Model
    --                         { data
    --                             | successes =
    --                                 updated.model
    --                                     |> List.map
    --                                         (\( success, maybeSuccess ) ->
    --                                             Maybe.withDefault success maybeSuccess
    --                                         )
    --                                     |> List.reverse
    --                         }
    --                 , effect = updated.effect
    --                 , seed = updated.seed
    --                 , needsPersisting = updated.needsPersisting
    --                 }
    --                     |> Just
    --         )
    Ok Nothing



-- UPDATE


{-| -}
type Msg msg
    = MsgForm Int msg


{-| -}
update :
    Config e f m (Model f) (Msg m)
    -> { populateDefaultValues : Bool }
    -> C
    -> Shared
    -> Seed
    -> Msg m
    -> Model f
    -> Updated (Model f) m
update config _ c shared seed msg ((Model data) as model) =
    case msg of
        MsgForm index msgForm ->
            case List.getAt index data.fields of
                Nothing ->
                    Updated.from seed model

                Just field ->
                    field
                        |> config.update c shared seed msgForm
                        |> Updated.map
                            (\fieldUpdated ->
                                Model
                                    { data
                                        | fields =
                                            List.setAt index fieldUpdated data.fields
                                    }
                            )
                        |> Updated.mapEffect (MsgForm index >> config.toMsg)


{-| -}
expandAll : Config e f m (Model f) (Msg m) -> Model f -> Model f
expandAll config (Model data) =
    Model { data | fields = List.map config.expandAll data.fields }



-- SUBSCRIPTIONS


{-| -}
subscriptions : Config e f m (Model f) (Msg m) -> Model f -> Sub m
subscriptions config (Model data) =
    data.fields
        |> List.map config.subscriptions
        |> Sub.batch



-- VIEW


{-| -}
view :
    Config e f m (Model f) (Msg m)
    -> C
    -> Report
    -> Bool
    -> Model f
    -> Maybe (Container m)
view config c report isDragging (Model data) =
    let
        addHelp =
            -- FIXME We have to decide if we render help here.  Probably we
            -- want to delegate this to the container inputs.
            if False then
                -- errorWrapper (help c report breadcrumbs.nodeFocus)
                identity

            else
                identity

        successes =
            data.fields
                |> List.indexedMap
                    (\index field ->
                        field
                            |> config.view c report isDragging
                            |> Maybe.map (Element.map (MsgForm index >> config.toMsg))
                    )
                |> List.filterMap identity
                -- FIXME We want to already have Containers here
                |> List.map Container.expand

        failures =
            List.map viewFailure data.failures
    in
    if List.isEmpty successes && List.isEmpty failures then
        Nothing

    else
        [ Container.wrap Container.configField successes
        , if List.isEmpty failures then
            none

          else
            failures
                |> column
                    [ width fill
                    , spacing Container.configField.spacingFieldY
                    ]
        ]
            |> column
                [ width fill
                , spacing Container.configField.spacingFieldY
                , userSelect (not isDragging)
                ]
            |> addHelp
            |> Container.expand
            |> Just


viewFailure : Translated.Failure -> Element msg
viewFailure =
    lazy
        (\failure ->
            column
                [ width fill
                , Border.rounded 12
                , Border.width 6
                , Border.color Color.error
                ]
                [ el
                    [ width fill
                    , Background.color error
                    , Font.color Color.onError
                    ]
                    (row
                        [ width fill
                        , height fill
                        , spacing 16
                        , paddingXY 14 8
                        ]
                        [ el
                            [ alignTop ]
                            (Icon.viewLarge Icon.Error)
                        , el
                            [ width fill ]
                            (h5 (verbatim "Form generation error"))
                        ]
                    )
                , column
                    [ width fill
                    , padding 16
                    , spacing 16
                    ]
                    [ paragraph [ width fill ]
                        [ body1 (verbatim "I could not generate a form element from the following property shapes:") ]

                    -- FIXME Render translate errors outside of Viewed components
                    -- , viewCodeBlock (propertyShapesToCode shapes failure.propertyShapes)
                    , paragraph [ width fill ]
                        [ body1 (verbatim "I tried the following kinds:")
                        ]
                    , failure.errors
                        |> List.map viewError
                        |> column
                            [ width fill
                            , spacing 8
                            ]
                    ]
                ]
        )


viewError : ( Translated.TagField, Translated.Error ) -> Element msg
viewError ( tag, error ) =
    [ [ tagToString tag
      , ": "
      ]
        |> String.concat
        |> verbatim
        |> body1Strong
    , error
        |> Translated.errorToString
        |> verbatim
        |> body1
    ]
        |> paragraph
            [ width fill
            ]


tagToString : Translated.TagField -> String
tagToString tag =
    case tag of
        Translated.TagFieldText ->
            "Text"

        Translated.TagFieldCheckbox ->
            "Checkbox"

        Translated.TagFieldCheckboxInstance ->
            "Checkbox Instance"

        Translated.TagFieldConstant ->
            "Constant"

        Translated.TagFieldConstantInjectable ->
            "Constant Injectable"

        Translated.TagFieldDateTime ->
            "Date (+ time)"

        Translated.TagFieldNumber ->
            "Number"

        Translated.TagFieldSelectOne ->
            "Select One"

        Translated.TagFieldSelectOneInjectable ->
            "Select One Injectable"

        Translated.TagFieldSelectMany ->
            "Select Many"

        Translated.TagFieldFileUpload ->
            "File upload"

        Translated.TagFieldIriReadOnly ->
            "Read-only IRI"

        Translated.TagFieldCollection ->
            "List"

        Translated.TagFieldCollectionSelect ->
            "Select Many with Nesting"

        Translated.TagFieldSingleNested ->
            "Singly nested component"

        Translated.TagFieldSchedule ->
            "Schedule"

        Translated.TagFieldGrid ->
            "Grid"

        Translated.TagFieldVariants ->
            "Variants"


{-| -}
viewFullscreen :
    Config e f m (Model f) (Msg m)
    -> C
    -> Report
    -> Bool
    -> Model f
    -> Maybe (Element m)
viewFullscreen config c report isDragging (Model data) =
    data.fields
        |> List.indexedMap
            (\index field ->
                field
                    |> config.fullscreen c report isDragging
                    |> Maybe.map (Element.map (MsgForm index >> config.toMsg))
            )
        |> List.filterMap identity
        |> List.head


{-| -}
card : Config e f m (Model f) (Msg m) -> C -> Model f -> Maybe (Container m)
card config c (Model data) =
    case
        data.fields
            |> List.indexedMap
                (\index field ->
                    field
                        |> config.card c
                        |> Maybe.map (Element.map (MsgForm index >> config.toMsg))
                )
            |> List.filterMap identity
            -- FIXME We want to already have Containers here
            |> List.map Container.expand
    of
        [] ->
            Nothing

        containers ->
            Container.wrap Container.configCard containers
                |> Container.expand
                |> Just


{-| -}
columns :
    Config e f m (Model f) (Msg m)
    -> C
    -> List String
    -> Grouped.DataForm
    -> List (Table.Column f m)
columns config c scope grouped =
    grouped.successes
        |> List.indexedMap Tuple.pair
        |> List.concatMap
            (\( index, success ) ->
                success.field
                    |> config.columns c scope
                    |> List.map
                        (\column ->
                            { header = column.header
                            , width = column.width
                            , view =
                                config.toField
                                    >> Maybe.andThen (\(Model data) -> List.getAt index data.fields)
                                    >> Maybe.map column.view
                                    >> Maybe.withDefault TableCell.empty
                            }
                        )
            )


{-| -}
help : Config e f m (Model f) (Msg m) -> C -> Report -> Model f -> List Fluent
help config c report (Model data) =
    List.concat
        [ report.results
            |> Report.validationResultsFor
                (Report.emptySelector
                    |> Maybe.apply Report.selectFocusNode (Rdf.toIri data.nodeFocus)
                )
            |> List.map (helpFromValidationResult c >> Fluent.verbatim)
        , List.concatMap (config.help c report) data.fields
        ]


helpFromValidationResult : C -> ValidationResult -> String
helpFromValidationResult c (ValidationResult { message }) =
    Localize.verbatim c message


{-| -}
summary : Config e f m (Model f) (Msg m) -> C -> Model f -> List Fluent
summary config c (Model data) =
    List.concatMap (config.summary c) data.fields



-- INFO


{-| -}
toNodeFocus : Model f -> BlankNodeOrIri
toNodeFocus (Model data) =
    data.nodeFocus


{-| -}
toPath : Model f -> Path
toPath (Model data) =
    data.path


{-| -}
isEmpty : Config e f m (Model f) (Msg m) -> Model f -> Bool
isEmpty config (Model data) =
    List.all config.isEmpty data.fields


{-| -}
dragging : Config e f m (Model f) (Msg m) -> Model f -> Bool
dragging config (Model data) =
    List.any config.dragging data.fields


{-| -}
log : Config e f m (Model f) (Msg m) -> Model f -> Pretty.Doc ()
log config (Model data) =
    Pretty.lines
        [ Pretty.string "Collection"
        , Pretty.indent 2 (Pretty.lines (List.map config.log data.fields))
        ]



-- OUTPUT


{-| -}
encoders :
    Config e f m (Model f) (Msg m)
    -> C
    -> Model f
    -> List Encode.PropertyEncoder
encoders config c (Model data) =
    List.concatMap (config.encoders c) data.fields
