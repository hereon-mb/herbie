module Shacl.Documentation exposing
    ( Documentation, Info, fromInfo
    , withShacl
    , expectUi, expectUiWithDefaultPopulation, expectGraph, testOnly, testInfoForm
    , tests
    , toInfo, toForm, InfoForm, Error(..)
    )

{-|

@docs Documentation, Info, fromInfo
@docs withShacl
@docs expectUi, expectUiWithDefaultPopulation, expectGraph, testOnly, testInfoForm
@docs tests
@docs toInfo, toForm, InfoForm, Error

-}

import Context exposing (C)
import Effect exposing (Effect)
import Expect exposing (Expectation)
import Expect.Shacl.Form.Viewed as Viewed
import Random
import Rdf exposing (Iri)
import Rdf.Decode as Decode
import Rdf.Graph as Graph exposing (Graph)
import Result.Extra as Result
import Result.Pipeline as Result
import Shacl.Form.Loader as Loader
import Shacl.Form.Viewed as Viewed
import String.Extra as String
import Test exposing (Test)
import Test.Html.Query as Query


{-| -}
type Documentation
    = Documentation Data


type alias Data =
    { headingFull : String
    , headingShort : String
    , description : String
    , documentShacl : String
    , documentData : String
    , expectationsUi : List (Query.Single Viewed.Msg -> Expectation)
    , expectationsUiWithDefaultPopulation : List (Query.Single Viewed.Msg -> Expectation)
    , expectationsGraph : List (Graph -> Result Decode.Error Expectation)
    , testOnly : Bool
    , testInfoForm : Bool
    }


{-| -}
type alias Info =
    { headingFull : String
    , headingShort : String
    , description : String
    }


{-| -}
fromInfo : Info -> Documentation
fromInfo info =
    Documentation
        { headingFull = info.headingFull
        , headingShort = info.headingShort
        , description = info.description
        , documentShacl = ""
        , documentData = ""
        , expectationsUi = []
        , expectationsUiWithDefaultPopulation = []
        , expectationsGraph = []
        , testOnly = False
        , testInfoForm = False
        }


{-| -}
withShacl : { documentShacl : String, documentData : String } -> Documentation -> Documentation
withShacl { documentShacl, documentData } (Documentation data) =
    Documentation
        { data
            | documentShacl =
                documentShacl
                    |> String.unindent
                    |> String.trim
                    |> toShape
                    |> String.unindent
                    |> String.trim
            , documentData =
                documentData
                    |> String.unindent
                    |> String.trim
                    |> toData
                    |> String.unindent
                    |> String.trim
        }


{-| -}
expectUi : (Query.Single Viewed.Msg -> Expectation) -> Documentation -> Documentation
expectUi expectation (Documentation data) =
    Documentation { data | expectationsUi = expectation :: data.expectationsUi }


{-| -}
expectUiWithDefaultPopulation : (Query.Single Viewed.Msg -> Expectation) -> Documentation -> Documentation
expectUiWithDefaultPopulation expectation (Documentation data) =
    Documentation { data | expectationsUiWithDefaultPopulation = expectation :: data.expectationsUiWithDefaultPopulation }


{-| -}
expectGraph : (Graph -> Result Decode.Error Expectation) -> Documentation -> Documentation
expectGraph expectation (Documentation data) =
    Documentation { data | expectationsGraph = expectation :: data.expectationsGraph }


{-| -}
testOnly : Documentation -> Documentation
testOnly (Documentation data) =
    Documentation { data | testOnly = True }


{-| -}
testInfoForm : Documentation -> Documentation
testInfoForm (Documentation data) =
    Documentation { data | testInfoForm = True }


{-| -}
toInfo : Documentation -> Info
toInfo (Documentation data) =
    { headingFull = data.headingFull
    , headingShort = data.headingShort
    , description = data.description
    }


type Error
    = ErrorGraphShape String Graph.Error
    | ErrorGraphData String Graph.Error
    | ErrorForm Viewed.Error


type alias InfoForm =
    { documentShacl : String
    , documentData : String
    , form : Viewed.Form
    }


{-| -}
toForm : C -> Documentation -> Maybe (Result Error ( InfoForm, Effect Viewed.Msg ))
toForm c (Documentation data) =
    if data.documentShacl /= "" && data.documentData /= "" then
        case
            Ok
                (\graphShape graphData ->
                    { iri = iriDocument
                    , preview = True
                    , strict = False
                    , workspaceUuid = "workspace"
                    , graph = graphData
                    , graphEntered = graphData
                    , graphPersisted = graphData
                    , graphGenerated = Graph.emptyGraph
                    , graphSupport = Graph.emptyGraph
                    , graphOntology = graphShape
                    , populateDefaultValues = { populateDefaultValues = True }
                    , seed =
                        -- FIXME Hack to force a different seed for the blank
                        -- node generation within the form
                        Random.initialSeed 42
                            |> Random.step Graph.seedGenerator
                            |> Tuple.first
                    }
                        |> Viewed.initStatic c.cShacl
                        |> Result.mapError ErrorForm
                        |> Result.map (\( f, effect, _ ) -> ( f, effect ))
                )
                |> Result.required (Result.mapError (ErrorGraphShape data.documentShacl) (Graph.parse data.documentShacl))
                |> Result.required (Result.mapError (ErrorGraphData data.documentData) (Graph.parse data.documentData))
                |> Result.join
        of
            Err error ->
                Just (Err error)

            Ok ( form, effect ) ->
                Just
                    (Ok
                        ( { documentShacl = data.documentShacl
                          , documentData = data.documentData
                          , form = form
                          }
                        , Loader.effectToEffect c "workspace" effect
                        )
                    )

    else
        Nothing


toShape : String -> String
toShape documentShacl =
    [ "@base <" ++ Rdf.toUrl iriShapes ++ "> ."
    , prefixes
        |> String.unindent
        |> String.trim
    , ""
    , documentShacl
    ]
        |> String.join "\n"


toData : String -> String
toData documentData =
    [ "@base <" ++ Rdf.toUrl iriDocument ++ "> ."
    , prefixes
        |> String.unindent
        |> String.trim
    , "<> a owl:Ontology ."
    , "<> dcterms:conformsTo <" ++ Rdf.toUrl iriShapes ++ "> ."
    , documentData
    ]
        |> String.join "\n"


prefixes : String
prefixes =
    """
        @prefix dash: <http://datashapes.org/dash#> .
        @prefix dcterms: <http://purl.org/dc/terms/> .
        @prefix hash: <http://purls.helmholtz-metadaten.de/herbie/hash/#> .
        @prefix owl: <http://www.w3.org/2002/07/owl#> .
        @prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
        @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
        @prefix sh: <http://www.w3.org/ns/shacl#> .
        @prefix showcase: <""" ++ Rdf.toUrl (showcase "") ++ """> .
        @prefix xsd: <""" ++ Rdf.toUrl (xsd "") ++ """> .
    """


iriShapes : Iri
iriShapes =
    Rdf.iri "http://example.org/shapes/"


iriDocument : Iri
iriDocument =
    document ""


showcase : String -> Iri
showcase name =
    Rdf.iri ("http://purls.helmholtz-metadaten.de/herbie/showcase/ont/#" ++ name)


document : String -> Iri
document name =
    Rdf.iri ("http://example.org/document/" ++ name)


xsd : String -> Iri
xsd name =
    Rdf.iri ("http://www.w3.org/2001/XMLSchema#" ++ name)


{-| -}
tests : Documentation -> List Test
tests (Documentation data) =
    let
        applyTestOnly =
            if data.testOnly then
                Test.only

            else
                identity

        testUi _ =
            Viewed.expectAll data.testInfoForm
                data.expectationsUi
                { shape = data.documentShacl
                , data = data.documentData
                }

        testUiWithDefaultPopulation _ =
            Viewed.expectAllWithDefaultPopulation data.testInfoForm
                data.expectationsUiWithDefaultPopulation
                { shape = data.documentShacl
                , data = data.documentData
                }

        testGraphAllWithDefaultPopulation _ =
            Viewed.expectGraphAllWithDefaultPopulation data.expectationsGraph
                { shape = data.documentShacl
                , data = data.documentData
                }
    in
    [ if List.isEmpty data.expectationsUi then
        Nothing

      else
        testUi
            |> Test.test "UI"
            |> applyTestOnly
            |> Just
    , if List.isEmpty data.expectationsUiWithDefaultPopulation then
        Nothing

      else
        testUiWithDefaultPopulation
            |> Test.test "UI"
            |> applyTestOnly
            |> Just
    , if List.isEmpty data.expectationsGraph then
        Nothing

      else
        testGraphAllWithDefaultPopulation
            |> Test.test "Graph"
            |> applyTestOnly
            |> Just
    ]
        |> List.filterMap identity
