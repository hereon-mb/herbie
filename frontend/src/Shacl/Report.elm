{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Shacl.Report exposing
    ( Report, ValidationResult(..), DataValidationResult, ConstraintComponent(..), Severity
    , empty, fromGraph
    , Selector
    , emptySelector
    , selectFocusNode
    , selectPath
    , selectSourceShape
    , selectValue
    , validationResultFor
    , validationResultsFor
    , detailsFor
    )

{-|

@docs Report, ValidationResult, DataValidationResult, ConstraintComponent, Severity


# Create

@docs empty, fromGraph


# Selector

@docs Selector
@docs emptySelector

@docs selectFocusNode
@docs selectPath
@docs selectSourceShape
@docs selectValue


# Select

@docs validationResultFor
@docs validationResultsFor


# Filter

@docs detailsFor

-}

import Maybe.Extra as Maybe
import Rdf exposing (AnyLiteral, BlankNodeOrIri, BlankNodeOrIriOrAnyLiteral, Iri, StringOrLangString)
import Rdf.Decode as Decode exposing (Decoder)
import Rdf.Decode.Extra as Decode
import Rdf.Graph exposing (Graph)
import Rdf.Namespaces exposing (sh)
import Rdf.PropertyPath exposing (PropertyPath)
import Shacl.PropertyPath as PropertyPath


type alias Report =
    { conforms : Bool
    , results : List ValidationResult
    }


type ValidationResult
    = ValidationResult DataValidationResult


type alias DataValidationResult =
    { focusNode : BlankNodeOrIri
    , path : Maybe PropertyPath
    , value : Maybe BlankNodeOrIriOrAnyLiteral
    , constraintComponent : ConstraintComponent
    , message : StringOrLangString
    , detail : List ValidationResult
    , severity : Severity
    , sourceShape : Maybe Iri
    }


type ConstraintComponent
    = -- VALUE TYPE
      ClassConstraintComponent
    | DatatypeConstraintComponent
    | NodeKindConstraintComponent
      -- CARDINALITY
    | MinCountConstraintComponent
    | MaxCountConstraintComponent
    | QualifiedMinCountConstraintComponent
    | QualifiedMaxCountConstraintComponent
      -- VALUE RANGE
    | MinExclusiveConstraintComponent
    | MinInclusiveConstraintComponent
    | MaxExclusiveConstraintComponent
    | MaxInclusiveConstraintComponent
      -- STRING BASED
    | MinLengthConstraintComponent
    | MaxLengthConstraintComponent
    | PatternLengthConstraintComponent
    | LanguageInConstraintComponent
    | UniqueLangConstraintComponent
      -- PROPERTY PAIR
    | EqualsConstraintComponent
    | DisjointConstraintComponent
    | LessThanConstraintComponent
    | LessThanOrEqualsConstraintComponent
      -- LOGICAL
    | NotConstrantComponent
    | AndConstrantComponent
    | OrConstrantComponent
    | XoneConstrantComponent
      -- SHAPE BASED
    | NodeConstraintComponent
    | PropertyConstraintComponent
      -- OTHER
    | HasValueConstraintComponent
    | InConstraintComponent
    | SparqlConstraintComponent


type Severity
    = Info
    | Warning
    | Violation


type alias Selector =
    { focusNode : Maybe Iri
    , path : Maybe PropertyPath
    , sourceShape : Maybe Iri
    , value : Maybe AnyLiteral
    }


emptySelector : Selector
emptySelector =
    { focusNode = Nothing
    , path = Nothing
    , sourceShape = Nothing
    , value = Nothing
    }


selectFocusNode : Iri -> Selector -> Selector
selectFocusNode iri selector =
    { selector | focusNode = Just iri }


selectPath : PropertyPath -> Selector -> Selector
selectPath path selector =
    { selector | path = Just path }


selectSourceShape : Iri -> Selector -> Selector
selectSourceShape iri selector =
    { selector | sourceShape = Just iri }


selectValue : AnyLiteral -> Selector -> Selector
selectValue anyLiteral selector =
    { selector | value = Just anyLiteral }


validationResultFor : Selector -> ValidationResult -> Bool
validationResultFor selector (ValidationResult dataValidationResult) =
    (Maybe.isNothing selector.focusNode
        || (selector.focusNode == Rdf.toIri dataValidationResult.focusNode)
    )
        && (Maybe.isNothing selector.path
                || (selector.path == dataValidationResult.path)
           )
        && (Maybe.isNothing selector.sourceShape
                || (selector.sourceShape == dataValidationResult.sourceShape)
           )
        && (Maybe.isNothing selector.value
                || (Maybe.map Rdf.asBlankNodeOrIriOrAnyLiteral selector.value == dataValidationResult.value)
           )


detailsFor : Selector -> Report -> Report
detailsFor selector report =
    { report
        | results =
            List.concatMap
                (\((ValidationResult dataValidationResult) as validationResult) ->
                    if validationResultFor selector validationResult then
                        dataValidationResult.detail

                    else
                        []
                )
                report.results
    }


{-| This is sematically different from

    validationResultsFor selectorNested . detailsFor selector

as `validationResultsFor selectorNested` recurses into a validation result's detail unconditionally, ie. without checking for `selector`.

-}
validationResultsFor : Selector -> List ValidationResult -> List ValidationResult
validationResultsFor selector validationResults =
    List.concatMap
        (\((ValidationResult validationResultData) as validationResult) ->
            if validationResultFor selector validationResult then
                [ validationResult ]

            else
                validationResultsFor selector validationResultData.detail
        )
        validationResults


empty : Report
empty =
    { conforms = True
    , results = []
    }


fromGraph : Graph -> Report
fromGraph graph =
    case
        graph
            |> Decode.decode (Decode.fromInstancesOf (sh "ValidationReport") decoderReport)
            |> Result.withDefault []
    of
        [ report ] ->
            report

        _ ->
            empty


decoderReport : Decoder Report
decoderReport =
    Decode.succeed Report
        |> Decode.required (sh "conforms") Decode.bool
        |> Decode.required (sh "result") (Decode.many decoderValidationResult)


decoderValidationResult : Decoder ValidationResult
decoderValidationResult =
    Decode.succeed DataValidationResult
        |> Decode.required (sh "focusNode") Decode.blankNodeOrIri
        |> Decode.optional (sh "resultPath") (Decode.map Just PropertyPath.decoder) Nothing
        |> Decode.optional (sh "value") (Decode.map Just Decode.object) Nothing
        |> Decode.required (sh "sourceConstraintComponent") decoderConstraintComponent
        |> Decode.required (sh "resultMessage") Decode.stringOrLangString
        |> Decode.optional (sh "detail") (Decode.many (Decode.lazy (\_ -> decoderValidationResult))) []
        |> Decode.required (sh "resultSeverity") decoderSeverity
        |> Decode.optional (sh "sourceShape") (Decode.map Just Decode.iri) Nothing
        |> Decode.map ValidationResult


decoderConstraintComponent : Decoder ConstraintComponent
decoderConstraintComponent =
    Decode.andThen
        (\iri ->
            case iriToConstraintComponent iri of
                Nothing ->
                    Decode.fail (Rdf.toUrl iri ++ " is not a valid constraint component")

                Just constraintComponent ->
                    Decode.succeed constraintComponent
        )
        Decode.iri


iriToConstraintComponent : Iri -> Maybe ConstraintComponent
iriToConstraintComponent iri =
    if iri == sh "ClassConstraintComponent" then
        Just ClassConstraintComponent

    else if iri == sh "DatatypeConstraintComponent" then
        Just DatatypeConstraintComponent

    else if iri == sh "NodeKindConstraintComponent" then
        Just NodeKindConstraintComponent

    else if iri == sh "MinCountConstraintComponent" then
        Just MinCountConstraintComponent

    else if iri == sh "QualifiedMinCountConstraintComponent" then
        Just QualifiedMinCountConstraintComponent

    else if iri == sh "MaxCountConstraintComponent" then
        Just MaxCountConstraintComponent

    else if iri == sh "QualifiedMaxCountConstraintComponent" then
        Just QualifiedMaxCountConstraintComponent

    else if iri == sh "MinExclusiveConstraintComponent" then
        Just MinExclusiveConstraintComponent

    else if iri == sh "MinInclusiveConstraintComponent" then
        Just MinInclusiveConstraintComponent

    else if iri == sh "MaxExclusiveConstraintComponent" then
        Just MaxExclusiveConstraintComponent

    else if iri == sh "MaxInclusiveConstraintComponent" then
        Just MaxInclusiveConstraintComponent

    else if iri == sh "MinLengthConstraintComponent" then
        Just MinLengthConstraintComponent

    else if iri == sh "MaxLengthConstraintComponent" then
        Just MaxLengthConstraintComponent

    else if iri == sh "PatternLengthConstraintComponent" then
        Just PatternLengthConstraintComponent

    else if iri == sh "LanguageInConstraintComponent" then
        Just LanguageInConstraintComponent

    else if iri == sh "UniqueLangConstraintComponent" then
        Just UniqueLangConstraintComponent

    else if iri == sh "EqualsConstraintComponent" then
        Just EqualsConstraintComponent

    else if iri == sh "DisjointConstraintComponent" then
        Just DisjointConstraintComponent

    else if iri == sh "LessThanConstraintComponent" then
        Just LessThanConstraintComponent

    else if iri == sh "LessThanOrEqualsConstraintComponent" then
        Just LessThanOrEqualsConstraintComponent

    else if iri == sh "NotConstraintComponent" then
        Just NotConstrantComponent

    else if iri == sh "AndConstraintComponent" then
        Just AndConstrantComponent

    else if iri == sh "OrConstraintComponent" then
        Just OrConstrantComponent

    else if iri == sh "XoneConstraintComponent" then
        Just XoneConstrantComponent

    else if iri == sh "NodeConstraintComponent" then
        Just NodeConstraintComponent

    else if iri == sh "PropertyConstraintComponent" then
        Just PropertyConstraintComponent

    else if iri == sh "HasValueConstraintComponent" then
        Just HasValueConstraintComponent

    else if iri == sh "InConstraintComponent" then
        Just InConstraintComponent

    else if iri == sh "SPARQLConstraintComponent" then
        Just SparqlConstraintComponent

    else
        Nothing


decoderSeverity : Decoder Severity
decoderSeverity =
    Decode.andThen
        (\iri ->
            case iriToSeverity iri of
                Nothing ->
                    Decode.fail (Rdf.toUrl iri ++ " is not a valid severity")

                Just severity ->
                    Decode.succeed severity
        )
        Decode.iri


iriToSeverity : Iri -> Maybe Severity
iriToSeverity iri =
    if iri == sh "Info" then
        Just Info

    else if iri == sh "Warning" then
        Just Warning

    else if iri == sh "Violation" then
        Just Violation

    else
        Just Violation
