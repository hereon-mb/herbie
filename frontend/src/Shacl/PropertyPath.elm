module Shacl.PropertyPath exposing (decoder)

import Rdf.Decode as Decode exposing (Decoder)
import Rdf.Namespaces.SH as SH
import Rdf.PropertyPath exposing (PropertyPath(..))
import Tuple.Extra as Tuple


decoder : Decoder PropertyPath
decoder =
    let
        lazy =
            Decode.lazy (\_ -> decoder)

        nonEmpty =
            Decode.nonEmpty lazy
    in
    Decode.oneOf
        [ Decode.map PredicatePath Decode.iri
        , Decode.map (Tuple.apply SequencePath) nonEmpty
        , Decode.map (Tuple.apply AlternativePath) (Decode.predicate SH.alternativePath nonEmpty)
        , Decode.map InversePath (Decode.predicate SH.inversePath lazy)
        , Decode.map ZeroOrMorePath (Decode.predicate SH.zeroOrMorePath lazy)
        , Decode.map OneOrMorePath (Decode.predicate SH.oneOrMorePath lazy)
        , Decode.map ZeroOrOnePath (Decode.predicate SH.zeroOrOnePath lazy)
        ]
