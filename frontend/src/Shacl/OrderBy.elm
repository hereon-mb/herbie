module Shacl.OrderBy exposing
    ( OrderBy(..)
    , decoder
    , sort
    , sortBy
    )

import Basics.Extra exposing (uncurry)
import List.NonEmpty as NonEmpty exposing (NonEmpty)
import Locale exposing (Locale)
import Rdf
import Rdf.Decode as Decode exposing (Decoder)
import Rdf.Graph exposing (Graph)
import Rdf.PropertyPath as Rdf
import Shacl.Namespaces exposing (hash)


type OrderBy
    = Iri Rdf.Iri
    | Asc OrderBy
    | Desc OrderBy
    | Batch (NonEmpty OrderBy)


decoder : Decoder OrderBy
decoder =
    Decode.oneOf
        [ Decode.map Iri Decode.iri
        , Decode.map Desc
            (Decode.blankNode
                (Decode.predicate (hash "desc") (Decode.lazy (\_ -> decoder)))
            )
        , Decode.map Asc
            (Decode.blankNode
                (Decode.predicate (hash "asc") (Decode.lazy (\_ -> decoder)))
            )
        , Decode.map Batch
            (Decode.nonEmpty (Decode.lazy (\_ -> decoder)))
        ]


sortBy : (a -> Rdf.Iri) -> Locale -> Graph -> OrderBy -> List a -> List a
sortBy f locale graph orderBy xs =
    uncurry (++) (sortByHelp f locale graph orderBy ( [], xs ))


{-| `sortByHelp f graph orderBy (ys, xs)` sorts `xs` according to `OrderBy`,
and collected unsortable elements `y` of `xs` into `ys`.

An element `y` of `xs` is _unsortable_ if `compare` returns `Nothing`.

-}
sortByHelp : (a -> Rdf.Iri) -> Locale -> Graph -> OrderBy -> ( List a, List a ) -> ( List a, List a )
sortByHelp f locale graph orderBy ( ys, xs ) =
    case orderBy of
        Iri path ->
            Tuple.mapBoth List.reverse
                (List.map Tuple.second << List.sortBy Tuple.first << List.reverse)
                (List.foldl (sortByStep f locale graph path) ( ys, [] ) xs)

        Asc orderBy_ ->
            sortByHelp f locale graph orderBy_ ( ys, xs )

        Desc orderBy_ ->
            Tuple.mapSecond List.reverse
                (sortByHelp f locale graph (Asc orderBy_) ( ys, xs ))

        Batch orderBys ->
            NonEmpty.foldr (sortByHelp f locale graph) ( ys, xs ) orderBys


sortByStep : (a -> Rdf.Iri) -> Locale -> Graph -> Rdf.Iri -> a -> ( List a, List ( String, a ) ) -> ( List a, List ( String, a ) )
sortByStep f locale graph path x ( ys, xs ) =
    case compare locale graph (Rdf.PredicatePath path) (f x) of
        Just z ->
            ( ys, ( z, x ) :: xs )

        Nothing ->
            ( x :: ys, xs )


compare : Locale -> Graph -> Rdf.PropertyPath -> Rdf.Iri -> Maybe String
compare locale graph path subject =
    graph
        |> Decode.decode (Decode.from subject (Decode.property path Decode.stringOrLangString))
        |> Result.toMaybe
        |> Maybe.andThen
            (Rdf.localize
                (case locale of
                    Locale.EnUS ->
                        "en"

                    Locale.De ->
                        "de"
                )
            )


sort : Locale -> Graph -> OrderBy -> List Rdf.Iri -> List Rdf.Iri
sort =
    sortBy identity
