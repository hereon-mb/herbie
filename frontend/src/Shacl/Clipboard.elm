module Shacl.Clipboard exposing
    ( allObjects
    , allObjectsOne
    , allObjectsWithClasses
    , allSubjectsOne
    , onPasteGraph
    , serialize
    )

{-| This module defines a heuristic approach to decoding the clipboard graph
into a value suitable for a component.

It assumes a component `Model` over `value`, along with its serialization
functions `valueSerialized : Model -> List Encode.PropertyEncoder` and
`valueDeserialized : Decoder value`.

As an example, we may take the component `Text` over `Maybe String`.

The functions here are concerned with taking the serialized clipboard graph (of
type `String`), the components `valueDeserialized` and a message `a -> msg`
appropriate to be passed to `Accessibility.Clipboard.onPaste`, and find out
which parts of the graph are actually holding the components value.

This arises in the first place because we want to maintain self-contained
graphs inside the clipboard. A component's value is likely not a graph, but
rather a fragment of a graph. As such, the clipboard contains auxilliary
structure. For example:

A text component with value "hello world" might serialize into the following
clipboard graph:

```turtle
<root> <hasText> "hello world" .
```

Another text component with value "foobar" might serialize into the following
clipboard graph:

```turtle
<root> <hasOptionalText> "foobar" .
```

We would like the differences in the auxilliary structure NOT to prevent from
pasting from one text component into another. Note that the auxilliary
structure can grow arbitrarily large if we assume arbitrarily nested
components.

The approach taken here is a best-efforts approach which seemed to work
reasonable at the time of implementing the clipboard feature. For more details
on the specific heuristics, see the function documentation below.


## A note about `valueSerialized`

The function `valueSerialized` is modeled after `Model -> List
Encode.PropertyEncoder`. This aligns well with the fact that we are serializing
a fragment of a graph: we can only turn this into a graph if we at least have
a focus node, ie. some auxiliary graph.

However, in some components, we would like to place additional information into
the clipboard that is not strictly part of the components value. An example for
this is the label of a file (`FileUpload`, the class of a dropdown option
(`SelectOne`, `SelectMany`), etc.

To resolve this, a future change would be to turn `valueSerialized` into `Model
-> BlankNodeOrIri -> Encode.GraphEncoder` (or even `Model -> BlankNodeOrIri
-> Graph`) which would then be able to contain arbitrary graph edges that
don't necessarily pertain to the components' value.

This change should be straight-forward and has not been implemented due to lack
of time, only.

-}

import Accessibility.Blob as Blob exposing (Blob)
import Accessibility.Clipboard as Clipboard
import Element
import List.Extra as List
import Maybe.Extra as Maybe
import Rdf exposing (Iri)
import Rdf.Decode as Decode exposing (Decoder)
import Rdf.Encode as Encode
import Rdf.Graph as Rdf exposing (Graph)
import Rdf.Namespaces exposing (a)
import Rdf.Query as Rdf


onPasteGraph : msg -> Decoder msg -> Element.Attribute msg
onPasteGraph ignored decoderMsg =
    let
        parse : String -> Maybe msg
        parse =
            Rdf.parse
                >> Result.toMaybe
                >> Maybe.andThen handleGraph

        handleGraph : Graph -> Maybe msg
        handleGraph graph =
            graph
                |> Decode.decode decoderMsg
                |> Result.toMaybe
    in
    Clipboard.onPaste "text/plain" ignored parse


{-| Runs `decoder` on all objects of the graph, requiring that the decoder
succeeds on all of them.

Note that a decoder can trivially succeed on a single object, but also on
multiple objects if it uses `Decode.many`.

-}
allObjects : (a -> msg) -> Decoder a -> String -> Maybe msg
allObjects msg decoder =
    Rdf.parse
        >> Result.toMaybe
        >> Maybe.andThen
            (\graph ->
                Rdf.emptyQuery
                    |> Rdf.getSubjects graph
                    |> List.map
                        (\subject ->
                            case Rdf.toBlankNodeOrIri subject of
                                Nothing ->
                                    Nothing

                                Just blankNodeOrIri ->
                                    graph
                                        |> Decode.decode (Decode.from blankNodeOrIri (Decode.anyPredicate decoder))
                                        |> Result.toMaybe
                        )
                    |> Maybe.combine
                    |> Maybe.andThen List.head
            )
        >> Maybe.map msg


{-| Runs `decoder` on all objects of the graph, permitting failure. This
function requires that there is precisely one object on which the decoder
succeeds.
-}
allObjectsOne : (a -> msg) -> Decoder a -> String -> Maybe msg
allObjectsOne msg decoder =
    Rdf.parse
        >> Result.toMaybe
        >> Maybe.andThen
            (\graph ->
                Rdf.emptyQuery
                    |> Rdf.getSubjects graph
                    |> List.filterMap
                        (\subject ->
                            case Rdf.toBlankNodeOrIri subject of
                                Nothing ->
                                    Nothing

                                Just blankNodeOrIri ->
                                    graph
                                        |> Decode.decode (Decode.from blankNodeOrIri (Decode.anyPredicate (one decoder)))
                                        |> Result.toMaybe
                        )
                    |> List.head
            )
        >> Maybe.map msg


{-| The same as `allObjectsOne`, but decodes subjects instead of objects.
-}
allSubjectsOne : (a -> msg) -> Decoder a -> String -> Maybe msg
allSubjectsOne msg decoder =
    (Rdf.parse >> Result.toMaybe)
        >> Maybe.andThen
            (\graph ->
                Rdf.emptyQuery
                    |> Rdf.getSubjects graph
                    |> List.filterMap
                        (\subject ->
                            Decode.decode (Decode.from subject (one decoder)) graph
                                |> Result.toMaybe
                        )
                    |> List.head
            )
        >> Maybe.map msg


{-| The same as `allObjectsWithClass`, but permitting multiple classes.
-}
allObjectsWithClasses : List Iri -> (a -> msg) -> Decoder a -> String -> Maybe msg
allObjectsWithClasses classes msg decoder =
    (Rdf.parse >> Result.toMaybe)
        >> Maybe.andThen
            (\graph ->
                (case classes of
                    [] ->
                        []

                    _ ->
                        classes
                            |> List.concatMap
                                (\class ->
                                    Rdf.emptyQuery
                                        |> Rdf.withPredicate a
                                        |> Rdf.withObject class
                                        |> Rdf.getSubjects graph
                                )
                            |> List.unique
                )
                    |> List.filterMap
                        (\class ->
                            Decode.decode (Decode.from class decoder) graph
                                |> Result.toMaybe
                        )
                    |> List.head
            )
        >> Maybe.map msg


one : Decoder a -> Decoder a
one decoder =
    Decode.many
        (Decode.oneOf
            [ Decode.map Just decoder
            , Decode.succeed Nothing
            ]
        )
        |> Decode.map Maybe.values
        |> Decode.andThen
            (\xs ->
                case xs of
                    [] ->
                        Decode.fail "one: no result"

                    _ :: _ :: _ ->
                        Decode.fail "one: ambigious results"

                    [ x ] ->
                        Decode.succeed x
            )


serialize : Encode.Subject -> Rdf.Seed -> List Encode.PropertyEncoder -> Blob
serialize nodeFocus seed =
    Encode.node nodeFocus
        >> Encode.encode seed
        >> Tuple.first
        >> Blob.textTurtle
