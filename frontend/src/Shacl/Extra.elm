{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Shacl.Extra exposing
    ( nodeShapesWithTargetClasses, NodeShapeWithTargetClasses
    , nodeShapesWithInstances, NodeShapeWithInstances
    , Error(..), errorToString
    )

{-|


# Query

@docs nodeShapesWithTargetClasses, NodeShapeWithTargetClasses
@docs nodeShapesWithInstances, NodeShapeWithInstances
@docs Error, errorToString

-}

import List.NonEmpty as NonEmpty exposing (NonEmpty)
import Rdf
import Rdf.Decode as Decode
import Rdf.Decode.Extra as Decode
import Rdf.Graph exposing (Graph)
import Shacl exposing (NodeShape, Shapes)


type Error
    = NoNodeShapes
    | NoNodeShapeWithTargetClass { nodeShapes : NonEmpty NodeShape }
    | NoSubjectForTargetClass


errorToString : Error -> String
errorToString error =
    case error of
        NoNodeShapes ->
            "The SHACL document does not contain any Node Shapes."

        NoNodeShapeWithTargetClass data ->
            String.concat
                [ "None of the Node Shapes "
                , String.join ", " (List.map (.node >> Rdf.serializeNode) (NonEmpty.toList data.nodeShapes))
                , "specifies a target class."
                ]

        NoSubjectForTargetClass ->
            "Could not find a subject for the target class."


type alias NodeShapeWithTargetClasses =
    { nodeShape : NodeShape
    , targetClasses : NonEmpty Rdf.Iri
    }


nodeShapesWithTargetClasses : Shapes -> Result Error (NonEmpty NodeShapeWithTargetClasses)
nodeShapesWithTargetClasses shapes =
    shapes
        |> Shacl.nodeShapes
        |> NonEmpty.fromList
        |> Result.fromMaybe NoNodeShapes
        |> Result.andThen forDocumentRoot


type alias NodeShapeWithInstances =
    { nodeShape : NodeShape
    , instances : NonEmpty Rdf.Iri
    }


nodeShapesWithInstances : Graph -> Shapes -> Result Error (NonEmpty NodeShapeWithInstances)
nodeShapesWithInstances graph shapes =
    shapes
        |> nodeShapesWithTargetClasses
        |> Result.andThen
            (NonEmpty.toList
                >> List.concatMap (nodeShapesWithInstancesHelp graph)
                >> NonEmpty.fromList
                >> Result.fromMaybe NoSubjectForTargetClass
            )


forDocumentRoot : NonEmpty NodeShape -> Result Error (NonEmpty NodeShapeWithTargetClasses)
forDocumentRoot nodeShapes =
    NonEmpty.filterMap forDocumentRootHelp nodeShapes
        |> Result.fromMaybe (NoNodeShapeWithTargetClass { nodeShapes = nodeShapes })


forDocumentRootHelp : NodeShape -> Maybe NodeShapeWithTargetClasses
forDocumentRootHelp nodeShape =
    let
        withDocumentRoot targetClasses =
            if nodeShape.hashDocumentRoot then
                Just
                    { nodeShape = nodeShape
                    , targetClasses = targetClasses
                    }

            else
                Nothing
    in
    nodeShape.shTargetClass
        |> NonEmpty.fromList
        |> Maybe.andThen withDocumentRoot


nodeShapesWithInstancesHelp : Graph -> NodeShapeWithTargetClasses -> List NodeShapeWithInstances
nodeShapesWithInstancesHelp graph { nodeShape } =
    List.filterMap
        (\targetClass ->
            graph
                |> instancesOf targetClass
                |> NonEmpty.fromList
                |> Maybe.map (NodeShapeWithInstances nodeShape)
        )
        nodeShape.shTargetClass


instancesOf : Rdf.Iri -> Graph -> List Rdf.Iri
instancesOf class graph =
    graph
        |> Decode.decode (Decode.fromInstancesOf class Decode.iri)
        |> Result.withDefault []
