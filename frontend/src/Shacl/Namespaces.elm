module Shacl.Namespaces exposing (hash)

import Rdf exposing (Iri)


hash : String -> Iri
hash name =
    Rdf.iri ("http://purls.helmholtz-metadaten.de/herbie/hash/#" ++ name)
