module SPARQL exposing
    ( Query
    , select
    , withPrefix
    , toString
    , GroupGraphPattern
    , subSelectDistinct, bgp, graph, optional, filter
    )

{-|

@docs Query

@docs select
@docs withPrefix

@docs toString

@docs GroupGraphPattern
@docs subSelectDistinct, bgp, graph, optional, filter

-}

import Rdf exposing (Iri)
import SPARQL.Node as Node exposing (Node)
import SPARQL.Prefix as Prefix exposing (Prefix)


type Query
    = Query (List Prefix) String


toString : Query -> String
toString (Query prefixes raw) =
    if List.isEmpty prefixes then
        raw

    else
        [ prefixes
            |> List.sortWith Prefix.compare
            |> List.map Prefix.toString
            |> String.join "\n"
        , "\n\n"
        , raw
        ]
            |> String.concat


type GroupGraphPattern
    = SubSelect Bool (Maybe String) (List String) (List GroupGraphPattern)
    | GraphGraphPattern String
    | OptionalPattern (List GroupGraphPattern)
    | FilterPattern String


groupGraphPatternToString : GroupGraphPattern -> String
groupGraphPatternToString groupGraphPattern =
    case groupGraphPattern of
        SubSelect distinct maybeVar vars groupGraphPatterns ->
            [ "{\n"
            , [ if distinct then
                    "SELECT DISTINCT "

                else
                    "SELECT "
              , vars
                    |> List.map (\var -> "?" ++ var)
                    |> String.join " "
              , "\nWHERE {\n"
              , groupGraphPatterns
                    |> List.map groupGraphPatternToString
                    |> String.join "\n"
                    |> indent
              , "\n}"
              , case maybeVar of
                    Nothing ->
                        ""

                    Just var ->
                        " GROUP BY ?" ++ var
              ]
                |> String.concat
                |> indent
            , "\n}"
            ]
                |> String.concat

        GraphGraphPattern raw ->
            raw

        OptionalPattern groupGraphPatterns ->
            [ "OPTIONAL {\n"
            , groupGraphPatterns
                |> List.map groupGraphPatternToString
                |> String.join "\n"
                |> indent
            , "\n}"
            ]
                |> String.concat

        FilterPattern raw ->
            [ "FILTER ( "
            , raw
            , " )"
            ]
                |> String.concat


subSelectDistinct : List String -> List GroupGraphPattern -> GroupGraphPattern
subSelectDistinct =
    SubSelect True Nothing


bgp : List ( Node, Node, Node ) -> GroupGraphPattern
bgp triples =
    let
        tripleToString ( nodeSubject, nodePredicate, nodeObject ) =
            [ Node.toString nodeSubject
            , Node.toString nodePredicate
            , Node.toString nodeObject
            ]
                |> String.join " "
    in
    [ "{\n"
    , triples
        |> List.map tripleToString
        |> String.join " .\n"
        |> indent
    , "\n}"
    ]
        |> String.concat
        |> GraphGraphPattern


graph : Node -> List ( Node, Node, Node ) -> GroupGraphPattern
graph node triples =
    let
        tripleToString ( nodeSubject, nodePredicate, nodeObject ) =
            [ Node.toString nodeSubject
            , Node.toString nodePredicate
            , Node.toString nodeObject
            ]
                |> String.join " "
    in
    [ "GRAPH "
    , Node.toString node
    , " {\n"
    , triples
        |> List.map tripleToString
        |> String.join " .\n"
        |> indent
    , "\n}"
    ]
        |> String.concat
        |> GraphGraphPattern


optional : List GroupGraphPattern -> GroupGraphPattern
optional =
    OptionalPattern


filter : String -> GroupGraphPattern
filter =
    FilterPattern


select : List String -> List GroupGraphPattern -> Query
select vars groupGraphPatterns =
    [ "SELECT "
    , vars
        |> List.map (\var -> "?" ++ var)
        |> String.join " "
    , "\nWHERE {\n"
    , groupGraphPatterns
        |> List.map groupGraphPatternToString
        |> String.join "\n"
        |> indent
    , "\n}"
    ]
        |> String.concat
        |> Query []


withPrefix : String -> Iri -> Query -> Query
withPrefix name iri (Query prefixes raw) =
    Query (Prefix.prefix name iri :: prefixes) raw


indent : String -> String
indent raw =
    raw
        |> String.lines
        |> List.map (\line -> "  " ++ line)
        |> String.join "\n"
