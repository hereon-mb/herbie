{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Locale exposing
    ( Locale(..)
    , decoder, encode
    , fromString
    )

{-|

@docs Locale
@docs decoder, encode
@docs fromString

-}

import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode exposing (Value)


type Locale
    = EnUS
    | De


fromString : String -> Maybe Locale
fromString rawLocale =
    case rawLocale of
        "en-US" ->
            Just EnUS

        "de" ->
            Just De

        "de-DE" ->
            Just De

        _ ->
            Nothing


toString : Locale -> String
toString locale =
    case locale of
        EnUS ->
            "en-US"

        De ->
            "de"


decoder : Decoder Locale
decoder =
    let
        help raw =
            case fromString raw of
                Nothing ->
                    Decode.fail ("unknown locale '" ++ raw ++ "'")

                Just locale ->
                    Decode.succeed locale
    in
    Decode.andThen help Decode.string


encode : Locale -> Value
encode locale =
    Encode.string (toString locale)
