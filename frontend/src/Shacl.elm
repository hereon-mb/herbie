{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Shacl exposing
    ( Shapes, NodeShape, PropertyShape, PropertyGroup
    , NodeKind(..), NodeExpression(..), Editor(..), Viewer(..), ConfigLabel(..)
    , empty, fromGraph, Error(..), errorToString
    , nodeShapes, getNodeShape
    , propertyShapes, getPropertyShape
    , propertyGroups, getPropertyGroup
    , GridEditorData, ScheduleEditorData, Value
    )

{-|

@docs Shapes, NodeShape, PropertyShape, PropertyGroup
@docs NodeKind, NodeExpression, Editor, Viewer, ConfigLabel


# Create

@docs empty, fromGraph, Error, errorToString


# Query

@docs nodeShapes, getNodeShape
@docs propertyShapes, getPropertyShape
@docs propertyGroups, getPropertyGroup

-}

import Dict exposing (Dict)
import List.Extra as List
import Maybe.Extra as Maybe
import Rdf exposing (AnyLiteral, BlankNodeOrIri, BlankNodeOrIriOrAnyLiteral, Iri, StringOrLangString)
import Rdf.Decode as Decode exposing (Decoder, Error)
import Rdf.Decode.Extra as Decode
import Rdf.Graph exposing (Graph)
import Rdf.Namespaces exposing (a, dash)
import Rdf.Namespaces.RDFS as RDFS
import Rdf.Namespaces.SH as SH
import Rdf.PropertyPath exposing (PropertyPath(..))
import Rdf.Query as Rdf exposing (emptyQuery)
import Shacl.Namespaces exposing (hash)
import Shacl.OrderBy as OrderBy exposing (OrderBy)
import Shacl.PropertyPath as PropertyPath


type Shapes
    = Shapes ShapesData


type alias ShapesData =
    { nodeShapes : Dict String NodeShape
    , propertyShapes : Dict String PropertyShape
    , propertyGroups : Dict String PropertyGroup
    }


type alias NodeShape =
    { node : BlankNodeOrIri
    , shTargetClass : List Iri
    , shClass : List Iri
    , shHasValue : List Value
    , shIn : List Value
    , shProperty : List BlankNodeOrIri
    , shXone : List (List BlankNodeOrIri)
    , shOr : List (List BlankNodeOrIri)
    , shNode : List BlankNodeOrIri
    , shName : Maybe StringOrLangString
    , rdfsLabel : Maybe StringOrLangString
    , rdfsComment : Maybe StringOrLangString
    , hashDocumentRoot : Bool
    }


type alias PropertyShape =
    { node : BlankNodeOrIri
    , shTargetClass : List Iri
    , shGroup : Maybe Iri
    , shOrder : Maybe Float
    , shName : Maybe StringOrLangString
    , shDescription : Maybe StringOrLangString
    , shPath : PropertyPath
    , shClass : List Iri
    , shNode : List BlankNodeOrIri
    , shDatatype : Maybe Iri
    , shNodeKind : Maybe NodeKind
    , shMinExclusive : Maybe AnyLiteral
    , shMinInclusive : Maybe AnyLiteral
    , shMaxExclusive : Maybe AnyLiteral
    , shMaxInclusive : Maybe AnyLiteral
    , shMinCount : List Int
    , shMaxCount : List Int
    , dashEditor : Maybe Editor
    , hashDetailsEditorOrderPath : Maybe PropertyPath
    , dashViewer : Maybe Viewer
    , dashSingleLine : Maybe Bool
    , shHasValue : List Value
    , shIn : List Value
    , shValue : Maybe NodeExpression
    , shXone : List (List BlankNodeOrIri)
    , shQualifiedValueShape : Maybe BlankNodeOrIri
    , shQualifiedMinCount : Maybe Int
    , shQualifiedMaxCount : Maybe Int
    , hashLabelInCard : Maybe ConfigLabel
    , hashReadonly : Maybe Bool
    , hashGenerated : Maybe Bool
    , hashPersisted : Maybe Bool
    , hashOptionalViaCheckbox : Maybe Bool
    , shDefaultValue : List BlankNodeOrIriOrAnyLiteral
    , hashInclude : List Iri
    , hashOrderBy : Maybe OrderBy
    }


type alias Value =
    { this : BlankNodeOrIriOrAnyLiteral
    , label : Maybe StringOrLangString
    }


type NodeKind
    = NodeKindBlankNode
    | NodeKindIri
    | NodeKindLiteral
    | NodeKindBlankNodeOrIri
    | NodeKindBlankNodeOrLiteral
    | NodeKindIriOrLiteral


type NodeExpression
    = -- TODO add these
      -- | ConstantTermExpressionIri Iri
      -- | ConstantTermExpressionLiteral Literal
      -- | FunctionExpression Iri (List NodeExpression)
      -- | PathExpression PropertyPath
      -- | ExistsExpression PropertyShape
      -- | IfExpression NodeExpression (Maybe NodeExpression) (Maybe NodeExpression)
      -- | FilterShapeExpression NodeShape (Maybe NodeExpression)
      -- | IntersectionExpression NodeExpression (List NodeExpression)
      -- | UnionExpression NodeExpression (List NodeExpression)
      -- | MinusExpression NodeExpression NodeExpression
      -- | DistinctExpression NodeExpression
      -- | CountExpression NodeExpression
      -- | MinExpression NodeExpression
      -- | MaxExpression NodeExpression
      -- | SumExpression NodeExpression
      -- | GroupConcatExpression NodeExpression (Maybe String)
      -- | OrderByExpression NodeExpression NodeExpression (Maybe Bool)
      -- | LimitExpression Int NodeExpression
      -- | OffsetExpression Int NodeExpression
      -- | SPARQLAskExpression String (List String) (Maybe NodeExpression)
      -- | SPARQLSelectExpression String (List String) (Maybe NodeExpression)
      FocusNodeExpression


type Editor
    = DetailsEditor
    | InstancesSelectEditor
    | RadioButtonsEditor
    | GridEditor GridEditorData
    | ScheduleEditor ScheduleEditorData


{-|

  - `availableColumnsPath` references a _grid column_, a `rdfs:label`ed node with a numeric `herbie:order` field for sorting.
  - `cellViewer` and `cellEditor` reference a node shape.

-}
type alias GridEditorData =
    { availableColumnsPath : PropertyPath
    , availableRowsPath : PropertyPath
    , columnPath : Iri
    , rowPath : Iri
    , cellViewer : Maybe BlankNodeOrIri
    , cellEditor : Maybe BlankNodeOrIri
    }


{-|

  - `hashScheduleEditorDayOffsetPath` references the required schedule editor instance's `xsd:integer`-typed day field.
  - `hashScheduleEditorOrderPath` references the required schedule editor instance's `xsd:integer`-typed order field.

-}
type alias ScheduleEditorData =
    { hashScheduleEditorDayOffsetPath : PropertyPath
    , hashScheduleEditorOrderPath : PropertyPath
    , hashScheduleEditorSamples : List Iri
    , hashScheduleEditorTitlePath : PropertyPath
    , hashScheduleEditorDatePath : PropertyPath
    }


type Viewer
    = ValueCardViewer
    | ValueTableViewer


type ConfigLabel
    = LabelAbove
    | LabelHidden


type alias PropertyGroup =
    { group : Maybe Iri
    , order : Maybe Float
    , name : Maybe StringOrLangString
    }


empty : Shapes
empty =
    Shapes
        { nodeShapes = Dict.empty
        , propertyShapes = Dict.empty
        , propertyGroups = Dict.empty
        }


getNodeShape : Shapes -> Rdf.IsBlankNodeOrIri compatible -> Maybe NodeShape
getNodeShape (Shapes data) blankNodeOrIri =
    Dict.get (Rdf.serializeNode blankNodeOrIri) data.nodeShapes


getPropertyShape : Shapes -> Rdf.IsBlankNodeOrIri compatible -> Maybe PropertyShape
getPropertyShape (Shapes data) blankNodeOrIri =
    Dict.get (Rdf.serializeNode blankNodeOrIri) data.propertyShapes


getPropertyGroup : Shapes -> Iri -> Maybe PropertyGroup
getPropertyGroup (Shapes data) iri =
    Dict.get (Rdf.serializeNode iri) data.propertyGroups


nodeShapes : Shapes -> List NodeShape
nodeShapes (Shapes data) =
    Dict.values data.nodeShapes


propertyShapes : Shapes -> List PropertyShape
propertyShapes (Shapes data) =
    Dict.values data.propertyShapes


propertyGroups : Shapes -> List PropertyGroup
propertyGroups (Shapes data) =
    Dict.values data.propertyGroups


type Error
    = CouldNotDecodeNodeShape
        { node : BlankNodeOrIri
        , errorDecode : Decode.Error
        }
    | CouldNotDecodePropertyShape
        { node : BlankNodeOrIri
        , errorDecode : Decode.Error
        }
    | MinCountNotAllowedInNodeShape { nodeShape : BlankNodeOrIri }
    | MaxCountNotAllowedInNodeShape { nodeShape : BlankNodeOrIri }


errorToString : Error -> String
errorToString error =
    case error of
        CouldNotDecodeNodeShape { node, errorDecode } ->
            "Could not decode the node shape " ++ Rdf.serializeNode node ++ ": " ++ Decode.errorToString errorDecode ++ "."

        CouldNotDecodePropertyShape { node, errorDecode } ->
            "Could not decode the property shape " ++ Rdf.serializeNode node ++ ": " ++ Decode.errorToString errorDecode ++ "."

        MinCountNotAllowedInNodeShape _ ->
            "sh:minCount is not allowed in Node Shapes"

        MaxCountNotAllowedInNodeShape _ ->
            "sh:maxCount is not allowed in Node Shapes"


fromGraph : Graph -> Result Error Shapes
fromGraph graph =
    let
        toShapes ( nodeShapesList, propertyShapesList ) =
            Shapes
                { nodeShapes = Dict.fromList nodeShapesList
                , propertyShapes = Dict.fromList propertyShapesList
                , propertyGroups =
                    graph
                        |> Decode.decode propertyGroupsWithKey
                        |> Result.withDefault []
                        |> Dict.fromList
                }
    in
    { nodeShapes =
        emptyQuery
            |> Rdf.withPredicate a
            |> Rdf.withObject SH.nodeShape
            |> Rdf.getSubjects graph
    , propertyShapes =
        emptyQuery
            |> Rdf.withPredicate a
            |> Rdf.withObject SH.propertyShape
            |> Rdf.getSubjects graph
    , nodeShapesDone = []
    , propertyShapesDone = []
    }
        |> collectShapes graph
        |> Result.map toShapes


type alias ShapesStep =
    { nodeShapes : List BlankNodeOrIri
    , propertyShapes : List BlankNodeOrIri
    , nodeShapesDone : List ( String, NodeShape )
    , propertyShapesDone : List ( String, PropertyShape )
    }


collectShapes : Graph -> ShapesStep -> Result Error ( List ( String, NodeShape ), List ( String, PropertyShape ) )
collectShapes graph step =
    case step.nodeShapes of
        [] ->
            case step.propertyShapes of
                [] ->
                    Ok
                        ( step.nodeShapesDone
                        , step.propertyShapesDone
                        )

                first :: rest ->
                    { step | propertyShapes = rest }
                        |> propertyShapesFromSubject graph first
                        |> Result.andThen (collectShapes graph)

        first :: rest ->
            { step | nodeShapes = rest }
                |> nodeShapesFromSubject graph first
                |> Result.andThen (collectShapes graph)


nodeShapesFromSubject : Graph -> BlankNodeOrIri -> ShapesStep -> Result Error ShapesStep
nodeShapesFromSubject graph subject step =
    case Decode.decode (Decode.from subject nodeShape) graph of
        Err ((Decode.NodeDoesNotExist node) as error) ->
            if node == subject then
                Ok
                    { step
                        | nodeShapesDone =
                            ( Rdf.serializeNode subject
                            , nodeShapeEmpty subject
                            )
                                :: step.nodeShapesDone
                    }

            else
                Err
                    (CouldNotDecodeNodeShape
                        { node = subject
                        , errorDecode = error
                        }
                    )

        Err error ->
            Err
                (CouldNotDecodeNodeShape
                    { node = subject
                    , errorDecode = error
                    }
                )

        Ok next ->
            if not (List.isEmpty (shMinCountFor graph subject)) then
                Err (MinCountNotAllowedInNodeShape { nodeShape = subject })

            else if not (List.isEmpty (shMaxCountFor graph subject)) then
                Err (MaxCountNotAllowedInNodeShape { nodeShape = subject })

            else
                Ok
                    { step
                        | nodeShapes =
                            [ List.concat next.shXone
                            , List.concat next.shOr
                            , next.shNode
                            , step.nodeShapes
                            ]
                                |> List.concat
                                |> List.uniqueBy Rdf.serializeNode
                        , propertyShapes =
                            [ next.shProperty
                            , step.propertyShapes
                            ]
                                |> List.concat
                                |> List.uniqueBy Rdf.serializeNode
                        , nodeShapesDone = ( Rdf.serializeNode subject, next ) :: step.nodeShapesDone
                    }


shMinCountFor : Graph -> BlankNodeOrIri -> List Int
shMinCountFor graph subject =
    graph
        |> Decode.decode (Decode.from subject (Decode.predicate SH.minCount (Decode.many Decode.int)))
        |> Result.withDefault []


shMaxCountFor : Graph -> BlankNodeOrIri -> List Int
shMaxCountFor graph subject =
    graph
        |> Decode.decode (Decode.from subject (Decode.predicate SH.maxCount (Decode.many Decode.int)))
        |> Result.withDefault []


propertyShapesFromSubject : Graph -> BlankNodeOrIri -> ShapesStep -> Result Error ShapesStep
propertyShapesFromSubject graph subject step =
    case Decode.decode (Decode.from subject (propertyShape subject)) graph of
        Err ((Decode.NodeDoesNotExist node) as error) ->
            if node == subject then
                Ok step

            else
                Err
                    (CouldNotDecodePropertyShape
                        { node = subject
                        , errorDecode = error
                        }
                    )

        Err error ->
            Err
                (CouldNotDecodePropertyShape
                    { node = subject
                    , errorDecode = error
                    }
                )

        Ok next ->
            Ok
                { step
                    | propertyShapes =
                        [ List.concat next.shXone
                        , step.propertyShapes
                        ]
                            |> List.concat
                            |> List.uniqueBy Rdf.serializeNode
                    , nodeShapes =
                        [ next.shNode
                        , step.nodeShapes
                        , Maybe.toList next.shQualifiedValueShape
                        ]
                            |> List.concat
                            |> List.uniqueBy Rdf.serializeNode
                    , propertyShapesDone = ( Rdf.serializeNode subject, next ) :: step.propertyShapesDone
                }



-- EMPTY


nodeShapeEmpty : BlankNodeOrIri -> NodeShape
nodeShapeEmpty node =
    { node = node
    , shTargetClass = []
    , shClass = []
    , shHasValue = []
    , shIn = []
    , shProperty = []
    , shXone = []
    , shOr = []
    , shNode = []
    , shName = Nothing
    , rdfsLabel = Nothing
    , rdfsComment = Nothing
    , hashDocumentRoot = False
    }



-- DECODER


nodeShape : Decoder NodeShape
nodeShape =
    Decode.succeed NodeShape
        |> Decode.custom Decode.blankNodeOrIri
        |> Decode.optional SH.targetClass (Decode.many Decode.iri) []
        |> Decode.optional SH.class (Decode.many Decode.iri) []
        |> Decode.optional SH.hasValue (Decode.many decoderValue) []
        |> Decode.optional SH.in_ (Decode.list decoderValue) []
        |> Decode.optional SH.property (Decode.many Decode.blankNodeOrIri) []
        |> Decode.optional SH.xone (Decode.many (Decode.list Decode.blankNodeOrIri)) []
        |> Decode.optional SH.or (Decode.many (Decode.list Decode.blankNodeOrIri)) []
        |> Decode.optional SH.node (Decode.many Decode.blankNodeOrIri) []
        |> Decode.optional SH.name (Decode.map Just Decode.stringOrLangString) Nothing
        |> Decode.optional RDFS.label (Decode.map Just Decode.stringOrLangString) Nothing
        |> Decode.optional RDFS.comment (Decode.map Just Decode.stringOrLangString) Nothing
        |> Decode.optional (hash "documentRoot") Decode.bool False


propertyShape : BlankNodeOrIri -> Decoder PropertyShape
propertyShape node =
    Decode.succeed PropertyShape
        |> Decode.hardcoded node
        |> Decode.optional SH.targetClass (Decode.many Decode.iri) []
        |> Decode.optional SH.group (Decode.map Just Decode.iri) Nothing
        |> Decode.optional SH.order (Decode.map Just Decode.number) Nothing
        |> Decode.optional SH.name (Decode.map Just Decode.stringOrLangString) Nothing
        |> Decode.optional SH.description (Decode.map Just Decode.stringOrLangString) Nothing
        |> Decode.required SH.path PropertyPath.decoder
        |> Decode.optional SH.class (Decode.many Decode.iri) []
        |> Decode.optional SH.node (Decode.many Decode.blankNodeOrIri) []
        |> Decode.optional SH.datatype (Decode.map Just Decode.iri) Nothing
        |> Decode.optional SH.nodeKind (Decode.map Just nodeKind) Nothing
        |> Decode.optional SH.minExclusive (Decode.map Just Decode.anyLiteral) Nothing
        |> Decode.optional SH.minInclusive (Decode.map Just Decode.anyLiteral) Nothing
        |> Decode.optional SH.maxExclusive (Decode.map Just Decode.anyLiteral) Nothing
        |> Decode.optional SH.maxInclusive (Decode.map Just Decode.anyLiteral) Nothing
        |> Decode.optional SH.minCount (Decode.many Decode.int) []
        |> Decode.optional SH.maxCount (Decode.many Decode.int) []
        |> Decode.optional (dash "editor") (Decode.map Just editor) Nothing
        |> Decode.optional (hash "detailsEditorOrderPath") (Decode.map Just PropertyPath.decoder) Nothing
        |> Decode.optional (dash "viewer") (Decode.map Just viewer) Nothing
        |> Decode.optional (dash "singleLine") (Decode.map Just Decode.bool) Nothing
        |> Decode.optional SH.hasValue (Decode.many decoderValue) []
        |> Decode.optional SH.in_ (Decode.list decoderValue) []
        |> Decode.optional SH.values (Decode.map Just nodeExpression) Nothing
        |> Decode.optional SH.xone (Decode.many (Decode.list Decode.blankNodeOrIri)) []
        |> Decode.optional SH.qualifiedValueShape (Decode.map Just Decode.blankNodeOrIri) Nothing
        |> Decode.optional SH.qualifiedMinCount (Decode.map Just Decode.int) Nothing
        |> Decode.optional SH.qualifiedMaxCount (Decode.map Just Decode.int) Nothing
        |> Decode.optional (hash "labelInCard") (Decode.map Just configLabel) Nothing
        |> Decode.optional (hash "readonly") (Decode.map Just Decode.bool) Nothing
        |> Decode.optional (hash "generated") (Decode.map Just Decode.bool) Nothing
        |> Decode.optional (hash "persisted") (Decode.map Just Decode.bool) Nothing
        |> Decode.optional (hash "optionalViaCheckbox") (Decode.map Just Decode.bool) Nothing
        |> Decode.optional SH.defaultValue (Decode.many Decode.blankNodeOrIriOrAnyLiteral) []
        |> Decode.optional (hash "include") (Decode.many Decode.iri) []
        |> Decode.optional (hash "orderBy") (Decode.map Just OrderBy.decoder) Nothing


decoderValue : Decoder Value
decoderValue =
    Decode.succeed Value
        |> Decode.custom Decode.blankNodeOrIriOrAnyLiteral
        |> Decode.optionalStringOrLangString RDFS.label


nodeKind : Decoder NodeKind
nodeKind =
    Decode.andThen
        (\iri ->
            if iri == SH.blankNode then
                Decode.succeed NodeKindBlankNode

            else if iri == SH.iri then
                Decode.succeed NodeKindIri

            else if iri == SH.literal then
                Decode.succeed NodeKindLiteral

            else if iri == SH.blankNodeOrIri then
                Decode.succeed NodeKindBlankNodeOrIri

            else if iri == SH.blankNodeOrLiteral then
                Decode.succeed NodeKindBlankNodeOrLiteral

            else if iri == SH.iriOrLiteral then
                Decode.succeed NodeKindIriOrLiteral

            else
                Decode.fail ("Invalid node kind: " ++ Rdf.serializeNode iri ++ ".")
        )
        Decode.iri


editor : Decoder Editor
editor =
    Decode.oneOf
        [ detailsEditorDecoder
        , instancesSelectEditorDecoder
        , radioButtonsEditorDecoder
        , gridEditorDecoder
        , scheduleEditorDecoder
        ]


detailsEditorDecoder : Decoder Editor
detailsEditorDecoder =
    Decode.map (\_ -> DetailsEditor)
        (expectedIri (dash "DetailsEditor"))


radioButtonsEditorDecoder : Decoder Editor
radioButtonsEditorDecoder =
    Decode.map (\_ -> RadioButtonsEditor)
        (expectedIri (dash "RadioButtonsEditor"))


instancesSelectEditorDecoder : Decoder Editor
instancesSelectEditorDecoder =
    Decode.map (\_ -> InstancesSelectEditor)
        (expectedIri (dash "InstancesSelectEditor"))


gridEditorDecoder : Decoder Editor
gridEditorDecoder =
    Decode.predicate a (expectedIri (hash "GridEditor"))
        |> Decode.andThen
            (\_ ->
                Decode.succeed GridEditorData
                    |> Decode.required (hash "gridEditorAvailableColumnsPath") PropertyPath.decoder
                    |> Decode.required (hash "gridEditorAvailableRowsPath") PropertyPath.decoder
                    |> Decode.required (hash "gridEditorColumnPath") Decode.iri
                    |> Decode.required (hash "gridEditorRowPath") Decode.iri
                    |> Decode.optional (hash "gridEditorCellViewer") (Decode.map Just Decode.blankNodeOrIri) Nothing
                    |> Decode.optional (hash "gridEditorCellEditor") (Decode.map Just Decode.blankNodeOrIri) Nothing
                    |> Decode.map GridEditor
            )


scheduleEditorDecoder : Decoder Editor
scheduleEditorDecoder =
    Decode.predicate a (expectedIri (hash "ScheduleEditor"))
        |> Decode.andThen
            (\_ ->
                Decode.succeed ScheduleEditorData
                    |> Decode.required (hash "scheduleEditorDayOffsetPath") PropertyPath.decoder
                    |> Decode.required (hash "scheduleEditorOrderPath") PropertyPath.decoder
                    |> Decode.optional (hash "scheduleEditorSamples") (Decode.list Decode.iri) []
                    |> Decode.required (hash "scheduleEditorTitlePath") PropertyPath.decoder
                    |> Decode.required (hash "scheduleEditorDatePath") PropertyPath.decoder
                    |> Decode.map ScheduleEditor
            )


expectedIri : Iri -> Decoder ()
expectedIri iriExpected =
    Decode.iri
        |> Decode.andThen
            (\iriFound ->
                if iriFound == iriExpected then
                    Decode.succeed ()

                else
                    Decode.fail ("expected " ++ Rdf.toUrl iriExpected ++ ", but got `" ++ Rdf.toUrl iriFound ++ "'")
            )


viewer : Decoder Viewer
viewer =
    Decode.andThen
        (\iri ->
            if iri == dash "ValueTableViewer" then
                Decode.succeed ValueTableViewer

            else if iri == dash "ValueCardViewer" then
                Decode.succeed ValueCardViewer

            else
                Decode.fail ("Invalid viewer: " ++ Rdf.serializeNode iri ++ ".")
        )
        Decode.iri


nodeExpression : Decoder NodeExpression
nodeExpression =
    Decode.andThen
        (\iri ->
            if iri == SH.this then
                Decode.succeed FocusNodeExpression

            else
                Decode.fail ("Invalid node expression: " ++ Rdf.serializeNode iri ++ ".")
        )
        Decode.iri


configLabel : Decoder ConfigLabel
configLabel =
    Decode.andThen
        (\iri ->
            if iri == hash "labelAbove" then
                Decode.succeed LabelAbove

            else if iri == hash "labelHidden" then
                Decode.succeed LabelHidden

            else
                Decode.fail ("Invalid config label: " ++ Rdf.serializeNode iri ++ ".")
        )
        Decode.iri


propertyGroupsWithKey : Decoder (List ( String, PropertyGroup ))
propertyGroupsWithKey =
    Decode.from SH.propertyGroup
        (Decode.property (InversePath (PredicatePath a))
            (Decode.many
                (Decode.map2 Tuple.pair Decode.iri propertyGroup)
            )
        )
        |> Decode.map (List.map (Tuple.mapFirst Rdf.serializeNode))


propertyGroup : Decoder PropertyGroup
propertyGroup =
    Decode.succeed PropertyGroup
        |> Decode.optional SH.group (Decode.map Just Decode.iri) Nothing
        |> Decode.optional SH.order (Decode.map Just Decode.number) Nothing
        |> Decode.optional RDFS.label (Decode.map Just Decode.stringOrLangString) Nothing
