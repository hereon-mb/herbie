module Fixture.Context exposing (default)

import Api.Author exposing (Author)
import Api.Id as Id
import Api.Me exposing (AuthorDetail)
import Context exposing (C)
import Dict
import Element exposing (Device)
import Gen.Route as Route
import Locale
import Rdf
import RemoteData
import Time
import Ui.Atom.Tooltip as Tooltip
import Url exposing (Protocol(..), Url)
import Viewport exposing (Viewport)


default : C
default =
    { sandbox = False
    , useTripleStore = True
    , useSparklis = True
    , allowUploads = True
    , imprintUrl = Nothing
    , dataProtectionUrl = Nothing
    , accessibilityUrl = Nothing
    , csrfToken = ""
    , author = authorDetail
    , url = url "/journal"
    , route = Route.Datasets
    , query = Dict.empty
    , workspaceUuid = Nothing
    , workspace = Nothing

    -- TIME
    , zone = Time.utc
    , now = Time.millisToPosix 0
    , locale = Locale.EnUS

    -- DOM
    , viewport = viewport
    , device = device
    , navigationVisible = False
    , tooltip = Tooltip.init

    -- DATA
    , authors = RemoteData.Success [ authorAlice ]
    , workspaces = RemoteData.Success []

    -- SHACL
    , cShacl =
        { tooltip = Tooltip.init
        , locale = Locale.EnUS
        , allowUploads = True
        , permissions = authorDetail.permissions
        , preferences = authorDetail.preferencesAuthor
        , toUrl = \_ -> Rdf.toUrl
        }
    }


authorDetail : AuthorDetail
authorDetail =
    { id = Id.fromInt 1
    , user =
        { id = Id.fromInt 1
        , url = "#"
        , username = "alice"
        , firstName = "Alice"
        , lastName = "Wonderland"
        , email = "alice@example.com"
        }
    , permissions =
        { superuser = False
        , canSendSparqlQueries = True
        , canUploadRawFiles = True
        }
    , preferencesAuthor =
        { renderIrisInDropdownMenus = False
        }
    , authTokens = []
    }


url : String -> Url
url path =
    { protocol = Https
    , host = "example.org"
    , port_ = Nothing
    , path = path
    , query = Nothing
    , fragment = Nothing
    }


viewport : Viewport
viewport =
    { width = 1024
    , height = 960
    }


device : Device
device =
    { class = Element.Desktop
    , orientation = Element.Landscape
    }


authorAlice : Author
authorAlice =
    { id = Id.fromInt 1
    , url = "#"
    , user =
        { id = Id.fromInt 1
        , url = "#"
        , username = "alice"
        , firstName = "Alice"
        , lastName = "Wonderland"
        , email = "alice@example.com"
        }
    }
