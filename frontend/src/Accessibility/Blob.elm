module Accessibility.Blob exposing
    ( Blob
    , textPlain
    , textTurtle
    , textUriList
    )

import List.NonEmpty as NonEmpty exposing (NonEmpty)
import Rdf
import Rdf.Graph as Rdf


type alias Blob =
    { mimeType : String
    , parts : NonEmpty String
    }


textPlain : String -> Blob
textPlain string =
    { mimeType = "text/plain"
    , parts = NonEmpty.singleton string
    }


{-| MIME type `text/turtle` is not supported by browsers yet!
-}
textTurtle : Rdf.Graph -> Blob
textTurtle =
    textPlain << Rdf.serialize


{-| MIME type `text/uri-list` is not supported by browsers yet!
-}
textUriList : Rdf.Iri -> Blob
textUriList =
    textPlain << Rdf.toUrl
