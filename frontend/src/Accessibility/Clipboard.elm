module Accessibility.Clipboard exposing
    ( onCopy
    , onCopyHtml
    , onPaste
    , onPasteHtml
    )

import Element exposing (Attribute)
import Html
import Html.Events
import Json.Decode as Decode
import Maybe.Extra as Maybe


onCopy : msg -> Attribute msg
onCopy msg =
    Element.htmlAttribute (onCopyHtml msg)


onCopyHtml : msg -> Html.Attribute msg
onCopyHtml msg =
    Html.Events.custom "copy"
        (Decode.succeed
            { message = msg
            , stopPropagation = True
            , preventDefault = True
            }
        )


onPaste : String -> msg -> (String -> Maybe msg) -> Attribute msg
onPaste format msgPrevented msg =
    Element.htmlAttribute (onPasteHtml format msgPrevented msg)


onPasteHtml : String -> msg -> (String -> Maybe msg) -> Html.Attribute msg
onPasteHtml format msgPrevented msg =
    Html.Events.custom "paste"
        (Decode.at [ "clipboardData", "_elmCompat", "data", format ] Decode.string
            |> Decode.map
                (\string ->
                    if String.isEmpty string then
                        Nothing

                    else
                        msg string
                )
            |> Decode.map
                (\message ->
                    { message = Maybe.withDefault msgPrevented message
                    , stopPropagation = True
                    , preventDefault = Maybe.isJust message
                    }
                )
        )
