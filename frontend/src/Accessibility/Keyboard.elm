{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Accessibility.Keyboard exposing
    ( Event
    , IntentFocus(..)
    , applyIntentFocus
    , onIntentFocus
    , eventsIntentFocus
    , IntentSelection(..)
    , applyIntentSelection
    , onIntentSelection
    , eventsIntentSelection
    , mapEvents
    , onKeyDown
    )

{-|

  - [Keyboard Interaction For Data Grids](https://www.w3.org/WAI/ARIA/apg/patterns/grid/)

@docs Event


## Focus Interaction

@docs IntentFocus
@docs applyIntentFocus

@docs onIntentFocus
@docs eventsIntentFocus


## Selection Interaction

@docs IntentSelection
@docs applyIntentSelection

@docs onIntentSelection
@docs eventsIntentSelection


## Low-level access

@docs mapEvents
@docs onKeyDown

-}

import Basics.Extra exposing (flip)
import Element exposing (htmlAttribute)
import Html.Events as Events
import Json.Decode as Decode
import Maybe.Extra as Maybe
import Set exposing (Set)


type IntentFocus
    = IntentFocusMoveDown
    | IntentFocusMoveUp
    | IntentFocusMoveLeft
    | IntentFocusMoveRight
    | IntentFocusMoveFirst
    | IntentFocusMoveLast
    | IntentFocusMoveRowFirst
    | IntentFocusMoveRowLast


type alias Event msg =
    { key : String
    , ctrlKey : Bool
    , shiftKey : Bool
    , msg : msg
    }


mapEvent : (msg1 -> msg2) -> Event msg1 -> Event msg2
mapEvent f { key, ctrlKey, shiftKey, msg } =
    { key = key
    , ctrlKey = ctrlKey
    , shiftKey = shiftKey
    , msg = f msg
    }


mapEvents : (msg1 -> msg2) -> List (Event msg1) -> List (Event msg2)
mapEvents f =
    List.map (mapEvent f)


eventsIntentFocus : List (Event IntentFocus)
eventsIntentFocus =
    [ { key = "ArrowDown"
      , ctrlKey = False
      , shiftKey = False
      , msg = IntentFocusMoveDown
      }
    , { key = "ArrowUp"
      , ctrlKey = False
      , shiftKey = False
      , msg = IntentFocusMoveUp
      }
    , { key = "ArrowLeft"
      , ctrlKey = False
      , shiftKey = False
      , msg = IntentFocusMoveLeft
      }
    , { key = "ArrowRight"
      , ctrlKey = False
      , shiftKey = False
      , msg = IntentFocusMoveRight
      }
    , { key = "Home"
      , ctrlKey = False
      , shiftKey = False
      , msg = IntentFocusMoveFirst
      }
    , { key = "End"
      , ctrlKey = False
      , shiftKey = False
      , msg = IntentFocusMoveLast
      }
    , { key = "Home"
      , ctrlKey = True
      , shiftKey = False
      , msg = IntentFocusMoveRowFirst
      }
    , { key = "End"
      , ctrlKey = True
      , shiftKey = False
      , msg = IntentFocusMoveRowLast
      }
    ]


onIntentFocus : Element.Attribute IntentFocus
onIntentFocus =
    onKeyDown eventsIntentFocus


applyIntentFocus :
    ( Int, Int )
    -> IntentFocus
    -> ( Int, Int )
    -> ( Int, Int )
applyIntentFocus ( indexRowMax, indexColumnMax ) interactionFocus ( indexRowCurrent, indexColumnCurrent ) =
    case interactionFocus of
        IntentFocusMoveDown ->
            if indexRowCurrent < indexRowMax then
                ( indexRowCurrent + 1, indexColumnCurrent )

            else
                ( indexRowMax, indexColumnMax )

        IntentFocusMoveUp ->
            if indexRowCurrent > 0 then
                ( indexRowCurrent - 1, indexColumnCurrent )

            else
                ( 0, 0 )

        IntentFocusMoveLeft ->
            if indexColumnCurrent > 0 then
                ( indexRowCurrent, indexColumnCurrent - 1 )

            else if indexRowCurrent > 0 then
                ( indexRowCurrent - 1, indexColumnMax )

            else
                ( 0, 0 )

        IntentFocusMoveRight ->
            if indexColumnCurrent < indexColumnMax then
                ( indexRowCurrent, indexColumnCurrent + 1 )

            else if indexRowCurrent < indexRowMax then
                ( indexRowCurrent + 1, 0 )

            else
                ( indexRowMax, indexColumnMax )

        IntentFocusMoveFirst ->
            ( 0, 0 )

        IntentFocusMoveLast ->
            ( indexRowMax, indexColumnMax )

        IntentFocusMoveRowFirst ->
            ( indexRowCurrent, 0 )

        IntentFocusMoveRowLast ->
            ( indexRowCurrent, indexColumnMax )


type IntentSelection
    = IntentToggleFocused
    | IntentSelectColumnFocused
    | IntentSelectRowFocused
    | IntentSelectAll
    | IntentSelectExtendRight
    | IntentSelectExtendLeft
    | IntentSelectExtendDown
    | IntentSelectExtendUp


{-|

  - [Keyboard Interaction For Data Grids](https://www.w3.org/WAI/ARIA/apg/patterns/grid/)

-}
eventsIntentSelection : List (Event IntentSelection)
eventsIntentSelection =
    [ { key = " "
      , ctrlKey = False
      , shiftKey = False
      , msg = IntentToggleFocused
      }
    , { key = " "
      , ctrlKey = True
      , shiftKey = False
      , msg = IntentSelectColumnFocused
      }
    , { key = " "
      , ctrlKey = False
      , shiftKey = True
      , msg = IntentSelectRowFocused
      }
    , { key = "a"
      , ctrlKey = True
      , shiftKey = False
      , msg = IntentSelectAll
      }
    , { key = "ArrowRight"
      , ctrlKey = False
      , shiftKey = True
      , msg = IntentSelectExtendRight
      }
    , { key = "ArrowLeft"
      , ctrlKey = False
      , shiftKey = True
      , msg = IntentSelectExtendLeft
      }
    , { key = "ArrowDown"
      , ctrlKey = False
      , shiftKey = True
      , msg = IntentSelectExtendDown
      }
    , { key = "ArrowUp"
      , ctrlKey = False
      , shiftKey = True
      , msg = IntentSelectExtendUp
      }
    ]


onIntentSelection : Element.Attribute IntentSelection
onIntentSelection =
    onKeyDown eventsIntentSelection


applyIntentSelection :
    ( Int, Int )
    -> IntentSelection
    -> ( Int, Int )
    -> Set ( Int, Int )
    -> ( Maybe ( Int, Int ), Set ( Int, Int ) )
applyIntentSelection (( indexRowMax, indexColumnMax ) as indexGridMax) interactionSelection (( indexRowFocused, indexColumnFocused ) as focused) selected_ =
    let
        selected =
            if Set.isEmpty selected_ then
                Set.singleton focused

            else
                selected_
    in
    case interactionSelection of
        IntentToggleFocused ->
            ( Nothing
            , (if Set.member focused selected_ then
                Set.remove

               else
                Set.insert
              )
                focused
                selected
            )

        IntentSelectColumnFocused ->
            ( Nothing
            , Set.fromList
                (List.map (\indexRow -> ( indexRow, indexColumnFocused ))
                    (List.range 0 indexRowMax)
                )
            )

        IntentSelectRowFocused ->
            ( Nothing
            , Set.fromList
                (List.map (\indexColumn -> ( indexRowFocused, indexColumn ))
                    (List.range 0 indexColumnMax)
                )
            )

        IntentSelectAll ->
            ( Nothing
            , Set.fromList
                (List.concatMap
                    (\indexRow ->
                        List.map (\indexColumn -> ( indexRow, indexColumn ))
                            (List.range 0 indexColumnMax)
                    )
                    (List.range 0 indexRowMax)
                )
            )

        IntentSelectExtendRight ->
            let
                focusedUpdated =
                    applyIntentFocus indexGridMax IntentFocusMoveRight focused
            in
            ( Just focusedUpdated
            , Set.insert focusedUpdated selected
            )

        IntentSelectExtendLeft ->
            let
                focusedUpdated =
                    applyIntentFocus indexGridMax IntentFocusMoveLeft focused
            in
            ( Just focusedUpdated
            , Set.insert focusedUpdated selected
            )

        IntentSelectExtendDown ->
            let
                focusedUpdated =
                    applyIntentFocus indexGridMax IntentFocusMoveDown focused
            in
            ( Just focusedUpdated
            , Set.insert focusedUpdated selected
            )

        IntentSelectExtendUp ->
            let
                focusedUpdated =
                    applyIntentFocus indexGridMax IntentFocusMoveUp focused
            in
            ( Just focusedUpdated
            , Set.insert focusedUpdated selected
            )


onKeyDown : List (Event msg) -> Element.Attribute msg
onKeyDown events =
    let
        match { key, ctrlKey, shiftKey } event =
            if
                (event.key == key)
                    && (event.ctrlKey == ctrlKey)
                    && (event.shiftKey == shiftKey)
            then
                Just event.msg

            else
                Nothing
    in
    htmlAttribute <|
        Events.preventDefaultOn "keydown"
            (Decode.map3
                (\key ctrlKey shiftKey ->
                    { key = key
                    , ctrlKey = ctrlKey
                    , shiftKey = shiftKey
                    }
                )
                (Decode.field "key" Decode.string)
                (Decode.field "ctrlKey" Decode.bool)
                (Decode.field "shiftKey" Decode.bool)
                |> Decode.andThen
                    (\keys ->
                        Maybe.unwrap (Decode.fail "")
                            (Decode.succeed << flip Tuple.pair True)
                        <|
                            List.head (List.filterMap (match keys) events)
                    )
            )
