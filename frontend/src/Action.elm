{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Action exposing
    ( Action(..)
    , ConfigGetGraph
    , ConfigGetInstances
    , toastFromError
    , map
    )

{-|

@docs Action

@docs ConfigGetGraph
@docs ConfigGetInstances

@docs toastFromError

@docs map

-}

import Accessibility.Blob exposing (Blob)
import Action.DatasetCreate as DatasetCreate
import Api
import Api.Me.Preferences exposing (Preferences)
import Context exposing (C)
import Dict exposing (Dict)
import File exposing (File)
import Fluent exposing (verbatim)
import Ontology.Instance exposing (Instance)
import Ontology.Shacl exposing (Class)
import Rdf exposing (BlankNodeOrIriOrAnyLiteral, Iri)
import Rdf.DictIri exposing (DictIri)
import Rdf.Format exposing (Format)
import Rdf.Graph exposing (Graph)
import Shacl.Form.Breadcrumbs exposing (Path)
import Shacl.Form.Decoded as Decoded
import Shacl.Form.Grouped as Grouped
import Shacl.Form.Localize as Localize
import Toast
import Ui.Atom.Tooltip as Tooltip


type Action msg
    = -- USER INTENTS
      UserIntentsToCreateDatasetFor String (List Class)
    | UserIntentsToCreateDataset Bool (Result DatasetCreate.Error String -> msg) String Iri
    | UserIntentsToEditDataset (Api.Response () -> msg) String
    | UserIntentsToEditDatasetWith (Api.Response () -> msg) String File Format
    | UserIntentsToDeleteDraft (Api.Response () -> msg) String
    | UserIntentsToPublishDraft (Api.Response () -> msg) String
    | UserIntentsToAddAlias (Api.Response () -> msg) String Iri
    | UserIntentsToDeleteAlias (Api.Response () -> msg) String Iri
    | UserIntentsToCollapseNavigation
    | UserIntentsToClearStore
    | UserIntentsToUpdatePreferences Preferences
      -- UI
    | TooltipMsg Tooltip.Msg
    | AddToast Toast.Content
    | AddToastExpireIn Int Toast.Content
      -- FOCUS
    | Focus String
      -- DATA FETCHING
    | RequestAuthorDetails
      -- LOAD
    | UpdateTriples
      -- GET
    | GetGraph ConfigGetGraph (Api.Response Graph -> msg) Iri
    | GetGraphTurtle (String -> msg) Iri
    | GetGraphNTriples (String -> msg) Iri
    | GetGraphJsonLd (String -> msg) Iri
    | GetInstances (Api.Response (List Instance) -> msg) ConfigGetInstances
      -- STORE
    | StoreTrig (Api.Response (DictIri Graph) -> msg) Iri String
      -- QUADSTORE
    | Query (List (Dict String BlankNodeOrIriOrAnyLiteral) -> msg) String
      -- CLIPBOARD
    | CopyToClipboard Blob
      -- WORKSPACE
    | SelectWorkspace String
      -- SHACL
    | CreateField (Api.Response Decoded.Field -> msg) Bool Path Decoded.Mint Grouped.Field
    | DuplicateField (Api.Response Decoded.Field -> msg) Bool Path Decoded.Mint Decoded.Field


{-| Indicate if the requested graph

    - **store:** should be stored in the browser triple store
    - **reload:** should be refetched from the backend, even if it already was fetched
    - **closure:** follow all `owl:imports` and return the union of all these graphs

-}
type alias ConfigGetGraph =
    { store : Bool
    , reload : Bool
    , closure : Bool
    }


type alias ConfigGetInstances =
    { workspaceUuid : String
    , class : Rdf.Iri
    , includes : List Iri
    }


map : (msgA -> msgB) -> Action msgA -> Action msgB
map func action =
    case action of
        UserIntentsToEditDataset onResponse uuid ->
            UserIntentsToEditDataset (onResponse >> func) uuid

        UserIntentsToEditDatasetWith onResponse uuid file format ->
            UserIntentsToEditDatasetWith (onResponse >> func) uuid file format

        UserIntentsToDeleteDraft onResponse uuid ->
            UserIntentsToDeleteDraft (onResponse >> func) uuid

        UserIntentsToPublishDraft onResponse uuid ->
            UserIntentsToPublishDraft (onResponse >> func) uuid

        UserIntentsToAddAlias onResponse uuid iri ->
            UserIntentsToAddAlias (onResponse >> func) uuid iri

        UserIntentsToDeleteAlias onResponse uuid iri ->
            UserIntentsToDeleteAlias (onResponse >> func) uuid iri

        UserIntentsToCollapseNavigation ->
            UserIntentsToCollapseNavigation

        UserIntentsToCreateDatasetFor workspaceUuid classes ->
            UserIntentsToCreateDatasetFor workspaceUuid classes

        UserIntentsToCreateDataset open onResponse workspaceUuid iri ->
            UserIntentsToCreateDataset open (onResponse >> func) workspaceUuid iri

        UserIntentsToClearStore ->
            UserIntentsToClearStore

        UserIntentsToUpdatePreferences preferencesAuthor ->
            UserIntentsToUpdatePreferences preferencesAuthor

        TooltipMsg msgTooltip ->
            TooltipMsg msgTooltip

        AddToast content ->
            AddToast content

        AddToastExpireIn milliseconds content ->
            AddToastExpireIn milliseconds content

        Focus id ->
            Focus id

        RequestAuthorDetails ->
            RequestAuthorDetails

        UpdateTriples ->
            UpdateTriples

        GetGraph config onResponse iri ->
            GetGraph config (onResponse >> func) iri

        GetGraphTurtle onResponse iri ->
            GetGraphTurtle (onResponse >> func) iri

        GetGraphNTriples onResponse iri ->
            GetGraphNTriples (onResponse >> func) iri

        GetGraphJsonLd onResponse iri ->
            GetGraphJsonLd (onResponse >> func) iri

        GetInstances onResponse config ->
            GetInstances (onResponse >> func) config

        StoreTrig onResponse iri raw ->
            StoreTrig (onResponse >> func) iri raw

        Query onRows query ->
            Query (onRows >> func) query

        CopyToClipboard value ->
            CopyToClipboard value

        SelectWorkspace uuid ->
            SelectWorkspace uuid

        CreateField onField preview path mint field ->
            CreateField (onField >> func) preview path mint field

        DuplicateField onField preview path mint field ->
            DuplicateField (onField >> func) preview path mint field


toastFromError : C -> Api.Error -> Action msg
toastFromError c error =
    { message =
        error
            |> Api.errorToMessages
            |> Localize.verbatims c
            |> verbatim
    }
        |> AddToast
