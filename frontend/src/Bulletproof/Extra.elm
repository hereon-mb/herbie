{- Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
   a client-server based web application for research documentation

   Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)

   Authors: Fabian Kirchner, Catriona Eschke

   This program is subject to the terms and conditions for non-commercial use of
   ELN. You can find the license text in the file LICENSE.en and under
   http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
   you have any questions or comments, you can contact us at hereon at
   herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
   Max-Planck-Straße 1, 21502 Geesthacht, Germany.
-}


module Bulletproof.Extra exposing (fromElmUi, smallWidth)

import Bulletproof exposing (Renderer)
import Element
    exposing
        ( Element
        , el
        , fill
        , fillPortion
        , focusStyle
        , height
        , layoutWith
        , none
        , row
        , width
        )
import Element.Font as Font
import Html


fromElmUi : Element msg -> Renderer
fromElmUi element =
    Bulletproof.fromHtml
        (Html.node "fluent-provider"
            []
            [ layoutWith
                { options =
                    [ focusStyle
                        { borderColor = Nothing
                        , backgroundColor = Nothing
                        , shadow = Nothing
                        }
                    ]
                }
                [ width fill
                , height fill
                , Font.family
                    [ Font.typeface "Work Sans"
                    , Font.sansSerif
                    ]
                ]
                element
            ]
        )


smallWidth : Element msg -> Element msg
smallWidth element =
    row
        [ width fill ]
        [ el [ width fill ] element
        , el [ width (fillPortion 2) ] none
        ]
