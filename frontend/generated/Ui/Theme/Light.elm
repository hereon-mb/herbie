module Ui.Theme.Light exposing (..)

import Element


light :
    { primary : Element.Color
    , onPrimary : Element.Color
    , primaryContainer : Element.Color
    , onPrimaryContainer : Element.Color
    , secondary : Element.Color
    , onSecondary : Element.Color
    , secondaryContainer : Element.Color
    , onSecondaryContainer : Element.Color
    , tertiary : Element.Color
    , onTertiary : Element.Color
    , tertiaryContainer : Element.Color
    , onTertiaryContainer : Element.Color
    , hereonLila : Element.Color
    , onHereonLila : Element.Color
    , hereonLilaContainer : Element.Color
    , onHereonLilaContainer : Element.Color
    , hereonMittelblau : Element.Color
    , onHereonMittelblau : Element.Color
    , hereonMittelblauContainer : Element.Color
    , onHereonMittelblauContainer : Element.Color
    , hereonGruen : Element.Color
    , onHereonGruen : Element.Color
    , hereonGruenContainer : Element.Color
    , onHereonGruenContainer : Element.Color
    , hereonDunkelrot : Element.Color
    , onHereonDunkelrot : Element.Color
    , hereonDunkelrotContainer : Element.Color
    , onHereonDunkelrotContainer : Element.Color
    , hereonOrange : Element.Color
    , onHereonOrange : Element.Color
    , hereonOrangeContainer : Element.Color
    , onHereonOrangeContainer : Element.Color
    , hereonGelb : Element.Color
    , onHereonGelb : Element.Color
    , hereonGelbContainer : Element.Color
    , onHereonGelbContainer : Element.Color
    , error : Element.Color
    , onError : Element.Color
    , errorContainer : Element.Color
    , onErrorContainer : Element.Color
    , background : Element.Color
    , onBackground : Element.Color
    , surfaceDim : Element.Color
    , surface : Element.Color
    , surfaceBright : Element.Color
    , surfaceContainerLowest : Element.Color
    , surfaceContainerLow : Element.Color
    , surfaceContainer : Element.Color
    , surfaceContainerHigh : Element.Color
    , surfaceContainerHighest : Element.Color
    , onSurface : Element.Color
    , surfaceVariant : Element.Color
    , onSurfaceVariant : Element.Color
    , outline : Element.Color
    , outlineVariant : Element.Color
    , shadow : Element.Color
    , scrim : Element.Color
    , inverseSurface : Element.Color
    , inverseOnSurface : Element.Color
    , inversePrimary : Element.Color
    , primarySurfaceContainerHighest : Element.Color
    }
light =
    { primary = Element.rgb255 19 96 165
    , onPrimary = Element.rgb255 255 255 255
    , primaryContainer = Element.rgb255 211 228 255
    , onPrimaryContainer = Element.rgb255 0 28 56
    , secondary = Element.rgb255 185 0 100
    , onSecondary = Element.rgb255 255 255 255
    , secondaryContainer = Element.rgb255 255 217 226
    , onSecondaryContainer = Element.rgb255 62 0 30
    , tertiary = Element.rgb255 108 86 119
    , onTertiary = Element.rgb255 255 255 255
    , tertiaryContainer = Element.rgb255 244 217 255
    , onTertiaryContainer = Element.rgb255 0 0 0
    , hereonLila = Element.rgb255 102 78 172
    , onHereonLila = Element.rgb255 255 255 255
    , hereonLilaContainer = Element.rgb255 232 221 255
    , onHereonLilaContainer = Element.rgb255 33 0 94
    , hereonMittelblau = Element.rgb255 0 96 169
    , onHereonMittelblau = Element.rgb255 255 255 255
    , hereonMittelblauContainer = Element.rgb255 211 228 255
    , onHereonMittelblauContainer = Element.rgb255 0 28 56
    , hereonGruen = Element.rgb255 43 108 0
    , onHereonGruen = Element.rgb255 255 255 255
    , hereonGruenContainer = Element.rgb255 165 248 119
    , onHereonGruenContainer = Element.rgb255 8 33 0
    , hereonDunkelrot = Element.rgb255 179 26 103
    , onHereonDunkelrot = Element.rgb255 255 255 255
    , hereonDunkelrotContainer = Element.rgb255 255 217 227
    , onHereonDunkelrotContainer = Element.rgb255 62 0 31
    , hereonOrange = Element.rgb255 170 51 61
    , onHereonOrange = Element.rgb255 255 255 255
    , hereonOrangeContainer = Element.rgb255 255 218 217
    , onHereonOrangeContainer = Element.rgb255 64 0 9
    , hereonGelb = Element.rgb255 114 92 0
    , onHereonGelb = Element.rgb255 255 255 255
    , hereonGelbContainer = Element.rgb255 255 224 126
    , onHereonGelbContainer = Element.rgb255 35 27 0
    , error = Element.rgb255 186 26 26
    , onError = Element.rgb255 255 255 255
    , errorContainer = Element.rgb255 255 218 214
    , onErrorContainer = Element.rgb255 65 0 2
    , background = Element.rgb255 253 252 255
    , onBackground = Element.rgb255 26 28 30
    , surfaceDim = Element.rgb255 218 217 221
    , surface = Element.rgb255 253 252 255
    , surfaceBright = Element.rgb255 250 249 253
    , surfaceContainerLowest = Element.rgb255 255 255 255
    , surfaceContainerLow = Element.rgb255 244 243 247
    , surfaceContainer = Element.rgb255 238 237 241
    , surfaceContainerHigh = Element.rgb255 232 232 235
    , surfaceContainerHighest = Element.rgb255 227 226 230
    , onSurface = Element.rgb255 26 28 30
    , surfaceVariant = Element.rgb255 223 226 235
    , onSurfaceVariant = Element.rgb255 67 71 78
    , outline = Element.rgb255 115 119 127
    , outlineVariant = Element.rgb255 195 198 207
    , shadow = Element.rgb255 0 0 0
    , scrim = Element.rgb255 0 0 0
    , inverseSurface = Element.rgb255 47 48 51
    , inverseOnSurface = Element.rgb255 241 240 244
    , inversePrimary = Element.rgb255 162 201 255
    , primarySurfaceContainerHighest = Element.rgb255 211 228 255
    }