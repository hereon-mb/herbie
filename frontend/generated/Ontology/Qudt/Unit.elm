module Ontology.Qudt.Unit exposing (..)


urlToSymbol : String -> Maybe String
urlToSymbol url =
    case url of
        "http://qudt.org/vocab/unit/FUR_Long" ->
            Just "furlong{long}"

        "http://qudt.org/vocab/unit/HectoPA-M3-PER-SEC" ->
            Just "hPa⋅m³/s"

        "http://qudt.org/vocab/unit/DRAM_UK" ->
            Just "dr{UK}"

        "http://qudt.org/vocab/unit/CentiM2-PER-CentiM3" ->
            Just "cm²/cm³"

        "http://qudt.org/vocab/unit/GAUSS" ->
            Just "Gs"

        "http://qudt.org/vocab/unit/BREATH-PER-MIN" ->
            Just "breath/min"

        "http://qudt.org/vocab/unit/LB-PER-IN3" ->
            Just "lbm/in³"

        "http://qudt.org/vocab/unit/NanoMOL-PER-L" ->
            Just "nmol/L"

        "http://qudt.org/vocab/unit/KiloMOL-PER-SEC" ->
            Just "kmol/s"

        "http://qudt.org/vocab/unit/EV-SEC" ->
            Just "eV⋅s"

        "http://qudt.org/vocab/unit/J-PER-M3" ->
            Just "J/m³"

        "http://qudt.org/vocab/unit/CentiM3-PER-CentiM3" ->
            Just "cm³/cm³"

        "http://qudt.org/vocab/unit/GT" ->
            Just "G.T."

        "http://qudt.org/vocab/unit/MicroATM" ->
            Just "μatm"

        "http://qudt.org/vocab/unit/K-PER-M" ->
            Just "K/m"

        "http://qudt.org/vocab/unit/PER-MO" ->
            Just "/mo"

        "http://qudt.org/vocab/unit/PERMITTIVITY_REL" ->
            Just "εᵣ"

        "http://qudt.org/vocab/unit/M2-HZ" ->
            Just "m²⋅Hz"

        "http://qudt.org/vocab/unit/PSI-PER-PSI" ->
            Just "psi/psi"

        "http://qudt.org/vocab/unit/OHM-M" ->
            Just "Ω⋅m"

        "http://qudt.org/vocab/unit/CentiM3-PER-M3" ->
            Just "cm³/m³"

        "http://qudt.org/vocab/unit/SV" ->
            Just "Sv"

        "http://qudt.org/vocab/unit/Gamma" ->
            Just "γ"

        "http://qudt.org/vocab/unit/CAL_IT-PER-SEC-CentiM-K" ->
            Just "cal{IT}/(s⋅cm⋅K)"

        "http://qudt.org/vocab/unit/M-K-PER-W" ->
            Just "m⋅K/W"

        "http://qudt.org/vocab/unit/PPTM" ->
            Just "PPTM"

        "http://qudt.org/vocab/unit/PlanckPressure" ->
            Just "planckpressure"

        "http://qudt.org/vocab/unit/V-PER-M" ->
            Just "V/m"

        "http://qudt.org/vocab/unit/ATM-M3-PER-MOL" ->
            Just "atm⋅m³/mol"

        "http://qudt.org/vocab/unit/W-HR" ->
            Just "W⋅hr"

        "http://qudt.org/vocab/unit/PA-SEC-PER-M3" ->
            Just "Pa⋅s/m³"

        "http://qudt.org/vocab/unit/MicroBQ-PER-KiloGM" ->
            Just "μBq/kg"

        "http://qudt.org/vocab/unit/PERM_Metric" ->
            Just "perm{Metric}"

        "http://qudt.org/vocab/unit/M2-HZ3" ->
            Just "m²⋅Hz³"

        "http://qudt.org/vocab/unit/W-HR-PER-M2" ->
            Just "W⋅hr/m²"

        "http://qudt.org/vocab/unit/DEGREE_API" ->
            Just "°API"

        "http://qudt.org/vocab/unit/ZettaC" ->
            Just "ZC"

        "http://qudt.org/vocab/unit/EV-PER-ANGSTROM" ->
            Just "eV/Å"

        "http://qudt.org/vocab/unit/MilliMOL-PER-M3" ->
            Just "mmol/m³"

        "http://qudt.org/vocab/unit/HART" ->
            Just "Hart"

        "http://qudt.org/vocab/unit/CAL_TH-PER-GM-K" ->
            Just "cal/(g⋅K)"

        "http://qudt.org/vocab/unit/MicroM2" ->
            Just "μm²"

        "http://qudt.org/vocab/unit/Z" ->
            Just "Z"

        "http://qudt.org/vocab/unit/HART-PER-SEC" ->
            Just "Hart/s"

        "http://qudt.org/vocab/unit/MilliC-PER-M3" ->
            Just "mC/m³"

        "http://qudt.org/vocab/unit/OHM_Ab" ->
            Just "abΩ"

        "http://qudt.org/vocab/unit/GM-PER-CentiM3" ->
            Just "g/cm³"

        "http://qudt.org/vocab/unit/CAL_TH-PER-CentiM-SEC-DEG_C" ->
            Just "cal/(cm⋅s⋅°C)"

        "http://qudt.org/vocab/unit/ANGSTROM3" ->
            Just "Å³"

        "http://qudt.org/vocab/unit/MicroMOL-PER-KiloGM" ->
            Just "µmol/kg"

        "http://qudt.org/vocab/unit/TOE" ->
            Just "toe"

        "http://qudt.org/vocab/unit/PARSEC" ->
            Just "pc"

        "http://qudt.org/vocab/unit/PlanckTemperature" ->
            Just "plancktemperature"

        "http://qudt.org/vocab/unit/DEG" ->
            Just "°"

        "http://qudt.org/vocab/unit/MilliA" ->
            Just "mA"

        "http://qudt.org/vocab/unit/OZ_VOL_US-PER-HR" ->
            Just "fl oz{US}/hr"

        "http://qudt.org/vocab/unit/FT-PER-MIN" ->
            Just "ft/min"

        "http://qudt.org/vocab/unit/M3-PER-HR" ->
            Just "m³/hr"

        "http://qudt.org/vocab/unit/BIT" ->
            Just "b"

        "http://qudt.org/vocab/unit/GigaBIT-PER-SEC" ->
            Just "Gbps"

        "http://qudt.org/vocab/unit/S" ->
            Just "S"

        "http://qudt.org/vocab/unit/FRAME-PER-SEC" ->
            Just "fps"

        "http://qudt.org/vocab/unit/PER-CentiM3" ->
            Just "/cm³"

        "http://qudt.org/vocab/unit/FT3-PER-DAY" ->
            Just "ft³/day"

        "http://qudt.org/vocab/unit/M3-PER-M2" ->
            Just "m³/m²"

        "http://qudt.org/vocab/unit/A" ->
            Just "A"

        "http://qudt.org/vocab/unit/W-PER-CentiM2" ->
            Just "W/cm²"

        "http://qudt.org/vocab/unit/KiloC-PER-M3" ->
            Just "kC/m³"

        "http://qudt.org/vocab/unit/DEGREE_TWADDELL" ->
            Just "°Tw"

        "http://qudt.org/vocab/unit/MicroSV" ->
            Just "µSv"

        "http://qudt.org/vocab/unit/J-PER-MOL" ->
            Just "J/mol"

        "http://qudt.org/vocab/unit/FARAD_Stat" ->
            Just "statF"

        "http://qudt.org/vocab/unit/FT3-PER-DEG_F" ->
            Just "ft³/°F"

        "http://qudt.org/vocab/unit/MilliMOL-PER-L" ->
            Just "mmol/L"

        "http://qudt.org/vocab/unit/HectoPA" ->
            Just "hPa"

        "http://qudt.org/vocab/unit/DEGREE_PLATO" ->
            Just "°P"

        "http://qudt.org/vocab/unit/M3-PER-KiloGM" ->
            Just "m³/kg"

        "http://qudt.org/vocab/unit/N-M-SEC" ->
            Just "N⋅m⋅s"

        "http://qudt.org/vocab/unit/BIT-PER-SEC" ->
            Just "b/s"

        "http://qudt.org/vocab/unit/AMU" ->
            Just "amu"

        "http://qudt.org/vocab/unit/KiloGM-PER-J" ->
            Just "kg/J"

        "http://qudt.org/vocab/unit/H" ->
            Just "H"

        "http://qudt.org/vocab/unit/W-PER-M" ->
            Just "W/m"

        "http://qudt.org/vocab/unit/LB_F-SEC-PER-IN2" ->
            Just "lbf⋅s/in²"

        "http://qudt.org/vocab/unit/FT-LB_F-SEC" ->
            Just "ft⋅lbf⋅s"

        "http://qudt.org/vocab/unit/GAL_UK-PER-HR" ->
            Just "gal{UK}/hr"

        "http://qudt.org/vocab/unit/RAD-M2-PER-MOL" ->
            Just "rad⋅m²/mol"

        "http://qudt.org/vocab/unit/A-M2" ->
            Just "A⋅m²"

        "http://qudt.org/vocab/unit/GM-PER-SEC" ->
            Just "g/s"

        "http://qudt.org/vocab/unit/LB-PER-DAY" ->
            Just "lbm/day"

        "http://qudt.org/vocab/unit/MicroMOL-PER-GM-HR" ->
            Just "µmol/(g⋅hr)"

        "http://qudt.org/vocab/unit/A-PER-M" ->
            Just "A/m"

        "http://qudt.org/vocab/unit/C-PER-CentiM3" ->
            Just "C/cm³"

        "http://qudt.org/vocab/unit/RAD-PER-MIN" ->
            Just "rad/min"

        "http://qudt.org/vocab/unit/DeciM3-PER-HR" ->
            Just "dm³/hr"

        "http://qudt.org/vocab/unit/DEG_C-PER-HR" ->
            Just "°C/hr"

        "http://qudt.org/vocab/unit/BBL_US_PET-PER-SEC" ->
            Just "bbl{UK petroleum}/s"

        "http://qudt.org/vocab/unit/MegaJ-PER-KiloGM" ->
            Just "MJ/kg"

        "http://qudt.org/vocab/unit/KiloGM-PER-CentiM3" ->
            Just "kg/cm³"

        "http://qudt.org/vocab/unit/DEGREE_BAUME_US_HEAVY" ->
            Just "°Bé{US Heavy}"

        "http://qudt.org/vocab/unit/E_h" ->
            Just "Ha"

        "http://qudt.org/vocab/unit/S-PER-M" ->
            Just "S/m"

        "http://qudt.org/vocab/unit/NUM-PER-MicroL" ->
            Just "/µL"

        "http://qudt.org/vocab/unit/MilliPA-SEC" ->
            Just "mPa⋅s"

        "http://qudt.org/vocab/unit/LB-PER-SEC" ->
            Just "lbm/s"

        "http://qudt.org/vocab/unit/A-PER-DEG_C" ->
            Just "A/°C"

        "http://qudt.org/vocab/unit/NanoH" ->
            Just "nH"

        "http://qudt.org/vocab/unit/QT_US_DRY" ->
            Just "qt{US Dry}"

        "http://qudt.org/vocab/unit/KiloOHM" ->
            Just "kΩ"

        "http://qudt.org/vocab/unit/NanoGM-PER-L" ->
            Just "ng/L"

        "http://qudt.org/vocab/unit/MilliIN" ->
            Just "mil"

        "http://qudt.org/vocab/unit/IU-PER-L" ->
            Just "IU/L"

        "http://qudt.org/vocab/unit/KiloGM-PER-M2-PA-SEC" ->
            Just "kg/(m²⋅Pa⋅s)"

        "http://qudt.org/vocab/unit/M-PER-SEC2" ->
            Just "m/s²"

        "http://qudt.org/vocab/unit/MilliC-PER-KiloGM" ->
            Just "mC/kg"

        "http://qudt.org/vocab/unit/KiloCAL-PER-GM" ->
            Just "kcal/g"

        "http://qudt.org/vocab/unit/M-PER-K" ->
            Just "m/K"

        "http://qudt.org/vocab/unit/TONNE-PER-M3" ->
            Just "t/m³"

        "http://qudt.org/vocab/unit/M2-K-PER-W" ->
            Just "m²⋅K/W"

        "http://qudt.org/vocab/unit/MilliL-PER-CentiM2-SEC" ->
            Just "mL/(cm²⋅s)"

        "http://qudt.org/vocab/unit/T-SEC" ->
            Just "T⋅s"

        "http://qudt.org/vocab/unit/W-PER-M2-PA" ->
            Just "W/(m²⋅Pa)"

        "http://qudt.org/vocab/unit/PSI-M3-PER-SEC" ->
            Just "psi⋅m³/s"

        "http://qudt.org/vocab/unit/FBM" ->
            Just "BDFT"

        "http://qudt.org/vocab/unit/CAL_IT-PER-GM-DEG_C" ->
            Just "cal{IT}/(g⋅°C)"

        "http://qudt.org/vocab/unit/KiloN-M2" ->
            Just "kN⋅m²"

        "http://qudt.org/vocab/unit/PicoS" ->
            Just "pS"

        "http://qudt.org/vocab/unit/PicoPA-PER-KiloM" ->
            Just "pPa/km"

        "http://qudt.org/vocab/unit/V-A_Reactive-HR" ->
            Just "V⋅A{reactive}⋅hr"

        "http://qudt.org/vocab/unit/DecaM3" ->
            Just "dam³"

        "http://qudt.org/vocab/unit/UNITLESS" ->
            Just "一"

        "http://qudt.org/vocab/unit/BYTE" ->
            Just "B"

        "http://qudt.org/vocab/unit/BBL_US_PET-PER-HR" ->
            Just "bbl{UK petroleum}/hr"

        "http://qudt.org/vocab/unit/MicroM-PER-K" ->
            Just "µm/K"

        "http://qudt.org/vocab/unit/PPTM-PER-K" ->
            Just "PPTM/K"

        "http://qudt.org/vocab/unit/MicroRAD" ->
            Just "µrad"

        "http://qudt.org/vocab/unit/MilliDARCY" ->
            Just "md"

        "http://qudt.org/vocab/unit/LB-IN" ->
            Just "lbm⋅in"

        "http://qudt.org/vocab/unit/PER-YD3" ->
            Just "/yd³"

        "http://qudt.org/vocab/unit/OHM" ->
            Just "Ω"

        "http://qudt.org/vocab/unit/WB" ->
            Just "Wb"

        "http://qudt.org/vocab/unit/TON_SHORT-PER-YD3" ->
            Just "ton{short}/yd³"

        "http://qudt.org/vocab/unit/TeraW-HR" ->
            Just "TW⋅hr"

        "http://qudt.org/vocab/unit/DecaPA" ->
            Just "daPa"

        "http://qudt.org/vocab/unit/EUR-PER-M2" ->
            Just "€/m²"

        "http://qudt.org/vocab/unit/GigaEV" ->
            Just "GeV"

        "http://qudt.org/vocab/unit/PA" ->
            Just "Pa"

        "http://qudt.org/vocab/unit/FT-LB_F-PER-SEC" ->
            Just "ft⋅lbf/s"

        "http://qudt.org/vocab/unit/DIOPTER" ->
            Just "D"

        "http://qudt.org/vocab/unit/PER-PA-SEC" ->
            Just "/(Pa⋅s)"

        "http://qudt.org/vocab/unit/SLUG" ->
            Just "slug"

        "http://qudt.org/vocab/unit/DeciN-M" ->
            Just "dN⋅m"

        "http://qudt.org/vocab/unit/MilliW-PER-M2" ->
            Just "mW/m²"

        "http://qudt.org/vocab/unit/LunarMass" ->
            Just "M☾"

        "http://qudt.org/vocab/unit/NP" ->
            Just "Np"

        "http://qudt.org/vocab/unit/BTU_IT-FT-PER-FT2-HR-DEG_F" ->
            Just "Btu{IT}⋅ft/(ft²⋅hr⋅°F)"

        "http://qudt.org/vocab/unit/FT_HG" ->
            Just "ftHg"

        "http://qudt.org/vocab/unit/LB-PER-FT-SEC" ->
            Just "lbm/(ft⋅s)"

        "http://qudt.org/vocab/unit/MilliGM-PER-M3-HR" ->
            Just "mg/(m³⋅hr)"

        "http://qudt.org/vocab/unit/PA-PER-SEC" ->
            Just "Pa/s"

        "http://qudt.org/vocab/unit/IN-PER-SEC2" ->
            Just "in/s²"

        "http://qudt.org/vocab/unit/YD" ->
            Just "yd"

        "http://qudt.org/vocab/unit/PT" ->
            Just "pt"

        "http://qudt.org/vocab/unit/FT3-PER-MIN-FT2" ->
            Just "ft³/(min⋅ft²)"

        "http://qudt.org/vocab/unit/V-PER-IN" ->
            Just "V/in"

        "http://qudt.org/vocab/unit/DYN-PER-CentiM2" ->
            Just "dyn/cm²"

        "http://qudt.org/vocab/unit/LUX-HR" ->
            Just "lx⋅hr"

        "http://qudt.org/vocab/unit/DeciN" ->
            Just "dN"

        "http://qudt.org/vocab/unit/CWT_LONG" ->
            Just "cwt{long}"

        "http://qudt.org/vocab/unit/MegaEV" ->
            Just "MeV"

        "http://qudt.org/vocab/unit/KiloN-M" ->
            Just "kN⋅m"

        "http://qudt.org/vocab/unit/C-PER-M3" ->
            Just "C/m³"

        "http://qudt.org/vocab/unit/KiloBTU_IT" ->
            Just "kBtu{IT}"

        "http://qudt.org/vocab/unit/GRAY-PER-SEC" ->
            Just "Gy/s"

        "http://qudt.org/vocab/unit/OZ_VOL_US" ->
            Just "fl oz{US}"

        "http://qudt.org/vocab/unit/W-PER-M2" ->
            Just "W/m²"

        "http://qudt.org/vocab/unit/KiloBTU_IT-PER-FT2" ->
            Just "kBtu{IT}/ft²"

        "http://qudt.org/vocab/unit/IN2" ->
            Just "in²"

        "http://qudt.org/vocab/unit/M2-PER-J" ->
            Just "m²/J"

        "http://qudt.org/vocab/unit/MegaN" ->
            Just "MN"

        "http://qudt.org/vocab/unit/MegaJ-PER-SEC" ->
            Just "MJ/s"

        "http://qudt.org/vocab/unit/TON_UK-PER-DAY" ->
            Just "ton{UK}/day"

        "http://qudt.org/vocab/unit/LA" ->
            Just "L"

        "http://qudt.org/vocab/unit/GM-PER-M3" ->
            Just "g/m³"

        "http://qudt.org/vocab/unit/V_Stat-CentiM" ->
            Just "statV⋅cm"

        "http://qudt.org/vocab/unit/V-PER-MicroSEC" ->
            Just "V/µs"

        "http://qudt.org/vocab/unit/MilliJ-PER-GM" ->
            Just "mJ/g"

        "http://qudt.org/vocab/unit/PK_US_DRY" ->
            Just "peck{US Dry}"

        "http://qudt.org/vocab/unit/CASES-PER-1000I-YR" ->
            Just "Cases/1000 individuals/year"

        "http://qudt.org/vocab/unit/MebiBYTE" ->
            Just "MiB"

        "http://qudt.org/vocab/unit/ExaBYTE" ->
            Just "EB"

        "http://qudt.org/vocab/unit/FemtoGM" ->
            Just "fg"

        "http://qudt.org/vocab/unit/T_Ab" ->
            Just "abT"

        "http://qudt.org/vocab/unit/KiloGM" ->
            Just "kg"

        "http://qudt.org/vocab/unit/W-PER-M-K" ->
            Just "W/(m⋅K)"

        "http://qudt.org/vocab/unit/PER-GM" ->
            Just "/g"

        "http://qudt.org/vocab/unit/KiloBIT-PER-SEC" ->
            Just "kbps"

        "http://qudt.org/vocab/unit/C-PER-KiloGM-SEC" ->
            Just "C/(kg⋅s)"

        "http://qudt.org/vocab/unit/DeciBAR" ->
            Just "dbar"

        "http://qudt.org/vocab/unit/N-M-PER-DEG" ->
            Just "N⋅m/°"

        "http://qudt.org/vocab/unit/MicroG" ->
            Just "μG"

        "http://qudt.org/vocab/unit/V_Ab" ->
            Just "abV"

        "http://qudt.org/vocab/unit/M2-PER-HZ2" ->
            Just "m²/Hz²"

        "http://qudt.org/vocab/unit/T" ->
            Just "T"

        "http://qudt.org/vocab/unit/NanoH-PER-M" ->
            Just "nH/m"

        "http://qudt.org/vocab/unit/BQ" ->
            Just "Bq"

        "http://qudt.org/vocab/unit/Da" ->
            Just "Da"

        "http://qudt.org/vocab/unit/MegaJ" ->
            Just "MJ"

        "http://qudt.org/vocab/unit/MilliDEG_C" ->
            Just "m°C"

        "http://qudt.org/vocab/unit/SEC" ->
            Just "s"

        "http://qudt.org/vocab/unit/FemtoMOL-PER-KiloGM" ->
            Just "fmol/kg"

        "http://qudt.org/vocab/unit/IN-PER-MIN" ->
            Just "in/min"

        "http://qudt.org/vocab/unit/KiloW" ->
            Just "kW"

        "http://qudt.org/vocab/unit/J-PER-KiloGM-K" ->
            Just "J/(kg⋅K)"

        "http://qudt.org/vocab/unit/NanoC" ->
            Just "nC"

        "http://qudt.org/vocab/unit/M3-PER-SEC" ->
            Just "m³/s"

        "http://qudt.org/vocab/unit/MilliMOL-PER-MOL" ->
            Just "mmol/mol"

        "http://qudt.org/vocab/unit/PER-M-K" ->
            Just "/(m⋅K)"

        "http://qudt.org/vocab/unit/MHO_Stat" ->
            Just "stat℧"

        "http://qudt.org/vocab/unit/NUM-PER-CentiM-KiloYR" ->
            Just "/(cm⋅1000 yr)"

        "http://qudt.org/vocab/unit/YD3" ->
            Just "yd³"

        "http://qudt.org/vocab/unit/RAD_R" ->
            Just "rad"

        "http://qudt.org/vocab/unit/MilliPA-SEC-PER-BAR" ->
            Just "mPa⋅s/bar"

        "http://qudt.org/vocab/unit/CAL_15_DEG_C" ->
            Just "cal{15 °C}"

        "http://qudt.org/vocab/unit/GRAIN-PER-GAL_US" ->
            Just "gr{UK}/gal{US}"

        "http://qudt.org/vocab/unit/M2-HR-DEG_C-PER-KiloCAL_IT" ->
            Just "m²⋅hr⋅°C/kcal{IT}"

        "http://qudt.org/vocab/unit/MicroM-PER-MilliL" ->
            Just "µm/mL"

        "http://qudt.org/vocab/unit/MilliMOL-PER-M2-DAY" ->
            Just "mmol/(m²⋅day)"

        "http://qudt.org/vocab/unit/QT_UK-PER-DAY" ->
            Just "qt{UK}/day"

        "http://qudt.org/vocab/unit/M2-PER-GM" ->
            Just "m²/g"

        "http://qudt.org/vocab/unit/TeraOHM" ->
            Just "TΩ"

        "http://qudt.org/vocab/unit/M2-PER-SEC" ->
            Just "m²/s"

        "http://qudt.org/vocab/unit/TON_US-PER-HR" ->
            Just "ton{US}/hr"

        "http://qudt.org/vocab/unit/BTU_IT-PER-LB-MOL" ->
            Just "Btu{IT}/(mol-lb)"

        "http://qudt.org/vocab/unit/FT2-HR-DEG_F-PER-BTU_IT" ->
            Just "ft²⋅hr⋅°F/Btu{IT}"

        "http://qudt.org/vocab/unit/PER-M-SEC" ->
            Just "/(m⋅s)"

        "http://qudt.org/vocab/unit/RAD-PER-SEC" ->
            Just "rad/s"

        "http://qudt.org/vocab/unit/DEG_C2-PER-SEC" ->
            Just "°C²/s"

        "http://qudt.org/vocab/unit/M2-PER-HZ" ->
            Just "m²/Hz"

        "http://qudt.org/vocab/unit/N" ->
            Just "N"

        "http://qudt.org/vocab/unit/KiloGM-PER-MegaBTU_IT" ->
            Just "kg/MBtu{IT}"

        "http://qudt.org/vocab/unit/DYN" ->
            Just "dyn"

        "http://qudt.org/vocab/unit/BTU_IT-FT" ->
            Just "Btu{IT}⋅ft"

        "http://qudt.org/vocab/unit/SR" ->
            Just "sr"

        "http://qudt.org/vocab/unit/BBL_UK_PET-PER-MIN" ->
            Just "bbl{UK petroleum}/min"

        "http://qudt.org/vocab/unit/TON_Metric" ->
            Just "t"

        "http://qudt.org/vocab/unit/PlanckTime" ->
            Just "tₚ"

        "http://qudt.org/vocab/unit/M-PER-MIN" ->
            Just "m/min"

        "http://qudt.org/vocab/unit/PER-M-NanoM-SR" ->
            Just "/(m⋅nm⋅sr)"

        "http://qudt.org/vocab/unit/MicroG-PER-CentiM2" ->
            Just "µg/cm²"

        "http://qudt.org/vocab/unit/M4-PER-SEC" ->
            Just "m⁴/s"

        "http://qudt.org/vocab/unit/BTU_TH-PER-MIN" ->
            Just "Btu{th}/min"

        "http://qudt.org/vocab/unit/NanoS-PER-CentiM" ->
            Just "nS/cm"

        "http://qudt.org/vocab/unit/FT3-PER-HR" ->
            Just "ft³/hr"

        "http://qudt.org/vocab/unit/DeciSEC" ->
            Just "ds"

        "http://qudt.org/vocab/unit/OZ_F-IN" ->
            Just "ozf⋅in"

        "http://qudt.org/vocab/unit/CentiM3-PER-K" ->
            Just "cm³/K"

        "http://qudt.org/vocab/unit/MilliL-PER-M3" ->
            Just "mL/m³"

        "http://qudt.org/vocab/unit/K-DAY" ->
            Just "K⋅day"

        "http://qudt.org/vocab/unit/CentiM-PER-SEC2" ->
            Just "cm/s²"

        "http://qudt.org/vocab/unit/PER-PicoM" ->
            Just "/pm"

        "http://qudt.org/vocab/unit/DeciC" ->
            Just "dC"

        "http://qudt.org/vocab/unit/KiloL" ->
            Just "kL"

        "http://qudt.org/vocab/unit/TON_Metric-PER-MIN" ->
            Just "t/min"

        "http://qudt.org/vocab/unit/MilliKAT-PER-L" ->
            Just "mkat/L"

        "http://qudt.org/vocab/unit/PER-KiloGM2" ->
            Just "/kg²"

        "http://qudt.org/vocab/unit/HZ-PER-K" ->
            Just "Hz/K"

        "http://qudt.org/vocab/unit/SUSCEPTIBILITY_MAG" ->
            Just "χ"

        "http://qudt.org/vocab/unit/MilliV-PER-MIN" ->
            Just "mV/min"

        "http://qudt.org/vocab/unit/PA-M0pt5" ->
            Just "Pa√m"

        "http://qudt.org/vocab/unit/LB-PER-FT-HR" ->
            Just "lbm/(ft⋅hr)"

        "http://qudt.org/vocab/unit/MicroMOL-PER-L" ->
            Just "µmol/L"

        "http://qudt.org/vocab/unit/MilliGM-PER-M2" ->
            Just "mg/m²"

        "http://qudt.org/vocab/unit/MicroMOL-PER-L-HR" ->
            Just "µmol/(L⋅hr)"

        "http://qudt.org/vocab/unit/KiloPOND" ->
            Just "kp"

        "http://qudt.org/vocab/unit/CentiM3-PER-MIN" ->
            Just "cm³/min"

        "http://qudt.org/vocab/unit/CUP" ->
            Just "cup"

        "http://qudt.org/vocab/unit/MegaV" ->
            Just "MV"

        "http://qudt.org/vocab/unit/BTU_IT-PER-LB-DEG_R" ->
            Just "Btu{IT}/(lbm⋅°R)"

        "http://qudt.org/vocab/unit/DeciS-PER-M" ->
            Just "dS/m"

        "http://qudt.org/vocab/unit/MilliGM-PER-MilliL" ->
            Just "mg/mL"

        "http://qudt.org/vocab/unit/SHANNON" ->
            Just "Sh"

        "http://qudt.org/vocab/unit/HZ-PER-T" ->
            Just "Hz/T"

        "http://qudt.org/vocab/unit/CAL_TH-PER-SEC-CentiM2-K" ->
            Just "cal/(s⋅cm²⋅K)"

        "http://qudt.org/vocab/unit/YD-PER-DEG_F" ->
            Just "yd/°F"

        "http://qudt.org/vocab/unit/FT-LB_F-PER-MIN" ->
            Just "ft⋅lbf/min"

        "http://qudt.org/vocab/unit/K-SEC" ->
            Just "K⋅s"

        "http://qudt.org/vocab/unit/MicroGM-PER-IN2" ->
            Just "μg/in²"

        "http://qudt.org/vocab/unit/PicoMOL-PER-KiloGM" ->
            Just "pmol/kg"

        "http://qudt.org/vocab/unit/GigaHZ" ->
            Just "GHz"

        "http://qudt.org/vocab/unit/MilliG" ->
            Just "mG"

        "http://qudt.org/vocab/unit/PPM-PER-K" ->
            Just "PPM/K"

        "http://qudt.org/vocab/unit/DEG_R-PER-SEC" ->
            Just "°R/s"

        "http://qudt.org/vocab/unit/LB_F-PER-IN2-SEC" ->
            Just "lbf/(in²⋅s)"

        "http://qudt.org/vocab/unit/H-PER-M" ->
            Just "H/m"

        "http://qudt.org/vocab/unit/EUR-PER-KiloW-HR" ->
            Just "€/(kW⋅hr)"

        "http://qudt.org/vocab/unit/OZ-PER-GAL" ->
            Just "oz/gal{US}"

        "http://qudt.org/vocab/unit/KiloCAL_TH" ->
            Just "kcal"

        "http://qudt.org/vocab/unit/NanoGM-PER-MilliL" ->
            Just "ng/mL"

        "http://qudt.org/vocab/unit/K-PA-PER-SEC" ->
            Just "K⋅Pa/s"

        "http://qudt.org/vocab/unit/GM-PER-GM" ->
            Just "g/g"

        "http://qudt.org/vocab/unit/KiloW-HR" ->
            Just "kW⋅hr"

        "http://qudt.org/vocab/unit/W-M2-PER-SR" ->
            Just "W⋅m²/sr"

        "http://qudt.org/vocab/unit/N-M" ->
            Just "N⋅m"

        "http://qudt.org/vocab/unit/MilliL-PER-MIN" ->
            Just "mL/min"

        "http://qudt.org/vocab/unit/DeciS" ->
            Just "dS"

        "http://qudt.org/vocab/unit/CLO" ->
            Just "clo"

        "http://qudt.org/vocab/unit/G" ->
            Just "G"

        "http://qudt.org/vocab/unit/MicroOHM" ->
            Just "μΩ"

        "http://qudt.org/vocab/unit/MI-PER-HR" ->
            Just "mi/hr"

        "http://qudt.org/vocab/unit/FT-LB_F-PER-FT2-SEC" ->
            Just "ft⋅lbf/(ft²⋅s)"

        "http://qudt.org/vocab/unit/MilliGM-PER-M3-DAY" ->
            Just "mg/(m³⋅day)"

        "http://qudt.org/vocab/unit/GAL_UK-PER-MIN" ->
            Just "gal{UK}/min"

        "http://qudt.org/vocab/unit/BU_UK-PER-MIN" ->
            Just "bsh{UK}/min"

        "http://qudt.org/vocab/unit/GM-PER-KiloM" ->
            Just "g/km"

        "http://qudt.org/vocab/unit/KiloGM-PER-M-SEC2" ->
            Just "kg/(m⋅s²)"

        "http://qudt.org/vocab/unit/AT-PER-IN" ->
            Just "AT/in"

        "http://qudt.org/vocab/unit/M2-SEC-PER-RAD" ->
            Just "m²⋅s/rad"

        "http://qudt.org/vocab/unit/CD" ->
            Just "cd"

        "http://qudt.org/vocab/unit/YoctoC" ->
            Just "yC"

        "http://qudt.org/vocab/unit/PER-YR" ->
            Just "/yr"

        "http://qudt.org/vocab/unit/KiloMOL" ->
            Just "kmol"

        "http://qudt.org/vocab/unit/NanoFARAD-PER-M" ->
            Just "nF/m"

        "http://qudt.org/vocab/unit/KiloN" ->
            Just "kN"

        "http://qudt.org/vocab/unit/KiloR" ->
            Just "kR"

        "http://qudt.org/vocab/unit/W-PER-M2-M" ->
            Just "W/(m²⋅m)"

        "http://qudt.org/vocab/unit/MicroPOISE" ->
            Just "μP"

        "http://qudt.org/vocab/unit/POISE-PER-BAR" ->
            Just "P/bar"

        "http://qudt.org/vocab/unit/DAY" ->
            Just "day"

        "http://qudt.org/vocab/unit/NanoL" ->
            Just "nL"

        "http://qudt.org/vocab/unit/A-PER-M2" ->
            Just "A/m²"

        "http://qudt.org/vocab/unit/DEG_R-PER-HR" ->
            Just "°R/hr"

        "http://qudt.org/vocab/unit/LB-PER-GAL_UK" ->
            Just "lbm/gal{UK}"

        "http://qudt.org/vocab/unit/MicroTORR" ->
            Just "µTorr"

        "http://qudt.org/vocab/unit/LB-MOL" ->
            Just "lb-mol"

        "http://qudt.org/vocab/unit/MicroM3-PER-MilliL" ->
            Just "µm³/mL"

        "http://qudt.org/vocab/unit/WK" ->
            Just "wk"

        "http://qudt.org/vocab/unit/BBL" ->
            Just "bbl"

        "http://qudt.org/vocab/unit/HA" ->
            Just "ha"

        "http://qudt.org/vocab/unit/PER-MILLE-PER-PSI" ->
            Just "/ksi"

        "http://qudt.org/vocab/unit/KiloWB" ->
            Just "kWb"

        "http://qudt.org/vocab/unit/KiloGM-PER-HR" ->
            Just "kg/hr"

        "http://qudt.org/vocab/unit/MilliH-PER-OHM" ->
            Just "mH/Ω"

        "http://qudt.org/vocab/unit/OHM-M2-PER-M" ->
            Just "Ω⋅m²/m"

        "http://qudt.org/vocab/unit/PERMEABILITY_EM_REL" ->
            Just "μₜ"

        "http://qudt.org/vocab/unit/PA-M3-PER-SEC" ->
            Just "Pa⋅m³/s"

        "http://qudt.org/vocab/unit/C-M" ->
            Just "C⋅m"

        "http://qudt.org/vocab/unit/V-SEC-PER-M" ->
            Just "V⋅s/m"

        "http://qudt.org/vocab/unit/DEG-PER-MIN" ->
            Just "°/min"

        "http://qudt.org/vocab/unit/M3-PER-HA" ->
            Just "m³/ha"

        "http://qudt.org/vocab/unit/PPTR" ->
            Just "PPTR"

        "http://qudt.org/vocab/unit/BTU_IT-PER-FT2-SEC-DEG_F" ->
            Just "Btu{IT}/(ft²⋅s⋅°F)"

        "http://qudt.org/vocab/unit/GAL_US-PER-MIN" ->
            Just "gal{US}/min"

        "http://qudt.org/vocab/unit/KiloGM-PER-KiloGM" ->
            Just "kg/kg"

        "http://qudt.org/vocab/unit/REV" ->
            Just "rev"

        "http://qudt.org/vocab/unit/PER-IN3" ->
            Just "/in³"

        "http://qudt.org/vocab/unit/LB" ->
            Just "lbm"

        "http://qudt.org/vocab/unit/CAL_IT" ->
            Just "cal{IT}"

        "http://qudt.org/vocab/unit/KiloCAL_IT-PER-HR-M-DEG_C" ->
            Just "kcal{IT}/(hr⋅m⋅°C)"

        "http://qudt.org/vocab/unit/PER-SEC-SR" ->
            Just "/(s⋅sr)"

        "http://qudt.org/vocab/unit/CentiM3-PER-DAY" ->
            Just "cm³/day"

        "http://qudt.org/vocab/unit/MicroMOL-PER-M2" ->
            Just "µmol/m²"

        "http://qudt.org/vocab/unit/L-PER-HR" ->
            Just "L/hr"

        "http://qudt.org/vocab/unit/PA-SEC-PER-BAR" ->
            Just "Pa⋅s/bar"

        "http://qudt.org/vocab/unit/NUM-PER-M3" ->
            Just "/m³"

        "http://qudt.org/vocab/unit/OERSTED-CentiM" ->
            Just "Oe⋅cm"

        "http://qudt.org/vocab/unit/PlanckVolume" ->
            Just "l³ₚ"

        "http://qudt.org/vocab/unit/MilliFARAD" ->
            Just "mF"

        "http://qudt.org/vocab/unit/BAR-L-PER-SEC" ->
            Just "bar⋅L/s"

        "http://qudt.org/vocab/unit/MOL-PER-M2-SEC-M-SR" ->
            Just "mol/(m²⋅s⋅m⋅sr)"

        "http://qudt.org/vocab/unit/FemtoMOL-PER-L" ->
            Just "fmol/L"

        "http://qudt.org/vocab/unit/C-PER-MOL" ->
            Just "C/mol"

        "http://qudt.org/vocab/unit/NAT-PER-SEC" ->
            Just "nat/s"

        "http://qudt.org/vocab/unit/MilliGM" ->
            Just "mg"

        "http://qudt.org/vocab/unit/MOL-PER-MIN" ->
            Just "mol/min"

        "http://qudt.org/vocab/unit/GigaW-HR" ->
            Just "GW⋅hr"

        "http://qudt.org/vocab/unit/KiloGM-PER-FT2" ->
            Just "kg/ft²"

        "http://qudt.org/vocab/unit/J-M2" ->
            Just "J⋅m²"

        "http://qudt.org/vocab/unit/NUM-PER-HA" ->
            Just "/ha"

        "http://qudt.org/vocab/unit/CAL_TH-PER-SEC" ->
            Just "cal/s"

        "http://qudt.org/vocab/unit/KIP_F" ->
            Just "kip"

        "http://qudt.org/vocab/unit/KiloCAL" ->
            Just "kcal"

        "http://qudt.org/vocab/unit/CentiST" ->
            Just "cSt"

        "http://qudt.org/vocab/unit/GAL_US_DRY" ->
            Just "gal{US Dry}"

        "http://qudt.org/vocab/unit/CP" ->
            Just "cp"

        "http://qudt.org/vocab/unit/NanoSEC" ->
            Just "ns"

        "http://qudt.org/vocab/unit/PER-DAY" ->
            Just "/day"

        "http://qudt.org/vocab/unit/MilliL-PER-L" ->
            Just "mL/L"

        "http://qudt.org/vocab/unit/GAL_US-PER-HR" ->
            Just "gal{US}/hr"

        "http://qudt.org/vocab/unit/MilliA-PER-MilliM" ->
            Just "mA/mm"

        "http://qudt.org/vocab/unit/DEG-PER-SEC2" ->
            Just "°/s²"

        "http://qudt.org/vocab/unit/MOL-PER-MOL" ->
            Just "mol/mol"

        "http://qudt.org/vocab/unit/PDL-PER-FT2" ->
            Just "pdl/ft²"

        "http://qudt.org/vocab/unit/MOL-PER-M2-SEC-M" ->
            Just "mol/(m²⋅s⋅m)"

        "http://qudt.org/vocab/unit/MI_N-PER-HR" ->
            Just "nmi/hr"

        "http://qudt.org/vocab/unit/NanoMOL-PER-M2-DAY" ->
            Just "nmol/(m²⋅day)"

        "http://qudt.org/vocab/unit/KiloA-PER-M" ->
            Just "kA/m"

        "http://qudt.org/vocab/unit/PERM_US" ->
            Just "perm{US}"

        "http://qudt.org/vocab/unit/DeciB_C" ->
            Just "dBc"

        "http://qudt.org/vocab/unit/MegaHZ-PER-K" ->
            Just "MHz/K"

        "http://qudt.org/vocab/unit/DEG-PER-M" ->
            Just "°/m"

        "http://qudt.org/vocab/unit/PER-J-M3" ->
            Just "/(J⋅m³)"

        "http://qudt.org/vocab/unit/MIN_Sidereal" ->
            Just "min{sidereal}"

        "http://qudt.org/vocab/unit/J-PER-M2" ->
            Just "J/m²"

        "http://qudt.org/vocab/unit/FT-PER-HR" ->
            Just "ft/hr"

        "http://qudt.org/vocab/unit/DEG_C-PER-YR" ->
            Just "°C/yr"

        "http://qudt.org/vocab/unit/MegaJ-PER-M2" ->
            Just "MJ/m²"

        "http://qudt.org/vocab/unit/BTU_IT-PER-MOL-DEG_F" ->
            Just "Btu{IT}/(lb-mol⋅°F)"

        "http://qudt.org/vocab/unit/GigaBYTE" ->
            Just "GB"

        "http://qudt.org/vocab/unit/MilliM_HGA" ->
            Just "mmHgA"

        "http://qudt.org/vocab/unit/W" ->
            Just "W"

        "http://qudt.org/vocab/unit/PER-MicroM" ->
            Just "/µm"

        "http://qudt.org/vocab/unit/FARAD-PER-M" ->
            Just "F/m"

        "http://qudt.org/vocab/unit/SolarMass" ->
            Just "S"

        "http://qudt.org/vocab/unit/MilliV-PER-M" ->
            Just "mV/m"

        "http://qudt.org/vocab/unit/MicroM-PER-N" ->
            Just "µm/N"

        "http://qudt.org/vocab/unit/T-M" ->
            Just "T⋅m"

        "http://qudt.org/vocab/unit/MilliA-PER-IN" ->
            Just "mA/in"

        "http://qudt.org/vocab/unit/ERG-PER-G" ->
            Just "erg/G"

        "http://qudt.org/vocab/unit/CM_H2O" ->
            Just "cmH₂0"

        "http://qudt.org/vocab/unit/MicroM-PER-L-DAY" ->
            Just "µm/(L⋅day)"

        "http://qudt.org/vocab/unit/MilliM-PER-DAY" ->
            Just "mm/day"

        "http://qudt.org/vocab/unit/KiloN-PER-M2" ->
            Just "kN/m²"

        "http://qudt.org/vocab/unit/DEG_F-PER-SEC2" ->
            Just "°F/s²"

        "http://qudt.org/vocab/unit/AttoJ" ->
            Just "aJ"

        "http://qudt.org/vocab/unit/M2-PER-HA" ->
            Just "m²/ha"

        "http://qudt.org/vocab/unit/KiloPA-PER-MilliM" ->
            Just "kPa/mm"

        "http://qudt.org/vocab/unit/L-PER-MOL" ->
            Just "L/mol"

        "http://qudt.org/vocab/unit/KiloCAL-PER-MIN" ->
            Just "kcal/min"

        "http://qudt.org/vocab/unit/OZ-PER-FT2" ->
            Just "oz/ft²"

        "http://qudt.org/vocab/unit/CentiPOISE" ->
            Just "cP"

        "http://qudt.org/vocab/unit/AU" ->
            Just "AU"

        "http://qudt.org/vocab/unit/KY" ->
            Just "K"

        "http://qudt.org/vocab/unit/OZ-IN" ->
            Just "oz⋅in"

        "http://qudt.org/vocab/unit/MegaW" ->
            Just "MW"

        "http://qudt.org/vocab/unit/DEATHS-PER-1000000I-YR" ->
            Just "deaths/million individuals/yr"

        "http://qudt.org/vocab/unit/FT2-SEC-DEG_F" ->
            Just "ft²⋅s⋅°F"

        "http://qudt.org/vocab/unit/M2-PER-M2" ->
            Just "m²/m²"

        "http://qudt.org/vocab/unit/MilliL" ->
            Just "mL"

        "http://qudt.org/vocab/unit/MicroCi" ->
            Just "μCi"

        "http://qudt.org/vocab/unit/PER-PlanckMass2" ->
            Just "/(µmol⋅L)"

        "http://qudt.org/vocab/unit/PicoMOL-PER-M-W-SEC" ->
            Just "pmol/(m⋅W⋅s)"

        "http://qudt.org/vocab/unit/M2-HZ4" ->
            Just "m²⋅Hz⁴"

        "http://qudt.org/vocab/unit/PK_UK" ->
            Just "peck{UK}"

        "http://qudt.org/vocab/unit/HR-FT2" ->
            Just "hr⋅ft²"

        "http://qudt.org/vocab/unit/J-M-PER-MOL" ->
            Just "J⋅m/mol"

        "http://qudt.org/vocab/unit/V-M" ->
            Just "V⋅m"

        "http://qudt.org/vocab/unit/KiloMOL-PER-KiloGM" ->
            Just "kmol/kg"

        "http://qudt.org/vocab/unit/PER-DEG_C" ->
            Just "/°C"

        "http://qudt.org/vocab/unit/CentiM2-MIN" ->
            Just "cm²⋅min"

        "http://qudt.org/vocab/unit/PlanckMass" ->
            Just "planckmass"

        "http://qudt.org/vocab/unit/KiloGM-M-PER-SEC" ->
            Just "kg⋅m/s"

        "http://qudt.org/vocab/unit/HectoBAR" ->
            Just "hbar"

        "http://qudt.org/vocab/unit/ATM_T" ->
            Just "at"

        "http://qudt.org/vocab/unit/KiloGM-PER-SEC2" ->
            Just "kg/s²"

        "http://qudt.org/vocab/unit/NanoBQ" ->
            Just "nBq"

        "http://qudt.org/vocab/unit/PER-M" ->
            Just "/m"

        "http://qudt.org/vocab/unit/DECADE" ->
            Just "dec"

        "http://qudt.org/vocab/unit/DEG_R" ->
            Just "°R"

        "http://qudt.org/vocab/unit/CD-PER-LM" ->
            Just "cd/lm"

        "http://qudt.org/vocab/unit/BTU_TH-PER-LB-DEG_F" ->
            Just "Btu{th}/(lbm⋅°F)"

        "http://qudt.org/vocab/unit/KiloGM2-PER-SEC2" ->
            Just "kg²/s²"

        "http://qudt.org/vocab/unit/YD2" ->
            Just "sqyd"

        "http://qudt.org/vocab/unit/MicroSV-PER-HR" ->
            Just "µSv/hr"

        "http://qudt.org/vocab/unit/OZ-PER-GAL_US" ->
            Just "oz/gal{US}"

        "http://qudt.org/vocab/unit/MegaDOLLAR_US-PER-FLIGHT" ->
            Just "$M/flight"

        "http://qudt.org/vocab/unit/PlanckArea" ->
            Just "planckarea"

        "http://qudt.org/vocab/unit/C-PER-KiloGM" ->
            Just "C/kg"

        "http://qudt.org/vocab/unit/BTU_TH-PER-SEC" ->
            Just "Btu{th}/s"

        "http://qudt.org/vocab/unit/PER-MilliM" ->
            Just "/mm"

        "http://qudt.org/vocab/unit/PINT_US-PER-MIN" ->
            Just "pt{US}/min"

        "http://qudt.org/vocab/unit/MilliL-PER-HR" ->
            Just "mL/hr"

        "http://qudt.org/vocab/unit/TON_F_US" ->
            Just "tonf{us}"

        "http://qudt.org/vocab/unit/MicroM3" ->
            Just "µm³"

        "http://qudt.org/vocab/unit/M3-PER-M3" ->
            Just "m³/m³"

        "http://qudt.org/vocab/unit/MegaOHM" ->
            Just "MΩ"

        "http://qudt.org/vocab/unit/MicroH-PER-KiloOHM" ->
            Just "µH/kΩ"

        "http://qudt.org/vocab/unit/FemtoC" ->
            Just "fC"

        "http://qudt.org/vocab/unit/BTU_IT-PER-SEC-FT2" ->
            Just "Btu{IT}/(s⋅ft²)"

        "http://qudt.org/vocab/unit/FT-PER-SEC2" ->
            Just "ft/s²"

        "http://qudt.org/vocab/unit/MicroC-PER-M2" ->
            Just "µC/m²"

        "http://qudt.org/vocab/unit/GI_UK" ->
            Just "gill{UK}"

        "http://qudt.org/vocab/unit/NanoA" ->
            Just "nA"

        "http://qudt.org/vocab/unit/GM_F" ->
            Just "gf"

        "http://qudt.org/vocab/unit/MicroMOL-PER-MicroMOL-DAY" ->
            Just "μmol/(µmol⋅day)"

        "http://qudt.org/vocab/unit/CAL_TH-PER-GM" ->
            Just "cal/g"

        "http://qudt.org/vocab/unit/KiloHZ-M" ->
            Just "kHz⋅m"

        "http://qudt.org/vocab/unit/PlanckFrequency" ->
            Just "planckfrequency"

        "http://qudt.org/vocab/unit/KiloCAL_TH-PER-MIN" ->
            Just "kcal{th}/min"

        "http://qudt.org/vocab/unit/TONNE-PER-HA" ->
            Just "t/ha"

        "http://qudt.org/vocab/unit/NUM-PER-M" ->
            Just "/m"

        "http://qudt.org/vocab/unit/QT_US-PER-HR" ->
            Just "qt/hr"

        "http://qudt.org/vocab/unit/KiloCi" ->
            Just "kCi"

        "http://qudt.org/vocab/unit/FT-PER-SEC" ->
            Just "ft/s"

        "http://qudt.org/vocab/unit/IN3" ->
            Just "in³"

        "http://qudt.org/vocab/unit/PlanckCharge" ->
            Just "planckcharge"

        "http://qudt.org/vocab/unit/W-PER-M2-NanoM-SR" ->
            Just "W/(m²⋅nm⋅sr)"

        "http://qudt.org/vocab/unit/BFT" ->
            Just "Beufort"

        "http://qudt.org/vocab/unit/MicroT" ->
            Just "µT"

        "http://qudt.org/vocab/unit/ARCSEC" ->
            Just "\""

        "http://qudt.org/vocab/unit/BU_US_DRY-PER-HR" ->
            Just "bsh{US}/hr"

        "http://qudt.org/vocab/unit/B" ->
            Just "B"

        "http://qudt.org/vocab/unit/LB_T" ->
            Just "lbt"

        "http://qudt.org/vocab/unit/MicroW-PER-M2" ->
            Just "µW/m²"

        "http://qudt.org/vocab/unit/NAT" ->
            Just "nat"

        "http://qudt.org/vocab/unit/BU_US_DRY-PER-MIN" ->
            Just "bsh{US}/min"

        "http://qudt.org/vocab/unit/GM-MilliM" ->
            Just "g⋅mm"

        "http://qudt.org/vocab/unit/HectoM" ->
            Just "hm"

        "http://qudt.org/vocab/unit/MilliGM-PER-M2-HR" ->
            Just "mg/(m²⋅hr)"

        "http://qudt.org/vocab/unit/H-PER-OHM" ->
            Just "H/Ω"

        "http://qudt.org/vocab/unit/MegaS-PER-M" ->
            Just "MS/m"

        "http://qudt.org/vocab/unit/BTU_IT-PER-FT2" ->
            Just "Btu{IT}/ft²"

        "http://qudt.org/vocab/unit/CentiM_H2O" ->
            Just "cmH₂0"

        "http://qudt.org/vocab/unit/MilliGM-PER-M2-DAY" ->
            Just "mg/(m²⋅day)"

        "http://qudt.org/vocab/unit/PicoMOL-PER-M3" ->
            Just "pmol/m³"

        "http://qudt.org/vocab/unit/CentiM-PER-K" ->
            Just "cm/K"

        "http://qudt.org/vocab/unit/KiloV-A_Reactive" ->
            Just "kV⋅A{Reactive}"

        "http://qudt.org/vocab/unit/NUM-PER-PicoL" ->
            Just "/pL"

        "http://qudt.org/vocab/unit/MegaV-A-HR" ->
            Just "MV⋅A⋅hr"

        "http://qudt.org/vocab/unit/L-PER-SEC-M2" ->
            Just "L/(s⋅m²)"

        "http://qudt.org/vocab/unit/NUM" ->
            Just "#"

        "http://qudt.org/vocab/unit/HP_Brake" ->
            Just "HP{brake}"

        "http://qudt.org/vocab/unit/REV-PER-HR" ->
            Just "rev/hr"

        "http://qudt.org/vocab/unit/MilliM" ->
            Just "mm"

        "http://qudt.org/vocab/unit/DPI" ->
            Just "DPI"

        "http://qudt.org/vocab/unit/PlanckFrequency_Ang" ->
            Just "planckangularfrequency"

        "http://qudt.org/vocab/unit/SEC-PER-RAD-M3" ->
            Just "s/(rad⋅m³)"

        "http://qudt.org/vocab/unit/N-M-PER-RAD" ->
            Just "N⋅m/rad"

        "http://qudt.org/vocab/unit/GM_Nitrogen-PER-M2-DAY" ->
            Just "g{nitrogen}/(m²⋅day)"

        "http://qudt.org/vocab/unit/NanoMOL-PER-L-DAY" ->
            Just "nmol/(L⋅day)"

        "http://qudt.org/vocab/unit/DeciBAR-PER-YR" ->
            Just "dbar/yr"

        "http://qudt.org/vocab/unit/MilliGM-PER-M3-SEC" ->
            Just "mg/(m³⋅s)"

        "http://qudt.org/vocab/unit/DWT" ->
            Just "dwt"

        "http://qudt.org/vocab/unit/PERCENT" ->
            Just "%"

        "http://qudt.org/vocab/unit/KiloM3-PER-SEC2" ->
            Just "km³/s²"

        "http://qudt.org/vocab/unit/AttoJ-SEC" ->
            Just "aJ⋅s"

        "http://qudt.org/vocab/unit/FT-LB_F" ->
            Just "ft⋅lbf"

        "http://qudt.org/vocab/unit/HP_Metric" ->
            Just "HP{metric}"

        "http://qudt.org/vocab/unit/KiloJ-PER-MOL" ->
            Just "kJ/mol"

        "http://qudt.org/vocab/unit/DEG-PER-SEC" ->
            Just "°/s"

        "http://qudt.org/vocab/unit/GAL_US-PER-SEC" ->
            Just "gal{US}/s"

        "http://qudt.org/vocab/unit/RT" ->
            Just "RT"

        "http://qudt.org/vocab/unit/KiloGAUSS" ->
            Just "kGs"

        "http://qudt.org/vocab/unit/GigaBQ" ->
            Just "GBq"

        "http://qudt.org/vocab/unit/GI_US-PER-DAY" ->
            Just "gill{US}/day"

        "http://qudt.org/vocab/unit/MI3" ->
            Just "mi³"

        "http://qudt.org/vocab/unit/BTU_IT-PER-HR-FT2" ->
            Just "Btu{IT}/(hr⋅ft²)"

        "http://qudt.org/vocab/unit/A_Stat-PER-CentiM2" ->
            Just "statA/cm²"

        "http://qudt.org/vocab/unit/H-PER-KiloOHM" ->
            Just "H/kΩ"

        "http://qudt.org/vocab/unit/TON_Metric-PER-M3" ->
            Just "t/m³"

        "http://qudt.org/vocab/unit/L-PER-K" ->
            Just "L/K"

        "http://qudt.org/vocab/unit/LB_F-PER-IN2" ->
            Just "lbf/in²"

        "http://qudt.org/vocab/unit/M2-PER-N" ->
            Just "m²/N"

        "http://qudt.org/vocab/unit/PER-SEC2" ->
            Just "/s²"

        "http://qudt.org/vocab/unit/AC-FT" ->
            Just "acre⋅ft"

        "http://qudt.org/vocab/unit/MegaEV-PER-SpeedOfLight" ->
            Just "MeV/c"

        "http://qudt.org/vocab/unit/LB_F-PER-IN" ->
            Just "lbf/in"

        "http://qudt.org/vocab/unit/MilliA-HR" ->
            Just "mA⋅hr"

        "http://qudt.org/vocab/unit/BQ-PER-M3" ->
            Just "Bq/m³"

        "http://qudt.org/vocab/unit/DEGREE_BRIX" ->
            Just "°Bx"

        "http://qudt.org/vocab/unit/K-PER-W" ->
            Just "K/W"

        "http://qudt.org/vocab/unit/CentiM-SEC-DEG_C" ->
            Just "cm⋅s⋅°C"

        "http://qudt.org/vocab/unit/MicroMOL" ->
            Just "μmol"

        "http://qudt.org/vocab/unit/N-PER-MilliM2" ->
            Just "N/mm²"

        "http://qudt.org/vocab/unit/N-M2" ->
            Just "N⋅m²"

        "http://qudt.org/vocab/unit/PINT_US" ->
            Just "pt{US}"

        "http://qudt.org/vocab/unit/GAL_UK" ->
            Just "gal{UK}"

        "http://qudt.org/vocab/unit/MegaBTU_IT-PER-HR" ->
            Just "MBtu{IT}/hr"

        "http://qudt.org/vocab/unit/KiloGM-PER-DAY" ->
            Just "kg/day"

        "http://qudt.org/vocab/unit/PicoC" ->
            Just "pC"

        "http://qudt.org/vocab/unit/HP_Electric" ->
            Just "HP{electric}"

        "http://qudt.org/vocab/unit/M2-PER-KiloGM" ->
            Just "m²/kg"

        "http://qudt.org/vocab/unit/GAL_US-PER-DAY" ->
            Just "gal{US}/day"

        "http://qudt.org/vocab/unit/KiloJ-PER-KiloGM-K" ->
            Just "kJ/(kg⋅K)"

        "http://qudt.org/vocab/unit/MicroGM-PER-M3" ->
            Just "μg/m³"

        "http://qudt.org/vocab/unit/MIN_Angle" ->
            Just "'"

        "http://qudt.org/vocab/unit/FemtoL" ->
            Just "fL"

        "http://qudt.org/vocab/unit/Gs" ->
            Just "G"

        "http://qudt.org/vocab/unit/M2-PER-SEC2-K" ->
            Just "m²/(s²⋅K)"

        "http://qudt.org/vocab/unit/EUR-PER-W-SEC" ->
            Just "€/(W⋅s)"

        "http://qudt.org/vocab/unit/OZ_TROY" ->
            Just "oz{Troy}"

        "http://qudt.org/vocab/unit/OZ-PER-GAL_UK" ->
            Just "oz/gal{UK}"

        "http://qudt.org/vocab/unit/MegaBAR" ->
            Just "Mbar"

        "http://qudt.org/vocab/unit/LA_FT" ->
            Just "ft-L"

        "http://qudt.org/vocab/unit/KiloPA-PER-K" ->
            Just "kPa/K"

        "http://qudt.org/vocab/unit/MI" ->
            Just "mi"

        "http://qudt.org/vocab/unit/PER-T-SEC" ->
            Just "/(T⋅s)"

        "http://qudt.org/vocab/unit/MilliV" ->
            Just "mV"

        "http://qudt.org/vocab/unit/KiloGM-PER-CentiM2" ->
            Just "kg/cm²"

        "http://qudt.org/vocab/unit/J-PER-CentiM2-DAY" ->
            Just "J/(cm²⋅day)"

        "http://qudt.org/vocab/unit/MilliBAR" ->
            Just "mbar"

        "http://qudt.org/vocab/unit/MI-PER-SEC" ->
            Just "mi/s"

        "http://qudt.org/vocab/unit/KiloGM-PER-HA" ->
            Just "kg/ha"

        "http://qudt.org/vocab/unit/RPK" ->
            Just "RPK"

        "http://qudt.org/vocab/unit/PicoS-PER-M" ->
            Just "pS/m"

        "http://qudt.org/vocab/unit/KiloGM-PER-MOL" ->
            Just "kg/mol"

        "http://qudt.org/vocab/unit/GR" ->
            Just "gr"

        "http://qudt.org/vocab/unit/W-PER-M2-M-SR" ->
            Just "W/(m²⋅m⋅sr)"

        "http://qudt.org/vocab/unit/HR" ->
            Just "hr"

        "http://qudt.org/vocab/unit/CentiC" ->
            Just "cC"

        "http://qudt.org/vocab/unit/MilliL-PER-DAY" ->
            Just "mL/day"

        "http://qudt.org/vocab/unit/PER-M2-SEC" ->
            Just "/(m²⋅s)"

        "http://qudt.org/vocab/unit/TeraW" ->
            Just "TW"

        "http://qudt.org/vocab/unit/PicoMOL-PER-M2-DAY" ->
            Just "pmol/(m²⋅day)"

        "http://qudt.org/vocab/unit/YD3-PER-SEC" ->
            Just "yd³/s"

        "http://qudt.org/vocab/unit/KiloGM-PER-M2" ->
            Just "kg/m²"

        "http://qudt.org/vocab/unit/M2-PER-SR-J" ->
            Just "m²/(sr⋅J)"

        "http://qudt.org/vocab/unit/KiloGM-PER-DeciM3" ->
            Just "kg/dm³"

        "http://qudt.org/vocab/unit/V2-PER-K2" ->
            Just "V²/K²"

        "http://qudt.org/vocab/unit/FemtoGM-PER-KiloGM" ->
            Just "fg/kg"

        "http://qudt.org/vocab/unit/PHOT" ->
            Just "ph"

        "http://qudt.org/vocab/unit/YR_Common" ->
            Just "yr"

        "http://qudt.org/vocab/unit/MilliH" ->
            Just "mH"

        "http://qudt.org/vocab/unit/PlanckEnergy" ->
            Just "Eᵨ"

        "http://qudt.org/vocab/unit/NanoMOL-PER-CentiM3-HR" ->
            Just "nmol/(cm³⋅hr)"

        "http://qudt.org/vocab/unit/MicroC" ->
            Just "μC"

        "http://qudt.org/vocab/unit/KiloGM-PER-M-SEC" ->
            Just "kg/(m⋅s)"

        "http://qudt.org/vocab/unit/DeciM3-PER-MOL" ->
            Just "dm³/mol"

        "http://qudt.org/vocab/unit/KiloTON_Metric" ->
            Just "kt"

        "http://qudt.org/vocab/unit/RAD" ->
            Just "rad"

        "http://qudt.org/vocab/unit/MOL" ->
            Just "mol"

        "http://qudt.org/vocab/unit/DYN-SEC-PER-CentiM" ->
            Just "dyn⋅s/cm"

        "http://qudt.org/vocab/unit/MegaHZ-M" ->
            Just "MHz⋅m"

        "http://qudt.org/vocab/unit/MicroGM-PER-L-HR" ->
            Just "μg/(L⋅hr)"

        "http://qudt.org/vocab/unit/C_Ab-PER-CentiM2" ->
            Just "abC/cm²"

        "http://qudt.org/vocab/unit/GRAIN" ->
            Just "gr{UK}"

        "http://qudt.org/vocab/unit/LB-DEG_R" ->
            Just "lbm⋅°R"

        "http://qudt.org/vocab/unit/M2-PER-SR" ->
            Just "m²/sr"

        "http://qudt.org/vocab/unit/MicroJ" ->
            Just "μJ"

        "http://qudt.org/vocab/unit/DEATHS-PER-1000I-YR" ->
            Just "deaths/1000 individuals/yr"

        "http://qudt.org/vocab/unit/V-PER-M2" ->
            Just "V/m²"

        "http://qudt.org/vocab/unit/J-PER-KiloGM-K-M3" ->
            Just "J/(kg⋅K⋅m³)"

        "http://qudt.org/vocab/unit/DEGREE_BALLING" ->
            Just "°Balling"

        "http://qudt.org/vocab/unit/OZ-PER-MIN" ->
            Just "oz/min"

        "http://qudt.org/vocab/unit/NanoGM-PER-MicroL" ->
            Just "ng/µL"

        "http://qudt.org/vocab/unit/TBSP" ->
            Just "tbsp"

        "http://qudt.org/vocab/unit/SEC2" ->
            Just "s²"

        "http://qudt.org/vocab/unit/FT-PER-DAY" ->
            Just "ft/day"

        "http://qudt.org/vocab/unit/DAY_Sidereal" ->
            Just "day{sidereal}"

        "http://qudt.org/vocab/unit/J-PER-GM-K" ->
            Just "J/(g⋅K)"

        "http://qudt.org/vocab/unit/KiloGM-PER-M-HR" ->
            Just "kg/(m⋅hr)"

        "http://qudt.org/vocab/unit/BTU_IT-PER-SEC-FT-DEG_R" ->
            Just "Btu{IT}/(s⋅ft⋅°R)"

        "http://qudt.org/vocab/unit/NanoM-PER-MilliM-MegaPA" ->
            Just "nm/(mm⋅MPa)"

        "http://qudt.org/vocab/unit/DARCY" ->
            Just "d"

        "http://qudt.org/vocab/unit/PA-PER-HR" ->
            Just "Pa/hr"

        "http://qudt.org/vocab/unit/OZ_M" ->
            Just "oz"

        "http://qudt.org/vocab/unit/MilliN-M" ->
            Just "mN⋅m"

        "http://qudt.org/vocab/unit/PicoA-PER-MicroMOL-L" ->
            Just "pA/(µmol⋅L)"

        "http://qudt.org/vocab/unit/FUR" ->
            Just "furlong"

        "http://qudt.org/vocab/unit/W-PER-M2-SR" ->
            Just "W/(m²⋅sr)"

        "http://qudt.org/vocab/unit/LB-DEG_F" ->
            Just "lbm⋅°F"

        "http://qudt.org/vocab/unit/MOL-PER-TONNE" ->
            Just "mol/t"

        "http://qudt.org/vocab/unit/LB-PER-MIN" ->
            Just "lbm/min"

        "http://qudt.org/vocab/unit/IN3-PER-SEC" ->
            Just "in³/s"

        "http://qudt.org/vocab/unit/NTU" ->
            Just "NTU"

        "http://qudt.org/vocab/unit/BBL_UK_PET" ->
            Just "bbl{UK petroleum}"

        "http://qudt.org/vocab/unit/DeciM3-PER-SEC" ->
            Just "dm³/s"

        "http://qudt.org/vocab/unit/KiloGM-SEC2" ->
            Just "kg⋅s²"

        "http://qudt.org/vocab/unit/DecaC" ->
            Just "daC"

        "http://qudt.org/vocab/unit/MilliGM-PER-KiloGM" ->
            Just "mg/kg"

        "http://qudt.org/vocab/unit/MilliM3-PER-M3" ->
            Just "mm³/m³"

        "http://qudt.org/vocab/unit/EV" ->
            Just "eV"

        "http://qudt.org/vocab/unit/GM-PER-MilliL" ->
            Just "g/mL"

        "http://qudt.org/vocab/unit/MegaS" ->
            Just "MS"

        "http://qudt.org/vocab/unit/DEG_R-PER-MIN" ->
            Just "°R/min"

        "http://qudt.org/vocab/unit/HectoL" ->
            Just "hL"

        "http://qudt.org/vocab/unit/ElementaryCharge" ->
            Just "e"

        "http://qudt.org/vocab/unit/V" ->
            Just "V"

        "http://qudt.org/vocab/unit/MIN" ->
            Just "min"

        "http://qudt.org/vocab/unit/C_Stat-PER-MOL" ->
            Just "statC/mol"

        "http://qudt.org/vocab/unit/PINT_US_DRY" ->
            Just "pt{US Dry}"

        "http://qudt.org/vocab/unit/KiloM-PER-DAY" ->
            Just "km/day"

        "http://qudt.org/vocab/unit/MilliARCSEC" ->
            Just "mas"

        "http://qudt.org/vocab/unit/KiloN-M-PER-DEG" ->
            Just "kN⋅m/°"

        "http://qudt.org/vocab/unit/SLUG-PER-MIN" ->
            Just "slug/min"

        "http://qudt.org/vocab/unit/KiloGM-PER-MIN" ->
            Just "kg/min"

        "http://qudt.org/vocab/unit/PicoH" ->
            Just "pH"

        "http://qudt.org/vocab/unit/MilliS-PER-CentiM" ->
            Just "mS/cm"

        "http://qudt.org/vocab/unit/NanoKAT-PER-L" ->
            Just "nkat/L"

        "http://qudt.org/vocab/unit/MilliH-PER-KiloOHM" ->
            Just "mH/kΩ"

        "http://qudt.org/vocab/unit/NanoMOL-PER-GM-SEC" ->
            Just "nmol/(g⋅s)"

        "http://qudt.org/vocab/unit/NanoGM-PER-DeciL" ->
            Just "ng/dL"

        "http://qudt.org/vocab/unit/KiloCAL-PER-MOL" ->
            Just "kcal/mol"

        "http://qudt.org/vocab/unit/LB-PER-HR" ->
            Just "lbm/hr"

        "http://qudt.org/vocab/unit/BQ-PER-M2" ->
            Just "Bq/m²"

        "http://qudt.org/vocab/unit/U" ->
            Just "u"

        "http://qudt.org/vocab/unit/BAN" ->
            Just "ban"

        "http://qudt.org/vocab/unit/LUX" ->
            Just "lx"

        "http://qudt.org/vocab/unit/DYN-SEC-PER-CentiM3" ->
            Just "dyn⋅s/cm³"

        "http://qudt.org/vocab/unit/BBL_UK_PET-PER-DAY" ->
            Just "bbl{UK petroleum}/day"

        "http://qudt.org/vocab/unit/WB-PER-MilliM" ->
            Just "Wb/mm"

        "http://qudt.org/vocab/unit/L" ->
            Just "L"

        "http://qudt.org/vocab/unit/NUM-PER-HR" ->
            Just "/hr"

        "http://qudt.org/vocab/unit/BTU_IT-PER-SEC" ->
            Just "Btu{IT}/s"

        "http://qudt.org/vocab/unit/MegaW-HR" ->
            Just "MW⋅hr"

        "http://qudt.org/vocab/unit/N-CentiM" ->
            Just "N⋅cm"

        "http://qudt.org/vocab/unit/GRAD" ->
            Just "grad"

        "http://qudt.org/vocab/unit/KiloCAL-PER-GM-DEG_C" ->
            Just "kcal/(g⋅°C)"

        "http://qudt.org/vocab/unit/KAT" ->
            Just "kat"

        "http://qudt.org/vocab/unit/GI_US" ->
            Just "gill{US}"

        "http://qudt.org/vocab/unit/PicoMOL-PER-L" ->
            Just "pmol/L"

        "http://qudt.org/vocab/unit/PA-PER-K" ->
            Just "Pa/K"

        "http://qudt.org/vocab/unit/TeraJ" ->
            Just "TJ"

        "http://qudt.org/vocab/unit/PER-PA" ->
            Just "/Pa"

        "http://qudt.org/vocab/unit/PicoGM-PER-L" ->
            Just "pg/L"

        "http://qudt.org/vocab/unit/PA-PER-BAR" ->
            Just "Pa/bar"

        "http://qudt.org/vocab/unit/KiloN-M-PER-DEG-M" ->
            Just "kN⋅m/(°·m)"

        "http://qudt.org/vocab/unit/PK_UK-PER-DAY" ->
            Just "peck{UK}/day"

        "http://qudt.org/vocab/unit/ERLANG" ->
            Just "E"

        "http://qudt.org/vocab/unit/NanoFARAD" ->
            Just "nF"

        "http://qudt.org/vocab/unit/CAL_TH" ->
            Just "cal"

        "http://qudt.org/vocab/unit/KiloGM-PER-YR" ->
            Just "kg/year"

        "http://qudt.org/vocab/unit/MicroMOL-PER-M2-DAY" ->
            Just "µmol/(m²⋅day)"

        "http://qudt.org/vocab/unit/SUSCEPTIBILITY_ELEC" ->
            Just "χ"

        "http://qudt.org/vocab/unit/QT_US" ->
            Just "qt"

        "http://qudt.org/vocab/unit/MilliPA" ->
            Just "mPa"

        "http://qudt.org/vocab/unit/PlanckPower" ->
            Just "planckpower"

        "http://qudt.org/vocab/unit/ERG-SEC" ->
            Just "erg⋅s"

        "http://qudt.org/vocab/unit/MicroS-PER-CentiM" ->
            Just "μS/cm"

        "http://qudt.org/vocab/unit/RAYL" ->
            Just "rayl"

        "http://qudt.org/vocab/unit/TON_LONG" ->
            Just "t{long}"

        "http://qudt.org/vocab/unit/M4" ->
            Just "m⁴"

        "http://qudt.org/vocab/unit/C-M2" ->
            Just "C⋅m²"

        "http://qudt.org/vocab/unit/MilliR_man" ->
            Just "mrem"

        "http://qudt.org/vocab/unit/KiloL-PER-HR" ->
            Just "kL/hr"

        "http://qudt.org/vocab/unit/KiloLB_F-PER-FT" ->
            Just "klbf/ft"

        "http://qudt.org/vocab/unit/PERMEABILITY_REL" ->
            Just "kᵣ"

        "http://qudt.org/vocab/unit/PSI-IN3-PER-SEC" ->
            Just "psi⋅in³/s"

        "http://qudt.org/vocab/unit/OZ-PER-IN3" ->
            Just "oz/in³"

        "http://qudt.org/vocab/unit/M2-PER-SEC2" ->
            Just "m²/s²"

        "http://qudt.org/vocab/unit/MicroC-PER-M3" ->
            Just "µC/m³"

        "http://qudt.org/vocab/unit/PicoW-PER-M2" ->
            Just "pW/m²"

        "http://qudt.org/vocab/unit/GigaHZ-M" ->
            Just "GHz⋅m"

        "http://qudt.org/vocab/unit/PicoMOL-PER-M3-SEC" ->
            Just "pmol/(m³⋅s)"

        "http://qudt.org/vocab/unit/KiloGM-MilliM2" ->
            Just "kg⋅mm²"

        "http://qudt.org/vocab/unit/DecaARE" ->
            Just "daa"

        "http://qudt.org/vocab/unit/MicroMOL-PER-GM-SEC" ->
            Just "μmol/(g⋅s)"

        "http://qudt.org/vocab/unit/BTU_IT-PER-SEC-FT2-DEG_R" ->
            Just "Btu{IT}/(s⋅ft²⋅°R)"

        "http://qudt.org/vocab/unit/PlanckForce" ->
            Just "planckforce"

        "http://qudt.org/vocab/unit/A-PER-MilliM2" ->
            Just "A/mm²"

        "http://qudt.org/vocab/unit/MHO" ->
            Just "℧"

        "http://qudt.org/vocab/unit/PicoW" ->
            Just "pW"

        "http://qudt.org/vocab/unit/GM-PER-DeciM3" ->
            Just "g/dm³"

        "http://qudt.org/vocab/unit/DEATHS-PER-KiloINDIV-YR" ->
            Just "deaths/1000 individuals/yr"

        "http://qudt.org/vocab/unit/J-PER-CentiM2" ->
            Just "J/cm²"

        "http://qudt.org/vocab/unit/FT-LB_F-PER-FT2" ->
            Just "ft⋅lbf/ft²"

        "http://qudt.org/vocab/unit/TebiBYTE" ->
            Just "TiB"

        "http://qudt.org/vocab/unit/GigaOHM" ->
            Just "GΩ"

        "http://qudt.org/vocab/unit/MilliN-PER-M" ->
            Just "mN/m"

        "http://qudt.org/vocab/unit/PicoSEC" ->
            Just "ps"

        "http://qudt.org/vocab/unit/EUR-PER-W" ->
            Just "€/W"

        "http://qudt.org/vocab/unit/MIL_Circ" ->
            Just "cmil"

        "http://qudt.org/vocab/unit/MilliGM-PER-M2-SEC" ->
            Just "mg/(m²⋅s)"

        "http://qudt.org/vocab/unit/C2-M2-PER-J" ->
            Just "C²⋅m²/J"

        "http://qudt.org/vocab/unit/FT2" ->
            Just "ft²"

        "http://qudt.org/vocab/unit/MilliGM-PER-DeciL" ->
            Just "mg/dL"

        "http://qudt.org/vocab/unit/DEG_C_GROWING_CEREAL-DAY" ->
            Just "GDD"

        "http://qudt.org/vocab/unit/MilliC-PER-M2" ->
            Just "mC/m²"

        "http://qudt.org/vocab/unit/L-PER-KiloGM" ->
            Just "L/kg"

        "http://qudt.org/vocab/unit/KiloGM-K" ->
            Just "kg⋅K"

        "http://qudt.org/vocab/unit/OZ_VOL_US-PER-SEC" ->
            Just "fl oz{US}/s"

        "http://qudt.org/vocab/unit/C-PER-M2" ->
            Just "C/m²"

        "http://qudt.org/vocab/unit/FT2-PER-SEC" ->
            Just "ft²/s"

        "http://qudt.org/vocab/unit/MilliL-PER-M2-DAY" ->
            Just "mL/(m²⋅day)"

        "http://qudt.org/vocab/unit/M2-PER-M" ->
            Just "m²/m"

        "http://qudt.org/vocab/unit/FemtoM" ->
            Just "fm"

        "http://qudt.org/vocab/unit/NUM-PER-HectoGM" ->
            Just "/hg"

        "http://qudt.org/vocab/unit/MilliGM-PER-MIN" ->
            Just "mg/min"

        "http://qudt.org/vocab/unit/MOL_LB" ->
            Just "lb-mol"

        "http://qudt.org/vocab/unit/OZ_VOL_UK" ->
            Just "oz{UK}"

        "http://qudt.org/vocab/unit/NanoMOL-PER-MicroGM-HR" ->
            Just "nmol/(µg⋅hr)"

        "http://qudt.org/vocab/unit/YottaC" ->
            Just "YC"

        "http://qudt.org/vocab/unit/LB-PER-GAL" ->
            Just "lb/gal"

        "http://qudt.org/vocab/unit/TON_US-PER-YD3" ->
            Just "ton{US}/yd³"

        "http://qudt.org/vocab/unit/KiloBYTE-PER-SEC" ->
            Just "kB/s"

        "http://qudt.org/vocab/unit/TON_UK" ->
            Just "ton{UK}"

        "http://qudt.org/vocab/unit/Hundredweight_US" ->
            Just "cwt{short}"

        "http://qudt.org/vocab/unit/V-PER-CentiM" ->
            Just "V/cm"

        "http://qudt.org/vocab/unit/EV-PER-K" ->
            Just "eV/K"

        "http://qudt.org/vocab/unit/WB-PER-M" ->
            Just "Wb/m"

        "http://qudt.org/vocab/unit/DeciGM" ->
            Just "dg"

        "http://qudt.org/vocab/unit/M3-PER-MOL-SEC" ->
            Just "m³/(mol⋅s)"

        "http://qudt.org/vocab/unit/POISE" ->
            Just "P"

        "http://qudt.org/vocab/unit/M-PER-HR" ->
            Just "m/hr"

        "http://qudt.org/vocab/unit/PINT_UK-PER-DAY" ->
            Just "pt{UK}/day"

        "http://qudt.org/vocab/unit/AT-PER-M" ->
            Just "AT/m"

        "http://qudt.org/vocab/unit/CentiM3" ->
            Just "cm³"

        "http://qudt.org/vocab/unit/PER-M-NanoM" ->
            Just "/(m⋅nm)"

        "http://qudt.org/vocab/unit/RAD-M2-PER-KiloGM" ->
            Just "rad⋅m²/kg"

        "http://qudt.org/vocab/unit/AttoC" ->
            Just "aC"

        "http://qudt.org/vocab/unit/W-M-PER-M2-SR" ->
            Just "W⋅m/(m²⋅sr)"

        "http://qudt.org/vocab/unit/PA-SEC" ->
            Just "Pa⋅s"

        "http://qudt.org/vocab/unit/GM-PER-HR" ->
            Just "g/hr"

        "http://qudt.org/vocab/unit/PK_US_DRY-PER-SEC" ->
            Just "peck{US Dry}/s"

        "http://qudt.org/vocab/unit/KiloGM-PER-GigaJ" ->
            Just "kg/GJ"

        "http://qudt.org/vocab/unit/BTU_IT-PER-LB_F-DEG_R" ->
            Just "Btu{IT}/(lbf⋅°R)"

        "http://qudt.org/vocab/unit/UnitPole" ->
            Just "pole"

        "http://qudt.org/vocab/unit/Hundredweight_UK" ->
            Just "cwt{long}"

        "http://qudt.org/vocab/unit/MegaPA-M0pt5" ->
            Just "MPa√m"

        "http://qudt.org/vocab/unit/BAR-M3-PER-SEC" ->
            Just "bar⋅m³/s"

        "http://qudt.org/vocab/unit/PK_US_DRY-PER-DAY" ->
            Just "peck{US Dry}/day"

        "http://qudt.org/vocab/unit/FARAD-PER-KiloM" ->
            Just "F/km"

        "http://qudt.org/vocab/unit/KiloLB_F-FT-PER-LB" ->
            Just "klbf⋅ft/lbm"

        "http://qudt.org/vocab/unit/GAL_US" ->
            Just "gal{US}"

        "http://qudt.org/vocab/unit/DEG_C-PER-SEC" ->
            Just "°C/s"

        "http://qudt.org/vocab/unit/PicoW-PER-CentiM2-L" ->
            Just "pW/(cm²⋅L)"

        "http://qudt.org/vocab/unit/LB_F-SEC-PER-FT2" ->
            Just "lbf⋅s/ft²"

        "http://qudt.org/vocab/unit/MegaPA" ->
            Just "MPa"

        "http://qudt.org/vocab/unit/W-PER-M2-K" ->
            Just "W/(m²⋅K)"

        "http://qudt.org/vocab/unit/MicroV-PER-M" ->
            Just "µV/m"

        "http://qudt.org/vocab/unit/M-PER-YR" ->
            Just "m/yr"

        "http://qudt.org/vocab/unit/W-M2" ->
            Just "W⋅m²"

        "http://qudt.org/vocab/unit/KiloGM-CentiM2" ->
            Just "kg⋅cm²"

        "http://qudt.org/vocab/unit/PPM" ->
            Just "PPM"

        "http://qudt.org/vocab/unit/MilliM4" ->
            Just "mm⁴"

        "http://qudt.org/vocab/unit/BTU_TH-PER-HR" ->
            Just "Btu{th}/hr"

        "http://qudt.org/vocab/unit/KiloGM-PER-SEC-M2" ->
            Just "kg/(s⋅m²)"

        "http://qudt.org/vocab/unit/IN-PER-SEC" ->
            Just "in/s"

        "http://qudt.org/vocab/unit/MilliGM-PER-M3" ->
            Just "mg/m³"

        "http://qudt.org/vocab/unit/DeciTON_Metric" ->
            Just "dt"

        "http://qudt.org/vocab/unit/MOL-PER-M2-DAY" ->
            Just "mol/(m²⋅day)"

        "http://qudt.org/vocab/unit/YR_Sidereal" ->
            Just "yr{sidereal}"

        "http://qudt.org/vocab/unit/KiloCAL-PER-CentiM2-SEC" ->
            Just "kcal/(cm²⋅s)"

        "http://qudt.org/vocab/unit/MilliW-PER-M2-NanoM-SR" ->
            Just "mW/(cm⋅nm⋅sr)"

        "http://qudt.org/vocab/unit/KiloN-M-PER-M" ->
            Just "kN⋅m/m"

        "http://qudt.org/vocab/unit/MilliS-PER-M" ->
            Just "mS/m"

        "http://qudt.org/vocab/unit/SLUG-PER-FT3" ->
            Just "slug/ft³"

        "http://qudt.org/vocab/unit/N-PER-CentiM2" ->
            Just "N/cm²"

        "http://qudt.org/vocab/unit/MicroMHO" ->
            Just "µ℧"

        "http://qudt.org/vocab/unit/KibiBYTE" ->
            Just "KiB"

        "http://qudt.org/vocab/unit/KiloTONNE-PER-YR" ->
            Just "kt/year"

        "http://qudt.org/vocab/unit/GM_Carbon-PER-M2-DAY" ->
            Just "g{carbon}/(m²⋅day)"

        "http://qudt.org/vocab/unit/FT-LA" ->
            Just "ft⋅L"

        "http://qudt.org/vocab/unit/FT2-DEG_F" ->
            Just "ft²⋅°F"

        "http://qudt.org/vocab/unit/GM-PER-MOL" ->
            Just "g/mol"

        "http://qudt.org/vocab/unit/BTU_TH-FT-PER-HR-FT2-DEG_F" ->
            Just "Btu{th}⋅ft/(hr⋅ft²⋅°F)"

        "http://qudt.org/vocab/unit/MegaN-M" ->
            Just "MN⋅m"

        "http://qudt.org/vocab/unit/OZ_VOL_UK-PER-DAY" ->
            Just "oz{UK}/day"

        "http://qudt.org/vocab/unit/Stone_UK" ->
            Just "st{UK}"

        "http://qudt.org/vocab/unit/MilliOHM" ->
            Just "mΩ"

        "http://qudt.org/vocab/unit/PA-M-PER-SEC" ->
            Just "Pa⋅m/s"

        "http://qudt.org/vocab/unit/MilliBAR-M3-PER-SEC" ->
            Just "mbar⋅m³/s"

        "http://qudt.org/vocab/unit/TON_FG" ->
            Just "TOR"

        "http://qudt.org/vocab/unit/PlanckCurrentDensity" ->
            Just "planckcurrentdensity"

        "http://qudt.org/vocab/unit/MO" ->
            Just "mo"

        "http://qudt.org/vocab/unit/NanoW" ->
            Just "nW"

        "http://qudt.org/vocab/unit/NanoM-PER-CentiM-MegaPA" ->
            Just "nm/(cm⋅MPa)"

        "http://qudt.org/vocab/unit/HR_Sidereal" ->
            Just "hr{sidereal}"

        "http://qudt.org/vocab/unit/GigaC-PER-M3" ->
            Just "GC/m³"

        "http://qudt.org/vocab/unit/HZ-M" ->
            Just "Hz⋅m"

        "http://qudt.org/vocab/unit/BQ-SEC-PER-M3" ->
            Just "Bq⋅s/m³"

        "http://qudt.org/vocab/unit/TON_FG-HR" ->
            Just "TOR⋅hr"

        "http://qudt.org/vocab/unit/BTU_IT-IN-PER-FT2-HR-DEG_F" ->
            Just "Btu{IT}⋅in/(ft²⋅hr⋅°F)"

        "http://qudt.org/vocab/unit/GM-PER-DAY" ->
            Just "g/day"

        "http://qudt.org/vocab/unit/MilliBAR-PER-K" ->
            Just "mbar/K"

        "http://qudt.org/vocab/unit/J" ->
            Just "J"

        "http://qudt.org/vocab/unit/MilliA-HR-PER-GM" ->
            Just "mA⋅h/g"

        "http://qudt.org/vocab/unit/QT_UK-PER-MIN" ->
            Just "qt{UK}/min"

        "http://qudt.org/vocab/unit/OZ-PER-YD3" ->
            Just "oz/yd³"

        "http://qudt.org/vocab/unit/PDL" ->
            Just "pdl"

        "http://qudt.org/vocab/unit/CD-PER-M2" ->
            Just "cd/m²"

        "http://qudt.org/vocab/unit/OHM_Stat" ->
            Just "statΩ"

        "http://qudt.org/vocab/unit/CentiM6" ->
            Just "cm⁶"

        "http://qudt.org/vocab/unit/NanoGM-PER-M2-PA-SEC" ->
            Just "kg/(m²⋅s⋅Pa)"

        "http://qudt.org/vocab/unit/BBL_US-PER-DAY" ->
            Just "bbl{US petroleum}/day"

        "http://qudt.org/vocab/unit/PicoMOL" ->
            Just "pmol"

        "http://qudt.org/vocab/unit/TonEnergy" ->
            Just "t/lbf"

        "http://qudt.org/vocab/unit/W-HR-PER-M3" ->
            Just "W⋅hr/m³"

        "http://qudt.org/vocab/unit/K-M-PER-SEC" ->
            Just "K⋅m/s"

        "http://qudt.org/vocab/unit/MOL-PER-KiloGM" ->
            Just "mol/kg"

        "http://qudt.org/vocab/unit/KiloGM_F-PER-CentiM2" ->
            Just "kgf/cm²"

        "http://qudt.org/vocab/unit/FATH" ->
            Just "fathom"

        "http://qudt.org/vocab/unit/GRAY" ->
            Just "Gy"

        "http://qudt.org/vocab/unit/BTU_IT-PER-FT2-HR-DEG_F" ->
            Just "Btu{IT}/(ft²⋅hr⋅°F)"

        "http://qudt.org/vocab/unit/BTU_IT-PER-MOL_LB" ->
            Just "Btu{IT}/(mol-lb)"

        "http://qudt.org/vocab/unit/TON_SHIPPING_US" ->
            Just "MTON"

        "http://qudt.org/vocab/unit/PER-K" ->
            Just "/K"

        "http://qudt.org/vocab/unit/KiloN-PER-M3" ->
            Just "kN/m³"

        "http://qudt.org/vocab/unit/DeciB" ->
            Just "dB"

        "http://qudt.org/vocab/unit/MilliM2-PER-SEC" ->
            Just "mm²/s"

        "http://qudt.org/vocab/unit/IN-PER-DEG_F" ->
            Just "in/°F"

        "http://qudt.org/vocab/unit/L-PER-MIN" ->
            Just "L/min"

        "http://qudt.org/vocab/unit/CentiMOL-PER-L" ->
            Just "cmol/L"

        "http://qudt.org/vocab/unit/MilliL-PER-KiloGM" ->
            Just "mL/kg"

        "http://qudt.org/vocab/unit/BTU_IT-PER-LB-DEG_F" ->
            Just "Btu{IT}/(lbm⋅°F)"

        "http://qudt.org/vocab/unit/PebiBYTE" ->
            Just "PiB"

        "http://qudt.org/vocab/unit/KiloLB_F-PER-IN2" ->
            Just "klbf/in²"

        "http://qudt.org/vocab/unit/KiloGM-PER-M2-SEC" ->
            Just "kg/(m²⋅s)"

        "http://qudt.org/vocab/unit/MilliM-PER-SEC" ->
            Just "mm/s"

        "http://qudt.org/vocab/unit/KiloCAL_TH-PER-HR" ->
            Just "kcal{th}/hr"

        "http://qudt.org/vocab/unit/OERSTED" ->
            Just "Oe"

        "http://qudt.org/vocab/unit/C" ->
            Just "C"

        "http://qudt.org/vocab/unit/KIP_F-PER-IN2" ->
            Just "kip/in²"

        "http://qudt.org/vocab/unit/GM-PER-M" ->
            Just "g/m"

        "http://qudt.org/vocab/unit/GM-PER-CentiM2-YR" ->
            Just "g/(cm²⋅yr)"

        "http://qudt.org/vocab/unit/KiloBTU_TH-PER-HR" ->
            Just "kBtu{th}/hr"

        "http://qudt.org/vocab/unit/LB_M" ->
            Just "lbm"

        "http://qudt.org/vocab/unit/MilliBQ-PER-KiloGM" ->
            Just "mBq/kg"

        "http://qudt.org/vocab/unit/KiloA-PER-M2" ->
            Just "kA/m²"

        "http://qudt.org/vocab/unit/R_man" ->
            Just "rem"

        "http://qudt.org/vocab/unit/CAL_TH-PER-MIN" ->
            Just "cal/min"

        "http://qudt.org/vocab/unit/MilliGM-PER-HA" ->
            Just "mg/ha"

        "http://qudt.org/vocab/unit/Quarter_UK" ->
            Just "quarter"

        "http://qudt.org/vocab/unit/N-PER-M" ->
            Just "N/m"

        "http://qudt.org/vocab/unit/BEAT-PER-MIN" ->
            Just "BPM"

        "http://qudt.org/vocab/unit/PK_UK-PER-SEC" ->
            Just "peck{UK}/s"

        "http://qudt.org/vocab/unit/FT3" ->
            Just "ft³"

        "http://qudt.org/vocab/unit/THM_US" ->
            Just "thm{US}"

        "http://qudt.org/vocab/unit/GigaPA" ->
            Just "GPa"

        "http://qudt.org/vocab/unit/DEG_F-HR" ->
            Just "°F⋅hr"

        "http://qudt.org/vocab/unit/V_Ab-SEC" ->
            Just "abV⋅s"

        "http://qudt.org/vocab/unit/PER-M2" ->
            Just "/m²"

        "http://qudt.org/vocab/unit/DEG-PER-HR" ->
            Just "°/hr"

        "http://qudt.org/vocab/unit/PA2-PER-SEC2" ->
            Just "Pa²/s²"

        "http://qudt.org/vocab/unit/WB-M" ->
            Just "Wb⋅m"

        "http://qudt.org/vocab/unit/PER-MilliM3" ->
            Just "/mm³"

        "http://qudt.org/vocab/unit/BBL_UK_PET-PER-HR" ->
            Just "bbl{UK petroleum}/hr"

        "http://qudt.org/vocab/unit/NUM-PER-GM" ->
            Just "/g"

        "http://qudt.org/vocab/unit/K-PER-SEC2" ->
            Just "K/s²"

        "http://qudt.org/vocab/unit/GigaBasePair" ->
            Just "Gbp"

        "http://qudt.org/vocab/unit/KiloCAL-PER-MOL-DEG_C" ->
            Just "kcal/(mol⋅°C)"

        "http://qudt.org/vocab/unit/QT_US-PER-SEC" ->
            Just "qt/s"

        "http://qudt.org/vocab/unit/BARAD" ->
            Just "Ba"

        "http://qudt.org/vocab/unit/L-PER-DAY" ->
            Just "L/day"

        "http://qudt.org/vocab/unit/MilliMOL-PER-M2-SEC" ->
            Just "mmol/(m²⋅s)"

        "http://qudt.org/vocab/unit/HectoPA-PER-HR" ->
            Just "hPa/hr"

        "http://qudt.org/vocab/unit/OZ-FT" ->
            Just "oz⋅ft"

        "http://qudt.org/vocab/unit/MicroFARAD" ->
            Just "μF"

        "http://qudt.org/vocab/unit/ANGSTROM" ->
            Just "Å"

        "http://qudt.org/vocab/unit/DEG_C-PER-K" ->
            Just "°C/K"

        "http://qudt.org/vocab/unit/OCT" ->
            Just "oct"

        "http://qudt.org/vocab/unit/L-PER-L" ->
            Just "L/L"

        "http://qudt.org/vocab/unit/MegaTOE" ->
            Just "Mtoe"

        "http://qudt.org/vocab/unit/FT3-PER-SEC" ->
            Just "ft³/s"

        "http://qudt.org/vocab/unit/CentiM-PER-SEC" ->
            Just "cm/s"

        "http://qudt.org/vocab/unit/BAR-PER-K" ->
            Just "bar/K"

        "http://qudt.org/vocab/unit/NanoM-PER-CentiM-PSI" ->
            Just "nm/(cm⋅psi)"

        "http://qudt.org/vocab/unit/LB_F-PER-LB" ->
            Just "lbf/lbm"

        "http://qudt.org/vocab/unit/KiloV-A_Reactive-HR" ->
            Just "kV⋅A{Reactive}⋅hr"

        "http://qudt.org/vocab/unit/PicoFARAD" ->
            Just "pF"

        "http://qudt.org/vocab/unit/BTU_TH-PER-LB" ->
            Just "Btu{th}/lbm"

        "http://qudt.org/vocab/unit/TON_UK-PER-YD3" ->
            Just "ton{UK}/yd³"

        "http://qudt.org/vocab/unit/MegaV-A_Reactive-HR" ->
            Just "MV⋅A{Reactive}⋅hr"

        "http://qudt.org/vocab/unit/M2-SR" ->
            Just "m²⋅sr"

        "http://qudt.org/vocab/unit/MegaV-PER-M" ->
            Just "MV/m"

        "http://qudt.org/vocab/unit/N-SEC-PER-M3" ->
            Just "N⋅s/m³"

        "http://qudt.org/vocab/unit/MilliSV" ->
            Just "mSv"

        "http://qudt.org/vocab/unit/NUM-PER-MilliGM" ->
            Just "/mg"

        "http://qudt.org/vocab/unit/A-PER-J" ->
            Just "A/J"

        "http://qudt.org/vocab/unit/N-PER-KiloGM" ->
            Just "N/kg"

        "http://qudt.org/vocab/unit/BU_UK" ->
            Just "bsh{UK}"

        "http://qudt.org/vocab/unit/Denier" ->
            Just "D"

        "http://qudt.org/vocab/unit/LB-FT2" ->
            Just "lbm⋅ft²"

        "http://qudt.org/vocab/unit/DEG_F-PER-HR" ->
            Just "°F/hr"

        "http://qudt.org/vocab/unit/BTU_IT" ->
            Just "Btu{IT}"

        "http://qudt.org/vocab/unit/MegaV-A_Reactive" ->
            Just "MV⋅A{Reactive}"

        "http://qudt.org/vocab/unit/MicroBAR" ->
            Just "μbar"

        "http://qudt.org/vocab/unit/BU_UK-PER-DAY" ->
            Just "bsh{UK}/day"

        "http://qudt.org/vocab/unit/MI-PER-MIN" ->
            Just "mi/min"

        "http://qudt.org/vocab/unit/S-PER-CentiM" ->
            Just "S/cm"

        "http://qudt.org/vocab/unit/DeciTONNE" ->
            Just "dt"

        "http://qudt.org/vocab/unit/TeraBYTE" ->
            Just "TB"

        "http://qudt.org/vocab/unit/MilliMOL-PER-M3-DAY" ->
            Just "mmol/(m³⋅day)"

        "http://qudt.org/vocab/unit/DEG_C-KiloGM-PER-M2" ->
            Just "°C⋅kg/m²"

        "http://qudt.org/vocab/unit/V_Stat-PER-CentiM" ->
            Just "statV/cm"

        "http://qudt.org/vocab/unit/PK_UK-PER-HR" ->
            Just "peck{UK}/hr"

        "http://qudt.org/vocab/unit/PlanckVolt" ->
            Just "Vₚ"

        "http://qudt.org/vocab/unit/W-PER-M2-K4" ->
            Just "W/(m²⋅K⁴)"

        "http://qudt.org/vocab/unit/MicroMOL-PER-SEC" ->
            Just "µmol/s"

        "http://qudt.org/vocab/unit/EarthMass" ->
            Just "M⊕"

        "http://qudt.org/vocab/unit/FM" ->
            Just "fm"

        "http://qudt.org/vocab/unit/BARN" ->
            Just "b"

        "http://qudt.org/vocab/unit/DecaM" ->
            Just "dam"

        "http://qudt.org/vocab/unit/PA-M" ->
            Just "Pa⋅m"

        "http://qudt.org/vocab/unit/AT" ->
            Just "AT"

        "http://qudt.org/vocab/unit/GRAIN-PER-M3" ->
            Just "gr{UK}/m³"

        "http://qudt.org/vocab/unit/N-M-PER-M" ->
            Just "N⋅m/m"

        "http://qudt.org/vocab/unit/CentiM_HG" ->
            Just "cmHg"

        "http://qudt.org/vocab/unit/IN4" ->
            Just "in⁴"

        "http://qudt.org/vocab/unit/C-PER-MilliM2" ->
            Just "C/mm²"

        "http://qudt.org/vocab/unit/DEG2" ->
            Just "°²"

        "http://qudt.org/vocab/unit/SEC-FT2" ->
            Just "s⋅ft²"

        "http://qudt.org/vocab/unit/FemtoMOL" ->
            Just "fmol"

        "http://qudt.org/vocab/unit/KiloLB_F" ->
            Just "klbf"

        "http://qudt.org/vocab/unit/MilliW-PER-M2-NanoM" ->
            Just "mW/(m²⋅nm)"

        "http://qudt.org/vocab/unit/DEG_F" ->
            Just "°F"

        "http://qudt.org/vocab/unit/PERCENT-PER-DAY" ->
            Just "%/day"

        "http://qudt.org/vocab/unit/KiloGM_F-M-PER-CentiM2" ->
            Just "kgf⋅m/cm²"

        "http://qudt.org/vocab/unit/BTU_IT-PER-LB_F-DEG_F" ->
            Just "Btu{IT}/(lbf⋅°F)"

        "http://qudt.org/vocab/unit/CentiM2-SEC" ->
            Just "cm²⋅s"

        "http://qudt.org/vocab/unit/NanoGM" ->
            Just "ng"

        "http://qudt.org/vocab/unit/W-PER-K" ->
            Just "W/K"

        "http://qudt.org/vocab/unit/REV-PER-SEC" ->
            Just "rev/s"

        "http://qudt.org/vocab/unit/MilliSEC" ->
            Just "ms"

        "http://qudt.org/vocab/unit/MOHM" ->
            Just "mohm"

        "http://qudt.org/vocab/unit/J-PER-MOL-K" ->
            Just "J/(mol⋅K)"

        "http://qudt.org/vocab/unit/MilliBQ" ->
            Just "mBq"

        "http://qudt.org/vocab/unit/CentiM-PER-HR" ->
            Just "cm/hr"

        "http://qudt.org/vocab/unit/GALILEO" ->
            Just "Gal"

        "http://qudt.org/vocab/unit/FA" ->
            Just "fa"

        "http://qudt.org/vocab/unit/PCA" ->
            Just "pc"

        "http://qudt.org/vocab/unit/C-PER-M" ->
            Just "C/m"

        "http://qudt.org/vocab/unit/MilliW-PER-MilliGM" ->
            Just "mW/mg"

        "http://qudt.org/vocab/unit/DEG_C-WK" ->
            Just "°C⋅wk"

        "http://qudt.org/vocab/unit/NUM-PER-L" ->
            Just "/L"

        "http://qudt.org/vocab/unit/M3-PER-SEC2" ->
            Just "m³/s²"

        "http://qudt.org/vocab/unit/BAR" ->
            Just "bar"

        "http://qudt.org/vocab/unit/KiloGM-PER-M" ->
            Just "kg/m"

        "http://qudt.org/vocab/unit/BU_US_DRY-PER-DAY" ->
            Just "bsh{US}/day"

        "http://qudt.org/vocab/unit/J-SEC-PER-MOL" ->
            Just "J⋅s/mol"

        "http://qudt.org/vocab/unit/CAL_TH-PER-SEC-CentiM-K" ->
            Just "cal/(s⋅cm⋅K)"

        "http://qudt.org/vocab/unit/PER-WB" ->
            Just "/Wb"

        "http://qudt.org/vocab/unit/HZ" ->
            Just "Hz"

        "http://qudt.org/vocab/unit/KiloCAL-PER-SEC" ->
            Just "kcal/s"

        "http://qudt.org/vocab/unit/C3-M-PER-J2" ->
            Just "C³⋅m/J²"

        "http://qudt.org/vocab/unit/KiloCAL_IT" ->
            Just "kcal{IT}"

        "http://qudt.org/vocab/unit/MilliRAD_R-PER-HR" ->
            Just "mrad/hr"

        "http://qudt.org/vocab/unit/A-PER-GM" ->
            Just "A/g"

        "http://qudt.org/vocab/unit/MilliL-PER-GM" ->
            Just "mL/g"

        "http://qudt.org/vocab/unit/GI_UK-PER-MIN" ->
            Just "gill{UK}/min"

        "http://qudt.org/vocab/unit/MOL-PER-SEC" ->
            Just "mol/s"

        "http://qudt.org/vocab/unit/LB-MOL-DEG_F" ->
            Just "lb-mol⋅°F"

        "http://qudt.org/vocab/unit/MilliW-PER-CentiM2-MicroM-SR" ->
            Just "mW/(cm²⋅µm⋅sr)"

        "http://qudt.org/vocab/unit/MilliGM-PER-DAY" ->
            Just "mg/day"

        "http://qudt.org/vocab/unit/MilliR" ->
            Just "mR"

        "http://qudt.org/vocab/unit/TeraHZ" ->
            Just "THz"

        "http://qudt.org/vocab/unit/V_Stat" ->
            Just "statV"

        "http://qudt.org/vocab/unit/FT-LB_F-PER-M2" ->
            Just "ft⋅lbf/m²"

        "http://qudt.org/vocab/unit/NanoMOL" ->
            Just "nmol"

        "http://qudt.org/vocab/unit/N-M-SEC-PER-M" ->
            Just "N⋅m⋅s/m"

        "http://qudt.org/vocab/unit/N-PER-RAD" ->
            Just "N/rad"

        "http://qudt.org/vocab/unit/MilliW" ->
            Just "mW"

        "http://qudt.org/vocab/unit/PER-J2" ->
            Just "/J²"

        "http://qudt.org/vocab/unit/GI" ->
            Just "Gb"

        "http://qudt.org/vocab/unit/J-PER-KiloGM-K-PA" ->
            Just "J/(kg⋅K⋅Pa)"

        "http://qudt.org/vocab/unit/TON_Metric-PER-HA" ->
            Just "t/ha"

        "http://qudt.org/vocab/unit/PER-MOL" ->
            Just "/mol"

        "http://qudt.org/vocab/unit/KiloP" ->
            Just "kP"

        "http://qudt.org/vocab/unit/GI_UK-PER-HR" ->
            Just "gill{UK}/hr"

        "http://qudt.org/vocab/unit/FT3-PER-MIN" ->
            Just "ft³/min"

        "http://qudt.org/vocab/unit/CentiM3-PER-HR" ->
            Just "cm³/hr"

        "http://qudt.org/vocab/unit/BTU_IT-IN" ->
            Just "Btu{IT}⋅in"

        "http://qudt.org/vocab/unit/C4-M4-PER-J3" ->
            Just "C⁴⋅m⁴/J³"

        "http://qudt.org/vocab/unit/GM-PER-CentiM2" ->
            Just "g/cm²"

        "http://qudt.org/vocab/unit/V_Ab-PER-CentiM" ->
            Just "abV/cm"

        "http://qudt.org/vocab/unit/MOL-PER-DeciM3" ->
            Just "mol/dm³"

        "http://qudt.org/vocab/unit/HP_Boiler" ->
            Just "HP{boiler}"

        "http://qudt.org/vocab/unit/KiloBQ" ->
            Just "kBq"

        "http://qudt.org/vocab/unit/KiloCAL-PER-CentiM2-MIN" ->
            Just "kcal/(cm²⋅min)"

        "http://qudt.org/vocab/unit/BTU_TH" ->
            Just "Btu{th}"

        "http://qudt.org/vocab/unit/BTU_IT-IN-PER-FT2-SEC-DEG_F" ->
            Just "Btu{IT}⋅in/(ft²⋅s⋅°F)"

        "http://qudt.org/vocab/unit/S_Ab" ->
            Just "aS"

        "http://qudt.org/vocab/unit/MilliWB" ->
            Just "mWb"

        "http://qudt.org/vocab/unit/MegaA" ->
            Just "MA"

        "http://qudt.org/vocab/unit/CentiMOL" ->
            Just "cmol"

        "http://qudt.org/vocab/unit/TEX" ->
            Just "tex"

        "http://qudt.org/vocab/unit/HectoPA-PER-K" ->
            Just "hPa/K"

        "http://qudt.org/vocab/unit/GM-PER-MilliM" ->
            Just "g/mm"

        "http://qudt.org/vocab/unit/GM-PER-KiloGM" ->
            Just "g/kg"

        "http://qudt.org/vocab/unit/KiloS" ->
            Just "kS"

        "http://qudt.org/vocab/unit/L-PER-SEC" ->
            Just "L/s"

        "http://qudt.org/vocab/unit/MicroMOL-PER-GM" ->
            Just "µmol/g"

        "http://qudt.org/vocab/unit/TeraC" ->
            Just "TC"

        "http://qudt.org/vocab/unit/KiloGM-PER-KiloMOL" ->
            Just "kg/kmol"

        "http://qudt.org/vocab/unit/ERG-PER-CentiM" ->
            Just "erg/cm"

        "http://qudt.org/vocab/unit/PER-SEC-M2" ->
            Just "/(s⋅m²)"

        "http://qudt.org/vocab/unit/OZ" ->
            Just "oz"

        "http://qudt.org/vocab/unit/PER-SEC-M2-SR" ->
            Just "/(s⋅m²⋅sr)"

        "http://qudt.org/vocab/unit/MegaL" ->
            Just "ML"

        "http://qudt.org/vocab/unit/PetaJ" ->
            Just "PJ"

        "http://qudt.org/vocab/unit/MicroGM-PER-CentiM2-WK" ->
            Just "μg/(cm²⋅wk)"

        "http://qudt.org/vocab/unit/KiloCAL_Mean" ->
            Just "kcal{mean}"

        "http://qudt.org/vocab/unit/W-PER-KiloGM" ->
            Just "W/kg"

        "http://qudt.org/vocab/unit/NanoMOL-PER-KiloGM" ->
            Just "nmol/kg"

        "http://qudt.org/vocab/unit/MESH" ->
            Just "mesh"

        "http://qudt.org/vocab/unit/MilliL-PER-CentiM2-MIN" ->
            Just "mL/(cm²⋅min)"

        "http://qudt.org/vocab/unit/MI_N" ->
            Just "nmi"

        "http://qudt.org/vocab/unit/HectoPA-L-PER-SEC" ->
            Just "hPa⋅L/s"

        "http://qudt.org/vocab/unit/PINT_US-PER-SEC" ->
            Just "pt{US}/s"

        "http://qudt.org/vocab/unit/K-PER-K" ->
            Just "K/K"

        "http://qudt.org/vocab/unit/KAT-PER-L" ->
            Just "kat/L"

        "http://qudt.org/vocab/unit/BTU_IT-PER-LB" ->
            Just "Btu{IT}/lb"

        "http://qudt.org/vocab/unit/KiloWB-PER-M" ->
            Just "kWb/m"

        "http://qudt.org/vocab/unit/KiloS-PER-M" ->
            Just "kS/m"

        "http://qudt.org/vocab/unit/GI_UK-PER-SEC" ->
            Just "gill{UK}/s"

        "http://qudt.org/vocab/unit/OZ-PER-YD2" ->
            Just "oz/yd³{US}"

        "http://qudt.org/vocab/unit/NUM-PER-NanoL" ->
            Just "/nL"

        "http://qudt.org/vocab/unit/MegaGM" ->
            Just "Mg"

        "http://qudt.org/vocab/unit/CentiN-M" ->
            Just "cN⋅m"

        "http://qudt.org/vocab/unit/NanoGM-PER-KiloGM" ->
            Just "ng/Kg"

        "http://qudt.org/vocab/unit/ERG-PER-GM" ->
            Just "erg/g"

        "http://qudt.org/vocab/unit/MicroS-PER-M" ->
            Just "μS/m"

        "http://qudt.org/vocab/unit/CentiL" ->
            Just "cL"

        "http://qudt.org/vocab/unit/MilliGM-PER-HR" ->
            Just "mg/hr"

        "http://qudt.org/vocab/unit/GM-PER-DEG_C" ->
            Just "g/°C"

        "http://qudt.org/vocab/unit/M2-PER-V-SEC" ->
            Just "m²/(V⋅s)"

        "http://qudt.org/vocab/unit/K-PER-T" ->
            Just "K/T"

        "http://qudt.org/vocab/unit/PER-MilliL" ->
            Just "/mL"

        "http://qudt.org/vocab/unit/C-M2-PER-V" ->
            Just "C⋅m²/V"

        "http://qudt.org/vocab/unit/BTU_IT-PER-MOL_LB-DEG_F" ->
            Just "Btu{IT}/(lb-mol⋅°F)"

        "http://qudt.org/vocab/unit/FemtoJ" ->
            Just "fJ"

        "http://qudt.org/vocab/unit/FC" ->
            Just "fc"

        "http://qudt.org/vocab/unit/NanoBQ-PER-L" ->
            Just "nBq/L"

        "http://qudt.org/vocab/unit/RAD-PER-SEC2" ->
            Just "rad/s²"

        "http://qudt.org/vocab/unit/BTU_IT-PER-DEG_R" ->
            Just "Btu{IT}/°R"

        "http://qudt.org/vocab/unit/NanoM2" ->
            Just "nm²"

        "http://qudt.org/vocab/unit/SAMPLE-PER-SEC" ->
            Just "sample/s"

        "http://qudt.org/vocab/unit/MicroKAT-PER-L" ->
            Just "µkat/L"

        "http://qudt.org/vocab/unit/BTU_IT-PER-HR" ->
            Just "Btu{IT}/hr"

        "http://qudt.org/vocab/unit/GON" ->
            Just "gon"

        "http://qudt.org/vocab/unit/YD3-PER-DEG_F" ->
            Just "yd³/°F"

        "http://qudt.org/vocab/unit/PER-GigaEV2" ->
            Just "/GeV²"

        "http://qudt.org/vocab/unit/KiloGM-PER-KiloM2" ->
            Just "kg/km²"

        "http://qudt.org/vocab/unit/MDOLLAR-PER-FLIGHT" ->
            Just "$M/flight"

        "http://qudt.org/vocab/unit/CentiM2" ->
            Just "cm²"

        "http://qudt.org/vocab/unit/DeciM3" ->
            Just "dm³"

        "http://qudt.org/vocab/unit/A-PER-CentiM2" ->
            Just "A/cm²"

        "http://qudt.org/vocab/unit/MI_N-PER-MIN" ->
            Just "nmi/min"

        "http://qudt.org/vocab/unit/MI_US" ->
            Just "mi{US}"

        "http://qudt.org/vocab/unit/CentiM-PER-YR" ->
            Just "cm/yr"

        "http://qudt.org/vocab/unit/PER-NanoM" ->
            Just "/nm"

        "http://qudt.org/vocab/unit/M-KiloGM" ->
            Just "m⋅kg"

        "http://qudt.org/vocab/unit/N-M-SEC-PER-RAD" ->
            Just "N⋅m⋅s/rad"

        "http://qudt.org/vocab/unit/BTU_IT-IN-PER-SEC-FT2-DEG_F" ->
            Just "Btu{IT}⋅in/(s⋅ft²⋅°F)"

        "http://qudt.org/vocab/unit/H_Stat-PER-CentiM" ->
            Just "statH/cm"

        "http://qudt.org/vocab/unit/FT2-HR-DEG_F" ->
            Just "ft²⋅hr⋅°F"

        "http://qudt.org/vocab/unit/MilliMOL-PER-KiloGM" ->
            Just "mmol/kg"

        "http://qudt.org/vocab/unit/PER-T-M" ->
            Just "/(T⋅m)"

        "http://qudt.org/vocab/unit/BTU_TH-IN-PER-FT2-SEC-DEG_F" ->
            Just "Btu{th}⋅in/(ft²⋅s⋅°F)"

        "http://qudt.org/vocab/unit/BU_UK-PER-HR" ->
            Just "bsh{UK}/hr"

        "http://qudt.org/vocab/unit/K-PER-SEC" ->
            Just "K/s"

        "http://qudt.org/vocab/unit/PH" ->
            Just "pH"

        "http://qudt.org/vocab/unit/PicoL" ->
            Just "pL"

        "http://qudt.org/vocab/unit/KN-PER-SEC" ->
            Just "kn/s"

        "http://qudt.org/vocab/unit/MACH" ->
            Just "Mach"

        "http://qudt.org/vocab/unit/GAL_IMP" ->
            Just "gal{Imp}"

        "http://qudt.org/vocab/unit/IN3-PER-HR" ->
            Just "in³/hr"

        "http://qudt.org/vocab/unit/PicoKAT-PER-L" ->
            Just "pkat/L"

        "http://qudt.org/vocab/unit/LB_F-IN" ->
            Just "lbf⋅in"

        "http://qudt.org/vocab/unit/OZ_VOL_UK-PER-HR" ->
            Just "oz{UK}/hr"

        "http://qudt.org/vocab/unit/MilliBQ-PER-M2-DAY" ->
            Just "mBq/(m²⋅day)"

        "http://qudt.org/vocab/unit/KiloV-PER-M" ->
            Just "kV/m"

        "http://qudt.org/vocab/unit/GibiBYTE" ->
            Just "GiB"

        "http://qudt.org/vocab/unit/SLUG-PER-SEC" ->
            Just "slug/s"

        "http://qudt.org/vocab/unit/DEG_C" ->
            Just "°C"

        "http://qudt.org/vocab/unit/MicroGM-PER-L" ->
            Just "μg/L"

        "http://qudt.org/vocab/unit/A-PER-MilliM" ->
            Just "A/mm"

        "http://qudt.org/vocab/unit/PER-PSI" ->
            Just "/psi"

        "http://qudt.org/vocab/unit/RAD-PER-M" ->
            Just "rad/m"

        "http://qudt.org/vocab/unit/PicoGM-PER-KiloGM" ->
            Just "pg/kg"

        "http://qudt.org/vocab/unit/PetaBYTE" ->
            Just "PB"

        "http://qudt.org/vocab/unit/PlanckImpedance" ->
            Just "Zₚ"

        "http://qudt.org/vocab/unit/QT_US-PER-MIN" ->
            Just "qt/min"

        "http://qudt.org/vocab/unit/GM-PER-DeciL" ->
            Just "g/dL"

        "http://qudt.org/vocab/unit/PERCENT-PER-M" ->
            Just "%/m"

        "http://qudt.org/vocab/unit/GM-PER-M2-DAY" ->
            Just "g/(m²⋅day)"

        "http://qudt.org/vocab/unit/QT_UK" ->
            Just "qt{UK}"

        "http://qudt.org/vocab/unit/TONNE-PER-DAY" ->
            Just "t/day"

        "http://qudt.org/vocab/unit/KiloGM-M2-PER-SEC" ->
            Just "kg⋅m²/s"

        "http://qudt.org/vocab/unit/ST" ->
            Just "St"

        "http://qudt.org/vocab/unit/MicroMOL-PER-M2-HR" ->
            Just "µmol/(m²⋅hr)"

        "http://qudt.org/vocab/unit/DEG_F-HR-PER-BTU_IT" ->
            Just "°F⋅hr/Btu{IT}"

        "http://qudt.org/vocab/unit/MilliM-PER-HR" ->
            Just "mm/hr"

        "http://qudt.org/vocab/unit/DeciM3-PER-MIN" ->
            Just "dm³/min"

        "http://qudt.org/vocab/unit/EV-PER-T" ->
            Just "eV/T"

        "http://qudt.org/vocab/unit/PER-SR" ->
            Just "/sr"

        "http://qudt.org/vocab/unit/THM_US-PER-HR" ->
            Just "thm{US}/hr"

        "http://qudt.org/vocab/unit/MegaGM-PER-HA" ->
            Just "Mg/ha"

        "http://qudt.org/vocab/unit/K-M" ->
            Just "K⋅m"

        "http://qudt.org/vocab/unit/CentiM" ->
            Just "cm"

        "http://qudt.org/vocab/unit/MegaPA-PER-K" ->
            Just "MPa/K"

        "http://qudt.org/vocab/unit/Debye" ->
            Just "D"

        "http://qudt.org/vocab/unit/MicroSEC" ->
            Just "µs"

        "http://qudt.org/vocab/unit/C_Stat" ->
            Just "statC"

        "http://qudt.org/vocab/unit/LB-IN2" ->
            Just "lbm⋅in²"

        "http://qudt.org/vocab/unit/PINT_UK-PER-MIN" ->
            Just "pt{UK}/min"

        "http://qudt.org/vocab/unit/E" ->
            Just "e"

        "http://qudt.org/vocab/unit/A_Ab-CentiM2" ->
            Just "abA⋅cm²"

        "http://qudt.org/vocab/unit/M-K" ->
            Just "m⋅K"

        "http://qudt.org/vocab/unit/KiloGM-PER-SEC" ->
            Just "kg/s"

        "http://qudt.org/vocab/unit/N-M2-PER-A" ->
            Just "N⋅m²/A"

        "http://qudt.org/vocab/unit/KiloMOL-PER-HR" ->
            Just "kmol/hr"

        "http://qudt.org/vocab/unit/MilliCi" ->
            Just "mCi"

        "http://qudt.org/vocab/unit/CD-PER-IN2" ->
            Just "cd/in²"

        "http://qudt.org/vocab/unit/PetaC" ->
            Just "PC"

        "http://qudt.org/vocab/unit/BTU_TH-PER-FT3" ->
            Just "Btu{th}/ft³"

        "http://qudt.org/vocab/unit/MegaGM-PER-M3" ->
            Just "Mg/m³"

        "http://qudt.org/vocab/unit/CentiM3-PER-GM" ->
            Just "cm³/g"

        "http://qudt.org/vocab/unit/MilLength" ->
            Just "mil"

        "http://qudt.org/vocab/unit/NanoT" ->
            Just "nT"

        "http://qudt.org/vocab/unit/PINT" ->
            Just "pt"

        "http://qudt.org/vocab/unit/LB-PER-FT" ->
            Just "lbm/ft"

        "http://qudt.org/vocab/unit/KAT-PER-MicroL" ->
            Just "kat/µL"

        "http://qudt.org/vocab/unit/TON_SHORT-PER-HR" ->
            Just "ton{short}/hr"

        "http://qudt.org/vocab/unit/M2-PER-MOL" ->
            Just "m²/mol"

        "http://qudt.org/vocab/unit/NanoGM-PER-CentiM2" ->
            Just "ng/cm²"

        "http://qudt.org/vocab/unit/KiloBTU_IT-PER-HR" ->
            Just "kBtu{IT}/hr"

        "http://qudt.org/vocab/unit/MilliGM-PER-L" ->
            Just "mg/L"

        "http://qudt.org/vocab/unit/KiloPA" ->
            Just "kPa"

        "http://qudt.org/vocab/unit/PicoGM" ->
            Just "pg"

        "http://qudt.org/vocab/unit/KiloGM-PER-MilliM" ->
            Just "kg/mm"

        "http://qudt.org/vocab/unit/DEATHS-PER-MegaINDIV-YR" ->
            Just "deaths/million individuals/yr"

        "http://qudt.org/vocab/unit/PK_UK-PER-MIN" ->
            Just "peck{UK}/min"

        "http://qudt.org/vocab/unit/MilliM-PER-MIN" ->
            Just "mm/min"

        "http://qudt.org/vocab/unit/TONNE-PER-HA-YR" ->
            Just "t/(ha⋅yr)"

        "http://qudt.org/vocab/unit/MicroIN" ->
            Just "μin"

        "http://qudt.org/vocab/unit/V-PER-K" ->
            Just "V/K"

        "http://qudt.org/vocab/unit/HectoC" ->
            Just "hC"

        "http://qudt.org/vocab/unit/KiloGM-PER-SEC3-K" ->
            Just "kg/(s³⋅K)"

        "http://qudt.org/vocab/unit/W-PER-M3" ->
            Just "W/m³"

        "http://qudt.org/vocab/unit/CentiMOL-PER-KiloGM" ->
            Just "cmol/kg"

        "http://qudt.org/vocab/unit/M3-PER-MOL" ->
            Just "m³/mol"

        "http://qudt.org/vocab/unit/K2" ->
            Just "K²"

        "http://qudt.org/vocab/unit/NanoGM-PER-CentiM2-DAY" ->
            Just "ng/(cm²⋅day)"

        "http://qudt.org/vocab/unit/N-PER-C" ->
            Just "N/C"

        "http://qudt.org/vocab/unit/QT_UK-PER-HR" ->
            Just "qt{UK}/hr"

        "http://qudt.org/vocab/unit/PER-H" ->
            Just "/H"

        "http://qudt.org/vocab/unit/MilliM3" ->
            Just "mm³"

        "http://qudt.org/vocab/unit/A-HR" ->
            Just "A⋅hr"

        "http://qudt.org/vocab/unit/NanoGM-PER-M3" ->
            Just "ng/m³"

        "http://qudt.org/vocab/unit/DecaGM" ->
            Just "dag"

        "http://qudt.org/vocab/unit/J-PER-GM-DEG_C" ->
            Just "J/(g⋅°C)"

        "http://qudt.org/vocab/unit/TSP" ->
            Just "tsp"

        "http://qudt.org/vocab/unit/KiloJ-PER-KiloGM" ->
            Just "kJ/kg"

        "http://qudt.org/vocab/unit/PER-L" ->
            Just "/L"

        "http://qudt.org/vocab/unit/ExaC" ->
            Just "EC"

        "http://qudt.org/vocab/unit/Standard" ->
            Just "standard"

        "http://qudt.org/vocab/unit/KiloGM-PER-L" ->
            Just "kg/L"

        "http://qudt.org/vocab/unit/FR" ->
            Just "Fr"

        "http://qudt.org/vocab/unit/NUM-PER-MilliM3" ->
            Just "/mm³"

        "http://qudt.org/vocab/unit/V-PER-SEC" ->
            Just "V/s"

        "http://qudt.org/vocab/unit/FT_US" ->
            Just "ft{US Survey}"

        "http://qudt.org/vocab/unit/DEG_F-PER-SEC" ->
            Just "°F/s"

        "http://qudt.org/vocab/unit/TON_Metric-PER-DAY" ->
            Just "t/day"

        "http://qudt.org/vocab/unit/ExaJ" ->
            Just "EJ"

        "http://qudt.org/vocab/unit/MicroGM-PER-M2-DAY" ->
            Just "μg/(m²⋅day)"

        "http://qudt.org/vocab/unit/MicroW" ->
            Just "µW"

        "http://qudt.org/vocab/unit/PER-M3" ->
            Just "/m³"

        "http://qudt.org/vocab/unit/MegaV-A" ->
            Just "MV⋅A"

        "http://qudt.org/vocab/unit/W-SEC-PER-M2" ->
            Just "W⋅s/m²"

        "http://qudt.org/vocab/unit/CentiBAR" ->
            Just "cbar"

        "http://qudt.org/vocab/unit/DEG_F-HR-FT2-PER-BTU_IT" ->
            Just "°F⋅hr⋅ft²/Btu{IT}"

        "http://qudt.org/vocab/unit/MicroN" ->
            Just "μN"

        "http://qudt.org/vocab/unit/PER-MIN" ->
            Just "/min"

        "http://qudt.org/vocab/unit/MicroGM-PER-M3-HR" ->
            Just "μg/(m³⋅hr)"

        "http://qudt.org/vocab/unit/A_Ab-PER-CentiM2" ->
            Just "abA/cm²"

        "http://qudt.org/vocab/unit/MilliRAD_R" ->
            Just "mrad"

        "http://qudt.org/vocab/unit/A_Ab" ->
            Just "abA"

        "http://qudt.org/vocab/unit/MilliRAD" ->
            Just "mrad"

        "http://qudt.org/vocab/unit/M3-PER-C" ->
            Just "m³/C"

        "http://qudt.org/vocab/unit/M3" ->
            Just "m³"

        "http://qudt.org/vocab/unit/DEG_F-HR-FT2-PER-BTU_TH" ->
            Just "°F⋅hr⋅ft²/Btu{th}"

        "http://qudt.org/vocab/unit/NUM-PER-KiloM2" ->
            Just "/km²"

        "http://qudt.org/vocab/unit/M3-PER-KiloGM-SEC2" ->
            Just "m³/(kg⋅s²)"

        "http://qudt.org/vocab/unit/MicroBQ-PER-L" ->
            Just "μBq/L"

        "http://qudt.org/vocab/unit/N-M-PER-KiloGM" ->
            Just "N⋅m/kg"

        "http://qudt.org/vocab/unit/GAL_UK-PER-DAY" ->
            Just "gal{UK}/day"

        "http://qudt.org/vocab/unit/MilliN" ->
            Just "mN"

        "http://qudt.org/vocab/unit/KiloC" ->
            Just "kC"

        "http://qudt.org/vocab/unit/TON_US" ->
            Just "ton{US}"

        "http://qudt.org/vocab/unit/N-PER-M2" ->
            Just "N/m²"

        "http://qudt.org/vocab/unit/M3-PER-K" ->
            Just "m³/K"

        "http://qudt.org/vocab/unit/A_Stat" ->
            Just "statA"

        "http://qudt.org/vocab/unit/BTU_MEAN" ->
            Just "BTU{mean}"

        "http://qudt.org/vocab/unit/PINT_US-PER-DAY" ->
            Just "pt{US}/day"

        "http://qudt.org/vocab/unit/A-PER-M2-K2" ->
            Just "A/(m²⋅K²)"

        "http://qudt.org/vocab/unit/GM_F-PER-CentiM2" ->
            Just "gf/cm²"

        "http://qudt.org/vocab/unit/Ci" ->
            Just "Ci"

        "http://qudt.org/vocab/unit/MilliBQ-PER-L" ->
            Just "mBq/L"

        "http://qudt.org/vocab/unit/failures-in-time" ->
            Just "failures/s"

        "http://qudt.org/vocab/unit/HectoGM" ->
            Just "hg"

        "http://qudt.org/vocab/unit/MicroGM-PER-CentiM2" ->
            Just "µG/cm²"

        "http://qudt.org/vocab/unit/W-PER-GM" ->
            Just "W/g"

        "http://qudt.org/vocab/unit/N-SEC-PER-RAD" ->
            Just "N⋅s/rad"

        "http://qudt.org/vocab/unit/KiloGM-PER-M3-SEC" ->
            Just "kg/(m³⋅s)"

        "http://qudt.org/vocab/unit/BARYE" ->
            Just "Ba"

        "http://qudt.org/vocab/unit/BU_US_DRY-PER-SEC" ->
            Just "bsh{US}/s"

        "http://qudt.org/vocab/unit/MilliGM-PER-SEC" ->
            Just "mg/s"

        "http://qudt.org/vocab/unit/PicoMOL-PER-L-DAY" ->
            Just "pmol/(L⋅day)"

        "http://qudt.org/vocab/unit/TORR" ->
            Just "Torr"

        "http://qudt.org/vocab/unit/J-M2-PER-KiloGM" ->
            Just "J⋅m²/kg"

        "http://qudt.org/vocab/unit/LM-SEC" ->
            Just "lm⋅s"

        "http://qudt.org/vocab/unit/NanoMOL-PER-MicroMOL-DAY" ->
            Just "nmol/(µmol⋅day)"

        "http://qudt.org/vocab/unit/SLUG-PER-HR" ->
            Just "slug/hr"

        "http://qudt.org/vocab/unit/KiloSEC" ->
            Just "ks"

        "http://qudt.org/vocab/unit/PA-SEC-PER-M" ->
            Just "Pa⋅s/m"

        "http://qudt.org/vocab/unit/Kilo-FT3" ->
            Just "k(ft³)"

        "http://qudt.org/vocab/unit/MegaBYTE" ->
            Just "MB"

        "http://qudt.org/vocab/unit/PER-KiloV-A-HR" ->
            Just "/(kV⋅A⋅hr)"

        "http://qudt.org/vocab/unit/KiloLB_F-FT-PER-A" ->
            Just "klbf⋅ft/A"

        "http://qudt.org/vocab/unit/TON_Metric-PER-SEC" ->
            Just "t/s"

        "http://qudt.org/vocab/unit/DeciM" ->
            Just "dm"

        "http://qudt.org/vocab/unit/N-M2-PER-KiloGM2" ->
            Just "N⋅m²/kg²"

        "http://qudt.org/vocab/unit/GI_US-PER-MIN" ->
            Just "gill{US}/min"

        "http://qudt.org/vocab/unit/LB-PER-GAL_US" ->
            Just "lbm/gal{US}"

        "http://qudt.org/vocab/unit/BBL_UK_PET-PER-SEC" ->
            Just "bbl{UK petroleum}/s"

        "http://qudt.org/vocab/unit/GRAIN-PER-GAL" ->
            Just "grain{UK}/gal"

        "http://qudt.org/vocab/unit/MilliM2" ->
            Just "mm²"

        "http://qudt.org/vocab/unit/W-PER-M2-NanoM" ->
            Just "W/(m²⋅nm)"

        "http://qudt.org/vocab/unit/QT_US-PER-DAY" ->
            Just "qt/day"

        "http://qudt.org/vocab/unit/Flight" ->
            Just "flight"

        "http://qudt.org/vocab/unit/N-SEC" ->
            Just "N⋅s"

        "http://qudt.org/vocab/unit/MicroGM-PER-KiloGM" ->
            Just "μg/kg"

        "http://qudt.org/vocab/unit/KiloGM_F-PER-MilliM2" ->
            Just "kgf/mm²"

        "http://qudt.org/vocab/unit/J-PER-SEC" ->
            Just "J/s"

        "http://qudt.org/vocab/unit/MicroMOL-PER-M2-SEC" ->
            Just "µmol/(m²⋅s)"

        "http://qudt.org/vocab/unit/W-SEC" ->
            Just "W⋅s"

        "http://qudt.org/vocab/unit/PERCENT-PER-WK" ->
            Just "%/wk"

        "http://qudt.org/vocab/unit/KiloPA-PER-BAR" ->
            Just "kPa/bar"

        "http://qudt.org/vocab/unit/MicroM3-PER-M3" ->
            Just "µm³/m³"

        "http://qudt.org/vocab/unit/MilliMOL-PER-M2" ->
            Just "mmol/m²"

        "http://qudt.org/vocab/unit/MegaPA-PER-BAR" ->
            Just "MPa/bar"

        "http://qudt.org/vocab/unit/C-PER-CentiM2" ->
            Just "C/cm²"

        "http://qudt.org/vocab/unit/LB-PER-M3" ->
            Just "lbm/m³"

        "http://qudt.org/vocab/unit/PPTH" ->
            Just "‰"

        "http://qudt.org/vocab/unit/PERCENT-PER-HR" ->
            Just "%/day"

        "http://qudt.org/vocab/unit/LB-PER-IN" ->
            Just "lbm/in"

        "http://qudt.org/vocab/unit/PSU" ->
            Just "PSU"

        "http://qudt.org/vocab/unit/TON_US-PER-DAY" ->
            Just "ton{US}/day"

        "http://qudt.org/vocab/unit/LB-PER-IN2" ->
            Just "lbm/in²"

        "http://qudt.org/vocab/unit/A-SEC" ->
            Just "A⋅s"

        "http://qudt.org/vocab/unit/PER-ANGSTROM" ->
            Just "/Å"

        "http://qudt.org/vocab/unit/MegaEV-FemtoM" ->
            Just "MeV⋅fm"

        "http://qudt.org/vocab/unit/KiloMOL-PER-M3" ->
            Just "kmol/m³"

        "http://qudt.org/vocab/unit/MilliMOL-PER-GM" ->
            Just "mmol/g"

        "http://qudt.org/vocab/unit/FARAD" ->
            Just "F"

        "http://qudt.org/vocab/unit/ERG-PER-CentiM3" ->
            Just "erg/cm³"

        "http://qudt.org/vocab/unit/LM" ->
            Just "lm"

        "http://qudt.org/vocab/unit/IN2-PER-SEC" ->
            Just "in²/s"

        "http://qudt.org/vocab/unit/A-PER-CentiM" ->
            Just "A/cm"

        "http://qudt.org/vocab/unit/A-M2-PER-J-SEC" ->
            Just "A⋅m²/(J⋅s)"

        "http://qudt.org/vocab/unit/KiloN-PER-M" ->
            Just "kN/m"

        "http://qudt.org/vocab/unit/KiloYR" ->
            Just "1000 yr"

        "http://qudt.org/vocab/unit/R" ->
            Just "R"

        "http://qudt.org/vocab/unit/V-A_Reactive" ->
            Just "V⋅A{Reactive}"

        "http://qudt.org/vocab/unit/M2-PER-W" ->
            Just "m²/W"

        "http://qudt.org/vocab/unit/BTU_IT-IN-PER-HR-FT2-DEG_F" ->
            Just "Btu{IT}⋅in/(hr⋅ft²⋅°F)"

        "http://qudt.org/vocab/unit/FT" ->
            Just "ft"

        "http://qudt.org/vocab/unit/J-PER-M3-K" ->
            Just "J/(m³⋅K)"

        "http://qudt.org/vocab/unit/BU_US" ->
            Just "bsh{US}"

        "http://qudt.org/vocab/unit/PER-MilliSEC" ->
            Just "/ms"

        "http://qudt.org/vocab/unit/BBL_US" ->
            Just "bbl{US petroleum}"

        "http://qudt.org/vocab/unit/KiloEV" ->
            Just "keV"

        "http://qudt.org/vocab/unit/MilliBAR-PER-BAR" ->
            Just "mbar/bar"

        "http://qudt.org/vocab/unit/BTU_IT-PER-DEG_F" ->
            Just "Btu{IT}/°F"

        "http://qudt.org/vocab/unit/BU_UK-PER-SEC" ->
            Just "bsh{UK}/s"

        "http://qudt.org/vocab/unit/FemtoGM-PER-L" ->
            Just "fg/L"

        "http://qudt.org/vocab/unit/DEG_C-CentiM" ->
            Just "°C⋅cm"

        "http://qudt.org/vocab/unit/MOL-PER-L" ->
            Just "mol/L"

        "http://qudt.org/vocab/unit/CWT_SHORT" ->
            Just "cwt{short}"

        "http://qudt.org/vocab/unit/GigaW" ->
            Just "GW"

        "http://qudt.org/vocab/unit/CentiM3-PER-SEC" ->
            Just "cm³/s"

        "http://qudt.org/vocab/unit/GM-PER-L" ->
            Just "g/L"

        "http://qudt.org/vocab/unit/MegaC-PER-M2" ->
            Just "MC/m²"

        "http://qudt.org/vocab/unit/PA2-SEC" ->
            Just "Pa²⋅s"

        "http://qudt.org/vocab/unit/KiloPA_A" ->
            Just "KPaA"

        "http://qudt.org/vocab/unit/M3-PER-DAY" ->
            Just "m³/day"

        "http://qudt.org/vocab/unit/MOL-DEG_C" ->
            Just "mol⋅°C"

        "http://qudt.org/vocab/unit/CentiM3-PER-MOL" ->
            Just "cm³/mol"

        "http://qudt.org/vocab/unit/M2-PER-K" ->
            Just "m²/K"

        "http://qudt.org/vocab/unit/J-PER-GM" ->
            Just "J/g"

        "http://qudt.org/vocab/unit/N-PER-CentiM" ->
            Just "N/cm"

        "http://qudt.org/vocab/unit/NUM-PER-SEC" ->
            Just "/s"

        "http://qudt.org/vocab/unit/SH" ->
            Just "shake"

        "http://qudt.org/vocab/unit/BQ-PER-KiloGM" ->
            Just "Bq/kg"

        "http://qudt.org/vocab/unit/V-A-HR" ->
            Just "V⋅A⋅hr"

        "http://qudt.org/vocab/unit/MicroGM" ->
            Just "μg"

        "http://qudt.org/vocab/unit/K-M-PER-W" ->
            Just "K⋅m/W"

        "http://qudt.org/vocab/unit/CentiN" ->
            Just "cN"

        "http://qudt.org/vocab/unit/KN" ->
            Just "kn"

        "http://qudt.org/vocab/unit/NanoS-PER-M" ->
            Just "nS/m"

        "http://qudt.org/vocab/unit/PPB" ->
            Just "PPB"

        "http://qudt.org/vocab/unit/NUM-PER-YR" ->
            Just "#/yr"

        "http://qudt.org/vocab/unit/J-PER-T" ->
            Just "J/T"

        "http://qudt.org/vocab/unit/PPTR_VOL" ->
            Just "pptr"

        "http://qudt.org/vocab/unit/BAR-PER-BAR" ->
            Just "bar/bar"

        "http://qudt.org/vocab/unit/OZ_VOL_UK-PER-SEC" ->
            Just "oz{UK}/s"

        "http://qudt.org/vocab/unit/KiloGM_F-PER-M2" ->
            Just "kgf/m²"

        "http://qudt.org/vocab/unit/SHANNON-PER-SEC" ->
            Just "Sh/s"

        "http://qudt.org/vocab/unit/SLUG-PER-FT2" ->
            Just "slug/ft²"

        "http://qudt.org/vocab/unit/PINT_UK-PER-SEC" ->
            Just "pt{UK}/s"

        "http://qudt.org/vocab/unit/SEC-PER-M" ->
            Just "s/m"

        "http://qudt.org/vocab/unit/PlanckLength" ->
            Just "plancklength"

        "http://qudt.org/vocab/unit/MOL-PER-M3" ->
            Just "mol/m³"

        "http://qudt.org/vocab/unit/CAL_IT-PER-GM" ->
            Just "cal{IT}/g"

        "http://qudt.org/vocab/unit/PK_US_DRY-PER-HR" ->
            Just "peck{US Dry}/hr"

        "http://qudt.org/vocab/unit/MicroN-M" ->
            Just "μN⋅m"

        "http://qudt.org/vocab/unit/N-M-PER-A" ->
            Just "N⋅m/A"

        "http://qudt.org/vocab/unit/MilliM-PER-K" ->
            Just "mm/K"

        "http://qudt.org/vocab/unit/MilliGAL-PER-MO" ->
            Just "mgal/mo"

        "http://qudt.org/vocab/unit/M2-PER-HZ-DEG" ->
            Just "m²/(Hz⋅°)"

        "http://qudt.org/vocab/unit/REM" ->
            Just "rem"

        "http://qudt.org/vocab/unit/MilliMOL" ->
            Just "mmol"

        "http://qudt.org/vocab/unit/OZ_VOL_US-PER-MIN" ->
            Just "fl oz{US}/min"

        "http://qudt.org/vocab/unit/CAL_IT-PER-SEC-CentiM2-K" ->
            Just "cal{IT}/(s⋅cm²⋅K)"

        "http://qudt.org/vocab/unit/GAL_UK-PER-SEC" ->
            Just "gal{UK}/s"

        "http://qudt.org/vocab/unit/PlanckCurrent" ->
            Just "planckcurrent"

        "http://qudt.org/vocab/unit/AC" ->
            Just "acre"

        "http://qudt.org/vocab/unit/YD3-PER-DAY" ->
            Just "yd³/day"

        "http://qudt.org/vocab/unit/MOL-PER-M3-SEC" ->
            Just "mol/(m³⋅s)"

        "http://qudt.org/vocab/unit/KiloM-PER-SEC" ->
            Just "km/s"

        "http://qudt.org/vocab/unit/M3-PER-MIN" ->
            Just "m³/min"

        "http://qudt.org/vocab/unit/MegaPSI" ->
            Just "Mpsi"

        "http://qudt.org/vocab/unit/NanoS" ->
            Just "nS"

        "http://qudt.org/vocab/unit/TON_SHORT" ->
            Just "ton{short}"

        "http://qudt.org/vocab/unit/CentiGM" ->
            Just "cg"

        "http://qudt.org/vocab/unit/MilliL-PER-K" ->
            Just "mL/K"

        "http://qudt.org/vocab/unit/LY" ->
            Just "ly"

        "http://qudt.org/vocab/unit/KiloV-A-HR" ->
            Just "kV⋅A⋅hr"

        "http://qudt.org/vocab/unit/PINT_US-PER-HR" ->
            Just "pt{US}/hr"

        "http://qudt.org/vocab/unit/N-M-PER-DEG-M" ->
            Just "N⋅m/(°·m)"

        "http://qudt.org/vocab/unit/YR_TROPICAL" ->
            Just "yr{tropical}"

        "http://qudt.org/vocab/unit/CUP_US" ->
            Just "cup{US}"

        "http://qudt.org/vocab/unit/ERG" ->
            Just "erg"

        "http://qudt.org/vocab/unit/NanoGM-PER-DAY" ->
            Just "ng/day"

        "http://qudt.org/vocab/unit/V-A" ->
            Just "V⋅A"

        "http://qudt.org/vocab/unit/MegaPA-L-PER-SEC" ->
            Just "MPa⋅L/s"

        "http://qudt.org/vocab/unit/PER-WK" ->
            Just "/wk"

        "http://qudt.org/vocab/unit/NanoMOL-PER-MicroMOL" ->
            Just "nmol/µmol"

        "http://qudt.org/vocab/unit/PINT_UK" ->
            Just "pt{UK}"

        "http://qudt.org/vocab/unit/M-PER-SEC" ->
            Just "m/s"

        "http://qudt.org/vocab/unit/C-PER-MilliM3" ->
            Just "C/mm³"

        "http://qudt.org/vocab/unit/A-PER-RAD" ->
            Just "A/rad"

        "http://qudt.org/vocab/unit/FARAD_Ab" ->
            Just "abF"

        "http://qudt.org/vocab/unit/ROD" ->
            Just "rod"

        "http://qudt.org/vocab/unit/NanoMOL-PER-L-HR" ->
            Just "nmol/(L⋅hr)"

        "http://qudt.org/vocab/unit/J-PER-M4" ->
            Just "J/m⁴"

        "http://qudt.org/vocab/unit/KiloGM_F-M" ->
            Just "kgf⋅m"

        "http://qudt.org/vocab/unit/MicroH-PER-OHM" ->
            Just "µH/Ω"

        "http://qudt.org/vocab/unit/MilliM-PER-YR" ->
            Just "mm/yr"

        "http://qudt.org/vocab/unit/BTU_IT-PER-LB_F" ->
            Just "Btu{IT}/lbf"

        "http://qudt.org/vocab/unit/MegaHZ-PER-T" ->
            Just "MHz/T"

        "http://qudt.org/vocab/unit/FT2-PER-BTU_IT-IN" ->
            Just "ft²/(Btu{IT}⋅in)"

        "http://qudt.org/vocab/unit/PINT_UK-PER-HR" ->
            Just "pt{UK}/hr"

        "http://qudt.org/vocab/unit/KiloV-A" ->
            Just "kV⋅A"

        "http://qudt.org/vocab/unit/MilliGM-PER-GM" ->
            Just "mg/g"

        "http://qudt.org/vocab/unit/TONNE-PER-YR" ->
            Just "t/year"

        "http://qudt.org/vocab/unit/M2-K" ->
            Just "m²⋅K"

        "http://qudt.org/vocab/unit/K" ->
            Just "K"

        "http://qudt.org/vocab/unit/KiloCAL_TH-PER-SEC" ->
            Just "kcal{th}/s"

        "http://qudt.org/vocab/unit/GAUGE_FR" ->
            Just "French gauge"

        "http://qudt.org/vocab/unit/CentiM2-PER-SEC" ->
            Just "cm²/s"

        "http://qudt.org/vocab/unit/TONNE-PER-MIN" ->
            Just "t/min"

        "http://qudt.org/vocab/unit/AttoFARAD" ->
            Just "aF"

        "http://qudt.org/vocab/unit/MilliL-PER-SEC" ->
            Just "mL/s"

        "http://qudt.org/vocab/unit/W-PER-FT2" ->
            Just "W/ft²"

        "http://qudt.org/vocab/unit/PicoM" ->
            Just "pm"

        "http://qudt.org/vocab/unit/CentiM-PER-KiloYR" ->
            Just "cm/(1000 yr)"

        "http://qudt.org/vocab/unit/CORD" ->
            Just "cord"

        "http://qudt.org/vocab/unit/CentiPOISE-PER-BAR" ->
            Just "cP/bar"

        "http://qudt.org/vocab/unit/KiloGM-PER-M3" ->
            Just "kg/m³"

        "http://qudt.org/vocab/unit/OZ_VOL_UK-PER-MIN" ->
            Just "oz{UK}/min"

        "http://qudt.org/vocab/unit/J-PER-T2" ->
            Just "J/T²"

        "http://qudt.org/vocab/unit/DEGREE_BAUME_US_LIGHT" ->
            Just "°Bé{US Light}"

        "http://qudt.org/vocab/unit/PA-PER-MIN" ->
            Just "Pa/min"

        "http://qudt.org/vocab/unit/PERCENT_RH" ->
            Just "%RH"

        "http://qudt.org/vocab/unit/PER-SEC" ->
            Just "/s"

        "http://qudt.org/vocab/unit/CH" ->
            Just "ch"

        "http://qudt.org/vocab/unit/H_Stat" ->
            Just "statH"

        "http://qudt.org/vocab/unit/M2-PER-GM_DRY" ->
            Just "m²/g{dry sediment}"

        "http://qudt.org/vocab/unit/MegaBQ" ->
            Just "MBq"

        "http://qudt.org/vocab/unit/KiloBYTE" ->
            Just "kB"

        "http://qudt.org/vocab/unit/PA-M-PER-SEC2" ->
            Just "Pa⋅m/s²"

        "http://qudt.org/vocab/unit/KiloW-HR-PER-M2" ->
            Just "kW⋅hr/m²"

        "http://qudt.org/vocab/unit/CentiM2-PER-GM" ->
            Just "cm²/g"

        "http://qudt.org/vocab/unit/CAL_MEAN" ->
            Just "cal{mean}"

        "http://qudt.org/vocab/unit/KiloGM_F-M-PER-SEC" ->
            Just "kgf⋅m/s"

        "http://qudt.org/vocab/unit/MegaEV-PER-CentiM" ->
            Just "MeV/cm"

        "http://qudt.org/vocab/unit/K-PER-MIN" ->
            Just "K/min"

        "http://qudt.org/vocab/unit/BBL_US-PER-MIN" ->
            Just "bbl{US petroleum}/min"

        "http://qudt.org/vocab/unit/PSI-YD3-PER-SEC" ->
            Just "psi⋅yd³/s"

        "http://qudt.org/vocab/unit/C_Ab" ->
            Just "abC"

        "http://qudt.org/vocab/unit/BQ-PER-L" ->
            Just "Bq/L"

        "http://qudt.org/vocab/unit/MilliM3-PER-KiloGM" ->
            Just "mm³/kg"

        "http://qudt.org/vocab/unit/MicroBQ" ->
            Just "μBq"

        "http://qudt.org/vocab/unit/DeciB_M" ->
            Just "dBmW"

        "http://qudt.org/vocab/unit/STR" ->
            Just "st"

        "http://qudt.org/vocab/unit/GigaJ-PER-HR" ->
            Just "GJ/hr"

        "http://qudt.org/vocab/unit/CFU" ->
            Just "CFU"

        "http://qudt.org/vocab/unit/PicoMOL-PER-L-HR" ->
            Just "pmol/(L⋅hr)"

        "http://qudt.org/vocab/unit/GI_US-PER-SEC" ->
            Just "gill{US}/s"

        "http://qudt.org/vocab/unit/MegaJ-PER-K" ->
            Just "MJ/K"

        "http://qudt.org/vocab/unit/MicroH-PER-M" ->
            Just "µH/m"

        "http://qudt.org/vocab/unit/THM_EEC" ->
            Just "thm{EEC}"

        "http://qudt.org/vocab/unit/N-PER-A" ->
            Just "N/A"

        "http://qudt.org/vocab/unit/PicoGM-PER-GM" ->
            Just "pg/g"

        "http://qudt.org/vocab/unit/M2-PER-KiloW" ->
            Just "m²/kW"

        "http://qudt.org/vocab/unit/MegaHZ" ->
            Just "MHz"

        "http://qudt.org/vocab/unit/PER-MicroMOL-L" ->
            Just "/(µmol⋅L)"

        "http://qudt.org/vocab/unit/YD3-PER-MIN" ->
            Just "yd³/min"

        "http://qudt.org/vocab/unit/EV-PER-M" ->
            Just "eV/m"

        "http://qudt.org/vocab/unit/PK_US_DRY-PER-MIN" ->
            Just "peck{US Dry}/min"

        "http://qudt.org/vocab/unit/MilliGM-PER-CentiM2" ->
            Just "mg/cm²"

        "http://qudt.org/vocab/unit/CASES-PER-KiloINDIV-YR" ->
            Just "Cases/1000 individuals/year"

        "http://qudt.org/vocab/unit/LB_F-PER-FT" ->
            Just "lbf/ft"

        "http://qudt.org/vocab/unit/MicroL" ->
            Just "μL"

        "http://qudt.org/vocab/unit/GM" ->
            Just "g"

        "http://qudt.org/vocab/unit/MicroA" ->
            Just "μA"

        "http://qudt.org/vocab/unit/PPTH-PER-HR" ->
            Just "‰/hr"

        "http://qudt.org/vocab/unit/J-SEC" ->
            Just "J⋅s"

        "http://qudt.org/vocab/unit/K-M2-PER-KiloGM-SEC" ->
            Just "K⋅m²/(kg⋅s)"

        "http://qudt.org/vocab/unit/MicroH" ->
            Just "μH"

        "http://qudt.org/vocab/unit/N-M-PER-M-RAD" ->
            Just "N⋅m/(m⋅rad)"

        "http://qudt.org/vocab/unit/CentiM3-PER-MOL-SEC" ->
            Just "cm³/(mol⋅s)"

        "http://qudt.org/vocab/unit/REV-PER-MIN" ->
            Just "rev/min"

        "http://qudt.org/vocab/unit/V-PER-MilliM" ->
            Just "V/mm"

        "http://qudt.org/vocab/unit/NUM-PER-M2" ->
            Just "/m²"

        "http://qudt.org/vocab/unit/MegaC" ->
            Just "MC"

        "http://qudt.org/vocab/unit/ATM" ->
            Just "atm"

        "http://qudt.org/vocab/unit/MOL-PER-HR" ->
            Just "mol/hr"

        "http://qudt.org/vocab/unit/BTU_TH-FT-PER-FT2-HR-DEG_F" ->
            Just "Btu{th}⋅ft/(ft²⋅hr⋅°F)"

        "http://qudt.org/vocab/unit/KiloTONNE" ->
            Just "kt"

        "http://qudt.org/vocab/unit/KiloM-PER-HR" ->
            Just "km/hr"

        "http://qudt.org/vocab/unit/PlanckMomentum" ->
            Just "planckmomentum"

        "http://qudt.org/vocab/unit/M2" ->
            Just "m²"

        "http://qudt.org/vocab/unit/IN_HG" ->
            Just "inHg"

        "http://qudt.org/vocab/unit/OZ-PER-SEC" ->
            Just "oz/s"

        "http://qudt.org/vocab/unit/OZ-PER-HR" ->
            Just "oz/hr"

        "http://qudt.org/vocab/unit/MilliS" ->
            Just "mS"

        "http://qudt.org/vocab/unit/HP" ->
            Just "HP"

        "http://qudt.org/vocab/unit/DYN-CentiM" ->
            Just "dyn⋅cm"

        "http://qudt.org/vocab/unit/FT_H2O" ->
            Just "ftH₂0"

        "http://qudt.org/vocab/unit/EUR-PER-KiloW" ->
            Just "€/kW"

        "http://qudt.org/vocab/unit/LB_F" ->
            Just "lbf"

        "http://qudt.org/vocab/unit/MOL-K" ->
            Just "mol⋅K"

        "http://qudt.org/vocab/unit/PER-CentiM" ->
            Just "/cm"

        "http://qudt.org/vocab/unit/MegaJ-PER-M3" ->
            Just "MJ/m³"

        "http://qudt.org/vocab/unit/KiloGM-M2" ->
            Just "kg⋅m²"

        "http://qudt.org/vocab/unit/MilliBQ-PER-GM" ->
            Just "mBq/g"

        "http://qudt.org/vocab/unit/CARAT" ->
            Just "ct"

        "http://qudt.org/vocab/unit/ARE" ->
            Just "a"

        "http://qudt.org/vocab/unit/OZ-PER-DAY" ->
            Just "oz/day"

        "http://qudt.org/vocab/unit/GigaJ-PER-M2" ->
            Just "GJ/m²"

        "http://qudt.org/vocab/unit/DEG_C-PER-M" ->
            Just "°C/m"

        "http://qudt.org/vocab/unit/BTU_IT-PER-FT3" ->
            Just "Btu{IT}/ft³"

        "http://qudt.org/vocab/unit/YR" ->
            Just "yr"

        "http://qudt.org/vocab/unit/PER-BAR" ->
            Just "/bar"

        "http://qudt.org/vocab/unit/MilliGAL" ->
            Just "mgal"

        "http://qudt.org/vocab/unit/LB-PER-FT2" ->
            Just "lbm/ft²"

        "http://qudt.org/vocab/unit/GigaC" ->
            Just "GC"

        "http://qudt.org/vocab/unit/PER-HR" ->
            Just "/hr"

        "http://qudt.org/vocab/unit/MX" ->
            Just "Mx"

        "http://qudt.org/vocab/unit/EUR-PER-W-HR" ->
            Just "€/(W⋅hr)"

        "http://qudt.org/vocab/unit/GI_US-PER-HR" ->
            Just "gill{US}/hr"

        "http://qudt.org/vocab/unit/MOL_LB-DEG_F" ->
            Just "lb-mol⋅°F"

        "http://qudt.org/vocab/unit/N-PER-MilliM" ->
            Just "N/mm"

        "http://qudt.org/vocab/unit/K-PER-HR" ->
            Just "K/hr"

        "http://qudt.org/vocab/unit/PER-EV2" ->
            Just "/eV²"

        "http://qudt.org/vocab/unit/NUM-PER-M2-DAY" ->
            Just "/(m²⋅day)"

        "http://qudt.org/vocab/unit/FT-PDL" ->
            Just "ft⋅pdl"

        "http://qudt.org/vocab/unit/N-PER-M3" ->
            Just "N/m³"

        "http://qudt.org/vocab/unit/MilliM3-PER-GM" ->
            Just "mm³/g"

        "http://qudt.org/vocab/unit/KiloM" ->
            Just "km"

        "http://qudt.org/vocab/unit/KiloGM-PER-M2-SEC2" ->
            Just "kg/(m²⋅s²)"

        "http://qudt.org/vocab/unit/PER-FT3" ->
            Just "/ft³"

        "http://qudt.org/vocab/unit/MegaC-PER-M3" ->
            Just "MC/m³"

        "http://qudt.org/vocab/unit/DeciM3-PER-DAY" ->
            Just "dm³/day"

        "http://qudt.org/vocab/unit/MegaA-PER-M2" ->
            Just "MA/m²"

        "http://qudt.org/vocab/unit/HectoPA-PER-BAR" ->
            Just "hPa/bar"

        "http://qudt.org/vocab/unit/PER-M3-SEC" ->
            Just "/(m³⋅s)"

        "http://qudt.org/vocab/unit/REV-PER-SEC2" ->
            Just "rev/s²"

        "http://qudt.org/vocab/unit/KiloCAL-PER-CentiM-SEC-DEG_C" ->
            Just "kcal/(cm⋅s⋅°C)"

        "http://qudt.org/vocab/unit/TON_LONG-PER-YD3" ->
            Just "t{long}/yd³"

        "http://qudt.org/vocab/unit/MicroM" ->
            Just "μm"

        "http://qudt.org/vocab/unit/SLUG-PER-FT" ->
            Just "slug/ft"

        "http://qudt.org/vocab/unit/W-PER-IN2" ->
            Just "W/in²"

        "http://qudt.org/vocab/unit/IN_H2O" ->
            Just "inH₂0"

        "http://qudt.org/vocab/unit/C_Stat-PER-CentiM2" ->
            Just "statC/cm²"

        "http://qudt.org/vocab/unit/J-PER-HR" ->
            Just "J/hr"

        "http://qudt.org/vocab/unit/DEGREE_BAUME" ->
            Just "°Bé{origin}"

        "http://qudt.org/vocab/unit/F" ->
            Just "F"

        "http://qudt.org/vocab/unit/KiloJ-PER-K" ->
            Just "kJ/K"

        "http://qudt.org/vocab/unit/MI2" ->
            Just "mi²"

        "http://qudt.org/vocab/unit/MilliT" ->
            Just "mT"

        "http://qudt.org/vocab/unit/TONNE-PER-HR" ->
            Just "t/hr"

        "http://qudt.org/vocab/unit/N-M-PER-M2" ->
            Just "N⋅m/m²"

        "http://qudt.org/vocab/unit/MicroL-PER-L" ->
            Just "μL/L"

        "http://qudt.org/vocab/unit/M-PER-FARAD" ->
            Just "m/F"

        "http://qudt.org/vocab/unit/L-PER-MicroMOL" ->
            Just "L/µmol"

        "http://qudt.org/vocab/unit/MegaPA-M3-PER-SEC" ->
            Just "MPa⋅m³/s"

        "http://qudt.org/vocab/unit/CAL_TH-PER-G" ->
            Just "cal/G"

        "http://qudt.org/vocab/unit/MegaJ-PER-HR" ->
            Just "MJ/hr"

        "http://qudt.org/vocab/unit/PlanckDensity" ->
            Just "planckdensity"

        "http://qudt.org/vocab/unit/KiloGM_F" ->
            Just "kgf"

        "http://qudt.org/vocab/unit/MOL-PER-GM-HR" ->
            Just "mol/(g⋅hr)"

        "http://qudt.org/vocab/unit/KiloMOL-PER-MIN" ->
            Just "kmol/min"

        "http://qudt.org/vocab/unit/DeciL-PER-GM" ->
            Just "dL/g"

        "http://qudt.org/vocab/unit/M" ->
            Just "m"

        "http://qudt.org/vocab/unit/W-PER-SR" ->
            Just "W/sr"

        "http://qudt.org/vocab/unit/BBL_US_DRY" ->
            Just "bbl{US dry}"

        "http://qudt.org/vocab/unit/KiloA" ->
            Just "kA"

        "http://qudt.org/vocab/unit/MIL" ->
            Just "mil{NATO}"

        "http://qudt.org/vocab/unit/LB-PER-YD3" ->
            Just "lbm/yd³"

        "http://qudt.org/vocab/unit/J-PER-K" ->
            Just "J/K"

        "http://qudt.org/vocab/unit/ARCMIN" ->
            Just "'"

        "http://qudt.org/vocab/unit/SLUG-PER-DAY" ->
            Just "slug/day"

        "http://qudt.org/vocab/unit/DeciM2" ->
            Just "dm²"

        "http://qudt.org/vocab/unit/M2-HZ2" ->
            Just "m²⋅Hz²"

        "http://qudt.org/vocab/unit/IN3-PER-MIN" ->
            Just "in³/min"

        "http://qudt.org/vocab/unit/GigaJ" ->
            Just "GJ"

        "http://qudt.org/vocab/unit/MilliM_H2O" ->
            Just "mmH₂0"

        "http://qudt.org/vocab/unit/DeciL" ->
            Just "dL"

        "http://qudt.org/vocab/unit/LM-PER-W" ->
            Just "lm/W"

        "http://qudt.org/vocab/unit/LB-PER-FT3" ->
            Just "lbm/ft³"

        "http://qudt.org/vocab/unit/S_Stat" ->
            Just "statS"

        "http://qudt.org/vocab/unit/RAD-PER-HR" ->
            Just "rad/hr"

        "http://qudt.org/vocab/unit/PicoPA" ->
            Just "pPa"

        "http://qudt.org/vocab/unit/FT-LB_F-PER-HR" ->
            Just "ft⋅lbf/hr"

        "http://qudt.org/vocab/unit/MegaYR" ->
            Just "Myr"

        "http://qudt.org/vocab/unit/MilliC" ->
            Just "mC"

        "http://qudt.org/vocab/unit/ExbiBYTE" ->
            Just "EiB"

        "http://qudt.org/vocab/unit/TONNE-PER-SEC" ->
            Just "t/s"

        "http://qudt.org/vocab/unit/ERG-PER-CentiM2-SEC" ->
            Just "erg/(cm²⋅s)"

        "http://qudt.org/vocab/unit/TON_Assay" ->
            Just "AT"

        "http://qudt.org/vocab/unit/MicroGRAY" ->
            Just "μGy"

        "http://qudt.org/vocab/unit/PicoA" ->
            Just "pA"

        "http://qudt.org/vocab/unit/KiloCAL-PER-CentiM2" ->
            Just "kcal/cm²"

        "http://qudt.org/vocab/unit/CAL_TH-PER-GM-DEG_C" ->
            Just "cal/(g⋅°C)"

        "http://qudt.org/vocab/unit/MicroFARAD-PER-M" ->
            Just "µF/m"

        "http://qudt.org/vocab/unit/BTU_IT-PER-MIN" ->
            Just "Btu{IT}/min"

        "http://qudt.org/vocab/unit/KiloBAR" ->
            Just "kbar"

        "http://qudt.org/vocab/unit/KiloPA-M2-PER-GM" ->
            Just "kPa⋅m²/g"

        "http://qudt.org/vocab/unit/GM-PER-M2" ->
            Just "g/m²"

        "http://qudt.org/vocab/unit/KiloA-HR" ->
            Just "kA⋅hr"

        "http://qudt.org/vocab/unit/PA-L-PER-SEC" ->
            Just "Pa⋅L/s"

        "http://qudt.org/vocab/unit/NanoM" ->
            Just "nm"

        "http://qudt.org/vocab/unit/MicroV" ->
            Just "µV"

        "http://qudt.org/vocab/unit/ERG-PER-GM-SEC" ->
            Just "erg/(g⋅s)"

        "http://qudt.org/vocab/unit/PA-PER-M" ->
            Just "Pa/m"

        "http://qudt.org/vocab/unit/ZeptoC" ->
            Just "zC"

        "http://qudt.org/vocab/unit/KiloJ" ->
            Just "kJ"

        "http://qudt.org/vocab/unit/PSI-L-PER-SEC" ->
            Just "psi⋅L/s"

        "http://qudt.org/vocab/unit/PER-M-SR" ->
            Just "/(m⋅sr)"

        "http://qudt.org/vocab/unit/J-PER-KiloGM-DEG_C" ->
            Just "J/(kg⋅°C)"

        "http://qudt.org/vocab/unit/QUAD" ->
            Just "quad"

        "http://qudt.org/vocab/unit/FT-PER-DEG_F" ->
            Just "ft/°F"

        "http://qudt.org/vocab/unit/MicroGM-PER-MilliL" ->
            Just "μg/mL"

        "http://qudt.org/vocab/unit/IU-PER-MilliGM" ->
            Just "IU/mg"

        "http://qudt.org/vocab/unit/OZ_VOL_US-PER-DAY" ->
            Just "fl oz{US}/day"

        "http://qudt.org/vocab/unit/S-M2-PER-MOL" ->
            Just "S⋅m²/mol"

        "http://qudt.org/vocab/unit/M6" ->
            Just "m⁶"

        "http://qudt.org/vocab/unit/IU" ->
            Just "IU"

        "http://qudt.org/vocab/unit/MilliM_HG" ->
            Just "mmHg"

        "http://qudt.org/vocab/unit/MicroGM-PER-DeciL" ->
            Just "μg/dL"

        "http://qudt.org/vocab/unit/TONNE" ->
            Just "t"

        "http://qudt.org/vocab/unit/MOL-PER-KiloGM-PA" ->
            Just "mol/(kg⋅Pa)"

        "http://qudt.org/vocab/unit/PicoFARAD-PER-M" ->
            Just "pF/m"

        "http://qudt.org/vocab/unit/LB_F-PER-IN2-DEG_F" ->
            Just "lbf/(in²⋅°F)"

        "http://qudt.org/vocab/unit/PicoGM-PER-MilliL" ->
            Just "pg/mL"

        "http://qudt.org/vocab/unit/MOL-PER-M2" ->
            Just "mol/m²"

        "http://qudt.org/vocab/unit/DecaL" ->
            Just "daL"

        "http://qudt.org/vocab/unit/KiloEV-PER-MicroM" ->
            Just "keV/µm"

        "http://qudt.org/vocab/unit/TON_Metric-PER-HR" ->
            Just "t/hr"

        "http://qudt.org/vocab/unit/LB_F-FT" ->
            Just "lbf⋅ft"

        "http://qudt.org/vocab/unit/SLUG-PER-FT-SEC" ->
            Just "slug/(ft⋅s)"

        "http://qudt.org/vocab/unit/IU-PER-MilliL" ->
            Just "IU/mL"

        "http://qudt.org/vocab/unit/MicroPA" ->
            Just "μPa"

        "http://qudt.org/vocab/unit/BTU_TH-IN-PER-FT2-HR-DEG_F" ->
            Just "Btu{th}⋅in/(ft²⋅hr⋅°F)"

        "http://qudt.org/vocab/unit/KiloHZ" ->
            Just "kHz"

        "http://qudt.org/vocab/unit/MicroFARAD-PER-KiloM" ->
            Just "µF/km"

        "http://qudt.org/vocab/unit/MicroGM-PER-GM" ->
            Just "μg/g"

        "http://qudt.org/vocab/unit/OZ_F" ->
            Just "ozf"

        "http://qudt.org/vocab/unit/CAL_IT-PER-GM-K" ->
            Just "cal{IT}/(g⋅K)"

        "http://qudt.org/vocab/unit/M5" ->
            Just "m⁵"

        "http://qudt.org/vocab/unit/MicroS" ->
            Just "μS"

        "http://qudt.org/vocab/unit/J-PER-M" ->
            Just "J/m"

        "http://qudt.org/vocab/unit/Pennyweight" ->
            Just "dwt"

        "http://qudt.org/vocab/unit/DeciM3-PER-M3" ->
            Just "dm³/m³"

        "http://qudt.org/vocab/unit/MOL-PER-M2-SEC-SR" ->
            Just "mol/(m²⋅s⋅sr)"

        "http://qudt.org/vocab/unit/DEGREE_OECHSLE" ->
            Just "°Oe"

        "http://qudt.org/vocab/unit/BTU_IT-PER-HR-FT2-DEG_R" ->
            Just "Btu{IT}/(hr⋅ft²⋅°R)"

        "http://qudt.org/vocab/unit/MilliBAR-L-PER-SEC" ->
            Just "mbar⋅L/s"

        "http://qudt.org/vocab/unit/KiloBTU_TH" ->
            Just "kBtu{th}"

        "http://qudt.org/vocab/unit/PER-KiloM" ->
            Just "/km"

        "http://qudt.org/vocab/unit/MilliGRAY" ->
            Just "mGy"

        "http://qudt.org/vocab/unit/MicroGAL-PER-M" ->
            Just "µGal/m"

        "http://qudt.org/vocab/unit/MegaBIT-PER-SEC" ->
            Just "mbps"

        "http://qudt.org/vocab/unit/DEG_F-PER-MIN" ->
            Just "°F/min"

        "http://qudt.org/vocab/unit/IN" ->
            Just "in"

        "http://qudt.org/vocab/unit/J-PER-KiloGM" ->
            Just "J/kg"

        "http://qudt.org/vocab/unit/DRAM_US" ->
            Just "dr{US}"

        "http://qudt.org/vocab/unit/ERG-PER-SEC" ->
            Just "erg/s"

        "http://qudt.org/vocab/unit/MilliTORR" ->
            Just "mTorr"

        "http://qudt.org/vocab/unit/BIOT" ->
            Just "Bi"

        "http://qudt.org/vocab/unit/GM-PER-MIN" ->
            Just "g/min"

        "http://qudt.org/vocab/unit/KiloC-PER-M2" ->
            Just "kC/m²"

        "http://qudt.org/vocab/unit/FT2-PER-HR" ->
            Just "ft²/hr"

        "http://qudt.org/vocab/unit/MilliGM-PER-M" ->
            Just "mg/m"

        "http://qudt.org/vocab/unit/N-SEC-PER-M" ->
            Just "N⋅s/m"

        "http://qudt.org/vocab/unit/MegaLB_F" ->
            Just "Mlbf"

        "http://qudt.org/vocab/unit/LB_F-PER-FT2" ->
            Just "lbf/ft²"

        "http://qudt.org/vocab/unit/FARAD_Ab-PER-CentiM" ->
            Just "abF/cm"

        "http://qudt.org/vocab/unit/DEG_C-PER-MIN" ->
            Just "°C/min"

        "http://qudt.org/vocab/unit/H_Ab" ->
            Just "abH"

        "http://qudt.org/vocab/unit/YD3-PER-HR" ->
            Just "yd³/hr"

        "http://qudt.org/vocab/unit/STILB" ->
            Just "sb"

        "http://qudt.org/vocab/unit/QT_UK-PER-SEC" ->
            Just "qt{UK}/s"

        "http://qudt.org/vocab/unit/DYN-PER-CentiM" ->
            Just "dyn/cm"

        "http://qudt.org/vocab/unit/PSI" ->
            Just "psi"

        "http://qudt.org/vocab/unit/KiloV" ->
            Just "kV"

        "http://qudt.org/vocab/unit/FRACTION" ->
            Just "÷"

        "http://qudt.org/vocab/unit/DEG_F-PER-K" ->
            Just "°F/K"

        "http://qudt.org/vocab/unit/MicroMOL-PER-MOL" ->
            Just "µmol/mol"

        "http://qudt.org/vocab/unit/HZ-PER-V" ->
            Just "Hz/V"

        "http://qudt.org/vocab/unit/MOL-PER-M2-SEC" ->
            Just "mol/(m²⋅s)"

        "http://qudt.org/vocab/unit/MilliJ" ->
            Just "mJ"

        "http://qudt.org/vocab/unit/GI_UK-PER-DAY" ->
            Just "gill{UK}/day"

        "http://qudt.org/vocab/currency/UYU" ->
            Just "UYU"

        "http://qudt.org/vocab/currency/GIP" ->
            Just "GIP"

        "http://qudt.org/vocab/currency/RON" ->
            Just "RON"

        "http://qudt.org/vocab/currency/YER" ->
            Just "YER"

        "http://qudt.org/vocab/currency/HKD" ->
            Just "HKD"

        "http://qudt.org/vocab/currency/FKP" ->
            Just "FKP"

        "http://qudt.org/vocab/currency/XAU" ->
            Just "XAU"

        "http://qudt.org/vocab/currency/CDF" ->
            Just "CDF"

        "http://qudt.org/vocab/currency/AZN" ->
            Just "AZN"

        "http://qudt.org/vocab/currency/BND" ->
            Just "BND"

        "http://qudt.org/vocab/currency/EGP" ->
            Just "EGP"

        "http://qudt.org/vocab/currency/CUP" ->
            Just "CUP"

        "http://qudt.org/vocab/currency/EEK" ->
            Just "EEK"

        "http://qudt.org/vocab/currency/EUR" ->
            Just "EUR"

        "http://qudt.org/vocab/currency/PHP" ->
            Just "PHP"

        "http://qudt.org/vocab/currency/CHE" ->
            Just "CHE"

        "http://qudt.org/vocab/currency/VUV" ->
            Just "VUV"

        "http://qudt.org/vocab/currency/XBC" ->
            Just "XBC"

        "http://qudt.org/vocab/currency/XAG" ->
            Just "XAG"

        "http://qudt.org/vocab/currency/TND" ->
            Just "TND"

        "http://qudt.org/vocab/currency/GYD" ->
            Just "GYD"

        "http://qudt.org/vocab/currency/MAD" ->
            Just "MAD"

        "http://qudt.org/vocab/currency/GHS" ->
            Just "GHS"

        "http://qudt.org/vocab/currency/GBP" ->
            Just "GBP"

        "http://qudt.org/vocab/currency/VES" ->
            Just "VES"

        "http://qudt.org/vocab/currency/HRK" ->
            Just "HRK"

        "http://qudt.org/vocab/currency/BGN" ->
            Just "BGN"

        "http://qudt.org/vocab/currency/SEK" ->
            Just "SEK"

        "http://qudt.org/vocab/currency/ERN" ->
            Just "ERN"

        "http://qudt.org/vocab/currency/DZD" ->
            Just "DZD"

        "http://qudt.org/vocab/currency/ZAR" ->
            Just "ZAR"

        "http://qudt.org/vocab/currency/JMD" ->
            Just "JMD"

        "http://qudt.org/vocab/currency/UZS" ->
            Just "UZS"

        "http://qudt.org/vocab/currency/KZT" ->
            Just "KZT"

        "http://qudt.org/vocab/currency/KYD" ->
            Just "KYD"

        "http://qudt.org/vocab/currency/MMK" ->
            Just "MMK"

        "http://qudt.org/vocab/currency/SRD" ->
            Just "SRD"

        "http://qudt.org/vocab/currency/CVE" ->
            Just "CVE"

        "http://qudt.org/vocab/currency/IDR" ->
            Just "IDR"

        "http://qudt.org/vocab/currency/KWD" ->
            Just "KWD"

        "http://qudt.org/vocab/currency/MZN" ->
            Just "MZN"

        "http://qudt.org/vocab/currency/MXV" ->
            Just "MXV"

        "http://qudt.org/vocab/currency/MDL" ->
            Just "MDL"

        "http://qudt.org/vocab/currency/KMF" ->
            Just "KMF"

        "http://qudt.org/vocab/currency/VND" ->
            Just "VND"

        "http://qudt.org/vocab/currency/MOP" ->
            Just "MOP"

        "http://qudt.org/vocab/currency/MXN" ->
            Just "MXN"

        "http://qudt.org/vocab/currency/BAM" ->
            Just "BAM"

        "http://qudt.org/vocab/currency/COP" ->
            Just "COP"

        "http://qudt.org/vocab/currency/GTQ" ->
            Just "GTQ"

        "http://qudt.org/vocab/currency/PAB" ->
            Just "PAB"

        "http://qudt.org/vocab/currency/WST" ->
            Just "WST"

        "http://qudt.org/vocab/currency/SLE" ->
            Just "SLE"

        "http://qudt.org/vocab/currency/TJS" ->
            Just "TJS"

        "http://qudt.org/vocab/currency/XFO" ->
            Just "XFO"

        "http://qudt.org/vocab/currency/USN" ->
            Just "USN"

        "http://qudt.org/vocab/currency/ETB" ->
            Just "ETB"

        "http://qudt.org/vocab/currency/KRW" ->
            Just "KRW"

        "http://qudt.org/vocab/currency/RWF" ->
            Just "RWF"

        "http://qudt.org/vocab/currency/TRY" ->
            Just "TRY"

        "http://qudt.org/vocab/currency/OMR" ->
            Just "OMR"

        "http://qudt.org/vocab/currency/ISK" ->
            Just "ISK"

        "http://qudt.org/vocab/currency/AOA" ->
            Just "AOA"

        "http://qudt.org/vocab/currency/JPY" ->
            Just "JPY"

        "http://qudt.org/vocab/currency/DKK" ->
            Just "DKK"

        "http://qudt.org/vocab/currency/SZL" ->
            Just "SZL"

        "http://qudt.org/vocab/currency/IRR" ->
            Just "IRR"

        "http://qudt.org/vocab/currency/KES" ->
            Just "KES"

        "http://qudt.org/vocab/currency/XCD" ->
            Just "XCD"

        "http://qudt.org/vocab/currency/XAF" ->
            Just "XAF"

        "http://qudt.org/vocab/currency/BHD" ->
            Just "BHD"

        "http://qudt.org/vocab/currency/LYD" ->
            Just "LYD"

        "http://qudt.org/vocab/currency/DOP" ->
            Just "DOP"

        "http://qudt.org/vocab/currency/BZD" ->
            Just "BZD"

        "http://qudt.org/vocab/currency/QAR" ->
            Just "QAR"

        "http://qudt.org/vocab/currency/USS" ->
            Just "USS"

        "http://qudt.org/vocab/currency/TMT" ->
            Just "TMT"

        "http://qudt.org/vocab/currency/MRU" ->
            Just "MRU"

        "http://qudt.org/vocab/currency/XBD" ->
            Just "XBD"

        "http://qudt.org/vocab/currency/KPW" ->
            Just "KPW"

        "http://qudt.org/vocab/currency/BRL" ->
            Just "BRL"

        "http://qudt.org/vocab/currency/SAR" ->
            Just "SAR"

        "http://qudt.org/vocab/currency/TZS" ->
            Just "TZS"

        "http://qudt.org/vocab/currency/MYR" ->
            Just "MYR"

        "http://qudt.org/vocab/currency/NOK" ->
            Just "NOK"

        "http://qudt.org/vocab/currency/TOP" ->
            Just "TOP"

        "http://qudt.org/vocab/currency/BMD" ->
            Just "BMD"

        "http://qudt.org/vocab/currency/AFN" ->
            Just "AFN"

        "http://qudt.org/vocab/currency/XPD" ->
            Just "XPD"

        "http://qudt.org/vocab/currency/PEN" ->
            Just "PEN"

        "http://qudt.org/vocab/currency/NAD" ->
            Just "NAD"

        "http://qudt.org/vocab/currency/LKR" ->
            Just "LKR"

        "http://qudt.org/vocab/currency/DJF" ->
            Just "DJF"

        "http://qudt.org/vocab/currency/NIO" ->
            Just "NIO"

        "http://qudt.org/vocab/currency/CHW" ->
            Just "CHW"

        "http://qudt.org/vocab/currency/LAK" ->
            Just "LAK"

        "http://qudt.org/vocab/currency/AWG" ->
            Just "AWG"

        "http://qudt.org/vocab/currency/LTL" ->
            Just "LTL"

        "http://qudt.org/vocab/currency/CYP" ->
            Just "CYP"

        "http://qudt.org/vocab/currency/NGN" ->
            Just "NGN"

        "http://qudt.org/vocab/currency/HNL" ->
            Just "HNL"

        "http://qudt.org/vocab/currency/SOS" ->
            Just "SOS"

        "http://qudt.org/vocab/currency/LRD" ->
            Just "LRD"

        "http://qudt.org/vocab/currency/BSD" ->
            Just "BSD"

        "http://qudt.org/vocab/currency/XDR" ->
            Just "XDR"

        "http://qudt.org/vocab/currency/XOF" ->
            Just "XOF"

        "http://qudt.org/vocab/currency/JOD" ->
            Just "JOD"

        "http://qudt.org/vocab/currency/SBD" ->
            Just "SBD"

        "http://qudt.org/vocab/currency/XBA" ->
            Just "XBA"

        "http://qudt.org/vocab/currency/COU" ->
            Just "COU"

        "http://qudt.org/vocab/currency/UAH" ->
            Just "UAH"

        "http://qudt.org/vocab/currency/BOV" ->
            Just "BOV"

        "http://qudt.org/vocab/currency/BDT" ->
            Just "BDT"

        "http://qudt.org/vocab/currency/NZD" ->
            Just "NZD"

        "http://qudt.org/vocab/currency/HUF" ->
            Just "HUF"

        "http://qudt.org/vocab/currency/BWP" ->
            Just "BWP"

        "http://qudt.org/vocab/currency/BTN" ->
            Just "BTN"

        "http://qudt.org/vocab/currency/CLF" ->
            Just "CLF"

        "http://qudt.org/vocab/currency/PYG" ->
            Just "PYG"

        "http://qudt.org/vocab/currency/SDG" ->
            Just "SDG"

        "http://qudt.org/vocab/currency/MUR" ->
            Just "MUR"

        "http://qudt.org/vocab/currency/CRC" ->
            Just "CRC"

        "http://qudt.org/vocab/currency/MTL" ->
            Just "MTL"

        "http://qudt.org/vocab/currency/GNF" ->
            Just "GNF"

        "http://qudt.org/vocab/currency/SYP" ->
            Just "SYP"

        "http://qudt.org/vocab/currency/BBD" ->
            Just "BBD"

        "http://qudt.org/vocab/currency/CAD" ->
            Just "CAD"

        "http://qudt.org/vocab/currency/PGK" ->
            Just "PGK"

        "http://qudt.org/vocab/currency/XPT" ->
            Just "XPT"

        "http://qudt.org/vocab/currency/CHF" ->
            Just "CHF"

        "http://qudt.org/vocab/currency/HTG" ->
            Just "HTG"

        "http://qudt.org/vocab/currency/PLN" ->
            Just "PLN"

        "http://qudt.org/vocab/currency/TWD" ->
            Just "TWD"

        "http://qudt.org/vocab/currency/FJD" ->
            Just "FJD"

        "http://qudt.org/vocab/currency/UGX" ->
            Just "UGX"

        "http://qudt.org/vocab/currency/MKD" ->
            Just "MKD"

        "http://qudt.org/vocab/currency/XPF" ->
            Just "XPF"

        "http://qudt.org/vocab/currency/XBB" ->
            Just "XBB"

        "http://qudt.org/vocab/currency/USD" ->
            Just "USD"

        "http://qudt.org/vocab/currency/CLP" ->
            Just "CLP"

        "http://qudt.org/vocab/currency/SCR" ->
            Just "SCR"

        "http://qudt.org/vocab/currency/ARS" ->
            Just "ARS"

        "http://qudt.org/vocab/currency/LSL" ->
            Just "LSL"

        "http://qudt.org/vocab/currency/MGA" ->
            Just "MGA"

        "http://qudt.org/vocab/currency/SHP" ->
            Just "SHP"

        "http://qudt.org/vocab/currency/THB" ->
            Just "THB"

        "http://qudt.org/vocab/currency/NPR" ->
            Just "NPR"

        "http://qudt.org/vocab/currency/MVR" ->
            Just "MVR"

        "http://qudt.org/vocab/currency/LVL" ->
            Just "LVL"

        "http://qudt.org/vocab/currency/ALL" ->
            Just "ALL"

        "http://qudt.org/vocab/currency/STN" ->
            Just "STN"

        "http://qudt.org/vocab/currency/XFU" ->
            Just "XFU"

        "http://qudt.org/vocab/currency/TTD" ->
            Just "TTD"

        "http://qudt.org/vocab/currency/SKK" ->
            Just "SKK"

        "http://qudt.org/vocab/currency/AED" ->
            Just "AED"

        "http://qudt.org/vocab/currency/IQD" ->
            Just "IQD"

        "http://qudt.org/vocab/currency/ANG" ->
            Just "ANG"

        "http://qudt.org/vocab/currency/BIF" ->
            Just "BIF"

        "http://qudt.org/vocab/currency/CZK" ->
            Just "CZK"

        "http://qudt.org/vocab/currency/ZWL" ->
            Just "ZWL"

        "http://qudt.org/vocab/currency/GEL" ->
            Just "GEL"

        "http://qudt.org/vocab/currency/SGD" ->
            Just "SGD"

        "http://qudt.org/vocab/currency/PKR" ->
            Just "PKR"

        "http://qudt.org/vocab/currency/BOB" ->
            Just "BOB"

        "http://qudt.org/vocab/currency/BYN" ->
            Just "BYN"

        "http://qudt.org/vocab/currency/GMD" ->
            Just "GMD"

        "http://qudt.org/vocab/currency/LBP" ->
            Just "LBP"

        "http://qudt.org/vocab/currency/CNY" ->
            Just "CNY"

        "http://qudt.org/vocab/currency/INR" ->
            Just "INR"

        "http://qudt.org/vocab/currency/ZMW" ->
            Just "ZMW"

        "http://qudt.org/vocab/currency/RSD" ->
            Just "RSD"

        "http://qudt.org/vocab/currency/KGS" ->
            Just "KGS"

        "http://qudt.org/vocab/currency/AUD" ->
            Just "AUD"

        "http://qudt.org/vocab/currency/RUB" ->
            Just "RUB"

        "http://qudt.org/vocab/currency/AMD" ->
            Just "AMD"

        "http://qudt.org/vocab/currency/MWK" ->
            Just "MWK"

        "http://qudt.org/vocab/currency/KHR" ->
            Just "KHR"

        "http://qudt.org/vocab/currency/ILS" ->
            Just "ILS"

        _ ->
            Nothing