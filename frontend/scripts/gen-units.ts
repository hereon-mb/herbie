import * as CodeGen from "elm-codegen";
import * as fs from "fs"
import * as path from "path"

const directoryPath = __dirname

const filePathUnits = path.join(directoryPath, "units.json")
const rawUnits = fs.readFileSync(filePathUnits, "utf-8")
const units = JSON.parse(rawUnits);

const filePathCurs = path.join(directoryPath, "curs.json")
const rawCurs = fs.readFileSync(filePathCurs, "utf-8")
const curs = JSON.parse(rawCurs);

const bindings = units.results.bindings.concat(curs.results.bindings);

CodeGen.run("QudtUnits.elm", {
  debug: true,
  output: "generated",
  flags: bindings,
  cwd: "./codegen"
});
