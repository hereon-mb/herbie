#!/usr/bin/env bash

set -x

sparql \
  --data unit.ttl \
  --query query-unit-info.rq \
  --results JSON \
  > units.json

sparql \
  --data cur.ttl \
  --query query-cur-info.rq \
  --results JSON \
  > curs.json
