module Api.DateTest exposing (suite)

import Api.Date
import Derberos.Date.Core as Calendar
import Expect
import Fuzz exposing (Fuzzer)
import Json.Decode as Decode
import Json.Encode as Encode
import Test exposing (Test, concat, fuzz, test)
import Time exposing (Posix)


suite : Test
suite =
    concat
        [ test "decode" <|
            \_ ->
                "2020-12-09"
                    |> Encode.string
                    |> Decode.decodeValue Api.Date.decoder
                    |> Expect.equal (Ok (Time.millisToPosix 1607472000000))
        , test "encode" <|
            \_ ->
                Time.millisToPosix 1607472000000
                    |> Api.Date.encode
                    |> Decode.decodeValue Decode.string
                    |> Expect.equal (Ok "2020-12-09")
        , fuzz fuzzDate "encode >> decode === identity" <|
            \date ->
                date
                    |> Api.Date.encode
                    |> Decode.decodeValue Api.Date.decoder
                    |> Expect.equal (Ok date)
        , fuzz fuzzDateString "decode >> encode === identity" <|
            \dateString ->
                dateString
                    |> Encode.string
                    |> Decode.decodeValue Api.Date.decoder
                    |> Result.map Api.Date.encode
                    |> Expect.equal (Ok (Encode.string dateString))
        ]


fuzzDate : Fuzzer Posix
fuzzDate =
    Fuzz.map3
        (\year month day ->
            Calendar.civilToPosix
                { year = year
                , month = month
                , day = day
                , hour = 0
                , minute = 0
                , second = 0
                , millis = 0
                , zone = Time.utc
                }
        )
        (Fuzz.intRange 1970 2060)
        (Fuzz.intRange 1 12)
        (Fuzz.intRange 1 28)


fuzzDateString : Fuzzer String
fuzzDateString =
    Fuzz.map3
        (\year month day ->
            String.join "-"
                [ String.fromInt year
                , toPaddedInt month
                , toPaddedInt day
                ]
        )
        (Fuzz.intRange 1970 2060)
        (Fuzz.intRange 1 12)
        (Fuzz.intRange 1 28)


toPaddedInt : Int -> String
toPaddedInt int =
    if int < 10 then
        "0" ++ String.fromInt int

    else
        String.fromInt int
