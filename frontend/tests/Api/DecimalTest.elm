module Api.DecimalTest exposing (suite)

import Api.Decimal as Decimal
import Expect
import Locale
import Test exposing (Test, describe, test)


suite : Test
suite =
    describe "Api.Decimal"
        [ describe "fromString >> toString"
            [ test "with exponent via e" <|
                \_ ->
                    "123e1"
                        |> Decimal.fromString Locale.EnUS
                        |> Maybe.map (Decimal.toString Locale.EnUS)
                        |> Expect.equal (Just "1230")
            ]
        , describe "decimalPlaces"
            [ test "with no decimal point" <|
                \_ ->
                    "123"
                        |> Decimal.fromString Locale.EnUS
                        |> Maybe.map Decimal.decimalPlacesCount
                        |> Expect.equal (Just 0)
            , test "with decimal point but no decimal places" <|
                \_ ->
                    "123."
                        |> Decimal.fromString Locale.EnUS
                        |> Maybe.map Decimal.decimalPlacesCount
                        |> Expect.equal (Just 0)
            , test "with decimal point and decimal places" <|
                \_ ->
                    "123.45"
                        |> Decimal.fromString Locale.EnUS
                        |> Maybe.map Decimal.decimalPlacesCount
                        |> Expect.equal (Just 2)
            ]
        ]
