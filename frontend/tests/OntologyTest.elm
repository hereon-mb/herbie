module OntologyTest exposing (suite)

import Expect
import Ontology exposing (..)
import Rdf exposing (Iri)
import Rdf.Graph as Rdf
import Test
    exposing
        ( Test
        , describe
        , test
        )


suite : Test
suite =
    describe "Ontology"
        [ testFromGraph
        ]


testFromGraph : Test
testFromGraph =
    describe "fromGraph"
        [ testWhenEmpty
        , testWithClass
        , testWithProperty
        ]


testWhenEmpty : Test
testWhenEmpty =
    test "when empty" <|
        \_ ->
            []
                |> Rdf.fromNTriples
                |> fromGraph
                |> Expect.equal empty


testWithClass : Test
testWithClass =
    test "with class" <|
        \_ ->
            """<http://example.org/Class> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2000/01/rdf-schema#Class> .
<http://example.org/Class> <http://www.w3.org/2000/01/rdf-schema#label> "english label"@en .
<http://example.org/Class> <http://www.w3.org/2000/01/rdf-schema#label> "german label"@de .
"""
                |> Rdf.parse
                |> Result.withDefault Rdf.emptyGraph
                |> fromGraph
                |> Expect.all
                    [ classes
                        >> Expect.equalLists
                            [ { iri = ex "Class"
                              , label =
                                    [ ( "en", "english label" )
                                    , ( "de", "german label" )
                                    ]
                                        |> Rdf.stringOrLangStringFromList
                                        |> Just
                              , comment = Nothing
                              }
                            ]
                    , properties >> Expect.equalLists []
                    ]


testWithProperty : Test
testWithProperty =
    test "with property" <|
        \_ ->
            """<http://example.org/path> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/1999/02/22-rdf-syntax-ns#Property> .
<http://example.org/path> <http://www.w3.org/2000/01/rdf-schema#label> "english label"@en .
<http://example.org/path> <http://www.w3.org/2000/01/rdf-schema#label> "german label"@de .
"""
                |> Rdf.parse
                |> Result.withDefault Rdf.emptyGraph
                |> fromGraph
                |> Expect.all
                    [ classes >> Expect.equalLists []
                    , properties
                        >> Expect.equalLists
                            [ { iri = ex "path"
                              , label =
                                    [ ( "en", "english label" )
                                    , ( "de", "german label" )
                                    ]
                                        |> Rdf.stringOrLangStringFromList
                                        |> Just
                              , comment = Nothing
                              }
                            ]
                    ]


ex : String -> Iri
ex value =
    Rdf.iri ("http://example.org/" ++ value)
