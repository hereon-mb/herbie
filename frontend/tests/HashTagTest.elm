module HashTagTest exposing (suite)

import Expect
import HashTag
import Test exposing (Test, describe, test)


suite : Test
suite =
    describe "HashTag"
        [ describe "HashTag.find"
            [ test "find sample id" <|
                \_ ->
                    " #210001 "
                        |> HashTag.find
                        |> Expect.equalLists
                            [ { position = 1
                              , tag = "210001"
                              }
                            ]
            , test "find sample id before full stop" <|
                \_ ->
                    " #210001."
                        |> HashTag.find
                        |> Expect.equalLists
                            [ { position = 1
                              , tag = "210001"
                              }
                            ]
            , test "find sample id with several steps" <|
                \_ ->
                    " #210001.A1.B2.C3 "
                        |> HashTag.find
                        |> Expect.equalLists
                            [ { position = 1
                              , tag = "210001.A1.B2.C3"
                              }
                            ]
            , test "find hash tag with single character" <|
                \_ ->
                    " #a "
                        |> HashTag.find
                        |> Expect.equalLists
                            [ { position = 1
                              , tag = "a"
                              }
                            ]
            , test "find several sample ids" <|
                \_ ->
                    " #210001 #nabertherm-k4 #210001.A1 "
                        |> HashTag.find
                        |> Expect.equalLists
                            [ { position = 1
                              , tag = "210001"
                              }
                            , { position = 9
                              , tag = "nabertherm-k4"
                              }
                            , { position = 24
                              , tag = "210001.A1"
                              }
                            ]
            ]
        , describe "HashTag.findAt"
            [ test "find sample id at beginning" <|
                \_ ->
                    " #210001 "
                        |> HashTag.findAt 2
                        |> Expect.equal
                            (Just
                                { position = 1
                                , tag = "210001"
                                }
                            )
            , test "find sample id at end" <|
                \_ ->
                    " #210001 "
                        |> HashTag.findAt 8
                        |> Expect.equal
                            (Just
                                { position = 1
                                , tag = "210001"
                                }
                            )
            , test "do not find sample id before beginning" <|
                \_ ->
                    " #210001 "
                        |> HashTag.findAt 1
                        |> Expect.equal Nothing
            , test "do not find sample id after end" <|
                \_ ->
                    " #210001 "
                        |> HashTag.findAt 9
                        |> Expect.equal Nothing
            ]
        ]
