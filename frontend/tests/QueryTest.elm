module QueryTest exposing (suite)

import Expect
import Fuzz
import Query
import Test exposing (Test, describe, fuzz, test)


suite : Test
suite =
    describe "Query"
        [ scoreFor
        ]


scoreFor : Test
scoreFor =
    describe "scoreFor"
        [ emptyQuery
        , exactlyMatchOneWord
        , matchOnceVsTwice
        , matchBeginningVsMiddle
        ]


emptyQuery : Test
emptyQuery =
    fuzz Fuzz.string "score for an empty query within a random string should be 0" <|
        \haystack ->
            score "" haystack
                |> Expect.equal 0


exactlyMatchOneWord : Test
exactlyMatchOneWord =
    test "an exact match of one word should yield a positive score" <|
        \_ ->
            score "word" "word"
                |> Expect.greaterThan 0


matchOnceVsTwice : Test
matchOnceVsTwice =
    test "matching a word twice yields a higher score than matching only once" <|
        \_ ->
            score "word" "word word"
                |> Expect.greaterThan (score "word" "word")


matchBeginningVsMiddle : Test
matchBeginningVsMiddle =
    test "matching with the start of a word yields a higher score then matching in the middle" <|
        \_ ->
            score "wo" "word"
                |> Expect.greaterThan (score "or" "word")


score : String -> String -> Int
score =
    Query.scoreFor [ Just ]
