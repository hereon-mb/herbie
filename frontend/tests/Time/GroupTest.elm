module Time.GroupTest exposing (suite)

import Expect
import Fuzz
import Test exposing (Test, describe, fuzz)
import Time
import Time.Group


suite : Test
suite =
    describe "Time.Group"
        [ fuzz (Fuzz.shuffledList [ thursday1230 ]) "one value gives one group, descending" <|
            Time.Group.byDay identity False Time.utc
                >> Expect.equalLists
                    [ ( thursday1230
                      , [ ( thursday1230, thursday1230 ) ]
                      )
                    ]
        , fuzz (Fuzz.shuffledList [ thursday1230 ]) "one value gives one group, ascending" <|
            Time.Group.byDay identity True Time.utc
                >> Expect.equalLists
                    [ ( thursday1230
                      , [ ( thursday1230, thursday1230 ) ]
                      )
                    ]
        , fuzz (Fuzz.shuffledList listPosix) "values are grouped by day, descending" <|
            Time.Group.byDay identity False Time.utc
                >> Expect.equalLists
                    [ ( friday1430
                      , [ ( friday1430, friday1430 )
                        , ( friday1230, friday1230 )
                        ]
                      )
                    , ( thursday1430
                      , [ ( thursday1430, thursday1430 )
                        , ( thursday1230, thursday1230 )
                        ]
                      )
                    ]
        , fuzz (Fuzz.shuffledList listPosix) "values are grouped by day, ascending" <|
            Time.Group.byDay identity True Time.utc
                >> Expect.equalLists
                    [ ( thursday1230
                      , [ ( thursday1230, thursday1230 )
                        , ( thursday1430, thursday1430 )
                        ]
                      )
                    , ( friday1230
                      , [ ( friday1230, friday1230 )
                        , ( friday1430, friday1430 )
                        ]
                      )
                    ]
        ]


listPosix : List Time.Posix
listPosix =
    [ thursday1230
    , thursday1430
    , friday1230
    , friday1430
    ]


thursday1230 : Time.Posix
thursday1230 =
    Time.millisToPosix (30 * 60 * 1000)


thursday1430 : Time.Posix
thursday1430 =
    Time.millisToPosix (Time.posixToMillis thursday1230 + (2 * 60 * 60 * 1000))


friday1230 : Time.Posix
friday1230 =
    Time.millisToPosix (Time.posixToMillis thursday1230 + (24 * 60 * 60 * 1000))


friday1430 : Time.Posix
friday1430 =
    Time.millisToPosix (Time.posixToMillis friday1230 + (2 * 60 * 60 * 1000))
