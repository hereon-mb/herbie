module Time.FormatTest exposing (suite)

import Expect
import Test exposing (Test, describe, test)
import Time
import Time.Format


suite : Test
suite =
    describe "Time.Format"
        [ describe "posixFromDate"
            [ test "with iso 8601 format" <|
                \_ ->
                    "2006-01-02"
                        |> Time.Format.posixFromDate Time.Format.Iso8601 Time.utc
                        |> Expect.equal (Just (Time.millisToPosix 1136160000000))
            , test "with en-us format" <|
                \_ ->
                    "1/2/2006"
                        |> Time.Format.posixFromDate Time.Format.EnUs Time.utc
                        |> Expect.equal (Just (Time.millisToPosix 1136160000000))
            , test "with de format" <|
                \_ ->
                    "2.1.2006"
                        |> Time.Format.posixFromDate Time.Format.De Time.utc
                        |> Expect.equal (Just (Time.millisToPosix 1136160000000))
            , test "timezone offset (utc)" <|
                \_ ->
                    "1.1.1970"
                        |> Time.Format.posixFromDate Time.Format.De Time.utc
                        |> Expect.equal (Just (Time.millisToPosix 0))
            , test "timezone offset (+2)" <|
                \_ ->
                    "1.1.1970"
                        |> Time.Format.posixFromDate Time.Format.De (Time.customZone 120 [])
                        |> Expect.equal (Just (Time.millisToPosix (-1 * 1000 * 60 * 60 * 2)))
            ]
        , describe "posixToDate"
            [ test "with iso 8601 format" <|
                \_ ->
                    Time.millisToPosix 1136203200000
                        |> Time.Format.posixToDate Time.Format.Iso8601 Time.utc
                        |> Expect.equal "2006-01-02"
            , test "with en-us format" <|
                \_ ->
                    Time.millisToPosix 1136203200000
                        |> Time.Format.posixToDate Time.Format.EnUs Time.utc
                        |> Expect.equal "1/2/2006"
            , test "with de format" <|
                \_ ->
                    Time.millisToPosix 1136203200000
                        |> Time.Format.posixToDate Time.Format.De Time.utc
                        |> Expect.equal "2.1.2006"
            , test "timezone offset (utc)" <|
                \_ ->
                    Time.millisToPosix 0
                        |> Time.Format.posixToDate Time.Format.De Time.utc
                        |> Expect.equal "1.1.1970"
            , test "timezone offset (+2)" <|
                \_ ->
                    Time.millisToPosix (-1 * 1000 * 60 * 60 * 2)
                        |> Time.Format.posixToDate Time.Format.De (Time.customZone 120 [])
                        |> Expect.equal "1.1.1970"
            ]
        , describe "posixFromDateTime"
            [ test "with iso 8601 format" <|
                \_ ->
                    "2006-01-02T15:04:05"
                        |> Time.Format.posixFromDateTime Time.Format.Iso8601 Time.utc
                        |> Expect.equal (Just (Time.millisToPosix 1136214245000))
            , test "with en-us format" <|
                \_ ->
                    "1/2/2006, 3:04:05 PM"
                        |> Time.Format.posixFromDateTime Time.Format.EnUs Time.utc
                        |> Expect.equal (Just (Time.millisToPosix 1136214245000))
            , test "with en-us format (without seconds)" <|
                \_ ->
                    "1/2/2006, 3:04 PM"
                        |> Time.Format.posixFromDateTime Time.Format.EnUs Time.utc
                        |> Expect.equal (Just (Time.millisToPosix 1136214240000))
            , test "with en-us format (midnight)" <|
                \_ ->
                    "1/2/2006, 12:00:00 AM"
                        |> Time.Format.posixFromDateTime Time.Format.EnUs Time.utc
                        |> Expect.equal (Just (Time.millisToPosix 1136160000000))
            , test "with en-us format (noon)" <|
                \_ ->
                    "1/2/2006, 12:00:00 PM"
                        |> Time.Format.posixFromDateTime Time.Format.EnUs Time.utc
                        |> Expect.equal
                            (Just (Time.millisToPosix (1136160000000 + 12 * 60 * 60 * 1000)))
            , test "with de format" <|
                \_ ->
                    "2.1.2006, 15:04:05"
                        |> Time.Format.posixFromDateTime Time.Format.De Time.utc
                        |> Expect.equal (Just (Time.millisToPosix 1136214245000))
            , test "with de format (without seconds)" <|
                \_ ->
                    "2.1.2006, 15:04"
                        |> Time.Format.posixFromDateTime Time.Format.De Time.utc
                        |> Expect.equal (Just (Time.millisToPosix 1136214240000))
            , test "timezone offset (utc)" <|
                \_ ->
                    "1.1.1970, 0:00"
                        |> Time.Format.posixFromDateTime Time.Format.De Time.utc
                        |> Expect.equal (Just (Time.millisToPosix 0))
            , test "timezone offset (+2)" <|
                \_ ->
                    "1.1.1970, 0:00"
                        |> Time.Format.posixFromDateTime Time.Format.De (Time.customZone 120 [])
                        |> Expect.equal (Just (Time.millisToPosix (-1 * 1000 * 60 * 60 * 2)))
            ]
        , describe "posixToDateTime"
            [ test "with iso 8601 format" <|
                \_ ->
                    Time.millisToPosix 1136214245000
                        |> Time.Format.posixToDateTime Time.Format.Iso8601 Time.utc
                        |> Expect.equal "2006-01-02T15:04:05"
            , test "with en-us format" <|
                \_ ->
                    Time.millisToPosix 1136214240000
                        |> Time.Format.posixToDateTime Time.Format.EnUs Time.utc
                        |> Expect.equal "1/2/2006, 3:04 PM"
            , test "with en-us format (midnight)" <|
                \_ ->
                    Time.millisToPosix 1136160000000
                        |> Time.Format.posixToDateTime Time.Format.EnUs Time.utc
                        |> Expect.equal "1/2/2006, 12:00 AM"
            , test "with en-us format (noon)" <|
                \_ ->
                    Time.millisToPosix (1136160000000 + 12 * 60 * 60 * 1000)
                        |> Time.Format.posixToDateTime Time.Format.EnUs Time.utc
                        |> Expect.equal "1/2/2006, 12:00 PM"
            , test "with de format" <|
                \_ ->
                    Time.millisToPosix 1136214240000
                        |> Time.Format.posixToDateTime Time.Format.De Time.utc
                        |> Expect.equal "2.1.2006, 15:04"
            , test "timezone offset (utc)" <|
                \_ ->
                    Time.millisToPosix 0
                        |> Time.Format.posixToDateTime Time.Format.De Time.utc
                        |> Expect.equal "1.1.1970, 0:00"
            , test "timezone offset (+2)" <|
                \_ ->
                    Time.millisToPosix (-1 * 1000 * 60 * 60 * 2)
                        |> Time.Format.posixToDateTime Time.Format.De (Time.customZone 120 [])
                        |> Expect.equal "1.1.1970, 0:00"
            ]
        ]
