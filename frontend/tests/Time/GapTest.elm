module Time.GapTest exposing (suite)

import Expect
import Fuzz
import Test exposing (Test, describe, fuzz, test)
import Time
import Time.Gap


suite : Test
suite =
    describe "Time.Gap"
        [ describe "fromPosix"
            [ test "with no times" <|
                \_ ->
                    []
                        |> Time.Gap.fromPosix
                        |> Expect.equal []
            , test "with one times" <|
                \_ ->
                    [ Time.millisToPosix 0 ]
                        |> Time.Gap.fromPosix
                        |> Expect.equal []
            , fuzz (Fuzz.intRange 0 20) "with many times" <|
                \int ->
                    List.range 0 (abs int)
                        |> List.map Time.millisToPosix
                        |> Time.Gap.fromPosix
                        |> Expect.equal (List.repeat int 1)
            ]
        ]
