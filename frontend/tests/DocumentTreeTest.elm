module DocumentTreeTest exposing (suite)

import DocumentTree exposing (DocumentTree(..))
import Expect
import Test exposing (Test, describe, test)


suite : Test
suite =
    describe "DocumentTree"
        [ describe "fromList"
            [ empty
            , singleUrl
            , twoDistinctUrls
            , twoUrlsWithSameDomain
            , twoUrlsWithSameDomainIncludingShared
            , severalPathsShared
            , twoNestings
            ]
        ]


empty : Test
empty =
    test "empty list" <|
        \_ ->
            []
                |> DocumentTree.fromList
                |> Expect.equalLists []


singleUrl : Test
singleUrl =
    test "one url" <|
        \_ ->
            [ "http://example.org/" ]
                |> DocumentTree.fromList
                |> Expect.equalLists [ Document "http://example.org/" ]


twoDistinctUrls : Test
twoDistinctUrls =
    test "two distinct urls" <|
        \_ ->
            [ "http://example.org/"
            , "http://example.com/"
            ]
                |> DocumentTree.fromList
                |> Expect.equalLists
                    [ Document "http://example.com/"
                    , Document "http://example.org/"
                    ]


twoUrlsWithSameDomain : Test
twoUrlsWithSameDomain =
    test "two urls with the same domain" <|
        \_ ->
            [ "http://example.org/alice"
            , "http://example.org/bob"
            ]
                |> DocumentTree.fromList
                |> Expect.equalLists
                    [ Folder "http://example.org/"
                        [ Document "http://example.org/alice"
                        , Document "http://example.org/bob"
                        ]
                    ]


twoUrlsWithSameDomainIncludingShared : Test
twoUrlsWithSameDomainIncludingShared =
    test "two urls with the same domain, including the shared domain" <|
        \_ ->
            [ "http://example.org/alice"
            , "http://example.org/bob"
            , "http://example.org/"
            ]
                |> DocumentTree.fromList
                |> Expect.equalLists
                    [ Document "http://example.org/"
                    , Folder "http://example.org/"
                        [ Document "http://example.org/alice"
                        , Document "http://example.org/bob"
                        ]
                    ]


severalPathsShared : Test
severalPathsShared =
    test "two urls which share several paths" <|
        \_ ->
            [ "http://example.org/users/alice"
            , "http://example.org/users/bob"
            ]
                |> DocumentTree.fromList
                |> Expect.equalLists
                    [ Folder "http://example.org/users/"
                        [ Document "http://example.org/users/alice"
                        , Document "http://example.org/users/bob"
                        ]
                    ]


twoNestings : Test
twoNestings =
    test "three urls causing two nestings" <|
        \_ ->
            [ "http://example.org/users/alice"
            , "http://example.org/users/alice/preferences"
            , "http://example.org/users/alice/documents"
            , "http://example.org/users/bob"
            ]
                |> DocumentTree.fromList
                |> Expect.equalLists
                    [ Folder "http://example.org/users/"
                        [ Document "http://example.org/users/alice"
                        , Folder "http://example.org/users/alice/"
                            [ Document "http://example.org/users/alice/documents"
                            , Document "http://example.org/users/alice/preferences"
                            ]
                        , Document "http://example.org/users/bob"
                        ]
                    ]
