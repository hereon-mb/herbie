module ShaclTest exposing (suite)

import Expect exposing (Expectation)
import Rdf exposing (Iri)
import Rdf.Graph as Rdf
import Rdf.Namespaces exposing (xsd)
import Rdf.PropertyPath exposing (PropertyPath(..))
import Shacl exposing (..)
import Test
    exposing
        ( Test
        , describe
        , test
        )


suite : Test
suite =
    describe "Shacl"
        [ testFromNTriples
        ]


testFromNTriples : Test
testFromNTriples =
    describe "fromNTriples"
        [ testNoTriples
        , testNodeShape
        , testPropertyShape
        , testPropertyPathSequencePath
        , testPropertyPathAlternativePath
        , testPropertyPathInversePath
        , testPropertyPathZeroOrMorePath
        , testPropertyPathOneOrMorePath
        , testPropertyPathZeroOrOnePath
        , testPropertyGroup
        , testNodeExpressionFocusNode
        ]


testNoTriples : Test
testNoTriples =
    test "no triples" <|
        \_ ->
            []
                |> Rdf.fromNTriples
                |> fromGraph
                |> Result.withDefault Shacl.empty
                |> Expect.equal empty


testNodeShape : Test
testNodeShape =
    test "node shape" <|
        \_ ->
            """
            @prefix example: <http://example.org/> .
            @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
            @prefix sh: <http://www.w3.org/ns/shacl#> .

            example:shape a sh:NodeShape ;
                sh:targetClass example:Class ;
                sh:property example:property1 ;
                sh:property example:property2 ;
            .
            example:Class a rdfs:Class ;
                rdfs:label "english label"@en ;
                rdfs:label "german label"@de ;
            .
            example:property1 sh:path example:path1 .
            example:property2 sh:path example:path2 .
            """
                |> Rdf.parse
                |> Result.withDefault Rdf.emptyGraph
                |> fromGraph
                |> Result.withDefault Shacl.empty
                |> Expect.all
                    [ propertyShapes
                        >> Expect.equalLists
                            [ { node = Rdf.asBlankNodeOrIri (ex "property1")
                              , shTargetClass = []
                              , shGroup = Nothing
                              , shOrder = Nothing
                              , shName = Nothing
                              , shDescription = Nothing
                              , shPath = PredicatePath (ex "path1")
                              , shClass = []
                              , shNode = []
                              , shDatatype = Nothing
                              , shNodeKind = Nothing
                              , shMinExclusive = Nothing
                              , shMinInclusive = Nothing
                              , shMaxExclusive = Nothing
                              , shMaxInclusive = Nothing
                              , shMinCount = []
                              , shMaxCount = []
                              , dashEditor = Nothing
                              , hashDetailsEditorOrderPath = Nothing
                              , dashViewer = Nothing
                              , dashSingleLine = Nothing
                              , shHasValue = []
                              , shIn = []
                              , shValue = Nothing
                              , shXone = []
                              , shQualifiedValueShape = Nothing
                              , shQualifiedMinCount = Nothing
                              , shQualifiedMaxCount = Nothing
                              , hashLabelInCard = Nothing
                              , hashReadonly = Nothing
                              , hashGenerated = Nothing
                              , hashPersisted = Nothing
                              , hashOptionalViaCheckbox = Nothing
                              , shDefaultValue = []
                              , hashInclude = []
                              , hashOrderBy = Nothing
                              }
                            , { node = Rdf.asBlankNodeOrIri (ex "property2")
                              , shTargetClass = []
                              , shGroup = Nothing
                              , shOrder = Nothing
                              , shName = Nothing
                              , shDescription = Nothing
                              , shPath = PredicatePath (ex "path2")
                              , shClass = []
                              , shNode = []
                              , shDatatype = Nothing
                              , shNodeKind = Nothing
                              , shMinExclusive = Nothing
                              , shMinInclusive = Nothing
                              , shMaxExclusive = Nothing
                              , shMaxInclusive = Nothing
                              , shMinCount = []
                              , shMaxCount = []
                              , dashEditor = Nothing
                              , hashDetailsEditorOrderPath = Nothing
                              , dashViewer = Nothing
                              , dashSingleLine = Nothing
                              , shHasValue = []
                              , shIn = []
                              , shValue = Nothing
                              , shXone = []
                              , shQualifiedValueShape = Nothing
                              , shQualifiedMinCount = Nothing
                              , shQualifiedMaxCount = Nothing
                              , hashLabelInCard = Nothing
                              , hashReadonly = Nothing
                              , hashGenerated = Nothing
                              , hashPersisted = Nothing
                              , hashOptionalViaCheckbox = Nothing
                              , shDefaultValue = []
                              , hashInclude = []
                              , hashOrderBy = Nothing
                              }
                            ]
                    , nodeShapes
                        >> Expect.equalLists
                            [ { node = Rdf.asBlankNodeOrIri (ex "shape")
                              , shTargetClass = [ ex "Class" ]
                              , shClass = []
                              , shHasValue = []
                              , shIn = []
                              , shProperty =
                                    [ Rdf.asBlankNodeOrIri (ex "property2")
                                    , Rdf.asBlankNodeOrIri (ex "property1")
                                    ]
                              , shXone = []
                              , shOr = []
                              , shNode = []
                              , shName = Nothing
                              , rdfsLabel = Nothing
                              , rdfsComment = Nothing
                              , hashDocumentRoot = False
                              }
                            ]
                    , propertyGroups >> Expect.equalLists []
                    ]


testPropertyShape : Test
testPropertyShape =
    test "property shape" <|
        \_ ->
            prefixes
                ++ """
                   example:propertyShape a sh:PropertyShape ;
                       sh:group example:propertyGroup ;
                       sh:order "1.5"^^xsd:double ;
                       sh:path example:path ;
                       sh:node example:nodeA ;
                       sh:node example:nodeB ;
                       sh:class example:classA ;
                       sh:class example:classB ;
                       sh:datatype xsd:string ;
                       sh:minCount "1"^^xsd:integer ;
                       sh:minCount "2"^^xsd:integer ;
                       sh:maxCount "3"^^xsd:integer ;
                       sh:maxCount "4"^^xsd:integer ;
                       dash:editor dash:InstancesSelectEditor ;
                   .
                   example:path a rdf:Property ;
                       rdfs:label "english label"@en ;
                       rdfs:label "german label"@de ;
                   .
                   """
                |> Rdf.parse
                |> Result.withDefault Rdf.emptyGraph
                |> fromGraph
                |> Result.withDefault empty
                |> Expect.all
                    [ nodeShapes
                        >> Expect.equalLists
                            [ { node = Rdf.asBlankNodeOrIri (ex "nodeA")
                              , shTargetClass = []
                              , shClass = []
                              , shHasValue = []
                              , shIn = []
                              , shProperty = []
                              , shXone = []
                              , shOr = []
                              , shNode = []
                              , shName = Nothing
                              , rdfsLabel = Nothing
                              , rdfsComment = Nothing
                              , hashDocumentRoot = False
                              }
                            , { node = Rdf.asBlankNodeOrIri (ex "nodeB")
                              , shTargetClass = []
                              , shClass = []
                              , shHasValue = []
                              , shIn = []
                              , shProperty = []
                              , shXone = []
                              , shOr = []
                              , shNode = []
                              , shName = Nothing
                              , rdfsLabel = Nothing
                              , rdfsComment = Nothing
                              , hashDocumentRoot = False
                              }
                            ]
                    , propertyShapes
                        >> Expect.equalLists
                            [ { node = Rdf.asBlankNodeOrIri (ex "propertyShape")
                              , shTargetClass = []
                              , shGroup = Just (ex "propertyGroup")
                              , shOrder = Just 1.5
                              , shName = Nothing
                              , shDescription = Nothing
                              , shPath = PredicatePath (ex "path")
                              , shClass =
                                    [ ex "classB"
                                    , ex "classA"
                                    ]
                              , shNode =
                                    [ Rdf.asBlankNodeOrIri (ex "nodeB")
                                    , Rdf.asBlankNodeOrIri (ex "nodeA")
                                    ]
                              , shDatatype = Just (xsd "string")
                              , shNodeKind = Nothing
                              , shMinExclusive = Nothing
                              , shMinInclusive = Nothing
                              , shMaxExclusive = Nothing
                              , shMaxInclusive = Nothing
                              , shMinCount = [ 2, 1 ]
                              , shMaxCount = [ 4, 3 ]
                              , dashEditor = Just InstancesSelectEditor
                              , hashDetailsEditorOrderPath = Nothing
                              , dashViewer = Nothing
                              , dashSingleLine = Nothing
                              , shHasValue = []
                              , shIn = []
                              , shValue = Nothing
                              , shXone = []
                              , shQualifiedValueShape = Nothing
                              , shQualifiedMinCount = Nothing
                              , shQualifiedMaxCount = Nothing
                              , hashLabelInCard = Nothing
                              , hashReadonly = Nothing
                              , hashGenerated = Nothing
                              , hashPersisted = Nothing
                              , hashOptionalViaCheckbox = Nothing
                              , shDefaultValue = []
                              , hashInclude = []
                              , hashOrderBy = Nothing
                              }
                            ]
                    , propertyGroups >> Expect.equalLists []
                    ]


testPropertyPathSequencePath : Test
testPropertyPathSequencePath =
    test "sequence path" <|
        \_ ->
            prefixes
                ++ """
                    [] a sh:PropertyShape ;
                        sh:name "english label"@en ;
                        sh:path ( example:pathA example:pathB ) .
                   """
                |> expectPropertyPaths
                    [ SequencePath
                        (PredicatePath (ex "pathA"))
                        [ PredicatePath (ex "pathB") ]
                    ]


testPropertyPathAlternativePath : Test
testPropertyPathAlternativePath =
    test "alternative path" <|
        \_ ->
            prefixes
                ++ """
                    [] a <http://www.w3.org/ns/shacl#PropertyShape> ;
                        sh:name "english label"@en ;
                        sh:path [ sh:alternativePath ( example:pathA example:pathB ) ] .
                   """
                |> expectPropertyPaths
                    [ AlternativePath
                        (PredicatePath (ex "pathA"))
                        [ PredicatePath (ex "pathB") ]
                    ]


expectPropertyPaths : List PropertyPath -> String -> Expectation
expectPropertyPaths paths raw =
    raw
        |> Rdf.parse
        |> Result.withDefault Rdf.emptyGraph
        |> fromGraph
        |> Result.withDefault Shacl.empty
        |> propertyShapes
        |> List.map .shPath
        |> Expect.equalLists paths


testPropertyPathInversePath : Test
testPropertyPathInversePath =
    test "inverse path" <|
        \_ ->
            prefixes
                ++ """
                    [] a sh:PropertyShape ;
                        sh:name "english label"@en ;
                        sh:path [ sh:inversePath example:pathA ] .
                   """
                |> expectPropertyPaths
                    [ InversePath (PredicatePath (ex "pathA")) ]


testPropertyPathZeroOrMorePath : Test
testPropertyPathZeroOrMorePath =
    test "zero or more path" <|
        \_ ->
            prefixes
                ++ """
                    [] a sh:PropertyShape ;
                        sh:name "english label"@en ;
                        sh:path [ sh:zeroOrMorePath example:pathA ] .
                    """
                |> expectPropertyPaths
                    [ ZeroOrMorePath (PredicatePath (ex "pathA")) ]


testPropertyPathOneOrMorePath : Test
testPropertyPathOneOrMorePath =
    test "one or more path" <|
        \_ ->
            prefixes
                ++ """
                    [] a sh:PropertyShape ;
                        sh:name "english label"@en ;
                        sh:path [ sh:oneOrMorePath example:pathA ] .
                    """
                |> expectPropertyPaths
                    [ OneOrMorePath (PredicatePath (ex "pathA")) ]


testPropertyPathZeroOrOnePath : Test
testPropertyPathZeroOrOnePath =
    test "zero or one path" <|
        \_ ->
            prefixes
                ++ """
                    [] a sh:PropertyShape ;
                        sh:name "english label"@en ;
                        sh:path [ sh:zeroOrOnePath example:pathA ] .
                    """
                |> expectPropertyPaths
                    [ ZeroOrOnePath (PredicatePath (ex "pathA")) ]


testPropertyGroup : Test
testPropertyGroup =
    test "property group" <|
        \_ ->
            prefixes
                ++ """
                    example:propertyGroup a sh:PropertyGroup ;
                        sh:group example:otherPropertyGroup ;
                        sh:order "1.5"^^xsd:double ;
                        rdfs:label "english label"@en ;
                        rdfs:label "german label"@de .
                    """
                |> Rdf.parse
                |> Result.withDefault Rdf.emptyGraph
                |> fromGraph
                |> Result.withDefault empty
                |> Expect.all
                    [ nodeShapes >> Expect.equalLists []
                    , propertyShapes >> Expect.equalLists []
                    , propertyGroups
                        >> Expect.equalLists
                            [ { group = Just (ex "otherPropertyGroup")
                              , order = Just 1.5
                              , name =
                                    [ ( "en", "english label" )
                                    , ( "de", "german label" )
                                    ]
                                        |> Rdf.stringOrLangStringFromList
                                        |> Just
                              }
                            ]
                    ]


testNodeExpressionFocusNode : Test
testNodeExpressionFocusNode =
    test "focus node expression" <|
        \_ ->
            prefixes
                ++ """
                    [] a sh:PropertyShape ;
                        sh:path example:path ;
                        sh:values sh:this .
                    """
                |> Rdf.parse
                |> Result.withDefault Rdf.emptyGraph
                |> fromGraph
                |> Result.withDefault Shacl.empty
                |> propertyShapes
                |> List.filterMap .shValue
                |> Expect.equalLists
                    [ FocusNodeExpression ]


ex : String -> Iri
ex value =
    Rdf.iri ("http://example.org/" ++ value)


prefixes : String
prefixes =
    """
    @prefix example: <http://example.org/> .
    @prefix dash: <http://datashapes.org/dash#> .
    @prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
    @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
    @prefix sh: <http://www.w3.org/ns/shacl#> .
    @prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
    """
