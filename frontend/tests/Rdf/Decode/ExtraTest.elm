module Rdf.Decode.ExtraTest exposing (suite)

import Expect exposing (Expectation)
import Rdf
import Rdf.Decode as Decode exposing (Decoder)
import Rdf.Decode.Extra as Decode
import Rdf.Graph as Graph
import Rdf.PropertyPath as Rdf
import Test exposing (Test, describe, test)


suite : Test
suite =
    describe "Rdf.Decode.Extra"
        [ describe "zeroOrOneAt"
            [ zeroOrOneAt_noProperty
            , zeroOrOneAt_oneCorrectProperty
            , zeroOrOneAt_oneIncorrectProperty
            , zeroOrOneAt_oneCorrectProperty_oneIncorrectProperty
            , zeroOrOneAt_twoCorrectProperties
            ]
        , describe "zeroOrManyAt"
            [ zeroOrManyAt_noProperty
            , zeroOrManyAt_oneCorrectProperty
            , zeroOrManyAt_oneIncorrectProperty
            , zeroOrManyAt_oneCorrectProperty_oneIncorrectProperty
            , zeroOrManyAt_twoCorrectProperties
            ]
        , describe "exact"
            [ exactSuccess
            , exactFailure
            ]
        , describe "instanceOfAll"
            [ instanceOfAllWithOneInstance
            , instanceOfAllWithOneInstanceWithAdditionalClasses
            , instanceOfAllWithOneInstanceAtProperty
            , instanceOfAllWithOneInstanceAndOneOtherInstanceAtProperty
            , instanceOfAllWithTwoInstancesAtProperty
            , instanceOfAllWithTwoInstancesOneNotSuccessfullAtProperty
            ]
        ]


zeroOrOneAt_noProperty : Test
zeroOrOneAt_noProperty =
    test "no property" <|
        \_ ->
            { raw =
                """
                @base <http://example.org/> .
                <x> <hasString> "string" .
                """
            , decoder =
                Decode.from (example "x")
                    (Decode.zeroOrOneAt (Rdf.PredicatePath (example "hasInteger")) Decode.int)
            }
                |> expectAll [ Expect.equal Nothing ]


zeroOrOneAt_oneCorrectProperty : Test
zeroOrOneAt_oneCorrectProperty =
    test "one correct property" <|
        \_ ->
            { raw =
                """
                @base <http://example.org/> .
                <x> <hasInteger> 42 .
                """
            , decoder =
                Decode.from (example "x")
                    (Decode.zeroOrOneAt (Rdf.PredicatePath (example "hasInteger")) Decode.int)
            }
                |> expectAll [ Expect.equal (Just 42) ]


zeroOrOneAt_oneIncorrectProperty : Test
zeroOrOneAt_oneIncorrectProperty =
    test "one incorrect property" <|
        \_ ->
            { raw =
                """
                @base <http://example.org/> .
                <x> <hasInteger> "string" .
                """
            , decoder =
                Decode.from (example "x")
                    (Decode.zeroOrOneAt (Rdf.PredicatePath (example "hasInteger")) Decode.int)
            }
                |> expectAll [ Expect.equal Nothing ]


zeroOrOneAt_oneCorrectProperty_oneIncorrectProperty : Test
zeroOrOneAt_oneCorrectProperty_oneIncorrectProperty =
    test "one correct property and one incorrect property" <|
        \_ ->
            { raw =
                """
                @base <http://example.org/> .
                <x> <hasInteger> 42 , "string" .
                """
            , decoder =
                Decode.from (example "x")
                    (Decode.zeroOrOneAt (Rdf.PredicatePath (example "hasInteger")) Decode.int)
            }
                |> expectAll [ Expect.equal (Just 42) ]


zeroOrOneAt_twoCorrectProperties : Test
zeroOrOneAt_twoCorrectProperties =
    test "two correct properties" <|
        \_ ->
            { raw =
                """
                @base <http://example.org/> .
                <x> <hasInteger> 42, 84 .
                """
            , decoder =
                Decode.from (example "x")
                    (Decode.zeroOrOneAt (Rdf.PredicatePath (example "hasInteger")) Decode.int)
            }
                |> expectAllError
                    [ Expect.equal
                        (Decode.Batch
                            [ Decode.PropertyPresent
                                (Rdf.asBlankNodeOrIri (example "x"))
                                (Rdf.PredicatePath (example "hasInteger"))
                            , Decode.CustomError "Found more then one decodable values"
                            ]
                        )
                    ]


zeroOrManyAt_noProperty : Test
zeroOrManyAt_noProperty =
    test "no property" <|
        \_ ->
            { raw =
                """
                @base <http://example.org/> .
                <x> <hasString> "string" .
                """
            , decoder =
                Decode.from (example "x")
                    (Decode.zeroOrManyAt (Rdf.PredicatePath (example "hasInteger")) Decode.int)
            }
                |> expectAll [ Expect.equal [] ]


zeroOrManyAt_oneCorrectProperty : Test
zeroOrManyAt_oneCorrectProperty =
    test "one correct property" <|
        \_ ->
            { raw =
                """
                @base <http://example.org/> .
                <x> <hasInteger> 42 .
                """
            , decoder =
                Decode.from (example "x")
                    (Decode.zeroOrManyAt (Rdf.PredicatePath (example "hasInteger")) Decode.int)
            }
                |> expectAll [ Expect.equal [ 42 ] ]


zeroOrManyAt_oneIncorrectProperty : Test
zeroOrManyAt_oneIncorrectProperty =
    test "one incorrect property" <|
        \_ ->
            { raw =
                """
                @base <http://example.org/> .
                <x> <hasInteger> "string" .
                """
            , decoder =
                Decode.from (example "x")
                    (Decode.zeroOrManyAt (Rdf.PredicatePath (example "hasInteger")) Decode.int)
            }
                |> expectAll [ Expect.equal [] ]


zeroOrManyAt_oneCorrectProperty_oneIncorrectProperty : Test
zeroOrManyAt_oneCorrectProperty_oneIncorrectProperty =
    test "one correct property and one incorrect property" <|
        \_ ->
            { raw =
                """
                @base <http://example.org/> .
                <x> <hasInteger> 42 , "string" .
                """
            , decoder =
                Decode.from (example "x")
                    (Decode.zeroOrManyAt (Rdf.PredicatePath (example "hasInteger")) Decode.int)
            }
                |> expectAll [ Expect.equal [ 42 ] ]


zeroOrManyAt_twoCorrectProperties : Test
zeroOrManyAt_twoCorrectProperties =
    test "two correct properties" <|
        \_ ->
            { raw =
                """
                @base <http://example.org/> .
                <x> <hasInteger> 42, 84 .
                """
            , decoder =
                Decode.from (example "x")
                    (Decode.zeroOrManyAt (Rdf.PredicatePath (example "hasInteger")) Decode.int)
            }
                |> expectAll
                    [ Expect.equal [ 84, 42 ]
                    ]


exactSuccess : Test
exactSuccess =
    test "success" <|
        \_ ->
            { raw =
                """
                    @base <http://example.org/> .
                    <x> <hasInstance> <a> .
                """
            , decoder =
                Decode.from (example "x")
                    (Decode.predicate (example "hasInstance")
                        (Decode.exact (example "a"))
                    )
            }
                |> expectAll [ Expect.equal () ]


exactFailure : Test
exactFailure =
    test "failure" <|
        \_ ->
            { raw =
                """
                    @base <http://example.org/> .
                    <x> <hasInstance> <a> .
                """
            , decoder =
                Decode.from (example "x")
                    (Decode.predicate (example "hasInstance")
                        (Decode.exact (example "b"))
                    )
            }
                |> expectAllError
                    [ Expect.equal
                        (Decode.CustomError "expected <http://example.org/b> but found <http://example.org/a>")
                    ]


instanceOfAllWithOneInstance : Test
instanceOfAllWithOneInstance =
    test "with one instance" <|
        \_ ->
            { raw =
                """
                    @base <http://example.org/> .
                    <x> a <ClassA> .
                """
            , decoder =
                Decode.from (example "x")
                    (Decode.instanceOfAll [ example "ClassA" ] (Decode.succeed ()))
            }
                |> expectAll [ Expect.equal () ]


instanceOfAllWithOneInstanceWithAdditionalClasses : Test
instanceOfAllWithOneInstanceWithAdditionalClasses =
    test "with one instance with additional classes" <|
        \_ ->
            { raw =
                """
                    @base <http://example.org/> .
                    <x> a <ClassA> , <ClassB> .
                """
            , decoder =
                Decode.from (example "x")
                    (Decode.instanceOfAll [ example "ClassA" ] (Decode.succeed ()))
            }
                |> expectAll [ Expect.equal () ]


instanceOfAllWithOneInstanceAtProperty : Test
instanceOfAllWithOneInstanceAtProperty =
    test "with one instance at property" <|
        \_ ->
            { raw =
                """
                    @base <http://example.org/> .
                    <x> <hasInstance> [
                        a <ClassA> ]
                    .
                """
            , decoder =
                Decode.from (example "x")
                    (Decode.predicate (example "hasInstance")
                        (Decode.instanceOfAll [ example "ClassA" ] (Decode.succeed ()))
                    )
            }
                |> expectAll [ Expect.equal () ]


instanceOfAllWithOneInstanceAndOneOtherInstanceAtProperty : Test
instanceOfAllWithOneInstanceAndOneOtherInstanceAtProperty =
    test "with one instance and one other instance at property" <|
        \_ ->
            { raw =
                """
                    @base <http://example.org/> .
                    <x> <hasInstance>
                        [ a <ClassA> ] ,
                        [ a <ClassB> ] ,
                    .
                """
            , decoder =
                Decode.from (example "x")
                    (Decode.predicate (example "hasInstance")
                        (Decode.instanceOfAll [ example "ClassA" ] (Decode.succeed ()))
                    )
            }
                |> expectAll [ Expect.equal () ]


instanceOfAllWithTwoInstancesAtProperty : Test
instanceOfAllWithTwoInstancesAtProperty =
    test "with two instances at property" <|
        \_ ->
            { raw =
                """
                    @base <http://example.org/> .
                    <x> <hasInstance>
                        [ a <ClassA> ] ,
                        [ a <ClassA> ] ,
                    .
                """
            , decoder =
                Decode.from (example "x")
                    (Decode.predicate (example "hasInstance")
                        (Decode.instanceOfAll [ example "ClassA" ] (Decode.succeed ()))
                    )
            }
                |> expectAllError
                    [ Expect.equal
                        (Decode.CustomError "Found more then one instance of <http://example.org/ClassA>")
                    ]


instanceOfAllWithTwoInstancesOneNotSuccessfullAtProperty : Test
instanceOfAllWithTwoInstancesOneNotSuccessfullAtProperty =
    test "with two instances, one not successfull, at property" <|
        \_ ->
            { raw =
                """
                    @base <http://example.org/> .
                    <x> <hasInstance>
                        [ a <ClassA> ; <hasString> "string" ] ,
                        [ a <ClassA> ; <hasInteger> 42 ] ,
                    .
                """
            , decoder =
                Decode.from (example "x")
                    (Decode.predicate (example "hasInstance")
                        (Decode.instanceOfAll [ example "ClassA" ]
                            (Decode.predicate (example "hasString") Decode.string)
                        )
                    )
            }
                |> expectAll [ Expect.equal "string" ]


example : String -> Rdf.Iri
example name =
    Rdf.iri ("http://example.org/" ++ name)


expectAll : List (a -> Expectation) -> { raw : String, decoder : Decoder a } -> Expectation
expectAll expectations { raw, decoder } =
    case Graph.parse raw of
        Err error ->
            Expect.fail ("Could not parse the graph: " ++ Graph.errorToString raw error)

        Ok graph ->
            case Decode.decode decoder graph of
                Err error ->
                    Expect.fail ("Could not decode the value: " ++ Decode.errorToString error)

                Ok value ->
                    Expect.all expectations value


expectAllError : List (Decode.Error -> Expectation) -> { raw : String, decoder : Decoder a } -> Expectation
expectAllError expectations { raw, decoder } =
    case Graph.parse raw of
        Err error ->
            Expect.fail ("Could not parse the graph: " ++ Graph.errorToString raw error)

        Ok graph ->
            case Decode.decode decoder graph of
                Err error ->
                    Expect.all expectations error

                Ok _ ->
                    Expect.fail "Could decode the value."
