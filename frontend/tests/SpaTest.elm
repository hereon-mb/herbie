module SpaTest exposing
    ( shared
    , toDocument
    )

import Browser
import Fixture.Context as Context
import Random
import Rdf.DictIri as DictIri
import Rdf.DictIri.ApiData as DictIriApiData
import Shared exposing (Dialog(..))
import Shared.Workspace
import Toast
import Ui.Atom.Scrolled
import Ui.Theme.Layout
import View exposing (View)


shared : Shared.Model
shared =
    let
        context =
            Context.default
    in
    Shared.initDebug
        { tray = Toast.tray
        , context = context
        , seeds =
            { seed1 = Random.initialSeed 1
            , seed2 = Random.initialSeed 2
            , seed3 = Random.initialSeed 3
            , seed4 = Random.initialSeed 4
            }
        , showHelp = False
        , helpOffsets = Ui.Atom.Scrolled.initialOffsets
        , workspace = Tuple.first (Shared.Workspace.init context)
        , dialog = NoDialog
        , instances = DictIriApiData.empty
        , graphsClosureLoading = DictIri.empty
        , triplesPrev = Nothing
        , triplesNext = Nothing
        , fetchingTriplesNext = False
        , fetchingTriplesNextNeeded = False
        , graphs = DictIriApiData.empty
        , graphsClosure = DictIriApiData.empty
        , creatingFields = False
        , duplicatingFields = False
        }


toDocument : View msg -> Browser.Document msg
toDocument { element } =
    { title = ""
    , body = [ Ui.Theme.Layout.default element ]
    }



{- FIXME re-add once avh4/elm-program-test is updated to latest elm-explorations/test

   start : String -> ProgramDefinition Shared.Flags model msg effect -> ProgramTest model msg effect
   start path definition =
       definition
           |> ProgramTest.withBaseUrl ("https://eln.fzg.local" ++ path)
           |> ProgramTest.start (flags path)
-}
{- FIXME re-add once avh4/elm-program-test is updated to latest elm-explorations/test

   clickButton : ProgramTest model msg effect -> ProgramTest model msg effect
   clickButton =
       ProgramTest.simulateDomEvent
           (Test.Html.Query.find
               [ Test.Html.Selector.attribute
                   (Html.Attributes.attribute "role" "button")
               , Test.Html.Selector.containing
                   [ Test.Html.Selector.attribute
                       (Html.Attributes.attribute
                           "messageId"
                           "pages-login__login"
                       )
                   ]
               ]
           )
           Test.Html.Event.click
-}
