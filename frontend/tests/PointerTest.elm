module PointerTest exposing (suite)

import Api.Id
import Expect
import Fuzz exposing (Fuzzer)
import Pointer exposing (Pointer)
import Test exposing (Test, describe, fuzz)


suite : Test
suite =
    describe "Pointer"
        [ fuzz (Fuzz.listOfLengthBetween 1 32 pointer) "result of toString starts with '/'" <|
            \pointers ->
                pointers
                    |> List.foldl Pointer.append Pointer.empty
                    |> Pointer.toString
                    |> String.left 1
                    |> Expect.equal "/"
        , fuzz (Fuzz.listOfLengthBetween 1 32 pointer) "result of toString does not end with '/', when not empty" <|
            \pointers ->
                pointers
                    |> List.foldl Pointer.append Pointer.empty
                    |> Pointer.toString
                    |> String.right 1
                    |> Expect.notEqual "/"
        , fuzz (Fuzz.listOfLengthBetween 1 32 pointer) "result of toString contains one '/' for each scope" <|
            \pointers ->
                pointers
                    |> List.foldl Pointer.append Pointer.empty
                    |> Pointer.toString
                    |> String.indexes "/"
                    |> List.length
                    |> Expect.equal (List.length pointers)
        , fuzz (Fuzz.listOfLengthBetween 1 32 pointer) "result of toString does not contain consecutive '/'" <|
            \pointers ->
                pointers
                    |> List.foldl Pointer.append Pointer.empty
                    |> Pointer.toString
                    |> String.contains "//"
                    |> Expect.equal False
        ]


pointer : Fuzzer Pointer
pointer =
    Fuzz.oneOf
        [ field
        , id
        ]


field : Fuzzer Pointer
field =
    Fuzz.map Pointer.field
        (Fuzz.asciiStringOfLengthBetween 1 10)


id : Fuzzer Pointer
id =
    Fuzz.map
        (Api.Id.fromInt >> Pointer.id)
        (Fuzz.intAtLeast 1)
