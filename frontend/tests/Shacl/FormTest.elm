module Shacl.FormTest exposing (suite)

import Documentation
import Expect exposing (Expectation)
import Expect.Shacl.Form.Viewed as Viewed
import Test exposing (Test, describe, test)
import Test.Html.Query as Query
import Test.Html.Query.Extra as Query
import Test.Html.Selector as Selector


suite : Test
suite =
    describe "Shacl.Form.Viewed.Test"
        [ describe "label"
            [ test "via rdfs:label on property" labelViaRdfsLabelOnProperty
            , test "via sh:name on property shape" labelViaShNameOnPropertyShape
            , describe "within single nested shape"
                [ test "via sh:name on nesting property shape" labelWithinSingleNestedShapeViaShNameOnNestingPropertyShape
                , test "via sh:name on property shape" labelWithinSingleNestedShapeViaShNameOnPropertyShape
                , describe "twice nested"
                    [ test "via sh:name on outer nesting property shape" labelWithinTwiceNestedShapeViaShNameOnOuterNestingPropertyShape
                    ]
                ]
            ]
        , describe "combinations"
            [ test "prefixed decimals within nested node shape" prefixedDecimalsWithinNestedNodeShape
            ]
        , Documentation.tests
        ]


labelViaRdfsLabelOnProperty : () -> Expectation
labelViaRdfsLabelOnProperty _ =
    { shape =
        """
            <RootShape> a sh:NodeShape ;
                rdfs:label "text property shapes"@en ;
                hash:documentRoot true ;
                sh:targetClass showcase:Root ;
                sh:property [
                    a sh:PropertyShape ;
                    sh:path showcase:hasValue ;
                    sh:datatype xsd:string ;
                    sh:minCount 1 ;
                    sh:maxCount 1 ;
                ] ;
            .

            showcase:Root a rdfs:Class ; rdfs:label "root"@en .
            showcase:hasValue a rdf:Property ; rdfs:label "label via rdfs:label"@en .
        """
    , data =
        """
            <root> a showcase:Root .
        """
    }
        |> Viewed.expectAll False
            [ Query.findAllInputs
                { inputType = Query.InputText
                , id = "72860c15"
                , label = "Label via rdfs:label"
                , info = Nothing
                , value = ""
                }
                >> Query.count (Expect.equal 1)
            ]


labelViaShNameOnPropertyShape : () -> Expectation
labelViaShNameOnPropertyShape _ =
    { shape =
        """
            <RootShape> a sh:NodeShape ;
                rdfs:label "text property shapes"@en ;
                hash:documentRoot true ;
                sh:targetClass showcase:Root ;
                sh:property [
                    a sh:PropertyShape ;
                    sh:path showcase:hasValue ;
                    sh:name "label via sh:name"@en ;
                    sh:datatype xsd:string ;
                    sh:minCount 1 ;
                    sh:maxCount 1 ;
                ] ;
            .

            showcase:Root a rdfs:Class ; rdfs:label "root"@en .
            showcase:hasValue a rdf:Property ; rdfs:label "label via rdfs:label"@en .
        """
    , data =
        """
            <root> a showcase:Root .
        """
    }
        |> Viewed.expectAll False
            [ Query.findAllInputs
                { inputType = Query.InputText
                , id = "72860c15"
                , label = "Label via sh:name"
                , info = Nothing
                , value = ""
                }
                >> Query.count (Expect.equal 1)
            ]


labelWithinSingleNestedShapeViaShNameOnNestingPropertyShape : () -> Expectation
labelWithinSingleNestedShapeViaShNameOnNestingPropertyShape _ =
    { shape =
        """
            <RootShape> a sh:NodeShape ;
                rdfs:label "text property shapes"@en ;
                hash:documentRoot true ;
                sh:targetClass showcase:Root ;
                sh:property [
                    sh:path showcase:hasNested ;
                    sh:name "label via sh:name on nesting property shape"@en ;
                    sh:class showcase:Nested ;
                    sh:node [
                        sh:property [
                            sh:path showcase:hasValue ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    ] ;
                    sh:minCount 1 ;
                    sh:maxCount 1 ;
                ] ;
            .

            showcase:Root a rdfs:Class ; rdfs:label "root"@en .
            showcase:hasValue a rdf:Property ; rdfs:label "label via rdfs:label"@en .
        """
    , data =
        """
            <root> a showcase:Root ;
                showcase:hasNested [
                    rdfs:isDefinedBy <#4aa8e174> ;
                    a showcase:Nested ;
                    showcase:hasValue "value" ;
                ] .
        """
    }
        |> Viewed.expectAll False
            [ Query.findAllInputs
                { inputType = Query.InputText
                , id = "4aa8e174-72860c15"
                , label = "Label via sh:name on nesting property shape"
                , info = Nothing
                , value = "value"
                }
                >> Query.count (Expect.equal 1)
            ]


labelWithinSingleNestedShapeViaShNameOnPropertyShape : () -> Expectation
labelWithinSingleNestedShapeViaShNameOnPropertyShape _ =
    { shape =
        """
            <RootShape> a sh:NodeShape ;
                rdfs:label "text property shapes"@en ;
                hash:documentRoot true ;
                sh:targetClass showcase:Root ;
                sh:property [
                    sh:path showcase:hasNested ;
                    sh:name "label via sh:name on nesting property shape"@en ;
                    sh:class showcase:Nested ;
                    sh:node [
                        sh:property [
                            sh:path showcase:hasValue ;
                            sh:name "label via sh:name"@en ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    ] ;
                    sh:minCount 1 ;
                    sh:maxCount 1 ;
                ] ;
            .

            showcase:Root a rdfs:Class ; rdfs:label "root"@en .
            showcase:hasValue a rdf:Property ; rdfs:label "label via rdfs:label"@en .
        """
    , data =
        """
            <root> a showcase:Root ;
                showcase:hasNested [
                    rdfs:isDefinedBy <#4aa8e174> ;
                    a showcase:Nested ;
                ] .
        """
    }
        |> Viewed.expectAll False
            [ Query.findAllInputs
                { inputType = Query.InputText
                , id = "4aa8e174-72860c15"
                , label = "Label via sh:name on nesting property shape"
                , info = Nothing
                , value = ""
                }
                >> Query.count (Expect.equal 1)
            ]


labelWithinTwiceNestedShapeViaShNameOnOuterNestingPropertyShape : () -> Expectation
labelWithinTwiceNestedShapeViaShNameOnOuterNestingPropertyShape _ =
    { shape =
        """
            <RootShape> a sh:NodeShape ;
                rdfs:label "text property shapes"@en ;
                hash:documentRoot true ;
                sh:targetClass showcase:Root ;
                sh:property [
                    sh:path showcase:hasNestedOuter ;
                    sh:name "label via sh:name on outer nesting property shape"@en ;
                    sh:class showcase:NestedOuter ;
                    sh:node [
                        sh:property [
                            sh:path showcase:hasNestedInner ;
                            sh:name "label via sh:name on nesting property shape"@en ;
                            sh:class showcase:NestedInner ;
                            sh:node [
                                sh:property [
                                    sh:path showcase:hasValue ;
                                    sh:datatype xsd:string ;
                                    sh:minCount 1 ;
                                    sh:maxCount 1 ;
                                ] ;
                            ] ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    ] ;
                    sh:minCount 1 ;
                    sh:maxCount 1 ;
                ] ;
            .

            showcase:Root a rdfs:Class ; rdfs:label "root"@en .
            showcase:hasValue a rdf:Property ; rdfs:label "label via rdfs:label"@en .
        """
    , data =
        """
            <root> a showcase:Root ;
                showcase:hasNestedOuter [
                    a showcase:NestedOuter ;
                    rdfs:isDefinedBy <#7624ee1> ;
                    showcase:hasNestedInner [
                        a showcase:NestedInner ;
                        rdfs:isDefinedBy <#7624ee1-42f3a924> ;
                    ] ;
                ] ;
            .
        """
    }
        |> Viewed.expectAll False
            [ Query.findAllInputs
                { inputType = Query.InputText
                , id = "7624ee1-42f3a924-72860c15"
                , label = "Label via sh:name on outer nesting property shape"
                , info = Nothing
                , value = ""
                }
                >> Query.count (Expect.equal 1)
            ]


prefixedDecimalsWithinNestedNodeShape : () -> Expectation
prefixedDecimalsWithinNestedNodeShape _ =
    { shape =
        """
            <RootShape> a sh:NodeShape ;
              hash:documentRoot true ;
              sh:targetClass showcase:Root ;
              sh:property <PropertyShape> ;
            .

            <PropertyShape> a sh:PropertyShape ;
                sh:path showcase:hasNestedNode ;
                sh:name "nested node"@en ;
                dash:editor dash:DetailsEditor ;
                sh:node <NodeShapeNested> ;
                sh:minCount 1 ;
                sh:maxCount 1 ;
            .

            <NodeShapeNested> a sh:NodeShape ;
                sh:class showcase:Leaf ;
                sh:property <PropertyShapeNestedA> ;
                sh:property <PropertyShapeNestedB> ;
            .

            <PropertyShapeNestedA> a sh:PropertyShape ;
                sh:path showcase:hasDecimalPrefixedA ;
                sh:name "prefixed decimal A"@en ;
                sh:order 0 ;
                sh:node [
                    sh:property [
                        sh:path showcase:hasDecimal ;
                        sh:datatype xsd:decimal ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    ] ;
                    sh:property [
                        sh:path showcase:hasPrefix ;
                        sh:hasValue showcase:prefix ;
                        hash:include hash:Prefix ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    ] ;
                ] ;
                sh:minCount 1 ;
                sh:maxCount 1 ;
            .

            <PropertyShapeNestedB> a sh:PropertyShape ;
                sh:path showcase:hasDecimalPrefixedB ;
                sh:name "prefixed decimal B"@en ;
                sh:order 1 ;
                sh:node [
                    sh:property [
                        sh:path showcase:hasDecimal ;
                        sh:datatype xsd:decimal ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    ] ;
                    sh:property [
                        sh:path showcase:hasPrefix ;
                        sh:hasValue showcase:prefix ;
                        hash:include hash:Prefix ;
                        sh:minCount 1 ;
                        sh:maxCount 1 ;
                    ] ;
                ] ;
                sh:minCount 1 ;
                sh:maxCount 1 ;
            .

            showcase:Root a rdfs:Class ; rdfs:label "root"@en .
            showcase:prefix a showcase:Prefix ; rdfs:label "prefix"@en .
        """
    , data =
        """
            <root> a showcase:Root ;
                showcase:hasNestedNode [
                    a showcase:Leaf ;
                    rdfs:isDefinedBy <#87a71ca4> ;
                    showcase:hasDecimalPrefixedA [
                        rdfs:isDefinedBy <#87a71ca4-773f7690> ;
                        showcase:hasDecimal 3.14 ;
                        showcase:hasPrefix showcase:prefix ;
                    ] ;
                    showcase:hasDecimalPrefixedB [
                        rdfs:isDefinedBy <#87a71ca4-beefa545> ;
                        showcase:hasDecimal 3.14 ;
                        showcase:hasPrefix showcase:prefix ;
                    ] ;
                ] ;
            .
        """
    }
        |> Viewed.expectAll False
            [ Query.findAllInputs
                { inputType = Query.InputText
                , id = "87a71ca4-773f7690-a50414b3"
                , label = "Prefixed decimal A"
                , info = Nothing
                , value = "3.14"
                }
                >> Query.count (Expect.equal 1)
            , Query.findAllInputs
                { inputType = Query.InputText
                , id = "87a71ca4-beefa545-a50414b3"
                , label = "Prefixed decimal B"
                , info = Nothing
                , value = "3.14"
                }
                >> Query.count (Expect.equal 1)
            , Query.findAll
                [ Selector.exactText "Prefix" ]
                >> Query.count (Expect.equal 2)
            ]
