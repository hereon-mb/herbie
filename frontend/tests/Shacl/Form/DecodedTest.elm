module Shacl.Form.DecodedTest exposing (suite)

import Expect exposing (Expectation)
import Expect.Shacl.Form.Viewed as Viewed exposing (document, showcase)
import Rdf.Decode as Decode exposing (decode)
import Rdf.Decode.Extra as Decode
import Rdf.Graph exposing (Graph)
import Rdf.PropertyPath exposing (PropertyPath(..))
import Rdf.SetIri as SetIri
import Test exposing (Test, describe, test)


suite : Test
suite =
    describe "Shacl.Form.Decoded"
        [ test "one unqualified property shape" oneUnqualifiedPropertyShape
        , describe "two qualified property shapes"
            [ describe "single nested"
                [ test "all present" twoQualifiedPropertyShapes_allPresent
                , test "all missing" twoQualifiedPropertyShapes_allMissing
                , test "one present, one missing" twoQualifiedPropertyShapes_onePresent_oneMissing
                ]
            , describe "single nested + constant"
                [ test "all present" twoQualifiedPropertyShapes_singleNested_and_Constant_allPresent
                , test "all missing" twoQualifiedPropertyShapes_singleNested_and_Constant_allMissing
                ]
            ]
        , describe "strictness"
            [ test "one qualified shape, partially filled" strictnessOneQualifiedShapePartiallyFilled
            , test "two overlapping qualified shapes" strictnessTwoOverlappingQualifiedShapes
            , test "two overlapping qualified shapes, partially filled" strictnessTwoOverlappingQualifiedShapesPartiallyFilled
            ]
        ]


oneUnqualifiedPropertyShape : () -> Expectation
oneUnqualifiedPropertyShape _ =
    { shacl =
        """
            <RootShape> a sh:NodeShape ;
                hash:documentRoot true ;
                sh:targetClass showcase:Root ;
                sh:property [
                    sh:path showcase:hasValue ;
                    sh:datatype xsd:string ;
                    sh:minCount 1 ;
                    sh:maxCount 1 ;
                ] ;
            .

            showcase:Root a rdfs:Class ; rdfs:label "root"@en .
        """
    , data =
        """
            <root> a showcase:Root ;
                showcase:hasValue "value" ;
            .
        """
    }
        |> expectAll
            [ decode
                (Decode.from (document "root")
                    (Decode.isInstanceOf (SetIri.singleton (showcase "Root"))
                        |> Decode.andThen
                            (\_ ->
                                Decode.predicate (showcase "hasValue")
                                    Decode.string
                            )
                    )
                )
                >> Result.map (Expect.equal "value")
            ]


twoQualifiedPropertyShapes_allPresent : () -> Expectation
twoQualifiedPropertyShapes_allPresent _ =
    { shacl =
        """
            <RootShape> a sh:NodeShape ;
                hash:documentRoot true ;
                sh:targetClass showcase:Root ;
                sh:property [
                    sh:path showcase:hasNested ;
                    sh:qualifiedValueShape [
                        sh:class showcase:NestedA ;
                        sh:property [
                            sh:path showcase:hasValue ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    ] ;
                    sh:qualifiedMinCount 1 ;
                    sh:qualifiedMaxCount 1 ;
                ] ;
                sh:property [
                    sh:path showcase:hasNested ;
                    sh:qualifiedValueShape [
                        sh:class showcase:NestedB ;
                        sh:property [
                            sh:path showcase:hasValue ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    ] ;
                    sh:qualifiedMinCount 1 ;
                    sh:qualifiedMaxCount 1 ;
                ] ;
            .

            showcase:Root a rdfs:Class ; rdfs:label "root"@en .
        """
    , data =
        """
            <root> a showcase:Root ;
                showcase:hasNested [
                    a showcase:NestedA ;
                    rdfs:isDefinedBy <#2222745c> ;
                    showcase:hasValue "valueA" ;
                ] ;
                showcase:hasNested [
                    a showcase:NestedB ;
                    rdfs:isDefinedBy <#5641dc1d> ;
                    showcase:hasValue "valueB" ;
                ] ;
            .
        """
    }
        |> expectAll
            [ decode
                (Decode.from (document "root")
                    (Decode.isInstanceOf (SetIri.singleton (showcase "Root"))
                        |> Decode.andThen
                            (\_ ->
                                Decode.succeed (\_ _ -> ())
                                    |> Decode.custom
                                        (Decode.oneAt (PredicatePath (showcase "hasNested"))
                                            (Decode.isInstanceOf (SetIri.singleton (showcase "NestedA")))
                                        )
                                    |> Decode.custom
                                        (Decode.oneAt (PredicatePath (showcase "hasNested"))
                                            (Decode.isInstanceOf (SetIri.singleton (showcase "NestedB")))
                                        )
                            )
                    )
                )
                >> Result.map (Expect.equal ())
            ]


twoQualifiedPropertyShapes_allMissing : () -> Expectation
twoQualifiedPropertyShapes_allMissing _ =
    { shacl =
        """
            <RootShape> a sh:NodeShape ;
                hash:documentRoot true ;
                sh:targetClass showcase:Root ;
                sh:property [
                    sh:path showcase:hasNested ;
                    sh:qualifiedValueShape [
                        sh:class showcase:NestedA ;
                        sh:property [
                            sh:path showcase:hasValue ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    ] ;
                    sh:qualifiedMinCount 1 ;
                    sh:qualifiedMaxCount 1 ;
                ] ;
                sh:property [
                    sh:path showcase:hasNested ;
                    sh:qualifiedValueShape [
                        sh:class showcase:NestedB ;
                        sh:property [
                            sh:path showcase:hasValue ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    ] ;
                    sh:qualifiedMinCount 1 ;
                    sh:qualifiedMaxCount 1 ;
                ] ;
            .

            showcase:Root a rdfs:Class ; rdfs:label "root"@en .
        """
    , data =
        """
            <root> a showcase:Root .
        """
    }
        |> expectAll
            [ decode
                (Decode.from (document "root")
                    (Decode.isInstanceOf (SetIri.singleton (showcase "Root")))
                )
                >> Result.map (Expect.equal ())
            ]


twoQualifiedPropertyShapes_onePresent_oneMissing : () -> Expectation
twoQualifiedPropertyShapes_onePresent_oneMissing _ =
    { shacl =
        """
            <RootShape> a sh:NodeShape ;
                hash:documentRoot true ;
                sh:targetClass showcase:Root ;
                sh:property [
                    sh:path showcase:hasNested ;
                    sh:qualifiedValueShape [
                        sh:class showcase:NestedA ;
                        sh:property [
                            sh:path showcase:hasValue ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    ] ;
                    sh:qualifiedMinCount 1 ;
                    sh:qualifiedMaxCount 1 ;
                ] ;
                sh:property [
                    sh:path showcase:hasNested ;
                    sh:qualifiedValueShape [
                        sh:class showcase:NestedB ;
                        sh:property [
                            sh:path showcase:hasValue ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    ] ;
                    sh:qualifiedMinCount 1 ;
                    sh:qualifiedMaxCount 1 ;
                ] ;
            .

            showcase:Root a rdfs:Class ; rdfs:label "root"@en .
        """
    , data =
        """
            <root> a showcase:Root ;
                showcase:hasNested [
                    a showcase:NestedA ;
                    rdfs:isDefinedBy <#2222745c> ;
                    showcase:hasValue "valueA" ;
                ] ;
            .
        """
    }
        |> expectAll
            [ decode
                (Decode.from (document "root")
                    (Decode.isInstanceOf (SetIri.singleton (showcase "Root"))
                        |> Decode.andThen
                            (\_ ->
                                Decode.succeed (\_ -> ())
                                    |> Decode.custom
                                        (Decode.oneAt (PredicatePath (showcase "hasNested"))
                                            (Decode.isInstanceOf (SetIri.singleton (showcase "NestedA")))
                                        )
                            )
                    )
                )
                >> Result.map (Expect.equal ())
            ]


twoQualifiedPropertyShapes_singleNested_and_Constant_allPresent : () -> Expectation
twoQualifiedPropertyShapes_singleNested_and_Constant_allPresent _ =
    { shacl =
        """
            <RootShape> a sh:NodeShape ;
                hash:documentRoot true ;
                sh:targetClass showcase:Root ;
                sh:property [
                    sh:path showcase:hasNested ;
                    sh:qualifiedValueShape [
                        sh:class showcase:NestedA ;
                        sh:property [
                            sh:path showcase:hasValue ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    ] ;
                    sh:qualifiedMinCount 1 ;
                    sh:qualifiedMaxCount 1 ;
                ] ;
                sh:property [
                    sh:path showcase:hasNested ;
                    sh:qualifiedValueShape [
                        sh:hasValue showcase:Constant ;
                    ] ;
                    sh:qualifiedMinCount 1 ;
                    sh:qualifiedMaxCount 1 ;
                ] ;
            .

            showcase:Root a rdfs:Class ; rdfs:label "root"@en .
        """
    , data =
        """
            <root> a showcase:Root ;
                showcase:hasNested [
                    a showcase:NestedA ;
                    rdfs:isDefinedBy <#2222745c> ;
                    showcase:hasValue "value" ;
                ] ;
                showcase:hasNested showcase:Constant ;
            .
        """
    }
        |> expectAll
            [ decode
                (Decode.from (document "root")
                    (Decode.isInstanceOf (SetIri.singleton (showcase "Root"))
                        |> Decode.andThen
                            (\_ ->
                                Decode.succeed (\_ _ -> ())
                                    |> Decode.custom
                                        (Decode.oneAt (PredicatePath (showcase "hasNested"))
                                            (Decode.isInstanceOf (SetIri.singleton (showcase "NestedA")))
                                        )
                                    |> Decode.custom
                                        (Decode.oneAt (PredicatePath (showcase "hasNested"))
                                            (Decode.exact (showcase "Constant"))
                                        )
                            )
                    )
                )
                >> Result.map (Expect.equal ())
            ]


twoQualifiedPropertyShapes_singleNested_and_Constant_allMissing : () -> Expectation
twoQualifiedPropertyShapes_singleNested_and_Constant_allMissing _ =
    { shacl =
        """
            <RootShape> a sh:NodeShape ;
                hash:documentRoot true ;
                sh:targetClass showcase:Root ;
                sh:property [
                    sh:path showcase:hasNested ;
                    sh:qualifiedValueShape [
                        sh:class showcase:NestedA ;
                        sh:property [
                            sh:path showcase:hasValue ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    ] ;
                    sh:qualifiedMinCount 1 ;
                    sh:qualifiedMaxCount 1 ;
                ] ;
                sh:property [
                    sh:path showcase:hasNested ;
                    sh:qualifiedValueShape [
                        sh:hasValue showcase:Constant ;
                    ] ;
                    sh:qualifiedMinCount 1 ;
                    sh:qualifiedMaxCount 1 ;
                ] ;
            .

            showcase:Root a rdfs:Class ; rdfs:label "root"@en .
        """
    , data =
        """
            <root> a showcase:Root .
        """
    }
        |> expectAll
            [ decode
                (Decode.from (document "root")
                    (Decode.isInstanceOf (SetIri.singleton (showcase "Root"))
                        |> Decode.andThen
                            (\_ ->
                                Decode.succeed (\_ -> ())
                                    |> Decode.custom
                                        (Decode.oneAt (PredicatePath (showcase "hasNested"))
                                            (Decode.exact (showcase "Constant"))
                                        )
                            )
                    )
                )
                >> Result.map (Expect.equal ())
            ]


strictnessOneQualifiedShapePartiallyFilled : () -> Expectation
strictnessOneQualifiedShapePartiallyFilled _ =
    { shacl =
        """
            <RootShape> a sh:NodeShape ;
                hash:documentRoot true ;
                sh:targetClass showcase:Root ;
                sh:property [
                    sh:path showcase:hasNested ;
                    sh:qualifiedValueShape [
                        sh:property [
                            sh:path showcase:hasValueA ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                        sh:property [
                            sh:path showcase:hasValueB ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    ] ;
                    sh:qualifiedMinCount 1 ;
                    sh:qualifiedMaxCount 1 ;
                ] ;
            .

            showcase:Root a rdfs:Class ; rdfs:label "root"@en .
        """
    , data =
        """
            <root> a showcase:Root ;
                showcase:hasNested [
                    rdfs:isDefinedBy <#c14d35b7> ;
                    showcase:hasValueA "value" ;
                ] ;
            .
        """
    }
        |> expectAll
            [ decode
                (Decode.from (document "root")
                    (Decode.isInstanceOf (SetIri.singleton (showcase "Root"))
                        |> Decode.andThen
                            (\_ ->
                                Decode.predicate (showcase "hasNested")
                                    (Decode.predicate (showcase "hasValueA")
                                        Decode.string
                                    )
                            )
                    )
                )
                >> Result.map (Expect.equal "value")
            ]


strictnessTwoOverlappingQualifiedShapes : () -> Expectation
strictnessTwoOverlappingQualifiedShapes _ =
    { shacl =
        """
            <RootShape> a sh:NodeShape ;
                hash:documentRoot true ;
                sh:targetClass showcase:Root ;
                sh:property [
                    sh:path showcase:hasNested ;
                    sh:qualifiedValueShape [
                        sh:property [
                            sh:path showcase:hasValueA ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                        sh:property [
                            sh:path showcase:hasValueB ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    ] ;
                    sh:qualifiedMinCount 1 ;
                    sh:qualifiedMaxCount 1 ;
                ] ;
                sh:property [
                    sh:path showcase:hasNested ;
                    sh:qualifiedValueShape [
                        sh:property [
                            sh:path showcase:hasValueA ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                        sh:property [
                            sh:path showcase:hasValueC ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    ] ;
                    sh:qualifiedMinCount 1 ;
                    sh:qualifiedMaxCount 1 ;
                ] ;
            .

            showcase:Root a rdfs:Class ; rdfs:label "root"@en .
        """
    , data =
        """
            <root> a showcase:Root ;
                rdfs:isDefinedBy <> ;
                showcase:hasNested [
                    rdfs:isDefinedBy <#c14d35b7> ;
                    showcase:hasValueA "value1" ;
                    showcase:hasValueB "value2" ;
                ] ;
                showcase:hasNested [
                    rdfs:isDefinedBy <#6c047c1d> ;
                    showcase:hasValueA "value3" ;
                    showcase:hasValueC "value4" ;
                ] ;
            .
        """
    }
        |> expectAll
            [ decode
                (Decode.from (document "root")
                    (Decode.isInstanceOf (SetIri.singleton (showcase "Root"))
                        |> Decode.andThen
                            (\_ ->
                                Decode.succeed (\_ _ -> ())
                                    |> Decode.custom
                                        (Decode.oneAt (PredicatePath (showcase "hasNested"))
                                            (Decode.succeed (\_ _ -> ())
                                                |> Decode.required (showcase "hasValueA") Decode.string
                                                |> Decode.required (showcase "hasValueB") Decode.string
                                            )
                                        )
                                    |> Decode.custom
                                        (Decode.oneAt (PredicatePath (showcase "hasNested"))
                                            (Decode.succeed (\_ _ -> ())
                                                |> Decode.required (showcase "hasValueA") Decode.string
                                                |> Decode.required (showcase "hasValueC") Decode.string
                                            )
                                        )
                            )
                    )
                )
                >> Result.map (Expect.equal ())
            ]


strictnessTwoOverlappingQualifiedShapesPartiallyFilled : () -> Expectation
strictnessTwoOverlappingQualifiedShapesPartiallyFilled _ =
    { shacl =
        """
            <RootShape> a sh:NodeShape ;
                hash:documentRoot true ;
                sh:targetClass showcase:Root ;
                sh:property [
                    sh:path showcase:hasNested ;
                    sh:qualifiedValueShape [
                        sh:property [
                            sh:path showcase:hasValueA ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                        sh:property [
                            sh:path showcase:hasValueB ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    ] ;
                    sh:qualifiedMinCount 1 ;
                    sh:qualifiedMaxCount 1 ;
                ] ;
                sh:property [
                    sh:path showcase:hasNested ;
                    sh:qualifiedValueShape [
                        sh:property [
                            sh:path showcase:hasValueA ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                        sh:property [
                            sh:path showcase:hasValueC ;
                            sh:datatype xsd:string ;
                            sh:minCount 1 ;
                            sh:maxCount 1 ;
                        ] ;
                    ] ;
                    sh:qualifiedMinCount 1 ;
                    sh:qualifiedMaxCount 1 ;
                ] ;
            .

            showcase:Root a rdfs:Class ; rdfs:label "root"@en .
        """
    , data =
        """
            <root> a showcase:Root ;
                showcase:hasNested [
                    rdfs:isDefinedBy <#6c047c1d> ;
                    showcase:hasValueA "value" ;
                ] ;
            .
        """
    }
        |> expectAll
            [ decode
                (Decode.from (document "root")
                    (Decode.isInstanceOf (SetIri.singleton (showcase "Root"))
                        |> Decode.andThen
                            (\_ ->
                                Decode.succeed (\_ -> ())
                                    |> Decode.custom
                                        (Decode.oneAt (PredicatePath (showcase "hasNested"))
                                            (Decode.succeed (\_ _ -> ())
                                                |> Decode.required (showcase "hasValueA") Decode.string
                                            )
                                        )
                            )
                    )
                )
                >> Result.map (Expect.equal ())
            ]


type alias Setup =
    { shacl : String
    , data : String
    }


expectAll : List (Graph -> Result Decode.Error Expectation) -> Setup -> Expectation
expectAll expectations setup =
    Viewed.expectGraphAllWithDefaultPopulation expectations
        { shape = setup.shacl
        , data = setup.data
        }
