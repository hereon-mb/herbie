module Shacl.Form.MergedTest exposing (suite)

import Expect exposing (Expectation)
import List.NonEmpty as NonEmpty
import Ontology
import Rdf.Graph as Rdf
import Shacl
import Shacl.Extra as Form
import Shacl.Form.Collected as Collected
import Shacl.Form.Merged as Merged
import Test exposing (Test, describe, test)


suite : Test
suite =
    describe "Shacl.Form.Merged"
        [ test "merge nodes" mergeNodes
        , test "merge properties" mergeProperties
        , test "merge nodes and properties" mergeNodesAndProperties
        , test "ors" ors
        , test "node in node" nodeInNode
        ]


mergeNodes : () -> Expectation
mergeNodes _ =
    toMerged
        """
            @base <http://example.com/> .
            @prefix hash: <http://purls.helmholtz-metadaten.de/herbie/hash/#> .
            @prefix sh: <http://www.w3.org/ns/shacl#> .

            <RootShape> a sh:NodeShape ;
                hash:documentRoot true ;
                sh:targetClass <Root> ;
                sh:property [
                    sh:path <nested> ;
                    sh:node [
                        sh:property [
                            sh:path <unit> ;
                            sh:hasValue <num> ;
                        ] ;
                    ] ;
                    sh:node [
                        sh:property [
                            sh:path <value> ;
                            sh:datatype <integer> ;
                        ] ;
                    ] ;
                ] ;
            .

           <root> a <Root> .
       """
        |> expectEqual
            (toMerged
                """
                    @base <http://example.com/> .
                    @prefix hash: <http://purls.helmholtz-metadaten.de/herbie/hash/#> .
                    @prefix sh: <http://www.w3.org/ns/shacl#> .

                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass <Root> ;
                        sh:property [
                            sh:path <nested> ;
                            sh:node [
                                sh:property [
                                    sh:path <unit> ;
                                    sh:hasValue <num> ;
                                ] ;
                                sh:property [
                                    sh:path <value> ;
                                    sh:datatype <integer> ;
                                ] ;
                            ] ;
                        ] ;
                    .

                    <root> a <Root> .
               """
            )


mergeProperties : () -> Expectation
mergeProperties _ =
    toMerged
        """
            @base <http://example.com/> .
            @prefix hash: <http://purls.helmholtz-metadaten.de/herbie/hash/#> .
            @prefix sh: <http://www.w3.org/ns/shacl#> .

            <RootShape> a sh:NodeShape ;
                hash:documentRoot true ;
                sh:targetClass <Root> ;
                sh:property [
                    sh:path <nested> ;
                    sh:node [
                        sh:property [
                            sh:path <unit> ;
                            sh:hasValue <num> ;
                        ] ;
                    ] ;
                    sh:node [
                        sh:property [
                            sh:path <unit> ;
                            sh:datatype <integer> ;
                        ] ;
                    ] ;
                ] ;
            .

           <root> a <Root> .
       """
        |> expectEqual
            (toMerged
                """
                    @base <http://example.com/> .
                    @prefix hash: <http://purls.helmholtz-metadaten.de/herbie/hash/#> .
                    @prefix sh: <http://www.w3.org/ns/shacl#> .

                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass <Root> ;
                        sh:property [
                            sh:path <nested> ;
                            sh:node [
                                sh:property [
                                    sh:path <unit> ;
                                    sh:hasValue <num> ;
                                    sh:datatype <integer> ;
                                ] ;
                            ] ;
                        ] ;
                    .

                    <root> a <Root> .
               """
            )


mergeNodesAndProperties : () -> Expectation
mergeNodesAndProperties _ =
    toMerged
        """
            @base <http://example.com/> .
            @prefix hash: <http://purls.helmholtz-metadaten.de/herbie/hash/#> .
            @prefix sh: <http://www.w3.org/ns/shacl#> .

            <RootShape> a sh:NodeShape ;
                hash:documentRoot true ;
                sh:targetClass <Root> ;
                sh:property [
                    sh:path <nested> ;
                    sh:node [
                        sh:property [
                            sh:path <leaf> ;
                            sh:node [
                                sh:property [
                                    sh:path <nestedA> ;
                                ] ;
                            ] ;
                        ] ;
                    ] ;
                    sh:node [
                        sh:property [
                            sh:path <leaf> ;
                            sh:node [
                                sh:property [
                                    sh:path <nestedB> ;
                                ] ;
                            ] ;
                        ] ;
                    ] ;
                 ] ;
            .

           <root> a <Root> .
       """
        |> expectEqual
            (toMerged
                """
                    @base <http://example.com/> .
                    @prefix hash: <http://purls.helmholtz-metadaten.de/herbie/hash/#> .
                    @prefix sh: <http://www.w3.org/ns/shacl#> .

                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass <Root> ;
                        sh:property [
                            sh:path <nested> ;
                            sh:node [
                                sh:property [
                                    sh:path <leaf> ;
                                    sh:node [
                                        sh:property [
                                            sh:path <nestedA> ;
                                        ] ;
                                        sh:property [
                                            sh:path <nestedB> ;
                                        ] ;
                                    ] ;
                                ] ;
                            ] ;
                         ] ;
                    .

                   <root> a <Root> .
               """
            )


ors : () -> Expectation
ors _ =
    toMerged
        """
            @base <http://example.com/> .
            @prefix hash: <http://purls.helmholtz-metadaten.de/herbie/hash/#> .
            @prefix sh: <http://www.w3.org/ns/shacl#> .

            <RootShape> a sh:NodeShape ;
                hash:documentRoot true ;
                sh:targetClass <Root> ;
                sh:property [
                    sh:path <nested> ;
                    sh:node [
                        sh:or ( <VariantA> <VariantB> ) ;
                    ] ;
                    sh:node [
                        sh:or ( <VariantAlpha> <VariantBeta> ) ;
                    ] ;
                 ] ;
            .

           <VariantA> a sh:NodeShape .
           <VariantAlpha> a sh:NodeShape .
           <VariantB> a sh:NodeShape .
           <VariantBeta> a sh:NodeShape .

           <root> a <Root> .
       """
        |> expectEqual
            (toMerged
                """
                    @base <http://example.com/> .
                    @prefix hash: <http://purls.helmholtz-metadaten.de/herbie/hash/#> .
                    @prefix sh: <http://www.w3.org/ns/shacl#> .

                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass <Root> ;
                        sh:property [
                            sh:path <nested> ;
                            sh:node [
                                sh:or ( <VariantA> <VariantB> ) ;
                                sh:or ( <VariantAlpha> <VariantBeta> ) ;
                            ] ;
                         ] ;
                    .

                   <VariantA> a sh:NodeShape .
                   <VariantAlpha> a sh:NodeShape .
                   <VariantB> a sh:NodeShape .
                   <VariantBeta> a sh:NodeShape .

                   <root> a <Root> .
               """
            )


nodeInNode : () -> Expectation
nodeInNode _ =
    toMerged
        """
            @base <http://example.com/> .
            @prefix hash: <http://purls.helmholtz-metadaten.de/herbie/hash/#> .
            @prefix sh: <http://www.w3.org/ns/shacl#> .

            <RootShape> a sh:NodeShape ;
                hash:documentRoot true ;
                sh:targetClass <Root> ;
                sh:node [
                    sh:property [ sh:path <propertyA> ] ;
                ] ;
                sh:property [ sh:path <propertyB> ] ;
            .

            <root> a <Root> .
       """
        |> expectEqual
            (toMerged
                """
                    @base <http://example.com/> .
                    @prefix hash: <http://purls.helmholtz-metadaten.de/herbie/hash/#> .
                    @prefix sh: <http://www.w3.org/ns/shacl#> .

                    <RootShape> a sh:NodeShape ;
                        hash:documentRoot true ;
                        sh:targetClass <Root> ;
                        sh:property [ sh:path <propertyA> ] ;
                        sh:property [ sh:path <propertyB> ] ;
                    .

                    <root> a <Root> .
               """
            )


type Error
    = RdfError Rdf.Error
    | ShaclError Shacl.Error
    | FormError Form.Error
    | CollectedError Collected.Error
    | MergedError Merged.Error


errorToString : Error -> String
errorToString error =
    case error of
        RdfError e ->
            "Rdf error: " ++ Rdf.errorToString "" e

        ShaclError shacl ->
            "SHACL error: " ++ Shacl.errorToString shacl

        FormError Form.NoNodeShapes ->
            "Form error: no node shapes"

        FormError (Form.NoNodeShapeWithTargetClass _) ->
            "Form error: no node shape with target class"

        FormError Form.NoSubjectForTargetClass ->
            "Form error: no subject for target class"

        CollectedError e ->
            "collected: " ++ Collected.errorToString e

        MergedError _ ->
            "merged (nodes): never"


toMerged : String -> Result Error ShapeNode
toMerged string =
    Rdf.parse string
        |> Result.mapError RdfError
        |> Result.andThen
            (\graph ->
                case Shacl.fromGraph graph of
                    Ok shapes ->
                        let
                            spec =
                                { data = Rdf.emptyGraph
                                , shapes = shapes
                                , ontology = Ontology.empty
                                , strict = False
                                }
                        in
                        shapes
                            |> Form.nodeShapesWithInstances graph
                            |> Result.mapError FormError
                            |> Result.map (NonEmpty.head >> .nodeShape)
                            |> Result.andThen (Collected.fromShacl spec >> Result.mapError CollectedError)

                    Err error ->
                        Err (ShaclError error)
            )
        |> Result.andThen
            (Merged.fromCollected
                >> Result.mapError MergedError
            )
        |> Result.map forgetShapeNode


type ShapeNode
    = ShapeNode
        { properties : List ShapeProperty
        }


forgetShapeNode : Merged.ShapeNode -> ShapeNode
forgetShapeNode (Merged.ShapeNode { properties }) =
    ShapeNode
        { properties = List.map forgetShapeProperty properties
        }


type ShapeProperty
    = ShapeProperty
        { nodes : List ShapeNode
        }


forgetShapeProperty : Merged.ShapeProperty -> ShapeProperty
forgetShapeProperty (Merged.ShapeProperty { nodes }) =
    ShapeProperty
        { nodes = List.map forgetShapeNode nodes
        }


type ExpectError
    = ErrorExpected Error
    | ErrorFound Error


errorExpectedToString : ExpectError -> String
errorExpectedToString expectError =
    case expectError of
        ErrorExpected e ->
            "the expected data failed to parse: " ++ errorToString e

        ErrorFound e ->
            "the found data failed to parse: " ++ errorToString e


expectEqual : Result Error value -> Result Error value -> Expectation
expectEqual expectedOrError foundOrError =
    case ( expectedOrError, foundOrError ) of
        ( Err e, _ ) ->
            Expect.fail (errorExpectedToString (ErrorExpected e))

        ( _, Err e ) ->
            Expect.fail (errorExpectedToString (ErrorFound e))

        ( Ok expected, Ok found ) ->
            Expect.equal expected found
