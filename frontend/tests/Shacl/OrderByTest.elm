module Shacl.OrderByTest exposing (suite)

import Expect
import List.NonEmpty as NonEmpty
import Locale
import Rdf
import Rdf.Decode as Decode exposing (Decoder)
import Rdf.Graph as Rdf
import Shacl.Namespaces exposing (hash)
import Shacl.OrderBy as OrderBy exposing (OrderBy)
import Test exposing (Test, describe, test)


suite : Test
suite =
    describe "OrderBy"
        [ decode
        , sort
        ]


decode : Test
decode =
    describe "decode"
        [ decodeIri
        , decodeAsc
        , decodeDesc
        , decodeList
        , decodeArb
        ]


decodeIri : Test
decodeIri =
    decodeHelp "iri"
        "example:root hash:orderBy example:prefLabel ."
        (OrderBy.Iri (example "prefLabel"))


decodeAsc : Test
decodeAsc =
    decodeHelp "asc"
        "example:root hash:orderBy [ hash:asc example:prefLabel ] ."
        (OrderBy.Asc (OrderBy.Iri (example "prefLabel")))


decodeDesc : Test
decodeDesc =
    decodeHelp "desc"
        "example:root hash:orderBy [ hash:desc example:prefLabel ] ."
        (OrderBy.Desc (OrderBy.Iri (example "prefLabel")))


decodeList : Test
decodeList =
    decodeHelp "list"
        "example:root hash:orderBy ( example:prefLabel example:altLabel ) ."
        (OrderBy.Batch
            (NonEmpty.fromCons (OrderBy.Iri (example "prefLabel"))
                [ OrderBy.Iri (example "altLabel")
                ]
            )
        )


decodeArb : Test
decodeArb =
    decodeHelp "arb"
        "example:root hash:orderBy ( [ hash:desc ( example:prefLabel example:altLabel ) ] example:label ) ."
        (OrderBy.Batch
            (NonEmpty.fromCons
                (OrderBy.Desc
                    (OrderBy.Batch
                        (NonEmpty.fromCons
                            (OrderBy.Iri (example "prefLabel"))
                            [ OrderBy.Iri (example "altLabel") ]
                        )
                    )
                )
                [ OrderBy.Iri (example "label")
                ]
            )
        )


decodeHelp : String -> String -> OrderBy -> Test
decodeHelp label graph expected =
    test label <|
        \_ ->
            (prefixes ++ graph)
                |> Rdf.parse
                |> Result.map (Decode.decode (Decode.from (example "root") decoder))
                |> Expect.equal (Ok (Ok expected))


sort : Test
sort =
    describe "sort"
        [ sortIri
        , sortAsc
        , sortDesc
        , sortList
        ]


sortIri : Test
sortIri =
    sortHelp "iri" (OrderBy.Iri (example "label")) <|
        [ example "option4"
        , example "option1"
        , example "option2"
        , example "option3"
        ]


sortAsc : Test
sortAsc =
    sortHelp "asc" (OrderBy.Asc (OrderBy.Iri (example "label"))) <|
        [ example "option4"
        , example "option1"
        , example "option2"
        , example "option3"
        ]


sortDesc : Test
sortDesc =
    sortHelp "desc" (OrderBy.Desc (OrderBy.Iri (example "label"))) <|
        [ example "option4"
        , example "option3"
        , example "option2"
        , example "option1"
        ]


sortList : Test
sortList =
    sortHelp "list"
        (OrderBy.Batch
            (NonEmpty.fromCons (OrderBy.Iri (example "label"))
                [ OrderBy.Iri (example "altLabel")
                ]
            )
        )
        [ example "option4"
        , example "option1"
        , example "option3"
        , example "option2"
        ]


sortHelp : String -> OrderBy -> List Rdf.Iri -> Test
sortHelp label orderBy expected =
    test label <|
        \_ ->
            (prefixes
                ++ """
                  example:option1
                    example:label "1" ;
                    example:altLabel "z" .
                  example:option2
                    example:label "2" ;
                    example:altLabel "y" .
                  example:option3
                    example:label "2" ;
                    example:altLabel "x" .
                  """
            )
                |> Rdf.parse
                |> Result.map
                    (\graph ->
                        OrderBy.sort Locale.De graph orderBy <|
                            [ example "option1"
                            , example "option2"
                            , example "option3"
                            , example "option4"
                            ]
                    )
                |> Expect.equal (Ok expected)


decoder : Decoder OrderBy
decoder =
    Decode.predicate (hash "orderBy") OrderBy.decoder


example : String -> Rdf.Iri
example =
    Rdf.iri << (++) "http://example.com/"


prefixes : String
prefixes =
    """
    @prefix hash: <http://purls.helmholtz-metadaten.de/herbie/hash/#> .
    @prefix example: <http://example.com/> .
    """
