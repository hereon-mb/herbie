from django.urls import re_path, path

from . import views

urlpatterns = [
    path('playground/', views.playground),
    re_path(r'.*', views.index),
]
