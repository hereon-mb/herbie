const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ConcatTextPlugin = require('concat-text-webpack-plugin-2023');
const webpack = require('webpack');

module.exports = (_, options) => {
  plugins = [
    new MiniCssExtractPlugin(),
    new ConcatTextPlugin({
      files: "src/**/*.en-US.ftl",
      name: "messages.en-US.ftl",
    }),
    new ConcatTextPlugin({
      files: "src/**/*.de.ftl",
      name: "messages.de.ftl",
    }),
  ]

  if (options.mode !== 'development') {
    plugins.push(
      new webpack.NormalModuleReplacementPlugin(
        /patches\.development\.css/,
        './patches.production.css',
      )
    );
  }

  config = {
    entry: {
      main: './src/index.js',
      sw: './src/sw.js',
      stories: './src/stories.js',
      playground: './src/playground',
    },
    output: {
      filename: '[name].js',
      path: path.resolve(__dirname, 'static', 'frontend')
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader"
          }
        },
        {
          test: /\.js/,
          include: /@fluent[\\/]bundle[\\/]esm/,
          type: "javascript/auto"
        },
        {
          test: /\.js/,
          include: /@fluent[\\/]langneg[\\/]esm/,
          type: "javascript/auto"
        },
        {
          test: /\.js/,
          include: /@fluent[\\/]sequence[\\/]esm/,
          type: "javascript/auto"
        },
        {
          test: /\.css$/,
          use: [MiniCssExtractPlugin.loader, 'css-loader']
        },
        {
          test: /\.s[ac]ss$/i,
          use: [
            MiniCssExtractPlugin.loader,
            'css-loader',
            'sass-loader',
          ],
        },
        {
          test: /\.ftl$/i,
          use: 'raw-loader',
        },
        {
          test: /\.elm$/,
          exclude: [
            /elm-stuff/,
            /node_modules/,
            path.resolve(__dirname, "src/Ui"),
            path.resolve(__dirname, "generated/Ui"),
          ],
          use: {
            loader: 'elm-webpack-loader',
            options: {
              debug: false,
              optimize: options.mode === "production",
            },
          }
        },
        {
          test: /\.elm$/,
          exclude: [/elm-stuff/, /node_modules/],
          include: [
            path.resolve(__dirname, "src/Ui"),
          ],
          use: {
            loader: 'elm-webpack-loader',
            options: {
              debug: false,
              optimize: options.mode === "production",
              files: [
                path.resolve(__dirname, "src/Ui/Atom/ChoiceChipsCustomElement.elm"),
                path.resolve(__dirname, "src/Ui/Atom/SegmentedButtonsSingleSelectCustomElement.elm"),
                path.resolve(__dirname, "src/Ui/Atom/TextInputAffixSelectCustomElement.elm"),
                path.resolve(__dirname, "src/Ui/Molecule/DropdownCustomElement.elm"),
                path.resolve(__dirname, "src/Ui/Molecule/DatePicker.elm"),
                path.resolve(__dirname, "src/Ui/Molecule/ComboboxMultiselect.elm"),
                path.resolve(__dirname, "src/Ui/Molecule/ComboboxSingleselectCustomElement.elm"),
              ],
            },
          }
        }
      ]
    },
    plugins: plugins,
  };

  return config;
};
