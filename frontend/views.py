from django.conf import settings
from django.shortcuts import render


def index(request):
    sandbox = "true" if settings.SANDBOX else "false"
    use_triple_store = "true" if settings.TRIPLE_STORE_PATH else "false"
    use_helmholtz_aai = "true" if settings.HELMHOLTZ_CLIENT_ID and settings.HELMHOLTZ_CLIENT_SECRET else "false"
    hide_user_password_login = "true" if settings.HIDE_USER_PASSWORD_LOGIN else "false"
    use_sparklis = "true" if settings.USE_SPARKLIS else "false"
    allow_uploads = "true" if settings.ALLOW_UPLOADS else "false"
    imprint_url = f'"{settings.IMPRINT_URL}"' if settings.IMPRINT_URL else "null"
    data_protection_url = f'"{settings.DATA_PROTECTION_URL}"' if settings.DATA_PROTECTION_URL else "null"
    accessibility_url = f'"{settings.ACCESSIBILITY_URL}"' if settings.ACCESSIBILITY_URL else "null"

    return render(
        request,
        "frontend/index.html",
        context={
            "sandbox": sandbox,
            "useTripleStore": use_triple_store,
            "useHelmholtzAai": use_helmholtz_aai,
            "hideUserPaswordLogin": hide_user_password_login,
            "useSparklis": use_sparklis,
            "allowUploads": allow_uploads,
            "imprintUrl": imprint_url,
            "dataProtectionUrl": data_protection_url,
            "accessibilityUrl": accessibility_url,
        },
    )


def playground(request):
    return render(request, "frontend/playground.html")


def stories(request):
    return render(request, "frontend/stories.html")
