module Benchmarks.RDF exposing (main)

import Benchmark
    exposing
        ( Benchmark
        , benchmark
        , describe
        )
import Benchmark.Runner
    exposing
        ( BenchmarkProgram
        , program
        )
import Dict
import RDF exposing (Graph(..), Node(..), NodeInternal(..))


main : BenchmarkProgram
main =
    program suite


suite : Benchmark
suite =
    describe "union"
        [ benchmark "union" <|
            \_ -> RDF.union sampleGraphSmall sampleGraphBig
        ]


sampleGraphBig : RDF.Graph
sampleGraphBig =
    Graph
        { byPredicateBySubject =
            Dict.fromList
                [ ( "<http://datashapes.org/dash#editor>"
                  , Dict.fromList
                        [ ( "_:na12ae186eadc4865952f7e3b972eb4c6b1"
                          , [ { object = Node (Iri "http://datashapes.org/dash#InstancesSelectEditor")
                              , predicate = Node (Iri "http://datashapes.org/dash#editor")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b1")
                              }
                            ]
                          )
                        , ( "_:na12ae186eadc4865952f7e3b972eb4c6b5"
                          , [ { object = Node (Iri "http://datashapes.org/dash#DetailsEditor")
                              , predicate = Node (Iri "http://datashapes.org/dash#editor")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
                              }
                            ]
                          )
                        , ( "_:na12ae186eadc4865952f7e3b972eb4c6b6"
                          , [ { object = Node (Iri "http://datashapes.org/dash#DetailsEditor")
                              , predicate = Node (Iri "http://datashapes.org/dash#editor")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
                              }
                            ]
                          )
                        , ( "_:na12ae186eadc4865952f7e3b972eb4c6b7"
                          , [ { object = Node (Iri "http://datashapes.org/dash#DetailsEditor")
                              , predicate = Node (Iri "http://datashapes.org/dash#editor")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
                              }
                            ]
                          )
                        ]
                  )
                , ( "<http://www.w3.org/1999/02/22-rdf-syntax-ns#first>"
                  , Dict.fromList
                        [ ( "_:na12ae186eadc4865952f7e3b972eb4c6b2"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment_wellPlateLayout")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#first")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b2")
                              }
                            ]
                          )
                        , ( "_:na12ae186eadc4865952f7e3b972eb4c6b3"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#WellPlateLayout_well")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#first")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b3")
                              }
                            ]
                          )
                        , ( "_:na12ae186eadc4865952f7e3b972eb4c6b4"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#Well_sampleSet")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#first")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b4")
                              }
                            ]
                          )
                        ]
                  )
                , ( "<http://www.w3.org/1999/02/22-rdf-syntax-ns#rest>"
                  , Dict.fromList
                        [ ( "_:na12ae186eadc4865952f7e3b972eb4c6b2"
                          , [ { object = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b3")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#rest")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b2")
                              }
                            ]
                          )
                        , ( "_:na12ae186eadc4865952f7e3b972eb4c6b3"
                          , [ { object = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b4")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#rest")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b3")
                              }
                            ]
                          )
                        , ( "_:na12ae186eadc4865952f7e3b972eb4c6b4"
                          , [ { object = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#nil")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#rest")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b4")
                              }
                            ]
                          )
                        ]
                  )
                , ( "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>"
                  , Dict.fromList
                        [ ( "<http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment>"
                          , [ { object = Node (Iri "http://www.w3.org/2000/01/rdf-schema#Class")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
                              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment")
                              }
                            ]
                          )
                        , ( "<http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape>"
                          , [ { object = Node (Iri "http://www.w3.org/ns/shacl#NodeShape")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
                              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape")
                              }
                            ]
                          )
                        , ( "_:na12ae186eadc4865952f7e3b972eb4c6b1"
                          , [ { object = Node (Iri "http://www.w3.org/ns/shacl#PropertyShape")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b1")
                              }
                            ]
                          )
                        , ( "_:na12ae186eadc4865952f7e3b972eb4c6b5"
                          , [ { object = Node (Iri "http://www.w3.org/ns/shacl#PropertyShape")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
                              }
                            ]
                          )
                        , ( "_:na12ae186eadc4865952f7e3b972eb4c6b6"
                          , [ { object = Node (Iri "http://www.w3.org/ns/shacl#PropertyShape")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
                              }
                            ]
                          )
                        , ( "_:na12ae186eadc4865952f7e3b972eb4c6b7"
                          , [ { object = Node (Iri "http://www.w3.org/ns/shacl#PropertyShape")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
                              }
                            ]
                          )
                        ]
                  )
                , ( "<http://www.w3.org/2000/01/rdf-schema#label>"
                  , Dict.fromList
                        [ ( "<http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment>"
                          , [ { object =
                                    Node
                                        (Literal
                                            { datatype = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#langString")
                                            , languageTag = Just "en"
                                            , value = "biological experiment"
                                            }
                                        )
                              , predicate = Node (Iri "http://www.w3.org/2000/01/rdf-schema#label")
                              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment")
                              }
                            ]
                          )
                        ]
                  )
                , ( "<http://www.w3.org/ns/shacl#class>"
                  , Dict.fromList
                        [ ( "_:na12ae186eadc4865952f7e3b972eb4c6b1"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/#SampleSet")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#class")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b1")
                              }
                            ]
                          )
                        , ( "_:na12ae186eadc4865952f7e3b972eb4c6b5"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#Condition")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#class")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
                              }
                            ]
                          )
                        , ( "_:na12ae186eadc4865952f7e3b972eb4c6b6"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#Medium")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#class")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
                              }
                            ]
                          )
                        , ( "_:na12ae186eadc4865952f7e3b972eb4c6b7"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#WellPlateLayout")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#class")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
                              }
                            ]
                          )
                        ]
                  )
                , ( "<http://www.w3.org/ns/shacl#minCount>"
                  , Dict.fromList
                        [ ( "_:na12ae186eadc4865952f7e3b972eb4c6b7"
                          , [ { object =
                                    Node
                                        (Literal
                                            { datatype = Node (Iri "http://www.w3.org/2001/XMLSchema#integer")
                                            , languageTag = Nothing
                                            , value = "1"
                                            }
                                        )
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#minCount")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
                              }
                            ]
                          )
                        ]
                  )
                , ( "<http://www.w3.org/ns/shacl#node>"
                  , Dict.fromList
                        [ ( "_:na12ae186eadc4865952f7e3b972eb4c6b5"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#ConditionShape")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#node")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
                              }
                            ]
                          )
                        , ( "_:na12ae186eadc4865952f7e3b972eb4c6b6"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#MediumShape")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#node")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
                              }
                            ]
                          )
                        , ( "_:na12ae186eadc4865952f7e3b972eb4c6b7"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#WellPlateLayoutShape")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#node")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
                              }
                            ]
                          )
                        ]
                  )
                , ( "<http://www.w3.org/ns/shacl#order>"
                  , Dict.fromList
                        [ ( "_:na12ae186eadc4865952f7e3b972eb4c6b1"
                          , [ { object =
                                    Node
                                        (Literal
                                            { datatype = Node (Iri "http://www.w3.org/2001/XMLSchema#integer")
                                            , languageTag = Nothing
                                            , value = "0"
                                            }
                                        )
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#order")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b1")
                              }
                            ]
                          )
                        , ( "_:na12ae186eadc4865952f7e3b972eb4c6b5"
                          , [ { object =
                                    Node
                                        (Literal
                                            { datatype = Node (Iri "http://www.w3.org/2001/XMLSchema#integer")
                                            , languageTag = Nothing
                                            , value = "1"
                                            }
                                        )
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#order")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
                              }
                            ]
                          )
                        , ( "_:na12ae186eadc4865952f7e3b972eb4c6b6"
                          , [ { object =
                                    Node
                                        (Literal
                                            { datatype = Node (Iri "http://www.w3.org/2001/XMLSchema#integer")
                                            , languageTag = Nothing
                                            , value = "2"
                                            }
                                        )
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#order")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
                              }
                            ]
                          )
                        , ( "_:na12ae186eadc4865952f7e3b972eb4c6b7"
                          , [ { object =
                                    Node
                                        (Literal
                                            { datatype = Node (Iri "http://www.w3.org/2001/XMLSchema#integer")
                                            , languageTag = Nothing
                                            , value = "3"
                                            }
                                        )
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#order")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
                              }
                            ]
                          )
                        ]
                  )
                , ( "<http://www.w3.org/ns/shacl#path>"
                  , Dict.fromList
                        [ ( "_:na12ae186eadc4865952f7e3b972eb4c6b1"
                          , [ { object = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b2")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#path")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b1")
                              }
                            ]
                          )
                        , ( "_:na12ae186eadc4865952f7e3b972eb4c6b5"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment_condition")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#path")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
                              }
                            ]
                          )
                        , ( "_:na12ae186eadc4865952f7e3b972eb4c6b6"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment_medium")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#path")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
                              }
                            ]
                          )
                        , ( "_:na12ae186eadc4865952f7e3b972eb4c6b7"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment_wellPlateLayout")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#path")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
                              }
                            ]
                          )
                        ]
                  )
                , ( "<http://www.w3.org/ns/shacl#property>"
                  , Dict.fromList
                        [ ( "<http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape>"
                          , [ { object = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#property")
                              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape")
                              }
                            , { object = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#property")
                              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape")
                              }
                            , { object = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b1")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#property")
                              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape")
                              }
                            , { object = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#property")
                              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape")
                              }
                            ]
                          )
                        ]
                  )
                , ( "<http://www.w3.org/ns/shacl#targetClass>"
                  , Dict.fromList
                        [ ( "<http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape>"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#targetClass")
                              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape")
                              }
                            ]
                          )
                        ]
                  )
                ]
        , bySubjectByPredicate =
            Dict.fromList
                [ ( "<http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment>"
                  , Dict.fromList
                        [ ( "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>"
                          , [ { object = Node (Iri "http://www.w3.org/2000/01/rdf-schema#Class")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
                              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/2000/01/rdf-schema#label>"
                          , [ { object =
                                    Node
                                        (Literal
                                            { datatype = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#langString")
                                            , languageTag = Just "en"
                                            , value = "biological experiment"
                                            }
                                        )
                              , predicate = Node (Iri "http://www.w3.org/2000/01/rdf-schema#label")
                              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment")
                              }
                            ]
                          )
                        ]
                  )
                , ( "<http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape>"
                  , Dict.fromList
                        [ ( "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>"
                          , [ { object = Node (Iri "http://www.w3.org/ns/shacl#NodeShape")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
                              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/ns/shacl#property>"
                          , [ { object = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#property")
                              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape")
                              }
                            , { object = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#property")
                              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape")
                              }
                            , { object = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b1")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#property")
                              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape")
                              }
                            , { object = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#property")
                              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/ns/shacl#targetClass>"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#targetClass")
                              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape")
                              }
                            ]
                          )
                        ]
                  )
                , ( "_:na12ae186eadc4865952f7e3b972eb4c6b1"
                  , Dict.fromList
                        [ ( "<http://datashapes.org/dash#editor>"
                          , [ { object = Node (Iri "http://datashapes.org/dash#InstancesSelectEditor")
                              , predicate = Node (Iri "http://datashapes.org/dash#editor")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b1")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>"
                          , [ { object = Node (Iri "http://www.w3.org/ns/shacl#PropertyShape")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b1")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/ns/shacl#class>"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/#SampleSet")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#class")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b1")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/ns/shacl#order>"
                          , [ { object =
                                    Node
                                        (Literal
                                            { datatype = Node (Iri "http://www.w3.org/2001/XMLSchema#integer")
                                            , languageTag = Nothing
                                            , value = "0"
                                            }
                                        )
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#order")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b1")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/ns/shacl#path>"
                          , [ { object = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b2")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#path")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b1")
                              }
                            ]
                          )
                        ]
                  )
                , ( "_:na12ae186eadc4865952f7e3b972eb4c6b2"
                  , Dict.fromList
                        [ ( "<http://www.w3.org/1999/02/22-rdf-syntax-ns#first>"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment_wellPlateLayout")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#first")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b2")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/1999/02/22-rdf-syntax-ns#rest>"
                          , [ { object = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b3")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#rest")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b2")
                              }
                            ]
                          )
                        ]
                  )
                , ( "_:na12ae186eadc4865952f7e3b972eb4c6b3"
                  , Dict.fromList
                        [ ( "<http://www.w3.org/1999/02/22-rdf-syntax-ns#first>"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#WellPlateLayout_well")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#first")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b3")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/1999/02/22-rdf-syntax-ns#rest>"
                          , [ { object = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b4")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#rest")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b3")
                              }
                            ]
                          )
                        ]
                  )
                , ( "_:na12ae186eadc4865952f7e3b972eb4c6b4"
                  , Dict.fromList
                        [ ( "<http://www.w3.org/1999/02/22-rdf-syntax-ns#first>"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#Well_sampleSet")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#first")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b4")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/1999/02/22-rdf-syntax-ns#rest>"
                          , [ { object = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#nil")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#rest")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b4")
                              }
                            ]
                          )
                        ]
                  )
                , ( "_:na12ae186eadc4865952f7e3b972eb4c6b5"
                  , Dict.fromList
                        [ ( "<http://datashapes.org/dash#editor>"
                          , [ { object = Node (Iri "http://datashapes.org/dash#DetailsEditor")
                              , predicate = Node (Iri "http://datashapes.org/dash#editor")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>"
                          , [ { object = Node (Iri "http://www.w3.org/ns/shacl#PropertyShape")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/ns/shacl#class>"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#Condition")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#class")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/ns/shacl#node>"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#ConditionShape")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#node")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/ns/shacl#order>"
                          , [ { object =
                                    Node
                                        (Literal
                                            { datatype = Node (Iri "http://www.w3.org/2001/XMLSchema#integer")
                                            , languageTag = Nothing
                                            , value = "1"
                                            }
                                        )
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#order")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/ns/shacl#path>"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment_condition")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#path")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
                              }
                            ]
                          )
                        ]
                  )
                , ( "_:na12ae186eadc4865952f7e3b972eb4c6b6"
                  , Dict.fromList
                        [ ( "<http://datashapes.org/dash#editor>"
                          , [ { object = Node (Iri "http://datashapes.org/dash#DetailsEditor")
                              , predicate = Node (Iri "http://datashapes.org/dash#editor")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>"
                          , [ { object = Node (Iri "http://www.w3.org/ns/shacl#PropertyShape")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/ns/shacl#class>"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#Medium")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#class")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/ns/shacl#node>"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#MediumShape")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#node")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/ns/shacl#order>"
                          , [ { object =
                                    Node
                                        (Literal
                                            { datatype = Node (Iri "http://www.w3.org/2001/XMLSchema#integer")
                                            , languageTag = Nothing
                                            , value = "2"
                                            }
                                        )
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#order")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/ns/shacl#path>"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment_medium")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#path")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
                              }
                            ]
                          )
                        ]
                  )
                , ( "_:na12ae186eadc4865952f7e3b972eb4c6b7"
                  , Dict.fromList
                        [ ( "<http://datashapes.org/dash#editor>"
                          , [ { object = Node (Iri "http://datashapes.org/dash#DetailsEditor")
                              , predicate = Node (Iri "http://datashapes.org/dash#editor")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>"
                          , [ { object = Node (Iri "http://www.w3.org/ns/shacl#PropertyShape")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/ns/shacl#class>"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#WellPlateLayout")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#class")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/ns/shacl#minCount>"
                          , [ { object =
                                    Node
                                        (Literal
                                            { datatype = Node (Iri "http://www.w3.org/2001/XMLSchema#integer")
                                            , languageTag = Nothing
                                            , value = "1"
                                            }
                                        )
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#minCount")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/ns/shacl#node>"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#WellPlateLayoutShape")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#node")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/ns/shacl#order>"
                          , [ { object =
                                    Node
                                        (Literal
                                            { datatype = Node (Iri "http://www.w3.org/2001/XMLSchema#integer")
                                            , languageTag = Nothing
                                            , value = "3"
                                            }
                                        )
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#order")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/ns/shacl#path>"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment_wellPlateLayout")
                              , predicate = Node (Iri "http://www.w3.org/ns/shacl#path")
                              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
                              }
                            ]
                          )
                        ]
                  )
                ]
        , nTriples =
            [ { object = Node (Iri "http://www.w3.org/2000/01/rdf-schema#Class")
              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment")
              }
            , { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment_wellPlateLayout")
              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#first")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b2")
              }
            , { object = Node (Iri "http://www.w3.org/ns/shacl#PropertyShape")
              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
              }
            , { object = Node (Iri "http://www.w3.org/ns/shacl#PropertyShape")
              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b1")
              }
            , { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#Well_sampleSet")
              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#first")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b4")
              }
            , { object = Node (Iri "http://datashapes.org/dash#DetailsEditor")
              , predicate = Node (Iri "http://datashapes.org/dash#editor")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
              }
            , { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment_wellPlateLayout")
              , predicate = Node (Iri "http://www.w3.org/ns/shacl#path")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
              }
            , { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment_medium")
              , predicate = Node (Iri "http://www.w3.org/ns/shacl#path")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
              }
            , { object = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
              , predicate = Node (Iri "http://www.w3.org/ns/shacl#property")
              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape")
              }
            , { object = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b3")
              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#rest")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b2")
              }
            , { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment_condition")
              , predicate = Node (Iri "http://www.w3.org/ns/shacl#path")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
              }
            , { object = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
              , predicate = Node (Iri "http://www.w3.org/ns/shacl#property")
              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape")
              }
            , { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#ConditionShape")
              , predicate = Node (Iri "http://www.w3.org/ns/shacl#node")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
              }
            , { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#Medium")
              , predicate = Node (Iri "http://www.w3.org/ns/shacl#class")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
              }
            , { object =
                    Node
                        (Literal
                            { datatype = Node (Iri "http://www.w3.org/2001/XMLSchema#integer")
                            , languageTag = Nothing
                            , value = "3"
                            }
                        )
              , predicate = Node (Iri "http://www.w3.org/ns/shacl#order")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
              }
            , { object = Node (Iri "http://datashapes.org/dash#DetailsEditor")
              , predicate = Node (Iri "http://datashapes.org/dash#editor")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
              }
            , { object =
                    Node
                        (Literal
                            { datatype = Node (Iri "http://www.w3.org/2001/XMLSchema#integer")
                            , languageTag = Nothing
                            , value = "1"
                            }
                        )
              , predicate = Node (Iri "http://www.w3.org/ns/shacl#order")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
              }
            , { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#WellPlateLayout")
              , predicate = Node (Iri "http://www.w3.org/ns/shacl#class")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
              }
            , { object =
                    Node
                        (Literal
                            { datatype = Node (Iri "http://www.w3.org/2001/XMLSchema#integer")
                            , languageTag = Nothing
                            , value = "1"
                            }
                        )
              , predicate = Node (Iri "http://www.w3.org/ns/shacl#minCount")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
              }
            , { object = Node (Iri "http://www.w3.org/ns/shacl#NodeShape")
              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape")
              }
            , { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#MediumShape")
              , predicate = Node (Iri "http://www.w3.org/ns/shacl#node")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
              }
            , { object =
                    Node
                        (Literal
                            { datatype = Node (Iri "http://www.w3.org/2001/XMLSchema#integer")
                            , languageTag = Nothing
                            , value = "2"
                            }
                        )
              , predicate = Node (Iri "http://www.w3.org/ns/shacl#order")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
              }
            , { object =
                    Node
                        (Literal
                            { datatype = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#langString")
                            , languageTag = Just "en"
                            , value = "biological experiment"
                            }
                        )
              , predicate = Node (Iri "http://www.w3.org/2000/01/rdf-schema#label")
              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment")
              }
            , { object = Node (Iri "http://datashapes.org/dash#InstancesSelectEditor")
              , predicate = Node (Iri "http://datashapes.org/dash#editor")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b1")
              }
            , { object =
                    Node
                        (Literal
                            { datatype = Node (Iri "http://www.w3.org/2001/XMLSchema#integer")
                            , languageTag = Nothing
                            , value = "0"
                            }
                        )
              , predicate = Node (Iri "http://www.w3.org/ns/shacl#order")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b1")
              }
            , { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#WellPlateLayout_well")
              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#first")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b3")
              }
            , { object = Node (Iri "http://www.w3.org/ns/shacl#PropertyShape")
              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
              }
            , { object = Node (Iri "http://localhost:8000/api/ontology/#SampleSet")
              , predicate = Node (Iri "http://www.w3.org/ns/shacl#class")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b1")
              }
            , { object = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b1")
              , predicate = Node (Iri "http://www.w3.org/ns/shacl#property")
              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape")
              }
            , { object = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b2")
              , predicate = Node (Iri "http://www.w3.org/ns/shacl#path")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b1")
              }
            , { object = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b4")
              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#rest")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b3")
              }
            , { object = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
              , predicate = Node (Iri "http://www.w3.org/ns/shacl#property")
              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape")
              }
            , { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment")
              , predicate = Node (Iri "http://www.w3.org/ns/shacl#targetClass")
              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape")
              }
            , { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#Condition")
              , predicate = Node (Iri "http://www.w3.org/ns/shacl#class")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
              }
            , { object = Node (Iri "http://www.w3.org/ns/shacl#PropertyShape")
              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
              }
            , { object = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#nil")
              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#rest")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b4")
              }
            , { object = Node (Iri "http://datashapes.org/dash#DetailsEditor")
              , predicate = Node (Iri "http://datashapes.org/dash#editor")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
              }
            , { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#WellPlateLayoutShape")
              , predicate = Node (Iri "http://www.w3.org/ns/shacl#node")
              , subject = Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
              }
            ]
        , objects =
            [ Node (Iri "http://www.w3.org/2000/01/rdf-schema#Class")
            , Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment_wellPlateLayout")
            , Node (Iri "http://www.w3.org/ns/shacl#PropertyShape")
            , Node (Iri "http://www.w3.org/ns/shacl#PropertyShape")
            , Node (Iri "http://localhost:8000/api/ontology/bio-exp/#Well_sampleSet")
            , Node (Iri "http://datashapes.org/dash#DetailsEditor")
            , Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment_wellPlateLayout")
            , Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment_medium")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b3")
            , Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment_condition")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
            , Node (Iri "http://localhost:8000/api/ontology/bio-exp/#ConditionShape")
            , Node (Iri "http://localhost:8000/api/ontology/bio-exp/#Medium")
            , Node
                (Literal
                    { datatype = Node (Iri "http://www.w3.org/2001/XMLSchema#integer")
                    , languageTag = Nothing
                    , value = "3"
                    }
                )
            , Node (Iri "http://datashapes.org/dash#DetailsEditor")
            , Node
                (Literal
                    { datatype = Node (Iri "http://www.w3.org/2001/XMLSchema#integer")
                    , languageTag = Nothing
                    , value = "1"
                    }
                )
            , Node (Iri "http://localhost:8000/api/ontology/bio-exp/#WellPlateLayout")
            , Node
                (Literal
                    { datatype = Node (Iri "http://www.w3.org/2001/XMLSchema#integer")
                    , languageTag = Nothing
                    , value = "1"
                    }
                )
            , Node (Iri "http://www.w3.org/ns/shacl#NodeShape")
            , Node (Iri "http://localhost:8000/api/ontology/bio-exp/#MediumShape")
            , Node
                (Literal
                    { datatype = Node (Iri "http://www.w3.org/2001/XMLSchema#integer")
                    , languageTag = Nothing
                    , value = "2"
                    }
                )
            , Node
                (Literal
                    { datatype = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#langString")
                    , languageTag = Just "en"
                    , value = "biological experiment"
                    }
                )
            , Node (Iri "http://datashapes.org/dash#InstancesSelectEditor")
            , Node
                (Literal
                    { datatype = Node (Iri "http://www.w3.org/2001/XMLSchema#integer")
                    , languageTag = Nothing
                    , value = "0"
                    }
                )
            , Node (Iri "http://localhost:8000/api/ontology/bio-exp/#WellPlateLayout_well")
            , Node (Iri "http://www.w3.org/ns/shacl#PropertyShape")
            , Node (Iri "http://localhost:8000/api/ontology/#SampleSet")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b1")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b2")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b4")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
            , Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment")
            , Node (Iri "http://localhost:8000/api/ontology/bio-exp/#Condition")
            , Node (Iri "http://www.w3.org/ns/shacl#PropertyShape")
            , Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#nil")
            , Node (Iri "http://datashapes.org/dash#DetailsEditor")
            , Node (Iri "http://localhost:8000/api/ontology/bio-exp/#WellPlateLayoutShape")
            ]
        , subjects =
            [ Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b2")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b1")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b4")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
            , Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b2")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
            , Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
            , Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
            , Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b1")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b1")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b3")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b1")
            , Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b1")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b3")
            , Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape")
            , Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperimentShape")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b6")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b4")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b5")
            , Node (BlankNode "na12ae186eadc4865952f7e3b972eb4c6b7")
            ]
        }


sampleGraphSmall : RDF.Graph
sampleGraphSmall =
    Graph
        { byPredicateBySubject =
            Dict.fromList
                [ ( "<http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment_wellPlateLayout>"
                  , Dict.fromList
                        [ ( "_:2273e9c9-fa2d-4c28-ae87-947ad36cbecd"
                          , [ { object = Node (BlankNode "06aa2bfa-af53-42c5-aa42-a0b964471911")
                              , predicate = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment_wellPlateLayout")
                              , subject = Node (BlankNode "2273e9c9-fa2d-4c28-ae87-947ad36cbecd")
                              }
                            ]
                          )
                        ]
                  )
                , ( "<http://localhost:8000/api/ontology/bio-exp/#WellPlateLayout_wellPlate>"
                  , Dict.fromList
                        [ ( "_:06aa2bfa-af53-42c5-aa42-a0b964471911"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/wellPlate24")
                              , predicate = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#WellPlateLayout_wellPlate")
                              , subject = Node (BlankNode "06aa2bfa-af53-42c5-aa42-a0b964471911")
                              }
                            ]
                          )
                        ]
                  )
                , ( "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>"
                  , Dict.fromList
                        [ ( "<http://localhost:8000/api/ontology/bio-exp/wellPlate24>"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/#WellPlate")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
                              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/wellPlate24")
                              }
                            ]
                          )
                        , ( "_:06aa2bfa-af53-42c5-aa42-a0b964471911"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#WellPlateLayout")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
                              , subject = Node (BlankNode "06aa2bfa-af53-42c5-aa42-a0b964471911")
                              }
                            ]
                          )
                        , ( "_:2273e9c9-fa2d-4c28-ae87-947ad36cbecd"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
                              , subject = Node (BlankNode "2273e9c9-fa2d-4c28-ae87-947ad36cbecd")
                              }
                            ]
                          )
                        ]
                  )
                ]
        , bySubjectByPredicate =
            Dict.fromList
                [ ( "<http://localhost:8000/api/ontology/bio-exp/wellPlate24>"
                  , Dict.fromList
                        [ ( "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/#WellPlate")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
                              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/wellPlate24")
                              }
                            ]
                          )
                        ]
                  )
                , ( "_:06aa2bfa-af53-42c5-aa42-a0b964471911"
                  , Dict.fromList
                        [ ( "<http://localhost:8000/api/ontology/bio-exp/#WellPlateLayout_wellPlate>"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/wellPlate24")
                              , predicate = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#WellPlateLayout_wellPlate")
                              , subject = Node (BlankNode "06aa2bfa-af53-42c5-aa42-a0b964471911")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#WellPlateLayout")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
                              , subject = Node (BlankNode "06aa2bfa-af53-42c5-aa42-a0b964471911")
                              }
                            ]
                          )
                        ]
                  )
                , ( "_:2273e9c9-fa2d-4c28-ae87-947ad36cbecd"
                  , Dict.fromList
                        [ ( "<http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment_wellPlateLayout>"
                          , [ { object = Node (BlankNode "06aa2bfa-af53-42c5-aa42-a0b964471911")
                              , predicate = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment_wellPlateLayout")
                              , subject = Node (BlankNode "2273e9c9-fa2d-4c28-ae87-947ad36cbecd")
                              }
                            ]
                          )
                        , ( "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>"
                          , [ { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment")
                              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
                              , subject = Node (BlankNode "2273e9c9-fa2d-4c28-ae87-947ad36cbecd")
                              }
                            ]
                          )
                        ]
                  )
                ]
        , nTriples =
            [ { object = Node (Iri "http://localhost:8000/api/ontology/#WellPlate")
              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
              , subject = Node (Iri "http://localhost:8000/api/ontology/bio-exp/wellPlate24")
              }
            , { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/wellPlate24")
              , predicate = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#WellPlateLayout_wellPlate")
              , subject = Node (BlankNode "06aa2bfa-af53-42c5-aa42-a0b964471911")
              }
            , { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#WellPlateLayout")
              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
              , subject = Node (BlankNode "06aa2bfa-af53-42c5-aa42-a0b964471911")
              }
            , { object = Node (BlankNode "06aa2bfa-af53-42c5-aa42-a0b964471911")
              , predicate = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment_wellPlateLayout")
              , subject = Node (BlankNode "2273e9c9-fa2d-4c28-ae87-947ad36cbecd")
              }
            , { object = Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment")
              , predicate = Node (Iri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
              , subject = Node (BlankNode "2273e9c9-fa2d-4c28-ae87-947ad36cbecd")
              }
            ]
        , objects =
            [ Node (Iri "http://localhost:8000/api/ontology/#WellPlate")
            , Node (Iri "http://localhost:8000/api/ontology/bio-exp/wellPlate24")
            , Node (Iri "http://localhost:8000/api/ontology/bio-exp/#WellPlateLayout")
            , Node (BlankNode "06aa2bfa-af53-42c5-aa42-a0b964471911")
            , Node (Iri "http://localhost:8000/api/ontology/bio-exp/#BiologicalExperiment")
            ]
        , subjects =
            [ Node (Iri "http://localhost:8000/api/ontology/bio-exp/wellPlate24")
            , Node (BlankNode "06aa2bfa-af53-42c5-aa42-a0b964471911")
            , Node (BlankNode "2273e9c9-fa2d-4c28-ae87-947ad36cbecd")
            ]
        }
