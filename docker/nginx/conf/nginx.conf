worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

events {
    worker_connections 1024;
}

http {
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';


    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 2048;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    server {
        listen       8080 default_server;
        server_name  _;
        client_max_body_size 50M;

        location /favicon.ico {
          access_log off;
          log_not_found off;
        }

        location /static/ {
            gzip on;
            gzip_types text/css application/javascript font/woff2;
            gzip_proxied no-cache no-store private expired auth;

            root /opt/app-root/src;
        }

        location /protected-media/ {
            internal;
            rewrite ^/protected-media/(.*) /media/$1 break;
            root /opt/app-root;
        }

        location / {
            proxy_pass http://django:8080;

            gzip on;
            gzip_types application/json;
            gzip_proxied no-cache no-store private expired auth;

            proxy_redirect off;
            proxy_set_header Host $http_host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Host $host;
            proxy_set_header X-Forwarded-Proto https;
        }

    }

}
