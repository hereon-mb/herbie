# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from .shared import *

TEST = True


IRI_PREFIX = "http://testserver/"


# DATABASE

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "HOST": environ.get("DJANGO_DATABASE_HOST", "localhost"),
        "PORT": environ.get("DJANGO_DATABASE_PORT", "5432"),
        "NAME": "herbie_test",
        "USER": environ.get("DJANGO_DATABASE_USER", "admin"),
        "PASSWORD": environ.get("DJANGO_DATABASE_PASSWORD", "secret"),
    },
}
