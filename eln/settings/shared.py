# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

import json
from os import environ
from pathlib import Path
from typing import Any, Dict, List

from django.contrib import messages

BASE_DIR = Path(__file__).resolve().parent.parent.parent

SANDBOX = environ.get("DJANGO_SANDBOX", "False") == "True"
EXPORT_AFFILIATION = environ.get("DJANGO_EXPORT_AFFILIATION", "Organization")

IRI_PREFIX = environ.get("DJANGO_IRI_PREFIX", "http://example.org/")

IMPRINT_URL = environ.get("DJANGO_IMPRINT_URL")
DATA_PROTECTION_URL = environ.get("DJANGO_DATA_PROTECTION_URL")
ACCESSIBILITY_URL = environ.get("DJANGO_ACCESSIBILITY_URL")

USE_SPARKLIS = environ.get("DJANGO_USE_SPARKLIS", "False") == "True"
ALLOW_UPLOADS = environ.get("DJANGO_ALLOW_UPLOADS", "False") == "True"


# TRIPLE STORE

TRIPLE_STORE_PATH = environ.get("DJANGO_TRIPLE_STORE_PATH")
TRIPLE_STORE_QUERY_PATH = environ.get("DJANGO_TRIPLE_STORE_QUERY_PATH")
TRIPLE_STORE_USER = environ.get("DJANGO_TRIPLE_STORE_USER")
TRIPLE_STORE_PASSWORD = environ.get("DJANGO_TRIPLE_STORE_PASSWORD")
TRIPLE_STORE_AUTH_HANDLER = environ.get("DJANGO_TRIPLE_STORE_AUTH_HANDLER", "digest")


# CACHE

CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.locmem.LocMemCache",
    }
}

CACHE_MIDDLEWARE_ALIAS = "default"

CACHE_MIDDLEWARE_KEY_PREFIX = ""

CACHE_MIDDLEWARE_SECONDS = 600


# DATABASE

DATABASES = {
    "default": {
        "ATOMIC_REQUESTS": False,
        "AUTOCOMMIT": True,
        "CONN_MAX_AGE": 0,
        "ENGINE": "django.db.backends.postgresql",
        "NAME": environ.get("POSTGRES_DB"),
        "USER": environ.get("POSTGRES_USER"),
        "PASSWORD": environ.get("POSTGRES_PASSWORD"),
        "HOST": environ.get("DJANGO_DATABASE_HOST"),
        "PORT": environ.get("DJANGO_DATABASE_PORT"),
    },
}

DATABASE_ROUTERS: List[Any] = []

DEFAULT_INDEX_TABLESPACE = ""

DEFAULT_TABLESPACE = ""

DEFAULT_AUTO_FIELD = "django.db.models.AutoField"


# WORKER

CELERY_RESULT_BACKEND = environ.get("CELERY_RESULT_BACKEND")

CELERY_BROKER_URL = environ.get("CELERY_BROKER_URL")

CELERY_CACHE_BACKEND = environ.get("CELERY_CACHE_BACKEND")


# DEBUGGING

DEBUG = environ.get("DJANGO_DEBUG", "False") == "True"

DEBUG_PROPAGATE_EXCEPTIONS = False

SILKY_AUTHENTICATION = True
SILKY_AUTHORISATION = True
SILKY_META = True
SILKY_PYTHON_PROFILER = True
SILKY_PYTHON_PROFILER_BINARY = True
if environ.get("DJANGO_MEDIA_ROOT"):
    SILKY_PYTHON_PROFILER_RESULT_PATH = environ.get("DJANGO_MEDIA_ROOT") + "/profiles/"  # type: ignore


def intercept_func(request):
    if hasattr(request.user, "author"):
        return request.user.author.profiler_enabled
    else:
        return False


SILKY_INTERCEPT_FUNC = intercept_func


# EMAIL

ADMINS = json.loads(environ.get("DJANGO_ADMINS", "[]"))  # type: ignore

DEFAULT_FROM_EMAIL = "no-reply@example.org"

SERVER_EMAIL = environ.get("DJANGO_SERVER_EMAIL", "no-reply@example.org")  # type: ignore

EMAIL_SUBJECT_PREFIX = environ.get("DJANGO_EMAIL_SUBJECT_PREFIX", "[Herbie] ")

if environ.get("DJANGO_EMAIL_BACKEND", "False") == "True":
    EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"  # type: ignore

    EMAIL_HOST = environ.get("DJANGO_EMAIL_HOST")

    EMAIL_PORT = environ.get("DJANGO_EMAIL_PORT")

    EMAIL_HOST_USER = environ.get("DJANGO_EMAIL_HOST_USER")

    EMAIL_HOST_PASSWORD = environ.get("DJANGO_EMAIL_HOST_PASSWORD")

    EMAIL_USE_TLS = environ.get("DJANGO_EMAIL_USE_TLS", "True") == "True"

    EMAIL_USE_SSL = environ.get("DJANGO_EMAIL_USE_SSL", "False") == "True"


# ERROR REPORTING

DEFAULT_EXCEPTION_REPORTER = "django.views.debug.ExceptionReporter"

DEFAULT_EXCEPTION_REPORTER_FILTER = "django.views.debug.SafeExceptionReporterFilter"

IGNORABLE_404_URLS: List[Any] = []

MANAGERS: List[Any] = []

SILENCED_SYSTEM_CHECKS: List[Any] = []


# FILE UPLOADS

FILE_UPLOAD_HANDLER = [
    "django.core.files.uploadhandler.MemoryFileUploadHandler",
    "django.core.files.uploadhandler.TemporaryFileUploadHandler",
]

FILE_UPLOAD_MAX_MEMORY_SIZE = int(environ.get("DJANGO_FILE_UPLOAD_MAX_MEMORY_SIZE_MB", "25")) * 1024 * 1024

FILE_UPLOAD_DIRECTORY_PERMISSIONS = None

FILE_UPLOAD_PERMISSIONS = None

FILE_UPLOAD_TEMP_DIR = environ.get("DJANGO_FILE_UPLOAD_TEMP_DIR", BASE_DIR / "uploads")

MEDIA_ROOT = environ.get("DJANGO_MEDIA_ROOT", BASE_DIR / "uploads")

MEDIA_URL = environ.get("DJANGO_MEDIA_URL", "api/media/")

MEDIA_VIA_X_ACCEL_REDIRECT = True


# FORMS

FORM_RENDERER = "django.forms.renderers.DjangoTemplates"


# GLOBALIZATION

FORMAT_MODULE_PATH = None

DATE_FORMAT = "%Y-%m-%d"

SHORT_DATE_FORMAT = "%Y-%m-%d"

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S.%f"

SHORT_DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S.%f"

TIME_FORMAT = "%H:%M:%S.%f"

MONTH_DAY_FORMAT = "F j"

YEAR_MONTH_FORMAT = "F Y"

FIRST_DAY_OF_WEEK = 1  # Monday

DECIMAL_SEPARATOR = "."

THOUSAND_SEPARATOR = ","

USE_THOUSAND_SEPARATOR = False

NUMBER_GROUPING = 0

LANGUAGE_CODE = "en-us"

USE_I18N = False

TIME_ZONE = "UTC"

USE_TZ = False


# HTTP

DATA_UPLOAD_MAX_MEMORY_SIZE = int(environ.get("DJANGO_DATA_UPLOAD_MAX_MEMORY_SIZE_MB", "100")) * 1024 * 1024

DATA_UPLOAD_MAX_NUMBER_FIELDS = int(environ.get("DJANGO_DATA_UPLOAD_MAX_NUMBER_FIELDS", "1000"))

DEFAULT_CHARSET = "utf-8"

DISALLOWED_USER_AGENTS: List[Any] = []

FORCE_SCRIPT_NAME = None

INTERNAL_IPS: List[Any] = []

MIDDLEWARE = [
    "eln.middleware.QueryCountDebugMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "silk.middleware.SilkyMiddleware",
]

SECURE_BROWSER_XSS_FILTER = False

SECURE_CONTENT_TYPE_NOSNIFF = True

SECURE_HSTS_INCLUDE_SUBDOMAINS = False

SECURE_HSTS_PRELOAD = False

SECURE_HSTS_SECONDS = 0

SECURE_PROXY_SSL_HEADER = None
DJANGO_SECURE_PROXY_SSL_HEADER_KEY = environ.get("DJANGO_SECURE_PROXY_SSL_HEADER_KEY", None)
DJANGO_SECURE_PROXY_SSL_HEADER_VALUE = environ.get("DJANGO_SECURE_PROXY_SSL_HEADER_VALUE", None)
if DJANGO_SECURE_PROXY_SSL_HEADER_KEY and DJANGO_SECURE_PROXY_SSL_HEADER_VALUE:
    SECURE_PROXY_SSL_HEADER = (DJANGO_SECURE_PROXY_SSL_HEADER_KEY, DJANGO_SECURE_PROXY_SSL_HEADER_VALUE)


SECURE_REDIRECT_EXEMPT: List[Any] = []

SECURE_REFERRER_POLICY = "same-origin"

SECURE_SSL_HOST = None

SECURE_SSL_REDIRECT = False

SIGNING_BACKEND = "django.core.signing.TimestampSigner"

USE_X_FORWARDED_HOST = environ.get("DJANGO_USE_X_FORWARDED_HOST", "False") == "True"

USE_X_FORWARDED_PORT = environ.get("DJANGO_USE_X_FORWARDED_PORT", "False") == "True"

WSGI_APPLICATION = "eln.wsgi.application"

ASGI_APPLICATION = "eln.asgi.application"


# LOGGING

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "filters": {
        "require_debug_false": {
            "()": "django.utils.log.RequireDebugFalse",
        },
        "require_debug_true": {
            "()": "django.utils.log.RequireDebugTrue",
        },
    },
    "formatters": {
        "django.server": {
            "()": "django.utils.log.ServerFormatter",
            "format": "[{server_time}] {message}",
            "style": "{",
        }
    },
    "handlers": {
        "console": {
            "level": "INFO",
            "filters": ["require_debug_true"],
            "class": "logging.StreamHandler",
        },
        "console_debug_false": {
            "level": "ERROR",
            "filters": ["require_debug_false"],
            "class": "logging.StreamHandler",
        },
        "django.server": {
            "level": "INFO",
            "class": "logging.StreamHandler",
            "formatter": "django.server",
        },
        "mail_admins": {
            "level": "ERROR",
            "filters": ["require_debug_false"],
            "class": "django.utils.log.AdminEmailHandler",
        },
    },
    "loggers": {
        "django": {
            "handlers": ["console", "console_debug_false", "mail_admins"],
            "level": "INFO",
        },
        "django.server": {
            "handlers": ["django.server"],
            "level": "INFO",
            "propagate": False,
        },
    },
}

LOGGING_CONFIG = "logging.config.dictConfig"


# MODELS

ABSOLUTE_URL_OVERRIDES: Dict[Any, Any] = {}

FIXTURE_DIRS: List[Any] = []

INSTALLED_APPS = [
    "rest_framework",
    "knox",
    "backend.apps.BackendConfig",
    "frontend",
    "silk",
    "django_extensions",
    "django_helmholtz_aai",
    "django_celery_results",
    # default apps
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
]


# SECURITY

ALLOWED_HOSTS = environ.get("DJANGO_ALLOWED_HOSTS", "").split()

CSRF_COOKIE_AGE = int(environ.get("DJANGO_CSRF_COOKIE_AGE", "31449600"))

CSRF_COOKIE_DOMAIN = None

CSRF_COOKIE_HTTPONLY = environ.get("DJANGO_CSRF_COOKIE_HTTPONLY", "False") == "True"

CSRF_COOKIE_NAME = "csrftoken"

CSRF_COOKIE_PATH = "/"

CSRF_COOKIE_SAMESITE = environ.get("DJANGO_CSRF_COOKIE_SAMESITE", "Strict")

CSRF_COOKIE_SECURE = environ.get("DJANGO_CSRF_COOKIE_SECURE", "False") == "True"

CSRF_USE_SESSION = environ.get("DJANGO_CSRF_USE_SESSION", "False") == "True"

CSRF_FAILURE_VIEW = "django.views.csrf.csrf_failure"

CSRF_HEADER_NAME = "HTTP_X_CSRFTOKEN"

CSRF_TRUSTED_ORIGINS = environ.get("DJANGO_CSRF_TRUSTED_ORIGINS", "").split()

SECRET_KEY = environ.get("DJANGO_SECRET_KEY")

X_FRAME_OPTIONS = "DENY"


# TEMPLATES

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]


# URLS

APPEND_SLASH = True

PREPEND_WWW = False

ROOT_URLCONF = "eln.urls.production"

MIGRATION_MODULES: Dict[Any, Any] = {}


# AUTH

AUTHENTICATION_BACKENDS = [
    "django.contrib.auth.backends.ModelBackend",
]

AUTH_USER_MODEL = "auth.User"

LOGIN_REDIRECT_URL = "/"

LOGIN_URL = "/"

LOGOUT_REDIRECT_URL = None

PASSWORD_RESET_TIMEOUT = 259200  # 3 days

PASSWORD_HASHERS = [
    "django.contrib.auth.hashers.PBKDF2PasswordHasher",
    "django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher",
    "django.contrib.auth.hashers.Argon2PasswordHasher",
    "django.contrib.auth.hashers.BCryptSHA256PasswordHasher",
]

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

AUTH_LDAP_SERVER_URI = environ.get("DJANGO_LDAP_SERVER_URI")

AUTH_LDAP_BIND_DN = environ.get("DJANGO_LDAP_BIND_DN")

AUTH_LDAP_BIND_PASSWORD = environ.get("DJANGO_LDAP_BIND_PASSWORD")

if AUTH_LDAP_SERVER_URI and AUTH_LDAP_BIND_DN and AUTH_LDAP_BIND_PASSWORD:
    import ldap  # type: ignore
    from django_auth_ldap.config import LDAPSearch, LDAPSearchUnion  # type: ignore

    AUTH_LDAP_USER_SEARCH = LDAPSearchUnion(
        *[
            LDAPSearch(ou, ldap.SCOPE_SUBTREE, environ.get("DJANGO_LDAP_USER_SEARCH", "()"))
            for ou in environ.get("DJANGO_LDAP_USER_OU", "").split(" ")
        ]
    )

    AUTH_LDAP_USER_ATTR_MAP = json.loads(environ.get("DJANGO_LDAP_USER_ATTR_MAP", "{}"))

    AUTH_LDAP_ALWAYS_UPDATE_USER = True

    AUTH_LDAP_CACHE_TIMEOUT = 3600

    LDAP_CA_FILE_PATH = environ.get("DJANGO_LDAP_CA_FILE_PATH", "")
    if LDAP_CA_FILE_PATH != "":
        AUTH_LDAP_CONNECTION_OPTIONS = {
            ldap.OPT_X_TLS_CACERTFILE: LDAP_CA_FILE_PATH,
            ldap.OPT_X_TLS_REQUIRE_CERT: ldap.OPT_X_TLS_ALLOW,
            ldap.OPT_X_TLS_NEWCTX: 0,
        }

    AUTHENTICATION_BACKENDS = [
        "django_auth_ldap.backend.LDAPBackend",
        "django.contrib.auth.backends.ModelBackend",
    ]

DJANGO_HELMHOLTZ_ALLOWED_VOS = environ.get("DJANGO_HELMHOLTZ_ALLOWED_VOS", None)
if DJANGO_HELMHOLTZ_ALLOWED_VOS:
    HELMHOLTZ_ALLOWED_VOS = DJANGO_HELMHOLTZ_ALLOWED_VOS.split(",")

DJANGO_HELMHOLTZ_ALLOWED_VOS_REGEXP = environ.get("DJANGO_HELMHOLTZ_ALLOWED_VOS_REGEXP", None)
if DJANGO_HELMHOLTZ_ALLOWED_VOS_REGEXP:
    HELMHOLTZ_ALLOWED_VOS_REGEXP = DJANGO_HELMHOLTZ_ALLOWED_VOS_REGEXP.split(",")

HELMHOLTZ_CLIENT_ID = environ.get("DJANGO_HELMHOLTZ_CLIENT_ID", None)

HELMHOLTZ_CLIENT_SECRET = environ.get("DJANGO_HELMHOLTZ_CLIENT_SECRET", None)

HELMHOLTZ_AAI_CONF_URL = environ.get("DJANGO_HELMHOLTZ_AAI_CONF_URL", None)

ROOT_URL = environ.get("DJANGO_ROOT_URL", None)

HIDE_USER_PASSWORD_LOGIN = environ.get("DJANGO_HIDE_USER_PASSWORD_LOGIN", "False") == "True"


# MESSAGES

MESSAGE_LEVEL = messages.INFO

MESSAGE_STORAGE = "django.contrib.messages.storage.fallback.FallbackStorage"

MESSAGE_TAGS = {
    messages.DEBUG: "debug",
    messages.INFO: "info",
    messages.SUCCESS: "success",
    messages.WARNING: "warning",
    messages.ERROR: "error",
}


# SESSIONS

SESSION_CACHE_ALIAS = "default"

SESSION_COOKIE_AGE = int(environ.get("DJANGO_SESSION_COOKIE_AGE", "1209600"))  # 2 weeks

SESSION_COOKIE_DOMAIN = None

SESSION_COOKIE_HTTPONLY = environ.get("DJANGO_SESSION_COOKIE_HTTPONLY", "True") == "True"

SESSION_COOKIE_NAME = "sessionid"

SESSION_COOKIE_PATH = "/"

SESSION_COOKIE_SAMESITE = environ.get("DJANGO_SESSION_COOKIE_SAMESITE", "Lax")

SESSION_COOKIE_SECURE = environ.get("DJANGO_SESSION_COOKIE_SECURE", "True") == "True"

SESSION_ENGINE = "django.contrib.sessions.backends.db"

SESSION_EXPIRE_AT_BROWSER_CLOSE = environ.get("DJANGO_SESSION_EXPIRE_AT_BROWSER_CLOSE", "False") == "True"

SESSION_SAVE_EVERY_REQUEST = environ.get("DJANGO_SESSION_SAVE_EVERY_REQUEST", "False") == "True"

SESSION_SERIALIZER = "django.contrib.sessions.serializers.JSONSerializer"

REST_KNOX = {
    "USER_SERIALIZER": "backend.serializers.user.UserSerializer",
    "TOKEN_LIMIT_PER_USER": None,
    "AUTO_REFRESH": False,
}

# STATIC FILES

STATIC_ROOT = environ.get("DJANGO_STATIC_ROOT", BASE_DIR / "static")

STATIC_URL = environ.get("DJANGO_STATIC_URL", "static/")

STATICFILES_DIR: List[Any] = []

STORAGES = {
    "default": {
        "BACKEND": "django.core.files.storage.FileSystemStorage",
    },
    "staticfiles": {
        "BACKEND": "django.contrib.staticfiles.storage.StaticFilesStorage",
    },
}

STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
]


# REST FRAMEWORK

REST_FRAMEWORK = {
    "DEFAULT_AUTHENTICATION_CLASSES": [
        "rest_framework.authentication.SessionAuthentication",
        "knox.auth.TokenAuthentication",
    ],
    "DEFAULT_PAGINATION_CLASS": "backend.pagination.CursorPagination",
    "PAGE_SIZE": 25,
    "TEST_REQUEST_DEFAULT_FORMAT": "json",
}
