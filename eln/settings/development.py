# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

import logging

from django.core.management.color import color_style

from .shared import *

INSTALLED_APPS = [
    "debug_toolbar",
    "corsheaders",
] + INSTALLED_APPS


# VISUALISATION

GRAPH_MODELS = {
    "all_applications": True,
    "group_models": True,
}


# DEBUGGING

SILKY_PYTHON_PROFILER_RESULT_PATH = str(BASE_DIR.absolute() / "uploads" / "profiles")


# FILE UPLOADS

MEDIA_VIA_X_ACCEL_REDIRECT = False


# CORS

CORS_ALLOWED_ORIGINS = [
    "http://localhost:4040",
]
CORS_ALLOW_CREDENTIALS = True


# URLS

ROOT_URLCONF = "eln.urls.development"


def showToolbarCallback(request):
    return (
        (request.path.startswith("/api/") or request.path.startswith("/__debug__/"))
        and not request.path.startswith("/api/admin/")
        and request.accepts("html/text")
    )


DEBUG_TOOLBAR_CONFIG = {
    "SHOW_TOOLBAR_CALLBACK": showToolbarCallback,
}

INTERNAL_IPS = ["172.0.0.1"]

MIDDLEWARE = [
    "debug_toolbar.middleware.DebugToolbarMiddleware",
    "log_request_id.middleware.RequestIDMiddleware",
    "corsheaders.middleware.CorsMiddleware",
] + MIDDLEWARE


# LOGGING


class ColorsFormatter(logging.Formatter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.style = self.configure_style(color_style())

    def configure_style(self, style):
        style.DEBUG = style.HTTP_NOT_MODIFIED
        style.INFO = style.HTTP_INFO
        style.WARNING = style.HTTP_NOT_FOUND
        style.ERROR = style.ERROR
        style.CRITICAL = style.HTTP_SERVER_ERROR
        return style

    def format(self, record):
        message = logging.Formatter.format(self, record)
        colorizer = getattr(self.style, record.levelname, self.style.HTTP_SUCCESS)
        return colorizer(message)


LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "filters": {
        "require_debug_true": {
            "()": "django.utils.log.RequireDebugTrue",
        },
        "request_id": {"()": "log_request_id.filters.RequestIDFilter"},
    },
    "formatters": {
        "colored": {
            "()": "eln.settings.development.ColorsFormatter",
            "format": "%(levelname)-8s [%(asctime)s] [%(request_id)s] %(message)s",
        },
    },
    "handlers": {
        "console": {
            "level": "DEBUG",
            "filters": ["require_debug_true", "request_id"],
            "class": "logging.StreamHandler",
            "formatter": "colored",
        },
        "file": {
            "level": "DEBUG",
            "filters": ["require_debug_true", "request_id"],
            "class": "logging.FileHandler",
            "filename": "log/debug.log",
            "formatter": "colored",
        },
    },
    "loggers": {
        "django.request": {
            "level": "DEBUG",
            "handlers": ["console", "file"],
        },
        "django.server": {
            "level": "DEBUG",
            "handlers": ["console", "file"],
        },
        "django.db.backends": {
            "level": "DEBUG",
            "handlers": ["console", "file"],
        },
        "eln.middleware": {
            "level": "DEBUG",
            "handlers": ["console", "file"],
        },
    },
}
