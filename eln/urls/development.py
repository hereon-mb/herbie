# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

# mypy: ignore-errors

import debug_toolbar
from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path

urlpatterns = (
    [path("__debug__/", include(debug_toolbar.urls))]
    + [path("silk/", include("silk.urls", namespace="silk"))]
    + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    + [path("", include("backend.urls"))]
    + [path("", include("frontend.urls_story"))]
    + [path("", include("frontend.urls"))]
)
