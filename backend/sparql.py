import random
import string
from textwrap import indent

from rdflib import Literal, URIRef


class Select:
    def __init__(self, *variables):
        self._variables = variables
        self._group_graph_patterns = []
        self._distinct = False
        self._ordering = None
        self._limit = None
        self._offset = None

    def where(self, *group_graph_patterns):
        self._group_graph_patterns += filter(None, group_graph_patterns)
        return self

    def distinct(self):
        self._distinct = True
        return self

    def order_by(self, ordering):
        self._ordering = ordering
        return self

    def limit(self, value):
        self._limit = value
        return self

    def offset(self, value):
        self._offset = value
        return self

    def __str__(self):
        select = "SELECT DISTINCT" if self._distinct else "SELECT"
        ordering = "ORDER BY " + self._ordering if self._ordering else None
        limit = "LIMIT " + str(self._limit) if self._limit else None
        offset = "OFFSET " + str(self._offset) if self._offset else None

        return join_with_newlines(
            f"{select} {self._variables_to_str(self._variables)}",
            "WHERE {",
            indent(group_graph_patterns_to_str(self._group_graph_patterns), "   "),
            "}",
            ordering,
            limit,
            offset,
        )

    def _variables_to_str(self, variables):
        if len(variables) == 0:
            return "*"
        else:
            return " ".join(map(str, variables))


class Ask:
    def __init__(self, *group_graph_patterns):
        self._group_graph_patterns = group_graph_patterns

    def __str__(self):
        return join_with_newlines(
            "ASK {",
            indent(group_graph_patterns_to_str(self._group_graph_patterns), "   "),
            "}",
        )


class Construct:
    def __init__(self, *triples):
        self._triples = triples
        self._group_graph_patterns = []
        self._ordering = None
        self._limit = None
        self._offset = None

    def where(self, *group_graph_patterns):
        self._group_graph_patterns += filter(None, group_graph_patterns)
        return self

    def order_by(self, ordering):
        self._ordering = ordering
        return self

    def limit(self, value):
        self._limit = value
        return self

    def offset(self, value):
        self._offset = value
        return self

    def __str__(self):
        ordering = "ORDER BY " + self._ordering if self._ordering else None
        limit = "LIMIT " + str(self._limit) if self._limit else None
        offset = "OFFSET " + str(self._offset) if self._offset else None

        return join_with_newlines(
            "CONSTRUCT {",
            indent(triples_to_str(self._triples), "   "),
            "} WHERE {",
            indent(group_graph_patterns_to_str(self._group_graph_patterns), "   "),
            "}",
            ordering,
            limit,
            offset,
        )


class Insert:
    def __init__(self, *group_graph_patterns):
        self._data = group_graph_patterns
        self._group_graph_patterns = []
        self._ordering = None
        self._limit = None
        self._offset = None

    def where(self, *group_graph_patterns):
        self._group_graph_patterns += filter(None, group_graph_patterns)
        return self

    def order_by(self, ordering):
        self._ordering = ordering
        return self

    def limit(self, value):
        self._limit = value
        return self

    def offset(self, value):
        self._offset = value
        return self

    def __str__(self):
        ordering = "ORDER BY " + self._ordering if self._ordering else None
        limit = "LIMIT " + str(self._limit) if self._limit else None
        offset = "OFFSET " + str(self._offset) if self._offset else None

        return join_with_newlines(
            "INSERT {",
            indent(group_graph_patterns_to_str(self._data), "   "),
            "}",
            "WHERE {",
            indent(group_graph_patterns_to_str(self._group_graph_patterns), "   "),
            "}",
            ordering,
            limit,
            offset,
        )


class Delete:
    def __init__(self, *group_graph_patterns):
        self._data = group_graph_patterns
        self._group_graph_patterns = []
        self._ordering = None
        self._limit = None
        self._offset = None

    def where(self, *group_graph_patterns):
        self._group_graph_patterns += filter(None, group_graph_patterns)
        return self

    def order_by(self, ordering):
        self._ordering = ordering
        return self

    def limit(self, value):
        self._limit = value
        return self

    def offset(self, value):
        self._offset = value
        return self

    def __str__(self):
        ordering = "ORDER BY " + self._ordering if self._ordering else None
        limit = "LIMIT " + str(self._limit) if self._limit else None
        offset = "OFFSET " + str(self._offset) if self._offset else None

        return join_with_newlines(
            "DELETE {",
            indent(group_graph_patterns_to_str(self._data), "   "),
            "}",
            "WHERE {",
            indent(group_graph_patterns_to_str(self._group_graph_patterns), "   "),
            "}",
            ordering,
            limit,
            offset,
        )


class InsertData:
    def __init__(self, *group_graph_patterns):
        self._data = group_graph_patterns

    def __str__(self):
        return join_with_newlines(
            "INSERT DATA {",
            indent(group_graph_patterns_to_str(self._data), "   "),
            "}",
        )


class Create:
    def __init__(self, iri_graph):
        self._iri_graph = iri_graph
        self._silent = False

    def silent(self):
        self._silent = True
        return self

    def __str__(self):
        silent = "SILENT" if self._silent else ""

        return join_with_newlines(f"CREATE {silent} GRAPH {word_to_str(self._iri_graph)}")


class Drop:
    def __init__(self, iri_graph):
        self._iri_graph = iri_graph
        self._silent = False

    def silent(self):
        self._silent = True
        return self

    def __str__(self):
        silent = "SILENT" if self._silent else ""

        return join_with_newlines(f"DROP {silent} GRAPH {word_to_str(self._iri_graph)}")


class GraphPattern:
    def __init__(self, graph, triples):
        self._graph = graph
        self._triples = triples

    def __str__(self):
        return join_with_newlines(
            f"GRAPH {word_to_str(self._graph)} {{",
            indent(triples_to_str(self._triples), "    "),
            "}",
        )


class Values:
    def __init__(self, name, *values):
        self._name = name
        self._values = values

    def __str__(self):
        return join_with_newlines(
            f"VALUES {self._name} {{",
            indent(join_with_newlines(*map(word_to_str, self._values)), "    "),
            "}",
        )


class Bind:
    def __init__(self, expr, variable):
        self._expr = expr
        self._variable = variable

    def __str__(self):
        return f"BIND({self._expr} AS {self._variable})"


class Filter:
    def __init__(self, expr):
        self._filter = expr

    def __str__(self):
        return f"FILTER ({self._filter})"


class FilterNotExists:
    def __init__(self, *triples):
        self._triples = triples

    def __str__(self):
        return join_with_newlines(
            "FILTER NOT EXISTS {",
            indent(triples_to_str(self._triples), "    "),
            "}",
        )


class Optional:
    def __init__(self, *group_graph_patterns):
        self._group_graph_patterns = group_graph_patterns

    def __str__(self):
        if len(self._group_graph_patterns) == 1:
            return f"OPTIONAL {{ {group_graph_pattern_to_str(self._group_graph_patterns[0])} }}"
        elif len(self._group_graph_patterns) > 1:
            return join_with_newlines(
                "OPTIONAL {",
                indent(group_graph_patterns_to_str(self._group_graph_patterns), "    "),
                "}",
            )


class Minus:
    def __init__(self, *group_graph_patterns):
        self._group_graph_patterns = group_graph_patterns

    def __str__(self):
        if len(self._group_graph_patterns) == 1:
            return f"MINUS {{ {group_graph_pattern_to_str(self._group_graph_patterns[0])} }}"
        elif len(self._group_graph_patterns) > 1:
            return join_with_newlines(
                "MINUS {",
                indent(group_graph_patterns_to_str(self._group_graph_patterns), "    "),
                "}",
            )


class Union:
    def __init__(self, *group_graph_patternses):
        self._group_graph_patternses = list(group_graph_patternses)

    def __str__(self):
        return "\nUNION\n".join(
            [
                join_with_newlines(
                    "{",
                    indent(group_graph_patterns_to_str(group_graph_patterns), "    "),
                    "}",
                )
                for group_graph_patterns in self._group_graph_patternses
            ]
        )


class Var:
    def __init__(self, name, randomize=True):
        self._name = name
        if randomize:
            self._name += "_" + self._random_suffix()

    @property
    def name(self):
        return self._name

    def _random_suffix(self):
        return "".join(random.choices(string.ascii_uppercase + string.digits, k=4))

    def __str__(self):
        return f"?{self._name}"


class BlankNode:
    def __init__(self, name):
        self._name = name

    def __str__(self):
        return f"_:{self._name}"


class Inverse:
    def __init__(self, path):
        self._path = path

    def __str__(self):
        return f"^{word_to_str(self._path)}"


class ZeroOrMore:
    def __init__(self, path):
        self._path = path

    def __str__(self):
        return f"{word_to_str(self._path)}*"


class ZeroOrOne:
    def __init__(self, path):
        self._path = path

    def __str__(self):
        return f"{word_to_str(self._path)}?"


class Sequence:
    def __init__(self, *paths) -> None:
        self._paths = paths

    def __str__(self):
        return " / ".join(map(word_to_str, self._paths))


class Alternative:
    def __init__(self, *paths) -> None:
        self._paths = paths

    def __str__(self):
        return "".join(
            [
                "(",
                " | ".join(map(word_to_str, self._paths)),
                ")",
            ]
        )


def triples_to_str(triples):
    return join_with_dotted_newlines(*map(triple_to_str, triples))


def triple_to_str(triple):
    if isinstance(triple, Optional):
        return str(triple)
    elif isinstance(triple, Minus):
        return str(triple)
    elif isinstance(triple, Filter):
        return str(triple)
    elif isinstance(triple, FilterNotExists):
        return str(triple)
    elif isinstance(triple, Union):
        return str(triple)
    elif isinstance(triple, GraphPattern):
        return str(triple)
    elif isinstance(triple, Var):
        return str(triple)
    elif isinstance(triple, Bind):
        return str(triple)
    elif isinstance(triple, Select):
        return str(triple)
    else:
        return " ".join(map(word_to_str, triple))


def word_to_str(word):
    if isinstance(word, URIRef):
        return f"<{word}>"
    elif isinstance(word, Literal):
        return word.n3()
    else:
        return str(word)


def group_graph_patterns_to_str(group_graph_patterns):
    return join_with_dotted_newlines(*map(group_graph_pattern_to_str, group_graph_patterns))


def group_graph_pattern_to_str(group_graph_pattern):
    if isinstance(group_graph_pattern, Select):
        return join_with_newlines(
            "{",
            indent(str(group_graph_pattern), "    "),
            "}",
        )
    elif isinstance(group_graph_pattern, GraphPattern):
        return str(group_graph_pattern)
    elif isinstance(group_graph_pattern, Values):
        return str(group_graph_pattern)
    else:
        return triple_to_str(group_graph_pattern)


def join_with_newlines(*lines):
    return "\n".join(filter(None, lines))


def join_with_dotted_newlines(*lines):
    return " .\n".join(filter(None, lines))
