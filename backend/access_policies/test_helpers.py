# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from types import SimpleNamespace


def has_permission(access_policy, action, user, get_object, method="GET") -> bool:
    return access_policy().has_permission(
        request(user, method),
        view(action, get_object),
    )


def scoped_queryset(access_policy, user, queryset, method="GET"):
    queryset = access_policy().scope_queryset(
        request(user, method),
        queryset,
    )
    return set(queryset)


def request(user, method):
    return SimpleNamespace(
        path="/api/",
        user=user,
        method=method,
    )


def view(action, get_object):
    return SimpleNamespace(
        action=action,
        get_object=get_object,
    )
