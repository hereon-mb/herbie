# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from django.db.models import Subquery

from ..models.workspace import Workspace
from . import BaseAccessPolicy
from .workspace import WorkspaceAccessPolicy


class DatasetsAccessPolicy(BaseAccessPolicy):
    statements = [
        {
            "action": ["list"],
            "principal": "authenticated",
            "effect": "allow",
            "condition": "is_at_least_guest",
        },
        {
            "action": [
                "retrieve",
                "access",
                "version_latest",
                "datasets_version",
                "datasets_version_entered",
                "datasets_version_persisted",
                "datasets_version_generated",
                "datasets_version_support",
            ],
            "principal": "authenticated",
            "effect": "allow",
            "condition": "is_at_least_guest",
        },
        {
            "action": [
                "draft",
                "draft_entered",
                "draft_persisted",
                "draft_generated",
                "draft_support",
            ],
            "principal": "authenticated",
            "effect": "allow",
            "condition_expression": """
                (is_author_draft and is_at_least_editor) or
                is_at_least_manager
            """,
        },
        {
            "action": ["create"],
            "principal": "authenticated",
            "effect": "allow",
            "condition": "is_at_least_editor",
        },
        {
            "action": [
                "edit",
                "destroy",
            ],
            "principal": "authenticated",
            "effect": "allow",
            "condition_expression": """
                (is_author_published and is_at_least_editor) or
                is_at_least_manager
            """,
        },
        {
            "action": [
                "access_update",
                "draft_update",
                "generate",
                "cancel",
                "publish",
                "aliases_create",
                "aliases_delete",
            ],
            "principal": "authenticated",
            "effect": "allow",
            "condition_expression": """
                (is_author_draft and is_at_least_editor) or
                is_at_least_manager
            """,
        },
        {
            "action": ["*"],
            "principal": "admin",
            "effect": "allow",
        },
    ]

    def is_at_least_manager(self, request, view, action):
        return view.get_workspace().memberships.all().at_least_manager().filter(author__user=request.user).exists()

    def is_at_least_editor(self, request, view, action):
        return view.get_workspace().memberships.all().at_least_editor().filter(author__user=request.user).exists()

    def is_at_least_guest(self, request, view, action):
        return view.get_workspace().memberships.all().active().filter(author__user=request.user).exists()

    def is_author_draft(self, request, view, action) -> bool:
        if version_draft := self._version_draft(view):
            return version_draft.created_by.user == request.user
        else:
            return False

    def is_author_published(self, request, view, action) -> bool:
        if version_published := self._version_published(view):
            return version_published.created_by.user == request.user
        else:
            return False

    def _version_draft(self, view):
        return view.get_object().version_draft

    def _version_published(self, view):
        return view.get_object().version_published

    @classmethod
    def scope_queryset(cls, author, queryset):
        if not author.user.is_superuser:
            workspaces = Workspace.objects.all()
            workspaces = WorkspaceAccessPolicy.scope_queryset(author, workspaces)
            queryset = queryset.filter(workspace__in=Subquery(workspaces.values("pk"))).distinct()
        return queryset
