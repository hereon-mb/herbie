# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2024 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.


from . import BaseAccessPolicy


class RoCrateImportAccessPolicy(BaseAccessPolicy):
    statements = [
        {
            "action": ["list"],
            "principal": "authenticated",
            "effect": "allow",
        },
        {
            "action": ["retrieve"],
            "principal": "authenticated",
            "effect": "allow",
            "condition": "is_at_least_guest",
        },
        {
            "action": ["create"],
            "principal": "authenticated",
            "effect": "allow",
            "condition": "is_at_least_manager",
        },
        {
            "action": ["*"],
            "principal": "admin",
            "effect": "allow",
        },
    ]

    @classmethod
    def scope_queryset(cls, request, queryset, workspace):
        return queryset.filter(workspace=workspace)

    def is_at_least_manager(self, request, view, action):
        return view.get_workspace().memberships.all().at_least_manager().filter(author__user=request.user).exists()

    def is_at_least_guest(self, request, view, action):
        return view.get_object().workspace.memberships.all().active().filter(author__user=request.user).exists()
