# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from django.db.models import OuterRef, Subquery

from ..models.workspace import Membership
from . import BaseAccessPolicy


class WorkspaceAccessPolicy(BaseAccessPolicy):
    statements = [
        {
            "action": ["list", "create"],
            "principal": "authenticated",
            "effect": "allow",
        },
        {
            "action": ["retrieve"],
            "principal": "authenticated",
            "effect": "allow",
            "condition": "is_at_least_guest",
        },
        {
            "action": ["update", "partial_update"],
            "principal": "authenticated",
            "effect": "allow",
            "condition": "is_at_least_admin",
        },
        {
            "action": ["destroy"],
            "principal": "authenticated",
            "effect": "allow",
            "condition": "is_owner",
        },
        {
            "action": ["*"],
            "principal": "admin",
            "effect": "allow",
        },
    ]

    @classmethod
    def scope_queryset(cls, author, queryset):
        memberships = Membership.objects.all().active().filter(workspace=OuterRef("pk")).filter(author=author)
        return queryset.filter(memberships__in=Subquery(memberships.values("pk")))

    def is_owner(self, request, view, action):
        return view.get_object().memberships.all().at_least_owner().filter(author__user=request.user).exists()

    def is_at_least_admin(self, request, view, action):
        return view.get_object().memberships.all().at_least_admin().filter(author__user=request.user).exists()

    def is_at_least_guest(self, request, view, action):
        return view.get_object().memberships.all().active().filter(author__user=request.user).exists()


class MembershipAccessPolicy(BaseAccessPolicy):
    statements = [
        {
            "action": ["retrieve"],
            "principal": "authenticated",
            "effect": "allow",
            "condition": "is_at_least_guest",
        },
        {
            "action": ["update", "destroy"],
            "principal": "authenticated",
            "effect": "allow",
            "condition": "is_at_least_admin",
        },
        {
            "action": ["*"],
            "principal": "admin",
            "effect": "allow",
        },
    ]

    def is_at_least_admin(self, request, view, action):
        return view.get_object().workspace.memberships.all().at_least_admin().filter(author__user=request.user).exists()

    def is_at_least_guest(self, request, view, action):
        return view.get_object().workspace.memberships.all().active().exists(author__user=request.user)
