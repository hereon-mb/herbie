# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

import typing
from dataclasses import dataclass
from typing import Any

import pytest
from rest_framework.test import APIClient

from .user import *


@pytest.fixture(scope="function")
def unauthenticated_client(db_session):
    client = APIClient()
    yield client


@pytest.fixture(scope="session")
def authenticated_client_for(db_session):
    def _authenticated_client_for(author):
        client = APIClient()
        client.login(username=author.user.username, password="secret")
        return client

    yield _authenticated_client_for


@pytest.fixture(scope="session")
def authenticated_client(db_session, author_alice):
    client = APIClient()
    client.login(username="alice", password="secret")
    yield client


@pytest.fixture(scope="session")
def authenticated_client_bob(db_session, author_bob):
    client = APIClient()
    client.login(username="bob", password="secret")
    yield client


@pytest.fixture(scope="session")
def authenticated_client_cindi(db_session, author_cindi):
    client = APIClient()
    client.login(username="cindi", password="secret")
    yield client


@pytest.fixture(scope="session")
def authenticated_client_dave(db_session, author_dave):
    client = APIClient()
    client.login(username="dave", password="secret")
    yield client


@dataclass
class Request:
    user: Any
    path: typing.Optional[str]

    def build_absolute_uri(self, url):
        return f"http://testserver{url}"

    @property
    def GET(self):
        return []

    @property
    def path_info(self):
        return self.path


@pytest.fixture(scope="session")
def request_by(user_alice):
    def _request_by(user=user_alice):
        return Request(user=user, path=None)

    yield _request_by


@pytest.fixture(scope="session")
def context_user_alice(user_alice):
    return {
        "request": Request(user=user_alice, path=None),
    }


@pytest.fixture(scope="session")
def context_user_alice_with_path(user_alice):
    def to_context(path):
        return {
            "request": Request(user=user_alice, path=path),
        }

    yield to_context


@pytest.fixture(scope="session")
def http_request__alice(user_alice, author_alice):
    @dataclass
    class HttpRequest:
        user: Any

        def build_absolute_uri(self, url):
            return f"http://testserver{url}"

        @property
        def GET(self):
            return []

    yield HttpRequest(user=user_alice)
