import pytest
from rdflib import DCTERMS, Graph, URIRef

from ..models.rdf_document import RDFDocument
from ..namespaces import Namespaces
from ..services.rdf_document import RDFDocumentService
from ..services.rdf_document_version import RDFDocumentVersionService  # type: ignore


@pytest.fixture(scope="function")
def rdf_document_service(author_alice):
    yield RDFDocumentService(author_alice)


@pytest.fixture(scope="function")
def rdf_document_version_service(author_alice):
    yield RDFDocumentVersionService(author_alice)


@pytest.fixture(scope="function")
def rdf_document__unpublished(rdf_document_service, create_workspace):
    rdf_documents = []

    workspace = create_workspace()

    def to_rdf_document(service=rdf_document_service, workspace=workspace, clean_up=True, sync_rdf_store=True):
        result = service.create(workspace=workspace)
        if sync_rdf_store:
            result.sync_rdf_store()
        rdf_document = result.rdf_document
        if clean_up:
            rdf_documents.append(rdf_document)
        return rdf_document

    yield to_rdf_document

    for rdf_document in rdf_documents:
        rdf_document.delete()


@pytest.fixture(scope="function")
def rdf_document__show_case(rdf_document_service, rdf_document__unpublished):
    def to_rdf_document():
        rdf_document_show_case = rdf_document__unpublished()
        graph = Graph().parse(
            "backend/fixtures/ontology/example/show_case/1.0.0.ttl",
            publicID=Namespaces().datasets_draft(rdf_document_show_case.uuid),
        )
        rdf_document_service.update(rdf_document_show_case, graph).sync_rdf_store()
        rdf_document_service.publish(rdf_document_show_case).sync_rdf_store()
        return rdf_document_show_case

    yield to_rdf_document


@pytest.fixture(scope="function")
def rdf_document__show_case__unpublished(rdf_document_service, rdf_document__unpublished):
    def to_rdf_document():
        rdf_document_show_case = rdf_document__unpublished()
        result = rdf_document_service.update(
            rdf_document_show_case, Graph().parse("backend/fixtures/ontology/example/show_case/1.0.0.ttl")
        )
        result.sync_rdf_store()
        result = rdf_document_service.publish(rdf_document_show_case)
        result.sync_rdf_store()

        rdf_document = rdf_document__unpublished()
        graph = Graph()
        conforms_to = Namespaces().datasets_version(rdf_document_show_case.uuid, 1)
        graph.add((URIRef(""), DCTERMS.conformsTo, URIRef(conforms_to)))
        result = rdf_document_service.update(rdf_document, graph)
        result.sync_rdf_store()
        return rdf_document

    yield to_rdf_document


@pytest.fixture(scope="function")
def rdf_document__published(rdf_document_service, rdf_document__unpublished):
    def to_rdf_document():
        rdf_document = rdf_document__unpublished()
        result = rdf_document_service.publish(rdf_document)
        result.sync_rdf_store()
        return rdf_document

    yield to_rdf_document


@pytest.fixture(scope="function")
def rdf_document__published_with_alias(rdf_document_service, rdf_document__unpublished):
    def to_rdf_document():
        rdf_document = rdf_document__unpublished(sync_rdf_store=True)
        result = rdf_document_service.add_alias(rdf_document, "http://example.org/document/")
        result.sync_rdf_store()
        result = rdf_document_service.publish(rdf_document)
        result.sync_rdf_store()
        rdf_document = RDFDocument.objects.all().with_prefetches().get(pk=rdf_document.pk)  # type: ignore
        return rdf_document

    yield to_rdf_document
