# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from uuid import uuid4

import pytest
from django.contrib.auth import get_user_model


@pytest.fixture(scope="function")
def user_random(db_session):
    users = []

    def _user_random(prefix=""):
        username_random = f"{prefix}{uuid4()}"
        user = get_user_model().objects.create_user(
            username=username_random,
            first_name=f"first name of {username_random}",
            last_name=f"last name of {username_random}",
            email=f"{username_random}@example.com",
            password="secret",
        )
        users.append(user)
        return user

    yield _user_random

    for user in users:
        user.delete()


@pytest.fixture(scope="session")
def user_superuser(db_session):
    user = get_user_model().objects.create_superuser(
        username="superuser",
        first_name="Super",
        last_name="User",
        email="superuser@example.com",
        password="secret",
    )
    yield user
    user.delete()


@pytest.fixture(scope="session")
def user_alice(db_session):
    user = get_user_model().objects.create_user(
        username="alice",
        first_name="Alice",
        last_name="Wonderland",
        email="alice@example.com",
        password="secret",
    )
    yield user
    user.delete()


@pytest.fixture(scope="session")
def user_bob(db_session):
    user = get_user_model().objects.create_user(
        username="bob",
        first_name="Bob",
        last_name="Builder",
        email="bob@example.com",
        password="secret",
    )
    yield user
    user.delete()


@pytest.fixture(scope="session")
def user_cindi(db_session):
    user = get_user_model().objects.create_user(
        username="cindi",
        first_name="Cindi",
        last_name="Mayweather",
        email="cindi@example.com",
        password="secret",
    )
    yield user
    user.delete()


@pytest.fixture(scope="session")
def user_dave(db_session):
    user = get_user_model().objects.create_user(
        username="dave",
        first_name="Dave",
        last_name="Davidson",
        email="dave@example.com",
        password="secret",
    )
    yield user
    user.delete()
