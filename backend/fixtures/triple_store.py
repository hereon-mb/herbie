from uuid import uuid4

import pytest
from django.conf import settings
from rdflib import URIRef

from ..services.triple_store import TripleStore
from ..sparql import Create, Drop, GraphPattern, Insert


@pytest.fixture(scope="session")
def triple_store(request):
    ts = TripleStore()

    def delete_all_graphs():
        result = ts.query(
            """
            SELECT DISTINCT ?graph WHERE {
                GRAPH ?graph { ?s ?p ?o }
            }
        """
        )
        graph_iris = [URIRef(binding["graph"]["value"]) for binding in result["results"]["bindings"] if "graph" in binding]
        for graph_iri in graph_iris:
            if graph_iri.startswith(settings.IRI_PREFIX):
                ts.delete_graph(graph_iri)

    request.addfinalizer(delete_all_graphs)
    delete_all_graphs()

    return ts


@pytest.fixture(scope="session")
def clear_triple_store(request):
    ts = TripleStore()

    def delete_all_graphs():
        result = ts.query(
            """
            SELECT DISTINCT ?graph WHERE {
                GRAPH ?graph { ?s ?p ?o }
            }
        """
        )
        graph_iris = [URIRef(binding["graph"]["value"]) for binding in result["results"]["bindings"] if "graph" in binding]
        for graph_iri in graph_iris:
            if graph_iri.startswith(settings.IRI_PREFIX):
                ts.delete_graph(graph_iri)

    return delete_all_graphs


@pytest.fixture
def graph_empty(iri_random):
    iris = []

    def _graph_empty():
        iri = iri_random()
        TripleStore().update(Create(iri))
        return iri

    yield _graph_empty

    for iri in iris:
        TripleStore().update(Drop(iri))


@pytest.fixture
def graph_from(graph_empty):
    def _graph_from(triples):
        iri = graph_empty()
        TripleStore().update(Insert(GraphPattern(iri, triples)))
        return iri

    yield _graph_from


@pytest.fixture
def iri_random():
    def _iri_random():
        return URIRef(f"http://testserver/{uuid4()}/")

    yield _iri_random
