# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from uuid import uuid4

import pytest

from ..models.department import Department


@pytest.fixture(scope="function")
def department_random(db_session):
    departments = []

    def _department_random():
        abbreviation_random = str(uuid4())[0:3]
        department = Department.objects.create(
            name_en="name en of {abbreviation_random}",
            name_de="name de of {abbreviation_random}",
            abbreviation=abbreviation_random,
        )
        departments.append(department)
        return department

    yield _department_random

    for department in departments:
        department.delete()


@pytest.fixture(scope="session")
def department_a(db_session):
    department = Department.objects.create(
        name_en="Department A",
        name_de="Department A",
        abbreviation="DPA",
    )
    yield department
    department.delete()


@pytest.fixture(scope="session")
def department_b(db_session):
    department = Department.objects.create(
        name_en="Department B",
        name_de="Department B",
        abbreviation="DPB",
    )
    yield department
    department.delete()
