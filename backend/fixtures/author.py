# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

import pytest

from ..models.author import Author
from .user import *


@pytest.fixture(scope="function")
def author_random(user_random):
    authors = []

    def _author_random():
        author = Author.objects.create(user=user_random())
        authors.append(author)
        return author

    yield _author_random

    for author in authors:
        if hasattr(author, "preferences_author"):
            author.preferences_author.delete()
        author.delete()


@pytest.fixture(scope="session")
def author_superuser(user_superuser):
    author = Author.objects.create(user=user_superuser)
    yield author
    if hasattr(author, "preferences_author"):
        author.preferences_author.delete()
    author.delete()


@pytest.fixture(scope="session")
def author_alice(user_alice):
    author = Author.objects.create(
        user=user_alice,
        can_send_sparql_queries=True,
        can_upload_raw_files=True,
    )
    yield author
    if hasattr(author, "preferences_author"):
        author.preferences_author.delete()
    author.delete()


@pytest.fixture(scope="session")
def author_bob(user_bob):
    author = Author.objects.create(user=user_bob)
    yield author
    if hasattr(author, "preferences_author"):
        author.preferences_author.delete()
    author.delete()


@pytest.fixture(scope="session")
def author_cindi(user_cindi):
    author = Author.objects.create(user=user_cindi)
    yield author
    if hasattr(author, "preferences_author"):
        author.preferences_author.delete()
    author.delete()


@pytest.fixture(scope="session")
def author_dave(user_dave):
    author = Author.objects.create(user=user_dave)
    yield author
    if hasattr(author, "preferences_author"):
        author.preferences_author.delete()
    author.delete()
