# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from uuid import uuid4

import pytest

from ..services.workspace import WorkspaceService


@pytest.fixture
def create_workspace(request_by, author_alice):
    workspaces = []

    def _create_workspace(author=author_alice, randomize=False):
        randomization = uuid4() if randomize else ""
        workspace = WorkspaceService(request_by(author.user)).create_workspace(
            slug=f"workspace-a{randomization}",
            name="Workspace A",
            description="The Workspace A description",
        )
        workspaces.append(workspace)
        return workspace

    yield _create_workspace

    for workspace in workspaces:
        workspace.delete()
