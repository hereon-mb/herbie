from rdflib.namespace import DefinedNamespace, Namespace
from rdflib.term import URIRef


class HASH(DefinedNamespace):
    """
    DESCRIPTION_EDIT_ME_!

    Generated from: SOURCE_RDF_FILE_EDIT_ME_!
    Date: 2024-11-15 08:27:11.476396
    """

    _NS = Namespace("http://purls.helmholtz-metadaten.de/herbie/hash/#")

    documentRoot: URIRef  # An ontology should contain exactly one sh:NodeShape with :documentRoot true.  This sh:NodeShape will be used for the main form of a document.
    gridEditorAvailableColumnsPath: URIRef  # All nodes which can be reached by following the SHACL property path from the current focus node build the set of available columns of the grid.  The label is given by an rdfs:label predicate on the node.
    gridEditorAvailableRowsPath: URIRef  # All nodes which can be reached by following the SHACL property path from the current focus node build the set of available rows of the grid.  The label is given by an rdfs:label predicate on the node.
    gridEditorCellEditor: URIRef  # The sh:NodeShape used to display the details form for a cell which is displayed next to the grid , defaults to the sh:NodeShape provided by the sh:node.
    gridEditorCellViewer: (
        URIRef  # The sh:NodeShape used to display the cell inside the grid, defaults to the sh:NodeShape provided by the sh:node.
    )
    gridEditorColumnPath: URIRef  # The node found by following the SHACL property path from the current focus node specifies in which column the current focus node belongs.  The must be exactly one such node.
    gridEditorRowPath: URIRef  # The node found by following the SHACL property path from the current focus node specifies in which row the current focus node belongs.  The must be exactly one such node.
    persist: URIRef  #
