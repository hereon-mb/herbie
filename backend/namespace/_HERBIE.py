from rdflib.namespace import DefinedNamespace, Namespace
from rdflib.term import URIRef


class HERBIE(DefinedNamespace):
    """
    DESCRIPTION_EDIT_ME_!

    Generated from: SOURCE_RDF_FILE_EDIT_ME_!
    Date: 2024-11-15 08:27:11.875740
    """

    _NS = Namespace("http://purls.helmholtz-metadaten.de/herbie/core/#")

    Author: URIRef  # The person who created and submitted the current entry.
    Cell: URIRef  # The smallest structural and functional unit of an organism, which is typically microscopic.
    Clerk: URIRef  # One or more persons who perform the current process this protocol belongs to. All clerks are related to the institution where the Herbie instance, in which the current protocol is created, is being used.
    Client: URIRef  # One or more persons, departments, institutes or projects that have requested the process this protocol belongs to and will use the resulting materials/samples and data. All clients are related to the institution where the Herbie instance, in which the current protocol is created, is being used.
    Department: URIRef  # An institute is divided into several smaller organizational units with distinct research topics.
    Document: URIRef  # A document is a container for different versions of a graph.
    DocumentDraft: URIRef  # A document draft is a concrete graph which has not been published, yet.  It can contain information about protocols, materials, experiments etc..
    DocumentPublication: URIRef  # A publication is an activity which starts when a user creates a draft for a document, and ends when the user publishes this document.
    DocumentVersion: URIRef  # A document version is a concrete graph.  It can contain information about protocols, materials, experiments etc..
    EndedAtTimeShape: URIRef  #
    Equipment: URIRef  # Equipment is any machine, device, instrument or part of that, that is used in the lab for fabricating, preparing or measuring a sample or material. Every piece of equipment is uniquely identifiable. Equipment does not include consumables.
    HasClerkShape: URIRef  #
    HasClientShape: URIRef  #
    IsPerformedOnShape: URIRef  #
    Material: URIRef  # A material represents the totality of a substance created by some process.
    OrganizationalGroup: URIRef  # General information which is not specific to process.
    Project: URIRef  # A project usually contains one or several similar research questions that are planned to be examined in a set of experiments. A certain group of people belongs to a project and is allowed to view any information relevant for the project. A project originate from third party funding, a thesis or any other research question of interest.
    Sample: URIRef  # A sample represents a specific piece of a material in a certain shape which is made suitable for the measurement.
    SampleSet: URIRef  # A sample set represents a set of samples made from some material. All samples within a sample set have the same shape.
    StartedAtTimeShape: URIRef  #
    Workspace: URIRef  #
    hasClerk: URIRef  # One or more persons who perform the current process this protocol belongs to. All clerks are related to the institution where the Herbie instance, in which the current protocol is created, is being used.
    hasClient: URIRef  # One or more persons, departments, institutes or projects that have requested the process this protocol belongs to and will use the resulting materials/samples and data. All clients are related to the institution where the Herbie instance, in which the current protocol is created, is being used.
    hasPrefix: URIRef  # This stores the IRI prefix of the dataset and mainly exists to improve query performance.
    hasPublicId: URIRef  # The unique identifier of a material is automatically generated within Herbie depending on the manufacturing process.
    isPerformedOn: URIRef  # Date on which process has been performed on.
    latestVersionPublished: URIRef  # This property points to the latest published version of the RDF document, if it exists.
    workspace: URIRef  #
