# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from os.path import basename, splitext

from django.utils.text import slugify


def slugify_filename(filename):
    return slugify(splitext(basename(filename)))


def add_suffix(slug, other_slugs):
    indices = [index(slug, other_slug) for other_slug in other_slugs if other_slug.startswith(slug)]
    indices = [i for i in indices if i is not None]

    if len(indices) == 0:
        return slug
    else:
        max_index = max(indices)
        return "{}-{}".format(slug, max_index + 1)


def index(slug, other_slug):
    suffix = other_slug[len(slug) + 1 :]
    if suffix == "":
        return 0
    elif suffix.isdigit():
        return int(suffix)
    else:
        return None
