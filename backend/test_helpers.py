# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

import pytest
from django.core.exceptions import ValidationError
from django.forms.models import model_to_dict


def models_to_dict(models):
    return list(map(model_to_dict, models))


def error_codes(validation_error: ValidationError):
    codes = {}
    if hasattr(validation_error, "error_dict"):
        for field, errors in validation_error.error_dict.items():
            codes[field] = [error.code for error in errors]
    return codes


def assert_validate_presence(field, to_data, entry_name="protocol"):
    """check validation for mandatory fields by removing values"""

    def remove_value(field, data):
        if isinstance(getattr(data, field), str):
            setattr(data, field, "")
        else:
            setattr(data, field, None)
        data.save()

    assert_validate("blank", field, to_data, remove_value, entry_name)


def assert_validate_max(field, max_value, to_data, entry_name="protocol"):
    """check validation for max value by setting above the maximum"""

    def set_above_max_value(field, data):
        setattr(data, field, max_value + 1)
        data.save()

    assert_validate("max_value", field, to_data, set_above_max_value, entry_name)


def assert_validate_min(field, min_value, to_data, entry_name="protocol"):
    """check validation for min value by setting below the maximum"""

    def set_below_min_value(field, data):
        setattr(data, field, min_value - 1)
        data.save()

    assert_validate("min_value", field, to_data, set_below_min_value, entry_name)


def assert_validate_non_empty(field, to_data, entry_name="protocol"):
    """check validation for non empty list by removing all leaves"""

    def delete_all_leaves(field, data):
        getattr(data, field).all().delete()

    assert_validate("required", field, to_data, delete_all_leaves, entry_name)


def assert_validate(error, field, to_data, invalidate, entry_name="protocol"):
    """general validation check"""

    assert getattr(to_data().version, entry_name).publish()

    data = to_data()
    entry = getattr(data.version, entry_name)
    data, field, key = _get_nested_data(field, data)
    invalidate(field, data)
    with pytest.raises(ValidationError) as info:
        entry.publish()
    assert error_codes(info.value) == {
        key: [error],
    }


def _get_nested_data(field, data):
    """traverse nested data structures to make changes on first instance on
    lowest level property
    """

    fields = field.split("__")
    if len(fields) == 0:
        assert False, "{} is not a valid field".format(field)
    key = fields[0]
    for index, nested_field in enumerate(fields[0:-1]):
        data = getattr(data, nested_field).first()
        key = "{}/{}/{}".format(key, data.pk, fields[index + 1])
    if len(fields) > 1:
        key = "/{}/".format(key)
    return data, fields[-1], key
