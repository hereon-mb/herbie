from dataclasses import dataclass
from datetime import datetime
from typing import Any, Optional

import pytest
from rdflib import DCTERMS, PROV, RDF, RDFS, Dataset, Graph, Literal, URIRef

from ..namespace._HERBIE import HERBIE
from ..namespaces import Namespaces
from .rdf_document_dataset import (
    AuthorDecorator,
    RDFDocumentDecorator,
    RDFDocumentVersionDecorator,
)


def test__author_decorator__iri(author):
    assert AuthorDecorator(author).iri == URIRef("http://testserver/authors/1/")


def test__author_decorator__graph(author):
    graph = AuthorDecorator(author).graph
    assert len(graph) == 2
    assert (URIRef("http://testserver/authors/1/"), RDF.type, HERBIE.Author) in graph
    assert (URIRef("http://testserver/authors/1/"), RDFS.label, Literal("Alice Wonderland")) in graph


def test__rdf_document_version_decorator__graph(rdf_document_version):
    graph = RDFDocumentVersionDecorator(rdf_document_version).graph
    assert len(graph) == 14
    iri = URIRef("http://testserver/datasets/uuid/draft/")
    iri_entered = URIRef("http://testserver/datasets/uuid/draft/entered/")
    iri_generated = URIRef("http://testserver/datasets/uuid/draft/generated/")
    iri_persisted = URIRef("http://testserver/datasets/uuid/draft/persisted/")
    assert (iri, RDF.type, HERBIE.DocumentDraft) in graph
    assert (iri, DCTERMS.modified, Literal(datetime(2024, 2, 12, 16, 3))) in graph
    assert (iri_entered, DCTERMS.modified, Literal(datetime(2024, 2, 12, 16, 3))) in graph
    assert (iri_generated, DCTERMS.modified, Literal(datetime(2024, 2, 12, 16, 3))) in graph
    assert (iri_persisted, DCTERMS.modified, Literal(datetime(2024, 2, 12, 16, 3))) in graph
    assert (iri, PROV.generatedAtTime, Literal(datetime(2024, 2, 12, 16, 3))) in graph
    assert (iri, PROV.hadMember, iri_entered) in graph
    assert (iri, PROV.hadMember, iri_generated) in graph
    assert (iri, PROV.hadMember, iri_persisted) in graph
    assert (iri, PROV.specializationOf, URIRef("http://testserver/datasets/uuid/")) in graph
    assert (iri, PROV.wasAttributedTo, URIRef("http://testserver/authors/1/")) in graph
    assert (URIRef("http://testserver/authors/1/"), RDF.type, HERBIE.Author) in graph
    assert (URIRef("http://testserver/authors/1/"), RDFS.label, Literal("Alice Wonderland")) in graph
    assert (URIRef("http://example.org/data/"), RDFS.label, Literal("data")) in graph


def test__rdf_document_version_decorator__graph_entered(rdf_document_version):
    graph = RDFDocumentVersionDecorator(rdf_document_version).graph_entered
    assert len(graph) == 2
    iri_entered = URIRef("http://testserver/datasets/uuid/draft/entered/")
    assert (iri_entered, DCTERMS.modified, Literal(datetime(2024, 2, 12, 16, 3))) in graph
    assert (URIRef("http://example.org/data/"), RDFS.label, Literal("data")) in graph


def test__rdf_document_version_decorator__graph_persisted(rdf_document_version):
    graph = RDFDocumentVersionDecorator(rdf_document_version).graph_persisted
    assert len(graph) == 1
    iri_persisted = URIRef("http://testserver/datasets/uuid/draft/persisted/")
    assert (iri_persisted, DCTERMS.modified, Literal(datetime(2024, 2, 12, 16, 3))) in graph


def test__rdf_document_version_decorator__graph_generated(rdf_document_version):
    graph = RDFDocumentVersionDecorator(rdf_document_version).graph_generated
    assert len(graph) == 1
    iri_generated = URIRef("http://testserver/datasets/uuid/draft/generated/")
    assert (iri_generated, DCTERMS.modified, Literal(datetime(2024, 2, 12, 16, 3))) in graph


def test__rdf_document_version_decorator__add_graphs(rdf_document_version):
    dataset = Dataset()
    RDFDocumentVersionDecorator(rdf_document_version).add_graphs(dataset)
    graph_iris = set([context.identifier for context in dataset.contexts()])
    assert len(list(dataset.contexts())) == 5
    assert URIRef("urn:x-rdflib:default") in graph_iris
    assert URIRef("http://testserver/datasets/uuid/draft/") in graph_iris
    assert URIRef("http://testserver/datasets/uuid/draft/entered/") in graph_iris
    assert URIRef("http://testserver/datasets/uuid/draft/persisted/") in graph_iris
    assert URIRef("http://testserver/datasets/uuid/draft/generated/") in graph_iris


def test__rdf_document_decorator__graph(rdf_document):
    graph = RDFDocumentDecorator(rdf_document).graph
    assert len(graph) == 19
    iri = URIRef("http://testserver/datasets/uuid/")
    iri_draft = URIRef("http://testserver/datasets/uuid/draft/")
    iri_entered = URIRef("http://testserver/datasets/uuid/draft/entered/")
    iri_generated = URIRef("http://testserver/datasets/uuid/draft/generated/")
    iri_persisted = URIRef("http://testserver/datasets/uuid/draft/persisted/")
    assert (iri, RDF.type, HERBIE.Document) in graph
    assert (iri, DCTERMS.created, Literal(datetime(2024, 2, 12, 16, 3))) in graph
    assert (iri, DCTERMS.modified, Literal(datetime(2024, 2, 12, 16, 3))) in graph
    assert (iri, PROV.wasAttributedTo, URIRef("http://testserver/authors/1/")) in graph
    assert (iri_draft, RDF.type, HERBIE.DocumentDraft) in graph
    assert (iri_draft, DCTERMS.modified, Literal(datetime(2024, 2, 12, 16, 3))) in graph
    assert (iri_entered, DCTERMS.modified, Literal(datetime(2024, 2, 12, 16, 3))) in graph
    assert (iri_generated, DCTERMS.modified, Literal(datetime(2024, 2, 12, 16, 3))) in graph
    assert (iri_persisted, DCTERMS.modified, Literal(datetime(2024, 2, 12, 16, 3))) in graph
    assert (iri_draft, PROV.generatedAtTime, Literal(datetime(2024, 2, 12, 16, 3))) in graph
    assert (iri_draft, PROV.hadMember, iri_entered) in graph
    assert (iri_draft, PROV.hadMember, iri_generated) in graph
    assert (iri_draft, PROV.hadMember, iri_persisted) in graph
    assert (iri_draft, PROV.specializationOf, URIRef("http://testserver/datasets/uuid/")) in graph
    assert (iri_draft, PROV.wasAttributedTo, URIRef("http://testserver/authors/1/")) in graph
    assert (URIRef("http://testserver/authors/1/"), RDF.type, HERBIE.Author) in graph
    assert (URIRef("http://testserver/authors/1/"), RDFS.label, Literal("Alice Wonderland")) in graph


def test__rdf_document_decorator__dataset(rdf_document):
    dataset = RDFDocumentDecorator(rdf_document).dataset
    graph_iris = set([context.identifier for context in dataset.contexts()])
    assert len(list(dataset.contexts())) == 7
    assert URIRef("urn:x-rdflib:default") in graph_iris
    assert URIRef("http://testserver/datasets/uuid/") in graph_iris
    assert URIRef("http://testserver/datasets/uuid/access/") in graph_iris
    assert URIRef("http://testserver/datasets/uuid/draft/") in graph_iris
    assert URIRef("http://testserver/datasets/uuid/draft/entered/") in graph_iris
    assert URIRef("http://testserver/datasets/uuid/draft/persisted/") in graph_iris
    assert URIRef("http://testserver/datasets/uuid/draft/generated/") in graph_iris


@dataclass
class Author:
    pk: int

    def add_to_graph(self, graph: Graph):
        graph.add((Namespaces().authors(self.pk), RDF.type, HERBIE.Author))
        graph.add((Namespaces().authors(self.pk), RDFS.label, Literal("Alice Wonderland")))


@dataclass
class RDFDocumentVersions:
    def all(self):
        return self

    def published(self):
        return []


@dataclass
class RDFDocument:
    uuid: str
    created_by: Author
    created_at: datetime
    updated_at: datetime
    graph_access: Graph
    version_draft: Optional["RDFDocumentVersion"]
    rdf_document_versions: RDFDocumentVersions
    versions_published: Any
    version_published: Any


@dataclass
class IriAliases:
    def all(self):
        return []


@dataclass
class Parts:
    def all(self):
        return []


@dataclass
class RDFDocumentVersion:
    rdf_document: RDFDocument
    has_part: Parts
    draft: bool
    version_index: int
    created_by: Author
    created_at: datetime
    updated_at: datetime
    deleted_at: Optional[datetime]
    graph_persisted_updated_at: datetime
    graph_generated_updated_at: datetime
    valid_from: Optional[datetime]
    valid_to: Optional[datetime]
    iri_aliases: IriAliases
    graph_entered: Graph
    graph_persisted: Graph
    graph_generated: Graph


@pytest.fixture
def author():
    return Author(1)


@pytest.fixture
def rdf_document_version():
    now = datetime(2024, 2, 12, 16, 3)
    graph = Graph()
    graph.add((URIRef("http://example.org/data/"), RDFS.label, Literal("data")))

    return RDFDocumentVersion(
        rdf_document=RDFDocument(
            uuid="uuid",
            created_by=Author(1),
            created_at=now,
            updated_at=now,
            graph_access=Graph(),
            version_draft=None,
            rdf_document_versions=RDFDocumentVersions(),
            versions_published=[],
            version_published=None,
        ),
        has_part=Parts(),
        draft=True,
        version_index=0,
        created_by=Author(1),
        created_at=now,
        updated_at=now,
        deleted_at=None,
        graph_persisted_updated_at=now,
        graph_generated_updated_at=now,
        valid_from=None,
        valid_to=None,
        iri_aliases=IriAliases(),
        graph_entered=graph.serialize(format="json-ld"),
        graph_persisted=Graph().serialize(format="json-ld"),
        graph_generated=Graph().serialize(format="json-ld"),
    )


@pytest.fixture
def rdf_document(rdf_document_version):
    now = datetime(2024, 2, 12, 16, 3)

    return RDFDocument(
        uuid="uuid",
        created_by=Author(1),
        created_at=now,
        updated_at=now,
        graph_access=Graph().serialize(format="json-ld"),
        version_draft=rdf_document_version,
        rdf_document_versions=RDFDocumentVersions(),
        versions_published=[],
        version_published=None,
    )
