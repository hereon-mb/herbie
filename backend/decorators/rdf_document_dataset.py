from functools import cached_property
from typing import List, Optional

from django.conf import settings
from pyshacl.rdfutil import clone_graph
from rdflib import (
    DCTERMS,
    OWL,
    PROV,
    RDF,
    RDFS,
    SH,
    Dataset,
    Graph,
    Literal,
    Namespace,
    URIRef,
)

from ..models.author import Author
from ..models.rdf_document import RDFDocument, RDFDocumentVersion
from ..namespace._HASH import HASH
from ..namespace._HERBIE import HERBIE
from ..namespaces import Namespaces

DASH = Namespace("http://datashapes.org/dash#")
SCHEMA = Namespace("http://schema.org/")


class AuthorDecorator:
    author: Author

    def __init__(self, author: Author):
        self.author = author

    @property
    def iri(self):
        return Namespaces().authors(self.author.pk)

    @cached_property
    def graph(self):
        graph = Graph()
        self.author.add_to_graph(graph)
        return graph


class RDFDocumentVersionDecorator:
    _rdf_document_version: RDFDocumentVersion

    _graph_entered: Graph
    _graph_generated: Graph
    _graph_persisted: Graph

    def __init__(self, rdf_document_version: RDFDocumentVersion):
        self._rdf_document_version = rdf_document_version
        self._graph_entered = Graph().parse(data=rdf_document_version.graph_entered, format="json-ld")
        self._graph_generated = Graph().parse(data=rdf_document_version.graph_generated, format="json-ld")
        self._graph_persisted = Graph().parse(data=rdf_document_version.graph_persisted, format="json-ld")

    @cached_property
    def iri(self):
        if self._draft:
            return Namespaces().datasets_draft(self._uuid)
        else:
            return Namespaces().datasets_version(self._uuid, self._version_index + 1)

    @cached_property
    def iri_version_previous(self):
        if self._draft:
            return None
        elif self._version_index > 0:
            return Namespaces().datasets_version(self._uuid, self._version_index)
        else:
            return None

    @cached_property
    def iri_entered(self):
        if self._draft:
            return Namespaces().datasets_draft_entered(self._uuid)
        else:
            return Namespaces().datasets_version_entered(self._uuid, self._version_index + 1)

    @cached_property
    def iri_persisted(self):
        if self._draft:
            return Namespaces().datasets_draft_persisted(self._uuid)
        else:
            return Namespaces().datasets_version_persisted(self._uuid, self._version_index + 1)

    @cached_property
    def iri_generated(self):
        if self._draft:
            return Namespaces().datasets_draft_generated(self._uuid)
        else:
            return Namespaces().datasets_version_generated(self._uuid, self._version_index + 1)

    @cached_property
    def iri_publication(self):
        if self._draft:
            return None
        else:
            return Namespaces().datasets_version(self._uuid, self._version_index + 1) + "publication/"

    @cached_property
    def graph(self):
        graph = Graph()

        clone_graph(self.graph_data, graph)

        self.add_triples_meta(graph)

        self._bind_namespaces(graph)
        graph.bind("dash", DASH)
        graph.bind("hash", HASH)

        return graph

    @cached_property
    def graph_data(self):
        graph = Graph()

        clone_graph(self._graph_entered, graph)
        clone_graph(self._graph_persisted, graph)
        clone_graph(self._graph_generated, graph)

        return graph

    @cached_property
    def graph_entered(self):
        graph_entered = Graph()

        clone_graph(self._graph_entered, graph_entered)

        graph_entered.add((self.iri_entered, DCTERMS.modified, Literal(self._updated_at)))

        self._bind_namespaces(graph_entered)
        graph_entered.bind("dash", DASH)
        graph_entered.bind("hash", HASH)

        return graph_entered

    @cached_property
    def graph_persisted(self):
        graph_persisted = Graph()

        clone_graph(self._graph_persisted, graph_persisted)

        graph_persisted.add((self.iri_persisted, DCTERMS.modified, Literal(self._graph_persisted_updated_at)))

        self._bind_namespaces(graph_persisted)
        graph_persisted.bind("dash", DASH)
        graph_persisted.bind("hash", HASH)

        return graph_persisted

    @cached_property
    def graph_generated(self):
        graph_generated = Graph()

        clone_graph(self._graph_generated, graph_generated)

        graph_generated.add((self.iri_generated, DCTERMS.modified, Literal(self._graph_generated_updated_at)))

        self._bind_namespaces(graph_generated)
        graph_generated.bind("dash", DASH)
        graph_generated.bind("hash", HASH)

        return graph_generated

    def add_graphs(self, dataset):
        graph = dataset.graph(self.iri)

        graph_entered = dataset.graph(self.iri_entered)
        graph_persisted = dataset.graph(self.iri_persisted)
        graph_generated = dataset.graph(self.iri_generated)

        clone_graph(self.graph, graph)
        clone_graph(self.graph_entered, graph_entered)
        clone_graph(self.graph_persisted, graph_persisted)
        clone_graph(self.graph_generated, graph_generated)

        self._bind_namespaces(dataset)

    def add_triples_meta(self, graph):
        iri_document = Namespaces().datasets(self._uuid)

        graph.add((self.iri, PROV.wasAttributedTo, self._created_by.iri))
        clone_graph(self._created_by.graph, graph)

        graph.add((self.iri, PROV.specializationOf, iri_document))

        if self._valid_to is not None:
            graph.add((self.iri, PROV.invalidatedAtTime, Literal(self._valid_to)))
        elif self._deleted_at is not None:
            graph.add((self.iri, PROV.invalidatedAtTime, Literal(self._deleted_at)))

        if self.iri_version_previous is not None:
            graph.add((self.iri, PROV.wasRevisionOf, self.iri_version_previous))

        if self._draft:
            graph.add((self.iri, RDF.type, HERBIE.DocumentDraft))

        else:
            graph.add((self.iri, RDF.type, HERBIE.DocumentVersion))

            self._add_triples_publication(graph)

        graph.add((self.iri, DCTERMS.modified, Literal(self._updated_at)))
        graph.add((self.iri_entered, DCTERMS.modified, Literal(self._updated_at)))
        graph.add((self.iri_persisted, DCTERMS.modified, Literal(self._graph_persisted_updated_at)))
        graph.add((self.iri_generated, DCTERMS.modified, Literal(self._graph_generated_updated_at)))

        graph.add((self.iri, PROV.generatedAtTime, Literal(self._created_at)))
        graph.add((self.iri, PROV.hadMember, self.iri_entered))
        graph.add((self.iri, PROV.hadMember, self.iri_persisted))
        graph.add((self.iri, PROV.hadMember, self.iri_generated))

        for iri_alias in self._iri_aliases:
            graph.add((self.iri, DCTERMS.source, iri_alias))

        for _, _, rdfs_label in self.graph_data.triples((self.iri, RDFS.label, None)):
            graph.add((self.iri, RDFS.label, rdfs_label))
        for _, _, rdfs_comment in self.graph_data.triples((self.iri, RDFS.comment, None)):
            graph.add((self.iri, RDFS.comment, rdfs_comment))
        if dcterms_conforms_to := self.graph_data.value(self.iri, DCTERMS.conformsTo, None):
            graph.add((self.iri, DCTERMS.conformsTo, dcterms_conforms_to))

        for ontology, _, _ in self.graph_data.triples((None, RDF.type, OWL.Ontology)):
            graph.add((ontology, RDF.type, OWL.Ontology))
            graph.add((ontology, RDFS.isDefinedBy, self.iri))

        for node_shape, _, _ in self.graph_data.triples((None, RDF.type, SH.NodeShape)):
            if (node_shape, HASH.documentRoot, Literal(True)) in self.graph_data:
                graph.add((node_shape, RDF.type, SH.NodeShape))
                graph.add((node_shape, RDFS.isDefinedBy, self.iri))

        for _, _, part in self.graph_data.triples((self.iri, SCHEMA.hasPart, None)):
            graph.add((self.iri, SCHEMA.hasPart, part))

        self._bind_namespaces(graph)

    def _add_triples_publication(self, graph):
        graph.add((self.iri_publication, RDF.type, HERBIE.DocumentPublication))
        graph.add((self.iri_publication, PROV.startedAtTime, Literal(self._created_at)))
        graph.add((self.iri_publication, PROV.endedAtTime, Literal(self._valid_from)))
        if self.iri_version_previous is not None:
            graph.add((self.iri_publication, PROV.used, self.iri_version_previous))
        graph.add((self.iri_publication, PROV.generated, self.iri))
        graph.add((self.iri_publication, PROV.wasStartedBy, self._created_by.iri))
        graph.add((self.iri_publication, PROV.wasEndedBy, self._created_by.iri))

    def _bind_namespaces(self, graph):
        graph.bind(f"data_{self._version_index + 1}", f"{self.iri}#")
        graph.bind(f"version_{self._version_index + 1}", f"{self.iri}")
        graph.bind(f"version_{self._version_index + 1}_publication", f"{self.iri}publication/")
        graph.bind(f"version_{self._version_index + 1}_entered", f"{self.iri}entered/")
        graph.bind(f"version_{self._version_index + 1}_persisted", f"{self.iri}persisted/")
        graph.bind(f"version_{self._version_index + 1}_generated", f"{self.iri}generated/")
        # FIXME
        # self._bind_namespace_for_conforms_to(graph)

    # def _bind_namespace_for_conforms_to(self, graph):
    #     if self.graph_shacl and self.conforms_to:
    #         prefix = self.graph_shacl.value(self.conforms_to, VANN.preferredNamespacePrefix, None, "ont")
    #         namespace = URIRef(self.conforms_to + "#")
    #         graph.bind(prefix, namespace)

    @property
    def _uuid(self):
        return self._rdf_document_version.rdf_document.uuid

    @property
    def _version_index(self):
        return self._rdf_document_version.version_index

    @property
    def _draft(self):
        return self._rdf_document_version.draft

    @property
    def _created_at(self):
        return self._rdf_document_version.created_at

    @property
    def _updated_at(self):
        return self._rdf_document_version.updated_at

    @property
    def _graph_persisted_updated_at(self):
        return self._rdf_document_version.graph_persisted_updated_at

    @property
    def _graph_generated_updated_at(self):
        return self._rdf_document_version.graph_generated_updated_at

    @property
    def _valid_to(self):
        return self._rdf_document_version.valid_to

    @property
    def _valid_from(self):
        return self._rdf_document_version.valid_from

    @property
    def _deleted_at(self):
        return self._rdf_document_version.deleted_at

    @cached_property
    def _created_by(self):
        return AuthorDecorator(self._rdf_document_version.created_by)

    @cached_property
    def _iri_aliases(self):
        return [URIRef(iri_alias.iri) for iri_alias in self._rdf_document_version.iri_aliases.all()]


class RDFDocumentDecorator:
    _rdf_document: RDFDocument

    _graph_access: Graph

    version_draft: Optional[RDFDocumentVersionDecorator]
    _versions_published: List[RDFDocumentVersionDecorator]

    def __init__(self, rdf_document: RDFDocument):
        self._rdf_document = rdf_document
        self._graph_access = Graph().parse(data=rdf_document.graph_access, format="json-ld")

        if rdf_document.version_draft:
            self.version_draft = RDFDocumentVersionDecorator(rdf_document.version_draft)
        else:
            self.version_draft = None

        self._versions_published = [
            RDFDocumentVersionDecorator(version_published) for version_published in rdf_document.versions_published
        ]

    @property
    def version_published(self):
        if len(self._versions_published) > 0:
            return self._versions_published[-1]
        else:
            return None

    @property
    def version_deprecated(self):
        if len(self._versions_published) > 1:
            return self._versions_published[-2]
        else:
            return None

    @cached_property
    def iri(self):
        return Namespaces().datasets(self._uuid)

    @cached_property
    def iri_access(self):
        return Namespaces().datasets_access(self._uuid)

    @cached_property
    def iri_workspace(self):
        if hasattr(self._rdf_document, "workspace"):
            return Namespaces().workspaces(self._rdf_document.workspace.uuid)
        else:
            return None

    @cached_property
    def graph(self):
        graph = Graph()
        self._add_triples_meta(graph)
        return graph

    @cached_property
    def dataset(self):
        dataset = Dataset()

        graph = dataset.graph(self.iri)
        self._add_triples_meta(graph)

        graph_access = dataset.graph(self.iri_access)
        clone_graph(self._graph_access, graph_access)

        for version_published in self._versions_published:
            version_published.add_graphs(dataset)

        if self.version_draft is not None:
            self.version_draft.add_graphs(dataset)

        dataset.bind("dash", DASH)
        dataset.bind("hash", HASH)

        return dataset

    def _add_triples_meta(self, graph: Graph):
        iri_document = Namespaces().datasets(self._uuid)
        iri_document_access = Namespaces().datasets_access(self._uuid)

        clone_graph(self._created_by.graph, graph)

        graph.add((iri_document, RDF.type, HERBIE.Document))
        if self.iri_workspace:
            graph.add((iri_document, HERBIE.workspace, self.iri_workspace))
        graph.add((iri_document, PROV.wasAttributedTo, self._created_by.iri))
        graph.add((iri_document, DCTERMS.created, Literal(self._created_at)))
        graph.add((iri_document, DCTERMS.modified, Literal(self._updated_at)))
        graph.add((iri_document, PROV.hadMember, iri_document_access))

        if self.version_draft is not None:
            self.version_draft.add_triples_meta(graph)

        for version_published in self._versions_published:
            version_published.add_triples_meta(graph)

        if self.version_published is not None:
            graph.add((iri_document, HERBIE.latestVersionPublished, URIRef(self.version_published.iri)))

        graph.add((iri_document, HERBIE.hasPrefix, URIRef(settings.IRI_PREFIX)))

    @property
    def _uuid(self):
        return self._rdf_document.uuid

    @property
    def _created_at(self):
        return self._rdf_document.created_at

    @property
    def _updated_at(self):
        return self._rdf_document.updated_at

    @cached_property
    def _created_by(self):
        return AuthorDecorator(self._rdf_document.created_by)
