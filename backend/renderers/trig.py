# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from rdflib import Graph
from rest_framework import renderers

from .error_details import error_details_to_graph, get_error_details


class TriGRenderer(renderers.BaseRenderer):
    media_type = "application/trig"
    format = "trig"
    charset = "utf-8"

    def render(self, data, accepted_media_type=None, renderer_context=None):
        if (error_details := get_error_details(data)) is not None:
            return error_details_to_graph(error_details).serialize(format="trig")
        elif isinstance(data, Graph):
            return data.serialize(format="trig")
        else:
            return ""
