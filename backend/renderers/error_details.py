from typing import List, Optional

from rdflib import RDF, SH, BNode, Graph, Literal
from rest_framework.exceptions import ErrorDetail


def get_error_details(data) -> Optional[List[ErrorDetail]]:
    if isinstance(data, list):
        for d in data:
            if not isinstance(d, ErrorDetail):
                return None
        return data
    elif isinstance(data, dict) and "detail" in data and isinstance(data["detail"], ErrorDetail):
        return [data["detail"]]
    else:
        return None


def error_details_to_graph(error_details):
    graph = Graph()

    validation_report = BNode()
    graph.add((validation_report, RDF.type, SH.ValidationReport))
    graph.add((validation_report, SH.conforms, Literal(False)))

    for error_detail in error_details:
        validation_result = BNode()
        graph.add((validation_report, SH.result, validation_result))
        graph.add((validation_result, RDF.type, SH.ValidationResult))
        graph.add((validation_result, SH.resultSeverity, SH.Violation))
        graph.add((validation_result, SH.resultMessage, Literal(str(error_detail))))

    return graph
