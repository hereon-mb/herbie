# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from django.conf import settings
from django.db import transaction
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_control
from pyshacl.errors import ReportableRuntimeError
from rdflib import RDF, Dataset, Graph, URIRef
from rest_framework import status, viewsets
from rest_framework.decorators import (
    action,
    api_view,
    permission_classes,
    renderer_classes,
)
from rest_framework.exceptions import NotFound, ValidationError
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response

from ..access_policies.datasets import DatasetsAccessPolicy
from ..access_policies.workspace import WorkspaceAccessPolicy
from ..models.cache_reset import last_cache_reset_at
from ..models.rdf_document import RDFDocument
from ..models.workspace import Workspace
from ..namespace._HERBIE import HERBIE
from ..namespaces import Namespaces
from ..paginator import Paginator
from ..parsers.json_ld import JsonLdParser
from ..parsers.n_triples import NTriplesParser
from ..parsers.turtle import TurtleParser
from ..queries import construct_graph, construct_support
from ..renderers.json_ld import JsonLdRenderer
from ..renderers.n_triples import NTriplesRenderer
from ..renderers.trig import TriGRenderer
from ..renderers.turtle import TurtleRenderer
from ..services.rdf_document import RDFDocumentService
from ..services.shacl import ShaclError
from ..services.triple_store import TripleStore
from ..sparql import Filter, GraphPattern, Select, Var
from ..util import fix_protocol
from .decorators import condition, condition_func


@transaction.atomic
def rdf_document_last_modified(request, uuid, *args, **kwargs):
    if (rdf_document := _get_object(request, uuid, False)) is not None:
        return max(filter(None, [rdf_document.updated_at, last_cache_reset_at()]))
    else:
        return None


@transaction.atomic
def rdf_document_draft_last_modified(request, uuid, *args, **kwargs):
    if (rdf_document := _get_object(request, uuid, False)) is not None and (draft := rdf_document.version_draft) is not None:
        return max(filter(None, [draft.updated_at, last_cache_reset_at()]))
    else:
        return None


class Datasets(viewsets.GenericViewSet):
    schema = None
    permission_classes = (DatasetsAccessPolicy,)
    parser_classes = [TurtleParser, NTriplesParser, JsonLdParser, JSONParser]
    renderer_classes = [TemplateHTMLRenderer, TurtleRenderer, NTriplesRenderer, JsonLdRenderer, TriGRenderer]  # type: ignore
    lookup_field = "uuid"

    @property
    def access_policy(self):
        return self.permission_classes[0]

    def get_object(self):
        if not hasattr(self, "_cached_object"):
            uuid = self.kwargs["uuid"]
            self._cached_object = _get_object(self.request, uuid)
        return self._cached_object

    def get_queryset(self):
        queryset = RDFDocument.objects.all()
        queryset = DatasetsAccessPolicy.scope_queryset(self.request.user.author, queryset)
        return queryset

    def get_workspace(self):
        if not hasattr(self, "_cached_workspace"):
            if "uuid" in self.kwargs:
                rdf_document = self.get_object()
                self._cached_workspace = rdf_document.workspace
            elif "workspace" in self.request.query_params:
                uuid = self.request.query_params.get("workspace")
                workspaces = Workspace.objects.all()
                workspaces = WorkspaceAccessPolicy.scope_queryset(self.request.user.author, workspaces)
                workspaces = workspaces.with_prefetches()
                self._cached_workspace = workspaces.get(uuid=uuid)
            else:
                raise NotFound()
        return self._cached_workspace

    # RETRIEVE

    @transaction.atomic
    @method_decorator(cache_control(no_cache=True, private=True))
    @condition(last_modified_func=rdf_document_last_modified)
    def retrieve(self, request, *args, **kwargs):
        if request.accepted_renderer.media_type == "text/html":
            return render_elm(request)

        rdf_document = self.get_object()
        return _response_full(
            request,
            rdf_document,
            rdf_document.rdf_document_versions.all().published().count(),
            rdf_document.version_draft is not None,
        )

    @transaction.atomic
    @method_decorator(cache_control(no_cache=True, private=True))
    @condition(last_modified_func=rdf_document_last_modified)
    @action(detail=True)
    def access(self, request, uuid=None):
        if request.accepted_renderer.media_type == "text/html":
            return render_elm(request)
        return _response_access(request, self.get_object().uuid)

    @transaction.atomic
    @method_decorator(cache_control(no_cache=True, private=True))
    @condition(last_modified_func=rdf_document_last_modified)
    @action(detail=True, url_path="version/latest")
    def version_latest(self, request, uuid=None):
        if request.accepted_renderer.media_type == "text/html":
            return render_elm(request)

        rdf_document = self.get_object()
        if (published := rdf_document.version_published) is not None:
            return _response_version(request, rdf_document.uuid, published.version_index + 1, published)
        else:
            raise ValidationError("a published version does not exist for this document, publish it first")

    @transaction.atomic
    @method_decorator(cache_control(no_cache=True, private=True))
    @condition(last_modified_func=rdf_document_draft_last_modified)
    @action(detail=True)
    def draft(self, request, uuid=None):
        if request.accepted_renderer.media_type == "text/html":
            return render_elm(request)

        if (draft := self.get_object().version_draft) is not None:
            return _response_draft(request, uuid, draft)
        else:
            raise ValidationError("a draft does not exist for this document, create one first")

    @transaction.atomic
    @method_decorator(cache_control(no_cache=True, private=True))
    @condition(last_modified_func=rdf_document_draft_last_modified)
    @action(detail=True, url_path="draft/entered")
    def draft_entered(self, request, uuid=None):
        rdf_document = self.get_object()
        if rdf_document.version_draft is not None:
            return _response_graph(request, Namespaces().datasets_draft_entered(rdf_document.uuid))
        else:
            raise ValidationError("a draft does not exist for this document, create one first")

    @transaction.atomic
    @method_decorator(cache_control(no_cache=True, private=True))
    @condition(last_modified_func=rdf_document_draft_last_modified)
    @action(detail=True, url_path="draft/persisted")
    def draft_persisted(self, request, uuid=None):
        rdf_document = self.get_object()
        if rdf_document.version_draft is not None:
            return _response_graph(request, Namespaces().datasets_draft_persisted(rdf_document.uuid))
        else:
            raise ValidationError("a draft does not exist for this document, create one first")

    @transaction.atomic
    @method_decorator(cache_control(no_cache=True, private=True))
    @condition(last_modified_func=rdf_document_draft_last_modified)
    @action(detail=True, url_path="draft/generated")
    def draft_generated(self, request, uuid=None):
        rdf_document = self.get_object()
        if rdf_document.version_draft is not None:
            return _response_graph(request, Namespaces().datasets_draft_generated(rdf_document.uuid))
        else:
            raise ValidationError("a draft does not exist for this document, create one first")

    @transaction.atomic
    @method_decorator(cache_control(no_cache=True, private=True))
    @condition(last_modified_func=rdf_document_draft_last_modified)
    @action(detail=True, url_path="draft/support")
    def draft_support(self, request, uuid=None):
        rdf_document = self.get_object()
        if rdf_document.version_draft is not None:
            return _response_graph_draft_support(request, rdf_document)
        else:
            raise ValidationError("a draft does not exist for this document, create one first")

    # LIST

    @transaction.atomic
    def list(self, request):
        if request.accepted_renderer.media_type == "text/html":
            return render_elm(request)

        url = self.reverse_action("list")
        var_document = "document"
        page = Paginator(
            url=url,
            vars_selected=[var_document],
            graph_triple_patterns=[],
            cursor_prev=request.query_params.get("prev"),
            cursor_next=request.query_params.get("next"),
            page_size=int(request.query_params.get("page_size", "5")),
            var_document=var_document,
        ).page()
        return Response(
            self._response_data_dict(page.documents, url),
            headers=page.headers,
        )

    def _response_data_dict(self, queryset, uri):
        if self.request.accepted_renderer.format == "trig":
            dataset = Dataset()
            graph = dataset.graph(fix_protocol(URIRef(uri)))
            for rdf_document in queryset:
                uri_base = rdf_document["document"]["value"]
                uri_ref_document = URIRef(uri_base)
                graph.add((uri_ref_document, RDF.type, HERBIE.Document))
            return dataset
        else:
            graph = Graph()
            for rdf_document in queryset:
                uri_base = rdf_document["document"]["value"]
                uri_ref_document = URIRef(uri_base)
                graph.add((uri_ref_document, RDF.type, HERBIE.Document))
            return graph

    # CREATE

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        result = self._create_rdf_document()
        result.sync_rdf_store()
        return Response(
            status=status.HTTP_201_CREATED,
            headers={"Location": fix_protocol(self.reverse_action("detail", args=[result.rdf_document.uuid]))},
        )

    def _create_rdf_document(self):
        request = self.request
        workspace = self.get_workspace()
        if isinstance(request.data, tuple):
            uuid, graph = request.data
            return RDFDocumentService(request.user.author).create(
                workspace,
                graph,
                request.query_params.get("mint"),
                uuid,
            )
        else:
            return RDFDocumentService(request.user.author).create(
                workspace,
                request.data,
                request.query_params.get("mint"),
            )

    # EDIT

    @transaction.atomic
    @action(detail=True, methods=["post"])
    def edit(self, request, uuid=None):
        result = RDFDocumentService(request.user.author).edit(self.get_object(), request.data)
        result.sync_rdf_store()
        return self._response_204_no_content(result.rdf_document)

    @transaction.atomic
    def destroy(self, request, uuid=None):
        result = RDFDocumentService(request.user.author).delete(self.get_object())
        result.sync_rdf_store()
        return self._response_204_no_content(result.rdf_document)

    @transaction.atomic
    @access.mapping.put
    def access_update(self, request, uuid=None):
        result = RDFDocumentService(request.user.author).update_access(self.get_object(), request.data)
        result.sync_rdf_store()
        return _response(request, result)

    @transaction.atomic
    @draft.mapping.put
    def draft_update(self, request, uuid=None):
        rdf_document = self.get_object()
        if (draft := rdf_document.version_draft) is not None:
            result = RDFDocumentService(request.user.author).update(rdf_document, request.data)
            result.sync_rdf_store()
            return _response_draft(request, uuid, draft)
        else:
            raise ValidationError("a draft does not exist for this document, create one first")

    @transaction.atomic
    @action(detail=True, methods=["post"], url_path="draft/generate")
    def generate(self, request, uuid=None):
        try:
            rdf_document = self.get_object()
            result = RDFDocumentService(request.user.author).generate(rdf_document)
            result.sync_rdf_store()
            if request.accepted_renderer.format == "trig":
                dataset = result.dataset
                iri_draft = Namespaces().datasets_draft(rdf_document.uuid)
                iris_object = [URIRef(binding["object"]["value"]) for binding in _get_objects(iri_draft)["results"]["bindings"]]
                iri_workspace = Namespaces().workspaces(rdf_document.workspace.uuid)
                iri_draft_support = Namespaces().datasets_draft_support(rdf_document.uuid)
                _add_triples_support(iris_object, iri_workspace, dataset.graph(iri_draft_support))
                return Response(dataset)
            else:
                return Response(result.graph)
        except ReportableRuntimeError as error:
            return Response(error.message, status=status.HTTP_400_BAD_REQUEST)
        except ShaclError as error:
            return Response(error.report, status=status.HTTP_400_BAD_REQUEST)

    @transaction.atomic
    @action(detail=True, methods=["post"], url_path="draft/cancel")
    def cancel(self, request, uuid=None):
        result = RDFDocumentService(request.user.author).cancel(self.get_object())
        result.sync_rdf_store()
        return self._response_204_no_content(result.rdf_document)

    @transaction.atomic
    @action(detail=True, methods=["post"], url_path="draft/publish")
    def publish(self, request, uuid=None):
        try:
            result = RDFDocumentService(request.user.author).publish(self.get_object())
            result.sync_rdf_store()
            return self._response_204_no_content(result.rdf_document)
        except ShaclError as error:
            return Response(error.report, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=[], url_path="draft/aliases")
    def aliases(self, request, uuid=None):
        pass

    @transaction.atomic
    @aliases.mapping.post
    def aliases_create(self, request, uuid=None):
        rdf_document = self.get_object()
        if rdf_document.version_draft is not None:
            iri = request.query_params.get("iri")
            result = RDFDocumentService(request.user.author).add_alias(self.get_object(), iri)
            result.sync_rdf_store()
            return _response(request, result)
        else:
            raise ValidationError("a draft does not exist for this document, create one first")

    @transaction.atomic
    @aliases.mapping.delete
    def aliases_delete(self, request, uuid=None):
        rdf_document = self.get_object()
        if rdf_document.version_draft is not None:
            iri = request.query_params.get("iri")
            result = RDFDocumentService(request.user.author).delete_alias(self.get_object(), iri)
            result.sync_rdf_store()
            return _response(request, result)
        else:
            raise ValidationError("a draft does not exist for this document, create one first")

    @property
    def _rdf_document_service(self):
        return RDFDocumentService(self.request.user.author)

    def _response_204_no_content(self, rdf_document=None):
        headers = {}
        if rdf_document is not None:
            headers["Location"] = fix_protocol(
                self.reverse_action(
                    "detail",
                    args=[rdf_document.uuid],
                )
            )
        return Response(status=status.HTTP_204_NO_CONTENT, headers=headers)


@transaction.atomic
@api_view(["GET"])
@permission_classes([IsAuthenticated])
@renderer_classes([TemplateHTMLRenderer, TurtleRenderer, NTriplesRenderer, JsonLdRenderer, TriGRenderer])
@cache_control(no_cache=True, private=True)
@condition_func(last_modified_func=rdf_document_last_modified)
def datasets_version(request, uuid, index):
    if request.accepted_renderer.media_type == "text/html":
        return render_elm(request)

    rdf_document = _get_object(request, uuid)
    versions = rdf_document.rdf_document_versions.all().published()
    if index <= versions.count() and (published := versions[index - 1]) is not None:
        return _response_version(request, uuid, index, published)
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)


@transaction.atomic
@api_view(["GET"])
@permission_classes([IsAuthenticated])
@renderer_classes([TurtleRenderer, NTriplesRenderer, JsonLdRenderer, TriGRenderer])
@cache_control(no_cache=True, private=True)
@condition_func(last_modified_func=rdf_document_last_modified)
def datasets_version_entered(request, uuid, index):
    rdf_document = _get_object(request, uuid)
    versions = rdf_document.rdf_document_versions.all().published()
    if index <= versions.count() and versions[index - 1] is not None:
        return _response_graph(request, Namespaces().datasets_version_entered(uuid, index))
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)


@transaction.atomic
@api_view(["GET"])
@permission_classes([IsAuthenticated])
@renderer_classes([TurtleRenderer, NTriplesRenderer, JsonLdRenderer, TriGRenderer])
@cache_control(no_cache=True, private=True)
@condition_func(last_modified_func=rdf_document_last_modified)
def datasets_version_persisted(request, uuid, index):
    rdf_document = _get_object(request, uuid)
    versions = rdf_document.rdf_document_versions.all().published()
    if index <= versions.count() and versions[index - 1] is not None:
        return _response_graph(request, Namespaces().datasets_version_persisted(uuid, index))
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)


@transaction.atomic
@api_view(["GET"])
@permission_classes([IsAuthenticated])
@renderer_classes([TurtleRenderer, NTriplesRenderer, JsonLdRenderer, TriGRenderer])
@cache_control(no_cache=True, private=True)
@condition_func(last_modified_func=rdf_document_last_modified)
def datasets_version_generated(request, uuid, index):
    rdf_document = _get_object(request, uuid)
    versions = rdf_document.rdf_document_versions.all().published()
    if index <= versions.count() and versions[index - 1] is not None:
        return _response_graph(request, Namespaces().datasets_version_generated(uuid, index))
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)


@transaction.atomic
@api_view(["GET"])
@permission_classes([IsAuthenticated])
@renderer_classes([TurtleRenderer, NTriplesRenderer, JsonLdRenderer, TriGRenderer])
@cache_control(no_cache=True, private=True)
@condition_func(last_modified_func=rdf_document_last_modified)
def datasets_version_support(request, uuid, index):
    rdf_document = _get_object(request, uuid)
    versions = rdf_document.rdf_document_versions.all().published()
    if index <= versions.count() and versions[index - 1] is not None:
        return _response_graph_version_support(request, uuid, index, versions[index - 1])
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)


def _get_object(request, uuid, prefetch=True):
    return RDFDocumentService(request.user.author).get(uuid, prefetch=prefetch)


def _response_full(request, rdf_document, version_count, draft):
    uuid = rdf_document.uuid
    if request.accepted_renderer.format == "trig":
        dataset = Dataset()
        _add_graph(Namespaces().datasets(uuid), dataset)
        if draft:
            _add_graph(Namespaces().datasets_access(uuid), dataset)
            _add_graph(Namespaces().datasets_draft(uuid), dataset)
            _add_graph(Namespaces().datasets_draft_entered(uuid), dataset)
            _add_graph(Namespaces().datasets_draft_persisted(uuid), dataset)
            _add_graph(Namespaces().datasets_draft_generated(uuid), dataset)
            _add_graph_draft_support(uuid, rdf_document.version_draft, dataset)
        for index in range(version_count):
            _add_graph(Namespaces().datasets_version(uuid, index + 1), dataset)
            _add_graph(Namespaces().datasets_version_entered(uuid, index + 1), dataset)
            _add_graph(Namespaces().datasets_version_persisted(uuid, index + 1), dataset)
            _add_graph(Namespaces().datasets_version_generated(uuid, index + 1), dataset)
            _add_graph_version_support(uuid, index + 1, rdf_document.rdf_document_versions.all()[index - 1], dataset)
        return Response(dataset)
    else:
        graph = Graph()
        _add_triples(Namespaces().datasets(uuid), graph)
        return Response(graph)


def _response_draft(request, uuid, draft):
    if request.accepted_renderer.format == "trig":
        dataset = Dataset()

        _add_graph(Namespaces().datasets_draft(uuid), dataset)
        _add_graph(Namespaces().datasets_draft_entered(uuid), dataset)
        _add_graph(Namespaces().datasets_draft_persisted(uuid), dataset)
        _add_graph(Namespaces().datasets_draft_generated(uuid), dataset)
        _add_graph_draft_support(uuid, draft, dataset)

        return Response(dataset)
    else:
        graph = Graph()
        _add_triples(Namespaces().datasets_draft(uuid), graph)
        return Response(graph)


def _response_access(request, uuid):
    if request.accepted_renderer.format == "trig":
        dataset = Dataset()
        _add_graph(Namespaces().datasets_access(uuid), dataset)
        return Response(dataset)
    else:
        graph = Graph()
        _add_triples(Namespaces().datasets_access(uuid), graph)
        return Response(graph)


def _response_version(request, uuid, index, version):
    if request.accepted_renderer.format == "trig":
        dataset = Dataset()

        _add_graph(Namespaces().datasets_version(uuid, index), dataset)
        _add_graph(Namespaces().datasets_version_entered(uuid, index), dataset)
        _add_graph(Namespaces().datasets_version_persisted(uuid, index), dataset)
        _add_graph(Namespaces().datasets_version_generated(uuid, index), dataset)
        _add_graph_version_support(uuid, index, version, dataset)

        return Response(dataset)
    else:
        graph = Graph()
        _add_triples(Namespaces().datasets_version(uuid, index), graph)
        return Response(graph)


def _response_graph(request, iri):
    if request.accepted_renderer.format == "trig":
        dataset = Dataset()
        _add_triples(iri, dataset.graph(iri))
        return Response(dataset)
    else:
        graph = Graph()
        _add_triples(iri, graph)
        return Response(graph)


def _add_graph(iri, dataset):
    _add_triples(iri, dataset.graph(iri))


def _add_triples(iri, graph):
    TripleStore().construct(
        construct_graph(iri),
        graph=graph,
    )


def _response_graph_draft_support(request, rdf_document):
    iri_draft = Namespaces().datasets_draft(rdf_document.uuid)
    iris_object = [URIRef(binding["object"]["value"]) for binding in _get_objects(iri_draft)["results"]["bindings"]]
    iri_workspace = Namespaces().workspaces(rdf_document.workspace.uuid)

    if request.accepted_renderer.format == "trig":
        dataset = Dataset()
        iri_draft_support = Namespaces().datasets_draft_support(rdf_document.uuid)
        _add_triples_support(iris_object, iri_workspace, dataset.graph(iri_draft_support))
        return Response(dataset)
    else:
        graph = Graph()
        _add_triples_support(iris_object, iri_workspace, graph)
        return Response(graph)


def _add_graph_draft_support(uuid, draft, dataset):
    iri_draft = Namespaces().datasets_draft(uuid)
    iris_object = [URIRef(binding["object"]["value"]) for binding in _get_objects(iri_draft)["results"]["bindings"]]
    iri_workspace = Namespaces().workspaces(draft.rdf_document.workspace.uuid)

    iri_draft_support = Namespaces().datasets_draft_support(uuid)
    _add_triples_support(iris_object, iri_workspace, dataset.graph(iri_draft_support))


def _response_graph_version_support(request, uuid, index, version):
    iri_version = Namespaces().datasets_version(uuid, index)
    iris_object = [URIRef(binding["object"]["value"]) for binding in _get_objects(iri_version)["results"]["bindings"]]
    iri_workspace = Namespaces().workspaces(version.rdf_document.workspace.uuid)

    if request.accepted_renderer.format == "trig":
        dataset = Dataset()
        iri_version_support = Namespaces().datasets_version_support(uuid, index)
        _add_triples_support(iris_object, iri_workspace, dataset.graph(iri_version_support))
        return Response(dataset)
    else:
        graph = Graph()
        _add_triples_support(iris_object, iri_workspace, graph)
        return Response(graph)


def _add_graph_version_support(uuid, index, version, dataset):
    iri_version = Namespaces().datasets_version(uuid, index)
    iris_object = [URIRef(binding["object"]["value"]) for binding in _get_objects(iri_version)["results"]["bindings"]]
    iri_workspace = Namespaces().workspaces(version.rdf_document.workspace.uuid)

    iri_version_support = Namespaces().datasets_version_support(uuid, index)
    _add_triples_support(iris_object, iri_workspace, dataset.graph(iri_version_support))


def _get_objects(iri):
    s, p, o = Var("subject"), Var("predicate"), Var("object", randomize=False)
    return TripleStore().query(
        Select(o).where(
            GraphPattern(
                iri,
                [
                    (s, p, o),
                    Filter(f"isIRI({o})"),
                ],
            ),
        ),
    )


def _add_triples_support(iris_object, iri_workspace, graph):
    TripleStore().construct(
        construct_support(iris_object, workspace=iri_workspace),
        graph=graph,
    )


def _response(request, decorated):
    if request.accepted_renderer.format == "trig":
        return Response(decorated.dataset)
    else:
        return Response(decorated.graph)


def render_elm(request):
    sandbox = "true" if settings.SANDBOX else "false"
    useTripleStore = "true" if settings.TRIPLE_STORE_PATH else "false"
    useHelmholtzAai = "true" if settings.HELMHOLTZ_CLIENT_ID and settings.HELMHOLTZ_CLIENT_SECRET else "false"
    hide_user_password_login = "true" if settings.HIDE_USER_PASSWORD_LOGIN else "false"
    use_sparklis = "true" if settings.USE_SPARKLIS else "false"
    allow_uploads = "true" if settings.ALLOW_UPLOADS else "false"
    imprint_url = f'"{settings.IMPRINT_URL}"' if settings.IMPRINT_URL else "null"
    data_protection_url = f'"{settings.DATA_PROTECTION_URL}"' if settings.DATA_PROTECTION_URL else "null"
    accessibility_url = f'"{settings.ACCESSIBILITY_URL}"' if settings.ACCESSIBILITY_URL else "null"

    return render(
        request,
        "frontend/index.html",
        context={
            "sandbox": sandbox,
            "useTripleStore": useTripleStore,
            "useHelmholtzAai": useHelmholtzAai,
            "hideUserPaswordLogin": hide_user_password_login,
            "useSparklis": use_sparklis,
            "allowUploads": allow_uploads,
            "imprintUrl": imprint_url,
            "dataProtectionUrl": data_protection_url,
            "accessibilityUrl": accessibility_url,
        },
    )
