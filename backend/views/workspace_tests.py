# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.


from rest_framework import status

from ..matchers import some_int, some_str
from ..models.workspace import Workspace


def test__list__empty(authenticated_client):
    response = authenticated_client.get("/workspaces/")
    assert response.status_code == status.HTTP_200_OK
    assert len(response.data) == 0


def test__list__non_empty(authenticated_client, author_alice, author_bob, create_workspace):
    create_workspace(author_alice, randomize=True)
    create_workspace(author_bob, randomize=True)

    response = authenticated_client.get("/workspaces/")

    assert response.status_code == status.HTTP_200_OK
    assert len(response.data) == 1


def test__create(request, authenticated_client):
    response = authenticated_client.post(
        "/workspaces/",
        data={
            "slug": "workspace-a",
            "name": "Workspace A",
            "description": "The Workspace A description",
        },
    )
    workspace = Workspace.objects.filter(uuid=response.data["uuid"]).first()
    request.addfinalizer(lambda: workspace.delete())

    data_author_alice = {
        "id": some_int(),
        "url": some_str(),
        "user": {
            "id": some_int(),
            "url": some_str(),
            "username": "alice",
            "email": "alice@example.com",
            "first_name": "Alice",
            "last_name": "Wonderland",
        },
    }

    assert response.status_code == status.HTTP_201_CREATED
    assert response.data == {
        "uuid": some_str(),
        "url": some_str(),
        "created_at": some_str(),
        "updated_at": some_str(),
        "deleted_at": None,
        "created_by": data_author_alice,
        "slug": "workspace-a",
        "name": "Workspace A",
        "description": "The Workspace A description",
        "memberships": [
            {
                "id": some_int(),
                "url": some_str(),
                "created_at": some_str(),
                "updated_at": some_str(),
                "deleted_at": None,
                "created_by": data_author_alice,
                "author": data_author_alice,
                "membership_type": "owner",
            },
        ],
    }
