from urllib import parse


def test__query__all_quads(
    request,
    clear_triple_store,
    author_superuser,
    authenticated_client_for,
    rdf_document__unpublished,
):
    request.addfinalizer(lambda: clear_triple_store())
    rdf_document__unpublished(sync_rdf_store=True)
    authenticated_client = authenticated_client_for(author_superuser)
    query = parse.urlencode(
        {
            "query": """
                SELECT ?g ?s ?p ?o WHERE {
                    GRAPH ?g { ?s ?p ?o }
                }"""
        }
    )
    response = authenticated_client.get(f"/query/?{query}")
    assert response.status_code == 200
