from django.conf import settings
from rdflib import URIRef
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response

from ..services.triple_store import TripleStore
from ..tasks.synchronization import (
    synchronize_authors,
    synchronize_departments,
    synchronize_rdf_documents,
)


@api_view(["POST"])
@permission_classes([IsAdminUser])
def clear_rdf_store(request):
    triple_store = TripleStore()

    result = triple_store.query(
        """
            SELECT ?graph WHERE {
                GRAPH ?graph { ?s ?p ?o }
            }
        """,
    )
    graph_iris = [URIRef(binding["graph"]["value"]) for binding in result["results"]["bindings"] if "graph" in binding]
    for graph_iri in graph_iris:
        if graph_iri.startswith(settings.IRI_PREFIX):
            triple_store.delete_graph(graph_iri)

    return Response(status=status.HTTP_200_OK)


@api_view(["POST"])
@permission_classes([IsAdminUser])
def sync_rdf_store(request):
    synchronize_authors.delay()
    synchronize_departments.delay()
    synchronize_rdf_documents.delay()

    return Response(status=status.HTTP_202_ACCEPTED)


@api_view(["POST"])
@permission_classes([IsAdminUser])
def sync_rdf_store_authors(request):
    synchronize_authors.delay()
    return Response(status=status.HTTP_202_ACCEPTED)


@api_view(["POST"])
@permission_classes([IsAdminUser])
def sync_rdf_store_departments(request):
    synchronize_departments.delay()
    return Response(status=status.HTTP_202_ACCEPTED)
