# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2024 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from functools import cached_property

from rest_framework import parsers, status
from rest_framework.exceptions import ValidationError
from rest_framework.mixins import CreateModelMixin, ListModelMixin, RetrieveModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from ..access_policies.ro_crate_import import RoCrateImportAccessPolicy
from ..access_policies.workspace import WorkspaceAccessPolicy
from ..models.ro_crate_import import RoCrateImport
from ..models.workspace import Workspace
from ..serializers.ro_crate_import import RoCrateImportSerializer
from ..services.ro_crate_import import RoCrateImportService


class RoCrateImportViewSet(
    ListModelMixin,
    RetrieveModelMixin,
    CreateModelMixin,
    GenericViewSet,
):
    serializer_class = RoCrateImportSerializer
    pagination_class = None
    parser_classes = (parsers.FileUploadParser,)
    permission_classes = (RoCrateImportAccessPolicy,)

    @property
    def access_policy(self):
        return self.permission_classes[0]

    def get_queryset(self):
        queryset = RoCrateImport.objects.all()
        queryset = self.access_policy.scope_queryset(self.request, queryset, self.get_workspace())
        queryset = queryset.with_prefetches()
        return queryset

    def get_workspace(self):
        if "workspace" in self.request.query_params:
            uuid = self.request.query_params.get("workspace")
            workspaces = Workspace.objects.all()
            workspaces = WorkspaceAccessPolicy.scope_queryset(self.request.user.author, workspaces)
            workspaces = workspaces.with_prefetches()
            return workspaces.get(uuid=uuid)
        else:
            raise ValidationError(detail="Workspace must be specified.", code="missing_workspace")

    def create(self, request):
        ro_crate_import = self._ro_crate_import_service.create_ro_crate_import(
            self.get_workspace(),
            request.data["file"],
        )
        serializer = self.get_serializer(ro_crate_import)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    @cached_property
    def _ro_crate_import_service(self):
        return RoCrateImportService(self.request)
