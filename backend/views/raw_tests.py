# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

import tempfile
from pathlib import Path

from django.core.files.uploadedfile import SimpleUploadedFile
from rdflib import RDFS, Literal, URIRef
from rest_framework import status

from ..models.raw_file import RawFile


def test__raw__post(request, authenticated_client):
    tmp_file = tempfile.NamedTemporaryFile(suffix=".pdf")
    tmp_file.write(b"content")
    tmp_file.seek(0)

    response = authenticated_client.post(
        "/raw/",
        {
            "file": tmp_file,
        },
        format="multipart",
        headers={"Accept": "application/n-triples"},
    )
    raw_file = RawFile.objects.last()
    request.addfinalizer(lambda: raw_file.delete())

    assert response.status_code == status.HTTP_201_CREATED
    assert (
        URIRef(f"http://testserver/{raw_file.file.name}/"),
        RDFS.label,
        Literal(Path(tmp_file.name).name),
    ) in response.data


def test__graph__get(request, authenticated_client, author_alice):
    raw_file = RawFile.objects.create(
        created_by=author_alice,
        file=SimpleUploadedFile(
            name="manual.pdf",
            content=open("backend/fixtures/manual.pdf", "rb").read(),
            content_type="application/pdf",
        ),
    )
    request.addfinalizer(lambda: raw_file.delete())

    response = authenticated_client.get(f"/{raw_file.file.name}/")
    assert response.status_code == status.HTTP_200_OK
