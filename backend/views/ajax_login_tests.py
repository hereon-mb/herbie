# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.


def test_ajax_login(unauthenticated_client, user_alice):
    response = unauthenticated_client.post(
        "/login/",
        {
            "username": "alice",
            "password": "secret",
        },
        format="multipart",
    )
    assert response.status_code == 201


def test_ajax_login_wrong_username(unauthenticated_client, user_alice):
    response = unauthenticated_client.post(
        "/login/",
        {
            "username": "wrong_alice",
            "password": "secret",
        },
        format="multipart",
    )
    assert response.status_code == 401


def test_ajax_login_wrong_password(unauthenticated_client, user_alice):
    response = unauthenticated_client.post(
        "/login/",
        {
            "username": "alice",
            "password": "wrong_secret",
        },
        format="multipart",
    )
    assert response.status_code == 401
