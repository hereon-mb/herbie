# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_control
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin
from rest_framework.viewsets import GenericViewSet

from ..access_policies.department import DepartmentAccessPolicy
from ..models.cache_reset import last_modified
from ..models.department import Department
from ..serializers.department import DepartmentSerializer
from .decorators import condition


def departments_last_modified(request, *args, **kwargs):
    departments = Department.objects.all()
    departments = DepartmentAccessPolicy.scope_queryset(request, departments)
    return last_modified(departments)


class DepartmentViewSet(
    ListModelMixin,
    RetrieveModelMixin,
    GenericViewSet,
):
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer
    pagination_class = None
    permission_classes = (DepartmentAccessPolicy,)

    @method_decorator(cache_control(no_cache=True, private=True))
    @condition(last_modified_func=departments_last_modified)
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)
