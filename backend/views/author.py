# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_control
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin
from rest_framework.viewsets import GenericViewSet

from ..access_policies.author import AuthorAccessPolicy
from ..models.author import Author
from ..models.cache_reset import last_modified
from ..serializers.author import AuthorSerializer
from .decorators import condition


def authors_last_modified(request, *args, **kwargs):
    authors = Author.objects.all()
    authors = AuthorAccessPolicy.scope_queryset(request, authors)
    return last_modified(authors)


class AuthorViewSet(
    ListModelMixin,
    RetrieveModelMixin,
    GenericViewSet,
):
    queryset = Author.objects.all().select_related("user")
    serializer_class = AuthorSerializer
    pagination_class = None
    permission_classes = (AuthorAccessPolicy,)

    @method_decorator(cache_control(no_cache=True, private=True))
    @condition(last_modified_func=authors_last_modified)
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)
