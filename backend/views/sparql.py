# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from itertools import batched

from django.conf import settings
from django.db import transaction
from django.db.models import Max, Q
from django.views.decorators.cache import cache_control
from rdflib import Graph, URIRef
from rest_framework.decorators import api_view, permission_classes, renderer_classes
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import BasePermission, IsAdminUser, IsAuthenticated
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.reverse import reverse

from ..access_policies.workspace import WorkspaceAccessPolicy
from ..models.author import Author
from ..models.cache_reset import last_cache_reset_at
from ..models.rdf_document import RDFDocumentVersion
from ..models.workspace import Workspace
from ..queries import (
    construct_classes_with_shacl_shapes,
    construct_documents_meta,
    construct_documents_ontology,
    construct_instances,
    construct_shapes_indirectly_for,
    select_documents_being_edited,
    select_documents_published,
)
from ..renderers.json_ld import JsonLdRenderer
from ..renderers.n_triples import NTriplesRenderer
from ..renderers.sparql_result_json import SPARQLResultJSONRenderer
from ..renderers.sparql_result_xml import SPARQLResultXMLRenderer
from ..renderers.trig import TriGRenderer
from ..renderers.turtle import TurtleRenderer
from ..services.triple_store import TripleStore
from ..sparql import Var
from ..sparql_scoping import scope_query
from ..util import fix_protocol
from .datasets import render_elm
from .decorators import condition_func


@transaction.atomic
def rdf_documents_last_modified(request, name=None, *args, **kwargs):
    workspace = _workspace(request)

    if name is not None:
        if name == "documents-published":
            return updated_at_max_published(workspace)
        elif name == "documents-being-edited":
            return updated_at_max_draft(workspace)
        elif name == "classes":
            return updated_at_max_published(workspace)
        elif name == "classes-with-shacl-shapes":
            return updated_at_max_published(workspace)
        elif name == "subclasses-with-shacl-shapes":
            return updated_at_max_published(workspace)
        elif name == "instances":
            return updated_at_max_published(workspace)
        elif name == "documents-ontology":
            return updated_at_max_published(workspace)
    return None


def updated_at_max_published(workspace):
    updated_at__max = RDFDocumentVersion.objects.filter(
        ~Q(valid_from=None) & Q(valid_to=None) & Q(deleted_at=None) & Q(rdf_document__workspace=workspace)
    ).aggregate(Max("updated_at"))["updated_at__max"]
    last_cache_reset = last_cache_reset_at()
    timestamps = list(filter(None, [updated_at__max, last_cache_reset]))
    if len(timestamps) > 0:
        return max(timestamps)
    else:
        return None


def updated_at_max_draft(workspace):
    updated_at__max = RDFDocumentVersion.objects.filter(
        Q(valid_from=None) & Q(valid_to=None) & Q(deleted_at=None) & Q(rdf_document__workspace=workspace)
    ).aggregate(Max("updated_at"))["updated_at__max"]
    last_cache_reset = last_cache_reset_at()
    timestamps = list(filter(None, [updated_at__max, last_cache_reset]))
    if len(timestamps) > 0:
        return max(timestamps)
    else:
        return None


@api_view(["GET"])
@permission_classes([IsAuthenticated])
@renderer_classes([TemplateHTMLRenderer, TurtleRenderer, NTriplesRenderer, JsonLdRenderer, TriGRenderer])
@cache_control(no_cache=True, private=True)
@condition_func(last_modified_func=rdf_documents_last_modified)
def construct(request, name: str):
    if request.accepted_renderer.media_type == "text/html":
        return render_elm(request)

    if name == "documents-published":
        return Response(_construct_documents_published(request))
    elif name == "documents-being-edited":
        return Response(_construct_documents_being_edited(request))
    elif name == "classes":
        return Response(_construct_classes(request))
    elif name == "classes-with-shacl-shapes":
        return Response(_construct_classes_with_shacl_shapes(request))
    elif name == "subclasses-with-shacl-shapes":
        return Response(_construct_subclasses_with_shacl_shapes(request))
    elif name == "instances":
        return Response(_construct_instances(request))
    elif name == "documents-ontology":
        return Response(_construct_documents_ontology(request))
    else:
        raise ValidationError("invalid query")


def _construct_documents_published(request):
    triple_store = TripleStore()
    iri_workspace = _iri_workspace(request)
    document = Var("document", randomize=False)
    query_documents_published = select_documents_published(
        selected=[document],
        iri_workspace=iri_workspace,
        document=document,
    )
    iris_document = [
        URIRef(iri_document["document"]["value"])
        for iri_document in triple_store.query(query_documents_published)["results"]["bindings"]
    ]
    return _query_in_chunks(construct_documents_meta, iris_document)


def _construct_documents_being_edited(request):
    triple_store = TripleStore()

    workspace = _workspace(request)
    author = _author(request)

    document = Var("document", randomize=False)

    if workspace.memberships.all().at_least_manager().filter(author=author).exists():
        iri_workspace = _iri_workspace_from_workspace(request, workspace)
        query_documents_being_edited = select_documents_being_edited(
            selected=[document],
            iri_workspace=iri_workspace,
            document=document,
        )
    elif workspace.memberships.all().at_least_editor().filter(author=author).exists():
        iri_workspace = _iri_workspace_from_workspace(request, workspace)
        iri_author = _iri_author_from_author(request, author)
        query_documents_being_edited = select_documents_being_edited(
            selected=[document],
            iri_workspace=iri_workspace,
            iri_author=iri_author,
            document=document,
        )
    else:
        return Graph()

    iris_document = [
        URIRef(iri_document["document"]["value"])
        for iri_document in triple_store.query(query_documents_being_edited)["results"]["bindings"]
    ]

    return _query_in_chunks(construct_documents_meta, iris_document)


def _construct_classes(request):
    triple_store = TripleStore()
    iri_workspace = _iri_workspace(request)
    construct = construct_classes_with_shacl_shapes(iri_workspace, shapes_required=False)
    graph = triple_store.construct(construct)
    return graph


def _construct_classes_with_shacl_shapes(request):
    triple_store = TripleStore()
    iri_workspace = _iri_workspace(request)
    construct = construct_classes_with_shacl_shapes(iri_workspace, shapes_required=True)
    graph = triple_store.construct(construct)
    return graph


def _construct_subclasses_with_shacl_shapes(request):
    iri_workspace = _iri_workspace(request)
    if (iri_class := request.query_params.get("class")) is None:
        raise ValidationError("class_missing", code="class_missing")
    graph = TripleStore().construct(construct_shapes_indirectly_for(URIRef(iri_class), workspace=URIRef(iri_workspace)))
    return graph


def _construct_instances(request):
    iri_workspace = _iri_workspace(request)
    if (iri_class := request.query_params.get("class")) is None:
        raise ValidationError("class_missing", code="class_missing")
    construct = construct_instances(iri_workspace, iri_class=iri_class)
    graph = TripleStore().construct(construct)
    return graph


def _construct_documents_ontology(request):
    triple_store = TripleStore()
    iri_workspace = _iri_workspace(request)
    construct = construct_documents_ontology(iri_workspace)
    return triple_store.construct(construct)


@api_view(["GET"])
@permission_classes([IsAdminUser])
@renderer_classes([TurtleRenderer, NTriplesRenderer, JsonLdRenderer, TriGRenderer])
def construct_custom(request):
    triple_store = TripleStore()
    query = request.query_params["query"]
    return Response(triple_store.construct(query))


class UsesSparklis(BasePermission):
    def has_permission(self, request, view):
        author, _ = Author.objects.get_or_create(user=request.user)
        return settings.USE_SPARKLIS and author.can_send_sparql_queries


@api_view(["GET"])
@permission_classes([IsAuthenticated, UsesSparklis])
@renderer_classes([SPARQLResultJSONRenderer, SPARQLResultXMLRenderer])
def query_sparklis(request, workspace_uuid):
    query = request.query_params["query"]
    workspaces = Workspace.objects.all()
    workspaces = WorkspaceAccessPolicy.scope_queryset(request.user.author, workspaces)
    workspaces = workspaces.with_prefetches()
    workspace = workspaces.get(uuid=workspace_uuid)
    iri_workspace = fix_protocol(reverse("workspace-detail", args=[workspace.uuid], request=request))
    query_scoped = scope_query(query, workspace=iri_workspace)
    result = TripleStore().query(
        query_scoped,
        parse=False,
        accept=request.accepted_media_type,
    )
    return Response(result)


@api_view(["GET"])
@permission_classes([IsAdminUser])
@renderer_classes([SPARQLResultJSONRenderer, SPARQLResultXMLRenderer])
def query_custom(request):
    query = request.query_params["query"]

    if "workspace" in request.query_params:
        iri_workspace = _iri_workspace(request)
        query_scoped = scope_query(query, workspace=iri_workspace)
        result = TripleStore().query(
            query_scoped,
            parse=False,
            accept=request.accepted_media_type,
        )
        return Response(result)
    else:
        result = TripleStore().query(
            query,
            parse=False,
            accept=request.accepted_media_type,
        )
        return Response(result)


def _iri_workspace(request):
    workspace = _workspace(request)
    return _iri_workspace_from_workspace(request, workspace)


def _iri_workspace_from_workspace(request, workspace):
    return fix_protocol(reverse("workspace-detail", args=[workspace.uuid], request=request))


def _workspace(request):
    if (uuid := request.GET.get("workspace", None)) is None:
        raise ValidationError("workspace_missing", code="workspace_missing")

    workspaces = Workspace.objects.all()
    workspaces = WorkspaceAccessPolicy.scope_queryset(request.user.author, workspaces)
    workspaces = workspaces.with_prefetches()
    return workspaces.get(uuid=uuid)


def _iri_author(request):
    author = _author(request)
    return _iri_author_from_author(request, author)


def _iri_author_from_author(request, author):
    return fix_protocol(reverse("author-detail", args=[author.id], request=request))


def _author(request):
    author, _ = Author.objects.get_or_create(user=request.user)
    return author


def _query_in_chunks(to_query, iris, graph=None):
    if graph is None:
        graph = Graph()

    triple_store = TripleStore()

    chunks = batched(iris, 10)
    for chunk in chunks:
        graph += triple_store.construct(to_query(chunk))

    return graph
