# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.


from ..models.rdf_identifier import RDFIdentifier


def test__graph__mint__post(request, authenticated_client):
    response = authenticated_client.post(
        "/graph/mint/",
        [
            {
                "rdf_class": "ProtocolCast",
            }
        ],
    )
    rdf_identifier = RDFIdentifier.objects.last()
    request.addfinalizer(lambda: rdf_identifier.delete())
    assert response.status_code == 201
    assert response.data == [f"http://testserver/graph/{rdf_identifier.path_segments}/"]


def test__graph__mint__post__with_container(request, authenticated_client, author_alice):
    rdf_identifier_container = RDFIdentifier.objects.create(created_by=author_alice, rdf_class_slug="protocol-cast")
    request.addfinalizer(lambda: rdf_identifier_container.delete())
    response = authenticated_client.post(
        f"/graph/{rdf_identifier_container.path_segments}/mint/",
        [
            {
                "rdf_class": "ProtocolCast",
            }
        ],
    )
    rdf_identifier = RDFIdentifier.objects.last()
    request.addfinalizer(lambda: rdf_identifier.delete())
    assert response.status_code == 201
    assert response.data == [f"http://testserver/graph/{rdf_identifier.path_segments}/"]
