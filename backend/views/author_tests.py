# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from datetime import datetime, timedelta

from freezegun import freeze_time
from rest_framework.test import APIClient


def test_list(authenticated_client):
    response = authenticated_client.get("/authors/")
    assert response.status_code == 200
    assert isinstance(response.data, list)


def test_retrieve(authenticated_client, author_alice):
    response = authenticated_client.get(f"/authors/{author_alice.id}/")
    assert response.status_code == 200
    assert isinstance(response.data, dict)


def test_list__last_modified(author_random):
    # FIXME this will break in ~1000 years
    initial_datetime = datetime(3022, 12, 1, 10, 30)
    with freeze_time(initial_datetime) as frozen_datetime:
        author = author_random()
        client = APIClient()
        client.login(username=author.user.username, password="secret")
        response = client.get("/authors/")
        assert response.headers["Last-Modified"] == "Sun, 01 Dec 3022 10:30:00 GMT"

        frozen_datetime.tick(timedelta(seconds=1))
        author_random()
        response = client.get("/authors/")
        assert response.headers["Last-Modified"] == "Sun, 01 Dec 3022 10:30:01 GMT"


def test_list__last_modified__after_cache_reset(author_random, cache_reset):
    # FIXME this will break in ~1000 years
    initial_datetime = datetime(3022, 12, 1, 10, 30)
    with freeze_time(initial_datetime) as frozen_datetime:
        author = author_random()
        client = APIClient()
        client.login(username=author.user.username, password="secret")
        response = client.get("/authors/")
        assert response.headers["Last-Modified"] == "Sun, 01 Dec 3022 10:30:00 GMT"

        frozen_datetime.tick(timedelta(seconds=1))
        cache_reset()
        response = client.get("/authors/")
        assert response.headers["Last-Modified"] == "Sun, 01 Dec 3022 10:30:01 GMT"
