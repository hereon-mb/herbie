# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from urllib.parse import urlencode

from django.shortcuts import redirect
from inflection import dasherize, underscore
from pyshacl.rdfutil.clone import clone_graph
from rdflib import PROV, RDF, RDFS, URIRef
from rest_framework import serializers, status
from rest_framework.decorators import api_view, permission_classes, renderer_classes
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.reverse import reverse

from ..access_policies.workspace import WorkspaceAccessPolicy
from ..models.author import Author
from ..models.rdf_identifier import RDFIdentifier
from ..models.workspace import Workspace
from ..namespace._HERBIE import HERBIE
from ..namespaces import Namespaces
from ..queries import construct_graph, construct_instance_graph, select_iri_is_defined_by
from ..renderers.json_ld import JsonLdRenderer
from ..renderers.n_triples import NTriplesRenderer
from ..renderers.trig import TriGRenderer
from ..renderers.turtle import TurtleRenderer
from ..services.rdf_document import RDFDocumentService
from ..services.rdf_document_version import RDFDocumentVersionService  # type: ignore
from ..services.triple_store import TripleStore
from ..sparql import Select, Values, Var
from ..util import fix_protocol
from .datasets import render_elm


class RDFIdentifierMintParamsSerializer(serializers.Serializer):
    rdf_class = serializers.CharField()


@api_view(["POST"])
@permission_classes([IsAuthenticated])
def rdf_identifier_mint(request, path_segments=None):
    serializer = RDFIdentifierMintParamsSerializer(data=request.data, many=True, allow_empty=False)
    serializer.is_valid(raise_exception=True)
    params = serializer.validated_data

    rdf_identifiers = []
    for param in params:
        created_by, _ = Author.objects.get_or_create(user=request.user)
        rdf_class_slug = dasherize(underscore(param["rdf_class"]))
        rdf_identifier = RDFIdentifier.objects.create_rdf_identifier(path_segments, created_by, rdf_class_slug)
        rdf_identifiers.append(rdf_identifier)

    path_segments = []
    for rdf_identifier in rdf_identifiers:
        path_segments.append(
            fix_protocol(
                reverse(
                    "graph-internal",
                    kwargs={"path_segments": rdf_identifier.path_segments},
                    request=request,
                )
            )
        )

    return Response(
        status=status.HTTP_201_CREATED,
        data=path_segments,
    )


@api_view(["GET"])
@permission_classes([IsAuthenticated])
@renderer_classes([TemplateHTMLRenderer, TurtleRenderer, NTriplesRenderer, JsonLdRenderer, TriGRenderer])
def graph_internal(request, path_segments):
    iri = URIRef(
        fix_protocol(
            reverse(
                "graph-internal",
                kwargs={"path_segments": path_segments},
                request=request,
            )
        )
    )

    if request.accepted_renderer.media_type == "text/html":
        if "iri" in request.query_params and "workspace" in request.query_params:
            return render_elm(request)
        else:
            is_defined_by = Var("isDefinedBy")
            response = TripleStore().query(select_iri_is_defined_by(iri, is_defined_by))
            bindings = response["results"]["bindings"]
            iri_document_version = URIRef(bindings[0][is_defined_by.name]["value"])
            document_version = RDFDocumentVersionService(request.user.author).get_version(iri_document_version)
            iri_workspace = Namespaces().workspaces(document_version.rdf_document.workspace.uuid)
            graph = _instance_graph(iri, iri_workspace)
            try:
                cls = list(graph.triples((iri, RDF["type"], None)))[0][2]
                iri_dataset = list(graph.triples((iri, RDFS.isDefinedBy, None)))[0][2]
            except IndexError:
                return render_elm(request)
            rdf_document_version = RDFDocumentService(request.user.author).get_version_via_iri(iri_dataset)
            base = reverse("graph-external", request=request)
            query = {
                "class": cls,
                "instance": iri,
                "workspace": rdf_document_version.rdf_document.workspace.uuid,
            }
            url = f"{base}?{urlencode(query)}"
            return redirect(url)

    iri_workspace = _iri_workspace(request)

    graph = _instance_graph(iri, iri_workspace)
    return Response(graph)


@api_view(["GET"])
@permission_classes([IsAuthenticated])
@renderer_classes([TemplateHTMLRenderer, TurtleRenderer, NTriplesRenderer, JsonLdRenderer, TriGRenderer])
def graph_external(request):
    if request.accepted_renderer.media_type == "text/html":
        return render_elm(request)

    iri = URIRef(request.query_params.get("iri"))
    iri_workspace = _iri_workspace(request)

    graph = _instance_graph(iri, iri_workspace)
    return Response(graph)


def _iri_workspace(request):
    workspace = _workspace(request)
    return _iri_workspace_from_workspace(request, workspace)


def _iri_workspace_from_workspace(request, workspace):
    return fix_protocol(reverse("workspace-detail", args=[workspace.uuid], request=request))


def _workspace(request):
    if (uuid := request.GET.get("workspace", None)) is None:
        raise ValidationError("workspace_missing", code="workspace_missing")

    workspaces = Workspace.objects.all()
    workspaces = WorkspaceAccessPolicy.scope_queryset(request.user.author, workspaces)
    workspaces = workspaces.with_prefetches()
    return workspaces.get(uuid=uuid)


def _instance_graph(iri, iri_workspace):
    graph = TripleStore().construct(construct_instance_graph(iri, iri_workspace))

    instance = Var("instance")
    is_defined_by = Var("isDefinedBy")
    is_defined_by_document = Var("isDefinedByDocument")

    result = graph.query(
        str(
            Select(
                is_defined_by_document,
            ).where(
                Values(instance, iri),
                (instance, RDFS.isDefinedBy, is_defined_by),
                (is_defined_by, RDF.type, HERBIE.DocumentVersion),
                (is_defined_by, PROV.specializationOf, is_defined_by_document),
            )
        )
    )
    if len(result.bindings) > 0:
        iri_document = result.bindings[0][is_defined_by_document.name]
        graph_document = TripleStore().construct(construct_graph(iri_document))
        clone_graph(graph_document, graph)

    return graph
