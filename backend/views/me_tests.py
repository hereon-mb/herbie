# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from ..matchers import matches, some_int


def test_me_unauthenticated(unauthenticated_client):
    response = unauthenticated_client.get("/me/")
    assert response.status_code == 401
    assert response.data is None


def test_me_authenticated(authenticated_client):
    response = authenticated_client.get("/me/")
    assert response.status_code == 200
    assert isinstance(response.data, dict)
    assert response.data == {
        "id": some_int(),
        "url": matches(".*/users/.*"),
        "username": "alice",
        "email": "alice@example.com",
        "first_name": "Alice",
        "last_name": "Wonderland",
    }


def test_me_author_unauthenticated(unauthenticated_client):
    response = unauthenticated_client.get("/me/author/")
    assert response.status_code == 401
    assert response.data is None


def test_me_author_authenticated(authenticated_client_for, author_random):
    author = author_random()

    response = authenticated_client_for(author).get("/me/author/")
    assert response.status_code == 200
    assert response.data == {
        "id": some_int(),
        "url": matches(".*/authors/.*"),
        "user": {
            "id": some_int(),
            "url": matches(".*/users/.*"),
            "username": author.user.username,
            "email": author.user.email,
            "first_name": author.user.first_name,
            "last_name": author.user.last_name,
        },
        "permissions": {
            "rdf_document": False,
            "can_send_sparql_queries": False,
            "can_upload_raw_files": False,
            "superuser": False,
        },
        "preferences_author": None,
        "auth_tokens": [],
    }


def test__preferences(authenticated_client_for, author_random):
    author = author_random()
    authenticated_client = authenticated_client_for(author)

    response = authenticated_client.put(
        "/me/preferences/",
        {
            "use_rdf_documents": True,
            "documents_ontology_default": ["http://example.org/"],
        },
    )
    assert response.data == {
        "use_rdf_documents": True,
        "documents_ontology_default": ["http://example.org/"],
    }
