# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from functools import cached_property

from rest_framework import status
from rest_framework.mixins import (
    CreateModelMixin,
    DestroyModelMixin,
    ListModelMixin,
    RetrieveModelMixin,
    UpdateModelMixin,
)
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from ..access_policies.workspace import MembershipAccessPolicy, WorkspaceAccessPolicy
from ..models.workspace import Membership, Workspace
from ..serializers.workspace import MembershipSerializer, WorkspaceSerializer
from ..services.workspace import WorkspaceService


class WorkspaceViewSet(
    ListModelMixin,
    RetrieveModelMixin,
    CreateModelMixin,
    UpdateModelMixin,
    DestroyModelMixin,
    GenericViewSet,
):
    serializer_class = WorkspaceSerializer
    pagination_class = None
    permission_classes = (WorkspaceAccessPolicy,)
    lookup_field = "uuid"

    @property
    def access_policy(self):
        return self.permission_classes[0]

    def get_queryset(self):
        queryset = Workspace.objects.all()
        queryset = queryset.filter(deleted_at__isnull=True)
        queryset = self.access_policy.scope_queryset(self.request.user.author, queryset)
        queryset = queryset.with_prefetches()
        return queryset

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        workspace = self._workspace_service.create_workspace(**serializer.validated_data)
        serializer = self.get_serializer(workspace)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_update(self, serializer: WorkspaceSerializer) -> None:  # type: ignore
        workspace = self.get_object()
        for membership in serializer.validated_data.pop("memberships", []):
            membership["author"] = membership.pop("author_id")
            self._workspace_service.add_membership(workspace, **membership)
        return super().perform_update(serializer)

    def destroy(self, request, *args, **kwargs):
        workspace = self.get_object()
        workspace = self._workspace_service.delete_workspace(workspace)
        serializer = WorkspaceSerializer(workspace, context=self.get_serializer_context())
        return Response(serializer.data, status=status.HTTP_200_OK)

    @cached_property
    def _workspace_service(self):
        return WorkspaceService(self.request)


class MembershipViewSet(
    RetrieveModelMixin,
    UpdateModelMixin,
    DestroyModelMixin,
    GenericViewSet,
):
    queryset = Membership.objects.all()
    serializer_class = MembershipSerializer

    pagination_class = None
    permission_classes = (MembershipAccessPolicy,)

    def destroy(self, request, *args, **kwargs):
        membership = self.get_object()
        membership = self._workspace_service.delete_membership(membership)
        workspace = membership.workspace
        serializer = WorkspaceSerializer(workspace, context=self.get_serializer_context())
        return Response(serializer.data, status=status.HTTP_200_OK)

    @cached_property
    def _workspace_service(self):
        return WorkspaceService(self.request)
