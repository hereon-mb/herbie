# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from ..models.author import Author
from ..models.preferences_author import PreferencesAuthor
from ..serializers.author import AuthorDetailSerializer
from ..serializers.preferences_author import PreferencesAuthorSerializer
from ..serializers.user import UserSerializer


@api_view(["GET"])
def me(request):
    if request.user.is_authenticated:
        serializer = UserSerializer(
            request.user,
            context={"request": request},
        )
        return Response(serializer.data)
    else:
        return Response(status=status.HTTP_401_UNAUTHORIZED)


@api_view(["GET"])
def me_author(request):
    if request.user.is_authenticated:
        author = ensure_author_exists(request.user)
        serializer = AuthorDetailSerializer(
            author,
            context={"request": request},
        )
        return Response(serializer.data)
    else:
        return Response(status=status.HTTP_401_UNAUTHORIZED)


@api_view(["PUT"])
def preferences(request):
    if request.user.is_authenticated:
        author = ensure_author_exists(request.user)
        preferences_author = ensure_preferences_author_exists(author)

        serializer = PreferencesAuthorSerializer(preferences_author, data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data)
    else:
        return Response(status=status.HTTP_401_UNAUTHORIZED)


def ensure_author_exists(user):
    author, _ = Author.objects.get_or_create(user=user)
    return author


def ensure_preferences_author_exists(author):
    preferences_author, _ = PreferencesAuthor.objects.get_or_create(author=author)
    return preferences_author
