# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

import mimetypes
import os
import re
import urllib

from django.conf import settings
from django.http import HttpResponse
from rdflib import RDFS, Dataset, Literal, URIRef
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes, renderer_classes
from rest_framework.permissions import BasePermission, IsAuthenticated
from rest_framework.renderers import BrowsableAPIRenderer, JSONRenderer
from rest_framework.response import Response
from rest_framework.reverse import reverse

from ..models.author import Author
from ..models.raw_file import RawFile
from ..renderers.json_ld import JsonLdRenderer
from ..renderers.n_triples import NTriplesRenderer
from ..renderers.trig import TriGRenderer
from ..renderers.turtle import TurtleRenderer
from ..util import fix_protocol


class AllowUploads(BasePermission):
    def has_permission(self, request, view):
        author, _ = Author.objects.get_or_create(user=request.user)
        return settings.ALLOW_UPLOADS and author.can_upload_raw_files


@api_view(["POST"])
@permission_classes([IsAuthenticated, AllowUploads])
@renderer_classes([TurtleRenderer, NTriplesRenderer, JsonLdRenderer, TriGRenderer])
def raw_upload(request):
    if "file" not in request.data:
        return Response(status=status.HTTP_400_BAD_REQUEST)
    file = request.data["file"]

    author, _ = Author.objects.get_or_create(user=request.user)
    raw_file = RawFile.objects.create(
        created_by=author,
        file=file,
    )
    filename = re.sub(r"^raw/", "", raw_file.file.name)
    location = reverse("rawfile", kwargs={"filename": filename}, request=request)
    location = fix_protocol(location)

    if request.accepted_renderer.format in ["turtle", "n-triples", "json-ld", "trig"]:
        dataset = Dataset()
        iri_location = URIRef(location)
        graph = dataset.graph(iri_location)
        filename_original = os.path.basename(raw_file.file.name)
        graph.add((iri_location, RDFS.label, Literal(filename_original)))
        if request.accepted_renderer.format in ["json-ld", "trig"]:
            return Response(status=status.HTTP_201_CREATED, data=dataset)
        else:
            return Response(status=status.HTTP_201_CREATED, data=graph)
    else:
        return Response(
            status=status.HTTP_201_CREATED,
            headers={"Location": location},
        )


@api_view(["GET"])
@renderer_classes([JSONRenderer, BrowsableAPIRenderer, TurtleRenderer, NTriplesRenderer, JsonLdRenderer, TriGRenderer])
@permission_classes([IsAuthenticated])
def raw_retrieve(request, filename):
    author, _ = Author.objects.get_or_create(user=request.user)
    raw_file = RawFile.objects.filter(created_by=author, file=f"raw/{filename}").first()

    if request.accepted_renderer.format in ["turtle", "n-triples", "json-ld", "trig"]:
        dataset = Dataset()
        location = reverse("rawfile", kwargs={"filename": filename}, request=request)
        location = fix_protocol(location)
        iri_location = URIRef(location)
        graph = dataset.graph(iri_location)
        filename_original = os.path.basename(raw_file.file.name)
        graph.add((iri_location, RDFS.label, Literal(filename_original)))
        if request.accepted_renderer.format in ["json-ld", "trig"]:
            return Response(status=status.HTTP_201_CREATED, data=dataset)
        else:
            return Response(status=status.HTTP_201_CREATED, data=graph)
    elif settings.MEDIA_VIA_X_ACCEL_REDIRECT:
        response = HttpResponse(status=200)
        response["Content-Type"] = ""
        response["X-Accel-Redirect"] = urllib.parse.quote(f"/protected-media/{raw_file.file.name}")
        return response
    else:
        file_path = f"{settings.MEDIA_ROOT}/{raw_file.file.name}"
        fsock = open(file_path, "rb")

        file_name = os.path.basename(file_path)
        mime_type_guess = mimetypes.guess_type(file_name)

        response = HttpResponse(fsock, content_type=mime_type_guess[0])
        response["Content-Disposition"] = 'attachment; filename="' + file_name + '"'
        return response
