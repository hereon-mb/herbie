# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.


from rdflib import RDF, RDFS, BNode, Graph, Literal, URIRef
from rest_framework import status

from ..models.rdf_document import RDFDocument
from ..namespace._HERBIE import HERBIE


def test__datasets__create(request, clear_triple_store, authenticated_client, create_workspace):
    request.addfinalizer(lambda: clear_triple_store())
    workspace = create_workspace()
    graph = Graph()
    graph.add((BNode(), RDFS.label, Literal("something")))
    response = authenticated_client.post(
        f"/datasets/?workspace={workspace.uuid}",
        data=graph.serialize(format="nt"),
        content_type="application/n-triples",
        headers={
            "Accept": "application/n-triples",
        },
    )
    rdf_document = RDFDocument.objects.last()
    request.addfinalizer(lambda: rdf_document.delete())
    assert response.status_code == status.HTTP_201_CREATED
    assert response.headers["Location"] == f"http://testserver/datasets/{rdf_document.uuid}/"


def test__datasets__retrieve__unpublished(request, clear_triple_store, rdf_document__unpublished, authenticated_client):
    request.addfinalizer(lambda: clear_triple_store())
    rdf_document = rdf_document__unpublished()
    response = authenticated_client.get(
        f"/datasets/{rdf_document.uuid}/",
        headers={"Accept": "application/n-triples"},
    )
    assert response.status_code == status.HTTP_200_OK
    graph = response.data
    assert (URIRef(f"http://testserver/datasets/{rdf_document.uuid}/"), RDF.type, HERBIE.Document) in graph
    assert (URIRef(f"http://testserver/datasets/{rdf_document.uuid}/draft/"), RDF.type, HERBIE.DocumentDraft) in graph


def test__datasets__retrieve__published(request, clear_triple_store, rdf_document__published, authenticated_client):
    request.addfinalizer(lambda: clear_triple_store())
    rdf_document = rdf_document__published()
    response = authenticated_client.get(
        f"/datasets/{rdf_document.uuid}/",
        headers={"Accept": "application/n-triples"},
    )
    assert response.status_code == status.HTTP_200_OK
    graph = response.data
    assert (URIRef(f"http://testserver/datasets/{rdf_document.uuid}/"), RDF.type, HERBIE.Document) in graph
    assert (URIRef(f"http://testserver/datasets/{rdf_document.uuid}/versions/1/"), RDF.type, HERBIE.DocumentVersion) in graph


def test__datasets__draft__put(request, clear_triple_store, rdf_document__unpublished, authenticated_client):
    request.addfinalizer(lambda: clear_triple_store())
    rdf_document = rdf_document__unpublished()
    graph = Graph()
    graph.add((URIRef("http://example.org/alice"), RDFS.label, Literal("Alice Wonderland")))
    response = authenticated_client.put(
        f"/datasets/{rdf_document.uuid}/draft/",
        graph.serialize(format="turtle"),
        content_type="text/turtle",
        headers={"Accept": "application/n-triples"},
    )
    assert response.status_code == status.HTTP_200_OK


def test__datasets__draft__cancel(request, clear_triple_store, rdf_document__unpublished, authenticated_client):
    request.addfinalizer(lambda: clear_triple_store())
    rdf_document = rdf_document__unpublished(clean_up=False, sync_rdf_store=True)
    response = authenticated_client.post(
        f"http://testserver/datasets/{rdf_document.uuid}/draft/cancel/",
        headers={"Accept": "application/n-triples"},
    )
    assert response.status_code == 204


def test__datasets__draft__publish(request, clear_triple_store, rdf_document__unpublished, authenticated_client):
    request.addfinalizer(lambda: clear_triple_store())
    rdf_document = rdf_document__unpublished(sync_rdf_store=True)
    response = authenticated_client.post(
        f"/datasets/{rdf_document.uuid}/draft/publish/",
        headers={"Accept": "application/n-triples"},
    )
    assert response.status_code == 204
    assert response.headers["Location"] == f"http://testserver/datasets/{rdf_document.uuid}/"


def test__datasets__edit(request, clear_triple_store, rdf_document__published, authenticated_client):
    request.addfinalizer(lambda: clear_triple_store())
    rdf_document = rdf_document__published()
    response = authenticated_client.post(
        f"/datasets/{rdf_document.uuid}/edit/",
        headers={"Accept": "application/n-triples"},
    )
    assert response.status_code == 204
    assert response.headers["Location"] == f"http://testserver/datasets/{rdf_document.uuid}/"


def test__datasets_version(request, clear_triple_store, rdf_document__published, authenticated_client):
    request.addfinalizer(lambda: clear_triple_store())
    rdf_document = rdf_document__published()
    response = authenticated_client.get(
        f"/datasets/{rdf_document.uuid}/versions/1/",
        headers={"Accept": "application/n-triples"},
    )
    assert response.status_code == status.HTTP_200_OK
    graph = response.data
    assert (URIRef(f"http://testserver/datasets/{rdf_document.uuid}/versions/1/"), RDF.type, HERBIE.DocumentVersion) in graph


# FIXME Flaky test
# def test__datasets__list(request, clear_triple_store, rdf_document_service, authenticated_client):
#     request.addfinalizer(lambda: clear_triple_store())
#
#     initial_datetime = datetime(2021, 9, 7, 10, 30)
#     with freeze_time(initial_datetime) as frozen_datetime:
#         result_a = rdf_document_service.create()
#
#         result_a.sync_rdf_store()
#
#         frozen_datetime.tick()
#
#         result_b = rdf_document_service.create()
#         result_c = rdf_document_service.create()
#
#         result_a.sync_rdf_store()
#         result_b.sync_rdf_store()
#         result_c.sync_rdf_store()
#
#         rdf_document_a = result_a.rdf_document
#         rdf_document_b = result_b.rdf_document
#         rdf_document_c = result_c.rdf_document
#         if rdf_document_b.uuid > rdf_document_c.uuid:
#             temp = rdf_document_b
#             rdf_document_b = rdf_document_c
#             rdf_document_c = temp
#
#         request.addfinalizer(lambda: rdf_document_a.delete())
#         request.addfinalizer(lambda: rdf_document_b.delete())
#         request.addfinalizer(lambda: rdf_document_c.delete())
#
#         frozen_datetime.tick()
#
#         response = authenticated_client.get(
#             "/datasets/?page_size=1",
#             headers={"Accept": "application/n-triples"},
#         )
#         assert response.status_code == status.HTTP_200_OK
#         assert len(list(response.data)) == 1
#         assert (URIRef(f"http://testserver/datasets/{rdf_document_c.uuid}/"), RDF.type, HERBIE.Document) in response.data
#         links = response.headers["Link"].split(", ")
#         assert len(links) == 2
#         url_prev = re.match("<(?P<prev>[^<]*)>; rel=prev", links[0]).group("prev")
#         url_next = re.match("<(?P<next>[^<]*)>; rel=next", links[1]).group("next")
#
#         response = authenticated_client.get(
#             url_prev,
#             headers={"Accept": "application/n-triples"},
#         )
#         assert response.status_code == status.HTTP_200_OK
#         assert len(list(response.data)) == 1
#         assert (URIRef(f"http://testserver/datasets/{rdf_document_b.uuid}/"), RDF.type, HERBIE.Document) in response.data
#         links = response.headers["Link"].split(", ")
#         assert len(links) == 1
#         url_prev = re.match("<(?P<prev>[^<]*)>; rel=prev", links[0]).group("prev")
#
#         response = authenticated_client.get(
#             url_prev,
#             headers={"Accept": "application/n-triples"},
#         )
#         assert response.status_code == status.HTTP_200_OK
#         assert len(list(response.data)) == 1
#         assert (URIRef(f"http://testserver/datasets/{rdf_document_a.uuid}/"), RDF.type, HERBIE.Document) in response.data
#         assert "link" not in response.headers
#
#         result_a = rdf_document_service.update(rdf_document_a, Graph())
#         result_b = rdf_document_service.update(rdf_document_b, Graph())
#
#         result_a.sync_rdf_store()
#         result_b.sync_rdf_store()
#
#         rdf_document_a = result_a.rdf_document
#         rdf_document_b = result_b.rdf_document
#         if rdf_document_a.uuid > rdf_document_b.uuid:
#             temp = rdf_document_a
#             rdf_document_a = rdf_document_b
#             rdf_document_b = temp
#
#         frozen_datetime.tick()
#
#         response = authenticated_client.get(
#             url_next,
#             headers={"Accept": "application/n-triples"},
#         )
#         assert response.status_code == status.HTTP_200_OK
#         assert len(list(response.data)) == 1
#         assert (URIRef(f"http://testserver/datasets/{rdf_document_a.uuid}/"), RDF.type, HERBIE.Document) in response.data
#         links = response.headers["Link"].split(", ")
#         assert len(links) == 1
#         url_next = re.match("<(?P<next>[^<]*)>; rel=next", links[0]).group("next")
#
#         response = authenticated_client.get(
#             url_next,
#             headers={"Accept": "application/n-triples"},
#         )
#         assert response.status_code == status.HTTP_200_OK
#         assert len(list(response.data)) == 1
#         assert (URIRef(f"http://testserver/datasets/{rdf_document_b.uuid}/"), RDF.type, HERBIE.Document) in response.data
#         links = response.headers["Link"].split(", ")
#         assert len(links) == 1
#         url_next = re.match("<(?P<next>[^<]*)>; rel=next", links[0]).group("next")
#
#         response = authenticated_client.get(
#             url_next,
#             headers={"Accept": "application/n-triples"},
#         )
#         assert response.status_code == status.HTTP_200_OK
#         assert len(list(response.data)) == 0
#         links = response.headers["Link"].split(", ")
#         assert len(links) == 1
#         url_next = re.match("<(?P<next>[^<]*)>; rel=next", links[0]).group("next")
#
#         frozen_datetime.tick()
#
#         response = authenticated_client.get(
#             url_next,
#             headers={"Accept": "application/n-triples"},
#         )
#         assert response.status_code == status.HTTP_200_OK
#         assert len(list(response.data)) == 0
#         links = response.headers["Link"].split(", ")
#         assert len(links) == 1
#         url_next_later = re.match("<(?P<next>[^<]*)>; rel=next", links[0]).group("next")
#
#         assert url_next != url_next_later
