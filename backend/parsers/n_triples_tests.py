import io

import pytest
from rdflib import OWL, RDF, Graph, URIRef
from rest_framework.exceptions import ParseError

from .n_triples import NTriplesParser


def test__empty(context_user_alice_with_path):
    context = context_user_alice_with_path("/api/graph/")
    content = ""
    graph = parse(content, context)
    assert isinstance(graph, Graph)


def test__valid(context_user_alice_with_path):
    context = context_user_alice_with_path("/api/graph/")
    content = "<http://testserver/api/graph/> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Ontology> .\n"
    graph = parse(content, context)
    assert isinstance(graph, Graph)
    assert ((URIRef("http://testserver/api/graph/"), RDF.type, OWL.Ontology)) in graph


def test__relative_iri(context_user_alice_with_path):
    context = context_user_alice_with_path("/api/graph/")
    content = "<> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Ontology> .\n"
    with pytest.raises(ParseError) as exc:
        parse(content, context)
    assert exc.value.detail.code == "parse_error"


def test__invalid(context_user_alice_with_path):
    context = context_user_alice_with_path("/api/graph/")
    content = "not an n-triples file"
    with pytest.raises(ParseError) as exc:
        parse(content, context)
    assert exc.value.detail.code == "parse_error"


def parse(content, context):
    return NTriplesParser().parse(io.StringIO(content), parser_context=context)
