# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from uuid import uuid4

from django.core.files.storage import FileSystemStorage
from rest_framework import parsers
from rest_framework.exceptions import ParseError, ValidationError
from rocrate.rocrate import ROCrate


class RoCrateParser(parsers.FileUploadParser):
    media_type = "application/zip"

    def parse(self, stream, media_type=None, parser_context=None):
        data_and_files = super().parse(stream, media_type=media_type, parser_context=parser_context)
        file_zip = data_and_files.files["file"]
        storage = FileSystemStorage()
        uuid = uuid4()
        storage.save(f"{uuid}.zip", file_zip)
        path = storage.path(f"{uuid}.zip")
        try:
            crate = ROCrate(path)
            return crate
        except (ValueError, TypeError) as exc:
            raise ParseError("RO-Crate parse error - %s" % str(exc)) from exc
        except KeyError as exc:
            raise ValidationError(
                f"The RO-Crate mentions files which are not described: {exc}",
                code="key_error",
            )
