# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from rdflib import Graph
from rdflib.exceptions import ParserError
from rest_framework import parsers
from rest_framework.exceptions import ParseError

from ..util import fix_protocol


class NTriplesParser(parsers.BaseParser):
    media_type = "application/n-triples"

    def parse(self, stream, media_type=None, parser_context=None):
        parser_context = parser_context or {}
        request = parser_context.get("request", None)
        publicID = None
        if request:
            publicID = fix_protocol(request.build_absolute_uri(request.path_info))

        try:
            graph = Graph()
            graph.parse(data=stream.read(), publicID=publicID, format="nt")
            return graph
        except ParserError as exc:
            raise ParseError("N-Triples parse error - %s" % str(exc))
