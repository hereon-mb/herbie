import io
from uuid import UUID

import pytest
from rdflib import Graph
from rest_framework.exceptions import ParseError

from .turtle import TurtleParser


def test__empty(context_user_alice_with_path):
    context = context_user_alice_with_path("/api/graph/")
    content = ""
    uuid, graph = parse(content, context)
    assert isinstance(uuid, UUID)
    assert isinstance(graph, Graph)


def test__relative_iri(context_user_alice_with_path):
    context = context_user_alice_with_path("/api/graph/")
    content = """
        @prefix owl: <http://www.w3.org/2002/07/owl#> .
        <> a owl:Ontology .
    """
    uuid, graph = parse(content, context)
    assert isinstance(uuid, UUID)
    assert isinstance(graph, Graph)


def test__invalid(context_user_alice_with_path):
    context = context_user_alice_with_path("/api/graph/")
    content = "not a turtle file"
    with pytest.raises(ParseError) as exc:
        parse(content, context)
    assert exc.value.detail.code == "parse_error"


def parse(content, context):
    return TurtleParser().parse(io.StringIO(content), parser_context=context)
