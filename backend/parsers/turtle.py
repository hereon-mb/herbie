# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from uuid import uuid4

from rdflib import Graph
from rdflib.plugins.parsers.notation3 import BadSyntax
from rest_framework import parsers
from rest_framework.exceptions import ParseError

from ..namespaces import Namespaces
from ..util import fix_protocol


class TurtleParser(parsers.BaseParser):
    media_type = "text/turtle"

    def parse(self, stream, media_type=None, parser_context=None):
        parser_context = parser_context or {}
        request = parser_context.get("request", None)
        kwargs = parser_context.get("kwargs", {})

        try:
            if "uuid" in kwargs:
                publicID = fix_protocol(request.build_absolute_uri(request.path_info))
                graph = Graph()
                graph.parse(data=stream.read(), publicID=publicID, format="turtle")
                return graph
            else:
                uuid = uuid4()
                publicID = Namespaces().datasets_draft(uuid)
                graph = Graph()
                graph.parse(data=stream.read(), publicID=publicID, format="turtle")
                return (uuid, graph)
        except BadSyntax as exc:
            raise ParseError("Turtle parse error - %s" % str(exc))
