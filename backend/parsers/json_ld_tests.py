import io

import pytest
from rdflib import OWL, RDF, Graph, URIRef
from rest_framework.exceptions import ParseError

from .json_ld import JsonLdParser


def test__empty(context_user_alice_with_path):
    context = context_user_alice_with_path("/api/graph/")
    content = ""
    with pytest.raises(ParseError) as exc:
        parse(content, context)
    assert exc.value.detail.code == "parse_error"


def test__empty_list(context_user_alice_with_path):
    context = context_user_alice_with_path("/api/graph/")
    content = "[]"
    graph = parse(content, context)
    assert isinstance(graph, Graph)


def test__valid(context_user_alice_with_path):
    context = context_user_alice_with_path("/api/graph/")
    content = """
        [
          {
            "@id": "http://testserver/api/graph/",
            "@type": [
              "http://www.w3.org/2002/07/owl#Ontology"
            ]
          }
        ]
    """
    graph = parse(content, context)
    assert isinstance(graph, Graph)
    assert ((URIRef("http://testserver/api/graph/"), RDF.type, OWL.Ontology)) in graph


def test__relative_iri(context_user_alice_with_path):
    context = context_user_alice_with_path("/api/graph/")
    content = """
        [
          {
            "@id": "",
            "@type": [
              "http://www.w3.org/2002/07/owl#Ontology"
            ]
          }
        ]
    """
    graph = parse(content, context)
    assert isinstance(graph, Graph)
    assert ((URIRef("http://testserver/api/graph/"), RDF.type, OWL.Ontology)) in graph


def test__invalid(context_user_alice_with_path):
    context = context_user_alice_with_path("/api/graph/")
    content = "not an n-triples file"
    with pytest.raises(ParseError) as exc:
        parse(content, context)
    assert exc.value.detail.code == "parse_error"


def parse(content, context):
    return JsonLdParser().parse(io.StringIO(content), parser_context=context)
