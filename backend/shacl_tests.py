# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

import io

from rdflib import RDF, Graph
from rdflib.term import URIRef

from .shacl import NodeShape, PropertyShape, parse_node_shape


def test__parse_node_shape():
    graph_shacl = Graph()
    graph_shacl.parse(
        io.StringIO(
            """
                @prefix : <http://example.org/> .
                @prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
                @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
                @prefix sh: <http://www.w3.org/ns/shacl#> .
                @prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

                :DieImportShape a sh:NodeShape ;
                  sh:class :Die ;
                  sh:property [
                    sh:path :characteristic ;
                    sh:node [
                      sh:property [ sh:path rdf:type ] ;
                      sh:property [ sh:path :hasValue ] ;
                      sh:property [ sh:path :hasUnit ] ;
                    ] ;
                  ] ;
                .
            """
        ),
        format="turtle",
    )

    assert parse_node_shape(graph_shacl, URIRef("http://example.org/DieImportShape")) == NodeShape(
        sh_class=[URIRef("http://example.org/Die")],
        sh_property=[
            PropertyShape(
                sh_path=URIRef("http://example.org/characteristic"),
                sh_node=NodeShape(
                    sh_class=[],
                    sh_property=[
                        PropertyShape(
                            sh_path=RDF["type"],
                        ),
                        PropertyShape(
                            sh_path=URIRef("http://example.org/hasValue"),
                        ),
                        PropertyShape(
                            sh_path=URIRef("http://example.org/hasUnit"),
                        ),
                    ],
                ),
            ),
        ],
    )
