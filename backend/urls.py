# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from django.contrib import admin
from django.urls import include, path, re_path
from rest_framework import routers

from .views.ajax_login import ajax_login
from .views.ajax_logout import ajax_logout
from .views.author import AuthorViewSet
from .views.datasets import (
    Datasets,
    datasets_version,
    datasets_version_entered,
    datasets_version_generated,
    datasets_version_persisted,
    datasets_version_support,
)
from .views.department import DepartmentViewSet
from .views.graph import graph_external, graph_internal, rdf_identifier_mint
from .views.me import me, me_author, preferences
from .views.raw import raw_retrieve, raw_upload
from .views.ro_crate_import import RoCrateImportViewSet
from .views.service_worker import service_worker
from .views.sparql import (
    construct,
    construct_custom,
    query_custom,
    query_sparklis,
)
from .views.sync_rdf_store import (
    clear_rdf_store,
    sync_rdf_store,
    sync_rdf_store_authors,
    sync_rdf_store_departments,
)
from .views.user import UserViewSet
from .views.workspace import MembershipViewSet, WorkspaceViewSet

aai_patterns = [
    path("helmholtz-aai/", include("django_helmholtz_aai.urls")),
]

auth_patterns = [
    path("auth/", include("knox.urls")),
    path("login/", ajax_login),
    path("logout/", ajax_logout),
]

router = routers.DefaultRouter()
router.register(r"users", UserViewSet)
router.register(r"authors", AuthorViewSet)
router.register(r"departments", DepartmentViewSet)
router.register(r"workspaces", WorkspaceViewSet, "workspace")
router.register(r"memberships", MembershipViewSet)
router.register(r"ro-crate-imports", RoCrateImportViewSet, "rocrateimport")
router.include_root_view = False

service_worker_patterns = [
    path("sw.js", service_worker),
]

me_patterns = [
    path("author/", me_author),
    path("preferences/", preferences, name="preferences"),
    path("", me),
]

triplestore_patterns = [
    path("clear/", clear_rdf_store),
    path("sync/authors/", sync_rdf_store_authors),
    path("sync/departments/", sync_rdf_store_departments),
    path("sync/", sync_rdf_store),
]

router_datasets = routers.DefaultRouter()
router_datasets.register("", Datasets, basename="dataset")
router_datasets.include_root_view = False

datasets_patterns = [
    path("<slug:uuid>/versions/<int:index>/", datasets_version, name="dataset-version"),
    path("<slug:uuid>/versions/<int:index>/entered/", datasets_version_entered, name="dataset-version-entered"),
    path("<slug:uuid>/versions/<int:index>/persisted/", datasets_version_persisted, name="dataset-version-persisted"),
    path("<slug:uuid>/versions/<int:index>/generated/", datasets_version_generated, name="dataset-version-generated"),
    path("<slug:uuid>/versions/<int:index>/support/", datasets_version_support, name="dataset-version-support"),
    path("", include(router_datasets.urls)),
]

raw_patterns = [
    re_path(r"^(?P<filename>.*)/", raw_retrieve, name="rawfile"),
    path("", raw_upload),
]

graph_patterns = [
    path("mint/", rdf_identifier_mint),
    re_path(r"^(?P<path_segments>.*)/mint/", rdf_identifier_mint),
    re_path(r"^(?P<path_segments>.*)/", graph_internal, name="graph-internal"),
    path("", graph_external, name="graph-external"),
]

construct_patterns = [
    path("<slug:name>/", construct),
    path("", construct_custom),
]

query_patterns = [
    path("<slug:workspace_uuid>/", query_sparklis),
    path("", query_custom),
]

urlpatterns = [
    path("", include(aai_patterns)),
    path("", include(auth_patterns)),  # type: ignore
    path("", include(router.urls)),
    path("", include(service_worker_patterns)),
    path("me/", include(me_patterns)),
    path("triplestore/", include(triplestore_patterns)),
    path("datasets/", include(datasets_patterns)),  # type: ignore
    path("raw/", include(raw_patterns)),
    path("graph/", include(graph_patterns)),
    path("construct/", include(construct_patterns)),
    path("query/", include(query_patterns)),
    path("admin/", admin.site.urls),
]
