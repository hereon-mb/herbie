# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

# This code was taken from https://github.com/RDFLib/pySHACL/ and adjusted to
# our needs.  The original code was licensed under the Apache License 2.0 (c.f.
# https://github.com/RDFLib/pySHACL/blob/master/LICENSE.txt)

from dataclasses import dataclass, field
from typing import Optional

from rdflib import RDF, RDFS, SH
from rdflib.term import URIRef

from .sparql import Construct, GraphPattern, Values, Var, ZeroOrMore


@dataclass
class NodeShape:
    sh_class: list[URIRef]
    sh_property: list["PropertyShape"] = field(default_factory=lambda: [])


@dataclass
class PropertyShape:
    sh_path: URIRef
    sh_node: Optional["NodeShape"] = None


def parse_node_shape(graph, iri):
    return NodeShape(
        sh_class=[iri_class for (_, _, iri_class) in graph.triples((iri, SH["class"], None))],
        sh_property=[
            parse_property_shape(graph, iri_property) for (_, _, iri_property) in graph.triples((iri, SH.property, None))
        ],
    )


def parse_property_shape(graph, iri):
    sh_node = None
    for _, _, iri_node in graph.triples((iri, SH.node, None)):
        sh_node = parse_node_shape(graph, iri_node)

    return PropertyShape(
        sh_path=[path for (_, _, path) in graph.triples((iri, SH.path, None))][0],
        sh_node=sh_node,
    )


def construct_from_node_shape(iri_workspace, node_shape):
    instance = Var("instance")

    triples, patterns = _from_node_shape(
        iri_workspace,
        node_shape,
        instance,
    )

    return Construct(*triples).where(*patterns)


def _from_node_shape(iri_workspace, node_shape, instance):
    triples = []
    patterns = []

    if len(node_shape.sh_class) > 0:
        cls = Var("class")
        cls_parent = Var("classParent")

        triples += [(instance, RDF.type, cls)]
        patterns += [
            Values(cls_parent, *set(node_shape.sh_class)),
            GraphPattern(
                iri_workspace,
                [
                    (instance, RDF.type, cls),
                    (cls, ZeroOrMore(RDFS.subClassOf), cls_parent),
                ],
            ),
        ]

    for sh_property in node_shape.sh_property:
        instance_nested = Var("instance")

        triples += [(instance, sh_property.sh_path, instance_nested)]
        patterns += [
            GraphPattern(
                iri_workspace,
                [(instance, sh_property.sh_path, instance_nested)],
            )
        ]

        if sh_property.sh_node is not None:
            triples_nested, patterns_nested = _from_node_shape(
                iri_workspace,
                sh_property.sh_node,
                instance_nested,
            )
            triples += triples_nested
            patterns += patterns_nested

    return triples, patterns
