# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from posixpath import join

from django.conf import settings
from rdflib import URIRef


class Namespaces:
    def authors(self, pk=None):
        return URIRef(self.iri_resource("authors", pk))

    def departments(self, pk=None):
        return URIRef(self.iri_resource("departments", pk))

    def workspaces(self, uuid=None):
        if uuid is None:
            url = settings.IRI_PREFIX
            url = join(url, "workspaces")
            url += "/"
            return URIRef(url)
        else:
            url = self._url_workspaces(uuid)
            url += "/"
            return URIRef(url)

    def workspace_provenances(self, uuid):
        url = self._url_workspaces(uuid)
        url += "/"
        url += "provenance/"
        return URIRef(url)

    def datasets(self, uuid=None):
        if uuid is None:
            url = settings.IRI_PREFIX
            url = join(url, "datasets")
            url += "/"
            return URIRef(url)
        else:
            url = self._url_datasets(uuid)
            url += "/"
            return URIRef(url)

    def datasets_access(self, uuid):
        url = self._url_datasets_access(uuid)
        url += "/"
        return URIRef(url)

    def datasets_draft(self, uuid):
        url = self._url_datasets_draft(uuid)
        url += "/"
        return URIRef(url)

    def datasets_draft_entered(self, uuid):
        url = self._url_datasets_draft(uuid)
        url = join(url, "entered")
        url += "/"
        return URIRef(url)

    def datasets_draft_persisted(self, uuid):
        url = self._url_datasets_draft(uuid)
        url = join(url, "persisted")
        url += "/"
        return URIRef(url)

    def datasets_draft_generated(self, uuid):
        url = self._url_datasets_draft(uuid)
        url = join(url, "generated")
        url += "/"
        return URIRef(url)

    def datasets_draft_support(self, uuid):
        url = self._url_datasets_draft(uuid)
        url = join(url, "support")
        url += "/"
        return URIRef(url)

    def datasets_version(self, uuid, index):
        url = self._url_datasets_version(uuid, index)
        url += "/"
        return URIRef(url)

    def datasets_version_entered(self, uuid, index):
        url = self._url_datasets_version(uuid, index)
        url = join(url, "entered")
        url += "/"
        return URIRef(url)

    def datasets_version_persisted(self, uuid, index):
        url = self._url_datasets_version(uuid, index)
        url = join(url, "persisted")
        url += "/"
        return URIRef(url)

    def datasets_version_generated(self, uuid, index):
        url = self._url_datasets_version(uuid, index)
        url = join(url, "generated")
        url += "/"
        return URIRef(url)

    def datasets_version_support(self, uuid, index):
        url = self._url_datasets_version(uuid, index)
        url = join(url, "support")
        url += "/"
        return URIRef(url)

    def iri_resource(self, path_segment, pk):
        url = settings.IRI_PREFIX
        url = join(url, path_segment)
        if pk is not None:
            url = join(url, str(pk))
        url += "/"
        return url

    def _url_workspaces(self, uuid):
        url = settings.IRI_PREFIX
        url = join(url, "workspaces")
        url = join(url, str(uuid))
        return url

    def _url_datasets_access(self, uuid):
        url = self._url_datasets(uuid)
        url = join(url, "access")
        return url

    def _url_datasets_draft(self, uuid):
        url = self._url_datasets(uuid)
        url = join(url, "draft")
        return url

    def _url_datasets_version(self, uuid, index):
        url = self._url_datasets(uuid)
        url = join(url, "versions")
        url = join(url, str(index))
        return url

    def _url_datasets(self, uuid):
        url = settings.IRI_PREFIX
        url = join(url, "datasets")
        url = join(url, str(uuid))
        return url
