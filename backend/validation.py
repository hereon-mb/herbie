# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from posixpath import normpath

from django.core.exceptions import ValidationError


def validate_present(model, field, errors, scope=""):
    if getattr(model, field) is None or getattr(model, field) == "":
        errors["{}{}".format(scope, field)] = ValidationError("blank", code="blank")
    return errors


def validate_non_empty(model, field, errors):
    if getattr(model, field).count() == 0:
        errors[field] = ValidationError("required", code="required")
    return errors


def validate_equipment_type(model, field, equipment_types, errors, scope=""):
    if getattr(model, field).kind not in equipment_types:
        errors["{}{}".format(scope, field)] = ValidationError("invalid", code="invalid")
    return errors


def validate_min_value(model, field, value, errors, scope=""):
    if getattr(model, field) < value:
        errors["{}{}".format(scope, field)] = ValidationError("min_value", code="min_value")
    return errors


def validate_max_value(model, field, value, errors, scope=""):
    if getattr(model, field) > value:
        errors["{}{}".format(scope, field)] = ValidationError("max_value", code="max_value")
    return errors


def validate_decimal_places(model, field, errors, scope=""):
    value = getattr(model, field)
    value_decimal_places = getattr(model, "{}_decimal_places".format(field))
    if round(value, value_decimal_places) - value != 0:
        errors["{}{}".format(scope, field)] = ValidationError("invalid_decimal_places", code="invalid_decimal_places")
    return errors


def validate_all(model, field, validator, errors):
    for index, child in enumerate(getattr(model, field).all()):
        for key, error in validator(child).items():
            errors["{}/".format(normpath("/{}/{}/{}".format(field, index, key)))] = error

    return errors
