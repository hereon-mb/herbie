# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from contextlib import contextmanager

from django.core import exceptions
from rest_framework.exceptions import ValidationError


def camelize(underscored: str):
    output = ""
    for index, word in enumerate(underscored.split("_")):
        if index == 0:
            output = output + word
        else:
            output = output + word.title()
    return output


def capitalize(underscored: str):
    output = ""
    for word in underscored.split("_"):
        output = output + word.title()
    return output


@contextmanager
def convert_validation_error():
    try:
        yield
    except exceptions.ValidationError as drf_error:
        if "error_dict" in drf_error:
            error_codes = {}
            for field, errors in drf_error.error_dict.items():
                error_codes[camelize(field)] = [error.code for error in errors]
            raise ValidationError(error_codes)
        else:
            raise ValidationError(drf_error.code)
