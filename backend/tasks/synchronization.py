# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2024 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from celery import shared_task
from rdflib import Graph

from ..decorators.rdf_document_dataset import RDFDocumentDecorator
from ..models.author import Author
from ..models.department import Department
from ..models.rdf_document import RDFDocument
from ..namespaces import Namespaces
from ..services.triple_store import TripleStore


@shared_task
def synchronize_authors():
    graph = Graph()

    queryset = Author.objects.all()

    for instance in queryset.all():
        instance.add_to_graph(graph)

    iri = Namespaces().authors()

    triple_store = TripleStore()
    triple_store.put_graph(graph, iri=iri)


@shared_task
def synchronize_departments():
    graph = Graph()

    queryset = Department.objects.all()

    for instance in queryset.all():
        instance.add_to_graph(graph)

    iri = Namespaces().departments()

    triple_store = TripleStore()
    triple_store.put_graph(graph, iri=iri)


@shared_task
def synchronize_rdf_documents():
    for rdf_document in RDFDocument.objects.all():
        decorated = RDFDocumentDecorator(rdf_document)
        triple_store = TripleStore()
        for graph in decorated.dataset.contexts():
            triple_store.put_graph(graph)
