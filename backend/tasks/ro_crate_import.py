# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2024 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.


from datetime import datetime

from celery import shared_task
from celery.utils.log import get_task_logger
from rest_framework.exceptions import APIException, ParseError, ValidationError

from ..models.ro_crate_import import RoCrateImport
from ..services.process_ro_crate_import import ProcessRoCrateImportService


@shared_task
def process_ro_crate_import(ro_crate_import_id):
    ro_crate_import = RoCrateImport.objects.get(id=ro_crate_import_id)

    if ro_crate_import.status != RoCrateImport.Status.PENDING:
        raise Exception(f"RoCrateImport {ro_crate_import.id} has already been processed.")

    ro_crate_import.status = RoCrateImport.Status.STARTED
    ro_crate_import.started_at = datetime.now()
    ro_crate_import.save()

    try:
        ProcessRoCrateImportService(
            ro_crate_import.created_by,
            get_task_logger(__name__),
        ).process_ro_crate_import(ro_crate_import)
    except (ParseError, ValidationError) as e:
        ro_crate_import.status = RoCrateImport.Status.FAILURE
        ro_crate_import.stopped_at = datetime.now()
        ro_crate_import.save()
        raise Exception("RoCrateImport failed") from e
    except APIException as e:
        ro_crate_import.status = RoCrateImport.Status.FAILURE
        ro_crate_import.stopped_at = datetime.now()
        ro_crate_import.save()
        raise Exception("RoCrateImport failed") from e

    ro_crate_import.status = RoCrateImport.Status.SUCCESS
    ro_crate_import.stopped_at = datetime.now()
    ro_crate_import.save()
