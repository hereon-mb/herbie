# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

import inspect

import pytest
import sqlparse  # type: ignore

from .fixtures.author import *
from .fixtures.cache_reset import *
from .fixtures.db import *
from .fixtures.department import *
from .fixtures.rdf_document import *
from .fixtures.request import *
from .fixtures.triple_store import *
from .fixtures.user import *
from .fixtures.workspace import *
from .matchers import Matcher


def print_query(query):
    print(sqlparse.format(str(query.query), reindent=True, keyword_case="upper"))


def pytest_assertrepr_compare(op, left, right):
    if isinstance(left, dict) and isinstance(right, dict) and op == "==":
        diff, missing, additional, counts = compare_recursive(left, right)

        if diff or missing or additional or counts:
            lines = ["equality of dict instances:"]

            for left, right, path in diff:
                if inspect.isclass(right) and issubclass(right, Matcher):
                    lines.append(f"{right.__name__} must be called at {path}")
                else:
                    lines.append(f"assert {left.__repr__()} == {right.__repr__()} at {path}")

            for keys, path in missing:
                keys_str = ", ".join([key.__repr__() for key in keys])
                lines.append(f"assert values for {keys_str} at {path}")

            for keys, path in additional:
                keys_str = ", ".join([key.__repr__() for key in keys])
                lines.append(f"assert no values for {keys_str} at {path}")

            for left, right, path in counts:
                lines.append(f"assert {left} == {right} number of list elements at {path}")

            return lines


def compare_recursive(left, right, path=None):
    if path is None:
        path = []

    path_str = f"{'.'.join(path)}"

    if isinstance(left, dict) and isinstance(right, dict):
        diff = []
        missing = []
        additional = []
        counts = []

        for key in left.keys():
            if key in right.keys():
                diff_new, missing_new, additional_new, counts_new = compare_recursive(left[key], right[key], path=path + [key])
                diff += diff_new
                missing += missing_new
                additional += additional_new
                counts += counts_new

        if keys := left.keys() - right.keys():
            missing.append((keys, path_str))
        if keys := right.keys() - left.keys():
            additional.append((keys, path_str))

        return diff, missing, additional, counts

    elif isinstance(left, list) and isinstance(right, list):
        diff = []
        missing = []
        additional = []
        counts = []

        if len(left) != len(right):
            counts.append((len(left), len(right), path_str))
        else:
            for index in range(len(left)):
                diff_new, missing_new, additional_new, counts_new = compare_recursive(
                    left[index], right[index], path=path + [f"[{index}]"]
                )
                diff += diff_new
                missing += missing_new
                additional += additional_new
                counts += counts_new

        return diff, missing, additional, counts

    elif left != right:
        return [(left, right, path_str)], [], [], []

    else:
        return [], [], [], []


def pytest_addoption(parser):
    parser.addoption("--runexplorative", action="store_true", default=False, help="run explorative tests")


def pytest_configure(config):
    config.addinivalue_line("markers", "exlorative: mark test as explorative")


def pytest_collection_modifyitems(config, items):
    if config.getoption("--runexplorative"):
        # --runslow given in cli: do not skip slow tests
        return
    skip_explorative = pytest.mark.skip(reason="need --runexplorative option to run")
    for item in items:
        if "explorative" in item.keywords:
            item.add_marker(skip_explorative)
