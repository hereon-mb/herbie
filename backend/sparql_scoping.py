import itertools

from pyparsing.exceptions import ParseException
from rdflib import URIRef, Variable
from rdflib.plugins.sparql.algebra import (
    BGP,
    Graph,
    Join,
    Query,
    ToMultiSet,
    _addVars,
    _AlgebraTranslator,
    _traverseAgg,
    translateQuery,
)
from rdflib.plugins.sparql.parser import parseQuery
from rdflib.plugins.sparql.parserutils import CompValue
from rest_framework.exceptions import ValidationError

from .queries import select_documents_published
from .sparql import Var


def scope_query(raw, workspace):
    try:
        query = translateQuery(parseQuery(raw))
    except ParseException as exc:
        raise ValidationError("could not parse query") from exc

    try:
        algebra_scoped = scope_algebra(query.algebra, workspace)
        _traverseAgg(algebra_scoped, _addVars)
    except NotImplementedError as error:
        raise ValidationError(f"unsupported query: {error}") from error

    return translateAlgebra(Query(query.prologue, algebra_scoped))


def scope_algebra(algebra, workspace):
    if algebra.name == "SelectQuery":
        algebra["p"] = scope_select_query(algebra.p, workspace)
        return algebra
    else:
        raise NotImplementedError(f"unsupported algebra: {algebra.name}")


def scope_select_query(p, workspace):
    if p.name == "Slice":
        p["p"] = scope_slice(p.p, workspace)
        return p
    elif p.name == "Project":
        p["p"] = scope_project(p.p, p.PV, workspace)
        return p
    else:
        raise NotImplementedError(f"{p.name} must not be a child of SelectQuery")


def scope_slice(p, workspace):
    if p.name == "Distinct":
        p["p"] = scope_distinct(p.p, workspace)
        return p
    elif p.name == "Project":
        p["p"] = scope_project(p.p, p.PV, workspace)
        return p
    else:
        raise NotImplementedError(f"{p.name} must not be a child of Slice")


def scope_project(p, PV, workspace):
    if p.name == "OrderBy":
        p["p"] = scope_graph_patterns(p.p, workspace)
    elif p.name == "Filter":
        p["p"] = scope_graph_patterns(p.p, workspace)
    elif p.name == "Join":
        p["p1"] = scope_graph_patterns(p.p1, workspace)
        p["p2"] = scope_graph_patterns(p.p2, workspace)
    elif p.name == "LeftJoin":
        p["p1"] = scope_graph_patterns(p.p1, workspace)
        p["p2"] = scope_graph_patterns(p.p2, workspace)
    elif p.name == "Union":
        p["p1"] = scope_graph_patterns(p.p1, workspace)
        p["p2"] = scope_graph_patterns(p.p2, workspace)
    elif p.name == "Extend":
        p["p"] = scope_graph_patterns(p.p, workspace)
    elif p.name == "BGP":
        p = scope_bgp(p, workspace)
    else:
        raise NotImplementedError(f"{p.name} must not be a child of Project")

    return p


def scope_distinct(p, workspace):
    if p.name == "Project":
        p["p"] = scope_project(p.p, p.PV, workspace)
        return p
    else:
        raise NotImplementedError(f"{p.name} must not be a child of Slice")


def scope_graph_patterns(p, workspace):
    if p.name == "Filter":
        p["p"] = scope_graph_patterns(p.p, workspace)
        return p
    elif p.name == "Join":
        p["p1"] = scope_graph_patterns(p.p1, workspace)
        p["p2"] = scope_graph_patterns(p.p2, workspace)
        return p
    elif p.name == "LeftJoin":
        p["p1"] = scope_graph_patterns(p.p1, workspace)
        p["p2"] = scope_graph_patterns(p.p2, workspace)
        return p
    elif p.name == "Union":
        p["p1"] = scope_graph_patterns(p.p1, workspace)
        p["p2"] = scope_graph_patterns(p.p2, workspace)
        return p
    elif p.name == "BGP":
        return scope_bgp(p, workspace)
    elif p.name == "ToMultiSet":
        return p
    else:
        raise NotImplementedError(f"{p.name} must not be a child of OrderBy")


def scope_bgp(bgp, workspace, group_by_subject=True):
    if len(bgp.triples) == 0:
        return bgp
    elif group_by_subject:
        triples_sorted = sorted(bgp.triples, key=subject_from_triple)
        grouped_triples = itertools.groupby(triples_sorted, key=subject_from_triple)
        return scope_grouped_triples([list(group) for _, group in grouped_triples], workspace)
    else:
        return scope_triples(bgp.triples, workspace)


def subject_from_triple(triple):
    return triple[0].__str__()


def scope_grouped_triples(grouped_triples, workspace):
    scoped_groups = []

    for group in grouped_triples:
        document_version = Var("documentVersion")
        scope = select_documents_published(
            iri_workspace=workspace,
            selected=[document_version],
            document_version_published=document_version,
        )
        scope = translateQuery(parseQuery(str(scope))).algebra

        scoped_groups.append(
            Graph(
                term=URIRef(workspace),  # Variable(str(document_version)),
                graph=BGP(triples=group),
            ),
        )

    return JoinMany(scoped_groups)


def JoinMany(ps):
    if len(ps) == 0:
        raise NotImplementedError("empty pairs")

    first = ps[0]
    rest = ps[1:]

    if len(rest) > 0:
        return Join(
            p1=first,
            p2=JoinMany(rest),
        )
    else:
        return first


def scope_triples(triples, workspace):
    if len(triples) == 0:
        return triples

    triple = triples[0]
    rest = triples[1:]

    document_version = Var("documentVersion")
    scope = select_documents_published(
        iri_workspace=workspace,
        selected=[document_version],
        document_version_published=document_version,
    )
    scope = translateQuery(parseQuery(str(scope))).algebra
    if len(rest) > 0:
        return Join(
            p1=Join(
                p1=Graph(
                    term=Variable(str(document_version)),
                    graph=BGP(triples=[triple]),
                ),
                p2=ToMultiSet(
                    p=scope.p,
                ),
            ),
            p2=scope_triples(rest, workspace),
        )
    else:
        return Join(
            p1=Graph(
                term=Variable(str(document_version)),
                graph=BGP(triples=[triple]),
            ),
            p2=ToMultiSet(
                p=scope.p,
            ),
        )


class AlgebraTranslator(_AlgebraTranslator):
    def sparql_query_text(self, node):
        if isinstance(node, CompValue) and node.name == "UnaryNot":
            self._replace("{UnaryNot}", "!(" + self.convert_node_arg(node.expr) + ")")
        else:
            return super().sparql_query_text(node)


def translateAlgebra(query_algebra: Query) -> str:
    query_from_algebra = AlgebraTranslator(query_algebra=query_algebra).translateAlgebra()
    return query_from_algebra
