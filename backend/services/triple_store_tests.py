from django.conf import settings
from rdflib import FOAF, Graph, URIRef


def test__put_graph(request, triple_store):
    graph = Graph()

    alice = URIRef(f"{settings.IRI_PREFIX}alice")
    bob = URIRef(f"{settings.IRI_PREFIX}bob")
    graph.add((alice, FOAF.knows, bob))

    iri = settings.IRI_PREFIX
    triple_store.put_graph(graph, iri=iri)
    request.addfinalizer(lambda: triple_store.delete_graph(iri))

    result = triple_store.query(
        f"""
            SELECT ?graph (COUNT(*) as ?count) WHERE {{
                GRAPH ?graph {{ ?s ?p ?o }}
                VALUES ?graph {{ <{settings.IRI_PREFIX}> }}
            }}
            GROUP BY ?graph
        """
    )

    assert {
        "graph": {"type": "uri", "value": settings.IRI_PREFIX},
        "count": {"datatype": "http://www.w3.org/2001/XMLSchema#integer", "type": "typed-literal", "value": "1"},
    } in result["results"]["bindings"]
