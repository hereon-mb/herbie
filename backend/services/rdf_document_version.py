# type: ignore
import re

from django.conf import settings
from django.db.models import Prefetch, Subquery
from rdflib import URIRef
from rest_framework.exceptions import NotFound

from ..access_policies.workspace import WorkspaceAccessPolicy
from ..models.rdf_document import RDFDocument, RDFDocumentVersion
from ..models.workspace import Workspace
from ..namespaces import Namespaces
from ..queries import select_document_version_published_via_iri_alias
from ..services.triple_store import TripleStore
from ..sparql import Var


class RDFDocumentVersionService:
    def __init__(self, author) -> None:
        self._author = author

    def get(self, iri, workspace) -> RDFDocumentVersion:
        iri_workspace = Namespaces().workspaces(workspace.uuid)
        result = TripleStore().query(
            select_document_version_published_via_iri_alias(
                iri_workspace=iri_workspace,
                iri=iri,
                document_version=Var("documentVersion", randomize=False),
            )
        )
        bindings = result["results"]["bindings"]
        if len(bindings) == 0:
            raise NotFound()

        document_version = bindings[0]["documentVersion"]["value"]

        return self.get_version(document_version)

    def get_version_iri(self, iri, workspace):
        iri_workspace = Namespaces().workspaces(workspace.uuid)
        result = TripleStore().query(
            select_document_version_published_via_iri_alias(
                iri_workspace=iri_workspace,
                iri=iri,
                document_version=Var("documentVersion", randomize=False),
            )
        )
        bindings = result["results"]["bindings"]
        if len(bindings) == 0:
            raise NotFound()

        document_version = bindings[0]["documentVersion"]["value"]

        if self.get_version(document_version) is None:
            raise NotFound()

        return URIRef(document_version)

    def get_version(self, document_version) -> RDFDocumentVersion:
        uuid, index = parse_iri_version(document_version)

        try:
            workspaces = Workspace.objects.all()
            workspaces = WorkspaceAccessPolicy.scope_queryset(self._author, workspaces)
            version = (
                RDFDocumentVersion.objects.all()
                .filter(
                    rdf_document=Subquery(RDFDocument.objects.all().filter(uuid=uuid).values("pk")[:1]),
                    rdf_document__workspace__in=workspaces,
                )
                .published()
                .select_related("created_by")
                .select_related("created_by__user")
                .select_related("rdf_document")
                .prefetch_related(
                    Prefetch(
                        "rdf_document__rdf_document_versions",
                        RDFDocumentVersion.objects.all().select_related("created_by").select_related("created_by__user"),
                    )
                )
            )[index - 1]
        except IndexError:
            raise NotFound()

        if version is None:
            raise NotFound()

        return version


PATTERN_SLUG = "[-a-zA-Z0-9]+"
PATTERN_INT = "[0-9]+"
PATTERN_RDF_DOCUMENT_VERSION = f"{settings.IRI_PREFIX}datasets/(?P<uuid>{PATTERN_SLUG})/versions/(?P<index>{PATTERN_INT})/"


def parse_iri_version(iri):
    if (match := re.match(PATTERN_RDF_DOCUMENT_VERSION, iri)) is None:
        raise NotFound()

    return match.group("uuid"), int(match.group("index"))
