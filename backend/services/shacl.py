from typing import Optional

from pyparsing.exceptions import ParseException
from pyshacl import validate
from pyshacl.errors import RuleLoadError, ShapeLoadError
from pyshacl.rdfutil import clone_graph
from rdflib import Dataset, Graph
from rest_framework.exceptions import ValidationError


class ShaclError(Exception):
    def __init__(self, report, *args):
        self.report = report
        super().__init__(*args)


class ShaclService:
    def __init__(
        self,
        graph_shacl: Optional[Graph] = None,
        graph_ont: Optional[Graph] = None,
    ):
        self._graph_shacl = graph_shacl
        self._graph_ont = graph_ont

    def generate(
        self,
        graph_original: Graph,
        only_persist: bool = False,
    ) -> Graph:
        if only_persist:
            return Graph()

        if self._graph_shacl is None:
            return Graph()

        try:
            dataset = Dataset()
            graph_data = dataset.graph()
            clone_graph(graph_original, graph_data)
            if self._graph_ont:
                clone_graph(self._graph_ont, graph_data)

            _, _, _ = validate(
                dataset,
                shacl_graph=self._graph_shacl,
                ont_graph=self._graph_ont,
                advanced=True,
                debug=False,
                inplace=True,
                iterate_rules=True,
            )
        except ShapeLoadError as error:
            raise ValidationError(f"ShapeLoadError: {error}", code="shape_load_error")
        except RuleLoadError as error:
            raise ValidationError(f"RuleLoadError: {error}", code="shape_load_error")
        except ParseException as error:
            raise ValidationError(f"RuleLoadError: {error}", code="shape_load_error")

        return dataset.graph(dataset.default_context)

    def validate(self, graph_original: Graph):
        if self._graph_shacl:
            graph = Graph()

            clone_graph(graph_original, graph)

            valid, graph_report, _ = validate(
                graph,
                shacl_graph=self._graph_shacl,
                ont_graph=self._graph_ont,
                advanced=True,
                debug=False,
                inplace=True,
                iterate_rules=True,
            )
            if not valid:
                raise ShaclError(graph_report)
