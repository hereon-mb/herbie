from rdflib import RDF, RDFS, Literal, Namespace

from ..sparql import BlankNode, Construct, GraphPattern, Select, Values, Var
from .triple_store import TripleStore
from .virtual_graph import VirtualGraphService

EX = Namespace("http://example.org/")


def test__create(graph_empty, graph_from):
    iri = graph_empty()
    graph_a = graph_from([(EX.alice, RDF.type, EX.Person)])
    graph_b = graph_from([(EX.bob, RDF.type, EX.Person)])

    def select(var_graph):
        return Select(var_graph).where(Values(var_graph, graph_a, graph_b))

    service = VirtualGraphService(iri, select)
    service.create()

    assert _triples_count(service.iri) == 2
    assert _triples_count(service.iri_provenance) == 10


def test__create_with_blank_node(graph_empty, graph_from):
    iri = graph_empty()
    bob = BlankNode("bob")
    graph = graph_from(
        [
            (EX.alice, EX.knows, bob),
            (bob, RDF.type, EX.Person),
        ]
    )

    def select(var_graph):
        return Select(var_graph).where(Values(var_graph, graph))

    # Note: we set the limit to 1 to force the update query to be split into
    # two queries, which work on the same blank node (bob).  This test will
    # only pass, if the used triple store keeps the identity of the blank node.
    service = VirtualGraphService(iri, select, limit_triples=1)
    service.create()

    assert _triples_count(service.iri) == 2
    assert _triples_count(service.iri_provenance) == 10


def test__insert_graph(graph_empty, graph_from):
    iri = graph_empty()
    graph_a = graph_from([(EX.alice, RDF.type, EX.Person)])
    graph_b = graph_from([(EX.bob, RDF.type, EX.Person)])

    def select(var_graph):
        return Select(var_graph).where(Values(var_graph, graph_a))

    service = VirtualGraphService(iri, select)
    service.create()
    assert _triples_count(service.iri) == 1
    assert _triples_count(service.iri_provenance) == 5

    service.insert_graph(graph_b)
    assert _triples_count(service.iri) == 2
    assert _triples_count(service.iri_provenance) == 10


def test__insert_graph__with_low_limit(graph_empty, graph_from):
    iri = graph_empty()
    graph_a = graph_from([(EX.alice, RDF.type, EX.Person)])
    cindi = BlankNode("cindi")
    graph_b = graph_from(
        [
            (EX.bob, RDF.type, EX.Person),
            (EX.bob, EX.knows, cindi),
            (cindi, RDF.type, EX.Person),
        ]
    )

    def select(var_graph):
        return Select(var_graph).where(Values(var_graph, graph_a))

    service = VirtualGraphService(iri, select, limit_triples=1)
    service.create()

    assert _triples_count(service.iri) == 1
    assert _triples_count(service.iri_provenance) == 5

    service.insert_graph(graph_b)
    assert _triples_count(service.iri) == 4
    assert _triples_count(service.iri_provenance) == 20


def test__remove_graph(graph_empty, graph_from):
    iri = graph_empty()
    graph_a = graph_from(
        [
            (EX.alice, RDF.type, EX.Person),
            (EX.alice, RDFS.label, Literal("Alice Wonderland")),
        ]
    )
    graph_b = graph_from(
        [
            (EX.bob, RDF.type, EX.Person),
            (EX.bob, RDFS.label, Literal("Bob Builder")),
        ]
    )

    def select(var_graph):
        return Select(var_graph).where(Values(var_graph, graph_a, graph_b))

    service = VirtualGraphService(iri, select)
    service.create()
    assert _triples_count(service.iri) == 4
    assert _triples_count(service.iri_provenance) == 20

    service.remove_graph(graph_b)
    assert _triples_count(service.iri) == 2
    assert _triples_count(service.iri_provenance) == 10


def test__remove_graph__with_low_limit(graph_empty, graph_from):
    iri = graph_empty()
    graph_a = graph_from(
        [
            (EX.alice, RDF.type, EX.Person),
            (EX.alice, RDFS.label, Literal("Alice Wonderland")),
        ]
    )
    cindi = BlankNode("cindi")
    graph_b = graph_from(
        [
            (EX.bob, RDF.type, EX.Person),
            (EX.bob, RDFS.label, Literal("Bob Builder")),
            (EX.bob, EX.knows, cindi),
            (cindi, RDF.type, EX.Person),
            (cindi, RDFS.label, Literal("Cindi Mayweather")),
        ]
    )

    def select(var_graph):
        return Select(var_graph).where(Values(var_graph, graph_a, graph_b))

    service = VirtualGraphService(iri, select, limit_triples=10)
    service.create()
    assert _triples_count(service.iri) == 7
    assert _triples_count(service.iri_provenance) == 35

    service.remove_graph(graph_b)
    assert _triples_count(service.iri) == 2
    assert _triples_count(service.iri_provenance) == 10


def test__remove_graph__whose_triples_are_added_twice(graph_empty, graph_from):
    iri = graph_empty()
    graph_a = graph_from([(EX.alice, RDF.type, EX.Person)])
    graph_b_one = graph_from([(EX.bob, RDF.type, EX.Person)])
    graph_b_two = graph_from([(EX.bob, RDF.type, EX.Person)])

    def select(var_graph):
        return Select(var_graph).where(
            Values(
                var_graph,
                graph_a,
                graph_b_one,
                graph_b_two,
            )
        )

    service = VirtualGraphService(iri, select)
    service.create()
    assert _triples_count(service.iri) == 2
    assert _triples_count(service.iri_provenance) == 15

    service.remove_graph(graph_b_one)
    assert _triples_count(service.iri) == 2
    assert _triples_count(service.iri_provenance) == 10


def _get_graph(iri):
    s, p, o = Var("s"), Var("p"), Var("o")
    return TripleStore().construct(
        Construct(
            (s, p, o),
        ).where(
            GraphPattern(
                iri,
                [(s, p, o)],
            ),
        )
    )


def _triples_count(iri):
    s, p, o = Var("s"), Var("p"), Var("o")
    response = TripleStore().query(
        Select("COUNT(*) AS ?count").where(
            GraphPattern(
                iri,
                [(s, p, o)],
            ),
        )
    )
    return int(response["results"]["bindings"][0]["count"]["value"])
