# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from datetime import datetime

import pytest
from freezegun import freeze_time

from ..models.author import Author
from ..models.workspace import MembershipType, Workspace
from .workspace import (
    CannotDeleteMembershipOfDeletedWorkspace,
    CannotDeleteOwnMembership,
    MembershipForAuthorAlreadyExists,
    WorkspaceAlreadyDeleted,
    WorkspaceService,
)


@freeze_time("2020-11-02")
def test__create_workspace(request, http_request__alice, author_alice):
    workspace = WorkspaceService(http_request__alice).create_workspace(
        slug="workspace-a",
        name="Workspace A",
        description="The Workspace A description",
    )
    request.addfinalizer(lambda: workspace.delete())

    now = datetime(2020, 11, 2)
    assert workspace
    assert workspace.created_at == now
    assert workspace.updated_at == now
    assert workspace.deleted_at is None
    assert workspace.created_by == author_alice
    assert workspace.uuid != ""
    assert workspace.slug == "workspace-a"
    assert workspace.name == "Workspace A"
    assert workspace.description == "The Workspace A description"
    assert workspace.memberships.count() == 1
    membership = workspace.memberships.first()
    assert membership.created_at == now
    assert membership.updated_at == now
    assert membership.deleted_at is None
    assert membership.created_by == author_alice
    assert membership.author == author_alice
    assert membership.membership_type == MembershipType.OWNER


@freeze_time("2020-11-02", as_kwarg="frozen_time")
def test__update_workspace__change_slug(http_request__alice, create_workspace, **kwargs):
    frozen_time = kwargs["frozen_time"]

    workspace = create_workspace()
    frozen_time.tick(1)
    workspace = WorkspaceService(http_request__alice).update_workspace(workspace, slug="new-slug")

    assert workspace
    assert workspace.created_at == datetime(2020, 11, 2)
    assert workspace.updated_at == datetime(2020, 11, 2, second=1)
    assert workspace.slug == "new-slug"


@freeze_time("2020-11-02", as_kwarg="frozen_time")
def test__update_workspace__change_name(http_request__alice, create_workspace, **kwargs):
    frozen_time = kwargs["frozen_time"]

    workspace = create_workspace()
    frozen_time.tick(1)
    workspace = WorkspaceService(http_request__alice).update_workspace(workspace, name="New Name")

    assert workspace
    assert workspace.created_at == datetime(2020, 11, 2)
    assert workspace.updated_at == datetime(2020, 11, 2, second=1)
    assert workspace.name == "New Name"


@freeze_time("2020-11-02", as_kwarg="frozen_time")
def test__update_workspace__change_description(http_request__alice, create_workspace, **kwargs):
    frozen_time = kwargs["frozen_time"]

    workspace = create_workspace()
    frozen_time.tick(1)
    workspace = WorkspaceService(http_request__alice).update_workspace(workspace, description="New Description")

    assert workspace
    assert workspace.created_at == datetime(2020, 11, 2)
    assert workspace.updated_at == datetime(2020, 11, 2, second=1)
    assert workspace.description == "New Description"


@freeze_time("2020-11-02", as_kwarg="frozen_time")
def test__delete_workspace(http_request__alice, create_workspace, **kwargs):
    frozen_time = kwargs["frozen_time"]

    workspace = create_workspace()
    frozen_time.tick(1)
    workspace = WorkspaceService(http_request__alice).delete_workspace(workspace)

    assert workspace
    assert workspace.created_at == datetime(2020, 11, 2)
    assert workspace.updated_at == datetime(2020, 11, 2, second=1)
    assert workspace.deleted_at == datetime(2020, 11, 2, second=1)


def test__delete_workspace__when_already_deleted(http_request__alice, create_workspace):
    workspace = create_workspace()
    WorkspaceService(http_request__alice).delete_workspace(workspace)
    with pytest.raises(WorkspaceAlreadyDeleted):
        WorkspaceService(http_request__alice).delete_workspace(workspace)


@freeze_time("2020-11-02")
def test__add_membership(http_request__alice, author_alice, author_bob, create_workspace):
    workspace = create_workspace()
    membership = WorkspaceService(http_request__alice).add_membership(
        workspace,
        author_bob,
        membership_type=MembershipType.GUEST,
    )

    now = datetime(2020, 11, 2)
    assert membership
    assert membership.created_at == now
    assert membership.updated_at == now
    assert membership.deleted_at is None
    assert membership.created_by == author_alice
    assert membership.author == author_bob
    assert membership.membership_type == MembershipType.GUEST


def test__add_membership__when_already_exists(http_request__alice, author_bob, create_workspace):
    workspace = create_workspace()
    WorkspaceService(http_request__alice).add_membership(workspace, author_bob, membership_type=MembershipType.GUEST)
    with pytest.raises(MembershipForAuthorAlreadyExists):
        WorkspaceService(http_request__alice).add_membership(workspace, author_bob, membership_type=MembershipType.GUEST)


@freeze_time("2020-11-02", as_kwarg="frozen_time")
def test__update_membership(http_request__alice, author_bob, create_workspace, add_membership, **kwargs):
    frozen_time = kwargs["frozen_time"]

    workspace = create_workspace()
    membership = add_membership(workspace, author_bob, MembershipType.GUEST)
    frozen_time.tick(1)
    membership = WorkspaceService(http_request__alice).update_membership(
        membership,
        membership_type=MembershipType.EDITOR,
    )

    assert membership
    assert membership.created_at == datetime(2020, 11, 2)
    assert membership.updated_at == datetime(2020, 11, 2, second=1)
    assert membership.membership_type == MembershipType.EDITOR


@freeze_time("2020-11-02", as_kwarg="frozen_time")
def test__delete_membership(http_request__alice, author_bob, create_workspace, add_membership, **kwargs):
    frozen_time = kwargs["frozen_time"]

    workspace = create_workspace()
    membership = add_membership(workspace, author_bob, MembershipType.GUEST)
    frozen_time.tick(1)
    membership = WorkspaceService(http_request__alice).delete_membership(membership)

    assert membership
    assert membership.created_at == datetime(2020, 11, 2)
    assert membership.updated_at == datetime(2020, 11, 2, second=1)
    assert membership.deleted_at == datetime(2020, 11, 2, second=1)


def test__delete_membership__when_workspace_is_deleted(http_request__alice, author_bob, create_workspace, add_membership):
    workspace = create_workspace()
    membership = add_membership(workspace, author_bob, MembershipType.GUEST)
    WorkspaceService(http_request__alice).delete_workspace(workspace)
    with pytest.raises(CannotDeleteMembershipOfDeletedWorkspace):
        WorkspaceService(http_request__alice).delete_membership(membership)


def test__delete_membership__when_own_membership(http_request__alice, create_workspace):
    workspace = create_workspace()
    membership = workspace.memberships.first()
    with pytest.raises(CannotDeleteOwnMembership):
        WorkspaceService(http_request__alice).delete_membership(membership)


@pytest.fixture
def add_membership(http_request__alice):
    memberships = []

    def _add_membership(workspace: Workspace, author: Author, membership_type: MembershipType):
        membership = WorkspaceService(http_request__alice).add_membership(workspace, author, membership_type)
        memberships.append(membership)
        return membership

    yield _add_membership

    for membership in memberships:
        membership.delete()
