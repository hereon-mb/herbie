# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from ..namespaces import Namespaces
from ..queries import select_documents_published
from ..sparql import GraphPattern, Select
from .virtual_graph import VirtualGraphService


class WorkspaceGraphService:
    def __init__(self, workspace, logger=None):
        self._iri_workspace = Namespaces().workspaces(workspace.uuid)
        self._virtual_graph_service = VirtualGraphService(
            self._iri_workspace,
            self._get_document_versions,
            logger=logger,
        )

    def delete(self):
        self._virtual_graph_service.delete()

    def create(self):
        self._virtual_graph_service.create()

    def insert_document(self, document_version_published):
        self._virtual_graph_service.insert_graph(document_version_published)

    def remove_document(self, document_version_published):
        self._virtual_graph_service.remove_graph(document_version_published)

    def _get_document_versions(self, document_version_published):
        return Select(document_version_published).where(
            GraphPattern(document_version_published, []),
            select_documents_published(
                iri_workspace=self._iri_workspace,
                selected=[document_version_published],
                document_version_published=document_version_published,
            ).order_by(str(document_version_published)),
        )
