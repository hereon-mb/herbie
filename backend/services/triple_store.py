import json
import logging
from json.decoder import JSONDecodeError
from urllib.error import HTTPError, URLError
from urllib.parse import urlencode, urlparse
from urllib.request import (
    HTTPBasicAuthHandler,
    HTTPDigestAuthHandler,
    HTTPPasswordMgrWithDefaultRealm,
    Request,
    build_opener,
)

from django.conf import settings
from rdflib import Graph
from rdflib.exceptions import ParserError
from rdflib.plugins.parsers.notation3 import BadSyntax
from rest_framework import status
from rest_framework.exceptions import APIException

from ..sparql import Create, Delete, Drop, Insert, InsertData, Select


class TripleStore:
    def __init__(self) -> None:
        self.use_triple_store = False

        if settings.TRIPLE_STORE_PATH:
            self.use_triple_store = True
            self.triple_store_path = settings.TRIPLE_STORE_PATH

    def put_graph(self, graph, iri=None):
        if not self.use_triple_store:
            return

        if iri is None:
            iri = graph.identifier

        query = {
            "graph": iri.__str__(),
        }
        url = urlparse(settings.TRIPLE_STORE_PATH)
        url = url._replace(query=urlencode(query))

        req = Request(
            method="PUT",
            headers={
                "Content-Type": "text/turtle",
            },
            url=url.geturl(),
            data=graph.serialize(format="turtle").encode("utf-8"),
        )
        try:
            with self._opener().open(req):
                pass
        except HTTPError as error:
            self._raise_error(
                f"could not PUT graph to triple store, iri={iri}, reason={error.reason}, headers={json.dumps(dict(error.headers))}"
            )
        except URLError:
            self._raise_error(f"could not PUT graph to triple store, iri={iri}, because of URLError")

    def delete_graph(self, iri):
        if not self.use_triple_store:
            return

        query = {
            "graph": iri.__str__(),
        }
        url = urlparse(settings.TRIPLE_STORE_PATH)
        url = url._replace(query=urlencode(query))

        req = Request(
            method="DELETE",
            headers={
                "Content-Type": "text/turtle",
            },
            url=url.geturl(),
        )
        try:
            with self._opener().open(req):
                pass
        except HTTPError as error:
            if error.getcode() == 404:
                pass
            else:
                self._raise_error(
                    f"could not DELETE graph from triple store, iri={iri}, reason={error.reason}, headers={json.dumps(dict(error.headers))}, url={url}"
                )
        except URLError:
            self._raise_error(f"could not DELETE graph to triple store, iri={iri}, because of URLError")

    def query(self, query, parse=True, accept="application/sparql-results+json"):
        if not self.use_triple_store:
            return {"results": {"bindings": []}}

        if isinstance(query, Select):
            query = str(query)

        headers = {
            "Accept": accept,
            "Content-Type": "application/x-www-form-urlencoded",
        }
        url = urlparse(settings.TRIPLE_STORE_QUERY_PATH)
        data = urlencode({"query": query}).encode("utf-8")

        req = Request(
            method="POST",
            headers=headers,
            url=url.geturl(),
            data=data,
        )

        try:
            response = self._opener().open(req)
        except HTTPError as error:
            body = error.read().decode()
            self._raise_error(
                f"could not query triple store, query={query}, reason={error.reason}, headers={json.dumps(dict(error.headers))}, body={body}"
            )
        except URLError as error:
            self._raise_error(f"could not query triple store, query={query}, because of URLError: {error}")
        body = response.read()

        if parse:
            try:
                result = json.loads(body.decode("utf-8"))
            except JSONDecodeError as error:
                self._raise_error(f"could not parse query result, body={body}, error={error}")
            return result
        else:
            return body

    def construct(self, query, graph=None):
        if not self.use_triple_store:
            return Graph()

        if isinstance(query, Select):
            query = str(query)

        headers = {
            "Accept": "text/turtle",
            "Content-Type": "application/x-www-form-urlencoded",
        }
        url = urlparse(settings.TRIPLE_STORE_QUERY_PATH)
        data = urlencode({"query": query}).encode("utf-8")

        req = Request(
            method="POST",
            headers=headers,
            url=url.geturl(),
            data=data,
        )

        try:
            response = self._opener().open(req)
        except ConnectionResetError as error:
            body = error.read().decode()
            self._raise_error(f"could not query triple store, query={query}, because of ConnectionResetError, body={body}")
        except HTTPError as error:
            if error.code == status.HTTP_404_NOT_FOUND:
                return Graph()
            elif error.code == status.HTTP_400_BAD_REQUEST:
                # FIXME We want to raise here, but this breaks some tests on CI.
                return Graph()
            else:
                body = error.read().decode()
                self._raise_error(
                    f"could not query triple store, query={query}, reason={error.reason}, headers={json.dumps(dict(error.headers))}, body={body}"
                )
        except URLError:
            self._raise_error(f"could not query triple store, query={query}, because of URLError")

        try:
            if graph is None:
                graph = Graph()
            graph = graph.parse(data=response.read().decode("latin-1"), format="turtle")
        except ParserError as error:
            self._raise_error(f"could not parse construct result, query={query}, because of ParserError: {error}")
        except BadSyntax as error:
            self._raise_error(f"could not parse construct result, query={query}, because of BadSyntax: {error}")

        return graph

    def update(self, query):
        if not self.use_triple_store:
            return

        if isinstance(query, (Create, Delete, Drop, Insert, InsertData)):
            query = str(query)

        headers = {
            "Accept": "text/turtle",
            "Content-Type": "application/sparql-query",
        }
        url = urlparse(settings.TRIPLE_STORE_QUERY_PATH)

        data = query.encode("utf-8")

        req = Request(
            method="POST",
            headers=headers,
            url=url.geturl(),
            data=data,
        )

        try:
            response = self._opener().open(req)
        except ConnectionResetError:
            self._raise_error(f"could not query triple store, query={query}, because of ConnectionResetError")
        except HTTPError as error:
            if error.code == status.HTTP_404_NOT_FOUND:
                return
            elif error.code == status.HTTP_400_BAD_REQUEST:
                body = error.read().decode()
                self._raise_error(
                    f"could not query triple store, query={query}, reason={error.reason}, headers={json.dumps(dict(error.headers))}, body={body}"
                )
            else:
                body = error.read().decode()
                self._raise_error(
                    f"could not query triple store, query={query}, reason={error.reason}, headers={json.dumps(dict(error.headers))}, body={body}"
                )
        except URLError:
            self._raise_error(f"could not query triple store, query={query}, because of URLError")

        try:
            graph = Graph().parse(data=response.read().decode("latin-1"), format="turtle")
        except ParserError as error:
            self._raise_error(f"could not parse construct result, query={query}, because of ParserError: {error}")
        except BadSyntax as error:
            self._raise_error(f"could not parse construct result, query={query}, because of BadSyntax: {error}")

        return graph

    def _opener(self):
        password_mgr = HTTPPasswordMgrWithDefaultRealm()
        top_level_url = urlparse(settings.TRIPLE_STORE_PATH)
        top_level_url = top_level_url._replace(path="")
        password_mgr.add_password(
            None,
            top_level_url.geturl(),
            settings.TRIPLE_STORE_USER,
            settings.TRIPLE_STORE_PASSWORD,
        )
        if settings.TRIPLE_STORE_AUTH_HANDLER == "basic":
            return build_opener(HTTPBasicAuthHandler(password_mgr))
        elif settings.TRIPLE_STORE_AUTH_HANDLER == "digest":
            return build_opener(HTTPDigestAuthHandler(password_mgr))

    def _raise_error(self, message):
        self._logger.error(message)
        raise APIException(message)

    @property
    def _logger(self):
        return logging.getLogger("django.server")
