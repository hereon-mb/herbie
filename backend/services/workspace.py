# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.


from datetime import datetime
from typing import Optional

from ..models.author import Author
from ..models.workspace import Membership, MembershipType, Workspace


class CannotUpdateDeletedWorkspace(Exception):
    pass


class WorkspaceAlreadyDeleted(Exception):
    pass


class MembershipForAuthorAlreadyExists(Exception):
    pass


class CannotUpdateDeletedMembership(Exception):
    pass


class CannotDeleteMembershipOfDeletedWorkspace(Exception):
    pass


class CannotDeleteOwnMembership(Exception):
    pass


class WorkspaceService:
    def __init__(self, request) -> None:
        self._request = request
        self._author, _ = Author.objects.get_or_create(user=self._request.user)

    def create_workspace(self, slug: str, name: str, description: str = ""):
        now = datetime.now()

        workspace = Workspace.objects.create(
            created_at=now,
            updated_at=now,
            created_by=self._author,
            slug=slug,
            name=name,
            description=description,
        )
        workspace.memberships.create(
            created_at=now,
            updated_at=now,
            created_by=self._author,
            author=self._author,
            membership_type=MembershipType.OWNER,
        )

        return workspace

    def update_workspace(
        self,
        workspace,
        slug: Optional[str] = None,
        name: Optional[str] = None,
        description: Optional[str] = None,
    ):
        now = datetime.now()

        if workspace.deleted_at is not None:
            raise CannotUpdateDeletedWorkspace()

        if slug is not None:
            workspace.slug = slug
        if name is not None:
            workspace.name = name
        if description is not None:
            workspace.description = description
        workspace.updated_at = now
        workspace.save()

        return workspace

    def delete_workspace(self, workspace: Workspace):
        now = datetime.now()

        if workspace.deleted_at is not None:
            raise WorkspaceAlreadyDeleted()

        workspace.updated_at = now
        workspace.deleted_at = now
        workspace.save()

        return workspace

    def add_membership(self, workspace: Workspace, author: Author, membership_type: MembershipType):
        now = datetime.now()

        if workspace.memberships.filter(author=author, deleted_at__isnull=True).exists():
            raise MembershipForAuthorAlreadyExists()

        membership = workspace.memberships.create(
            created_at=now,
            updated_at=now,
            created_by=self._author,
            author=author,
            membership_type=membership_type,
        )

        return membership

    def update_membership(self, membership, membership_type: Optional[MembershipType] = None):
        now = datetime.now()

        if membership.deleted_at is not None:
            raise CannotUpdateDeletedMembership()

        if membership_type is not None:
            membership.membership_type = membership_type
        membership.updated_at = now
        membership.save()

        return membership

    def delete_membership(self, membership: Membership):
        now = datetime.now()

        if membership.workspace.deleted_at is not None:
            raise CannotDeleteMembershipOfDeletedWorkspace()
        if membership.author == self._author:
            raise CannotDeleteOwnMembership()

        membership.updated_at = now
        membership.deleted_at = now
        membership.save()

        return membership
