def test__get(rdf_document_version_service, rdf_document__published_with_alias):
    rdf_document = rdf_document__published_with_alias()
    iri_alias = rdf_document.version_published.iri_aliases.first().iri
    assert rdf_document_version_service.get(iri_alias, rdf_document.workspace).rdf_document == rdf_document
