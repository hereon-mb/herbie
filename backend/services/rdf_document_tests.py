from rdflib import DCTERMS, RDFS, BNode, Graph, Literal, URIRef

from ..models.rdf_document import RDFDocument


def test__get_graph_with_imports__draft(request, clear_triple_store, rdf_document_service, rdf_document__unpublished):
    request.addfinalizer(lambda: clear_triple_store())
    rdf_document = rdf_document__unpublished()
    iri = URIRef(f"http://testserver/datasets/{rdf_document.uuid}/draft/")

    graph = rdf_document_service._get_graph_with_imports(iri)
    assert graph is not None


def test__get_graph_with_imports__version(request, clear_triple_store, rdf_document_service, rdf_document__published):
    request.addfinalizer(lambda: clear_triple_store())
    rdf_document = rdf_document__published()
    iri = URIRef(f"http://testserver/datasets/{rdf_document.uuid}/versions/1/")

    graph = rdf_document_service._get_graph_with_imports(iri)
    assert graph is not None


def test__create(request, rdf_document_service, author_alice, create_workspace):
    workspace = create_workspace()
    result = rdf_document_service.create(workspace=workspace)
    request.addfinalizer(lambda: result.rdf_document.delete())

    assert result.rdf_document is not None
    assert result.rdf_document.created_by == author_alice
    uuid = result.rdf_document.uuid
    assert len(result.updated) == 6
    assert URIRef(f"http://testserver/datasets/{uuid}/") in result.updated
    assert URIRef(f"http://testserver/datasets/{uuid}/draft/") in result.updated
    assert URIRef(f"http://testserver/datasets/{uuid}/draft/persisted/") in result.updated
    assert URIRef(f"http://testserver/datasets/{uuid}/draft/generated/") in result.updated
    assert len(result.deleted) == 0


def test__update(request, rdf_document_service, create_workspace):
    workspace = create_workspace()
    rdf_document = rdf_document_service.create(workspace=workspace).rdf_document
    request.addfinalizer(lambda: rdf_document.delete())
    uuid = rdf_document.uuid

    graph = Graph()
    triple = (BNode(), RDFS.label, Literal("something"))
    graph.add(triple)
    result = rdf_document_service.update(rdf_document, graph)
    rdf_document = result.rdf_document
    assert result.rdf_document is not None
    assert len(result.updated) == 3
    assert URIRef(f"http://testserver/datasets/{uuid}/") in result.updated
    assert URIRef(f"http://testserver/datasets/{uuid}/draft/") in result.updated
    assert triple in result.updated[URIRef(f"http://testserver/datasets/{uuid}/draft/")]
    assert len(result.deleted) == 0


def test__update_access(request, rdf_document_service, create_workspace):
    workspace = create_workspace()
    rdf_document = rdf_document_service.create(workspace=workspace).rdf_document
    request.addfinalizer(lambda: rdf_document.delete())
    uuid = rdf_document.uuid

    graph = Graph()
    triple = (BNode(), RDFS.label, Literal("something"))
    graph.add(triple)
    result = rdf_document_service.update_access(rdf_document, graph)
    rdf_document = result.rdf_document
    assert result.rdf_document is not None
    assert len(result.updated) == 2
    assert URIRef(f"http://testserver/datasets/{uuid}/") in result.updated
    assert URIRef(f"http://testserver/datasets/{uuid}/access/") in result.updated
    assert triple in result.updated[URIRef(f"http://testserver/datasets/{uuid}/access/")]
    assert len(result.deleted) == 0


def test__add_alias(request, rdf_document_service, create_workspace):
    workspace = create_workspace()
    result = rdf_document_service.create(workspace=workspace)
    result.sync_rdf_store()
    rdf_document = result.rdf_document
    request.addfinalizer(lambda: rdf_document.delete())
    uuid = rdf_document.uuid

    iri_alias = "http://example.org/alias/"
    result = rdf_document_service.add_alias(rdf_document, iri_alias)
    rdf_document = result.rdf_document
    iri_document = URIRef(f"http://testserver/datasets/{uuid}/")
    iri_draft = URIRef(f"http://testserver/datasets/{uuid}/draft/")
    assert len(result.updated) == 2
    assert iri_document in result.updated
    assert iri_draft in result.updated
    assert (URIRef(iri_draft), DCTERMS.source, URIRef(iri_alias)) in result.updated[iri_document]
    rdf_document = RDFDocument.objects.all().with_prefetches().get(pk=rdf_document.pk)  # type: ignore
    assert rdf_document.version_draft.iri_aliases.count() == 1
    assert rdf_document.version_draft.iri_aliases.first().iri == "http://example.org/alias/"
    assert len(result.deleted) == 0


def test__delete_alias(request, rdf_document_service, create_workspace):
    workspace = create_workspace()
    result = rdf_document_service.create(workspace=workspace)
    result.sync_rdf_store()
    rdf_document = result.rdf_document
    request.addfinalizer(lambda: rdf_document.delete())
    uuid = rdf_document.uuid

    iri = "http://example.org/alias/"
    rdf_document = rdf_document_service.add_alias(rdf_document, iri).rdf_document

    result = rdf_document_service.delete_alias(rdf_document, iri)
    rdf_document = result.rdf_document
    assert len(result.updated) == 2
    assert URIRef(f"http://testserver/datasets/{uuid}/") in result.updated
    assert URIRef(f"http://testserver/datasets/{uuid}/draft/") in result.updated
    assert rdf_document.version_draft.iri_aliases.count() == 0
    assert len(result.deleted) == 0


def test__cancel(rdf_document_service, create_workspace):
    workspace = create_workspace()
    rdf_document = rdf_document_service.create(workspace=workspace).rdf_document
    uuid = rdf_document.uuid

    result = rdf_document_service.cancel(rdf_document)
    rdf_document = result.rdf_document
    assert result.rdf_document is None
    assert len(result.updated) == 0
    assert len(result.deleted) == 6
    assert URIRef(f"http://testserver/datasets/{uuid}/") in result.deleted
    assert URIRef(f"http://testserver/datasets/{uuid}/draft/") in result.deleted
    assert URIRef(f"http://testserver/datasets/{uuid}/draft/persisted/") in result.deleted
    assert URIRef(f"http://testserver/datasets/{uuid}/draft/generated/") in result.deleted


def test__publish(request, rdf_document_service, create_workspace):
    workspace = create_workspace()
    rdf_document = rdf_document_service.create(workspace=workspace).rdf_document
    request.addfinalizer(lambda: rdf_document.delete())
    uuid = rdf_document.uuid

    result = rdf_document_service.publish(rdf_document)
    rdf_document = result.rdf_document
    assert result.rdf_document is not None
    assert len(result.updated) == 5
    assert URIRef(f"http://testserver/datasets/{uuid}/") in result.updated
    assert URIRef(f"http://testserver/datasets/{uuid}/versions/1/") in result.updated
    assert URIRef(f"http://testserver/datasets/{uuid}/versions/1/persisted/") in result.updated
    assert URIRef(f"http://testserver/datasets/{uuid}/versions/1/generated/") in result.updated
    assert len(result.deleted) == 4
    assert URIRef(f"http://testserver/datasets/{uuid}/draft/") in result.deleted
    assert URIRef(f"http://testserver/datasets/{uuid}/draft/persisted/") in result.deleted
    assert URIRef(f"http://testserver/datasets/{uuid}/draft/generated/") in result.deleted


def test__edit(request, rdf_document_service, create_workspace):
    workspace = create_workspace()
    rdf_document = rdf_document_service.create(workspace=workspace).rdf_document
    request.addfinalizer(lambda: rdf_document.delete())
    uuid = rdf_document.uuid

    rdf_document = rdf_document_service.publish(rdf_document).rdf_document
    result = rdf_document_service.edit(rdf_document)
    rdf_document = result.rdf_document
    assert result.rdf_document is not None
    assert len(result.updated) == 5
    assert URIRef(f"http://testserver/datasets/{uuid}/") in result.updated
    assert URIRef(f"http://testserver/datasets/{uuid}/draft/") in result.updated
    assert URIRef(f"http://testserver/datasets/{uuid}/draft/persisted/") in result.updated
    assert URIRef(f"http://testserver/datasets/{uuid}/draft/generated/") in result.updated
    assert len(result.deleted) == 0


def test__cancel__after_edit(request, rdf_document_service, create_workspace):
    workspace = create_workspace()
    rdf_document = rdf_document_service.create(workspace=workspace).rdf_document
    request.addfinalizer(lambda: rdf_document.delete())
    uuid = rdf_document.uuid

    rdf_document = rdf_document_service.publish(rdf_document).rdf_document
    rdf_document = rdf_document_service.edit(rdf_document).rdf_document

    result = rdf_document_service.cancel(rdf_document)
    rdf_document = result.rdf_document
    assert result.rdf_document is not None
    assert len(result.updated) == 1
    assert URIRef(f"http://testserver/datasets/{uuid}/") in result.updated
    assert len(result.deleted) == 4
    assert URIRef(f"http://testserver/datasets/{uuid}/draft/") in result.deleted
    assert URIRef(f"http://testserver/datasets/{uuid}/draft/persisted/") in result.deleted
    assert URIRef(f"http://testserver/datasets/{uuid}/draft/generated/") in result.deleted


def test__create_and_publish(request, rdf_document_service, create_workspace):
    workspace = create_workspace()
    iri_tmp = URIRef("http://example.org/tmp/")
    graph = Graph()
    graph.add((iri_tmp, RDFS.label, Literal("document")))
    result = rdf_document_service.create_and_publish(graph, workspace=workspace, iri_tmp=iri_tmp)
    request.addfinalizer(lambda: result.rdf_document.delete())
    uuid = result.rdf_document.uuid
    assert result.rdf_document is not None
    assert len(result.updated) == 6
    assert URIRef(f"http://testserver/datasets/{uuid}/") in result.updated
    assert URIRef(f"http://testserver/datasets/{uuid}/access/") in result.updated
    assert URIRef(f"http://testserver/datasets/{uuid}/versions/1/") in result.updated
    assert URIRef(f"http://testserver/datasets/{uuid}/versions/1/entered/") in result.updated
    assert URIRef(f"http://testserver/datasets/{uuid}/versions/1/persisted/") in result.updated
    assert URIRef(f"http://testserver/datasets/{uuid}/versions/1/generated/") in result.updated
    assert len(result.deleted) == 0
    iri_version = URIRef(f"http://testserver/datasets/{uuid}/versions/1/")
    graph_version = result.dataset.graph(iri_version)
    assert (iri_version, RDFS.label, Literal("document")) in graph_version


def test__edit_and_publish__unmodified(request, rdf_document_service, create_workspace):
    workspace = create_workspace()
    rdf_document = rdf_document_service.create(workspace=workspace).rdf_document
    request.addfinalizer(lambda: rdf_document.delete())

    rdf_document = rdf_document_service.publish(rdf_document).rdf_document
    result = rdf_document_service.edit_and_publish(rdf_document, Graph())
    rdf_document = result.rdf_document
    assert result.rdf_document is not None
    assert len(result.updated) == 0
    assert len(result.deleted) == 0


def test__edit_and_publish__modified(request, rdf_document_service, create_workspace):
    workspace = create_workspace()
    rdf_document = rdf_document_service.create(workspace=workspace).rdf_document
    request.addfinalizer(lambda: rdf_document.delete())
    uuid = rdf_document.uuid

    rdf_document = rdf_document_service.publish(rdf_document).rdf_document
    iri_tmp = URIRef("http://example.org/tmp/")
    graph = Graph()
    graph.add((iri_tmp, RDFS.label, Literal("document")))
    graph.add((URIRef("http://example.org/alice"), RDFS.label, Literal("Alice")))
    result = rdf_document_service.edit_and_publish(rdf_document, graph, iri_tmp=iri_tmp)
    rdf_document = result.rdf_document
    assert result.rdf_document is not None
    assert len(result.updated) == 9
    assert URIRef(f"http://testserver/datasets/{uuid}/") in result.updated
    assert URIRef(f"http://testserver/datasets/{uuid}/versions/1/") in result.updated
    assert URIRef(f"http://testserver/datasets/{uuid}/versions/1/entered/") in result.updated
    assert URIRef(f"http://testserver/datasets/{uuid}/versions/1/persisted/") in result.updated
    assert URIRef(f"http://testserver/datasets/{uuid}/versions/1/generated/") in result.updated
    assert URIRef(f"http://testserver/datasets/{uuid}/versions/2/") in result.updated
    assert URIRef(f"http://testserver/datasets/{uuid}/versions/2/entered/") in result.updated
    assert URIRef(f"http://testserver/datasets/{uuid}/versions/2/persisted/") in result.updated
    assert URIRef(f"http://testserver/datasets/{uuid}/versions/2/generated/") in result.updated
    assert len(result.deleted) == 0
    iri_version = URIRef(f"http://testserver/datasets/{uuid}/versions/2/")
    graph_version = result.dataset.graph(iri_version)
    assert (iri_version, RDFS.label, Literal("document")) in graph_version
