# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2024 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from datetime import datetime

from django.db import transaction

from ..models.author import Author
from ..models.ro_crate_import import RoCrateImport
from ..tasks.ro_crate_import import process_ro_crate_import


class RoCrateImportService:
    def __init__(self, request):
        self.author, _ = Author.objects.get_or_create(user=request.user)

    @transaction.atomic
    def create_ro_crate_import(self, workspace, file):
        now = datetime.now()

        ro_crate_import = RoCrateImport.objects.create(
            created_at=now,
            status=RoCrateImport.Status.PENDING,
            created_by=self.author,
            workspace=workspace,
            file=file,
        )

        process_ro_crate_import.delay_on_commit(ro_crate_import.id)

        return ro_crate_import
