# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2024 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

import json  # noqa: I001
import logging
from dataclasses import dataclass
from typing import Dict, List, Optional
from uuid import uuid4

from django.core.files.storage import FileSystemStorage
from django.db import transaction
from graphlib import CycleError, TopologicalSorter
from rdflib import OWL, Graph, URIRef
from rdflib.plugins.parsers.notation3 import BadSyntax
from rest_framework.exceptions import NotFound, ParseError, ValidationError
from rocrate.rocrate import ROCrate

from ..models.rdf_document import RDFDocumentVersion
from ..namespaces import Namespaces
from ..services.rdf_document import RDFDocumentService, copy_graph_and_replace_iri_exact
from ..services.rdf_document_version import RDFDocumentVersionService  # type: ignore
from .triple_store import TripleStore


@dataclass
class Part:
    part_id: str
    graph: Graph
    iri_tmp: URIRef
    identifier: Optional[str] = None
    document_version_published: Optional[RDFDocumentVersion] = None

    def resolve_imports(self, aliases: Dict[URIRef, URIRef]):
        for _, _, iri_import in self.graph.triples((self.iri_tmp, OWL.imports, None)):
            if isinstance(iri_import, URIRef) and iri_import in aliases:
                self.graph.remove((self.iri_tmp, OWL.imports, iri_import))
                self.graph.add((self.iri_tmp, OWL.imports, aliases[iri_import]))


def sort_parts(parts: List[Part]) -> List[Part]:
    graph = {
        part.part_id: {
            part_imported.part_id
            for _, _, iri_import in part.graph.triples((part.iri_tmp, OWL.imports, None))
            if isinstance(iri_import, URIRef) and (part_imported := part_with_alias(iri_import, parts))
        }
        for part in parts
    }

    ts = TopologicalSorter(graph)
    try:
        parts_sorted = [part for part_id in ts.static_order() if (part := part_with_id(part_id, parts))]
    except CycleError as e:
        raise ValidationError("The owl:import statements are in cycle") from e

    return parts_sorted


def part_with_alias(iri: URIRef, parts: List[Part]) -> Optional[Part]:
    for part in parts:
        if part.identifier is not None and URIRef(part.identifier) == iri:
            return part
    return None


def part_with_id(part_id: str, parts: List[Part]):
    for part in parts:
        if part.part_id == part_id:
            return part
    return None


class ProcessRoCrateImportService:
    def __init__(self, author, logger=None):
        self.triple_store = TripleStore()
        if logger is None:
            self._logger = logging.getLogger("django.server")
        else:
            self._logger = logger
        self.rdf_document_service = RDFDocumentService(author)
        self.rdf_document_version_service = RDFDocumentVersionService(author)
        self.ro_crate_context = json.load(open("backend/ro-crate-1.1-context.jsonld", "r"))
        self.aliases = {}

    @transaction.atomic
    def process_ro_crate_import(self, ro_crate_import):
        self.author = ro_crate_import.created_by

        storage = FileSystemStorage()
        uuid = uuid4()
        storage.save(f"{uuid}.zip", ro_crate_import.file)
        path = storage.path(f"{uuid}.zip")
        try:
            crate = ROCrate(path)
        except (ValueError, TypeError) as exc:
            raise ParseError("RO-Crate parse error - %s" % str(exc)) from exc
        except KeyError as exc:
            raise ValidationError(
                f"The RO-Crate mentions files which are not described: {exc}",
                code="key_error",
            )

        results = self.create_or_update_rdf_documents(crate, ro_crate_import.workspace)
        self._logger.info(f"{len(results)} rdf documents need to be created or updated")
        for result in results:
            self._logger.info(f"  ...creating or updating {Namespaces().datasets(result.rdf_document.uuid)}")
            result.sync_rdf_store(logger=self._logger)

    @transaction.atomic
    def create_or_update_rdf_documents(self, crate, workspace):
        identifier = crate.root_dataset.get("identifier")

        if identifier is None:
            return self._create_rdf_document_crate(crate, workspace)
        else:
            try:
                rdf_document_version = self.rdf_document_version_service.get(identifier, workspace)
            except NotFound:
                return self._create_rdf_document_crate(crate, workspace, identifier)

            self._is_writable(rdf_document_version, identifier, raise_exception=True)
            return self._update_rdf_document_crate(crate, workspace, identifier, rdf_document_version)

    def _create_rdf_document_crate(self, crate, workspace, identifier=None):
        self._logger.info("Creating rdf document")

        public_id_tmp = Namespaces().datasets(uuid4())
        iri_tmp = URIRef(public_id_tmp)

        graph_crate = self._graph_from_crate(crate, public_id_tmp)

        parts = [self._part_from_part(part, workspace) for part in get_parts_turtle(crate)]
        parts = sort_parts(parts)
        results = [self._process_part(workspace, part, public_id_tmp, graph_crate) for part in parts]

        graph_crate = self._update_has_part_iris(graph_crate, parts, results, iri_tmp)

        result_crate = self.rdf_document_service.create_and_publish(
            graph_crate,
            workspace=workspace,
            iri_tmp=iri_tmp,
            iri_alias=identifier,
        )
        result_crate.rdf_document.version_published.has_part.add(*[result.rdf_document.version_published for result in results])
        results.append(result_crate)

        return results

    def _update_rdf_document_crate(self, crate, workspace, identifier, rdf_document_version):
        self._logger.info("Updating rdf document")

        public_id_tmp = Namespaces().datasets(uuid4())
        iri_tmp = URIRef(public_id_tmp)

        graph_crate = self._graph_from_crate(crate, public_id_tmp)

        parts = [self._part_from_part(part, workspace) for part in get_parts_turtle(crate)]
        parts = sort_parts(parts)
        results = [
            self._process_part(rdf_document_version.rdf_document.workspace, part, public_id_tmp, graph_crate) for part in parts
        ]

        graph_crate = self._update_has_part_iris(graph_crate, parts, results, iri_tmp)

        result_crate = self.rdf_document_service.edit_and_publish(
            rdf_document_version.rdf_document,
            graph_crate,
            iri_tmp=iri_tmp,
            iri_alias=identifier,
        )
        result_crate.rdf_document.version_published.has_part.add(*[result.rdf_document.version_published for result in results])
        results.append(result_crate)

        return results

    def _graph_from_crate(self, crate, public_id):
        if crate.dereference("ro-crate-metadata.json").PROFILE != "https://w3id.org/ro/crate/1.1":
            raise NotImplementedError

        try:
            return Graph().parse(
                data=json.dumps(self._json_ld_from_crate(crate)),
                publicID=public_id,
                format="json-ld",
            )
        except BadSyntax as exc:
            raise ValidationError(
                f"The ro-crate-metadata.json could not be parsed: {exc}",
                code="bad_syntax",
            )

    def _part_from_part(self, part, workspace):
        public_id_tmp = f"{Namespaces().datasets(uuid4())}"
        iri_tmp = URIRef(public_id_tmp)

        identifier = part.get("identifier")

        try:
            graph = Graph().parse(
                part.source,
                publicID=public_id_tmp,
                format="turtle",
            )
        except BadSyntax as exc:
            raise ValidationError(
                f"The document with identifier {identifier} could not be parsed: {exc}",
                code="bad_syntax",
            )

        document_version_published = None
        if identifier is not None:
            try:
                document_version_published = self.rdf_document_version_service.get(identifier, workspace)
            except NotFound:
                pass
            if document_version_published is not None:
                self._is_writable(document_version_published, identifier, raise_exception=True)

        return Part(
            part_id=part.get("@id"),
            graph=graph,
            iri_tmp=iri_tmp,
            identifier=identifier,
            document_version_published=document_version_published,
        )

    def _process_part(self, workspace, part: Part, public_id_crate, graph_crate: Graph):
        part.resolve_imports(self.aliases)
        result = self._create_or_update_rdf_document_part(workspace, part)

        iri_part = URIRef(f"{public_id_crate}{part.part_id}")
        iri_part_rdf_document = result.decorated.version_published.iri

        if part.identifier is not None:
            self.aliases[URIRef(part.identifier)] = iri_part_rdf_document

        graph_crate = copy_graph_and_replace_iri_exact(graph_crate, iri_part, iri_part_rdf_document)

        return result

    def _update_has_part_iris(self, graph_crate, parts, results, iri_tmp):
        for index, part in enumerate(parts):
            graph_crate = copy_graph_and_replace_iri_exact(
                graph_crate,
                iri_tmp + part.part_id,
                results[index].decorated.version_published.iri,
            )
        return graph_crate

    def _create_or_update_rdf_document_part(self, workspace, part: Part):
        if part.identifier is not None and part.document_version_published is not None:
            return self._update_rdf_document_part(part)
        else:
            return self._create_rdf_document_part(workspace, part)

    def _create_rdf_document_part(self, workspace, part: Part):
        return self.rdf_document_service.create_and_publish(
            part.graph,
            workspace=workspace,
            iri_tmp=part.iri_tmp,
            iri_alias=part.identifier,
            iris_ignored=set(self.aliases.values()),
        )

    def _update_rdf_document_part(self, part: Part):
        return self.rdf_document_service.edit_and_publish(
            part.document_version_published.rdf_document,  # type: ignore
            part.graph,
            iri_tmp=part.iri_tmp,
            iri_alias=part.identifier,
            iris_ignored=set(self.aliases.values()),
        )

    def _is_writable(self, rdf_document_version, identifier, raise_exception=False):
        is_writable = (
            rdf_document_version.created_by != self.author
            or rdf_document_version.rdf_document.workspace.memberships.all().at_least_editor().filter(author=self.author).exists()
        )
        if not is_writable and raise_exception:
            raise ValidationError(
                f"No write access for document <{Namespaces().datasets(rdf_document_version.rdf_document.uuid)}> (via <{identifier}>)",
                code="no_write_access",
            )
        return is_writable

    def _json_ld_from_crate(self, crate):
        return {
            "@context": self.ro_crate_context,
            "@graph": [
                crate.root_dataset.as_jsonld(),
                crate.metadata.as_jsonld(),
            ]
            + [data_entity.as_jsonld() for data_entity in crate.data_entities]
            + [contextual_entity.as_jsonld() for contextual_entity in crate.contextual_entities],
        }


def get_parts_turtle(crate):
    parts = []

    for has_part in crate.root_dataset.get("hasPart"):
        data_entity_part = crate.dereference(has_part.get("@id"))
        is_turtle = False
        for encoding_format in data_entity_part.get("encodingFormat"):
            if isinstance(encoding_format, str) and encoding_format == "text/turtle":
                is_turtle = True
        if is_turtle:
            parts.append(data_entity_part)

    return parts
