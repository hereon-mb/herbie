# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

import re
from datetime import datetime
from hashlib import sha256
from pathlib import PurePosixPath
from typing import Dict, Optional
from uuid import uuid4

from django.conf import settings
from django.db import transaction
from inflection import dasherize, underscore
from pyshacl.rdfutil import clone_graph
from rdflib import DCTERMS, OWL, RDF, SH, Graph, URIRef
from rdflib.compare import to_isomorphic
from rest_framework import exceptions
from rest_framework.exceptions import NotFound, PermissionDenied, ValidationError
from silk.profiling.profiler import silk_profile

from ..access_policies.datasets import DatasetsAccessPolicy
from ..decorators.rdf_document_dataset import AuthorDecorator, RDFDocumentDecorator
from ..models.rdf_document import RDFDocument
from ..models.rdf_identifier import RDFIdentifier
from ..namespace._HASH import HASH
from ..namespaces import Namespaces
from ..shacl import construct_from_node_shape, parse_node_shape
from ..util import fix_protocol
from .rdf_document_version import RDFDocumentVersionService  # type: ignore
from .shacl import ShaclService
from .triple_store import TripleStore
from .workspace_graph import WorkspaceGraphService


class Result:
    def __init__(
        self,
        rdf_document,
        updated=None,
        deleted=None,
        dataset=None,
        graph=None,
        decorated=None,
        remove_from_workspace_graph=None,
        insert_into_workspace_graph=None,
    ) -> None:
        self.rdf_document = rdf_document
        self.updated = updated if updated is not None else {}
        self.deleted = deleted if deleted is not None else []
        self.dataset = dataset
        self.graph = graph
        self.decorated = decorated
        self.remove_from_workspace_graph = remove_from_workspace_graph
        self.insert_into_workspace_graph = insert_into_workspace_graph

    def sync_rdf_store(self, logger=None):
        for graph in self.updated.values():
            TripleStore().put_graph(graph)
        for iri in self.deleted:
            TripleStore().delete_graph(iri)

        if self.remove_from_workspace_graph is not None:
            workspace_graph_service = WorkspaceGraphService(
                self.rdf_document.workspace,
                logger=logger,
            )
            workspace_graph_service.remove_document(self.remove_from_workspace_graph)

        if self.insert_into_workspace_graph is not None:
            workspace_graph_service = WorkspaceGraphService(
                self.rdf_document.workspace,
                logger=logger,
            )
            workspace_graph_service.insert_document(self.insert_into_workspace_graph)


class RDFDocumentService:
    def __init__(self, author) -> None:
        self._author = author
        self._cached_graphs_with_imports: Dict[URIRef, Graph] = {}
        self._cached_graph_onts: Dict[URIRef, Graph] = {}
        self._cached_dataset_authors: Dict[int, AuthorDecorator] = {}

    @transaction.atomic
    def get(self, uuid, prefetch=True):
        rdf_documents = RDFDocument.objects.all()
        rdf_documents = DatasetsAccessPolicy.scope_queryset(self._author, rdf_documents)
        if prefetch:
            rdf_documents = rdf_documents.with_prefetches()  # type: ignore
        if (rdf_document := rdf_documents.filter(uuid=uuid).first()) is None:
            raise NotFound()
        return rdf_document

    @transaction.atomic
    def get_version_via_iri(self, iri, prefetch=True):
        url = fix_protocol(iri)
        root = settings.IRI_PREFIX

        pattern_slug = "[-a-zA-Z0-9]+"
        pattern_int = "[0-9]+"

        pattern_version = root + f"datasets/(?P<uuid>{pattern_slug})/versions/(?P<index>{pattern_int})/"
        if match := re.match(pattern_version, url):
            uuid = match.group("uuid")
            index = int(match.group("index"))
            rdf_document = self.get(uuid)
            if (version := rdf_document.rdf_document_versions.all().order_by("valid_from")[index - 1]) is not None:
                return version
            else:
                raise Exception(f"{iri}: version not available")

    @transaction.atomic
    def _get_graph_with_imports(self, iri: URIRef):
        if iri in self._cached_graphs_with_imports:
            return self._cached_graphs_with_imports[iri]

        graph = Graph()
        parsed_ontologies = []

        def parse_ontology(iri):
            url = fix_protocol(iri)
            root = settings.IRI_PREFIX
            pattern_slug = "[-a-zA-Z0-9]+"
            pattern_int = "[0-9]+"

            pattern_draft = root + f"datasets/(?P<uuid>{pattern_slug})/draft/"
            pattern_version = root + f"datasets/(?P<uuid>{pattern_slug})/versions/(?P<index>{pattern_int})/"

            if match := re.match(pattern_draft, url):
                uuid = match.group("uuid")
                rdf_document = self.get(uuid)
                if (draft := rdf_document.version_draft) is not None:
                    graph.parse(data=draft.graph_entered, format="json-ld")
                else:
                    raise Exception(f"{iri}: draft version not available")
            elif match := re.match(pattern_version, url):
                uuid = match.group("uuid")
                index = int(match.group("index"))
                rdf_document = self.get(uuid)
                if (version := rdf_document.rdf_document_versions.all().order_by("valid_from")[index - 1]) is not None:
                    graph.parse(data=version.graph_entered, format="json-ld")
                else:
                    raise Exception(f"{iri}: version not available")
            else:
                raise Exception(f"{iri}: invalid ontology iri")

            parsed_ontologies.append(URIRef(url))
            for _, _, o in graph.triples((URIRef(url), OWL.imports, None)):
                if o not in parsed_ontologies:
                    parse_ontology(o)

        parse_ontology(iri)

        self._cached_graphs_with_imports[iri] = graph

        return graph

    @transaction.atomic
    def create(self, workspace, graph=None, mint=None, uuid=None):
        now = datetime.now()

        uuid = uuid4() if uuid is None else uuid
        graph = Graph() if graph is None else graph
        graph_access = Graph()

        self._add_minted_instance(mint, graph)
        self._resolve_imports(workspace, graph, Namespaces().datasets_draft(uuid))

        # UPDATE DB

        rdf_document = RDFDocument.objects.create(
            uuid=uuid,
            workspace=workspace,
            graph_access=graph_access.serialize(format="json-ld"),
            graph_access_updated_at=now,
        )
        rdf_document.rdf_document_versions.create(
            created_at=now,
            updated_at=now,
            created_by=self._author,
            valid_from=None,
            valid_to=None,
            graph_entered=graph.serialize(format="json-ld"),
            graph_generated=Graph().serialize(format="json-ld"),
            graph_generated_updated_at=now,
            graph_persisted=Graph().serialize(format="json-ld"),
            graph_persisted_updated_at=now,
        )

        # DECORATE

        rdf_document = RDFDocument.objects.all().with_prefetches().get(pk=rdf_document.pk)  # type: ignore
        decorated = RDFDocumentDecorator(rdf_document)

        # RETURN RESULT

        return Result(
            rdf_document,
            updated={
                iri: decorated.dataset.graph(iri)
                for iri in [
                    decorated.iri,
                    decorated.iri_access,
                    decorated.version_draft.iri,
                    decorated.version_draft.iri_entered,
                    decorated.version_draft.iri_persisted,
                    decorated.version_draft.iri_generated,
                ]
            },
            deleted=[],
            dataset=decorated.dataset,
            graph=decorated.dataset.graph(decorated.iri),
        )

    @silk_profile(name="RDFDocumentService.update")
    @transaction.atomic
    def update(self, rdf_document, graph):
        if (draft := rdf_document.version_draft) is None:
            raise ValidationError("no_draft", code="no_draft")

        now = datetime.now()

        self._resolve_imports(rdf_document.workspace, graph, Namespaces().datasets_draft(rdf_document.uuid))

        # UPDATE DB

        draft.updated_at = now
        draft.graph_entered = graph.serialize(format="json-ld")
        draft.save()

        # DECORATE

        rdf_document = RDFDocument.objects.all().with_prefetches().get(pk=rdf_document.pk)  # type: ignore
        decorated = RDFDocumentDecorator(rdf_document)

        # RETURN RESULT

        return Result(
            rdf_document,
            updated={
                iri: decorated.dataset.graph(iri)
                for iri in [
                    decorated.iri,
                    decorated.version_draft.iri,
                    decorated.version_draft.iri_entered,
                ]
            },
            deleted=[],
            dataset=decorated.dataset,
            graph=decorated.dataset.graph(decorated.version_draft.iri),
        )

    @silk_profile(name="RDFDocumentService.generate")
    @transaction.atomic
    def generate(self, rdf_document):
        if (draft := rdf_document.version_draft) is None:
            raise ValidationError("no_draft", code="no_draft")

        now = datetime.now()

        graph_draft = Graph().parse(data=draft.graph_entered, format="json-ld")

        # GENERATE

        graph_persisted = self._shacl_service(graph_draft, rdf_document.workspace).generate(graph_draft, only_persist=True)
        graph_generated = self._shacl_service(graph_draft, rdf_document.workspace).generate(graph_draft, only_persist=False)

        # UPDATE DB

        draft.updated_at = now
        draft.graph_persisted = graph_persisted.serialize(format="json-ld")
        draft.graph_persisted_updated_at = now
        draft.graph_generated = graph_generated.serialize(format="json-ld")
        draft.graph_generated_updated_at = now
        draft.save()

        # DECORATE

        rdf_document = RDFDocument.objects.all().with_prefetches().get(pk=rdf_document.pk)  # type: ignore
        decorated = RDFDocumentDecorator(rdf_document)

        # RETURN RESULT

        return Result(
            rdf_document,
            updated={
                iri: decorated.dataset.graph(iri)
                for iri in [
                    decorated.iri,
                    decorated.version_draft.iri,
                    decorated.version_draft.iri_persisted,
                    decorated.version_draft.iri_generated,
                ]
            },
            deleted=[],
            dataset=decorated.dataset,
            graph=decorated.dataset.graph(decorated.version_draft.iri),
        )

    @transaction.atomic
    def update_access(self, rdf_document, graph):
        now = datetime.now()

        if not graph:
            graph = Graph()

        # UPDATE DB

        rdf_document.graph_access = graph.serialize(format="json-ld")
        rdf_document.graph_access_updated_at = now
        rdf_document.save()

        # DECORATE

        rdf_document = RDFDocument.objects.all().with_prefetches().get(pk=rdf_document.pk)  # type: ignore
        decorated = RDFDocumentDecorator(rdf_document)

        # RETURN RESULT

        return Result(
            rdf_document,
            updated={
                iri: decorated.dataset.graph(iri)
                for iri in [
                    decorated.iri,
                    decorated.iri_access,
                ]
            },
            deleted=[],
            dataset=decorated.dataset,
            graph=decorated.dataset.graph(decorated.iri),
        )

    @transaction.atomic
    def add_alias(self, rdf_document, iri_alias: str):
        if (draft := rdf_document.version_draft) is None:
            raise ValidationError("no_draft", code="no_draft")

        now = datetime.now()

        # UPDATE DB

        draft.updated_at = now
        draft.iri_aliases.create(iri=iri_alias)
        draft.save()

        # DECORATE

        rdf_document = RDFDocument.objects.all().with_prefetches().get(pk=rdf_document.pk)  # type: ignore
        decorated = RDFDocumentDecorator(rdf_document)

        # RETURN RESULT

        return Result(
            rdf_document,
            updated={
                iri: decorated.dataset.graph(iri)
                for iri in [
                    decorated.iri,
                    decorated.version_draft.iri,  # type: ignore
                ]
            },
            deleted=[],
            dataset=decorated.dataset,
            graph=decorated.dataset.graph(decorated.version_draft.iri),  # type: ignore
        )

    @transaction.atomic
    def delete_alias(self, rdf_document, iri_alias: str):
        if (draft := rdf_document.version_draft) is None:
            raise ValidationError("no_draft", code="no_draft")

        if (iri_alias := draft.iri_aliases.filter(iri=iri_alias).first()) is None:
            raise ValidationError("alias_does_not_exist", code="alias_does_not_exist")

        now = datetime.now()

        # UPDATE DB

        iri_alias.delete()
        draft.updated_at = now
        draft.save()

        # DECORATE

        rdf_document = RDFDocument.objects.all().with_prefetches().get(pk=rdf_document.pk)  # type: ignore
        decorated = RDFDocumentDecorator(rdf_document)

        # RETURN RESULT

        return Result(
            rdf_document,
            updated={
                iri: decorated.dataset.graph(iri)
                for iri in [
                    decorated.iri,
                    decorated.version_draft.iri,  # type: ignore
                ]
            },
            deleted=[],
            dataset=decorated.dataset,
            graph=decorated.dataset.graph(decorated.version_draft.iri),  # type: ignore
        )

    @transaction.atomic
    def cancel(self, rdf_document):
        if (draft := rdf_document.version_draft) is None:
            raise ValidationError("no_draft", code="no_draft")

        now = datetime.now()

        iri = Namespaces().datasets(rdf_document.uuid)
        iri_draft = Namespaces().datasets_draft(rdf_document.uuid)
        iri_draft_entered = Namespaces().datasets_draft_entered(rdf_document.uuid)
        iri_draft_persisted = Namespaces().datasets_draft_persisted(rdf_document.uuid)
        iri_draft_generated = Namespaces().datasets_draft_generated(rdf_document.uuid)

        if rdf_document.rdf_document_versions.count() > 1:
            # UPDATE DB

            draft.deleted_at = now
            draft.save()

            # DECORATE

            rdf_document = RDFDocument.objects.all().with_prefetches().get(pk=rdf_document.pk)  # type: ignore
            decorated = RDFDocumentDecorator(rdf_document)

            # RETURN RESULT

            return Result(
                rdf_document,
                updated={decorated.iri: decorated.dataset.graph(decorated.iri)},
                deleted=[
                    iri_draft,
                    iri_draft_entered,
                    iri_draft_persisted,
                    iri_draft_generated,
                ],
                dataset=decorated.dataset,
                graph=decorated.dataset.graph(decorated.iri),
            )

        else:
            # UPDATE DB

            rdf_document.delete()

            # RETURN RESULT

            return Result(
                None,
                updated={},
                deleted=[
                    iri,
                    Namespaces().datasets_access(rdf_document.uuid),
                    iri_draft,
                    iri_draft_entered,
                    iri_draft_persisted,
                    iri_draft_generated,
                ],
            )

    @transaction.atomic
    def publish(self, rdf_document):
        if (draft := rdf_document.version_draft) is None:
            raise ValidationError("no_draft", code="no_draft")

        now = datetime.now()

        iri_draft = Namespaces().datasets_draft(rdf_document.uuid)
        iri_version = Namespaces().datasets_version(rdf_document.uuid, draft.version_index + 1)

        graph_draft = Graph().parse(data=draft.graph_entered, format="json-ld")

        shacl_service = self._shacl_service(graph_draft, rdf_document.workspace)

        # GENERATE

        graph_draft_persisted = shacl_service.generate(graph_draft, only_persist=True)
        graph_draft_generated = shacl_service.generate(graph_draft, only_persist=False)

        # VALIDATE

        graph_for_validation = Graph()
        clone_graph(graph_draft, graph_for_validation)
        clone_graph(graph_draft_generated, graph_for_validation)
        shacl_service.validate(graph_for_validation)

        # UPDATE DB

        graph_published = copy_graph_and_replace_iri(graph_draft, iri_draft, iri_version)
        graph_published_generated = copy_graph_and_replace_iri(graph_draft_generated, iri_draft, iri_version)
        graph_published_persisted = copy_graph_and_replace_iri(graph_draft_persisted, iri_draft, iri_version)

        iri_version_current = None
        if published := rdf_document.version_published:
            iri_version_current = Namespaces().datasets_version(rdf_document.uuid, published.version_index + 1)
            published.updated_at = now
            published.valid_to = now
            published.save()

        draft.updated_at = now
        draft.valid_from = now
        draft.graph_entered = graph_published.serialize(format="json-ld")
        draft.graph_entered_sha256 = self._graph_to_sha256(graph_published)
        draft.graph_generated = graph_published_generated.serialize(format="json-ld")
        draft.graph_generated_updated_at = now
        draft.graph_persisted = graph_published_persisted.serialize(format="json-ld")
        draft.graph_persisted_updated_at = now
        draft.save()

        # DECORATE

        rdf_document = RDFDocument.objects.all().with_prefetches().get(pk=rdf_document.pk)  # type: ignore
        decorated = RDFDocumentDecorator(rdf_document)

        # RETURN RESULT

        return Result(
            rdf_document,
            updated={
                iri: decorated.dataset.graph(iri)
                for iri in [
                    decorated.iri,
                    decorated.version_published.iri,
                    decorated.version_published.iri_entered,
                    decorated.version_published.iri_persisted,
                    decorated.version_published.iri_generated,
                ]
            },
            deleted=[
                iri_draft,
                Namespaces().datasets_draft_entered(rdf_document.uuid),
                Namespaces().datasets_draft_persisted(rdf_document.uuid),
                Namespaces().datasets_draft_generated(rdf_document.uuid),
            ],
            dataset=decorated.dataset,
            graph=decorated.dataset.graph(decorated.iri),
            remove_from_workspace_graph=iri_version_current,
            insert_into_workspace_graph=decorated.version_published.iri,
        )

    @transaction.atomic
    def edit(self, rdf_document, graph_uploaded=None):
        if (published := rdf_document.version_published) is None:
            raise ValidationError("no_version_published", code="no_version_published")

        if rdf_document.version_draft is not None:
            raise ValidationError("This document is already being edited.", code="entry_is_being_edited")

        now = datetime.now()

        iri = Namespaces().datasets(rdf_document.uuid)
        iri_version = Namespaces().datasets_version(rdf_document.uuid, published.version_index + 1)
        iri_draft = Namespaces().datasets_draft(rdf_document.uuid)

        if graph_uploaded:
            iri_edit = URIRef(f"{iri}edit/")
            graph_draft = copy_graph_and_replace_iri(graph_uploaded, iri_edit, iri_draft)
            self._resolve_imports(rdf_document.workspace, graph_draft, iri_draft)
            graph_draft_generated = Graph()
            graph_draft_persisted = Graph()
        else:
            graph_published = Graph().parse(data=published.graph_entered, format="json-ld")
            graph_draft = copy_graph_and_replace_iri(graph_published, iri_version, iri_draft)

            graph_published_generated = Graph().parse(data=published.graph_generated, format="json-ld")
            graph_draft_generated = copy_graph_and_replace_iri(graph_published_generated, iri_version, iri_draft)

            graph_published_persisted = Graph().parse(data=published.graph_persisted, format="json-ld")
            graph_draft_persisted = copy_graph_and_replace_iri(graph_published_persisted, iri_version, iri_draft)

        # UPDATE DB

        rdf_document.rdf_document_versions.create(
            created_at=now,
            updated_at=now,
            created_by=self._author,
            valid_from=None,
            valid_to=None,
            graph_entered=graph_draft.serialize(format="json-ld"),
            graph_generated=graph_draft_generated.serialize(format="json-ld"),
            graph_generated_updated_at=now,
            graph_persisted=graph_draft_persisted.serialize(format="json-ld"),
            graph_persisted_updated_at=now,
        )

        # DECORATE

        rdf_document = RDFDocument.objects.all().with_prefetches().get(pk=rdf_document.pk)  # type: ignore
        decorated = RDFDocumentDecorator(rdf_document)

        # RETURN RESULT

        updated = [
            decorated.iri,
            decorated.version_draft.iri,
            decorated.version_draft.iri_entered,
            decorated.version_draft.iri_persisted,
            decorated.version_draft.iri_generated,
        ]
        return Result(
            rdf_document,
            updated={iri: decorated.dataset.graph(iri) for iri in updated},
            deleted=[],
            dataset=decorated.dataset,
            graph=decorated.dataset.graph(iri),
        )

    def create_and_publish(self, graph, workspace, iri_tmp, iri_alias=None, iris_ignored=None):
        now = datetime.now()

        uuid = uuid4()

        iri_version_new = Namespaces().datasets_version(uuid, 1)
        graph = copy_graph_and_replace_iri(graph, iri_tmp, iri_version_new)
        self._resolve_imports(workspace, graph, iri_version_new, iris_ignored)

        shacl_service = self._shacl_service(graph, workspace)

        # GENERATE

        graph_persisted = shacl_service.generate(graph, only_persist=True)
        graph_generated = shacl_service.generate(graph, only_persist=False)

        # VALIDATE

        graph_for_validation = Graph()
        clone_graph(graph, graph_for_validation)
        clone_graph(graph_generated, graph_for_validation)
        shacl_service.validate(graph_for_validation)

        # UPDATE DB

        rdf_document = RDFDocument.objects.create(
            uuid=uuid,
            workspace=workspace,
            graph_access=Graph().serialize(format="json-ld"),
            graph_access_updated_at=now,
        )
        rdf_document_version = rdf_document.rdf_document_versions.create(
            created_at=now,
            updated_at=now,
            created_by=self._author,
            valid_from=now,
            valid_to=None,
            graph_entered=graph.serialize(format="json-ld"),
            graph_entered_sha256=self._graph_to_sha256(graph),
            graph_generated=graph_generated.serialize(format="json-ld"),
            graph_generated_updated_at=now,
            graph_persisted=graph_persisted.serialize(format="json-ld"),
            graph_persisted_updated_at=now,
        )

        if iri_alias:
            rdf_document_version.iri_aliases.create(iri=iri_alias)

        # DECORATE

        rdf_document = RDFDocument.objects.all().with_prefetches().get(pk=rdf_document.pk)  # type: ignore
        decorated = RDFDocumentDecorator(rdf_document)

        # RETURN RESULT

        return Result(
            rdf_document,
            updated={
                iri: decorated.dataset.graph(iri)
                for iri in [
                    decorated.iri,
                    decorated.iri_access,
                    decorated.version_published.iri,
                    decorated.version_published.iri_entered,
                    decorated.version_published.iri_persisted,
                    decorated.version_published.iri_generated,
                ]
            },
            deleted=[],
            dataset=decorated.dataset,
            graph=decorated.dataset.graph(decorated.iri),
            decorated=decorated,
            insert_into_workspace_graph=decorated.version_published.iri,
        )

    def edit_and_publish(self, rdf_document, graph, iri_tmp=None, iri_alias=None, iris_ignored=None):
        if (published := rdf_document.version_published) is None:
            raise ValidationError("no_version_published", code="no_version_published")

        if rdf_document.version_draft is not None:
            raise ValidationError("entry_is_being_edited", code="entry_is_being_edited")

        now = datetime.now()

        if self._graph_unchanged(rdf_document.workspace, graph, iri_tmp, published, rdf_document.uuid, iris_ignored):
            decorated = RDFDocumentDecorator(rdf_document)
            return Result(
                rdf_document,
                decorated=decorated,
            )

        if iri_tmp:
            iri_version_new = Namespaces().datasets_version(
                rdf_document.uuid,
                published.version_index + 2,
            )
            graph = copy_graph_and_replace_iri(graph, iri_tmp, iri_version_new)
            self._resolve_imports(rdf_document.workspace, graph, iri_version_new, iris_ignored)

        iri_version_current = Namespaces().datasets_version(rdf_document.uuid, published.version_index + 1)

        shacl_service = self._shacl_service(graph, rdf_document.workspace)

        # GENERATE

        graph_persisted = shacl_service.generate(graph, only_persist=True)
        graph_generated = shacl_service.generate(graph, only_persist=False)

        # VALIDATE

        graph_for_validation = Graph()
        clone_graph(graph, graph_for_validation)
        clone_graph(graph_generated, graph_for_validation)
        shacl_service.validate(graph_for_validation)

        # UPDATE DB

        published.updated_at = now
        published.valid_to = now
        published.save()

        rdf_document_version = rdf_document.rdf_document_versions.create(
            created_at=now,
            updated_at=now,
            created_by=self._author,
            valid_from=now,
            valid_to=None,
            graph_entered=graph.serialize(format="json-ld"),
            graph_entered_sha256=self._graph_to_sha256(graph),
            graph_generated=graph_generated.serialize(format="json-ld"),
            graph_generated_updated_at=now,
            graph_persisted=graph_persisted.serialize(format="json-ld"),
            graph_persisted_updated_at=now,
        )

        if iri_alias:
            rdf_document_version.iri_aliases.create(iri=iri_alias)

        # DECORATE

        rdf_document = RDFDocument.objects.all().with_prefetches().get(pk=rdf_document.pk)  # type: ignore
        decorated = RDFDocumentDecorator(rdf_document)

        # RETURN RESULT

        updated = [
            decorated.iri,
            decorated.version_published.iri,
            decorated.version_published.iri_entered,
            decorated.version_published.iri_persisted,
            decorated.version_published.iri_generated,
            decorated.version_deprecated.iri,
            decorated.version_deprecated.iri_entered,
            decorated.version_deprecated.iri_persisted,
            decorated.version_deprecated.iri_generated,
        ]
        return Result(
            rdf_document,
            updated={iri: decorated.dataset.graph(iri) for iri in updated},
            deleted=[],
            dataset=decorated.dataset,
            graph=decorated.dataset.graph(decorated.iri),
            decorated=decorated,
            remove_from_workspace_graph=iri_version_current,
            insert_into_workspace_graph=decorated.version_published.iri,
        )

    def delete(self, rdf_document):
        if (published := rdf_document.version_published) is None:
            raise ValidationError("no_version_published", code="no_version_published")

        if rdf_document.version_draft is not None:
            raise ValidationError("entry_is_being_edited", code="entry_is_being_edited")

        now = datetime.now()

        iri = Namespaces().datasets(rdf_document.uuid)
        iri_version = Namespaces().datasets_version(rdf_document.uuid, published.version_index + 1)

        # UPDATE DB

        published.deleted_at = now
        published.save()

        # DECORATE

        rdf_document = RDFDocument.objects.all().with_prefetches().get(pk=rdf_document.pk)  # type: ignore
        decorated = RDFDocumentDecorator(rdf_document)

        # RETURN RESULT

        updated = [
            iri,
            iri_version,
        ]
        return Result(
            rdf_document,
            updated={iri: decorated.dataset.graph(iri) for iri in updated},
            deleted=[],
            dataset=decorated.dataset,
            graph=decorated.dataset.graph(iri),
            remove_from_workspace_graph=iri_version,
        )

    def _graph_unchanged(self, workspace, graph, iri_tmp, published, uuid, iris_ignored):
        if iri_tmp:
            iri_version_current = Namespaces().datasets_version(
                uuid,
                published.version_index + 1,
            )
            graph = copy_graph_and_replace_iri(graph, iri_tmp, iri_version_current)
            self._resolve_imports(workspace, graph, iri_version_current, iris_ignored)
        return self._graph_to_sha256(graph) == published.graph_entered_sha256

    def _graph_to_sha256(self, graph):
        return sha256(to_isomorphic(graph).graph_digest().__str__().encode(encoding="utf-8")).hexdigest()

    def _add_minted_instance(self, mint, graph: Graph):
        if mint is None:
            return

        if not isinstance(mint, str):
            return

        iri_class = URIRef(mint)
        rdf_class_slug = slug_from_iri(iri_class)
        rdf_identifier = RDFIdentifier.objects.create_rdf_identifier(None, self._author, rdf_class_slug)
        iri_minted = URIRef(f"{settings.IRI_PREFIX}graph/{rdf_identifier.path_segments}/")
        graph.add((iri_minted, RDF.type, iri_class))

    def _resolve_imports(self, workspace, graph: Graph, iri, iris_ignored=None):
        if iris_ignored is None:
            iris_ignored = {}

        for _, _, iri_import in graph.triples((iri, OWL.imports, None)):
            if iri_import not in iris_ignored:
                try:
                    iri_version = RDFDocumentVersionService(self._author).get_version_iri(iri_import, workspace)
                except exceptions.NotFound:
                    raise ValidationError(f"The import {iri_import} could not be resolved.", code="import_not_resolvable")
                graph.remove((iri, OWL.imports, iri_import))
                graph.add((iri, OWL.imports, iri_version))

    def _shacl_service(self, graph, workspace):
        conforms_to = conforms_to_from_graph(graph)
        if conforms_to is None:
            if workspace.memberships.all().at_least_manager().filter(author=self._author).exists():
                return ShaclService()
            else:
                raise PermissionDenied(
                    detail="At least manager membership is required when no terms:conformsTo is provided.",
                    code="missing_conforms_to",
                )
        elif conforms_to == URIRef("https://w3id.org/ro/crate/1.1"):
            return ShaclService()
        else:
            return ShaclService(
                graph_shacl=self._graph_shacl(conforms_to),
                graph_ont=self._graph_ont(workspace, conforms_to),
            )

    def _graph_shacl(self, conforms_to: URIRef):
        return self._get_graph_with_imports(conforms_to)

    def _graph_ont(self, workspace, conforms_to: URIRef):
        if conforms_to in self._cached_graph_onts:
            return self._cached_graph_onts[conforms_to]

        graph_ont = Graph()

        for hash_import_iri in self._hash_import_iris(conforms_to):
            clone_graph(self._graph_constructed(workspace, hash_import_iri, conforms_to), graph_ont)

        self._cached_graph_onts[conforms_to] = graph_ont

        return graph_ont

    def _hash_import_iris(self, conforms_to: URIRef):
        graph_shacl = self._graph_shacl(conforms_to)
        return set(
            [
                iri
                for (_, _, iri) in graph_shacl.triples((None, HASH["import"], None))
                if (iri, RDF.type, SH.NodeShape) in graph_shacl
            ]
        )

    def _graph_constructed(self, workspace, hash_import_iri, conforms_to):
        graph_shacl = self._graph_shacl(conforms_to)
        iri_workspace = Namespaces().workspaces(workspace.uuid)
        node_shape = parse_node_shape(graph_shacl, hash_import_iri)
        construct = construct_from_node_shape(iri_workspace, node_shape)
        return TripleStore().construct(construct)


def conforms_to_from_graph(graph: Graph) -> Optional[URIRef]:
    # FIXME we want to put the correct subject here
    for _, _, o in graph.triples((None, DCTERMS.conformsTo, None)):
        return URIRef(str(o))
    return None


def copy_graph_and_replace_iri(graph, iri_from, iri_to):
    graph_result = Graph()
    for sub, pre, obj in graph:
        if isinstance(sub, URIRef):
            sub = URIRef(sub.replace(iri_from, iri_to))  # noqa: PLW2901
        if isinstance(pre, URIRef):
            pre = URIRef(pre.replace(iri_from, iri_to))  # noqa: PLW2901
        if isinstance(obj, URIRef):
            obj = URIRef(obj.replace(iri_from, iri_to))  # noqa: PLW2901
        graph_result.add((sub, pre, obj))
    return graph_result


def copy_graph_and_replace_iri_exact(graph, iri_from, iri_to):
    graph_result = Graph()
    for sub, pre, obj in graph:
        if isinstance(sub, URIRef):
            if sub == iri_from:
                sub = iri_to  # noqa: PLW2901
        if isinstance(pre, URIRef):
            if pre == iri_from:
                pre = iri_to  # noqa: PLW2901
        if isinstance(obj, URIRef):
            if obj == iri_from:
                obj = iri_to  # noqa: PLW2901
        graph_result.add((sub, pre, obj))
    return graph_result


def slug_from_iri(iri):
    if fragment := iri.fragment:
        return dasherize(underscore(fragment))
    elif name := PurePosixPath(iri).name:
        return dasherize(underscore(name))
    else:
        raise ValidationError("could_not_mint", code="could_not_mint")
