# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

import logging

from rdflib import RDF, RDFS, URIRef

from ..sparql import (
    BlankNode,
    Construct,
    Delete,
    Drop,
    FilterNotExists,
    GraphPattern,
    Insert,
    Select,
    Var,
)
from .triple_store import TripleStore

LIMIT_TRIPLES_DEFAULT = 2000


class VirtualGraphService:
    def __init__(self, iri, select_graphs, limit_triples=None, logger=None):
        self.iri = iri
        self.iri_provenance = URIRef(f"{iri}provenance/")

        if limit_triples is None:
            self._limit_triples = LIMIT_TRIPLES_DEFAULT
        else:
            self._limit_triples = limit_triples
        self._triple_store = TripleStore()
        self._select_graphs = select_graphs
        if logger is None:
            self._logger = logging.getLogger("django.server")
        else:
            self._logger = logger

    def delete(self):
        self._triple_store.update(Drop(self.iri).silent())
        self._triple_store.update(Drop(self.iri_provenance).silent())
        self._logger.info(f"  Deleted virtual graph {self.iri}.")

    def create(self):
        graphs = self._get_graphs()
        self._logger.info(f"  Creating virtual graph {self.iri} from {len(graphs)} graphs...")
        for graph in graphs:
            self._insert_triples(graph)
        self._logger.info("    ...done.")

    def insert_graph(self, graph):
        self._insert_triples(graph)

    def remove_graph(self, graph):
        self._remove_triples(graph)

    def _get_graphs(self):
        graph = Var("graph")
        result = self._triple_store.query(self._select_graphs(graph))
        bindings = result["results"]["bindings"]
        return [URIRef(binding[graph.name]["value"]) for binding in bindings]

    def _insert_triples(self, graph):
        triples_count = self._get_triples_count(graph)
        self._logger.info(f"    ...pushing {triples_count} triples + reification")

        statement = BlankNode("statement")
        s, p, o = Var("s"), Var("p"), Var("o")

        for triple_offset in range(0, triples_count, self._limit_triples):
            insert = (
                Insert(
                    GraphPattern(
                        self.iri,
                        [
                            (s, p, o),
                        ],
                    ),
                )
                .where(
                    GraphPattern(
                        graph,
                        [(s, p, o)],
                    )
                )
                .order_by("?s ?p ?o")
                .limit(self._limit_triples)
                .offset(triple_offset)
            )
            insert_provenance = (
                Insert(
                    GraphPattern(
                        self.iri_provenance,
                        [
                            (statement, RDF.type, RDF.Statement),
                            (statement, RDF.subject, s),
                            (statement, RDF.predicate, p),
                            (statement, RDF.object, o),
                            (statement, RDFS.isDefinedBy, graph),
                        ],
                    ),
                )
                .where(
                    GraphPattern(
                        graph,
                        [(s, p, o)],
                    )
                )
                .order_by("?s ?p ?o")
                .limit(self._limit_triples)
                .offset(triple_offset)
            )
            self._triple_store.update(insert)
            self._triple_store.update(insert_provenance)

    def _remove_triples(self, graph):
        triples_count = self._get_triples_count(graph)
        self._logger.info(f"    ...removing up to {triples_count} triples + reification")

        statement = Var("statement")
        s, p, o = Var("s"), Var("p"), Var("o")

        graph_other = Var("graphOther")

        self._triple_store.update(
            Delete(
                GraphPattern(
                    self.iri_provenance,
                    [
                        (statement, RDF.type, RDF.Statement),
                        (statement, RDF.subject, s),
                        (statement, RDF.predicate, p),
                        (statement, RDF.object, o),
                        (statement, RDFS.isDefinedBy, graph),
                    ],
                ),
            )
            .where(
                GraphPattern(
                    self.iri_provenance,
                    [
                        (statement, RDF.type, RDF.Statement),
                        (statement, RDF.subject, s),
                        (statement, RDF.predicate, p),
                        (statement, RDF.object, o),
                        (statement, RDFS.isDefinedBy, graph),
                    ],
                ),
            )
            .order_by("?s ?p ?o")
        )
        self._triple_store.update(
            Delete(
                GraphPattern(
                    self.iri,
                    [(s, p, o)],
                ),
            )
            .where(
                GraphPattern(
                    self.iri,
                    [(s, p, o)],
                ),
                GraphPattern(
                    graph,
                    [(s, p, o)],
                ),
                FilterNotExists(
                    GraphPattern(
                        self.iri_provenance,
                        [
                            (statement, RDF.type, RDF.Statement),
                            (statement, RDF.subject, s),
                            (statement, RDF.predicate, p),
                            (statement, RDF.object, o),
                            (statement, RDFS.isDefinedBy, graph_other),
                        ],
                    ),
                ),
            )
            .order_by("?s ?p ?o")
        )

    def _get_triples_count(self, graph):
        s, p, o = Var("s"), Var("p"), Var("o")
        select = Select(f"COUNT({s})").where(
            GraphPattern(
                graph,
                [(s, p, o)],
            ),
        )
        result = self._triple_store.query(select)
        var = result["head"]["vars"][0]
        return int(result["results"]["bindings"][0][var]["value"])


def _get_graph(iri):
    s, p, o = Var("s"), Var("p"), Var("o")
    return TripleStore().construct(
        Construct(
            (s, p, o),
        ).where(
            GraphPattern(
                iri,
                [(s, p, o)],
            ),
        )
    )
