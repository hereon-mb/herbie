# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from ..matchers import some_dict, some_int, some_str
from .author import AuthorSerializer


def test_serializing(author_alice, context_user_alice):
    data = AuthorSerializer(author_alice, context=context_user_alice).data
    assert data == {
        "id": some_int(),
        "url": some_str(),
        "user": some_dict(),
    }


def test_deserializing():
    serializer = AuthorSerializer(data={})
    assert serializer.is_valid()
