# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from rest_framework import serializers

from ..models.author import Author
from ..models.workspace import Membership, MembershipType, Workspace
from .author import AuthorSerializer
from .choice_field import ChoiceField


class MembershipSerializer(serializers.HyperlinkedModelSerializer):
    created_by = AuthorSerializer(read_only=True)
    author = AuthorSerializer(read_only=True)
    author_id = serializers.PrimaryKeyRelatedField(
        write_only=True,
        queryset=Author.objects.all(),
    )
    membership_type = ChoiceField(choices=MembershipType.choices)

    class Meta:
        model = Membership
        fields = [
            "id",
            "url",
            "created_at",
            "updated_at",
            "deleted_at",
            "created_by",
            "author",
            "author_id",
            "membership_type",
        ]
        extra_kwargs = {
            "id": {"read_only": True},
            "url": {"read_only": True},
            "created_at": {"read_only": True},
            "updated_at": {"read_only": True},
            "deleted_at": {"read_only": True},
            "created_by": {"read_only": True},
            "author": {"read_only": True},
        }


class WorkspaceSerializer(serializers.HyperlinkedModelSerializer):
    created_by = AuthorSerializer(read_only=True)
    memberships = MembershipSerializer(many=True, required=False)

    class Meta:
        model = Workspace
        fields = [
            "uuid",
            "url",
            "created_at",
            "updated_at",
            "deleted_at",
            "created_by",
            "slug",
            "name",
            "description",
            "memberships",
        ]
        extra_kwargs = {
            "url": {"lookup_field": "uuid"},
            "created_at": {"read_only": True},
            "updated_at": {"read_only": True},
            "deleted_at": {"read_only": True},
        }
