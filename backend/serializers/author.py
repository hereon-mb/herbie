# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

import math

from knox.models import AuthToken  # type: ignore
from rest_framework import serializers

from ..models.author import Author
from .preferences_author import PreferencesAuthorSerializer
from .user import UserSerializer


class AuthorSerializer(serializers.HyperlinkedModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Author
        fields = [
            "id",
            "url",
            "user",
        ]


class AuthTokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = AuthToken
        fields = [
            "token_key",
            "expiry",
        ]


class AuthorDetailSerializer(AuthorSerializer):
    permissions = serializers.SerializerMethodField()
    preferences_author = PreferencesAuthorSerializer(read_only=True)
    auth_tokens = serializers.SerializerMethodField()

    def get_auth_tokens(self, author):
        tokens = author.user.auth_token_set.all()
        return [AuthTokenSerializer(token).data for token in tokens]

    def get_permissions(self, author):
        return {
            "rdf_document": author.can_create_rdf_documents or author.user.is_superuser,
            "can_send_sparql_queries": author.can_send_sparql_queries or author.user.is_superuser,
            "can_upload_raw_files": author.can_upload_raw_files or author.user.is_superuser,
            "superuser": author.user.is_superuser,
        }

    def _protocol_frecency(self, now, timestamps):
        return sum(math.exp(-((now - timestamp).microseconds) / 100000) for timestamp in timestamps)

    class Meta(AuthorSerializer.Meta):
        fields = AuthorSerializer.Meta.fields + [
            "permissions",
            "preferences_author",
            "auth_tokens",
        ]
