# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2024 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from pathlib import PurePath

from rest_framework import serializers

from ..models.ro_crate_import import RoCrateImport
from .author import AuthorSerializer
from .choice_field import ChoiceField


class RoCrateImportSerializer(serializers.HyperlinkedModelSerializer):
    created_by = AuthorSerializer(read_only=True)
    status = ChoiceField(choices=RoCrateImport.Status.choices)
    filename = serializers.SerializerMethodField()

    class Meta:
        model = RoCrateImport
        fields = [
            "id",
            "url",
            "created_at",
            "started_at",
            "stopped_at",
            "status",
            "created_by",
            "filename",
        ]

    def get_filename(self, ro_crate_import):
        return PurePath(ro_crate_import.file.name).name
