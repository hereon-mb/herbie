from typing import List

from django.conf import settings
from rdflib import DCTERMS, OWL, PROV, RDF, RDFS, SH, Literal, Namespace, URIRef

from .namespace._HASH import HASH
from .namespace._HERBIE import HERBIE
from .sparql import (
    Alternative,
    Construct,
    Filter,
    GraphPattern,
    Optional,
    Select,
    Sequence,
    Union,
    Values,
    Var,
    ZeroOrMore,
)

DASH = Namespace("http://datashapes.org/dash#")

# CONSTRUCTS


def construct_graph(iri):
    s, p, o = Var("subject"), Var("predicate"), Var("object")
    return Construct((s, p, o)).where(
        GraphPattern(
            iri,
            [(s, p, o)],
        ),
    )


def construct_documents_meta(iris_document):
    s, p, o = Var("s"), Var("p"), Var("o")
    document = Var("document")

    return Construct((s, p, o)).where(
        GraphPattern(document, [(s, p, o)]),
        Values(document, *iris_document),
    )


def construct_classes_with_shacl_shapes(iri_workspace, shapes_required=False):
    workspace = URIRef(iri_workspace)
    workspace_provenance = URIRef(f"{iri_workspace}provenance/")

    statement = Var("statement")
    cls_cls = Var("cls_cls")

    cls = Var("cls")
    cls_parent = Var("cls_parent")
    label = Var("label")
    comment = Var("comment")

    node_shape = Var("nodeShape")
    document_version_shape = Var("documentVersionShape")
    generated_at_time = Var("generatedAtTime")
    was_attributed_to = Var("wasAttributedTo")
    label_was_attributed_to = Var("labelWasAttributedTo")
    label_document_version_shape = Var("labelDocumentVersionShape")
    comment_document_version_shape = Var("commentDocumentVersionShape")

    patterns_cls_parent = [
        Optional((cls, RDFS.subClassOf, cls_parent)),
    ]

    patterns_shape = [
        GraphPattern(
            workspace,
            [
                (node_shape, RDF.type, SH.NodeShape),
                (node_shape, SH.targetClass, cls),
                (node_shape, HASH.documentRoot, Literal(True)),
                (document_version_shape, PROV.generatedAtTime, generated_at_time),
                (document_version_shape, PROV.wasAttributedTo, was_attributed_to),
                (was_attributed_to, RDFS.label, label_was_attributed_to),
                Optional((document_version_shape, RDFS.label, label_document_version_shape)),
                Optional((document_version_shape, RDFS.comment, comment_document_version_shape)),
            ],
        ),
        GraphPattern(
            workspace_provenance,
            [
                (statement, RDF.type, RDF.Statement),
                (statement, RDF.subject, node_shape),
                (statement, RDF.predicate, RDF.type),
                (statement, RDF.object, SH.NodeShape),
                (statement, RDFS.isDefinedBy, document_version_shape),
            ],
        ),
    ]

    construct = Construct(
        (cls, RDF.type, cls_cls),
        (cls, RDFS.label, label),
        (cls, RDFS.comment, comment),
        (cls, RDFS.subClassOf, cls_parent),
        (cls_parent, RDF.type, cls_cls),
        (node_shape, RDF.type, SH.NodeShape),
        (node_shape, SH.targetClass, cls),
        (node_shape, RDFS.isDefinedBy, document_version_shape),
        (document_version_shape, RDFS.label, label_document_version_shape),
        (document_version_shape, RDFS.comment, comment_document_version_shape),
        (document_version_shape, PROV.generatedAtTime, generated_at_time),
        (document_version_shape, PROV.wasAttributedTo, was_attributed_to),
        (was_attributed_to, RDFS.label, label_was_attributed_to),
    ).where(
        *(
            [
                Values(cls_cls, OWL.Class, RDFS.Class),
                GraphPattern(
                    workspace,
                    (
                        [
                            (cls, RDF.type, cls_cls),
                            Optional((cls, RDFS.label, label)),
                            Optional((cls, RDFS.comment, comment)),
                        ]
                        + ([] if shapes_required else patterns_cls_parent)
                    ),
                ),
            ]
            + (patterns_shape if shapes_required else [Optional(*patterns_shape)])
        )
    )
    return construct


def construct_shapes_indirectly_for(
    cls: URIRef,
    /,
    workspace: URIRef,
):
    workspace_provenance = URIRef(f"{workspace}provenance/")
    statement = Var("statement")

    cls_cls = Var("clsCls")

    cls_created = Var("clsCreated")

    cls_target = Var("classTarget")
    cls_target_label = Var("classTargetLabelTarget")
    cls_target_comment = Var("classTargetComment")

    node_shape_cls_target = Var("nodeShapeClsTarget")
    node_shape_cls_child_wrapper = Var("nodeShapeClsChildWrapper")
    node_shape_cls_child = Var("nodeShapeClsChild")

    document_version_shape = Var("documentVersionShape")
    generated_at_time = Var("generatedAtTime")
    was_attributed_to = Var("wasAttributedTo")
    label_was_attributed_to = Var("labelWasAttributedTo")
    label_document_version_shape = Var("labelDocumentVersionShape")
    comment_document_version_shape = Var("commentDocumentVersionShape")

    graph_patterns_node_shape_top_level = [
        GraphPattern(
            workspace,
            [
                (cls_target, RDF.type, cls_cls),
                (cls_target, ZeroOrMore(RDFS.subClassOf), cls),
                (node_shape_cls_target, RDF.type, SH.NodeShape),
                (node_shape_cls_target, SH.targetClass, cls_target),
                (node_shape_cls_target, HASH.documentRoot, Literal(True)),
                Optional((cls_target, RDFS.label, cls_target_label)),
                Optional((cls_target, RDFS.comment, cls_target_comment)),
            ],
        ),
    ]

    graph_patterns_node_shape_nested = [
        GraphPattern(
            workspace,
            [
                (cls_created, RDF.type, cls_cls),
                (cls_created, ZeroOrMore(RDFS.subClassOf), cls),
                (node_shape_cls_child, SH["class"], cls_created),
                (
                    node_shape_cls_child_wrapper,
                    ZeroOrMore(
                        Alternative(
                            SH.node,
                            SH.qualifiedValueShape,
                        ),
                    ),
                    node_shape_cls_child,
                ),
                (node_shape_cls_child_wrapper, DASH.editor, DASH.DetailsEditor),
                (node_shape_cls_target, RDF.type, SH.NodeShape),
                (node_shape_cls_target, SH.targetClass, cls_target),
                (node_shape_cls_target, HASH.documentRoot, Literal(True)),
                (
                    node_shape_cls_target,
                    ZeroOrMore(
                        Sequence(
                            SH.property,
                            Alternative(
                                SH.node,
                                SH.qualifiedValueShape,
                            ),
                        ),
                    ),
                    node_shape_cls_child_wrapper,
                ),
                Optional((cls_target, RDFS.label, cls_target_label)),
                Optional((cls_target, RDFS.comment, cls_target_comment)),
            ],
        ),
    ]

    construct = Construct(
        (cls_target, RDF.type, cls_cls),
        (cls_target, RDFS.label, cls_target_label),
        (cls_target, RDFS.comment, cls_target_comment),
        (node_shape_cls_target, RDF.type, SH.NodeShape),
        (node_shape_cls_target, SH.targetClass, cls_target),
        (node_shape_cls_target, RDFS.isDefinedBy, document_version_shape),
        (document_version_shape, RDFS.label, label_document_version_shape),
        (document_version_shape, RDFS.comment, comment_document_version_shape),
        (document_version_shape, PROV.generatedAtTime, generated_at_time),
        (document_version_shape, PROV.wasAttributedTo, was_attributed_to),
        (was_attributed_to, RDFS.label, label_was_attributed_to),
    ).where(
        Values(cls_cls, OWL.Class, RDFS.Class),
        Select(
            cls_target,
            cls_target_label,
            cls_target_comment,
            node_shape_cls_target,
        ).where(
            Union(
                graph_patterns_node_shape_top_level,
                graph_patterns_node_shape_nested,
            )
        ),
        GraphPattern(
            workspace_provenance,
            [
                (statement, RDF.type, RDF.Statement),
                (statement, RDF.subject, node_shape_cls_target),
                (statement, RDF.predicate, RDF.type),
                (statement, RDF.object, SH.NodeShape),
                (statement, RDFS.isDefinedBy, document_version_shape),
            ],
        ),
        GraphPattern(
            workspace,
            [
                (document_version_shape, PROV.generatedAtTime, generated_at_time),
                (document_version_shape, PROV.wasAttributedTo, was_attributed_to),
                (was_attributed_to, RDFS.label, label_was_attributed_to),
                Optional((document_version_shape, RDFS.label, label_document_version_shape)),
                Optional((document_version_shape, RDFS.comment, comment_document_version_shape)),
            ],
        ),
    )
    return construct


def construct_instances(iri_workspace, iri_class):
    workspace = URIRef(iri_workspace)
    workspace_provenance = URIRef(f"{iri_workspace}provenance/")

    instance = Var("instance")
    label = Var("label")
    comment = Var("comment")

    cls = URIRef(iri_class)
    cls_child = Var("clsChild")
    cls_child_label = Var("clsChildLabel")
    cls_child_comment = Var("clsChildComment")

    see_also = Var("seeAlso")
    statement = Var("statement")

    is_defined_by = Var("isDefinedBy")
    is_defined_by_label = Var("isDefinedByLabel")
    is_defined_by_document = Var("isDefinedByDocument")

    document_s, document_p, document_o = Var("documentS"), Var("documentP"), Var("documentO")

    conforms_to = Var("conformsTo")
    conforms_to_label = Var("conformsToLabel")
    conforms_to_comment = Var("conformsToComment")

    return Construct(
        (instance, RDF.type, cls_child),
        (cls_child, RDFS.subClassOf, cls),
        (cls_child, RDFS.label, cls_child_label),
        (cls_child, RDFS.comment, cls_child_comment),
        (instance, RDFS.label, label),
        (instance, RDFS.comment, comment),
        (instance, RDFS.seeAlso, see_also),
        (instance, RDFS.isDefinedBy, is_defined_by),
        (is_defined_by, RDF.type, HERBIE.DocumentVersion),
        (is_defined_by, RDFS.label, is_defined_by_label),
        (is_defined_by, DCTERMS.conformsTo, conforms_to),
        (is_defined_by, PROV.specializationOf, is_defined_by_document),
        (document_s, document_p, document_o),
        (conforms_to, RDF.type, HERBIE.DocumentVersion),
        (conforms_to, RDFS.label, conforms_to_label),
        (conforms_to, RDFS.comment, conforms_to_comment),
    ).where(
        GraphPattern(
            workspace,
            [
                (instance, RDF.type, cls_child),
                (cls_child, ZeroOrMore(RDFS.subClassOf), cls),
                Optional((instance, RDFS.label, label)),
                Optional((instance, RDFS.comment, comment)),
            ],
        ),
        GraphPattern(
            workspace_provenance,
            [
                (statement, RDF.type, RDF.Statement),
                (statement, RDF.subject, instance),
                (statement, RDF.predicate, RDF.type),
                (statement, RDF.object, cls_child),
                (statement, RDFS.isDefinedBy, see_also),
            ],
        ),
        Optional(
            GraphPattern(
                workspace,
                [
                    (cls_child, RDF.type, OWL.Class),
                    Optional((cls_child, RDFS.label, cls_child_label)),
                    Optional((cls_child, RDFS.comment, cls_child_comment)),
                ],
            ),
        ),
        Optional(
            GraphPattern(
                workspace,
                [
                    (instance, RDFS.isDefinedBy, is_defined_by),
                    (is_defined_by, RDF.type, HERBIE.DocumentVersion),
                    (is_defined_by, PROV.specializationOf, is_defined_by_document),
                    Optional((is_defined_by, RDFS.label, is_defined_by_label)),
                ],
            ),
            GraphPattern(
                is_defined_by_document,
                [
                    (document_s, document_p, document_o),
                ],
            ),
        ),
        Optional(
            GraphPattern(
                workspace,
                [
                    (is_defined_by, DCTERMS.conformsTo, conforms_to),
                    (conforms_to, RDF.type, HERBIE.DocumentVersion),
                    Optional((conforms_to, DCTERMS.conformsTo, conforms_to)),
                    Optional((conforms_to, RDFS.label, conforms_to_label)),
                    Optional((conforms_to, RDFS.comment, conforms_to_comment)),
                ],
            ),
        ),
    )


def construct_documents_ontology(iri_workspace):
    ontology = Var("ontology")
    label = Var("label")
    comment = Var("comment")
    document_version = Var("documentVersion")

    return Construct(
        *[
            (document_version, RDF.type, HERBIE.DocumentVersion),
            (ontology, RDF.type, OWL.Ontology),
            (ontology, RDFS.label, label),
            (ontology, RDFS.comment, comment),
            (ontology, RDFS.isDefinedBy, document_version),
        ]
    ).where(
        GraphPattern(
            document_version,
            [
                (ontology, RDF.type, OWL.Ontology),
                Optional((ontology, RDFS.label, label)),
                Optional((ontology, RDFS.comment, comment)),
            ],
        ),
        select_documents_published(
            iri_workspace=iri_workspace,
            selected=[document_version],
            document_version_published=document_version,
        ),
    )


def construct_instance_graph(iri, iri_workspace):
    workspace = URIRef(iri_workspace)
    workspace_provenance = URIRef(f"{iri_workspace}provenance/")

    instance = Var("instance")

    s, p, o = Var("s"), Var("p"), Var("o")
    s_label, o_label = Var("sLabel"), Var("oLabel")

    is_defined_by = Var("isDefinedBy")
    is_defined_by_label = Var("isDefinedByLabel")
    is_defined_by_document = Var("isDefinedByDocument")

    see_also = Var("seeAlso")
    statement = Var("statement")

    conforms_to = Var("conformsTo")
    conforms_to_label = Var("conformsToLabel")
    conforms_to_comment = Var("conformsToComment")

    construct = Construct(
        (instance, p, o),
        (o, RDFS.label, o_label),
        (s, p, instance),
        (instance, RDFS.seeAlso, see_also),
        (instance, RDFS.isDefinedBy, is_defined_by),
        (is_defined_by, RDF.type, HERBIE.DocumentVersion),
        (is_defined_by, RDFS.label, is_defined_by_label),
        (is_defined_by, DCTERMS.conformsTo, conforms_to),
        (is_defined_by, PROV.specializationOf, is_defined_by_document),
        (conforms_to, RDF.type, HERBIE.DocumentVersion),
        (conforms_to, RDFS.label, conforms_to_label),
        (conforms_to, RDFS.comment, conforms_to_comment),
    ).where(
        Values(instance, iri),
        Union(
            [
                GraphPattern(
                    workspace,
                    [
                        (instance, p, o),
                        Optional((o, RDFS.label, o_label)),
                    ],
                ),
                GraphPattern(
                    workspace_provenance,
                    [
                        (statement, RDF.type, RDF.Statement),
                        (statement, RDF.subject, instance),
                        (statement, RDF.predicate, p),
                        (statement, RDF.object, o),
                        (statement, RDFS.isDefinedBy, see_also),
                    ],
                ),
            ],
            [
                GraphPattern(
                    workspace,
                    [
                        (s, p, instance),
                        Optional((s, RDFS.label, s_label)),
                    ],
                ),
                GraphPattern(
                    workspace_provenance,
                    [
                        (statement, RDF.type, RDF.Statement),
                        (statement, RDF.subject, s),
                        (statement, RDF.predicate, p),
                        (statement, RDF.object, instance),
                        (statement, RDFS.isDefinedBy, see_also),
                    ],
                ),
            ],
        ),
        Optional(
            GraphPattern(
                workspace,
                [
                    (instance, RDFS.isDefinedBy, is_defined_by),
                    (is_defined_by, RDF.type, HERBIE.DocumentVersion),
                    (is_defined_by, PROV.specializationOf, is_defined_by_document),
                    Optional((is_defined_by, RDFS.label, is_defined_by_label)),
                ],
            ),
            Optional(
                GraphPattern(
                    workspace,
                    [
                        (is_defined_by, DCTERMS.conformsTo, conforms_to),
                        (conforms_to, RDF.type, HERBIE.DocumentVersion),
                        Optional((conforms_to, DCTERMS.conformsTo, conforms_to)),
                        Optional((conforms_to, RDFS.label, conforms_to_label)),
                        Optional((conforms_to, RDFS.comment, conforms_to_comment)),
                    ],
                ),
            ),
        ),
    )
    return construct


def construct_support(
    objects: List[URIRef],
    /,
    workspace: URIRef,
):
    subject = Var("subject")
    label = Var("label")
    cls = Var("class")
    parent_cls = Var("class")

    return Construct(
        (subject, RDFS.label, label),
        (subject, RDF.type, cls),
        (subject, RDF.type, parent_cls),
    ).where(
        Values(subject, *objects),
        GraphPattern(
            workspace,
            [
                (subject, RDFS.label, label),
                Optional(
                    (subject, RDF.type, cls),
                    Filter(f"isIRI({cls})"),
                    Optional(
                        (cls, ZeroOrMore(RDFS.subClassOf), parent_cls),
                        Filter(f"isIRI({parent_cls})"),
                    ),
                ),
            ],
        ),
    )


# SELECTS


def select_document_version_published_via_iri_alias(iri_workspace, iri, document_version):
    author = Var("author")
    workspace = Var("workspace")
    document = Var("document")
    modified = Var("modified")
    source = Var("source")

    return (
        Select(document_version)
        .where(
            Values(workspace, URIRef(iri_workspace)),
            Values(source, URIRef(iri)),
            GraphPattern(
                document,
                [
                    (document_version, DCTERMS.modified, modified),
                    (document_version, DCTERMS.source, source),
                ]
                + herbie_document(author, workspace, document)
                + herbie_document_version_published(document, document_version),
            ),
        )
        .order_by(f"DESC({modified})")
        .limit(1)
        .distinct()
    )


def select_documents_published(
    selected,
    iri_workspace=None,
    document=None,
    document_version_published=None,
):
    author = Var("author")
    workspace = Var("workspace")
    if document is None:
        document = Var("document")
    if document_version_published is None:
        document_version_published = Var("documentVersionPublished")

    return Select(*selected).where(
        Values(workspace, URIRef(iri_workspace)) if iri_workspace is not None else None,
        GraphPattern(
            document,
            herbie_document(author, workspace, document)
            + herbie_document_version_published(document, document_version_published),
        ),
    )


def select_documents_being_edited(
    selected,
    iri_workspace=None,
    iri_author=None,
    document=None,
    document_version_draft=None,
):
    author_document = Var("authorDocument")
    author_draft = Var("authorDraft")
    workspace = Var("workspace")
    if document is None:
        document = Var("document")
    if document_version_draft is None:
        document_version_draft = Var("documentVersionDraft")

    return Select(*selected).where(
        Values(workspace, URIRef(iri_workspace)) if iri_workspace is not None else None,
        Values(author_draft, URIRef(iri_author)) if iri_author is not None else None,
        GraphPattern(
            document,
            herbie_document(author_document, workspace, document)
            + herbie_document_draft(author_draft, document, document_version_draft),
        ),
    )


def select_iri_is_defined_by(iri, is_defined_by):
    graph = Var("graph")

    return (
        Select(is_defined_by)
        .where(
            GraphPattern(
                graph,
                [(iri, RDFS.isDefinedBy, is_defined_by)],
            )
        )
        .distinct()
    )


# TRIPLE PATTERNS


def herbie_document(author, workspace, document, document_access=None):
    if document_access is None:
        document_access = Var("documentAccess")

    return [
        (document, HERBIE.hasPrefix, URIRef(settings.IRI_PREFIX)),
        (document, HERBIE.workspace, workspace),
        (document, RDF.type, HERBIE.Document),
        (document, PROV.wasAttributedTo, author),
        (document, PROV.hadMember, document_access),
    ]


def herbie_document_with_document_version(
    author_document,
    author_draft,
    workspace,
    document,
    document_version,
    document_access=None,
):
    return herbie_document(author_document, workspace, document, document_access) + [
        Union(
            herbie_document_draft(author_draft, document, document_version),
            herbie_document_version_published(document, document_version),
        )
    ]


def herbie_document_draft(author_draft, document, document_draft):
    return [
        (document_draft, RDF.type, HERBIE.DocumentDraft),
        (document_draft, PROV.wasAttributedTo, author_draft),
        (document_draft, PROV.specializationOf, document),
    ]


def herbie_document_version_published(document, document_version_published):
    return [
        (document, HERBIE.latestVersionPublished, document_version_published),
        (document_version_published, RDF.type, HERBIE.DocumentVersion),
        (document_version_published, PROV.specializationOf, document),
    ]
