# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

import re
from datetime import datetime


class Matcher:
    pass


class matches(Matcher):
    def __init__(self, pattern, flags=0):
        self._regex = re.compile(pattern, flags)

    def __eq__(self, actual):
        return bool(self._regex.match(actual))

    def __repr__(self):
        return self._regex.pattern


class some_api_path(matches):
    def __init__(self, resource):
        super(some_api_path, self).__init__(".*/api/{}/".format(resource))


class some_bool(Matcher):
    def __eq__(self, actual):
        return bool(isinstance(actual, bool))


class some_int(Matcher):
    def __eq__(self, actual):
        return bool(isinstance(actual, int))


class some_int_or_none(Matcher):
    def __eq__(self, actual):
        return bool(isinstance(actual, int)) or actual is None


class some_str(Matcher):
    def __eq__(self, actual):
        return bool(isinstance(actual, str))


class some_dict(Matcher):
    def __eq__(self, actual):
        return bool(isinstance(actual, dict))


class some_list(Matcher):
    def __eq__(self, actual):
        return bool(isinstance(actual, list))


class some_dict_or_none(Matcher):
    def __eq__(self, actual):
        return bool(isinstance(actual, dict)) or actual is None


class some_datetime(Matcher):
    def __eq__(self, actual):
        return bool(isinstance(actual, datetime))
