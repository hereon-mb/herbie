from django.dispatch import receiver
from django_helmholtz_aai import signals

from .models.author import Author
from .tasks.synchronization import synchronize_authors


@receiver(signals.aai_user_created)
def on_user_created(sender, user, request, userinfo, **kwargs):
    Author.objects.get_or_create(user=user)
    synchronize_authors.delay()


@receiver(signals.aai_user_updated)
def on_user_updated(sender, user, request, userinfo, **kwargs):
    synchronize_authors.delay()
