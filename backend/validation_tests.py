# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from dataclasses import dataclass
from typing import Any

from django.core.exceptions import ValidationError

from .validation import validate_non_empty, validate_present


@dataclass
class Model:
    field: Any


@dataclass
class WithCount:
    elements: Any

    def count(self):
        return len(self.elements)


def test_validate_present():
    assert validate_present(Model(field=None), "field", {}) == {"field": ValidationError("blank", code="blank")}
    assert validate_present(Model(field=""), "field", {}) == {"field": ValidationError("blank", code="blank")}
    assert validate_present(Model(field="something"), "field", {}) == {}


def test_validate_non_empty():
    assert validate_non_empty(Model(field=WithCount([])), "field", {}) == {"field": ValidationError("required", code="required")}
    assert validate_non_empty(Model(field=WithCount([0])), "field", {}) == {}
