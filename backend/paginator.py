import json
from base64 import b64decode, b64encode
from dataclasses import dataclass
from datetime import datetime
from typing import Optional

from rdflib import DCTERMS, XSD, Literal, URIRef
from rest_framework.exceptions import ValidationError
from rest_framework.utils.urls import replace_query_param

from .queries import herbie_document_with_document_version
from .services.triple_store import TripleStore
from .sparql import Ask, Filter, GraphPattern, Select, Values, Var


@dataclass(frozen=True)
class Cursor:
    document: Optional[str]
    created_at: Optional[datetime]
    updated_at: datetime

    @classmethod
    def current(cls):
        return cls(
            document=None,
            created_at=None,
            updated_at=datetime.now(),
        )

    @classmethod
    def from_row(cls, row):
        return cls(
            document=row["document"]["value"],
            created_at=datetime.fromisoformat(row["created_at"]["value"].replace("Z", "+00:00")),
            updated_at=datetime.fromisoformat(row["updated_at"]["value"].replace("Z", "+00:00")),
        )

    @classmethod
    def from_json(cls, raw):
        data = json.loads(raw)
        return cls(
            document=data["document"],
            created_at=datetime.fromisoformat(data["created_at"]) if data["created_at"] else None,
            updated_at=datetime.fromisoformat(data["updated_at"]),
        )

    def to_json(self):
        return json.dumps(
            {
                "document": self.document if self.document else None,
                "created_at": self.created_at.isoformat() if self.created_at else None,
                "updated_at": self.updated_at.isoformat(),
            },
        )


class Page:
    def __init__(
        self,
        result,
        url: str,
        iri_workspace: URIRef,
        graph_triple_patterns,
        cursor_prev: Optional[str],
        cursor_next: Optional[str],
        page_size: int,
        var_document: str,
        var_created_at: str,
        var_updated_at: str,
        var_author: str,
    ):
        self._triple_store = TripleStore()
        self._result = result
        self._url = url
        self._iri_workspace = iri_workspace
        self._graph_triple_patterns = graph_triple_patterns
        self._cursor_prev = cursor_prev
        self._cursor_next = cursor_next
        self._page_size = page_size
        self._var_document = var_document
        self._var_created_at = var_created_at
        self._var_updated_at = var_updated_at
        self._var_author = var_author

    @property
    def documents(self):
        if self._result is None:
            return None
        else:
            return self._result["results"]["bindings"]

    @property
    def headers(self):
        headers = {}

        links = self.links
        if len(links) > 0:
            headers["Link"] = ", ".join(links)

        return headers

    @property
    def links(self):
        links = []

        if self.documents and len(self.documents) > 0:
            cursor_last = Cursor.from_row(self.documents[-1])
            cursor_first = Cursor.from_row(self.documents[0])

            if self._cursor_prev is None and self._cursor_next is None:
                if self._exists_document_created_before(cursor_last):
                    links.append(self._link_prev(cursor_last))
                if self._exists_document_created_after(cursor_first):
                    links.append(self._link_next(cursor_first))
                else:
                    links.append(self._link_next(Cursor.current()))
            elif self._cursor_prev is not None and self._cursor_next is None:
                if self._exists_document_created_before(cursor_last):
                    links.append(self._link_prev(cursor_last))
            elif self._cursor_prev is None and self._cursor_next is not None:
                if self._exists_document_created_after(cursor_last):
                    links.append(self._link_next(cursor_first))
                else:
                    links.append(self._link_next(Cursor.current()))
        elif self._cursor_prev is None:
            links.append(self._link_next(Cursor.current()))

        return links

    def _exists_document_created_before(self, cursor):
        author_document = Var("authorDocument")
        author_draft = Var("authorDraft")
        workspace = Var("workspace")
        document = Var(self._var_document, randomize=False)
        document_version = Var("documentVersion")
        created = Var(self._var_created_at, randomize=False)
        query_documents = Select(document, created).where(
            Values(workspace, URIRef(self._iri_workspace)),
            GraphPattern(
                document,
                [
                    (document, DCTERMS.created, created),
                    _is_created_earlier(cursor, self._var_document, self._var_created_at),
                ]
                + herbie_document_with_document_version(
                    author_document,
                    author_draft,
                    workspace,
                    document,
                    document_version,
                ),
            ),
        )
        graph_triple_patterns = self._graph_triple_patterns + [query_documents]
        query = Ask(*graph_triple_patterns)
        result = TripleStore().query(query)
        return result["boolean"]

    def _exists_document_created_after(self, cursor):
        author_document = Var("authorDocument")
        author_draft = Var("authorDraft")
        workspace = Var("workspace")
        document = Var(self._var_document, randomize=False)
        document_version = Var("documentVersion")
        modified = Var(self._var_updated_at, randomize=False)
        query_documents = Select(document, modified).where(
            Values(workspace, URIRef(self._iri_workspace)),
            GraphPattern(
                document,
                [
                    (document, DCTERMS.modified, modified),
                    _is_updated_later(cursor, self._var_document, self._var_updated_at),
                ]
                + herbie_document_with_document_version(
                    author_document,
                    author_draft,
                    workspace,
                    document,
                    document_version,
                ),
            ),
        )
        graph_triple_patterns = self._graph_triple_patterns + [query_documents]
        query = Ask(*graph_triple_patterns)
        result = TripleStore().query(query)
        return result["boolean"]

    def _link_prev(self, cursor):
        encoded = _cursor_encode(cursor)
        link = self._url
        link = replace_query_param(link, "prev", encoded)
        link = replace_query_param(link, "page_size", self._page_size)
        return f"<{link}>; rel=prev"

    def _link_next(self, cursor):
        encoded = _cursor_encode(cursor)
        link = self._url
        link = replace_query_param(link, "next", encoded)
        link = replace_query_param(link, "page_size", self._page_size)
        return f"<{link}>; rel=next"


class Paginator:
    def __init__(
        self,
        url: str,
        iri_workspace: URIRef,
        vars_selected,
        graph_triple_patterns,
        cursor_prev: Optional[str],
        cursor_next: Optional[str],
        page_size: int = 5,
        var_document: str = "document",
        var_created_at: str = "created_at",
        var_updated_at: str = "updated_at",
        var_author: str = "author",
    ):
        self._triple_store = TripleStore()
        self._url = url
        self._iri_workspace = iri_workspace
        self._vars_selected = vars_selected
        self._graph_triple_patterns = graph_triple_patterns
        self._cursor_prev = _cursor_decode(cursor_prev) if cursor_prev else None
        self._cursor_next = _cursor_decode(cursor_next) if cursor_next else None
        self._page_size = page_size
        self._var_document = var_document
        self._var_created_at = var_created_at
        self._var_updated_at = var_updated_at
        self._var_author = var_author

    def page(self):
        author_document = Var("authorDocument")
        author_draft = Var("authorDraft")
        workspace = Var("workspace")
        document = Var(self._var_document, randomize=False)
        document_version = Var("documentVersion")
        created = Var(self._var_created_at, randomize=False)
        modified = Var(self._var_updated_at, randomize=False)
        query_documents = (
            Select(document, created, modified)
            .where(
                Values(workspace, URIRef(self._iri_workspace)),
                GraphPattern(
                    document,
                    [
                        (document, DCTERMS.created, created),
                        (document, DCTERMS.modified, modified),
                    ]
                    + herbie_document_with_document_version(
                        author_document,
                        author_draft,
                        workspace,
                        document,
                        document_version,
                    ),
                ),
            )
            .distinct()
            .limit(self._page_size)
        )

        if self._cursor_prev is None and self._cursor_next is None:
            query_documents = query_documents.order_by(f"DESC(?{self._var_created_at}) DESC(?{self._var_document})")

        elif self._cursor_prev is not None and self._cursor_next is None:
            query_documents = query_documents.order_by(f"DESC(?{self._var_created_at}) DESC(?{self._var_document})")
            query_documents = query_documents.where(
                _is_created_earlier(self._cursor_prev, self._var_document, self._var_created_at)
            )

        elif self._cursor_prev is None and self._cursor_next is not None:
            query_documents = query_documents.order_by(f"ASC(?{self._var_updated_at}) ASC(?{self._var_document})")
            query_documents = query_documents.where(
                _is_updated_later(self._cursor_next, self._var_document, self._var_updated_at)
            )

        else:
            raise ValidationError("prev and next must not be set at the same time")

        vars_selected = [
            Var(var_selected, randomize=False)
            for var_selected in list(set(self._vars_selected + [self._var_created_at, self._var_updated_at]))
        ]
        graph_triple_patterns = self._graph_triple_patterns + [query_documents]
        query = Select(*vars_selected).where(*graph_triple_patterns)

        return Page(
            result=self._triple_store.query(query),
            url=self._url,
            iri_workspace=self._iri_workspace,
            graph_triple_patterns=self._graph_triple_patterns,
            cursor_prev=self._cursor_prev,
            cursor_next=self._cursor_next,
            page_size=self._page_size,
            var_document=self._var_document,
            var_created_at=self._var_created_at,
            var_updated_at=self._var_updated_at,
            var_author=self._var_author,
        )


def _is_created_earlier(
    cursor,
    var_document: str,
    var_created_at: str,
):
    created_at = Literal(cursor.created_at, datatype=XSD.dateTime).n3()
    document = URIRef(cursor.document)

    return Filter(
        f"?{var_created_at} < {created_at}"
        + "|| ("
        + f"?{var_created_at} = {created_at}"
        + f'&& STR(?{var_document}) < "{document}"'
        + ")"
    )


def _is_updated_later(
    cursor,
    var_document: str,
    var_updated_at: str,
):
    updated_at = Literal(cursor.updated_at, datatype=XSD.dateTime).n3()
    document = Literal(cursor.document) if cursor.document else None

    if document:
        return Filter(
            f"?{var_updated_at} > {updated_at}"
            + "|| ("
            + f"?{var_updated_at} = {updated_at}"
            + f'&& STR(?{var_document}) > "{document}"'
            + ")"
        )
    else:
        return Filter(f"?{var_updated_at} >= {updated_at}")


def _cursor_encode(cursor: Cursor):
    querystring = cursor.to_json()
    return b64encode(querystring.encode("ascii")).decode("ascii")


def _cursor_decode(encoded: str) -> Cursor:
    querystring = b64decode(encoded.encode("ascii")).decode("ascii")
    return Cursor.from_json(querystring)
