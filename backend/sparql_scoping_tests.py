import re

import pytest
from rdflib import RDFS, Literal, Variable
from rdflib.plugins.sparql.algebra import (
    BGP,
    Filter,
    Graph,
    OrderBy,
    Project,
    ToMultiSet,
    Union,
    Values,
    _addVars,
    _traverseAgg,
    translateQuery,
)
from rdflib.plugins.sparql.parser import parseQuery
from rdflib.plugins.sparql.parserutils import CompValue

from .sparql_scoping import scope_query


@pytest.mark.explorative
def test__algebra__bgp():
    PV = [
        Variable("s"),
        Variable("p"),
        Variable("o"),
    ]
    assert from_string("SELECT * WHERE { ?s ?p ?o }") == from_value(
        SelectQuery(
            p=Project(
                p=BGP(
                    triples=[
                        (
                            Variable("s"),
                            Variable("p"),
                            Variable("o"),
                        )
                    ],
                ),
                PV=PV,
            ),
            PV=PV,
        )
    )


@pytest.mark.explorative
def test__algebra__distinct():
    PV = [
        Variable("s"),
        Variable("p"),
        Variable("o"),
    ]
    assert from_string("SELECT DISTINCT * WHERE { ?s ?p ?o }") == from_value(
        SelectQuery(
            p=Distinct(
                p=Project(
                    p=BGP(
                        triples=[
                            (
                                Variable("s"),
                                Variable("p"),
                                Variable("o"),
                            )
                        ],
                    ),
                    PV=PV,
                ),
            ),
            PV=PV,
        )
    )


@pytest.mark.explorative
def test__algebra__graph_graph_pattern():
    PV = [
        Variable("g"),
        Variable("o"),
        Variable("p"),
        Variable("s"),
    ]
    assert from_string("SELECT * WHERE { GRAPH ?g { ?s ?p ?o } }") == from_value(
        SelectQuery(
            p=Project(
                p=Graph(
                    term=Variable("g"),
                    graph=BGP(
                        triples=[
                            (
                                Variable("s"),
                                Variable("p"),
                                Variable("o"),
                            )
                        ],
                    ),
                ),
                PV=PV,
            ),
            PV=PV,
        )
    )


@pytest.mark.explorative
def test__algebra__graph_pattern__two():
    PV = [
        Variable("s"),
        Variable("p"),
        Variable("o"),
    ]
    assert from_string("SELECT * WHERE { { ?s ?p ?o } { ?s ?p ?o } }") == from_value(
        SelectQuery(
            p=Project(
                p=Join(
                    p1=BGP(
                        triples=[
                            (
                                Variable("s"),
                                Variable("p"),
                                Variable("o"),
                            )
                        ],
                    ),
                    p2=BGP(
                        triples=[
                            (
                                Variable("s"),
                                Variable("p"),
                                Variable("o"),
                            )
                        ],
                    ),
                ),
                PV=PV,
            ),
            PV=PV,
        )
    )


@pytest.mark.explorative
def test__algebra__subselect():
    PV = [
        Variable("s"),
        Variable("p"),
        Variable("o"),
    ]
    assert from_string("SELECT * WHERE { { SELECT * WHERE { ?s ?p ?o } } }") == from_value(
        SelectQuery(
            p=Project(
                p=ToMultiSet(
                    p=Project(
                        p=BGP(
                            triples=[
                                (
                                    Variable("s"),
                                    Variable("p"),
                                    Variable("o"),
                                )
                            ],
                        ),
                        PV=PV,
                    ),
                ),
                PV=PV,
            ),
            PV=PV,
        )
    )


@pytest.mark.explorative
def test__algebra__subselect_and_graph_graph_pattern():
    PV = [
        Variable("g"),
        Variable("s"),
        Variable("o"),
        Variable("p"),
    ]
    PV_nested = [
        Variable("s"),
        Variable("p"),
        Variable("o"),
    ]
    assert from_string("SELECT * WHERE { GRAPH ?g { ?s ?p ?o } { SELECT * WHERE { ?s ?p ?o } } }") == from_value(
        SelectQuery(
            p=Project(
                p=Join(
                    p1=Graph(
                        term=Variable("g"),
                        graph=BGP(
                            triples=[
                                (
                                    Variable("s"),
                                    Variable("p"),
                                    Variable("o"),
                                )
                            ],
                        ),
                    ),
                    p2=ToMultiSet(
                        p=Project(
                            p=BGP(
                                triples=[
                                    (
                                        Variable("s"),
                                        Variable("p"),
                                        Variable("o"),
                                    )
                                ],
                            ),
                            PV=PV_nested,
                        ),
                    ),
                ),
                PV=PV,
            ),
            PV=PV,
        )
    )


@pytest.mark.explorative
def test__algebra__union():
    PV = [
        Variable("s"),
        Variable("p"),
        Variable("o"),
    ]
    assert from_string("SELECT * WHERE { { ?s ?p ?o } UNION { ?s ?p ?o } }") == from_value(
        SelectQuery(
            p=Project(
                p=Union(
                    p1=BGP(
                        triples=[
                            (
                                Variable("s"),
                                Variable("p"),
                                Variable("o"),
                            )
                        ],
                    ),
                    p2=BGP(
                        triples=[
                            (
                                Variable("s"),
                                Variable("p"),
                                Variable("o"),
                            )
                        ],
                    ),
                ),
                PV=PV,
            ),
            PV=PV,
        )
    )


@pytest.mark.explorative
def test__algebra__values():
    PV = [Variable("s")]
    assert from_string("SELECT * WHERE { VALUES ?s { 1 2 3 } }") == from_value(
        SelectQuery(
            p=Project(
                p=ToMultiSet(
                    p=Values(
                        [
                            {Variable("s"): Literal(1)},
                            {Variable("s"): Literal(2)},
                            {Variable("s"): Literal(3)},
                        ]
                    ),
                ),
                PV=PV,
            ),
            PV=PV,
        )
    )


@pytest.mark.explorative
def test__algebra__filter():
    PV = [
        Variable("s"),
        Variable("p"),
        Variable("o"),
    ]
    assert from_string("SELECT * WHERE { ?s ?p ?o FILTER BOUND(?s) }") == from_value(
        SelectQuery(
            p=Project(
                p=Filter(
                    expr=BOUND(arg=Variable("s")),
                    p=BGP(
                        triples=[
                            (
                                Variable("s"),
                                Variable("p"),
                                Variable("o"),
                            )
                        ],
                    ),
                ),
                PV=PV,
            ),
            PV=PV,
        )
    )


@pytest.mark.explorative
def test__algebra__limit_and_offset():
    PV = [
        Variable("s"),
        Variable("p"),
        Variable("o"),
    ]
    assert from_string("SELECT * WHERE { ?s ?p ?o } LIMIT 100 OFFSET 10") == from_value(
        SelectQuery(
            p=Slice(
                p=Project(
                    p=BGP(
                        triples=[
                            (
                                Variable("s"),
                                Variable("p"),
                                Variable("o"),
                            )
                        ],
                    ),
                    PV=PV,
                ),
                start=10,
                length=100,
            ),
            PV=PV,
        )
    )


@pytest.mark.explorative
def test__algebra__distinct__and__limit_and_offset():
    PV = [
        Variable("s"),
        Variable("p"),
        Variable("o"),
    ]
    assert from_string("SELECT DISTINCT * WHERE { ?s ?p ?o } LIMIT 100 OFFSET 10") == from_value(
        SelectQuery(
            p=Slice(
                p=Distinct(
                    p=Project(
                        p=BGP(
                            triples=[
                                (
                                    Variable("s"),
                                    Variable("p"),
                                    Variable("o"),
                                )
                            ],
                        ),
                        PV=PV,
                    ),
                ),
                start=10,
                length=100,
            ),
            PV=PV,
        )
    )


@pytest.mark.explorative
def test__algebra__order_by():
    PV = [
        Variable("s"),
        Variable("p"),
        Variable("o"),
    ]
    assert from_string("SELECT * WHERE { ?s ?p ?o } ORDER BY ?s") == from_value(
        SelectQuery(
            p=Project(
                p=OrderBy(
                    p=BGP(
                        triples=[
                            (
                                Variable("s"),
                                Variable("p"),
                                Variable("o"),
                            )
                        ],
                    ),
                    expr=[
                        OrderCondition(
                            expr=Variable("s"),
                            order=None,
                        )
                    ],
                ),
                PV=PV,
            ),
            PV=PV,
        )
    )


@pytest.mark.explorative
def test__algebra__prefix():
    query = """PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        SELECT * WHERE { ?s rdfs:label "label" }"""
    PV = [Variable("s")]
    assert from_string(query) == from_value(
        SelectQuery(
            p=Project(
                p=BGP(
                    triples=[
                        (
                            Variable("s"),
                            RDFS.label,
                            Literal("label"),
                        )
                    ],
                ),
                PV=PV,
            ),
            PV=PV,
        )
    )


def test__scope_query__bgp():
    workspace = "http://example.org/workspace/"
    query = "SELECT * WHERE { ?s ?p ?o }"
    assert scope_query(query, workspace)


def test__scope_query__optional():
    workspace = "http://example.org/workspace/"
    query = "SELECT * WHERE { OPTIONAL { ?s ?p ?o } }"
    assert scope_query(query, workspace)


def test__scope_query__union():
    workspace = "http://example.org/workspace/"
    query = "SELECT * WHERE { { ?s ?p ?o } UNION { ?s ?p ?o } }"
    assert scope_query(query, workspace)


def test__scope_query__filter():
    workspace = "http://example.org/workspace/"
    query = "SELECT * WHERE { ?s ?p ?o FILTER BOUND(?s) }"
    assert scope_query(query, workspace)


def test__scope_query__unmentioned_variable():
    workspace = "http://example.org/workspace/"
    query = "SELECT ?a WHERE { ?s ?p ?o }"
    assert scope_query(query, workspace)


# SUPPORT


def from_string(query):
    return translateQuery(parseQuery(query)).algebra


def from_value(value):
    _traverseAgg(value, _addVars)
    return value


def SelectQuery(p, PV):
    return CompValue(
        "SelectQuery",
        p=p,
        datasetClause=None,
        PV=PV,
    )


def Distinct(p):
    return CompValue("Distinct", p=p)


def Join(p1, p2):
    return CompValue("Join", p1=p1, p2=p2, lazy=True)


def Slice(p, start, length):
    return CompValue("Slice", p=p, start=start, length=length)


def OrderCondition(expr, order):
    return CompValue("OrderCondition", expr=expr, order=order)


def BOUND(arg):
    return CompValue(
        "Builtin__BOUND",
        arg=arg,
    )


def clean_up(query):
    return re.sub(r"_\w\w\w\w", "", str(query))
