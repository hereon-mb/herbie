# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from django.contrib import admin


class AuthorAdmin(admin.ModelAdmin):
    list_display = (
        "user",
        "first_name",
        "last_name",
        "email",
        "department",
        "head_of_department",
        "can_send_sparql_queries",
        "can_upload_raw_files",
        "profiler_enabled",
    )

    list_filter = (
        "department",
        "head_of_department",
    )

    @admin.display(  # type: ignore
        description="First Name",
        ordering="user__first_name",
    )
    def first_name(self, obj):
        return obj.user.first_name

    @admin.display(  # type: ignore
        description="Last Name",
        ordering="user__last_name",
    )
    def last_name(self, obj):
        return obj.user.last_name

    @admin.display(  # type: ignore
        description="Email Address",
        ordering="user__email",
    )
    def email(self, obj):
        return obj.user.email

    @admin.display(  # type: ignore
        description="Can send SPARQL queries?",
    )
    def can_send_sparql_queries(self, obj):
        return obj.can_send_sparql_queries

    @admin.display(  # type: ignore
        description="Can upload raw files?",
    )
    def can_upload_raw_files(self, obj):
        return obj.can_upload_raw_files
