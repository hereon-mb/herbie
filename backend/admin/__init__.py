# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.


from django.contrib import admin
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.utils.html import format_html
from pygments import highlight
from pygments.formatters import HtmlFormatter
from pygments.lexers import TurtleLexer
from rdflib import Graph
from silk.models import mark_safe

from ..decorators.rdf_document_dataset import RDFDocumentDecorator
from ..models.author import Author
from ..models.cache_reset import CacheReset
from ..models.department import Department
from ..models.rdf_document import RDFDocument, RDFDocumentVersion
from ..models.rdf_identifier import RDFIdentifier
from ..models.ro_crate_import import RoCrateImport
from ..models.workspace import Workspace
from ..namespaces import Namespaces
from ..services.triple_store import TripleStore
from ..sparql import Construct, GraphPattern, Var
from .author import AuthorAdmin
from .my_user import MyUserAdmin

admin.site.unregister(get_user_model())
admin.site.register(get_user_model(), MyUserAdmin)
admin.site.register(Author, AuthorAdmin)


@admin.register(CacheReset)
class CacheResetAdmin(admin.ModelAdmin):
    list_display = ("my_created_at",)

    @admin.display(description="Created at ")  # type: ignore
    def my_created_at(self, obj):
        return obj.created_at.strftime("%Y-%m-%d %H:%M:%S")


@admin.register(Department)
class DepartmentAdmin(admin.ModelAdmin):
    list_display = (
        "name_en",
        "name_de",
        "abbreviation",
    )


@admin.register(Workspace)
class WorkspaceAdmin(admin.ModelAdmin):
    list_display = (
        "uuid",
        "name",
        "slug",
        "created_at",
        "updated_at",
        "created_by",
        "deleted_at",
    )

    fields = (
        ("uuid", "name", "slug"),
        "description",
        ("created_at", "updated_at", "created_by"),
        "deleted_at",
    )

    readonly_fields = [
        "uuid",
        "created_at",
        "updated_at",
        "created_by",
        "deleted_at",
    ]


@admin.register(RDFDocument)
class RDFDocumentAdmin(admin.ModelAdmin):
    list_display = (
        "uuid",
        "workspace",
        "version_draft",
        "version_published",
        "versions",
        "created_at",
        "updated_at",
        "created_by",
        "published_at",
        "published_by",
    )

    fields = (
        ("uuid", "workspace"),
        ("version_draft", "version_published", "versions"),
        ("created_at", "updated_at", "created_by"),
        ("published_at", "published_by"),
        "graph_meta",
        "graph_access_pretty",
    )

    readonly_fields = [
        "uuid",
        "version_draft",
        "version_published",
        "versions",
        "created_at",
        "updated_at",
        "created_by",
        "published_at",
        "published_by",
        "graph_meta",
        "graph_access_pretty",
    ]

    actions = [
        "update_in_triple_store",
    ]

    def get_queryset(self, request):
        return super().get_queryset(request).with_prefetches()

    @admin.display(description="created at")  # type: ignore
    def created_at(self, obj):
        return obj.created_at.isoformat()

    @admin.display(description="updated at")  # type: ignore
    def updated_at(self, obj):
        return obj.updated_at.isoformat()

    @admin.display(description="created by")  # type: ignore
    def created_by(self, obj):
        link = reverse("admin:backend_author_change", args=[obj.created_by.user_id])
        return format_html('<a href="{}">{}</a>', link, obj.created_by.user.username)

    @admin.display(description="published at")  # type: ignore
    def published_at(self, obj):
        if obj.published_at:
            return obj.published_at.isoformat()
        else:
            return None

    @admin.display(description="published by")  # type: ignore
    def published_by(self, obj):
        if obj.published_by:
            link = reverse("admin:backend_author_change", args=[obj.published_by.user_id])
            return format_html('<a href="{}">{}</a>', link, obj.published_by.user.username)
        else:
            return None

    @admin.display(description="draft")  # type: ignore
    def version_draft(self, obj):
        if obj.version_draft:
            link = reverse("admin:backend_rdfdocumentversion_change", args=[obj.version_draft.id])
            return format_html('<a href="{}">#{}</a>', link, _print_index(obj.version_draft.version_index))
        else:
            return None

    @admin.display(description="published")  # type: ignore
    def version_published(self, obj):
        if obj.version_published:
            link = reverse("admin:backend_rdfdocumentversion_change", args=[obj.version_published.id])
            return format_html('<a href="{}">#{}</a>', link, _print_index(obj.version_published.version_index))
        else:
            return None

    @admin.display(description="versions")  # type: ignore
    def versions(self, obj):
        if obj.version_published:
            return mark_safe(
                ", ".join(
                    [
                        format_html(
                            '<a href="{}">#{}</a>',
                            reverse("admin:backend_rdfdocumentversion_change", args=[version.id]),
                            _print_index(version.version_index),
                        )
                        for version in obj.rdf_document_versions.all()
                    ]
                )
            )
        else:
            return None

    @admin.display(description="Graph meta (from triple store)")  # type: ignore
    def graph_meta(self, obj):
        iri_document = Namespaces().datasets(obj.uuid)
        return _pretty_print_graph_from_triple_store(iri_document)

    @admin.display(description="Graph access (from triple store)")  # type: ignore
    def graph_access_pretty(self, obj):
        iri_document = Namespaces().datasets_access(obj.uuid)
        return _pretty_print_graph_from_triple_store(iri_document)

    def update_in_triple_store(self, request, queryset):
        for rdf_document in queryset.all():
            decorated = RDFDocumentDecorator(rdf_document)
            triple_store = TripleStore()
            for graph in decorated.dataset.contexts():
                triple_store.put_graph(graph)

    update_in_triple_store.short_description = "Update in Triple Store"  # type: ignore


@admin.register(RDFDocumentVersion)
class RDFDocumentVersionAdmin(admin.ModelAdmin):
    list_display = (
        "rdf_document_uuid",
        "version_index_actual",
        "created_at_pretty",
        "updated_at_pretty",
        "deleted_at_pretty",
        "created_by_pretty",
    )

    fields = [
        "rdf_document_uuid",
        "version_index_actual",
        "created_by_pretty",
        ("created_at_pretty", "updated_at_pretty", "deleted_at_pretty"),
        ("valid_from_pretty", "valid_to_pretty"),
        "graph_entered_pretty",
        "graph_generated_pretty",
        "graph_persisted_pretty",
        "graph_meta",
    ]

    readonly_fields = [
        "rdf_document_uuid",
        "version_index_actual",
        "created_by_pretty",
        "created_at_pretty",
        "updated_at_pretty",
        "deleted_at_pretty",
        "valid_from_pretty",
        "valid_to_pretty",
        "graph_entered_pretty",
        "graph_generated_pretty",
        "graph_persisted_pretty",
        "graph_meta",
    ]

    @admin.display(description="RDF Document")  # type: ignore
    def rdf_document_uuid(self, obj):
        link = reverse("admin:backend_rdfdocument_change", args=[obj.rdf_document.id])
        return format_html('<a href="{}">{}</a>', link, obj.rdf_document.uuid)

    @admin.display(description="Version index")  # type: ignore
    def version_index_actual(self, obj):
        link = reverse("admin:backend_rdfdocumentversion_change", args=[obj.id])
        return format_html('<a href="{}">#{}</a>', link, _print_index(obj.version_index))

    @admin.display(description="created at")  # type: ignore
    def created_at_pretty(self, obj):
        return obj.created_at.isoformat()

    @admin.display(description="updated at")  # type: ignore
    def updated_at_pretty(self, obj):
        return obj.updated_at.isoformat()

    @admin.display(description="deleted at")  # type: ignore
    def deleted_at_pretty(self, obj):
        if obj.deleted_at:
            return obj.deleted_at.isoformat()
        else:
            return None

    @admin.display(description="valid from")  # type: ignore
    def valid_from_pretty(self, obj):
        if obj.valid_from:
            return obj.valid_from.isoformat()
        else:
            return None

    @admin.display(description="valid to")  # type: ignore
    def valid_to_pretty(self, obj):
        if obj.valid_to:
            return obj.valid_to.isoformat()
        else:
            return None

    @admin.display(description="created by")  # type: ignore
    def created_by_pretty(self, obj):
        link = reverse("admin:backend_author_change", args=[obj.created_by.user_id])
        return format_html('<a href="{}">{}</a>', link, obj.created_by.user.username)

    @admin.display(description="Graph entered")  # type: ignore
    def graph_entered_pretty(self, obj):
        return _pretty_print_json_ld(obj.graph_entered)

    @admin.display(description="Graph generated")  # type: ignore
    def graph_generated_pretty(self, obj):
        return _pretty_print_json_ld(obj.graph_generated)

    @admin.display(description="Graph persisted")  # type: ignore
    def graph_persisted_pretty(self, obj):
        return _pretty_print_json_ld(obj.graph_persisted)

    @admin.display(description="Graph meta (from triple store)")  # type: ignore
    def graph_meta(self, obj):
        if obj.draft:
            iri_document = Namespaces().datasets_draft(obj.rdf_document.uuid)
        else:
            iri_document = Namespaces().datasets_version(obj.rdf_document.uuid, _print_index(obj.version_index))
        return _pretty_print_graph_from_triple_store(iri_document)


@admin.register(RoCrateImport)
class RoCrateImportAdmin(admin.ModelAdmin):
    list_display = (
        "created_at_pretty",
        "started_at_pretty",
        "stopped_at_pretty",
        "status",
        "created_by_pretty",
        "workspace_pretty",
    )

    fields = [
        ("created_at_pretty", "started_at_pretty", "stopped_at_pretty"),
        "status",
        "created_by_pretty",
        "workspace_pretty",
        "file",
    ]

    readonly_fields = [
        "created_at_pretty",
        "started_at_pretty",
        "stopped_at_pretty",
        "created_by_pretty",
        "workspace_pretty",
        "file",
    ]

    @admin.display(description="created at")  # type: ignore
    def created_at_pretty(self, obj):
        return obj.created_at.isoformat()

    @admin.display(description="started at")  # type: ignore
    def started_at_pretty(self, obj):
        if obj.started_at:
            return obj.started_at.isoformat()
        else:
            return None

    @admin.display(description="stopped at")  # type: ignore
    def stopped_at_pretty(self, obj):
        if obj.stopped_at:
            return obj.stopped_at.isoformat()
        else:
            return None

    @admin.display(description="created by")  # type: ignore
    def created_by_pretty(self, obj):
        link = reverse("admin:backend_author_change", args=[obj.created_by.user_id])
        return format_html('<a href="{}">{}</a>', link, obj.created_by.user.username)

    @admin.display(description="workspace")  # type: ignore
    def workspace_pretty(self, obj):
        link = reverse("admin:backend_workspace_change", args=[obj.workspace.id])
        return format_html('<a href="{}">{}</a>', link, obj.workspace.name)


@admin.register(RDFIdentifier)
class RDFIdentifierAdmin(admin.ModelAdmin):
    list_display = (
        "container_path_segments",
        "rdf_class_slug",
        "uuid",
    )


def _print_index(index):
    if index is not None:
        return index + 1
    else:
        return "no index"


def _pretty_print_json_ld(raw):
    graph = Graph().parse(data=raw, format="json-ld")
    return _pretty_print_graph(graph)


def _pretty_print_graph_from_triple_store(iri):
    s, p, o = Var("s"), Var("p"), Var("o")

    graph = TripleStore().construct(
        Construct((s, p, o)).where(
            GraphPattern(iri, [(s, p, o)]),
        )
    )
    return _pretty_print_graph(graph, iri)


def _pretty_print_graph(graph, iri=None):
    formatter = HtmlFormatter(style="colorful")
    serialized = graph.serialize(format="turtle")
    if iri:
        serialized = f"# {iri}\n" + serialized
    response = highlight(serialized, TurtleLexer(), formatter)
    style = "<style>" + formatter.get_style_defs() + "</style><br>"
    return mark_safe(style + response)
