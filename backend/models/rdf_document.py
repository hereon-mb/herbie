# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from functools import cached_property
from uuid import uuid4

from django.core.validators import URLValidator
from django.db import models
from django.db.models import Exists, OuterRef, Prefetch, Q

from .author import Author
from .workspace import Workspace


class RDFDocumentQuerySet(models.QuerySet):
    def being_edited(self):
        versions_draft = self._versions().draft()
        return self.filter(Exists(versions_draft))

    def published(self):
        versions_published = self._versions().published()
        return self.filter(Exists(versions_published))

    def with_prefetches(self):
        rdf_document_versions = Prefetch(
            "rdf_document_versions",
            RDFDocumentVersion.objects.all()
            .select_related("created_by")
            .select_related("created_by__user")
            .prefetch_related("iri_aliases"),
        )
        return self.prefetch_related(rdf_document_versions)

    def _versions(self):
        return RDFDocumentVersion.objects.filter(Q(rdf_document=OuterRef("pk")))


class RDFDocumentManager(models.Manager):
    def get_queryset(self):
        return RDFDocumentQuerySet(self.model, using=self._db)


class RDFDocument(models.Model):
    uuid = models.UUIDField(default=uuid4, editable=False)

    workspace = models.ForeignKey(Workspace, on_delete=models.PROTECT)

    graph_access = models.JSONField()
    graph_access_updated_at = models.DateTimeField()

    objects = RDFDocumentManager()

    @property
    def created_at(self):
        return self.version_first.created_at

    @property
    def updated_at(self):
        version_last = self.version_last_including_deleted
        return version_last.deleted_at or version_last.updated_at

    @property
    def created_by(self):
        return self.version_first.created_by

    @property
    def published(self):
        return self.version_published is not None

    @property
    def published_at(self):
        return self.version_first.valid_from if self.published else None

    @property
    def published_by(self):
        return self.version_first.created_by if self.published else None

    @cached_property
    def version_draft(self):
        if self._versions_prefetched:
            return next(filter(lambda v: v.draft, self.rdf_document_versions.all()), None)
        else:
            return self.rdf_document_versions.all().draft().first()

    @cached_property
    def version_published(self):
        if self._versions_prefetched:
            version_publisheds = [version for version in self.rdf_document_versions.all() if version.published]
            if len(version_publisheds) > 0:
                return version_publisheds[-1]
            else:
                return None
        else:
            return self.rdf_document_versions.all().published().last()

    @cached_property
    def versions_published(self):
        if self._versions_prefetched:
            versions_published = [
                rdf_document_version
                for rdf_document_version in self.rdf_document_versions.all()
                if rdf_document_version.valid_from is not None
            ]
            versions_published.sort(key=lambda version: version.valid_from)
            return versions_published
        else:
            return self.rdf_document_versions.all().published()  # type: ignore

    @property
    def version_first(self):
        if self._versions_prefetched:
            return [version for version in self.rdf_document_versions.all()][0]
        else:
            return self.rdf_document_versions.all().first()

    @property
    def version_last(self):
        if self._versions_prefetched:
            return [version for version in self.rdf_document_versions.all()][-1]
        else:
            return self.rdf_document_versions.all().last()

    @property
    def version_last_including_deleted(self):
        if self._versions_prefetched:
            return [version for version in self.rdf_document_versions.all()][-1]
        else:
            return self.rdf_document_versions.all().last()

    @property
    def _versions_prefetched(self):
        return hasattr(self, "_prefetched_objects_cache") and "rdf_document_versions" in self._prefetched_objects_cache


class RDFDocumentVersionQuerySet(models.QuerySet):
    def draft(self):
        return self.filter(Q(valid_from=None) & Q(valid_to=None) & Q(deleted_at=None))

    def published(self):
        """All published versions sorted from oldest to most recent."""
        return self.filter(~Q(valid_from=None)).order_by("valid_from")

    def not_deleted(self):
        return self.filter(Q(deleted_at=None))


class RDFDocumentVersionManager(models.Manager):
    def get_queryset(self):
        return RDFDocumentVersionQuerySet(self.model, using=self._db)


class RDFDocumentVersion(models.Model):
    rdf_document = models.ForeignKey(RDFDocument, related_name="rdf_document_versions", on_delete=models.CASCADE)

    has_part = models.ManyToManyField("self", related_name="is_part_of", symmetrical=False)

    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    deleted_at = models.DateTimeField(blank=True, null=True)

    created_by = models.ForeignKey(Author, related_name="+", on_delete=models.PROTECT)

    valid_from = models.DateTimeField(blank=True, null=True)
    valid_to = models.DateTimeField(blank=True, null=True)

    graph_entered = models.JSONField()
    graph_entered_sha256 = models.CharField(max_length=64, blank=True, default="")

    graph_generated = models.JSONField()
    graph_generated_updated_at = models.DateTimeField()

    graph_persisted = models.JSONField()
    graph_persisted_updated_at = models.DateTimeField()

    objects = RDFDocumentVersionManager()

    @property
    def draft(self):
        return self.valid_from is None and self.valid_to is None and self.deleted_at is None

    @property
    def published(self):
        return self.valid_from is not None and self.valid_to is None

    @property
    def deleted(self):
        return self.deleted_at is not None

    @cached_property
    def version_index(self):
        # we assume that rdf_document_versions have been prefetched
        versions = [version for version in self.rdf_document.rdf_document_versions.all()]
        versions_sorted = sorted(versions, key=lambda version: (version.valid_from is None, version.valid_from))
        version_ids = [version.id for version in versions_sorted]
        if self.id in version_ids:
            return version_ids.index(self.id)
        else:
            return None


class IRIAlias(models.Model):
    rdf_document_version = models.ForeignKey(RDFDocumentVersion, related_name="iri_aliases", on_delete=models.CASCADE)

    iri = models.CharField(max_length=2048, validators=[URLValidator(schemes=["http", "https"])])

    class Meta:
        unique_together = [
            ["iri", "rdf_document_version"],
        ]
