# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from uuid import uuid4

import pytest
from django.contrib.auth.models import User
from django.forms.models import model_to_dict

from ..matchers import some_int
from .author import Author


def test_fixtures(author_alice, author_bob, user_alice, user_bob):
    assert model_to_dict(author_alice) == {
        "id": some_int(),
        "user": user_alice.pk,
        "department": None,
        "head_of_department": False,
        "can_create_rdf_documents": False,
        "can_send_sparql_queries": True,
        "can_upload_raw_files": True,
        "profiler_enabled": False,
    }
    assert model_to_dict(author_bob) == {
        "id": some_int(),
        "user": user_bob.pk,
        "department": None,
        "head_of_department": False,
        "can_create_rdf_documents": False,
        "can_send_sparql_queries": False,
        "can_upload_raw_files": False,
        "profiler_enabled": False,
    }


def test_get_or_create_for_username(author_alice, user_random, request):
    count_before = Author.objects.count()

    assert Author.objects.get_or_create_for_username("alice") == author_alice
    assert Author.objects.count() == count_before

    author = Author.objects.get_or_create_for_username(user_random().username)

    def delete_author():
        user = author.user
        author.delete()
        user.delete()

    request.addfinalizer(delete_author)
    assert author is not None
    assert Author.objects.count() == count_before + 1

    with pytest.raises(User.DoesNotExist):
        Author.objects.get_or_create_for_username(f"not_a_username_{uuid4()}")
    assert Author.objects.count() == count_before + 1
