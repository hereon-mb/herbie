# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from django.contrib.auth import get_user_model
from django.db import models
from rdflib import RDF, RDFS, Literal

from ..namespace._HERBIE import HERBIE
from ..namespaces import Namespaces
from .department import Department


class AuthorManager(models.Manager):
    def get_or_create_for_username(self, username):
        user = get_user_model().objects.get(username=username)
        author, _ = self.model.objects.get_or_create(user=user)
        return author


class Author(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    user = models.OneToOneField(get_user_model(), on_delete=models.PROTECT)
    department = models.ForeignKey(Department, related_name="members", on_delete=models.PROTECT, null=True, blank=True)
    head_of_department = models.BooleanField(default=False)
    profiler_enabled = models.BooleanField(default=False)

    can_create_rdf_documents = models.BooleanField(default=False)
    can_send_sparql_queries = models.BooleanField(default=False)
    can_upload_raw_files = models.BooleanField(default=False)

    objects = AuthorManager()

    def __str__(self):
        return self.user.username

    def add_to_graph(self, graph):
        s = Namespaces().authors(self.pk)

        graph.add((s, RDF.type, HERBIE.Author))
        graph.add((s, RDF.type, HERBIE.Client))
        graph.add((s, RDF.type, HERBIE.Clerk))
        graph.add((s, RDFS.label, Literal(f"{self.user.first_name} {self.user.last_name}")))
