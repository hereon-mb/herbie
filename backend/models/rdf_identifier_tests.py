# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

import re
from datetime import datetime

from freezegun import freeze_time

from .rdf_identifier import RDFIdentifier

REGEX_UUID = "[0-9a-zA-Z]*"


@freeze_time(datetime(2021, 9, 7, 10, 30))
def test__rdf_identifier_manager__create__without_container(request, author_alice):
    rdf_identifier = RDFIdentifier.objects.create(created_by=author_alice, rdf_class_slug="protocol-cast")
    request.addfinalizer(lambda: rdf_identifier.delete())
    assert rdf_identifier.container_path_segments == ""
    assert rdf_identifier.created_at == datetime(2021, 9, 7, 10, 30)
    assert rdf_identifier.created_by == author_alice


@freeze_time(datetime(2021, 9, 7, 10, 30))
def test__rdf_identifier_manager__create__with_container(request, author_alice):
    rdf_identifier_container = RDFIdentifier.objects.create(created_by=author_alice, rdf_class_slug="protocol-cast")
    request.addfinalizer(lambda: rdf_identifier_container.delete())

    rdf_identifier = RDFIdentifier.objects.create(
        created_by=author_alice, rdf_class_slug="component", container=rdf_identifier_container
    )
    request.addfinalizer(lambda: rdf_identifier.delete())

    assert rdf_identifier.container_path_segments == rdf_identifier_container.path_segments
    assert rdf_identifier.created_at == datetime(2021, 9, 7, 10, 30)
    assert rdf_identifier.created_by == author_alice


def test__rdf_identifier__path_segments__without_container(request, author_alice):
    rdf_identifier = RDFIdentifier.objects.create(created_by=author_alice, rdf_class_slug="protocol-cast")
    request.addfinalizer(lambda: rdf_identifier.delete())

    assert re.match(f"protocol-casts/{REGEX_UUID}", rdf_identifier.path_segments)


def test__rdf_identifier__path_segments__with_container(request, author_alice):
    rdf_identifier_container = RDFIdentifier.objects.create(created_by=author_alice, rdf_class_slug="protocol-cast")
    request.addfinalizer(lambda: rdf_identifier_container.delete())

    rdf_identifier = RDFIdentifier.objects.create(
        created_by=author_alice, rdf_class_slug="component", container=rdf_identifier_container
    )
    request.addfinalizer(lambda: rdf_identifier.delete())

    assert re.match(f"protocol-casts/{REGEX_UUID}/components/{REGEX_UUID}", rdf_identifier.path_segments)
