# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from uuid import uuid4

from django.db import models
from django.db.models import Prefetch

from .author import Author


class WorkspaceQuerySet(models.QuerySet):
    def with_prefetches(self):
        memberships = Prefetch(
            "memberships",
            Membership.objects.all()
            .select_related("created_by")
            .select_related("created_by__user")
            .select_related("author")
            .select_related("author__user"),
        )
        return self.select_related("created_by").select_related("created_by__user").prefetch_related(memberships)


class WorkspaceManager(models.Manager):
    def get_queryset(self):
        return WorkspaceQuerySet(self.model, using=self._db)


class Workspace(models.Model):
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    deleted_at = models.DateTimeField(blank=True, null=True)

    created_by = models.ForeignKey(Author, related_name="+", on_delete=models.PROTECT)

    uuid = models.UUIDField(default=uuid4, unique=True, editable=False)

    slug = models.SlugField(unique=True)
    name = models.TextField()
    description = models.TextField(blank=True, default="")

    objects = WorkspaceManager()

    def __str__(self) -> str:
        return self.name


class MembershipType(models.IntegerChoices):
    OWNER = 1
    ADMIN = 2
    MANAGER = 3
    EDITOR = 4
    GUEST = 5


class MembershipQuerySet(models.QuerySet):
    def active(self):
        return self.filter(deleted_at__isnull=True)

    def at_least_owner(self):
        return self.active().filter(membership_type=MembershipType.OWNER)

    def at_least_admin(self):
        return self.active().filter(
            membership_type__in=(
                MembershipType.OWNER,
                MembershipType.ADMIN,
            )
        )

    def at_least_manager(self):
        return self.active().filter(
            membership_type__in=(
                MembershipType.OWNER,
                MembershipType.ADMIN,
                MembershipType.MANAGER,
            )
        )

    def at_least_editor(self):
        return self.active().filter(
            membership_type__in=(
                MembershipType.OWNER,
                MembershipType.ADMIN,
                MembershipType.MANAGER,
                MembershipType.EDITOR,
            )
        )


class MembershipManager(models.Manager):
    def get_queryset(self):
        return MembershipQuerySet(self.model, using=self._db)


class Membership(models.Model):
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    deleted_at = models.DateTimeField(blank=True, null=True)

    created_by = models.ForeignKey(Author, related_name="+", on_delete=models.PROTECT)

    author = models.ForeignKey(Author, related_name="+", on_delete=models.PROTECT)
    workspace = models.ForeignKey(Workspace, related_name="memberships", on_delete=models.CASCADE)

    membership_type = models.IntegerField(choices=MembershipType.choices)

    objects = MembershipManager()
