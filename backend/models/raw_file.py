from datetime import datetime
from uuid import uuid4

from django.db import models

from .author import Author


def get_raw_file_path(instance, filename):
    now = datetime.now()
    return "raw/{year:04d}/{month:02d}/{day:02d}/{uuid}/{filename}".format(
        year=now.year,
        month=now.month,
        day=now.day,
        uuid=uuid4(),
        filename=filename,
    )


class RawFile(models.Model):
    created_by = models.ForeignKey(Author, related_name="+", on_delete=models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True)
    file = models.FileField(upload_to=get_raw_file_path, max_length=4096)
