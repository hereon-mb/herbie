# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from django.db import models
from rdflib import RDF, RDFS, Literal

from ..namespace._HERBIE import HERBIE
from ..namespaces import Namespaces


class Department(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    name_en = models.CharField(max_length=256)
    name_de = models.CharField(max_length=256)
    abbreviation = models.CharField(max_length=3)

    def __str__(self):
        return "{} ({})".format(self.name_en, self.abbreviation)

    def add_to_graph(self, graph):
        s = Namespaces().departments(self.pk)

        graph.add((s, RDF.type, HERBIE.Department))
        graph.add((s, RDF.type, HERBIE.Client))
        graph.add((s, RDFS.label, Literal(self.name_en, lang="en")))
        graph.add((s, RDFS.label, Literal(self.name_de, lang="de")))
