# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

import typing
from datetime import datetime
from uuid import uuid4

import shortuuid
from django.core.validators import RegexValidator
from django.db import models

from .author import Author


class RDFIdentifierManager(models.Manager):
    def create_rdf_identifier(self, path_segments, created_by, rdf_class_slug):
        if path_segments is None:
            return self.create(created_by=created_by, rdf_class_slug=rdf_class_slug)
        else:
            path_segments = path_segments.split("/")
            uuid = shortuuid.decode(path_segments[-1])
            rdf_identifier_container = self.get(
                container_path_segments="/".join(path_segments[0:-2]),
                rdf_class_slug=path_segments[-2][:-1],
                uuid=uuid,
            )
            return self.create(created_by=created_by, rdf_class_slug=rdf_class_slug, container=rdf_identifier_container)

    def create(self, created_by: Author, rdf_class_slug: str, container: typing.Optional["RDFIdentifier"] = None):  # type: ignore
        now = datetime.now()

        container_path_segments = ""
        if container is not None:
            container_path_segments = container.path_segments

        return super().create(
            container_path_segments=container_path_segments,
            rdf_class_slug=rdf_class_slug,
            created_at=now,
            created_by=created_by,
        )


REGEX_RDF_CLASS_SLUG = "[a-z][a-z\\-]*[a-z]"


class RDFIdentifier(models.Model):
    container_path_segments = models.TextField(default="")
    rdf_class_slug = models.TextField(validators=[RegexValidator(REGEX_RDF_CLASS_SLUG)])
    uuid = models.UUIDField(default=uuid4, editable=False, unique=True)

    created_at = models.DateTimeField()

    created_by = models.ForeignKey(Author, related_name="+", on_delete=models.PROTECT)

    objects = RDFIdentifierManager()

    @property
    def path_segments(self):
        uuid = shortuuid.encode(self.uuid)
        if self.container_path_segments == "":
            return f"{self.rdf_class_slug}s/{uuid}"
        else:
            return f"{self.container_path_segments}/{self.rdf_class_slug}s/{uuid}"
