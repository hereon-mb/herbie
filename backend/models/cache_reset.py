# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from django.db import models
from django.db.models import Max


def last_modified(queryset):
    last_modified_datetimes = [
        queryset.aggregate(Max("updated_at"))["updated_at__max"],
        CacheReset.objects.aggregate(Max("created_at"))["created_at__max"],
    ]
    last_modified_datetimes = list(filter(None, last_modified_datetimes))
    if last_modified_datetimes:
        return max(last_modified_datetimes)
    else:
        return None


def last_cache_reset_at():
    return CacheReset.objects.aggregate(Max("created_at"))["created_at__max"]


class CacheReset(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.created_at.isoformat()
