from datetime import datetime

from freezegun import freeze_time


def test__rdf_document_version__version_index(rdf_document__unpublished, rdf_document_service):
    initial_datetime = datetime(2022, 12, 1, 10, 30)
    with freeze_time(initial_datetime) as frozen_datetime:
        rdf_document = rdf_document__unpublished()

        frozen_datetime.tick()

        rdf_document = rdf_document_service.publish(rdf_document).rdf_document

        frozen_datetime.tick()

        rdf_document = rdf_document_service.edit(rdf_document).rdf_document

        frozen_datetime.tick()

        rdf_document = rdf_document_service.publish(rdf_document).rdf_document

        frozen_datetime.tick()

        rdf_document = rdf_document_service.edit(rdf_document).rdf_document

        versions = rdf_document.rdf_document_versions.order_by("created_at")
        version_previous = versions[0]
        version_published = versions[1]
        version_draft = versions[2]
        assert version_draft.version_index == 2
        assert version_previous.version_index == 0
        assert version_published.version_index == 1
