# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2024 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

from datetime import datetime
from uuid import uuid4

from django.db import models

from .author import Author
from .workspace import Workspace


def get_file_path(instance, filename):
    now = datetime.now()
    return "ro_crates/{year:04d}/{month:02d}/{day:02d}/{uuid}/{filename}".format(
        year=now.year,
        month=now.month,
        day=now.day,
        uuid=uuid4(),
        filename=filename,
    )


class RoCrateImportQuerySet(models.QuerySet):
    def with_prefetches(self):
        return self.select_related("created_by").select_related("created_by__user")


class RoCrateImportManager(models.Manager):
    def get_queryset(self):
        return RoCrateImportQuerySet(self.model, using=self._db)


class RoCrateImport(models.Model):
    class Status(models.IntegerChoices):
        PENDING = 1
        STARTED = 2
        SUCCESS = 3
        FAILURE = 4

    created_at = models.DateTimeField()
    started_at = models.DateTimeField(blank=True, null=True)
    stopped_at = models.DateTimeField(blank=True, null=True)

    status = models.IntegerField(choices=Status.choices)

    created_by = models.ForeignKey(Author, related_name="+", on_delete=models.PROTECT)
    workspace = models.ForeignKey(Workspace, on_delete=models.PROTECT)

    file = models.FileField(upload_to=get_file_path, max_length=4096)

    objects = RoCrateImportManager()
