from rdflib import RDF, Namespace

from .services.triple_store import TripleStore
from .sparql import (
    Create,
    Delete,
    Drop,
    GraphPattern,
    Insert,
    InsertData,
    Select,
    Var,
)

RES = Namespace("http://www.w3.org/2005/sparql-results#")
EX = Namespace("http://example.org/")


def test__create(request, iri_random):
    iri = iri_random()
    request.addfinalizer(lambda: TripleStore().update(Drop(iri)))

    graph = TripleStore().update(Create(iri))
    assert _message(graph) == f"Create graph <{iri}> -- done"


def test__drop(iri_random):
    iri = iri_random()
    TripleStore().update(Create(iri))
    graph = TripleStore().update(Drop(iri))
    assert _message(graph) == f"Drop graph <{iri}> -- done"


def test__insert_data(graph_empty):
    iri = graph_empty()
    graph = TripleStore().update(
        InsertData(
            GraphPattern(
                iri,
                [(EX.alice, RDF.type, EX.person)],
            ),
        )
    )
    assert _message(graph) == f"Insert into <{iri}>, 1 (or less) triples -- done"


def test__insert(graph_empty):
    iri = graph_empty()
    graph = TripleStore().update(
        Insert(
            GraphPattern(
                iri,
                [(EX.alice, RDF.type, EX.person)],
            ),
        )
    )
    assert _message(graph) == f"Insert into <{iri}>, 1 (or less) triples -- done"


def test__delete(graph_from):
    iri = graph_from([(EX.alice, RDF.type, EX.person)])
    assert _triples_count(iri) == 1

    s, p, o = Var("s"), Var("p"), Var("o")
    graph = TripleStore().update(
        Delete(
            GraphPattern(
                iri,
                [(s, p, o)],
            ),
        ).where(
            GraphPattern(
                iri,
                [(s, p, o)],
            ),
        )
    )
    assert _message(graph) == f"Delete from <{iri}>, 1 (or less) triples -- done"
    assert _triples_count(iri) == 0


def _message(graph):
    res = graph.query(
        """
        PREFIX res: <http://www.w3.org/2005/sparql-results#>
        SELECT ?value
        WHERE {
            [] a res:ResultSet ;
              res:solution [
                res:binding [
                  res:value ?value ;
                ] ;
              ] ;
            .
        }
        """
    )
    return str(res.bindings[0]["value"])


def _triples_count(iri):
    s, p, o = Var("s"), Var("p"), Var("o")
    response = TripleStore().query(
        Select("COUNT(*) AS ?count").where(
            GraphPattern(
                iri,
                [(s, p, o)],
            ),
        )
    )
    return int(response["results"]["bindings"][0]["count"]["value"])
