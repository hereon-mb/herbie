# Herbie - The electronic laboratory notebook (ELN) named "Herbie" is
# a client-server based web application for research documentation
#
# Copyright (C) 2020 - 2023 Helmholtz-Zentrum hereon GmbH (hereon)
#
# Authors: Fabian Kirchner, Catriona Eschke
#
# This program is subject to the terms and conditions for non-commercial use of
# ELN. You can find the license text in the file LICENSE.en and under
# http://codebase.helmholtz.cloud/hereon-mb/herbie/-/blob/master/LICENSE.en. If
# you have any questions or comments, you can contact us at hereon at
# herbie@hereon.de or by mail at Helmholtz-Zentrum hereon GmbH,
# Max-Planck-Straße 1, 21502 Geesthacht, Germany.

import wrapt  # type: ignore
from django.apps import AppConfig
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured


# we need to make sure that parseQuery calls are thread safe, c.f.
# https://github.com/RDFLib/rdflib/issues/1282
@wrapt.patch_function_wrapper("rdflib.plugins.sparql.parser", "parseQuery")
@wrapt.synchronized
def parseQuery_threadsafe(wrapped, instance, args, kwargs):
    rv = wrapped(*args, **kwargs)
    return rv


# we need to make sure that Graph.query calls are thread safe, c.f.
# https://github.com/RDFLib/rdflib/issues/1282
@wrapt.patch_function_wrapper("rdflib", "Graph.query")
@wrapt.synchronized
def query_threadsafe(wrapped, instance, args, kwargs):
    rv = wrapped(*args, **kwargs)
    return rv


class BackendConfig(AppConfig):
    name = "backend"

    def ready(self) -> None:
        if settings.IRI_PREFIX is None:
            raise ImproperlyConfigured("IRI_PREFIX must be set")
