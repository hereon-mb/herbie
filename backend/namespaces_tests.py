from rdflib import URIRef

from .namespaces import Namespaces


def test__authors():
    assert Namespaces().authors() == URIRef("http://testserver/authors/")
    assert Namespaces().authors(1) == URIRef("http://testserver/authors/1/")


def test__datasets():
    assert Namespaces().datasets("some-uuid") == URIRef("http://testserver/datasets/some-uuid/")
    assert Namespaces().datasets_draft("some-uuid") == URIRef("http://testserver/datasets/some-uuid/draft/")
    assert Namespaces().datasets_draft_generated("some-uuid") == URIRef("http://testserver/datasets/some-uuid/draft/generated/")
    assert Namespaces().datasets_draft_persisted("some-uuid") == URIRef("http://testserver/datasets/some-uuid/draft/persisted/")
    assert Namespaces().datasets_version("some-uuid", 1) == URIRef("http://testserver/datasets/some-uuid/versions/1/")
    assert Namespaces().datasets_version_generated("some-uuid", 1) == URIRef(
        "http://testserver/datasets/some-uuid/versions/1/generated/"
    )
    assert Namespaces().datasets_version_persisted("some-uuid", 1) == URIRef(
        "http://testserver/datasets/some-uuid/versions/1/persisted/"
    )
