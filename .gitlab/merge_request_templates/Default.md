## What has been changed?
<!-- Give a short summary of the changes introduced.  A list of bullet points is fine -->


## Related issues
<!-- Paste links to all issues which are affected by this merge request -->


## Checklist
<!-- Make sure to go through every item and either check them of or write why it does not apply/must not be done in this merge request -->

- [ ] :pen_fountain: i18n keys are updated according to [coding guidelines](https://hereon-mb.pages.hzdr.de/herbie/development.html#fluent-i18n) (en-US suffices)
- [ ] :fingers_crossed: New code is covered by tests (especially for backend and protocol independent code)
- [ ] :package: CI Pipeline is green
- [ ] :unicorn: (Basic) QA (Quality Assurance) testing (e.g. trying out/playing with the feature in the browser)
