## Is your proposal related to a problem?
<!-- Provide a clear and concise description of what the problem is.  For example, "I'm always frustrated when..." -->


## Who will benefit?
<!-- Will this fix a problem that only one user has, or will it benefit a lot of people? -->


## Describe the solution you'd like
<!-- Provide a clear and concise description of what you want to happen. -->


## Describe alternatives you've considered
<!-- Let us know about other solutions you've tried or researched. -->


## Additional context
<!-- Is there anything else you can add about the proposal?  You might want to link to related issues here, if you haven't already.  -->



<!-- Label the issue with one of the following priorities.  Delete the lines which do not apply.  -->

/label ~High    <!-- This will bring a huge increase in performance/productivity/usability/... -->
/label ~Medium  <!-- This will bring a good increas in performance/productivity/usability/... -->
/label ~Low     <!-- anything else e.g., trivial, minor improvements -->
/label ~Enhancement ~Inbox
