## Summary
<!-- Summarize the bug encountered concisely -->


## Steps to reproduce
<!-- How one can reproduce the issue - this is very important -->


## What is the current bug behavior?
<!-- What actually happens -->


## What is the expected correct behavior?
<!-- What you should see instead -->


## Relevant logs and/or screenshots
<!-- Paste any relevant logs - please use code blocks (```) to format console output, logs, and code, as it's very hard to read otherwise. -->


## Possible fixes
<!-- If you can, link to the line of code that might be responsible for the problem -->


## Priority/Severity

- [ ] High (anything that impacts the normal user flow or blocks app usage)
- [ ] Medium (anything that negatively affects the user experience)
- [ ] Low (anything else e.g., typos, missing icons, small layout issues, etc.)

/label ~Bug ~Inbox
