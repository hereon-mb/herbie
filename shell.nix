{ pkgs ? import sources.nixpkgs { }
, sources ? import ./nix/sources.nix
}:

with pkgs;

let

  python =
    python312.withPackages (pythonPkgs: [
      pythonPkgs.black
    ]);

  pt = writeShellScriptBin "pt" ''
      ./scripts/test.sh "$@"
    '';

in

mkShell {
  packages = [
    apache-jena
    cyrus_sasl
    elmPackages.elm
    elmPackages.elm-doc-preview
    elmPackages.elm-format
    elmPackages.elm-language-server
    elmPackages.elm-test
    firefox-esr
    graphviz
    inotify-tools
    kompose
    libffi
    nodejs_20
    openldap
    openshift
    openssl
    podman
    postgresql
    pt
    python
    ruff
    zip
  ];
  shellHook = ''
    if [ ! -d .venv ]; then
      python -m venv .venv
    fi

    source .venv/bin/activate

    pip install --upgrade pip
    pip install --upgrade pipenv
    pipenv sync --dev

    # set SOURCE_DATE_EPOCH so that we can use python wheels
    export SOURCE_DATE_EPOCH=$(date +%s)
    export PYTHONBREAKPOINT=ipdb.set_trace
    export DJANGO_COLORS=dark

    export $(cat .env | xargs)
  '';
}
